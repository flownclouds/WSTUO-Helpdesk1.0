<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:template match="/drawing/figures">
<process name="test" xmlns="http://jbpm.org/4.4/jpdl">
<!-- start 
<start g="100,500,100,100" name="start1">
      <transition g="-18,-28" name="to task1" to="task1"/>
</start>
-->
<xsl:for-each select="start">
<xsl:variable name="StartId" select="@id"/>
<start>
	<xsl:attribute name="g">
		<xsl:value-of select="@x"></xsl:value-of>,<xsl:value-of select="@y"></xsl:value-of>,<xsl:value-of select="@w"></xsl:value-of>,<xsl:value-of select="@h"></xsl:value-of>
	</xsl:attribute>
	<xsl:attribute name="name">
		<xsl:value-of select="@name"></xsl:value-of>
	</xsl:attribute>
	
	<!-- transition -->
	<xsl:for-each select="/drawing/figures/transition">
	
	<xsl:variable name="StartStartREF_StartId" select="startConnector/rConnector/Owner/start/@ref"/>
	<xsl:variable name="EndStartREF_StartId" select="endConnector/rConnector/Owner/start/@ref"/>
	
	<xsl:variable name="EndForkREF_StartId" select="endConnector/rConnector/Owner/fork/@ref"/>
	
	<xsl:variable name="EndTaskREF_StartId" select="endConnector/rConnector/Owner/task/@ref"/>

	<xsl:variable name="EndEndREF_StartId" select="endConnector/rConnector/Owner/end/@ref"/>
	
	<xsl:variable name="EndDecisionREF_StartId" select="endConnector/rConnector/Owner/decision/@ref"/>
	
	<xsl:variable name="EndForeachREF_StartId" select="endConnector/rConnector/Owner/foreach/@ref"/>
	
	<xsl:variable name="EndJoinREF_StartId" select="endConnector/rConnector/Owner/join/@ref"/>
	
	<xsl:variable name="EndStateREF_StateId" select="endConnector/rConnector/Owner/state/@ref"/>
	
	<xsl:if test="$StartStartREF_StartId=$StartId">
		<transition>
		<xsl:attribute name="name">
			<xsl:value-of select="@name"></xsl:value-of>
		</xsl:attribute>
		
		<xsl:attribute name="g">
			<xsl:value-of select="@g"></xsl:value-of>
		</xsl:attribute>
		
		<!-- to -->
		<xsl:attribute name="to">
			<!-- 任务 -->
			<xsl:value-of select="/drawing/figures/task[@id=$EndTaskREF_StartId]/@name"></xsl:value-of>
			<!-- 结束 -->
			<xsl:value-of select="/drawing/figures/end[@id=$EndEndREF_StartId]/@name"></xsl:value-of>
			<!-- 分支 -->
			<xsl:value-of select="/drawing/figures/decision[@id=$EndDecisionREF_StartId]/@name"></xsl:value-of>
			<!-- 循环 -->
			<xsl:value-of select="/drawing/figures/foreach[@id=$EndForeachREF_StartId]/@name"></xsl:value-of>
			<!-- 汇集任务-->
			<xsl:value-of select="/drawing/figures/join[@id=$EndJoinREF_StartId]/@name"></xsl:value-of>
			<!-- fork -->
			<xsl:value-of select="/drawing/figures/fork[@id=$EndForkREF_StartId]/@name"></xsl:value-of>
			<!-- state -->
			<xsl:value-of select="/drawing/figures/state[@id=$EndStateREF_StateId]/@name"></xsl:value-of>
			
		</xsl:attribute>
		</transition>
	</xsl:if>
	</xsl:for-each>
	
	<!-- transition -->
	<xsl:if test="$StartId = transition/startConnector/rConnector/Owner/start/@ref">
		<transition>
			<xsl:attribute name="g">
				<xsl:value-of select="transition/@g"></xsl:value-of>
			</xsl:attribute>
			<xsl:attribute name="name">
				<xsl:value-of select="transition/@name"></xsl:value-of>
			</xsl:attribute>
			<!-- to -->
			<xsl:attribute name="to">
				<xsl:value-of select="/drawing/figures/transition/endConnector/rConnector/Owner/task/@ref" />-
				<xsl:value-of select="/drawing/figures/transition/endConnector/rConnector/Owner/state/@ref" />
				<!-- 任务 -->
				<xsl:value-of select="/drawing/figures/task[@id=/drawing/figures/transition/endConnector/rConnector/Owner/task/@ref]"></xsl:value-of>
				<!-- 分支 -->
				<xsl:value-of select="decision[@id=/drawing/figures/transition/endConnector/rConnector/Owner/decision/@ref]"></xsl:value-of>
				<!-- state -->
				<xsl:value-of select="state[@id=/drawing/figures/transition/endConnector/rConnector/Owner/state/@ref]"></xsl:value-of>
				
			</xsl:attribute>
		</transition>
	</xsl:if>
</start>
</xsl:for-each>


<!-- state
<state g="12,206,92,52" name="请求登记">
      <transition g="25,28" name="是否审批" to="是否审批"/>
   </state>
 -->
<xsl:for-each select="state">
<xsl:variable name="StateId" select="@id"/>
<state>
	<xsl:attribute name="name">
		<xsl:value-of select="@name"></xsl:value-of>
	</xsl:attribute>
	<xsl:attribute name="g">
		<xsl:value-of select="@x"></xsl:value-of>,<xsl:value-of select="@y"></xsl:value-of>,<xsl:value-of select="@w"></xsl:value-of>,<xsl:value-of select="@h"></xsl:value-of>
	</xsl:attribute>
	
	<!-- transition -->
	<xsl:for-each select="/drawing/figures/transition">
	
	<xsl:variable name="StartStateREF_StateId" select="startConnector/rConnector/Owner/state/@ref"/>
	<xsl:variable name="EndStateREF_StateId" select="endConnector/rConnector/Owner/state/@ref"/>
	
	<xsl:variable name="EndForkREF_StateId" select="startConnector/rConnector/Owner/fork/@ref"/>
	
	<xsl:variable name="EndTaskREF_StateId" select="endConnector/rConnector/Owner/task/@ref"/>

	<xsl:variable name="EndEndREF_StateId" select="endConnector/rConnector/Owner/end/@ref"/>
	
	<xsl:variable name="EndDecisionREF_StateId" select="endConnector/rConnector/Owner/decision/@ref"/>
	
	<xsl:variable name="EndForeachREF_StateId" select="endConnector/rConnector/Owner/foreach/@ref"/>
	
	<xsl:variable name="EndJoinREF_StateId" select="endConnector/rConnector/Owner/join/@ref"/>
	
	
	
	<xsl:if test="$StartStateREF_StateId=$StateId">
		<transition>
		<xsl:attribute name="name">
			<xsl:value-of select="@name"></xsl:value-of>
		</xsl:attribute>
		
		<xsl:attribute name="g">
			<xsl:value-of select="@g"></xsl:value-of>
		</xsl:attribute>
		
		<!-- to -->
		<xsl:attribute name="to">
			<!-- 任务 -->
			<xsl:value-of select="/drawing/figures/task[@id=$EndTaskREF_StateId]/@name"></xsl:value-of>
			<!-- 结束 -->
			<xsl:value-of select="/drawing/figures/end[@id=$EndEndREF_StateId]/@name"></xsl:value-of>
			<!-- 分支 -->
			<xsl:value-of select="/drawing/figures/decision[@id=$EndDecisionREF_StateId]/@name"></xsl:value-of>
			<!-- 循环 -->
			<xsl:value-of select="/drawing/figures/foreach[@id=$EndForeachREF_StateId]/@name"></xsl:value-of>
			<!-- 汇集任务-->
			<xsl:value-of select="/drawing/figures/join[@id=$EndJoinREF_StateId]/@name"></xsl:value-of>
			<!-- fork -->
			<xsl:value-of select="/drawing/figures/fork[@id=$EndForkREF_StateId]/@name"></xsl:value-of>
			
		</xsl:attribute>
		</transition>
	</xsl:if>
	</xsl:for-each>
</state>
</xsl:for-each>

<!-- end
<end g="388,209,48,48" name="end1"/>
 -->
<end>
	<xsl:attribute name="g">
		<xsl:value-of select="end/@x"></xsl:value-of>,<xsl:value-of select="end/@y"></xsl:value-of>,<xsl:value-of select="end/@w"></xsl:value-of>,<xsl:value-of select="end/@h"></xsl:value-of>
	</xsl:attribute>
	<xsl:attribute name="name">
		<xsl:value-of select="end/@name"></xsl:value-of>
	</xsl:attribute>
</end>

<!-- task 
<task g="183,229,92,52" name="task1">
      <transition g="-21,-20" name="to end1" to="end1"/>
</task>
-->
<xsl:for-each select="task">
<xsl:variable name="TId" select="@id"/>
<task>
	<xsl:attribute name="g">
		<xsl:value-of select="@x"></xsl:value-of>,<xsl:value-of select="@y"></xsl:value-of>,<xsl:value-of select="@w"></xsl:value-of>,<xsl:value-of select="@h"></xsl:value-of>
	</xsl:attribute>
	<xsl:attribute name="name">
		<xsl:value-of select="@name"></xsl:value-of>
	</xsl:attribute>
	
	<!-- transition -->
	<xsl:for-each select="/drawing/figures/transition">
	<xsl:variable name="StartTaskREF" select="startConnector/rConnector/Owner/task/@ref"/>
	<xsl:variable name="EndTaskREF" select="endConnector/rConnector/Owner/task/@ref"/>

	<xsl:variable name="EndEndREF" select="endConnector/rConnector/Owner/end/@ref"/>
	
	<xsl:variable name="EndDecisionREF" select="endConnector/rConnector/Owner/decision/@ref"/>
	
	<xsl:variable name="EndForeachREF" select="endConnector/rConnector/Owner/foreach/@ref"/>
	
	<xsl:variable name="EndJoinREF" select="endConnector/rConnector/Owner/join/@ref"/>
	
	<xsl:variable name="EndForkREF" select="endConnector/rConnector/Owner/fork/@ref"/>
	
	<xsl:if test="$StartTaskREF=$TId">
		<transition>
		<xsl:attribute name="g">
			<xsl:value-of select="@g"></xsl:value-of>
		</xsl:attribute>
		<xsl:attribute name="name">
			<xsl:value-of select="@name"></xsl:value-of>
		</xsl:attribute>
		<!-- to -->
		<xsl:attribute name="to">
			<!-- 任务 -->
			<xsl:value-of select="/drawing/figures/task[@id=$EndTaskREF]/@name"></xsl:value-of>
			<!-- 结束 -->
			<xsl:value-of select="/drawing/figures/end[@id=$EndEndREF]/@name"></xsl:value-of>
			<!-- 分支 -->
			<xsl:value-of select="/drawing/figures/decision[@id=$EndDecisionREF]/@name"></xsl:value-of>
			<!-- 循环 -->
			<xsl:value-of select="/drawing/figures/foreach[@id=$EndForeachREF]/@name"></xsl:value-of>
			<!-- 汇集任务 -->
			<xsl:value-of select="/drawing/figures/join[@id=$EndJoinREF]/@name"></xsl:value-of>
			<!-- fork -->
			<xsl:value-of select="/drawing/figures/fork[@id=$EndForkREF]/@name"></xsl:value-of>
		</xsl:attribute>
		
		</transition>
	</xsl:if>
	</xsl:for-each>
</task>
</xsl:for-each>
<!-- decision 
<decision g="145,209,48,48" name="是否审批">
      <transition g="-37,-6" name="审批" to="审批">
      	<condition expr="#{dto.approvalNo != 0}"/>
      </transition>
      <transition g="-23,-28" name="一线处理" to="一线处理">
      	<condition expr="#{dto.approvalNo == 0}"/>
      </transition>
   </decision>
-->
<xsl:for-each select="decision">
<xsl:variable name="DecisionId" select="@id"/>
	<decision>
		<xsl:attribute name="g">
		<xsl:value-of select="@x"></xsl:value-of>,<xsl:value-of select="@y"></xsl:value-of>,<xsl:value-of select="@w"></xsl:value-of>,<xsl:value-of select="@h"></xsl:value-of>
	</xsl:attribute>
	<xsl:attribute name="name">
		<xsl:value-of select="@name"></xsl:value-of>
	</xsl:attribute>
	
	<!-- transition -->
	<xsl:for-each select="/drawing/figures/transition">
	
	<xsl:variable name="StartDecisionREF_DecisionId" select="startConnector/rConnector/Owner/decision/@ref"/>
	<xsl:variable name="EndDecisionREF_DecisionId" select="endConnector/rConnector/Owner/decision/@ref"/>
	
	<xsl:variable name="EndTaskREF_DecisionId" select="endConnector/rConnector/Owner/task/@ref"/>

	<xsl:variable name="EndEndREF_DecisionId" select="endConnector/rConnector/Owner/end/@ref"/>
	
	<xsl:variable name="EndForeachREF_DecisionId" select="endConnector/rConnector/Owner/foreach/@ref"/>
	
	<xsl:variable name="EndJoinREF_DecisionId" select="endConnector/rConnector/Owner/join/@ref"/>
	
	<xsl:variable name="EndForkREF_DecisionId" select="endConnector/rConnector/Owner/fork/@ref"/>
	
	
	<xsl:if test="$StartDecisionREF_DecisionId=$DecisionId">
		<transition>
		<xsl:attribute name="g">
			<xsl:value-of select="@g"></xsl:value-of>
		</xsl:attribute>
		<xsl:attribute name="name">
			<xsl:value-of select="@name"></xsl:value-of>
		</xsl:attribute>
		<!-- to -->
		<xsl:attribute name="to">
		
			<!-- 分支 -->
			<xsl:value-of select="/drawing/figures/decision[@id=$EndDecisionREF_DecisionId]/@name"></xsl:value-of>
			<!-- 任务 -->
			<xsl:value-of select="/drawing/figures/task[@id=$EndTaskREF_DecisionId]/@name"></xsl:value-of>
			<!-- 结束 -->
			<xsl:value-of select="/drawing/figures/end[@id=$EndEndREF_DecisionId]/@name"></xsl:value-of>
			<!-- 循环 -->
			<xsl:value-of select="/drawing/figures/foreach[@id=$EndForeachREF_DecisionId]/@name"></xsl:value-of>
			<!-- 汇集任务-->
			<xsl:value-of select="/drawing/figures/join[@id=$EndJoinREF_DecisionId]/@name"></xsl:value-of>
			<!-- fork -->
			<xsl:value-of select="/drawing/figures/fork[@id=$EndForkREF_DecisionId]/@name"></xsl:value-of>
			
		</xsl:attribute>
		<condition>
			<xsl:attribute name="expr">#{dto.approvalNo==0}</xsl:attribute>
		</condition>
		</transition>
	</xsl:if>
	</xsl:for-each>
	
	
	</decision>	
</xsl:for-each>

<!-- foreach
<foreach g="265,74,48,48" in="#{dto.approveCommentDTO}" name="foreach1" var="approvers">
      <transition g="-19,23" name="变更审批" to="变更审批"/>
   </foreach>
 -->
<xsl:for-each select="foreach">
<xsl:variable name="ForeachId" select="@id"/>
<foreach in="" var="">
	<xsl:attribute name="name">
		<xsl:value-of select="@name"></xsl:value-of>
	</xsl:attribute>
	<xsl:attribute name="g">
		<xsl:value-of select="@x"></xsl:value-of>,<xsl:value-of select="@y"></xsl:value-of>,<xsl:value-of select="@w"></xsl:value-of>,<xsl:value-of select="@h"></xsl:value-of>
	</xsl:attribute>
	<!-- transition -->
	<xsl:for-each select="/drawing/figures/transition">
	
	<xsl:variable name="StartForeachREF_ForeachId" select="startConnector/rConnector/Owner/foreach/@ref"/>
	<xsl:variable name="EndForeachREF_ForeachId" select="endConnector/rConnector/Owner/foreach/@ref"/>
	
	<xsl:variable name="EndTaskREF_ForeachId" select="endConnector/rConnector/Owner/task/@ref"/>
	
	<xsl:variable name="EndEndREF_ForeachId" select="endConnector/rConnector/Owner/end/@ref"/>
	
	<xsl:variable name="EndDecisionREF_ForeachId" select="endConnector/rConnector/Owner/decision/@ref"/>
	
	<xsl:variable name="EndJoinREF_ForeachId" select="endConnector/rConnector/Owner/join/@ref"/>
	
	<xsl:variable name="EndForkREF_ForeachId" select="endConnector/rConnector/Owner/fork/@ref"/>
	
	
	<xsl:if test="$StartForeachREF_ForeachId=$ForeachId">
		<transition>
		<xsl:attribute name="name">
			<xsl:value-of select="@name"></xsl:value-of>
		</xsl:attribute>
		
		<xsl:attribute name="g">
			<xsl:value-of select="@g"></xsl:value-of>
		</xsl:attribute>
		<xsl:attribute name="in">
		<xsl:value-of>#{approveCommentDTO}</xsl:value-of>
		</xsl:attribute>
		<xsl:attribute name="var">
			#{approvers}
		</xsl:attribute>
		<!-- to -->
		<xsl:attribute name="to">
			<!-- 任务 -->
			<xsl:value-of select="/drawing/figures/task[@id=$EndTaskREF_ForeachId]/@name"></xsl:value-of>
			<!-- 结束 -->
			<xsl:value-of select="/drawing/figures/end[@id=$EndEndREF_ForeachId]/@name"></xsl:value-of>
			<!-- 分支 -->
			<xsl:value-of select="/drawing/figures/decision[@id=$EndDecisionREF_ForeachId]/@name"></xsl:value-of>
			<!-- 循环 -->
			<xsl:value-of select="/drawing/figures/foreach[@id=$EndForeachREF_ForeachId]/@name"></xsl:value-of>
			<!-- 汇集任务-->
			<xsl:value-of select="/drawing/figures/join[@id=$EndJoinREF_ForeachId]/@name"></xsl:value-of>
			<!-- fork -->
			<xsl:value-of select="/drawing/figures/fork[@id=$EndForkREF_ForeachId]/@name"></xsl:value-of>
			
		</xsl:attribute>
		</transition>
	</xsl:if>
	</xsl:for-each>
</foreach>
</xsl:for-each>


<!-- join
<join g="490,75,48,48" multiplicity="#{dto.approverNum}" name="join1">
      <transition g="35,48" name="判断是否同意" to="exclusive1"/>
   </join>
 -->
<xsl:for-each select="join">
<xsl:variable name="JoinId" select="@id"/>
<join multiplicity="">
	<xsl:attribute name="name">
		<xsl:value-of select="@name"></xsl:value-of>
	</xsl:attribute>
	<xsl:attribute name="g">
		<xsl:value-of select="@x"></xsl:value-of>,<xsl:value-of select="@y"></xsl:value-of>,<xsl:value-of select="@w"></xsl:value-of>,<xsl:value-of select="@h"></xsl:value-of>
	</xsl:attribute>
	<!-- transition -->
	<xsl:for-each select="/drawing/figures/transition">
	
	<xsl:variable name="StartJoinREF_JoinId" select="startConnector/rConnector/Owner/join/@ref"/>
	<xsl:variable name="EndJoinREF_JoinId" select="endConnector/rConnector/Owner/join/@ref"/>
	
	<xsl:variable name="EndTaskREF_JoinId" select="endConnector/rConnector/Owner/task/@ref"/>
	
	<xsl:variable name="EndEndREF_JoinId" select="endConnector/rConnector/Owner/end/@ref"/>
	
	<xsl:variable name="EndDecisionREF_JoinId" select="endConnector/rConnector/Owner/decision/@ref"/>

	<xsl:variable name="EndForeachREF_JoinId" select="endConnector/rConnector/Owner/foreach/@ref"/>
	
	<xsl:variable name="EndForkREF_JoinId" select="endConnector/rConnector/Owner/fork/@ref"/>
	
	
	<xsl:if test="$StartJoinREF_JoinId=$JoinId">
		<transition>
		<xsl:attribute name="name">
			<xsl:value-of select="@name"></xsl:value-of>
		</xsl:attribute>
		
		<xsl:attribute name="g">
			<xsl:value-of select="@g"></xsl:value-of>
		</xsl:attribute>
		
		<xsl:attribute name="multiplicity">
			#{approverNum}
		</xsl:attribute>
		
		<!-- to -->
		<xsl:attribute name="to">
			<!-- 任务 -->
			<xsl:value-of select="/drawing/figures/task[@id=$EndTaskREF_JoinId]/@name"></xsl:value-of>
			<!-- 结束 -->
			<xsl:value-of select="/drawing/figures/end[@id=$EndEndREF_JoinId]/@name"></xsl:value-of>
			<!-- 分支 -->
			<xsl:value-of select="/drawing/figures/decision[@id=$EndDecisionREF_JoinId]/@name"></xsl:value-of>
			<!-- 循环 -->
			<xsl:value-of select="/drawing/figures/foreach[@id=$EndForeachREF_JoinId]/@name"></xsl:value-of>
			<!-- 汇集任务-->
			<xsl:value-of select="/drawing/figures/join[@id=$EndJoinREF_JoinId]/@name"></xsl:value-of>
			<!-- fork -->
			<xsl:value-of select="/drawing/figures/fork[@id=$EndForkREF_JoinId]/@name"></xsl:value-of>
			
		</xsl:attribute>
		</transition>
	</xsl:if>
	</xsl:for-each>
</join>
</xsl:for-each>


<!-- fork
<fork g="309,269,48,48" name="fork1">
      <transition g="-50,-22" name="to end1" to="end1"/>
   </fork>
 -->
<xsl:for-each select="fork">
<xsl:variable name="ForkId" select="@id"/>
<fork>
	<xsl:attribute name="name">
		<xsl:value-of select="@name"></xsl:value-of>
	</xsl:attribute>
	<xsl:attribute name="g">
		<xsl:value-of select="@x"></xsl:value-of>,<xsl:value-of select="@y"></xsl:value-of>,<xsl:value-of select="@w"></xsl:value-of>,<xsl:value-of select="@h"></xsl:value-of>
	</xsl:attribute>
	
	<!-- transition -->
	<xsl:for-each select="/drawing/figures/transition">
	
	<xsl:variable name="StartForkREF_ForkId" select="startConnector/rConnector/Owner/fork/@ref"/>
	<xsl:variable name="EndForkREF_ForkId" select="endConnector/rConnector/Owner/fork/@ref"/>
	
	
	<xsl:variable name="EndTaskREF_ForkId" select="endConnector/rConnector/Owner/task/@ref"/>

	<xsl:variable name="EndEndREF_ForkId" select="endConnector/rConnector/Owner/end/@ref"/>
	
	<xsl:variable name="EndDecisionREF_ForkId" select="endConnector/rConnector/Owner/decision/@ref"/>
	
	<xsl:variable name="EndForeachREF_ForkId" select="endConnector/rConnector/Owner/foreach/@ref"/>
	
	<xsl:variable name="EndJoinREF_ForkId" select="endConnector/rConnector/Owner/join/@ref"/>
	
	
	
	<xsl:if test="$StartForkREF_ForkId=$ForkId">
		<transition>
		<xsl:attribute name="name">
			<xsl:value-of select="@name"></xsl:value-of>
		</xsl:attribute>
		
		<xsl:attribute name="g">
			<xsl:value-of select="@g"></xsl:value-of>
		</xsl:attribute>
		
		<!-- to -->
		<xsl:attribute name="to">
			<!-- 任务 -->
			<xsl:value-of select="/drawing/figures/task[@id=$EndTaskREF_ForkId]/@name"></xsl:value-of>
			<!-- 结束 -->
			<xsl:value-of select="/drawing/figures/end[@id=$EndEndREF_ForkId]/@name"></xsl:value-of>
			<!-- 分支 -->
			<xsl:value-of select="/drawing/figures/decision[@id=$EndDecisionREF_ForkId]/@name"></xsl:value-of>
			<!-- 循环 -->
			<xsl:value-of select="/drawing/figures/foreach[@id=$EndForeachREF_ForkId]/@name"></xsl:value-of>
			<!-- 汇集任务-->
			<xsl:value-of select="/drawing/figures/join[@id=$EndJoinREF_ForkId]/@name"></xsl:value-of>
			<!-- fork -->
			<xsl:value-of select="/drawing/figures/fork[@id=$EndForkREF_ForkId]/@name"></xsl:value-of>
			
		</xsl:attribute>
		</transition>
	</xsl:if>
	</xsl:for-each>
</fork>
</xsl:for-each>






</process>
</xsl:template>
</xsl:stylesheet>
