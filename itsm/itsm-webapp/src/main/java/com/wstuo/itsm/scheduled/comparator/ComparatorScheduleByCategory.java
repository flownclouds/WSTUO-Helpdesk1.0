package com.wstuo.itsm.scheduled.comparator;

import java.util.Comparator;

import com.wstuo.common.scheduled.dto.ScheduleShowDataDTO;

/**
 * 人员行程分类比较器
 * @author Ciel
 *
 */
@SuppressWarnings("rawtypes")
public class ComparatorScheduleByCategory implements Comparator {
	public int compare(Object arg0, Object arg1) {
		ScheduleShowDataDTO dto0 = (ScheduleShowDataDTO)arg0;
		ScheduleShowDataDTO dto1 = (ScheduleShowDataDTO)arg1;
		if(dto0.getCategory()==null)
			dto0.setCategory("null");
		
		if(dto1.getCategory()==null)
			dto1.setCategory("null");
		
		if(dto0.getTechnician()==null)
			dto0.setTechnician("null");
		
		if(dto1.getTechnician()==null)
			dto1.setTechnician("null");
		
		int flag=dto0.getCategory().compareTo(dto1.getCategory());
		if(flag==0){
			return dto0.getTechnician().compareTo(dto1.getTechnician());
		}else{
			return flag;
		}
	}
}
