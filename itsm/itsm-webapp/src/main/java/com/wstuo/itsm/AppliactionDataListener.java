package com.wstuo.itsm;

import java.util.Calendar; 
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.wstuo.common.config.category.service.IEventCategoryService;
import com.wstuo.common.sla.dao.ISLAContractDAO;
import com.wstuo.common.sla.dao.SLAContractDAO;
import com.wstuo.common.config.moduleManage.entity.ModuleManage;
import com.wstuo.common.config.moduleManage.service.IModuleManageService;
import com.wstuo.common.security.dao.IOrganizationDAO;
import com.wstuo.common.security.dao.IResourceDAO;
import com.wstuo.common.security.dao.OrganizationDAO;
import com.wstuo.common.security.dao.ResourceDAO;
import com.wstuo.common.config.moduleManage.dto.ModuleManageDTO;

/**
 * 上下文数据监听
 * @author will
 *
 */
public class AppliactionDataListener implements ServletContextListener {
	private Boolean configureItemHave=false;

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(event.getServletContext());
		//初始化机构信息
		IOrganizationDAO organizationDAO = (IOrganizationDAO) ctx.getBean("organizationDAO");
		OrganizationDAO.latesOrganizationNo=organizationDAO.getNextOrgNo();

		
	    //初始化资源信息
		IResourceDAO resourceDAO = (IResourceDAO) ctx.getBean("resourceDAO");
		ResourceDAO.latesResNo=resourceDAO.getNextResNo();
		
		//同步知识库分类和资源
		IEventCategoryService eventCategoryService=(IEventCategoryService) ctx.getBean("eventCategoryService");
		eventCategoryService.syncKnowledgeCategorys();
		
		//同步SLA
		ISLAContractDAO contractDAO=(ISLAContractDAO) ctx.getBean("slaContractDAO");
		SLAContractDAO.latesSLANo=contractDAO.getNextResNo();
		
		
		/**
		 * 模块管理
		 */
		IModuleManageService moduleManageService=(IModuleManageService) ctx.getBean("moduleManageService");
		List<ModuleManage> lis=moduleManageService.findAll();
		if(lis!=null && lis.size()>0){
			for(ModuleManage module:lis){
				if(module.getModuleName().equals("request")){
					ctx.getServletContext().setAttribute("requestHave", true);
				}
				/*if(module.getModuleName().equals("problem")){
					ctx.getServletContext().setAttribute("problemHave", true);
				}
				if(module.getModuleName().equals("change")){
					ctx.getServletContext().setAttribute("changeHave", true);
				}
				if(module.getModuleName().equals("release")){
					ctx.getServletContext().setAttribute("releaseHave", true);
				}*/
				/*if(module.getModuleName().equals("cim")){
					ctx.getServletContext().setAttribute("cimHave", true);
					//同步配置项分类和资源
					ICICategoryService ciCategoryService=(ICICategoryService) ctx.getBean("cicategoryService");
					ciCategoryService.syncCICategory();
					//同步配置项分类编号
					ICICategoryDAO cicategoryDAO=(ICICategoryDAO) ctx.getBean("cicategoryDAO");
					CICategoryDAO.latesCICategoryNo=cicategoryDAO.getNextCICategoryNo();
					configureItemHave=true;
				}*/
			}

		}else{
			ModuleManageDTO moduleManage=new ModuleManageDTO();
			moduleManage.setModuleName("request");
			moduleManage.setTitle("服务台");
			moduleManage.setShowSort(1L);
			moduleManage.setDataFlag((byte) 1);
			moduleManage.setCreateTime(new Date());
			moduleManage.setPid("request_default-1");
			moduleManageService.saveModule(moduleManage);
			
			/*ModuleManage moduleManage4=new ModuleManage();
			moduleManage4.setModuleName("cim");
			moduleManage4.setShowSort(4L);
			moduleManage4.setTitle("配置项管理");
			moduleManageService.saveModuleManage(moduleManage4);*/
			ctx.getServletContext().setAttribute("requestHave", true);
			/*ctx.getServletContext().setAttribute("problemHave", true);
			ctx.getServletContext().setAttribute("changeHave", true);
			ctx.getServletContext().setAttribute("releaseHave", true);*/
			//ctx.getServletContext().setAttribute("cimHave", true);
		}
		
		
		//同步管理员的知识库权限
		eventCategoryService.syncKnowledgeCategorys();
		
		Calendar cl = Calendar.getInstance(); 
		ctx.getServletContext().setAttribute("YEAR_NOW",cl.get(Calendar.YEAR));
		
		System.out.println("-----------------------------------------------");
/*		System.out.println("-- Organization:"+OrganizationDAO.latesOrganizationNo);
		System.out.println("-- Resource:"+ResourceDAO.latesResNo);
		System.out.println("-- SLA:"+SLAContractDAO.latesSLANo);
		if(configureItemHave){
			System.out.println("-- CICategory:"+CICategoryDAO.latesCICategoryNo);
		}*/
		//System.out.println("-- FullTextSearch Service: " + (solrStauts? "started": "Stoped" ) );
		System.out.println("---------- WSTUO System startup success ----------");
		System.out.println("-----------------------------------------------");
		
	}
}
