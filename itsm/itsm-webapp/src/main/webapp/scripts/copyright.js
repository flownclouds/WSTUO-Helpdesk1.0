$package('copyright')

/**
 * @author wing  
 * @constructor 版权管理
 * @description 版权管理
 * @date 2014-12-19
 * @since version 1.0 
 */

copyright=function(){
	return {
		/**
		 * 密码
		 */
		rereadTxt:function(){
			var url = "copyrightmd5!copyright.action";
			var param = 'copyrightCode='+$('#copyrightCode').val();
			$.post(url,param,function(result){
				if(result=='failure'){
					alert("验证码错误!");
				}else{
					window.location.href='index.jsp';
				}
			});
			
		},
		init:function(){
			$('#copyrightCode_submit').click(copyright.rereadTxt);
			document.onkeydown = function(evt){
				var evt = window.event?window.event:evt;
				if(evt.keyCode==13){
					copyright.rereadTxt();
					return false;
				}
			}
		}
	};
}();
$(function(){copyright.init()});
