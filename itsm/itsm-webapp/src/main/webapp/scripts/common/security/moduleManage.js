
$package('common.security');

/**  
 * @author Wstuo 
 * @constructor Wstuo
 * @description 技术组管理主函数.
 * @date 2013-05-08
 * @since version 1.0 
 */
common.security.moduleManage = function() {


	return {
		/**
		 * @description 模板管理格式化.
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 */
		moduleManageGridFormatter:function(cell,opt,data){
			return '<div style="padding:0px">'+
			'&nbsp;&nbsp;&nbsp;<a href="javascript:common.security.moduleManage.unInstall(\''+(data.moduleId)+'\')" class="easyui-linkbutton" plain="true" icon="icon-delete" title='+i18n.deletes+'>'+
			'<img src="../images/icons/delete.gif" border="0"/></a>'+
			'</div>';
		},
		/**
		 * @description 加载格式化.
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 */
		isHaveDataRes:function(cell,opt,data){
			if(data.isHaveData){
				return "未加载";
			}else{
				return "已加载";
			}
		},
		/**
		 * @description 安装标识
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 */
		installFlagRes:function(cell,opt,data){
			if(data.installFlag=="true"){
				return "模块";
			}else{
				return "定制包";
			}
		},
		/**
		 * @description 加载模块列表.
		 */
		showModuleManageGrid:function(){
			var params = $.extend({},jqGridParams, {
				url:'module!findPager.action',
				colNames:['ID','安装包名称','描述','','基础数据',i18n.moduleInstallVersion,'支持的版本','标识',i18n.moduleInstallDate,i18n.sort,i18n.common_action],
				colModel:[
				          {name:'moduleId',width:30,align:'center'},
				          {name:'title',width:100,align:'center'},
				          {name:'description',width:180,align:'center'},
				          {name:'moduleName',align:'center',hidden:true},
				          {name:'isHaveData',width:50,align:'center',formatter:common.security.moduleManage.isHaveDataRes},
				          {name:'installVersion',width:80,align:'center'},
				          {name:'supportITSMVersion',width:80,align:'center'},
				          {name:'installFlag',width:80,align:'center',formatter:common.security.moduleManage.installFlagRes},
				          {name:'installDate',width:120,align:'center'},
				          {name:'showSort',width:30,align:'center'},
				          {name:'act', width:80,align:'center',sortable:false,formatter:common.security.moduleManage.moduleManageGridFormatter}
						  ],
				jsonReader: $.extend(jqGridJsonReader, {id:"moduleId"}),
				sortname:'moduleId',
				pager:'#moduleManageGridPager'
				});
				$("#moduleManageGrid").jqGrid(params);
				$("#moduleManageGrid").navGrid('#moduleManageGridPager',navGridParams);
				//列表操作项
				$("#t_moduleManageGrid").css(jqGridTopStyles);
				$("#t_moduleManageGrid").append($('#moduleManageGridToolbar').html());
				
				//自适应宽度
				setGridWidth("#moduleManageGrid","regCenter",20);
				
		},
		/**
		 * 打开安装窗口
		 */
		install_openWindow:function(){
			$('#moduleInstall_success_attachment').html('');
			windows('installModuleDiv',{width: 400});
		},
		/**
		 * 搜索窗口
		 */
		search_openWindow:function(){
			windows('searchModuleDiv',{width:350,modal: false});
		},
		/**
		 * 分页查询模块
		 */
		doSearch:function(){
			var _url = 'module!findPager.action';
			var sdata = $('#searchModuleDiv form').getForm();
			var postData = $("#moduleManageGrid").jqGrid("getGridParam", "postData");     
			$.extend(postData, sdata);
			$('#moduleManageGrid').jqGrid('setGridParam',{page:1,url:_url}).trigger('reloadGrid');
		},
		/**
		 * 卸载
		 */
		unInstall:function(id){
			msgConfirm(i18n['msg_msg'],'<br/>'+i18n.moduleUnInstallTip,function(){
				common.security.moduleManage.doUnInstall(id);
			});
		},
		unInstall_aff:function(){
			checkBeforeEditGrid("#moduleManageGrid",function(rowData){
				msgConfirm(i18n['msg_msg'],'<br/>'+i18n.moduleUnInstallTip,function(){
					common.security.moduleManage.doUnInstall(rowData.moduleId);
				});
			});
		},
		/**
		 * 卸载模块
		 * @param id 编号
		 */
		doUnInstall:function(id){
			var moduleId = id;
			startProcess();
			$.post("module!unInstallModule.action",{"dto.moduleId":moduleId},function(){
				endProcess();
				msgAlert(i18n.moduleUnInstallSussess,"info");
				$('#moduleManageGrid').trigger('reloadGrid');
			})
		},
		/**
		 * 修改显示顺序
		 */
		editShowSort:function(){
			checkBeforeEditGrid("#moduleManageGrid",function(rowData){
				$("#editModuleId").val(rowData.moduleId);
				$("#editShowSort").val(rowData.showSort);
				windows('editModuleDiv',{width: 400});
			});
		},
		/**
		 * 修改模块显示顺序
		 */
		editModuleSort:function(){
			var frm = $('#editModuleDiv form').serialize();
			$.post("module!editModuleSort.action",frm,function(){
				$('#editModuleDiv').dialog('close');
				$('#moduleManageGrid').trigger('reloadGrid');
				msgAlert(i18n.moduleEditSortTip,"info");
			});
		},
		/**
		 * 初始化加载
		 */
		init:function(){
			//绑定日期控件
			DatePicker97(['#startTime','#endTime']);
			//隐显加载图标
			showAndHidePanel('#moduleManage_content','#moduleManage_loading');
			//加载列表
			common.security.moduleManage.showModuleManageGrid();
			setTimeout(function(){
				getUploaderHar('#moduleInstall_file','#moduleInstall_success_attachment','moduleInstallQueId');
			},0)
			$("#searchModuleBtn_OK").click(function(){
				common.security.moduleManage.doSearch();
			});
			$("#editModuleBtn_OK").click(function(){
				common.security.moduleManage.editModuleSort();
			});
		}
		
		
	};
}();


//载入
$(document).ready(common.security.moduleManage.init);