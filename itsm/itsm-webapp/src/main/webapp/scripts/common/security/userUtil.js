$package('common.security');

$import('common.security.organizationTreeUtil');
$import('itsm.app.autocomplete.autocomplete');

/**  
 * @author Van  
 * @constructor WSTO
 * @description 用户选择公共函数.
 * @date 2011-02-25
 * @since version 1.0 
 */
common.security.userUtil=function(){

	this.showFlag='loginName';
	this.userNamePut='';
	this.userIdPut='';
	this.userCostPut='';
	this.loadUserSelectFlag='no';
	this.multiTag=true;
	this._companyNo=0;
	this._isHolidayTip = false;
	this._callback="";
	this._startTime="";
	this._endTime="";
	this._orgNo="";
	this._belongsGroup="";
	return {

		/**
		 * 用户选择，多选.
		 * namePut:用户名显示ID
		 * idPut：隐藏值ID
		 * showType：显示类型（LoginName Or FullName）
		 * p_companyNo：公司编号
		 */
		selectUserMulti:function(namePut,idPut,showType,p_companyNo){
			_callback="";
			multiTag=true;
			_isHolidayTip = false;
			_companyNo=p_companyNo;
			$('#selectUser_south_multi').show();//显示按钮
			$('#selectUser_south_single').hide();//隐藏提示
			common.security.userUtil.selectUserComm(namePut,idPut,'',showType,'');
			
		},
	
		/**
		 * 用户选择，多选.
		 * namePut:用户名显示ID
		 * idPut：隐藏值ID
		 * showType：显示类型（LoginName Or FullName）
		 * p_companyNo：公司编号
		 */
		selectUserMultiRule:function(namePut,idPut,showType,p_companyNo){
			_callback="";
			multiTag=true;
			_isHolidayTip = false;
			_companyNo=p_companyNo;
			$('#selectUser_south_multi').show();//显示按钮
			$('#selectUser_south_single').hide();//隐藏提示
			common.security.userUtil.selectUserComm(namePut,idPut,'',showType,'');
			$('#index_related_user_grid_select').click(common.security.userUtil.confirmMultiSelectRule);
		},
		
		/**
         * 用户选择，單選.
         * namePut:用户名显示ID
         * idPut：隐藏值ID
         * showType：显示类型（LoginName Or FullName）
         * p_companyNo：公司编号
         */
        selectUserSingleRule:function(namePut,idPut,showType,p_companyNo){
            _callback="";
            multiTag=false;
            _isHolidayTip = false;
            _companyNo=p_companyNo;
            $('#selectUser_south_multi').show();//显示按钮
            $('#selectUser_south_single').hide();//隐藏提示
            common.security.userUtil.selectUserComm(namePut,idPut,'',showType,'');
            $('#index_related_user_grid_select').click(common.security.userUtil.confirmMultiSelectRule);
        },
		
		/**
		 * 用户选择，单选.
		 * namePut:用户名显示ID
		 * idPut：隐藏值ID
		 * tcCostPut：技术员成本ID
		 * showType：显示类型（LoginName Or FullName）
		 * p_companyNo：公司编号
		 * callBack：回调函数
		 */
		selectUser:function(namePut,idPut,tcCostPut,showType,p_companyNo,callBack){
			multiTag=false;
			_isHolidayTip = false;
			_companyNo=p_companyNo;
			_callback = callBack;
			$('#selectUser_south_multi').hide();//隐藏按钮
			$('#selectUser_south_single').show();//显示提示
			common.security.userUtil.selectUserComm(namePut,idPut,tcCostPut,showType,'',callBack);
		
		},
		
		/**
		 * 选择用户时休假提醒
		 * namePut:用户名显示ID
		 * idPut：隐藏值ID
		 * tcCostPut：技术员成本ID
		 * showType：显示类型（LoginName Or FullName）
		 * p_companyNo：公司编号
		 * callBack：回调函数
		 */
		selectUserHolidayTip:function(namePut,idPut,tcCostPut,showType,p_companyNo,orgNo,callBack){
			common.security.userUtil.selectUserHolidayTipInit(namePut,idPut,tcCostPut,showType,p_companyNo,orgNo,callBack);
			$("#selectUserByGroup_attendance").hide();
			common.security.userUtil.selectUserComm(namePut,idPut,tcCostPut,showType,orgNo,callBack);
		},
		
		
		/**
		 * 选择用户,公共.
		 * p_namePut:用户名显示ID
		 * p_idPut：隐藏值ID
		 * p_tcCostPut：技术员成本ID
		 * p_showType：显示类型（LoginName Or FullName）
		 * callBack：回调函数
		 */
		selectUserComm:function(p_namePut,p_idPut,p_tcCostPut,p_showType,orgNo,callBack){
			common.security.userUtil.selectUserCommBefore(p_namePut,p_idPut,p_tcCostPut,p_showType,orgNo,callBack);
			common.security.userUtil.showSelectUserGrid(orgNo,callBack);
			common.security.userUtil.selectUserCommAfter(p_namePut,p_idPut,p_tcCostPut,p_showType,orgNo,callBack);
			
			itsm.app.autocomplete.autocomplete.bindAutoComplete('#selectUser_userGrid_loginName','com.wstuo.common.security.entity.User','fullName','fullName','userId','Long','','','false');
		},
		/**
		 * 加载树结构
		 */
		loadTree:function(){
			common.security.organizationTreeUtil.showAll('#selectUser_organizationTree',function(e,data){
				var orgType = data.rslt.obj.attr("orgType");
				if(orgType=='servicePanel'){
					$('#searchSelectUser_orgType').val('allService');
				}else if(orgType=='innerPanel'){
					$('#searchSelectUser_orgType').val('allInner');
				}else{
					$('#searchSelectUser_orgType').val('');
				}
				$('#searchSelectUser_loginName').val("");
				$('#searchSelectUser_orgNo').val("0");
				//ROOT
				if(data.rslt.obj.attr("orgNo")!=null){
					$('#searchSelectUser_orgNo').val(data.rslt.obj.attr("orgNo"));
				}
				var sdata=$('#searchUserOnSelect form').getForm();
				var postData = $("#selectUser_userGrid").jqGrid("getGridParam", "postData");
				$.extend(postData,sdata);
				$('#selectUser_userGrid').trigger('reloadGrid',[{"page":"1"}]);
			},_companyNo);
		},
		
		/**
		 * @description 系统数据类型格式化
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 * @private
		 */
		userGridFormatter:function(cell,event,data){
			return '<div style="padding:0px">'+
			'<a href="javascript:common.security.userUtil.confirmCheck(\''
			+(data.fullName)+'\',\''+(data.loginName)+'\',\''+data.userId+'\',\''+data.userCost
			+'\',\''+data.email+'\',\''+data.holidayStatus+'\')" title="'+i18n['check']+'">'+
			'<img src="../images/icons/ok.png"/></a>'+
			'</div>';
		},
//		/**
//		 * 姓名格式化.
//		 */
//		fullNameFormatter:function(cell,event,data){
//			var firstName='';
//			var lastName='';
//			if(data.firstName!=null && data.firstName!='null')
//				firstName=data.firstName;
//			if(data.lastName!=null && data.lastName!='null')
//				lastName=data.lastName;
//				
//			return lastName+firstName;
//		},
		/**
		 * 确认选定.
		 * @param fullName  全名
		 * @param loginName 登录名
		 * @param userId 用户编号
		 * @param userCost 成本
		 * @param email 邮件
		 * @param holidayStatus 假期状态
		 */
		confirmCheck:function(fullName,loginName,userId,userCost,email,holidayStatus){
			var callBack = _callback;
			if(_isHolidayTip && (holidayStatus==i18n['label_user_holiday_ing'] || holidayStatus == 'true')){
				msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_holiday_ing'],function(){
					common.security.userUtil.confirmChecked(fullName,loginName,userId,userCost,email,holidayStatus,callBack);
				});
			}else{
				common.security.userUtil.confirmChecked(fullName,loginName,userId,userCost,email,holidayStatus,callBack);
			}
		},
		
		/**
		 * 确认选定.
		 * @param fullName  全名
		 * @param loginName 登录名
		 * @param userId 用户编号
		 * @param userCost 成本
		 * @param email 邮件
		 * @param holidayStatus 假期状态
		 * @param callBack 回调函数
		 */
		confirmChecked:function(fullName,loginName,userId,userCost,email,holidayStatus,callBack){
			if(multiTag){
				if(showFlag=="fullName"){
					if($(userNamePut).val()==''){
						$(userNamePut).val(fullName);
					}else{
						//add mars 
						if($(userNamePut).val().indexOf(fullName)==-1){
							if($(userNamePut).val().substring($(userNamePut).val().length-1,$(userNamePut).val().length)==';'){
								$(userNamePut).val($(userNamePut).val()+fullName);
							}else{
								$(userNamePut).val($(userNamePut).val()+";"+fullName);
							}
						}
					}
				}else if(showFlag=="email"){
					if(email!=null && email!='' && email!='null'){
						if($(userNamePut).val()=='')
							$(userNamePut).val(email);
						else{
							//add tan
							if($(userNamePut).val().indexOf(email)==-1){
								if($(userNamePut).val().substring($(userNamePut).val().length-1,$(userNamePut).val().length)==';'){
									$(userNamePut).val($(userNamePut).val()+email);
								}else{
									$(userNamePut).val($(userNamePut).val()+";"+email);
								}
							}
						}	
					}else{
						msgAlert(i18n['msg_select_email_is_null'],'info');
					}
				}else if(showFlag=="ci_fullName"){
					$(userNamePut).val(fullName);
				}else if(showFlag=="filter_fullName"){
					$(userNamePut).val(fullName);
				}else if(showFlag!="Tesk" && showFlag!="EditTesk"&& showFlag!="candidate"){
					if($(userNamePut).val()=='')
						$(userNamePut).val(loginName);
					else{
						//add mars
						if($(userNamePut).val().indexOf(loginName)==-1){
							var unamePut=$(userNamePut).val();
							if(unamePut.substring(unamePut.length-1,unamePut.length)==';'){
								$(userNamePut).val(unamePut+loginName+";");
							}else{
								$(userNamePut).val($(userNamePut).val()+";"+loginName+";");
							}
						}else{
							userId="";
						}
					}
				}
			}else{
				if(showFlag=="fullName"){
					$(userNamePut).val(fullName);
				}else if(showFlag=="email"){
					//add tan
					if(email!='' && email!=null && email!='null' ){
						$(userNamePut).val(email);
					}else{
						msgAlert(i18n['msg_select_email_is_null'],'info');
					}
				}else if(showFlag=="ci_fullName"){
					$(userNamePut).val(fullName);
				}else if(showFlag=="filter_fullName"){
					$(userNamePut).val(fullName);
				}else{
					$(userNamePut).val(loginName);
				}
				if(userCostPut!=''){
					$(userCostPut).val(userCost);
				}
			}
			if(showFlag=="ci_fullName"){
				$(userIdPut).val(fullName);
			}else if(showFlag=="filter_fullName"){
				$(userIdPut).val(loginName);
			}else if(showFlag=="Tesk"){
				if($(userNamePut).val().indexOf(fullName)==-1){
					$(userNamePut).val($(userNamePut).val()+fullName+";");
				}
				if($(userIdPut).val().indexOf(loginName)==-1){
					$(userIdPut).val($(userIdPut).val()+loginName+";");
				}
			}else if(showFlag=="candidate"){
				if($(userNamePut).val().indexOf(fullName)==-1){
					$(userNamePut).val($(userNamePut).val()+fullName+";");
					$(userIdPut).val($(userIdPut).val()+userId+";");
				}
			}else if(showFlag=="EditTesk"){
				$(userNamePut).val(fullName);
				$(userIdPut).val(loginName);
			}else{
				$(userIdPut).val(userId);
			}
			$(userNamePut).focus();
			$('#selectUser_window').dialog('close');
			if(callBack!=null && callBack!=''){
				callBack();
			}
		},
		/**
		 * 加载用户列表.
		 * @param orgNo 机构编号
		 * @param callBack  回调函数
		 */
		showSelectUserGrid:function(orgNo,callBack){
			var user_url ='user!find.action';
			common.security.userUtil.showSelectUserGridInit(user_url,orgNo,callBack);
		},
		
		/**
		 * 搜索用户.
		 */
		searchUser:function(){
			var sdata=$('#searchUserOnSelect form').getForm();
			var postData = $("#selectUser_userGrid").jqGrid("getGridParam", "postData");
			$.extend(postData,sdata);  //将postData中的查询参数覆盖为空值
			$('#selectUser_userGrid').trigger('reloadGrid',[{"page":"1"}]);
			$('#searchUserOnSelect').dialog('close');
		},
		/**
		 * 搜索用户.公用
		 * @param gridId 编号
		 */
		searchUserCommon:function(gridId){
			var sdata=$('#searchUserOnSelect form').getForm();
			var postData = $('#'+gridId).jqGrid("getGridParam", "postData");
			$.extend(postData,sdata);  //将postData中的查询参数覆盖为空值
			$('#'+gridId).trigger('reloadGrid',[{"page":"1"}]);
			$('#searchSelectUser_loginName').val('');
			resetForm('#searchUserOnSelectForm');
			$('#searchUserOnSelect').dialog('close');
		},
		/**
		 * 多选，确认选择.
		 * @param callBack  回调函数
		 */
		confirmMultiSelect:function(callBack){
			var userNames="";
			var userIds="";
			var _email="";
			var rowIds = $("#selectUser_userGrid").getGridParam('selarrrow');
			if(rowIds==''){
				msgAlert(i18n['msg_atLeastChooseOneData'],'info');
				return false;
			}
			$.each(rowIds, function(k, v){
				var rowData=$("#selectUser_userGrid").getRowData(v); 
				//add mars
				if(showFlag=="fullName"){
					if($(userNamePut).val().indexOf(rowData.fullName)==-1){
						if(rowData.fullName!=null && rowData.fullName!='')
							userNames+=rowData.fullName+";"
						if(rowData.userId!=null && rowData.userId!='')
							userIds+=rowData.userId+";";
					}
				}else if(showFlag=="email"){
					//add tan
					if($(userNamePut).val().indexOf(rowData.email)==-1){
						if(rowData.email!=null && rowData.email!='')
							userNames+=rowData.email+";"
						if(rowData.userId!=null && rowData.userId!='')
							userIds+=rowData.userId+";";

						_email=_email+rowData.email;
					}
					
				}else if(showFlag=="Tesk"){
					if($(userNamePut).val().indexOf(rowData.fullName)==-1){
						if(rowData.fullName!=null && rowData.fullName!='')
							userNames+=rowData.fullName+";"
						if(rowData.loginName!=null && rowData.loginName!='')	
							userIds+=rowData.loginName+";";
					}
				}else if(showFlag=="candidate"){
					if($(userNamePut).val().indexOf(rowData.fullName)==-1){
						if(rowData.fullName!=null && rowData.fullName!='')
							userNames+=rowData.fullName+";"
						if(rowData.userId!=null && rowData.userId!='')	
							userIds+=rowData.userId+";";
					}
				}else{
					if($(userNamePut).val().indexOf(rowData.loginName)==-1){
						if(rowData.loginName!=null && rowData.loginName!='')
							userNames+=rowData.loginName+";"
						if(rowData.userId!=null && rowData.userId!='')	
							userIds+=rowData.userId+";";
					}
				}
				
			});
			var IdsPut='';
			if(showFlag=="email" && _email==""){
				msgAlert(i18n['msg_select_email_is_null'],'info');
			}
			userNames=userNames.substring(0,userNames.length-1);
			if($(userNamePut).val()=='')
				$(userNamePut).val(userNames);
			else{
				var unamePut=trim($(userNamePut).val());
				if(unamePut.substring(unamePut.length-1,unamePut.length)==';'){
					$(userNamePut).val(unamePut+userNames+";");
				}else{
					$(userNamePut).val(unamePut+";"+userNames+";");
				}
			}
			userIds=userIds.substring(0,userIds.length-1);
			if($(userIdPut).val()==''){
				$(userIdPut).val(userIds);
			}else{
				if($(userIdPut).val()){
					 IdsPut=trim($(userIdPut).val());
				}
				if(IdsPut.substring(IdsPut.length-1,IdsPut.length)==';'&&IdsPut!=undefined){
					$(userIdPut).val(IdsPut+userIds+";");
				}else{
					$(userIdPut).val(IdsPut+";"+userIds+";");
				}
			}
			$(userNamePut).focus()
			$(userIdPut).focus()
			$('#selectUser_window').dialog('close');
			if(callBack!=null && callBack!=''){
				callBack();
			}
		},
		/**
		 * 多选，确认选择.
		 * @param callBack  回调函数
		 */
		confirmMultiSelectRule:function(callBack){
			var userNames="";
			var userIds="";
			var _email="";
			var rowIds = $("#selectUser_userGrid").getGridParam('selarrrow');
			$.each(rowIds, function(k, v){
				var rowData=$("#selectUser_userGrid").getRowData(v); 
				//add mars
				if(showFlag=="fullName"){
					if($(userNamePut).val().indexOf(rowData.fullName)==-1){
						if(rowData.fullName!=null && rowData.fullName!='')
							userNames+=rowData.fullName+","
						if(rowData.userId!=null && rowData.userId!='')
							userIds+=rowData.userId+",";
					}
				}else if(showFlag=="email"){
					//add tan
					if($(userNamePut).val().indexOf(rowData.email)==-1){
						if(rowData.email!=null && rowData.email!='')
							userNames+=rowData.email+","
						if(rowData.userId!=null && rowData.userId!='')
							userIds+=rowData.userId+",";
						_email=_email+rowData.email;
					}
				}else{
					if($(userNamePut).val().indexOf(rowData.loginName)==-1){
						if(rowData.loginName!=null && rowData.loginName!='')
							userNames+=rowData.loginName+","
						if(rowData.userId!=null && rowData.userId!='')	
							userIds+=rowData.userId+",";
					}
				}
			});
			if(showFlag=="email" && _email==""){
				msgAlert(i18n['msg_select_email_is_null'],'info');
			}
			userNames=userNames.substring(0,userNames.length-1);
			if($(userNamePut).val()=='')
				$(userNamePut).val(userNames);
			else{
				if($(userNamePut).val().substring($(userNamePut).val().length-1,$(userNamePut).val().length)==','){
					$(userNamePut).val($(userNamePut).val()+userNames);
				}else{
					$(userNamePut).val($(userNamePut).val()+","+userNames);
				}
			}
			userIds=userIds.substring(0,userIds.length-1);
			$(userIdPut).val(userIds);
			$(userNamePut).focus()
			$('#selectUser_window').dialog('close');
			if(callBack!=null && callBack!=''){
				callBack();
			}
		},
		/**
		 * @description 清空选择
		 * @param  sname1 字段值
		 */
		cleanSelect:function(sname1){
			$('#'+sname1).val('');
		},
		
		/**
		 * 加载用户角色
		 */
		loadSelectRoles:function(){		
			$("#selectUser_userGrid_Roles").empty(); 
			$("<option value='0'> -- "+i18n['label_user_selectRole']+" -- </option>").appendTo("#selectUser_userGrid_Roles");
			$.post("role!find.action?rows=500", function(data){
				if(data!=null && data.data!=null && data.data.length!=0){
					var optionsHTML="";
					for(var i=0;i<data.data.length;i++){
						//外包客户
						if(topCompanyNo!=companyNo){
							if(data.data[i].roleCode=="ROLE_ENDUSER" || data.data[i].roleCode=="ROLE_ITSOP_MANAGEMENT"){
								optionsHTML+="<option value='{0}'>{1}</option>".replace("{0}",data.data[i].roleId).replace("{1}",data.data[i].roleName);
							}							
						}else{
							optionsHTML+="<option value='{0}'>{1}</option>".replace("{0}",data.data[i].roleId).replace("{1}",data.data[i].roleName);
						}
					}
					$("#selectUser_userGrid_Roles").append(optionsHTML);
					$("#selectUser_userGrid_Roles").change(function(){
						$('#selectUser_userGrid_Roles').val(this.value);
						common.security.userUtil.searchuserRoleFmUser();
					});
					
				}
			}, "json");
		},
		
		/**
		 * 显示指定角色的用户列表
		 * @param roleCode 角色代码
		 * @param userName_id 用户名编号
		 * @param userId_id  用户编号
		 */
		selectUserByRole:function(roleCode,userName_id,userId_id){
			userNamePut=userName_id;
			userIdPut=userId_id;
			var _postData={};
			$.extend(_postData, {'userQueryDto.roleCode':roleCode});
			if(basics.ie6.htmlIsNull("#selectUserByRole_grid")){
				var user_url ='user!find.action';
				var postData = $("#selectUserByRole_grid").jqGrid("getGridParam", "postData");
				$.extend(postData,{'userQueryDto.companyNo':'-1','userQueryDto.roleCode':roleCode});
				$('#selectUserByRole_grid').jqGrid('setGridParam',{page:1,url:user_url}).trigger('reloadGrid');
			}else{
				var params = $.extend({},jqGridParams, {	
					url:'user!find.action',
					postData:_postData,
					colNames:[i18n['loginName'],i18n['title_user_firstName'],i18n['common_email'],i18n['check'],'','','',''],
					colModel:[{name:'loginName',width:140},
							  {name:'fullName',width:140,align:'center',sortable:false},
							  {name:'email',width:140,sortable:false,align:'center',editable:true},
							  {name:'act', width:80,sortable:false,align:'center',formatter:common.security.userUtil.userGridFormatter_byRole},
							  {name:'userCost',hidden:true},
							  {name:'userId',hidden:true},
							  {name:'firstName',hidden:true},
							  {name:'lastName',hidden:true}
							  ],
					jsonReader:$.extend(jqGridJsonReader,{id:"userId"}),
					sortname:'userId',
					pager:'#selectUserByRole_pager',
					toolbar:[false,"top"],
					multiselect:false,
					ondblClickRow:function(rowId){
						var data=$('#selectUserByRole_grid').getRowData(rowId);
						common.security.userUtil.confirmCheck_byRole(data.fullName,data.loginName,data.userId,data.userCost,data.email);
					}
				});
				
				$("#selectUserByRole_grid").jqGrid(params);
				$("#selectUserByRole_grid").navGrid('#selectUserByRole_pager', navGridParams);
				
				$("#selectUserByRole_grid").navButtonAdd('#selectUserByRole_pager',{caption:"",buttonicon:"ui-icon-search",onClickButton:function(){
					windows('searchUserOnSelect',{width:350});
				},position:"last"});
				$('#selectUser_common_dosearch').bind('click',function(){
					common.security.userUtil.searchUserCommon("selectUserByRole_grid");
				});//搜索用户
			}
			windows('selectUserByRole_window',{width:530,maxHeight:480});
			
		},
		/**
		 * 显示指定技术组的用户列表
		 * @param belongsGroup 所属组
		 * @param userName_id 用户名编号
		 * @param userId_id 用户编号
		 */
		selectUserByGroup:function(belongsGroup,userName_id,userId_id){
			var bool = $('#selectUser_opteron_attendance').is(":checked");
			//切换显示列表过滤
			$('input[name="selectUserByGroupR"]').change(common.security.userUtil.attendanceUsers);
			userNamePut=userName_id;
			userIdPut=userId_id;
			var param = null;
			if(belongsGroup!=''){
				param = $.param({'userQueryDto.belongsGroupIds':belongsGroup},true);
			}
			var _postData={};
			$.extend(_postData, {'state':'true'});
			var user_url ='user!find.action?'+param;
			common.security.userUtil.selectUserByGroupInit(user_url,_postData,belongsGroup,userName_id,userId_id);
			windowsAuto('selectUserByGroup_window',{width:530,maxHeight:480},'#selectUserByGroup_grid',0);
		},
		
		/**
		 * 确认角色.
		 * @param fullName  全名
		 * @param loginName 登录名
		 * @param userId 用户编号
		 * @param userCost 成本
		 * @param email 邮件
		 */
		confirmCheck_byRole:function(fullName,loginName,userId,userCost,email){
			$(userIdPut).val(userId);
			$(userNamePut).val(fullName).focus();
			$('#selectUserByRole_window').dialog('close');//关闭窗口
		},
		/**
		 * 确认角色.
		 * @param fullName  全名
		 * @param loginName 登录名
		 * @param userId 用户编号
		 * @param userCost 成本
		 * @param email 邮件
		 */
		confirmCheck_byGroup:function(fullName,loginName,userId,userCost,email){
			$(userIdPut).val(userId);
			$(userNamePut).val(fullName).focus();
			$('#selectUserByGroup_window').dialog('close');//关闭窗口
		},
		/**
		 * @description 动作格式化.
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 * @private
		 */
		userGridFormatter_byRole:function(cell,event,data){
			return '<div style="padding:0px">'+
			'<a href="javascript:common.security.userUtil.confirmCheck_byRole(\''
			+(data.fullName)+'\',\''+(data.loginName)+'\',\''+data.userId+'\',\''+data.userCost
			+'\',\''+data.email+'\')" title="'+i18n['check']+'">'+
			'<img src="../images/icons/ok.png"/></a>'+
			'</div>';
		},
		/**
		 * @description 动作格式化.
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 * @private
		 */
		userGridFormatter_byGroup:function(cell,event,data){
			return '<div style="padding:0px">'+
			'<a href="javascript:common.security.userUtil.confirmCheck_byGroup(\''
			+(data.fullName)+'\',\''+(data.loginName)+'\',\''+data.userId+'\',\''+data.userCost
			+'\',\''+data.email+'\')" title="'+i18n['check']+'">'+
			'<img src="../images/icons/ok.png"/></a>'+
			'</div>';
		},
		/**
		 * @description 用户假期动作格式化.
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 */
		userGridHolidayStatusFormatter:function(cell,event,data){
			var holidayStatus="";
			if(data.holidayStatus){
				holidayStatus = i18n.label_user_holiday_ing;
			}else{
				holidayStatus = i18n.label_user_duty_ing;
			}
			return holidayStatus;
		},

		/**
		 * @description 指派选择用户组
		 * @param name 当前列值
		 * @param no 事件
		 * @param assigneeName 行数据
		 * @param assigneeGroup 指派组
		 */
		againAssignSelectUser_group:function(name,no,assigneeName,assigneeGroup){
			$("#selectUserByGroup_opteron").hide();
			var belongsGroup = '';
			if(assigneeGroup!='' && assigneeGroup!=null ){
				belongsGroup = $(assigneeGroup).val();
			}
			var loginName ='';
			if(assigneeName!=''){
				loginName = $(assigneeName).val();
				if(loginName==undefined){
					loginName = '';
				}
			}
			if(loginName == "" && belongsGroup != ""){
				common.security.userUtil.selectUserByGroup(belongsGroup,name,no);
			}else{
				var url='user!findByUserName.action';
				var param='userName='+loginName;
				$.post(url,param,function(res){
					var belongsGroupIds='';
					if(res!=null){
						belongsGroupIds =  res.belongsGroupIds;
						belongsGroup = res.belongsGroup.substring(0,res.belongsGroup.length-1);
					}
					common.security.userUtil.selectUserByGroup(belongsGroupIds,name,no);
				});
			}
		},
		selectUserFun:function(callBack){
			if(basics.ie6.htmlIsNull("#selectUser_grid")){
				var user_url ='user!find.action';
				var postData = $("#selectUser_grid").jqGrid("getGridParam", "postData");
				$.extend(postData,{});
				$('#selectUser_grid').jqGrid('setGridParam',{page:1,url:user_url}).trigger('reloadGrid');
			}else{
				var params = $.extend({},jqGridParams, {
					url:'user!find.action',
					postData:{'userQueryDto.companyNo':companyNo},
					colNames:[i18n['loginName'],i18n['title_user_firstName'],i18n['common_email'],i18n['check'],'','','',''],
					colModel:[{name:'loginName',width:160},
							  {name:'fullName',width:160,align:'center',sortable:false},
							  {name:'email',width:180,sortable:false,align:'center',editable:true},
							  {name:'act', width:80,sortable:false,align:'center',formatter:common.security.userUtil.userGridFormatter_byRole,hidden:true},
							  {name:'userCost',hidden:true},
							  {name:'userId',hidden:true},
							  {name:'firstName',hidden:true},
							  {name:'lastName',hidden:true}
							  ],
					jsonReader:$.extend(jqGridJsonReader,{id:"userId"}),
					sortname:'userId',
					pager:'#selectUser_pager',
					toolbar:[true,"top"],
					multiselect:true,
					ondblClickRow:function(rowId){
						var data=$('#selectUser_grid').getRowData(rowId);
						//common.security.userUtil.confirmCheck_byRole(data.fullName,data.loginName,data.userId,data.userCost,data.email);
					}
				});
				
				$("#selectUser_grid").jqGrid(params);
				$("#selectUser_grid").navGrid('#selectUser_pager', navGridParams);
				//列表操作项
				$("#t_selectUser_grid").css(jqGridTopStyles);
				$("#t_selectUser_grid").append('<form id="selectUserSearchForm">'+i18n.loginName+':<input name="userQueryDto.loginName" id="selectRole_roleName" />'+
						i18n.name1+':<input name="userQueryDto.fullName" />'+
						'&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" id="selectUser_search"><b>['+i18n.search+']</b></a>'+
						'&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" id="selectUser_multipleSelectConfirm_link"><b>['+i18n.Confirm+']</b></a>'+
						'</form>');
				//搜索用户
				$('#selectUser_search').bind('click',function(){
					common.security.userUtil.roleSearch();
				});
				//绑定确认
				$('#selectUser_multipleSelectConfirm_link').unbind().click(function(){common.security.userUtil.multipleSelectConfirm(callBack)});
			}
			windows('selectUserGrid_window',{width:530,height:350});
		},
		//用户搜索
		roleSearch:function(){
			var sdata=$('#selectUserSearchForm').getForm();
			var postData = $("#selectUser_grid").jqGrid("getGridParam", "postData");       
			$.extend(postData, sdata);
			var _url = 'user!find.action';
			$('#selectUser_grid').jqGrid('setGridParam',{url:_url})
				.trigger('reloadGrid',[{"page":"1"}]).jqGrid('setGridParam',{url:_url});
		},
		//单选确定选择按钮
		radioSelectConfirm:function(callBack){
			
		},
		//多选确定选择按钮
		multipleSelectConfirm:function(callBack){
			var rowIds = $("#selectUser_grid").getGridParam('selarrrow');
			if(rowIds==''){
				msgAlert(i18n.msg_atLeastChooseOneData,'info');
			}else{
				callBack(rowIds);
				$('#selectUserGrid_window').dialog('close');
			}
		},
		//因为其他的模块没有排班，所以拷贝了原有方法来进行修改
		//============排班管理====start
		againAssignSelectUser_group1:function(isAttendance,start,end,name,no,assigneeName,assigneeGroup){
			_startTime = start;
			_endTime = end;
			if (isAttendance) {
				$("#selectUserByGroup_opteron").show();
				if (isAttendance === "true") {
					$("#selectUser_opteron_attendance").attr("checked","true");
				}else{
					$("#selectUser_opteron_all").attr("checked","true");
				}
			}else{
				$("#selectUserByGroup_opteron").hide();
			}
			/**/
			var belongsGroup = '';
			if(assigneeGroup!='' && assigneeGroup!=null ){
				belongsGroup = $(assigneeGroup).val();
				_belongsGroup = belongsGroup;
			}
			var loginName ='';
			if(assigneeName!=''){
				loginName = $(assigneeName).val();
				if(loginName==undefined){
					loginName = '';
				}
			}
			if(loginName == "" && belongsGroup != ""){
				common.security.userUtil.selectUserByGroup1(belongsGroup,name,no);
			}else{
				var url='user!findByUserName.action';
				var param='userName='+loginName;
				$.post(url,param,function(res){
					var belongsGroupIds='';
					if(res!=null){
						belongsGroupIds =  res.belongsGroupIds;
						belongsGroup = res.belongsGroup.substring(0,res.belongsGroup.length-1);
						_belongsGroup = belongsGroupIds;
					}
					common.security.userUtil.selectUserByGroup1(belongsGroupIds,name,no);
				});
			}
		},
		selectUserByGroup1:function(belongsGroup,userName_id,userId_id){
			var bool = $('#selectUser_opteron_attendance').is(":checked");
			//切换显示列表过滤
			$('input[name="selectUserByGroupR"]').change(function(){
				common.security.userUtil.attendanceUsers(belongsGroup);
			});
			userNamePut=userName_id;
			userIdPut=userId_id;
			var param = null;
			if(belongsGroup!=''){
				param = $.param({'userQueryDto.belongsGroupIds':belongsGroup},true);
			}
			var _postData={};
			$.extend(_postData, {'state':'true','taskDto.startTime':_startTime,'taskDto.endTime':_endTime});
			var user_url ='attendance!find.action?userQueryDto.attendance='+bool+'&'+param;
			common.security.userUtil.selectUserByGroupInit(user_url,_postData,belongsGroup,userName_id,userId_id);
			windowsAuto('selectUserByGroup_window',{width:530,maxHeight:480},'#selectUserByGroup_grid',0);
		},
		attendanceUsers:function(belongsGroup){
			var userGroup = belongsGroup;
			if (_belongsGroup != "") {userGroup = _belongsGroup;}
			var bool = $('#selectUser_opteron_attendance').is(":checked");
			var user_url ='attendance!find.action?userQueryDto.attendance='+bool;
			var postData = $("#selectUserByGroup_grid").jqGrid("getGridParam", "postData");
			$.extend(postData,{'userQueryDto.companyNo':companyNo,'taskDto.startTime':_startTime
				,'taskDto.endTime':_endTime,'userQueryDto.belongsGroupIds':userGroup});
			$('#selectUserByGroup_grid').jqGrid('setGridParam',{page:1,url:user_url}).trigger('reloadGrid');
		},
		/**
		 * 选择用户,公共.
		 * p_namePut:用户名显示ID
		 * p_idPut：隐藏值ID
		 * p_tcCostPut：技术员成本ID
		 * p_showType：显示类型（LoginName Or FullName）
		 * callBack：回调函数
		 */
		selectUserComm1:function(p_namePut,p_idPut,p_tcCostPut,p_showType,orgNo,callBack){
			common.security.userUtil.selectUserCommBefore(p_namePut,p_idPut,p_tcCostPut,p_showType,orgNo,callBack);
			
			common.security.userUtil.showSelectUserGrid1(orgNo,callBack);
			
			common.security.userUtil.selectUserCommAfter(p_namePut,p_idPut,p_tcCostPut,p_showType,orgNo,callBack);
			
			itsm.app.autocomplete.autocomplete.bindAutoComplete('#selectUser_userGrid_loginName','com.wstuo.common.security.entity.User','fullName','fullName','userId','Long','','','false');
		},
		/**
		 * 加载用户列表.
		 * @param orgNo 机构编号
		 * @param callBack  回调函数
		 */
		showSelectUserGrid1:function(orgNo,callBack){
			var bool = $('#selectUserSouth_attendance').is(":checked");
			var user_url ='attendance!find.action?userQueryDto.attendance='+bool;
			common.security.userUtil.showSelectUserGridInit(user_url,orgNo,callBack);
		},
		/**
		 * 选择用户时休假提醒
		 * namePut:用户名显示ID
		 * idPut：隐藏值ID
		 * tcCostPut：技术员成本ID
		 * showType：显示类型（LoginName Or FullName）
		 * p_companyNo：公司编号
		 * callBack：回调函数
		 */
		selectUserHolidayTip1:function(isAttendance,start,end,namePut,idPut,tcCostPut,showType,p_companyNo,orgNo,callBack){
			_startTime = start;
			_endTime = end;
			_orgNo = orgNo;
			$('input[name="selectUserByGroupSouth"]').change(common.security.userUtil.attendanceUsers2);
			if (isAttendance) {
				$("#selectUserByGroup_attendance").show();
				if (isAttendance === "true") {
					$("#selectUserSouth_attendance").attr("checked","true");
				}else{
					$("#selectUserSouth_all").attr("checked","true");
				}
			}else{
				$("#selectUserByGroup_attendance").hide();
			}
			common.security.userUtil.selectUserHolidayTipInit(namePut,idPut,tcCostPut,showType,p_companyNo,orgNo,callBack);
			common.security.userUtil.selectUserComm1(namePut,idPut,tcCostPut,showType,orgNo,callBack);
		},
		attendanceUsers2:function(){
			var bool = $('#selectUserSouth_attendance').is(":checked");
			var user_url ='attendance!find.action?userQueryDto.attendance='+bool;
			
			var postData = $("#selectUser_userGrid").jqGrid("getGridParam", "postData"); 
			$.extend(postData,{'userQueryDto.companyNo':_companyNo,'userQueryDto.orgNo':_orgNo,'userQueryDto.roleNo':'0','state':'true','taskDto.startTime':_startTime,'taskDto.endTime':_endTime});
			resetForm('#searchUserOnSelectForm');
			var sdata=$('#searchUserOnSelect form').getForm();
			var postData = $("#selectUser_userGrid").jqGrid("getGridParam", "postData");
			$('#selectUser_userGrid').jqGrid('setGridParam',{page:1,url:user_url}).trigger('reloadGrid');
		},
		//============排班管理====end
		//===========提取 ---start
		selectUserByGroupInit:function(url,_postData,belongsGroup,userName_id,userId_id){
			if(basics.ie6.htmlIsNull("#selectUserByGroup_grid")){

				var sdata=$('#searchUserOnSelect form').getForm();
				var user_url = url;//'attendance!find.action?userQueryDto.attendance='+bool+'&'+param;
				var postData = $("#selectUserByGroup_grid").jqGrid("getGridParam", "postData");
				$.extend(postData,sdata,{'userQueryDto.companyNo':companyNo});
				$('#selectUserByGroup_grid').jqGrid('setGridParam',{page:1,url:user_url}).trigger('reloadGrid');
			}else{
				var params = $.extend({},jqGridParams, {	
					url:url,//'attendance!find.action?userQueryDto.companyNo='+companyNo+'&userQueryDto.attendance='+bool+'&'+param,
					postData:_postData,
					colNames:[i18n.loginName,i18n.title_user_firstName,'',i18n.label_user_holidayStatus,i18n.title_user_org,i18n.check,'','','',''],
					colModel:[{name:'loginName',width:120,align:'center'},
							  {name:'fullName',width:120,align:'center',sortable:false},
							  {name:'email',width:140,sortable:false,align:'center',hidden:true},
							  {name:'holidayStatus',width:100,align:'center',formatter:common.security.userUtil.userGridHolidayStatusFormatter},
							  {name:'orgName',width:100,sortable:false,align:'center'},
							  {name:'act', width:60,sortable:false,align:'center',formatter:common.security.userUtil.userGridFormatter_byGroup},
							  {name:'userCost',hidden:true},
							  {name:'userId',hidden:true},
							  {name:'firstName',hidden:true},
							  {name:'lastName',hidden:true}
							  ],
					jsonReader:$.extend(jqGridJsonReader,{id:"userId"}),
					sortname:'userId',
					pager:'#selectUserByGroup_pager',
					toolbar:[false,"top"],
					multiselect:false,
					ondblClickRow:function(rowId){
						var data=$('#selectUserByGroup_grid').getRowData(rowId);
						common.security.userUtil.confirmCheck_byGroup(data.fullName,data.loginName,data.userId,data.userCost,data.email);
					}
				});
				$("#selectUserByGroup_grid").jqGrid(params);
				$("#selectUserByGroup_grid").navGrid('#selectUserByGroup_pager', navGridParams);
				
				$("#selectUserByGroup_grid").navButtonAdd('#selectUserByGroup_pager',{caption:"",buttonicon:"ui-icon-search",onClickButton:function(){
					windows('searchUserOnSelect',{width:350});
				},position:"last"});
				$('#selectUser_common_dosearch').bind('click',function(){
					common.security.userUtil.searchUserCommon("selectUserByGroup_grid");
				});//搜索用户
			}
		},
		showSelectUserGridInit:function(url,orgNo,callBack){
			$('#selectUser_userGrid_loginName').val("");
			$('#searchSelectUser_orgNo').val("0");
			$('#selectUser_userGrid_Roles').val("0");
			$('#searchSelectUser_orgType').val('');
			if(basics.ie6.htmlIsNull("#selectUser_userGrid")){
				var postData = $("#selectUser_userGrid").jqGrid("getGridParam", "postData"); 
				$.extend(postData,{'userQueryDto.companyNo':_companyNo,'userQueryDto.orgNo':orgNo,'userQueryDto.roleNo':'0','state':'true','taskDto.startTime':_startTime,'taskDto.endTime':_endTime});
				resetForm('#searchUserOnSelectForm');
				var sdata=$('#searchUserOnSelect form').getForm();
				var postData = $("#selectUser_userGrid").jqGrid("getGridParam", "postData");
				$.extend(postData,sdata);  //将postData中的查询参数覆盖为空值
				//var user_url ='attendance!find.action?userQueryDto.attendance='+bool;
				var user_url = url;
				$('#selectUser_userGrid').jqGrid('setGridParam',{page:1,url:user_url}).trigger('reloadGrid');
			}else{
				var params = $.extend({},jqGridParams, {	
					url : url,
					postData:{'userQueryDto.companyNo':_companyNo,'userQueryDto.orgNo':orgNo,'state':'true','taskDto.startTime':_startTime,'taskDto.endTime':_endTime},
					colNames:[i18n['loginName'],i18n['title_user_firstName'],i18n['common_email'],i18n['label_user_holidayStatus'],i18n['check'],'','','',''],
					colModel:[{name:'loginName',width:90},
							  {name:'fullName',width:130,align:'center',sortable:false},
							  {name:'email',width:140,sortable:false,align:'center',editable:true},
							  {name:'holidayStatus',align:'center',width:80,formatter:function(cell,opt,data){
				 	        	  if(cell=='true' || cell==true)
				 	        		 return i18n['label_user_holiday_ing'];
				 	        	  else
				 	        		 return i18n['label_user_duty_ing'];
				 	          }},
							  {name:'act', width:80,sortable:false,align:'center',formatter:common.security.userUtil.userGridFormatter},
							  {name:'userCost',hidden:true},
							  {name:'userId',hidden:true},
							  {name:'firstName',hidden:true},
							  {name:'lastName',hidden:true}
							  ],
					jsonReader:$.extend(jqGridJsonReader,{id:"userId"}),
					sortname:'userId',
					multiselect: multiTag,//复选框
					pager:'#selectUser_userGridPager',
					toolbar:[false,"top"],
					rowList:[5,10,15,20],
					rowNum:10,
					ondblClickRow:function(rowId){
						var data=$('#selectUser_userGrid').getRowData(rowId);
						common.security.userUtil.confirmCheck(data.fullName,data.loginName,data.userId,data.userCost,data.email,data.holidayStatus);
					}
				});
				$("#selectUser_userGrid").jqGrid(params);
				$("#selectUser_userGrid").navGrid('#selectUser_userGridPager', navGridParams);
				$("#selectUser_userGrid").navButtonAdd('#selectUser_userGridPager',{caption:"",buttonicon:"ui-icon-search",onClickButton:function(){
					windows('searchUserOnSelect',{width:350});
				},position:"last"}); 
				//$("#t_selectUser_userGrid").css(jqGridTopStyles);
				//$("#t_selectUser_userGrid").append($('#selectUser_userGridToolbar').html());
				$('#selectUser_common_dosearch').click(common.security.userUtil.searchUser);//搜索用户
				common.security.userUtil.loadSelectRoles();
				$('#selectUser_SearchSelect').click(function(){
					common.security.userUtil.searchuserRoleFmUser();
				});
			}
		},
		selectUserHolidayTipInit:function(namePut,idPut,tcCostPut,showType,p_companyNo,orgNo,callBack){
			multiTag=false;
			_isHolidayTip = true;
			_companyNo=p_companyNo;
			_callback = callBack;
			$('#selectUser_south_multi').hide();//隐藏按钮
			$('#selectUser_south_single').show();//显示提示
		},
		selectUserCommAfter:function(p_namePut,p_idPut,p_tcCostPut,p_showType,orgNo,callBack){
//			//多选，确认选择
//			$('#selectUser_ConfirmSelect').click(common.security.userUtil.confirmMultiSelect);
			common.security.userUtil.loadTree();//加载树
			$('#selectUser_window').dialog({width:800,height:470,resizeStop: function(event, ui) { 
				var widthCenter = $("#selectUser_window").width()- 200;
				$("#selectUser_userGrid").setGridWidth(widthCenter-20);
				var $suw_layout = $('#selectUser_window .easyui-layout');$suw_layout.css('width',widthCenter+200);
				$suw_layout.layout('panel', 'center').panel('resize',{width:widthCenter});
				$suw_layout.layout('panel', 'south').panel('resize',{width:widthCenter+200});
			}});
			var widthCenter = $("#selectUser_window").width()- 200;
			$("#selectUser_userGrid").setGridWidth(widthCenter-20);
			var $suw_layout = $('#selectUser_window .easyui-layout');$suw_layout.css('width',widthCenter+200);
			$suw_layout.layout('panel', 'center').panel('resize',{width:widthCenter});
			$suw_layout.layout('panel', 'south').panel('resize',{width:widthCenter+200});
//			$('#selectUser_window_tabs').tabs();//显示tab
			setTimeout(function(){//隐藏显示复选框
				if(multiTag){
					$('#index_related_user_grid_select').hide();
					$('#selectUser_userGrid').jqGrid('showCol', 'cb');
				}else{
					$('#index_related_user_grid_select').hide();
					$('#selectUser_userGrid').jqGrid('hideCol', 'cb');
				}
			},0);
			//禁止回车事件
			$("#selectUser_userGrid_loginName").keydown(function (e) {
	            var curKey = e.which;
	            if (curKey == 13) {
	            	common.security.userUtil.searchuserRoleFmUser();
	            }
	        });
		},
		/**
		 * 搜索用户.
		 */
		searchuserRoleFmUser:function(){
			var roleData = $('#userRoleFm').getForm();
			var postData = $("#selectUser_userGrid").jqGrid("getGridParam", "postData");
			$.extend(postData,roleData);  //将postData中的查询参数覆盖为空值
			$('#selectUser_userGrid').trigger('reloadGrid',[{"page":"1"}]);
		},
		/**
		 * 选择用户,公共.
		 * p_namePut:用户名显示ID
		 * p_idPut：隐藏值ID
		 * p_tcCostPut：技术员成本ID
		 * p_showType：显示类型（LoginName Or FullName）
		 * callBack：回调函数
		 */
		selectUserCommBefore:function(p_namePut,p_idPut,p_tcCostPut,p_showType,orgNo,callBack){
			userNamePut=p_namePut;
			userIdPut=p_idPut;
			userCostPut=p_tcCostPut;
			showFlag=p_showType;
			//tan
			if(loadUserSelectFlag=="no"){
//				common.security.userUtil.showSelectUserGrid();
				loadUserSelectFlag="yes";
				//多选，确认选择
				$('#selectUser_ConfirmSelect').click(function(){common.security.userUtil.confirmMultiSelect(callBack);});
			}
		}
		//===========提取 ---end
	}
}();
