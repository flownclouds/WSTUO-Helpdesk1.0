$package('common.security');

/**  
 * @author Van  
 * @constructor WSTO
 * @description 服务时间管理主函数.
 * @date 2011-02-25
 * @since version 1.0 
 */
common.security.organizationServiceTime = function() {

	
	return {
		
		/**
		 * @description 根据机构编号查找服务时间.
		 */
		findServiceByOrganizationNo:function(orgNo){
			
			
			var url="serviceTime!findByOrgNo.action?serviceTimeDTO.orgNo="+orgNo;
			
			$.post(url,function(res){
					
					if(res.sid!=null){
						$('#serviceTimeDTO_sid').val(res.sid);
					}else{
						$('#serviceTimeDTO_sid').val('');
					}
				 	if(res.allday){
				 		
				 		$('#setTime').hide();
				 		$('#fullday').attr("checked",true);
						$('#fullDayTd').css('width','100%');
				 		
				 		
				 	}else{
				 		
				 		$('#setTime').show();
				 		$('#diyday').attr("checked",true);
						$('#fullDayTd').css('width','50%');
				 	}
				    $('#serviceTimeDTO_startHour').val(res.startHour);
				    $('#serviceTimeDTO_startMinute').val(res.startMinute);
				    $('#serviceTimeDTO_startNoonHour').val(res.startNoonHour);
				    $('#serviceTimeDTO_startNoonMinute').val(res.startNoonMinute);
				    $('#serviceTimeDTO_endNoonHour').val(res.endNoonHour);
				    $('#serviceTimeDTO_endNoonMinute').val(res.endNoonMinute);
				    $('#serviceTimeDTO_endHour').val(res.endHour);
				    $('#serviceTimeDTO_endMinute').val(res.endMinute);
				    
				    
				    
				    if(res.monday){
				    	$('#serviceTimeDTO_monday').attr("checked",true);
				    }else{
				    	$('#serviceTimeDTO_monday').attr("checked",'');
				    }
				    
				    if(res.tuesday){
				    	$('#serviceTimeDTO_tuesday').attr("checked",true);
				    }else{
				    	$('#serviceTimeDTO_tuesday').attr("checked",'');
				    }

				    if(res.wednesday){
				    	$('#serviceTimeDTO_wednesday').attr("checked",true);
				    }else{
				    	$('#serviceTimeDTO_wednesday').attr("checked",'');
				    }

				    if(res.thursday){
				    	$('#serviceTimeDTO_thursday').attr("checked",true);
				    }else{
				    	$('#serviceTimeDTO_thursday').attr("checked",'');
				    }

				    if(res.friday){
				    	$('#serviceTimeDTO_friday').attr("checked",true);
				    }else{
				    	$('#serviceTimeDTO_friday').attr("checked",'');
				    }
				    
				    if(res.saturday){
				    	$('#serviceTimeDTO_saturday').attr("checked",true);
				    }else{
				    	$('#serviceTimeDTO_saturday').attr("checked",'');
				    }
				    
				    if(res.sunday){
				    	$('#serviceTimeDTO_sunday').attr("checked",true);
				    }else{
				    	$('#serviceTimeDTO_sunday').attr("checked",'');
				    }
				    var isSelectAll=true;
				    $("#serviceTimeDiv [type='checkbox']").each(function(){
				    	if($(this).attr("checked")==false)
					    {
					    	isSelectAll=false;
					    }
				    });
				    if($("#serviceTimeDiv [type='checkbox']").is(':checked')){
				    	if(isSelectAll){
					    	$("#workDay_reverseSelect").attr("checked",true);
					    	$("#workDay_normalSelect").attr("checked",'');
					    }else{
					    	$("#workDay_normalSelect").attr("checked",true);
					    	$("#workDay_reverseSelect").attr("checked",'');
					    }
				    }else{
				    	$("#workDay_reverseSelect").attr("checked",'');
				    	$("#workDay_normalSelect").attr("checked",'');
				    }
				    
				});
		},
		
		
		/**
		 * @description 保存服务时间.
		 */
		saveServiceTime:function(){
			startProcess();
			var serviceTime_orgNo=$('#serviceTime_orgNo').val();
			
			if(serviceTime_orgNo==''){
				endProcess();
				msgAlert(i18n['msg_chooseOrg'],'info');
			}else{
			
				common.security.organizationServiceTime.setFormValue();
					
				
				var startHour=($('#serviceTimeDTO_startHour').val())*1;
				var startMinute=($('#serviceTimeDTO_startMinute').val())*1;
				var startNoonHour=($('#serviceTimeDTO_startNoonHour').val())*1;
				var startNoonMinute=($('#serviceTimeDTO_startNoonMinute').val())*1;
							
				var startworkminutes=((startNoonHour*60)+startNoonMinute)-((startHour*60)+startMinute);
				
				var endNoonHour=($('#serviceTimeDTO_endNoonHour').val())*1;
				var endNoonMinute=($('#serviceTimeDTO_endNoonMinute').val())*1;
				var endHour=($('#serviceTimeDTO_endHour').val())*1;
				var endMinute=($('#serviceTimeDTO_endMinute').val())*1;
							
				var endworkminutes=((endHour*60)+endMinute)-((endNoonHour*60)+endNoonMinute);
				
				var Noonworkminutes=((endNoonHour*60)+endNoonMinute)-((startNoonHour*60)+startNoonMinute);
				
				if($('#serviceTimeDTO_allday').val()=="true" || (startworkminutes>=0 && endworkminutes>=0 && Noonworkminutes>=0)){	
					
					var url="serviceTime!saveOrgUpdate.action";
					var frm = $('#serviceTimeDiv form').serialize();
					$.post(url,frm, function(){
						endProcess();
						msgShow(i18n['msg_serviceTime_updateSuccessful'],'show')
					});
				}else if(startworkminutes<0){
					endProcess();
					msgAlert(i18n['msg_startworkminutes_not_correct'],'info')
				}else if(Noonworkminutes<0){
					endProcess();
					msgAlert(i18n['msg_Noonworkminutes_not_correct'],'info')
				}else if(endworkminutes<0){
					endProcess();
					msgAlert(i18n['msg_endworkminutes_not_correct'],'info')
				}
			}
		},
		
		
		
		/**
		 * @description 设置表单值.
		 * @param method 方法
		 */
		setFormValue:function(method){
			
			var fullday=$('#fullday').attr("checked");

			if(fullday){
				$('#serviceTimeDTO_allday').val("true");
			}else{
				$('#serviceTimeDTO_allday').val("false");
			}
			
			
			
			
			
			
			
			
			if($('#serviceTimeDTO_monday').attr("checked")){
				$('#serviceTimeDTO_monday').val("true");
			}
			if($('#serviceTimeDTO_tuesday').attr("checked")){
				$('#serviceTimeDTO_tuesday').val("true");
			}
			if($('#serviceTimeDTO_wednesday').attr("checked")){
				$('#serviceTimeDTO_wednesday').val("true");
			}
			if($('#serviceTimeDTO_thursday').attr("checked")){
				$('#serviceTimeDTO_thursday').val("true");
			}
			if($('#serviceTimeDTO_friday').attr("checked")){
				$('#serviceTimeDTO_friday').val("true");
			}
			if($('#serviceTimeDTO_saturday').attr("checked")){
				$('#serviceTimeDTO_saturday').val("true");
			}
			if($('#serviceTimeDTO_sunday').attr("checked")){
				$('#serviceTimeDTO_sunday').val("true");
			}
			
			
			
			
			
			
		},
		
		/**
		 * 初始化加载
		 */
		init:function(){
			$('#serviceTime_save').click(common.security.organizationServiceTime.saveServiceTime);
			
			
			$('#fullday').click(function(){//全天工作
				
				$('#setTime').hide();
				$('#fullDayTd').css('width','100%');
				common.security.organizationServiceTime.setFormValue();
			});
			

			$('#diyday').click(function(){//自定义工作时间
				
				$('#setTime').show();
				$('#fullDayTd').css('width','50%');
				common.security.organizationServiceTime.setFormValue();
			});
			
			
			//全选
			$('#workDay_reverseSelect').click(function(){
				
				var checkFlag=false;
				
				if(this.checked){ 
					checkFlag=true;				
				}

				$("#workDay_workTime input,#workDay_weekEnd input").each(function(){
					this.checked=checkFlag;				
				});
			});
			
			//常用时间
			$('#workDay_normalSelect').click(function(){

				
				$("#workDay_weekEnd input").each(function(){
					this.checked=false;				
				});
				
				
				$("#workDay_workTime input").each(function(){
					this.checked=true;				
				});
				
			});
		}
	};
}();
