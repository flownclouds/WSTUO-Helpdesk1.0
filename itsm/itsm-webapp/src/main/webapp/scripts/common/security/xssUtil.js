$package('common.security');
/**
 * 字符转义Util
 */
common.security.xssUtil = function() {
	return {
		 html_encode : function(str){  
			  var s = "";  
			  if (str.length == 0) 
				  return "";
			  s = str.replace(/&/g, "&amp;");  
			  s = s.replace(/</g, "&lt;");  
			  s = s.replace(/>/g, "&gt;");
			  s = s.replace(/\'/g, "&#39;");  
			  s = s.replace(/\"/g, "&quot;");  
			  s = s.replace(/\n/g, "<br>");  
			  return s;  
		},
		html_code : function(str){
			  var s = "";  
			  if (str.length == 0) 
				  return "";
			  s = str.replace(/&amp;/g, "&");
			  s = s.replace(/&lt;/g, "<");
			  s = s.replace(/&gt;/g, ">");
			  s = s.replace(/&nbsp;/g, "&nbsp;");
			  s = s.replace(/&#39;/g, "'");
			  s = s.replace(/&quot;/g, "\"");
			  s = s.replace(/<br>/g, "\n");
			  return s;  
		}
	}
}();