
$package('common.security');

/**  
* @fileOverview 默认公司主函数.
* @author QXY
* @version 1.0  
*/  
/**  
* @author QXY  
* @constructor company
* @description 默认公司主函数.
* @date 2011-08-02
* @since version 1.0 
*/
common.security.defaultCompany=function(){
	return {
		loadDefaultCompany:function(companyNoId,companyNameId){
			$(companyNoId).val(companyNo);
			$(companyNameId).val(companyName);
		}
	};
}();