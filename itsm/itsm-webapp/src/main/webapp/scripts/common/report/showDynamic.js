$package('common.report');

/**  
 * @fileOverview "可链接点击事件方法管理"
 * @author Martin  
 * @constructor Martin
 * @description 可链接点击事件方法管理
 * @date 2013-09-13
 * @version 1.0  
 * @since version 1.0 
 */
common.report.showDynamic=function(){
	return{
		
		/**
		 * @description 动作格式化.
		 * @param rowValue 行值
		 * @param reportId 编号
		 * @param colValue 当前列值
		 */
		searchContract:function(rowValue,reportId,colValue){
			rowValue=trim(rowValue);
			colValue=trim(colValue);
			rowValue=encodeURI(rowValue);
			rowValue=encodeURI(rowValue);
			colValue=encodeURI(colValue);
			colValue=encodeURI(colValue);
			if(rowValue=="null"&&colValue=="null"){
				
			}else{
				var _url='crossreport!findRequestById.action';
				$.post(_url,'reportId='+reportId,function(data){
					if(data.entityClass=="com.wstuo.itsm.request.entity.Request"){
						basics.tab.tabUtils.refreshTab(i18n['title_request_requestGrid'],'../pages/itsm/request/requestMain.jsp?countQueryType=dynamicSearch&rowValue='+rowValue+'&colValue='+colValue+'&rowKey='+data.rowGroup+'&colKey='+data.columnGroup+'&customFilterNo='+data.customFilterNo);
					}
					if(data.entityClass=="com.wstuo.itsm.domain.entity.Problems"){
						basics.tab.tabUtils.refreshTab(i18n['problem_grid'],'../pages/itsm/problem/problemMain.jsp?countQueryType=dynamicSearch&rowValue='+rowValue+'&colValue='+colValue+'&rowKey='+data.rowGroup+'&colKey='+data.columnGroup+'&customFilterNo='+data.customFilterNo);
					}
					if(data.entityClass=="com.wstuo.itsm.domain.entity.Changes"){
						basics.tab.tabUtils.refreshTab(i18n['title_changeList'],'../pages/itsm/change/changeMain.jsp?fullsearch=dynamicSearch&rowValue='+rowValue+'&colValue='+colValue+'&rowKey='+data.rowGroup+'&colKey='+data.columnGroup+'&customFilterNo='+data.customFilterNo);
					}
					if(data.entityClass=="com.wstuo.itsm.domain.entity.CI"){
						basics.tab.tabUtils.refreshTab(i18n['ci_configureItemAdmin'],'../pages/itsm/cim/configureItem.jsp?countQueryType=dynamicSearch&rowValue='+rowValue+'&colValue='+colValue+'&rowKey='+data.rowGroup+'&colKey='+data.columnGroup+'&customFilterNo='+data.customFilterNo);
					}
					if(data.entityClass=="com.wstuo.itsm.knowledge.entity.KnowledgeInfo"){
						basics.tab.tabUtils.refreshTab(i18n['title_request_knowledgeGrid'],'../pages/common/knowledge/knowledgeMain.jsp?countQueryType=dynamicSearch&rowValue='+rowValue+'&colValue='+colValue+'&rowKey='+data.rowGroup+'&colKey='+data.columnGroup+'&customFilterNo='+data.customFilterNo);
					}
				});
			}
		},
		/**
		 * @description 单项报表
		 * @param rowValue 行值
		 * @param reportId 报表编号
		 */
		SingleGroupReport:function(rowValue,reportId){
			rowValue=trim(rowValue);
			rowValue=encodeURI(rowValue);
			rowValue=encodeURI(rowValue);
			var _url='sgreport!findRequestById.action';
			$.post(_url,'reportId='+reportId,function(data){
				var colValue="";
				var colKey="";
				if(data.entityClass=="com.wstuo.itsm.request.entity.Request"){
					basics.tab.tabUtils.refreshTab(i18n['title_request_requestGrid'],'../pages/itsm/request/requestMain.jsp?countQueryType=dynamicSearch&rowValue='+rowValue+'&colValue='+colValue+'&rowKey='+data.xgroupField+'&colKey='+colKey+'&customFilterNo='+data.customFilterNo);
				}
				if(data.entityClass=="com.wstuo.itsm.domain.entity.Problems"){
					basics.tab.tabUtils.refreshTab(i18n['problem_grid'],'../pages/itsm/problem/problemMain.jsp?countQueryType=dynamicSearch&rowValue='+rowValue+'&colValue='+colValue+'&rowKey='+data.xgroupField+'&colKey='+colKey+'&customFilterNo='+data.customFilterNo);
				}
				if(data.entityClass=="com.wstuo.itsm.domain.entity.Changes"){
					basics.tab.tabUtils.refreshTab(i18n['title_changeList'],'../pages/itsm/change/changeMain.jsp?fullsearch=dynamicSearch&rowValue='+rowValue+'&colValue='+colValue+'&rowKey='+data.xgroupField+'&colKey='+colKey+'&customFilterNo='+data.customFilterNo);
				}
				if(data.entityClass=="com.wstuo.itsm.domain.entity.CI"){
					basics.tab.tabUtils.refreshTab(i18n['ci_configureItemAdmin'],'../pages/itsm/cim/configureItem.jsp?countQueryType=dynamicSearch&rowValue='+rowValue+'&colValue='+colValue+'&rowKey='+data.xgroupField+'&colKey='+colKey+'&customFilterNo='+data.customFilterNo);
				}
				if(data.entityClass=="com.wstuo.itsm.knowledge.entity.KnowledgeInfo"){
					basics.tab.tabUtils.refreshTab(i18n['title_request_knowledgeGrid'],'../pages/common/knowledge/knowledgeMain.jsp?countQueryType=dynamicSearch&rowValue='+rowValue+'&colValue='+colValue+'&rowKey='+data.xgroupField+'&colKey='+colKey+'&customFilterNo='+data.customFilterNo);
				}
			});
		},
		/**
		 * @description KPI
		 * @param rowValue 行值
		 * @param reportId 编号
		 * @param colValue 当前列值
		 */
		kpiReport:function(reportId,colValue,rowValue){
			rowValue=trim(rowValue);
			colValue=trim(colValue);
			rowValue=encodeURI(rowValue);
			rowValue=encodeURI(rowValue);
			colValue=encodeURI(colValue);
			colValue=encodeURI(colValue);
			var _url='kpireport!findRequestById.action';
			$.post(_url,'reportId='+reportId+'&col='+colValue,function(data){
				if(data.entityClass=="com.wstuo.itsm.request.entity.Request"){
					basics.tab.tabUtils.refreshTab(i18n['title_request_requestGrid'],'../pages/itsm/request/requestMain.jsp?countQueryType=dynamicSearch&rowValue='+rowValue+'&colValue='+data.type+'&rowKey='+data.groupField+'&customFilterNo='+data.customFilterNo);
				}
				if(data.entityClass=="com.wstuo.itsm.domain.entity.Problems"){
					basics.tab.tabUtils.refreshTab(i18n['problem_grid'],'../pages/itsm/problem/problemMain.jsp?countQueryType=dynamicSearch&rowValue='+rowValue+'&colValue='+data.type+'&rowKey='+data.groupField+'&customFilterNo='+data.customFilterNo);
				}
				if(data.entityClass=="com.wstuo.itsm.domain.entity.Changes"){
					basics.tab.tabUtils.refreshTab(i18n['title_changeList'],'../pages/itsm/change/changeMain.jsp?fullsearch=dynamicSearch&rowValue='+rowValue+'&colValue='+data.type+'&rowKey='+data.groupField+'&customFilterNo='+data.customFilterNo);
				}
				if(data.entityClass=="com.wstuo.itsm.domain.entity.CI"){
					basics.tab.tabUtils.refreshTab(i18n['ci_configureItemAdmin'],'../pages/itsm/cim/configureItem.jsp?countQueryType=dynamicSearch&rowValue='+rowValue+'&colValue='+data.type+'&rowKey='+data.groupField+'&customFilterNo='+data.customFilterNo);
				}
				if(data.entityClass=="com.wstuo.itsm.knowledge.entity.KnowledgeInfo"){
					basics.tab.tabUtils.refreshTab(i18n['title_request_knowledgeGrid'],'../pages/common/knowledge/knowledgeMain.jsp?countQueryType=dynamicSearch&rowValue='+rowValue+'&colValue='+data.type+'&rowKey='+data.groupField+'&customFilterNo='+data.customFilterNo);
				}
			});
		},
		
		/**
		 * @description 多项
		 * @param  colValue 当前列值
		 * @param  stisticField 编号
		 * @param  reportId 编号
		 */ 
		MutilReport:function(colValue,stisticField,reportid){
			colValue=trim(colValue);
			colValue=encodeURI(colValue);
			colValue=encodeURI(colValue);
			var _url='mutilreport!findReportById.action';
			$.post(_url,'reportId='+reportid,function(data){
				var _url1='stistic!findReportById.action';
				$.post(_url1,'reportId='+stisticField,function(data1){
					if(data.entityClass=="com.wstuo.itsm.request.entity.Request"){
						basics.tab.tabUtils.refreshTab(i18n['title_request_requestGrid'],'../pages/itsm/request/requestMain.jsp?countQueryType=dynamicSearch&rowValue=mutil&colValue='+colValue+'&rowKey='+data1.stisticField+'&colKey='+data.groupField+'&customFilterNo='+data.customFilterNo);
					}
					if(data.entityClass=="com.wstuo.itsm.domain.entity.Problems"){
						basics.tab.tabUtils.refreshTab(i18n['problem_grid'],'../pages/itsm/problem/problemMain.jsp?countQueryType=dynamicSearch&rowValue=mutil&colValue='+colValue+'&rowKey='+data1.stisticField+'&colKey='+data.groupField+'&customFilterNo='+data.customFilterNo);
					}
					if(data.entityClass=="com.wstuo.itsm.domain.entity.Changes"){
						basics.tab.tabUtils.refreshTab(i18n['title_changeList'],'../pages/itsm/change/changeMain.jsp?fullsearch=dynamicSearch&rowValue=mutil&colValue='+colValue+'&rowKey='+data1.stisticField+'&colKey='+data.groupField+'&customFilterNo='+data.customFilterNo);
					}
					if(data.entityClass=="com.wstuo.itsm.domain.entity.CI"){
						basics.tab.tabUtils.refreshTab(i18n['ci_configureItemAdmin'],'../pages/itsm/cim/configureItem.jsp?countQueryType=dynamicSearch&rowValue=mutil&colValue='+colValue+'&rowKey='+data1.stisticField+'&colKey='+data.groupField+'&customFilterNo='+data.customFilterNo);
					}
					if(data.entityClass=="com.wstuo.itsm.knowledge.entity.KnowledgeInfo"){
						basics.tab.tabUtils.refreshTab(i18n['title_request_knowledgeGrid'],'../pages/common/knowledge/knowledgeMain.jsp?countQueryType=dynamicSearch&rowValue=mutil&colValue='+colValue+'&rowKey='+data1.stisticField+'&colKey='+data.groupField+'&customFilterNo='+data.customFilterNo);
					}
				});
			});
		}
    };
}();