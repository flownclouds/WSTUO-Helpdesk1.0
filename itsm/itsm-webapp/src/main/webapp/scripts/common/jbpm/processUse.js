$package('common.jbpm') 
$import('common.config.systemGuide.systemGuide');
/**  
 * @fileOverview "processUse"
 * @author QXY
 * @version 1.0  
 */  
 /**  
 * @author QXY  
 * @constructor processUse
 * @description 流程选项函数
 * @date 2010-11-17
 * @since version 1.0 
 */
common.jbpm.processUse=function(){
	return {
		
		/**
		 * @description 流程下拉选择
		 */
		getProcess:function(select,filterCondition1,filterCondition2){

			$('#'+select).html('');
			$('<option value="">--'+i18n['pleaseSelect']+'--</option>').appendTo('#'+select);
			$.post('jbpm!findPageProcessDefinitions.action?rows=10000',function(process){
				if(process!=null){
					var _data = process.data;
					if(_data.length>0){
						for(var i=0;i<_data.length;i++){
							if(_data[i].name.lastIndexOf(filterCondition1)>=0 || _data[i].name.lastIndexOf(filterCondition2)>=0)
								$('<option value="'+_data[i].id+'">'+_data[i].name+'(Ver:'+_data[i].version+')</option>').appendTo('#'+select);
						}
					}
				}
			});
		},
		
		/**
		 * @description 保存设置
		 */
		processUseSet:function(form){
			var frm = $('#'+form+' form').serialize();
			$.post('processUse!processUseSet.action',frm,function(){
				msgShow(i18n['saveSuccess'],'show');
				
				 /**保存向导状态*/
				common.config.systemGuide.systemGuide.saveSystemGuide("requestProcess");
			});
		},
		/**
		 * 各模块当前使用的流程
		 */
		processUseBy:function(){
			$.post('processUse!processUseByUseName.action','processUseDTO.useName=processUse',function(data){
				if(data.requestProcessDefinitionId!=null)
					$('#requestProcessKey').val(data.requestProcessDefinitionId);
				if(data.changeProcessDefinitionId!=null)
					$('#changeProcessKey').val(data.changeProcessDefinitionId);
				if(data.problemProcessDefinitionId!=null)
					$('#problemProcessKey').val(data.problemProcessDefinitionId);
				if(data.releaseProcessDefinitionId!=null)
					$('#releaseProcessKey').val(data.releaseProcessDefinitionId);
			})
		},
		/**
		 * 初始化
		 * @private
		 */
		init:function(){
			$("#processUse_loading").hide();
			$("#processUse_content").show();
			common.jbpm.processUse.getProcess("requestProcessKey","request","Request");//请求
			common.jbpm.processUse.getProcess("changeProcessKey","change","Change");//变更
			common.jbpm.processUse.getProcess("problemProcessKey","problem","Problem");//问题
			common.jbpm.processUse.getProcess("releaseProcessKey","release","Release");//发布
			setTimeout(function(){
				common.jbpm.processUse.processUseBy();
			},500);
			
		}
	}
}();
$(document).ready(common.jbpm.processUse.init);