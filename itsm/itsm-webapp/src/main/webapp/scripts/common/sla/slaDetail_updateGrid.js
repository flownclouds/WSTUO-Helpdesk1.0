$package('common.sla');
/**  
 * @author Van  
 * @constructor SLAServiceManage
 * @description SLA详细信息主函数.
 * @date 2011-02-25
 * @since version 1.0 
 */

common.sla.slaDetail_updateGrid = function(){
	
	
	this.updateGrid_operation;
	
	
	
	
	return {
		
	
	
		/**
		 * @description 格式化逾期前，逾期后.
		 * @param cell 列显示值
		 */
		formatBeforeOrAfterStr:function(cell){
			return cell=='逾期前升级'?i18n['label_slaRule_upgradeBeforeOverdue']:i18n['label_slaRule_upgradeAfterOverdue'];
		},
	
		/**
		 * @description 美化时间显示方式.
		 * @param cell 列显示值
		 */
		timeStrFormatter:function(cell){
			
			return cell.replace('天',i18n['label_slaRule_days']).replace('小时',i18n['label_slaRule_hours']).replace('分钟',i18n['label_slaRule_minutes']);
		},
		
		
		/**
		 * @description 自动升级列表.
		 */
		showAutoUpdateGrid:function(){
			
	
			var params = $.extend({},jqGridParams, {	
				url:'promoteRule!findPromoteRulePage.action?contractNo='+_contractNo+"&ruleType=promote",
				colNames:[i18n['title_sla_autoName'],i18n['title_sla_updateStyle'],i18n['title_sla_updateTime'],'','','','','','','',''],
				colModel:[
						  {name:'ruleName',width:70,align:'center'},
						  {name:'beforeOrAfterStr',index:'beforeOrAfter',width:120,align:'center',formatter:common.sla.slaDetail_updateGrid.formatBeforeOrAfterStr},
						  {name:'timeStr',index:'ruleTime',width:120,align:'center',formatter:common.sla.slaDetail_updateGrid.timeStrFormatter},
						  {name:'beforeOrAfter',hidden:true},
						  {name:'ruleTime',hidden:true},
						  {name:'isIncludeHoliday',hidden:true},
						  {name:'assigneeNo',hidden:true},
						  {name:'assigneeName',hidden:true},
						  {name:'ruleNo',hidden:true},
						  {name:'updateLevelNo',hidden:true},
						  {name:'referType',hidden:true}
						  
						  ],
				jsonReader: $.extend(jqGridJsonReader, {id: "ruleNo"}),
				sortname:'ruleNo',
				pager:'#autoUpdateGridPager'
				});
				
				$("#autoUpdateGrid").jqGrid(params);
				$("#autoUpdateGrid").navGrid('#autoUpdateGridPager',navGridParams);
				//列表操作项
				$("#t_autoUpdateGrid").css(jqGridTopStyles);
				$("#t_autoUpdateGrid").append($('#autoUpdateGridToolbar').html());
				//自适应宽度
				setGridWidth("#autoUpdateGrid","regCenter",338);
				slaDetailGrids.push('#autoUpdateGrid');
		},
		
	
		/**
		 * @description 新增自动升级策略.
		 */
		addAutoUpdate_OpenWindow:function(){
			updateGrid_operation = "savePromoteRule";
			common.sla.slaDetail_updateGrid.resetAutoUpdateFrom();
			windows('autoUpdateWindow',{width:420});
		},
		
	
		/**
		 * @description 保存自动升级.
		 */
		saveAutoUpdate:function(){
			
			var day=$('#promoteRuleDay').val();
			var hour=$('#promoteRuleHour').val();
			var minute=$('#promoteRuleMinute').val();
			var Level=$('#SLA_AutoUpdateLevel').val();
			if(day==null||day==''){
				day='0';
				$('#promoteRuleDay').val('0');
			}
			if(hour==null||hour==''){
				hour='0';
				$('#promoteRuleHour').val('0');
			}
			if(minute==null||minute==''){
				minute='0';
				$('#promoteRuleMinute').val('0');
			}
			if($('#autoUpdateForm').form('validate')){
				
				if(day!='0'||hour!='0'||minute!='0'){
					
					var frm = $('#autoUpdateWindow form').serialize();
					var _url="promoteRule!"+updateGrid_operation+".action";
					if(Level!=null&&Level!=''){
						startProcess();
						$.post(_url,frm, function(){
							$('#autoUpdateWindow').dialog('close');
							$('#autoUpdateGrid').trigger('reloadGrid');
							msgShow(i18n['msg_operationSuccessful'],'show');
							endProcess();
						});
					}else{
						msgAlert(i18n['msg_request_chooseUpdateOwner'],'info');
					}
					
				}else{
					
					msgAlert(i18n['NOT_Update'],'info');
				}
			}
			
		},
		
		
		/**
		 * @description 清空表单.
		 */
		resetAutoUpdateFrom:function(){
			
			$('#promoteRule_ruleName').val('');
			$('#promoteRuleDay').val('0');
			$('#promoteRuleHour').val('0');
			$('#promoteRuleMinute').val('0');
			$('#promoteRule_assigneeName').val('');
			$('#promoteRule_assigneeNo').val('');
		},
		
		
		/**
		 * @description 打开编辑自动升级窗口.
		 */
		editAutoUpdate_OpenWindow:function(){
			
			checkBeforeEditGrid('#autoUpdateGrid',function(rowData){
				
				
				 common.sla.slaDetail_updateGrid.resetAutoUpdateFrom();
				
				 var time=rowData.ruleTime;
				 
				 $('#promoteRule_ruleNo').val(rowData.ruleNo);
				 $('#promoteRule_ruleName').val(rowData.ruleName);
				 $("input[name='promoteRuleDTO.beforeOrAfter']").val([rowData.beforeOrAfter]); 
				 				 
				 $('#promoteRuleDay,#promoteRuleHour,#promoteRuleMinute').val('0');

				 if(time!=null&&time!='null'){
					 
					var day=Math.floor(time/86400);
					if(day>=1){
						 $('#promoteRuleDay').val(day);
					}
					
					var hour=Math.floor(time%86400/3600);					
					if(hour>=1){
						 $('#promoteRuleHour').val(hour);
					}
					
					var min=time%86400%3600/60;
					if(min>=1){
						$('#promoteRuleMinute').val(min);
					}

					 
				 }
				 
				 $("input[name='promoteRuleDTO.isIncludeHoliday']").val([rowData.isIncludeHoliday]);  
				 $("input[name='promoteRuleDTO.referType']").val([rowData.referType]);  
				 $('#promoteRule_assigneeName').val(rowData.assigneeName);
				 $('#promoteRule_assigneeNo').val(rowData.assigneeNo);
				 $('#SLA_AutoUpdateLevel').val(rowData.updateLevelNo);

				 updateGrid_operation="mergePromoteRule";
				 windows('autoUpdateWindow',{width:420});
			});
		},
	
		
		/**
		 * @description 删除自动升级.
		 */
		deleteAutoUpdate:function(){
			
			checkBeforeDeleteGrid('#autoUpdateGrid',function(rowIds){
				
				var _param = $.param({'ruleNos':rowIds},true);
				
				$.post("promoteRule!deletePromoteRules.action",_param,function(){
					
					$("#autoUpdateGrid").trigger('reloadGrid');
					
					msgShow(i18n['msg_deleteSuccessful'],'show');
					
				},"json");
			});
		},


		/**
		 * @description 初始化表单
		 */
		init:function(){
			
			common.sla.slaDetail_updateGrid.showAutoUpdateGrid();
			
			$('#autoUpdateGrid_add').click(common.sla.slaDetail_updateGrid.addAutoUpdate_OpenWindow);
			$('#autoUpdateGrid_edit').click(common.sla.slaDetail_updateGrid.editAutoUpdate_OpenWindow);
			$('#autoUpdateGrid_delete').click(common.sla.slaDetail_updateGrid.deleteAutoUpdate);
			$('#autoUpdateGrid_save').click(common.sla.slaDetail_updateGrid.saveAutoUpdate);
			
			
//			$('#promoteRule_assigneeName').click(function(){
//				common.security.userUtil.selectUser('#promoteRule_assigneeName','#promoteRule_assigneeNo','','loginName',companyNo);
//			});
			common.rules.ruleCM.loadUpdateLevel('#SLA_AutoUpdateLevel');
		}
	}
 }();

