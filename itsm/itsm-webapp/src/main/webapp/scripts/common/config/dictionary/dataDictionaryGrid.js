$package('common.config.dictionary');
$import('common.config.systemGuide.systemGuide');
/**  
 * @author QXY  
 * @constructor dictionary
 * @description 数据字典列表
 * @date 2010-11-17
 * @since version 1.0 
 */  
common.config.dictionary.dataDictionaryGrid = function(){
	this.dcOperation='';
	/**
	 * 组装对应模块元素
	 * @param selector 选择对象
	 */
	function $dcfull(selector) {
		return $("#dataDicionaryMain_content_"+_groupCode+" " + selector);
	}
	/**
	 * 组装对应模块groupCode
	 * @param panelDIV 面板div
	 */
	function $groupCode(panelDIV) {
		return panelDIV+"_"+_groupCode;
	}
	return {
		/**
		 * @description 颜色格式化
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 * @private
		 */
		colorFormatter:function(cell,event,data){
			return "<div style='background-color:"+data.color+";width:96%;height:18px;margin:3px'></div>";
		
		},
		/**@description 类表菜单*/
		showDataDictionaryGrid:function(){
			var params = $.extend({},jqGridParams, {	
				url :'dataDictionaryItems!find.action?dataDictionaryQueryDto.groupCode='+_groupCode,
				colNames:['ID',i18n['name'],i18n['description'],i18n['remark'],'','','',i18n['label_color']],
				colModel:[
				          	{name:'dcode',width:20,align:'center',sortable:true,hidden:true},
							{name:'dname',width:20,align:'center',sortable:true},
							{name:'description',align:'center',width:40,sortable:false},
							{name:'remark',align:'center',width:40,sortable:false},
							{name:'groupCode',hidden:true},	
							{name:'dno',hidden:true},
							{name:'color',hidden:true},
							{name:'colorPanel',width:20,sortable:false,align:'center',formatter:common.config.dictionary.dataDictionaryGrid.colorFormatter}
					    ],
				jsonReader : $.extend(jqGridJsonReader, {
					id : "dcode"
				}),
				sortname : 'dcode',
				pager : $groupCode('#dictionaryItemGridPager'),
				autowidth:true
			});
			$($groupCode('#dictionaryItemGrid')).jqGrid(params);
			$($groupCode('#dictionaryItemGrid')).navGrid($groupCode('#dictionaryItemGridPager'), navGridParams);
			//列表操作项
			$($groupCode('#t_dictionaryItemGrid')).css(jqGridTopStyles);
			if($($groupCode('#t_dictionaryItemGrid')).html()=="")
				$($groupCode('#t_dictionaryItemGrid')).html($dcfull('#dataDictionaryGridToolbar').html());
			//自适应宽度
			setGridWidth($groupCode('#dictionaryItemGrid'),"regCenter",20);
		},
		/**
		 * 保存数据字典
		 */
		addDictionaryItem:function(){
			//清空框内内容
			$($groupCode('#addEditDictionaryWindow')+' [name="dataDictionaryItemsDto.dname"]').val("");
			$($groupCode('#addEditDictionaryWindow')+' [name="dataDictionaryItemsDto.description"]').val("");
			$($groupCode('#addEditDictionaryWindow')+' [name="dataDictionaryItemsDto.remark"]').val("");
			$($groupCode('#addEditDictionaryWindow')+' [name="dataDictionaryItemsDto.dno"]').val("");
			$($groupCode('#addEditDictionaryWindow')+' [name="dataDictionaryItemsDto.dcode"]').val("");
			$("#groupCode_"+_groupCode).val(_groupCode);
			windows('addEditDictionaryWindow_'+_groupCode,{width:400});
			dcOperation = 'save';
		},
		/**
		 * @description 保存数据字典.
		 * 
		 */
		saveDictionary:function(){		
			vaildateForm($groupCode('#addDataDictionaryForm'),function(){
				var ser = $('#addEditDictionaryWindow_'+_groupCode+' form').serialize();	
				var url = "dataDictionaryItems!"+dcOperation+".action";
				startProcess();
				$.post(url, ser, function(){
					$($groupCode('#addEditDictionaryWindow')).dialog('close');
					$($groupCode('#dictionaryItemGrid')).trigger('reloadGrid');			
					msgShow(i18n['saveSuccess'],'show');
					/**保存向导状态*/
					common.config.systemGuide.systemGuide.saveSystemGuide(_groupCode+"Guide");
					endProcess();
				});	
			})
		},
		 /**
		  * @description 删除数据字典.
		  */
		delDictionary:function(){
			
			checkBeforeDeleteGrid($groupCode('#dictionaryItemGrid'),function(rowIds){				
				var _param = $.param({'dcode':rowIds},true);
				//判断是否为系统数据
				$.post("dataDictionaryItems!findByDcodeIsSystemData.action", _param, function(data){
				if(data==1){
					msgShow(i18n['label_Is_System_Data'],'show');
				}else{
					$.post("dataDictionaryItems!delete.action", _param, function(data){
					if(data){
						msgShow(i18n['msg_deleteSuccessful'],'show');
						$($groupCode('#dictionaryItemGrid')).jqGrid('setGridParam',{page:1}).trigger('reloadGrid'); 
						
					}else{
						msgAlert(i18n['systemDataNotDelete'],'info');
					}
				}, "json");		
				}
				});
			});
		},
		/**
		 * 打开搜索框
		 */
		openSearchWindow:function(){
			windows($groupCode('searchDataDictionaryDiv'),{width:380,modal: false});
		},
		 /**
		  * @description 编辑数据字典.
		  */
		editDictionary:function() {		
			checkBeforeEditGrid($groupCode('#dictionaryItemGrid'),function(data){
				$("#dcode_"+_groupCode).val(data.dcode);
				$("#dname_"+_groupCode).val(data.dname).focus();
				$("#description_"+_groupCode).val(data.description);
				$("#remark_"+_groupCode).val(data.remark);
				$("#groupCode_"+_groupCode).val(data.groupNo);
				$("#dno_"+_groupCode).val(data.dno);
				if(data.color==null || data.color==''){
					$("#color_"+_groupCode).val('#ffffff');
					$("#color_"+_groupCode).css({'background':'#ffffff'});
				}else{
					$("#color_"+_groupCode).val(data.color);
				    $("#color_"+_groupCode).css({'background':data.color});
				}
				dcOperation = 'merge';	
				windows($groupCode('#addEditDictionaryWindow').replace('#',''),{width:400});
			});
		},
		 /**
		  * @description 搜索
		  * */
		searchDictionary:function() {		
			var sdata = $($groupCode('#searchDataDictionaryDiv')+' form').getForm();
			var postData = $($groupCode('#dictionaryItemGrid')).jqGrid("getGridParam", "postData");  
			$.extend(postData, sdata);  //将postData中的查询参数覆盖为空值
			var _url = 'dataDictionaryItems!find.action';		
			$($groupCode('#dictionaryItemGrid')).jqGrid('setGridParam',{url:_url}).trigger('reloadGrid'); 
			return false;
		},
		/**
		 * 导出数据到Excel
		 */
		exportView:function(){
			$($groupCode('#searchDataDictionaryDiv')+' form').submit();
		},
		/**
		 * 导入数据.
		 */
		importData:function(){
			windows('importDataDictionaryWindow_'+_groupCode,{width:400});
		},
		/**
		 * @description 导入数据.
		 */
		importExcel:function(){
			if($("#dataDictionaryImportFile"+_groupCode).val().length>0){
				startProcess();
				$.ajaxFileUpload({
		            url:'dataDictionaryItems!importDataDictionaryItemData.action?dataDictionaryItemsDto.groupCode='+_groupCode,
		            secureuri:false,
		            fileElementId:'dataDictionaryImportFile'+_groupCode, 
		            dataType:'json',
		            success: function(data,status){
		            	if(data=="FileNotFound"){
							msgAlert(i18n['msg_dc_fileNotExists'],'error');
							endProcess();
						}else if(data=="IOError"){
							msgAlert(i18n['msg_dc_importFailure'],'error');
							endProcess();
						}else{
							$('#importDataDictionaryWindow').dialog('close');
							$($groupCode('#dictionaryItemGrid')).trigger('reloadGrid'); 
							msgAlert(data.replace('Total',i18n['opertionTotal']).replace('Insert',i18n['newAdd']).replace('Update',i18n['update']).replace('Failure',i18n['failure']),'show');
							endProcess();
						}
		            }
		        });
			}else{
				msgAlert(i18n['msg_dc_fileNull'],'info');
				endProcess();
			}
		},
		/**
		 * 初始化
		 * @private 
		 */
		init:function(){
			$($groupCode("#dataDicionaryMain_loading")).hide();
			$($groupCode("#dataDicionaryMain_content")).show();
			common.config.dictionary.dataDictionaryGrid.showDataDictionaryGrid();//加载数据列表
			$dcfull('#saveDataDictionaryBtn').click(common.config.dictionary.dataDictionaryGrid.saveDictionary);//绑定事件
			$dcfull('#searchDataDictionaryBtn').click(common.config.dictionary.dataDictionaryGrid.searchDictionary);//绑定事件
			$dcfull('#exportDataDc').click(common.config.dictionary.dataDictionaryGrid.exportView);//导出
			$dcfull('#importDataDc').click(common.config.dictionary.dataDictionaryGrid.importData);//导入	
		}
	}
}();
$(document).ready(common.config.dictionary.dataDictionaryGrid.init);

