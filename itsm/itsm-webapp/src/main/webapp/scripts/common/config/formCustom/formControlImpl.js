﻿$package('common.config.formCustom');
$import('common.security.base64Util');
/**
 * 表单控件实现
 * @author will
 */
common.config.formCustom.formControlImpl=function(){
	/**
	 * 初始化控件
	 */
	this.initControl=function(attrs){
		var attrType=attrs.attrType;
		if(attrType==='String')
			attrType = 'Text';
		else if(attrType==='Date')
			attrType = 'DateTime';
		else if(attrType==='DataDictionaray')
			attrType = 'DataDictionary';
		var obj=calculateFunction(attrType);
		return new obj(attrs);
	};
	this.twoColumn=function(json){
		var html='<div class="field_options"></div>';
		if(json.attrColumn=="2" || json.attrColumn=="9")
			html='<div class="field_options_2Column field_options"></div>';
		if(json.attrType=="Lob"){
			html='<div class="field_options_lob field_options"></div>';
		}
		return $(html);
	};
	/**
	 * 定义不能删除的字段名称
	 */
	this.finalFied='requestDTO.companyName,requestDTO.etitle,requestDTO.edesc,requestDTO.createdByName,ciDto.companyName,ciDto.cino,ciDto.ciname';
				//'ciDto.statusId,ciDto.locId,ciDto.buyDate,ciDto.arrivalDate,ciDto.warningDate,ciDto.lifeCycle,ciDto.warranty,ciDto.assetsOriginalValue,ciDto.department,ciDto.owner,ciDto.userName';
	return {
		initControl:function(attrs){
			return initControl(attrs);
		},
		/**
		 * 生成控件方法
		 * @param attr 控件属性
		 */
		control:function(attrs){
			var field_opt='<div class="field_opt"><a onclick="common.config.formCustom.formCustomDesign.deleteField(this);"><img src="../skin/default/images/grid_delete.png"/></a></div>';
			if(finalFied.indexOf(attrs.attrName)>-1){
				field_opt='<div class="field_opt"><a class="opacity" title="'+i18n.basicFieldsNotRemove+'"><img src="../skin/default/images/grid_delete.png"/></a></div>';
			}
			return initControl(attrs).getControl().replace(/{field_opt}/g,field_opt);
		},
		/**
		 * 生成设计HTMl
		 */
		designHtml:function(attrJson){
			var allHtml=$('<div></div>');
			if(attrJson && attrJson!==''){
				attrJson=JSON.parse(common.security.base64Util.decode(attrJson));
				for ( var _int = 0; _int < attrJson.length; _int++) {
					var attrs=attrJson[_int];
					var event=twoColumn(attrs);
					event.append(common.config.formCustom.formControlImpl.control(attrs));
					//编辑表单时设置字段属性
					event.attr(attrs);
					if(attrs.attrType==="Lob" || attrs.attrColumn=="2")
						event.find('.field').attr("class","field_lob");
					allHtml.append(event);
				}
			}
			return common.config.formCustom.formControlImpl.replaceSpecialAttr(allHtml.html());
		},
		/**
		 * 生成详情HTMl
		 */
		detailHtml:function(attrJson){
			var allHtml=$('<div></div>');
			if(attrJson && attrJson!==''){
				attrJson=JSON.parse(common.security.base64Util.decode(attrJson));
				var mark=false;//记录是否需要插入空白字段
				var column=0;
				for ( var _int = 0; _int < attrJson.length; _int++) {
					var attrs=attrJson[_int];
					var event=twoColumn(attrs);
					if(attrs.dname)//数据字典时显示文本
						attrs.value=attrs.dname;
					var valueHtml='{value}';
					if(attrs.attrName==='requestDTO.createdByName')
						valueHtml=valueHtml+'<a title="'+i18n.label_contactInfo+'" onclick="$(\'#requesterContactInfo_win\').dialog()"><img src="../images/icons/user.gif" style="vertical-align:middle;"></a>';
					var html=initControl(attrs).replaceCommonAttrByDetail(valueHtml,true);
					if(attrs.dcode)
						html+="<input type='hidden' attrtypename='dataDictionaryDecode' value='"+attrs.dcode+"' />";
					event.append(html);
					if(attrs.attrType==="Lob" || attrs.attrColumn=="2"){
						event.find(".field").attr('class','field_lob');
					}
					allHtml.append(event);
					if(attrs.attrColumn=="2" || attrs.attrType=="Lob"|| attrs.attrColumn=="9"){
						if(mark){
							mark=false;
							column=0;
							event.before('<div class="field_options"><div class="label"></div><div class="field"></div></div>');
						}
					}else{
						column++;
						if(column==1){//为偶数时
							mark=true;
						}else if(column==2){//为偶数时
							mark=false;
							column=0;
						}
					}
				}
			}
			return allHtml.html().replace(/{field_opt}/g,'');
		},
		/**
		 * 生成新增编辑HTMl
		 */
		editHtml:function(attrJson,formId){
			var allHtml=$('<div></div>');
			if(attrJson && attrJson!==''){
				attrJson=JSON.parse(common.security.base64Util.decode(attrJson));
				var mark=false;//记录是否需要插入空白字段
				var column=0;
				for ( var _int = 0; _int < attrJson.length; _int++) {
					var event=twoColumn(attrJson[_int]);
					
					if(attrJson[_int].fieldType && attrJson[_int].fieldType==='customField'){
						attrJson[_int].attrNo=formId+"_"+attrJson[_int].attrNo;
					}
					event.append(initControl(attrJson[_int]).getControl());
					
					if(attrJson[_int].attrType==="Lob" || attrJson[_int].attrColumn=="2")
						event.find('.field').attr("class","field_lob");
					
					allHtml.append(event);
					if(attrJson[_int].attrColumn=="2" || attrJson[_int].attrType=="Lob"|| attrJson[_int].attrColumn=="9"){
						if(mark){
							mark=false;
							column=0;
							event.before('<div class="field_options"><div class="label"></div><div class="field"></div></div>');
						}
					}else{
						column++;
						if(column==1){//为偶数时
							mark=true;
						}else if(column==2){//为偶数时
							mark=false;
							column=0;
						}
						
					}
				}
			}
			return common.config.formCustom.formControlImpl.replaceSpecialAttr(allHtml.html());
		},
		/**
		 * 对特殊属性进行处理
		 */
		replaceSpecialAttr:function(html){
			return html.replace(/{field_opt}/g,'');
		},
		/**
		 * 新增、编辑请求时保存value
		 * @param attrJson 控件属性Json数组
		 * @param formId 表单id
		 */
		getFormValues:function(attr,formId){
			var attrJson=JSON.parse(attr);
			for ( var _int = 0; _int < attrJson.length; _int++) {
				var attr=attrJson[_int];
				var name=attr.attrName.substring(attr.attrName.indexOf('\'')+1,attr.attrName.lastIndexOf('\']'));
				if(attr.attrType==='Checkbox'){
					if(name!=null && name != ""){
						var value='';
						$(formId+" input[name="+name+"]:checked").each(function(i, o){
						    value=value+","+$(o).val();
						});
						attrJson[_int].value=value.substr(1);
					}
				}else if(attr.attrType==='Radio'){
					if(name!=null && name != ""){
						attrJson[_int].value=$(formId+" input[name="+name+"]:checked").val();
					}
				}else if(attr.attrType==='DataDictionaray'){
					var dname = '';
					if(name!=null && name != ""){
						var obj=$(formId+" select[name*='"+name+"']");
						attrJson[_int].value=obj.val();
						dname=obj.find("option:selected").text();
					}
					if(dname.indexOf(i18n.pleaseSelect)>-1)
						dname='';
					attrJson[_int].dname=dname;
				}else if(attrJson[_int].fieldType && attrJson[_int].fieldType==='customField'){
					if(name!=null && name != ""){
						var obj=$(formId+" input[name*='"+name+"']");
						attrJson[_int].value=obj.val();
					}
				}else{
					if(name!=null && name != ""){
						var obj=$(formId+" :input[name*='"+name+"']");
						attrJson[_int].value=obj.val();
					}
					if(attrJson[_int].attrHiddenId && attr.attrHiddenName !=null && attr.attrHiddenName!=""){
						attrJson[_int].attrHiddenVal=$(formId+" :hidden[name='"+attr.attrHiddenName+"']").val();
					}
				}
			}
			return common.security.base64Util.encode(JSON.stringify(attrJson));
		},
		getAttrs:function(event){
			return {
				fieldType:event.attr("fieldType"),
				attrType:event.attr("attrType"),
				label:event.attr("label"),
				attrNo:event.attr("attrNo"),
				attrName:event.attr("attrName"),
				required:event.attr("required"),
				attrdataDictionary:event.attr("attrdataDictionary"),
				attrItemName:event.attr("attrItemName"),
				attrHiddenId:event.attr("attrHiddenId"),
				attrHiddenName:event.attr("attrHiddenName"),
				attrHiddenVal:event.attr("attrHiddenVal"),
				attrEventType:event.attr("attrEventType"),
				attrEventVal:event.attr("attrEventVal"),
				attrImgSrc:event.attr("attrImgSrc"),
				value:event.attr("value"),
				attrI18n:event.attr("attrI18n"),
				attrColumn:event.attr("attrColumn")//显示列数
			};
		},
		/**
		 * 保存JSON格式
		 */
		setFormCustomContentJson:function(htmlDivId){
			var formCustomContentJson = [];
			$(htmlDivId).children("div").each(function(){
				formCustomContentJson.push(common.config.formCustom.formControlImpl.getAttrs($(this)));
			});
			return common.config.formCustom.formControlImpl.getFormValues(JSON.stringify(formCustomContentJson),htmlDivId);
		},
		/**
		 * 控件格式
		 */
		formCustomInit:function(htmlDivId){
			var eles = $(htmlDivId+" :input[attrType=Lob]");
			$.each(eles,function(index,value){
				//初始化请求内容描述富文本
				setTimeout(function(){
					$(value).parent().attr("class","field_lob");
					initCkeditor($(value).attr("id"),'Simple',function(){});
				},0);
			});
			$(htmlDivId+" :input[attrType=Integer]").bind("propertychange input",function(){
				$(this).val($(this).val().replace(/\D/g,''));
			});
			$(htmlDivId+" :input[attrType=Double]").bind("propertychange input",function(){
		        //先把非数字的都替换掉，除了数字和.
				$(this).val($(this).val().replace(/[^\d.]/g,""));
		        //必须保证第一个为数字而不是.
				$(this).val($(this).val().replace(/^\./g,""));
		        //保证只有出现一个.而没有多个.
				$(this).val($(this).val().replace(/\.{2,}/g,"."));
		        //保证.只出现一次，而不能出现两次以上
				$(this).val($(this).val().replace(".","$#$").replace(/\./g,"").replace("$#$","."));
			});
			DatePicker97($(htmlDivId+" :input[attrType=Date]"),"yyyy-MM-dd HH:mm:ss");
		},
		/**
		 * 控件格式
		 */
		formCustomInitByDataDictionaray:function(htmlDivId){
			$(htmlDivId+" :input[attrtype=DataDictionaray]").each(function(i,obj){
				common.config.dictionary.dataDictionaryUtil.loadOptionsByCode($(obj).attr("attrdatadictionary"),htmlDivId+' #'+$(obj).attr("id"),function(){
					$(obj).val($(obj).attr('val'));
				});
			});
		}
		
	}
}();
