$package('common.config.category');
$import('common.config.category.eventCategoryTree');
$import('common.config.category.CICategory');
$import('common.config.systemGuide.systemGuide');
/**  
 * @author QXY  
 * @constructor WSTO
 * @description 分类
 * @date 2010-11-17
 * @since version 1.0 
 */ 
common.config.category.eventCategory = function(){
	this.loadCategoryFlag="0";
	return {
			/**
			 * 根据模块确定公共元素属于哪个模块
			 * @param selector 公共元素
			 */
			full:function(selector){
				return $('#'+_categoryCode+"CategoryDiv_layout " + selector); 
			},
			/**
			 * @description 修改分类信息.
			 */
			updateCategory:function() {
				var eventId=$('#eventId_'+_categoryCode).val();
				if(eventId==null || eventId==''){
					msgAlert(i18n['msg_msg_pleaseChooseCategory'],'info');
				}else{
					if($("#eventName_"+_categoryCode).val().toLowerCase() == "null"){
						$("#eventName_"+_categoryCode).val("");
						msgAlert(i18n['err_categoryNameNotNull'],'info');
					}
					if(common.config.category.eventCategory.full('#editEventCategoryTree form').form('validate')){
                        
					    var editForm = common.config.category.eventCategory.full('#editEventCategoryTree form').serialize();
                        var _url = "event!update.action";
                        startProcess();
					    // 判断分类是否存在    
                        $.post('event!isCategoryExistdOnEdit.action', editForm, function(data){
                            if(data) {
                                msgAlert(i18n['err_categoryNameIsExisted'],'info');
                                endProcess();
                            } else {
        						
        						$.post(_url,editForm,
        							function (r) {
        							
        								common.config.category.eventCategoryTree.showCategoryTree( common.config.category.eventCategory.full("#eventCategoryTree"));
        								msgShow(i18n['msg_edit_successful'],'show');
        								/**保存向导状态*/
        								common.config.systemGuide.systemGuide.saveSystemGuide(_categoryCode+"Guide");
        								endProcess();
        							},
        							'json'
        						);
                            }
                        });
					}
				}
			},
			/**
			 * @description 新增子分类.
			 */
			addCategory:function() {
				var parentEventId=$('#parentEventId_'+_categoryCode).val();
				var rootId = common.config.category.eventCategory.full('#eventCategoryTree').find('li').length;
				if( (parentEventId==null || parentEventId=='') && rootId > 0){
					msgAlert(i18n['msg_msg_pleaseChooseParent'],'info');
				}else{
					if($("#addeventName_"+_categoryCode).val().toLowerCase() == "null"){
						$("#addeventName_"+_categoryCode).val("");
						msgAlert(i18n['err_categoryNameNotNull'],'info');
					}
					if(common.config.category.eventCategory.full('#addEventCategoryTree form').form('validate')){
					    var addForm = common.config.category.eventCategory.full('#addEventCategoryTree form').serialize();
					    // 判断分类是否存在    
					    startProcess();
					    $.post('event!isCategoryExisted.action', addForm, function(data){
		                    if(data) {
		                    	endProcess();
		                        msgAlert(i18n['err_categoryNameIsExisted'],'info');
		                    } else {
		                        if(rootId>0){
		                            $.post('event!save.action', addForm, function(r){
		                                 common.config.category.eventCategoryTree.showCategoryTree( common.config.category.eventCategory.full("#eventCategoryTree"));
		                                 common.config.category.eventCategory.full('#addEventCategoryTree [name="eventCategoryDto.eventName"]').val("");
		                                 common.config.category.eventCategory.full('#addEventCategoryTree [name="eventCategoryDto.eavId"]').val("");
		                                 common.config.category.eventCategory.full('#addEventCategoryTree [name="eventCategoryDto.scores"]').val("");
		                                 common.config.category.eventCategory.full('#addEventCategoryTree [name="eventCategoryDto.eventDescription"]').val("");
		                                 common.config.category.eventCategory.full('#addEventCategoryTree [name="eventCategoryDto.categoryCodeRule"]').val("");
		                                 msgShow(i18n['msg_add_successful'],'show');
		                                
		                                 /**保存向导状态*/
		                                 common.config.systemGuide.systemGuide.saveSystemGuide(_categoryCode+"Guide");
		                                 
		                                 endProcess();
		                                },'json'); 
		                        }else{
		                             $.post('event!save.action', addForm+"&eventCategoryDto.categoryRoot="+param, function(r){
		                                 common.config.category.eventCategoryTree.showCategoryTree( common.config.category.eventCategory.full("#eventCategoryTree"));
		                                 common.config.category.eventCategory.full('#addEventCategoryTree [name="eventCategoryDto.eventName"]').val("");
		                                 common.config.category.eventCategory.full('#addEventCategoryTree [name="eventCategoryDto.eavId"]').val("");
		                                 common.config.category.eventCategory.full('#addEventCategoryTree [name="eventCategoryDto.scores"]').val("");
		                                 common.config.category.eventCategory.full('#addEventCategoryTree [name="eventCategoryDto.eventDescription"]').val("");
		                                 common.config.category.eventCategory.full('#addEventCategoryTree [name="eventCategoryDto.categoryCodeRule"]').val("");
		                                 msgShow(i18n['msg_add_successful'],'show');
		                            
		                                 /**保存向导状态*/
	                                    common.config.systemGuide.systemGuide.saveSystemGuide(_categoryCode+"Guide");
	                                    
	                                    endProcess();
	                                },'json');
		                        }
		                    }
		                });
					}
				}
			},
			/**
			 * 分类导出 
			 */
			exportEventCategory:function(){
				$("#exportEventCategoryWindow_"+_categoryCode+" form").submit();
			},
			/**
			 * 选择导入文件框
			 */
			importEventCategory:function(){
				windows('importEventCategoryWindow_'+_categoryCode,{width:400});
			},
			/**
			 * @description 导入数据库
			 */
			importEventCategoryCsv:function(){
				$.ajaxFileUpload({
		            url:'event!importEventCategory.action',
		            secureuri:false,
		            fileElementId:'importEventCategoryFile_'+_categoryCode, 
		            dataType:'json',
		            success: function(data,status){
		            	if(data=="FileNotFound"){
							msgAlert(i18n['msg_dc_fileNotExists'],'error');
						}else if(data=="IOError"){
							msgAlert(i18n['msg_dc_importFailure'],'error');
						}else{
							msgAlert(i18n['msg_dc_importSuccess'].replace('N','<br>['+data+']<br>'),'show');
							common.config.category.eventCategoryTree.showCategoryTree( common.config.category.eventCategory.full("#eventCategoryTree"));
							$('#importEventCategoryWindow_'+_categoryCode).dialog('colse');
						}
		            	
		            }
		        });
			},
			/**
			 * @description  加载自定义表单下拉列表.
			 * @param select 自定义表单下拉列表Id
			 */
			loadFormToSelect:function(select){
				var url = "formCustom!findAllRequestFormCustom.action";
				$.post(url, function(res){
					$(select).html('');
					$('<option value="">--'+i18n.pleaseSelect+'--</option>').appendTo(select);
					$.each(res,function(index,formCustom){
						$("<option value='"+formCustom.formCustomId+"'>"+formCustom.formCustomName+"</option>").appendTo(select);
					});
				});
			},
			/**
			 * 初始化
			 * @private
			 */
			init:function(){
				 $("#evetCategoryLoading_"+_categoryCode).hide();
				 common.config.category.eventCategoryTree.showCategoryTree( common.config.category.eventCategory.full("#eventCategoryTree"));
				 $("#save_data_"+_categoryCode).click(common.config.category.eventCategory.updateCategory);	
				 $("#save_sub_"+_categoryCode).click(common.config.category.eventCategory.addCategory);	
				 common.config.category.CICategory.loadEavsToSelect("#edit_category_"+_categoryCode+",#add_category_"+_categoryCode);//加载列表
				 //common.config.category.eventCategory.loadFormToSelect("#edit_form_"+_categoryCode+",#add_form_"+_categoryCode);//加载列表
				 common.config.category.CICategory.loadEavsToSelect("#add_category_"+_categoryCode);//加载列表
				 common.config.category.eventCategory.loadFormToSelect("#edit_form_"+_categoryCode);//加载列表
				 common.config.category.eventCategory.loadFormToSelect("#add_form_"+_categoryCode);//加载列表
				 $("#evetCategoryContent_"+_categoryCode).show();
			}
	}
 }();
$(document).ready(common.config.category.eventCategory.init);