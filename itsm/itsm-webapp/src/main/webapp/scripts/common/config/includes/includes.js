$package('common.config.includes');
/**  
 * @author QXY  
 * @constructor Includes
 * @description "用于加载Includes文件JS主函数"
 * @date 2012-7-31
 * @since version 1.0 
 */ 
common.config.includes.includes=function(){
	//加载服务目录includes文件
	this._loadServiceDirectoryItemsIncludesFileFlag=false;
	//是否已加载面板数据详细信息页面
	this._loadDashboardDataInfoIncludesFileFlag=false;
	//分类
	this._loadCategoryIncludesFileFlag=false;
	//自定义过滤器
	this._loadCustomFilterIncludesFileFlag=false;
	//附件
	this._loadChooseAttachmentIncludesFileFlag=false;
	return{
		/**
		 * 加载服务目录includes文件
		 */
		loadServiceDirectoryItemsIncludesFile:function(){
			if(!_loadServiceDirectoryItemsIncludesFileFlag){
				$('#knowledge_html').load('common/config/includes/includes_selectServiceDirectoryItems.jsp',function(){
					$.parser.parse($('#knowledge_html'));	
				});
			}
			_loadServiceDirectoryItemsIncludesFileFlag=true;
		},
		/**
		 * 加载面板数据详细信息includes文件
		 */
		loadDashboardDataInfoIncludesFile:function(){
			if(!_loadDashboardDataInfoIncludesFileFlag){
				$('#dashboardDataInfo_html').load('common/config/includes/includes_dashboardDataInfo.jsp',function(){
					$.parser.parse($('#dashboardDataInfo_html'));
				});
			}
			_loadDashboardDataInfoIncludesFileFlag=true;
		},
		/**
		 * 加载分类选择includes文件
		 */
		loadCategoryIncludesFile:function(){
			if(!_loadCategoryIncludesFileFlag){
				$('#category_html').load('common/config/includes/includes_categorys.jsp',function(){
					$.parser.parse($('#category_html'));
				});
			}
			_loadCategoryIncludesFileFlag=true;
		},
		/**
		 * 加载自定义查询过滤器includes文件
		 */
		loadCustomFilterIncludesFile:function(){
			if(!_loadCustomFilterIncludesFileFlag){
				$('#customFilter_html').load('common/config/includes/includes_customFilter.jsp',function(){
					$.parser.parse($('#customFilter_html'));
				});
			}
			_loadCustomFilterIncludesFileFlag=true;
		},
		/**
		 * 加载选择附件窗口includes文件
		 */
		loadRelatedAttachmentIncludesFile:function(){
			if(!_loadChooseAttachmentIncludesFileFlag){
				$('#chooseAttachment_html').load('common/config/includes/includes_chooseAttachment.jsp',function(){
					$.parser.parse($('#chooseAttachment_html'));
				});
			}
			_loadChooseAttachmentIncludesFileFlag=true;
		},
		/**
		 * 初始化
		 * @private
		 */
		init:function(){
			
		}
	}
}();
$(function(){common.config.includes.includes.init});