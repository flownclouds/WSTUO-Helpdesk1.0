﻿$package('common.config.attachment');
$import('common.tools.file.attachIcon');
$import('common.config.includes.includes');
/**  
 * @fileOverview "选择已有附件"
 * @author Van  
 * @constructor WSTO
 * @description 选择已有附件
 * @date 2011-06-13
 * @version 1.0  
 * @since version 1.0 
 */
common.config.attachment.chooseAttachment=function(){
	this._showAttachmentId = '';
	this._dto='';
	return {
		 /**
		  * 显示已有附件列表
		  * @param id 附件列表ID
		  * @param dto 附件所属模块name
		  */
		 showAttachmentGrid:function(id,dto){
		    _showAttachmentId = id;
		    _dto=dto;
		    var _url="attachment!findattachmentPager.action";
		    var _moduleSelect={};
		    if(id.indexOf("request")>=0){
		    	$.extend(_moduleSelect,{'dto.type':'itsm.request'});
			}else if(id.indexOf("problem")>=0){
				$.extend(_moduleSelect,{'dto.type':'itsm.problem'});
			}else if(id.indexOf("change")>=0){
				$.extend(_moduleSelect,{'dto.type':'itsm.change'});
			}else if(id.indexOf("ConfigureItem")>=0){
				$.extend(_moduleSelect,{'dto.type':'itsm.configureItem'});
			}else if(id.indexOf("release")>=0){
				$.extend(_moduleSelect,{'dto.type':'itsm.release'});
			}else if(id.indexOf("Soft")>=0){
				$.extend(_moduleSelect,{'dto.type':'itsm.soft'});
			}else{
				$.extend(_moduleSelect,{'dto.type':'itsm.knowledge'});
			}
			if($("#attachmentGrid").html()!=''){
				$("#attachmentGrid").jqGrid('setGridParam',{url:_url,postData:_moduleSelect}).trigger('reloadGrid');
			}else{
				var params = $.extend({},jqGridParams, {
					url: _url,
					postData:_moduleSelect,
					colNames:[i18n['attachment_Name'],i18n['attachment_date'],i18n['attachment_size'],i18n['attachment_creator'],i18n['check'],'',''], 
					colModel:[
								{name:'attachmentName',width:200,formatter:common.config.attachment.chooseAttachment.nameClick},
								{name:'createTime',width:130,formatter:timeFormatter,align:'center'},
								{name:'size',width:100,align:'center',formatter:common.config.attachment.chooseAttachment.sizeFormat},
								{name:'creator',width:100,hidden:true,align:'center'},
								{name:'act', width:80,sortable:false,align:'center',formatter:function(cell,event,data){
									return '<div style="padding:0px">'+
									'<a href="javascript:common.config.attachment.chooseAttachment.confirmCheck(\''+(data.aid)+'\',\''+data.attachmentName+'\',\''+data.url+'\')" title="'+i18n['check']+'">'+
									'<img src="../images/icons/ok.png"/></a>'+
									'&nbsp;&nbsp;<a href="javascript:common.config.attachment.chooseAttachment.deleteAttachments(\''+(data.aid)+'\',\''+data.attachmentName+'\',\''+data.url+'\')" title="'+i18n.deletes+'">'+
									'<img src="../images/icons/delete.gif"/></a>'+
									'</div>';
								}},
								{name:'aid',width:60,hidden:true,align:'center',sortable:true},
								{name:'url',hidden:true}
							],
					jsonReader:$.extend({},jqGridJsonReader,{id:'aid'}),
					ondblClickRow:function(rowId){
						var rowData= $("#attachmentGrid").jqGrid('getRowData',rowId);  // 行数据  
						common.config.attachment.chooseAttachment.confirmCheck(rowData.aid,rowData.attachmentName,rowData.url);
					},
					sortname:'aid',
					multiselect:false,
					pager:'#attachmentGridPager'
				});
				$("#attachmentGrid").jqGrid(params);
				$("#attachmentGrid").navGrid('#attachmentGridPager',navGridParams);
				$("#attachmentGrid").navButtonAdd('#attachmentGridPager',{caption:"",buttonicon:"ui-icon-search",onClickButton:function(){
					windows('searchAttachment',{width:400});
					$("#attachment_common_dosearch").click(common.config.attachment.chooseAttachment.doSearch);
					$('#searchCreator').click(function(){
						common.security.userUtil.selectUser('#searchCreator','','','fullName','')
					}
				);
				},position:"last"});
				$("#t_attachmentGrid").css(jqGridTopStyles);
				$("#t_attachmentGrid").css("overflow","hidden");
				$("#t_attachmentGrid").append($('#attachmentGridToolbar').html());
			}
			windows('index_choose_attachment_window');
		},
		/**
		 * 
		 * @private
		 */
		
		/**
		 * @description 文件大小显示格式
		 * @param cell 当前列值
		 * @param options 选项值
		 * @param rowObject 行对象
		 */
		sizeFormat:function(cellvalue, options, rowObject){
			if((cellvalue/1024).toFixed(2)>1024){
				var num=common.config.attachment.chooseAttachment.usaFormat(((cellvalue/1024)/1024).toFixed(2));
				return  num+" MB";
			}else{
				var num=common.config.attachment.chooseAttachment.usaFormat((cellvalue/1024).toFixed(2));
				return  num+" KB";
			}
		
		},
		/**
		 * 文件大小显示规格
		 * @param num 大小
		 * @private
		 */
		usaFormat:function(num){
				num = num.toString().replace(/\$|\,/g,'');
			    if(isNaN(num))
			    num = "0";
			    sign = (num == (num = Math.abs(num)));
			    num = Math.floor(num*100+0.50000000001);
			    cents = num%100;
			    num = Math.floor(num/100).toString();
			    if(cents<10)
			    cents = "0" + cents;
			    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
			    num = num.substring(0,num.length-(4*i+3))+','+
			    num.substring(num.length-(4*i+3));
			    return (((sign)?'':'-') + num + '.' + cents);
		},
		
		/**
		 * @description 点击附件名称下载
		 * @param cellvalue 当前列值
		 * @param options 选项值
		 * @param rowObject 行对象
		 * @private
		 */
		nameClick:function(cellvalue, options, rowObject){
			var returnHtml="";
			returnHtml="<a target=_bank href='attachment!download.action?downloadAttachmentId="+rowObject.aid+"'>"+cellvalue+"</a>";
			return returnHtml;
		},
		/**
		 * @description 选中附件
		 * @param aid  编号
		 * @param attachmentName 附件名称
		 * @param url 路径
		 * @private
		 */
		confirmCheck:function(aid,attachmentName,url){
			$.post('attachment!findExist.action?aid='+aid+'&imagePath='+url,function(result){
				if(!result){
					msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_Attachment_does_not_exist'],function(){
						common.config.attachment.chooseAttachment.confirmCheckfollow(aid,attachmentName,url);
					});
				}else{
					common.config.attachment.chooseAttachment.confirmCheckfollow(aid,attachmentName,url);
				}
			});
		},
		/**
		 * 删除已有附件
		 * @param aid  编号
		 * @param attachmentName 附件名称
		 * @param fileurl 路径
		 * @private
		 */
		deleteAttachments:function(aid,attachmentName,fileurl){
			msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirmDelete'],function(){
				var eveUrl="eventAttachment!deleteByUrl.action";
				$.post(eveUrl,"filePath="+fileurl+"&&aid="+aid,function(res){
					if(res==="error"){
						msgShow(i18n.ERROR_DATA_CAN_NOT_DELETE,'show');
					}else{
						$("#attachmentGrid").trigger('reloadGrid');
					}
				});
			});
			
		},
		/**
		 * 选中附件
		 * @param aid  编号
		 * @param attachmentName 附件名称
		 * @param url 路径
		 * @private
		 */
		confirmCheckfollow:function(aid,attachmentName,url){
			var fileName=url.substring(url.lastIndexOf("/")+1);
			var iconUrl=common.tools.file.attachIcon.getIcon(fileName);
			var _showAttach=$("#"+_showAttachmentId+"_ID_"+aid);
			if(_showAttach.length>0){
					msgAlert(i18n['attachment_have'],'info');
			}else{
				if(_showAttachmentId=='request' || _showAttachmentId=='requestDetail'){
					if(_showAttachmentId=='request'){
						var eno=$('#requestEdit_eno').val();
						common.tools.event.eventAttachment.editEventAttachment(aid,'show_edit_request_attachment',eno,'itsm.request',attachmentName,true);
					}else{
						var eno=$('#requestDetails_requestNo').val();
						common.tools.event.eventAttachment.editEventAttachment(aid,'show_request_effectAttachment',eno,'itsm.request',attachmentName,true);
					}
				}else if(_showAttachmentId=='change' || _showAttachmentId=='changeDetail'){
					if(_showAttachmentId=='change'){
						var eno=$('#edit_changeEno').val();
						common.tools.event.eventAttachment.editEventAttachment(aid,'show_edit_change_attachment',eno,'itsm.change',attachmentName,true);
					}else{
						var eno=$('#changeInfo_eno').val();
						common.tools.event.eventAttachment.editEventAttachment(aid,'show_change_effectAttachment',eno,'itsm.change',attachmentName,true);
					}
				} else if(_showAttachmentId=='problem' || _showAttachmentId=='problemDetail'){
					if(_showAttachmentId=='problem'){
						var eno=$('#edit_problemEno').val();
						common.tools.event.eventAttachment.editEventAttachment(aid,'show_problem_edit_effectAttachment',eno,'itsm.problem',attachmentName,true);
					}else{
						common.tools.event.eventAttachment.editEventAttachment(aid,'show_problem_effectAttachment',problem_eno,'itsm.problem',attachmentName,true);
					}
				}else if(_showAttachmentId=='release' || _showAttachmentId=='releaseDetail'){
					if(_showAttachmentId=='release'){
						var eno=$('#editReleaseNo').val();
						common.tools.event.eventAttachment.editEventAttachment(aid,'show_edit_release_attachment',eno,'itsm.release',attachmentName,true);
					}else{
						var eno=$('#releaseDetails_eno').val();
						common.tools.event.eventAttachment.editEventAttachment(aid,'show_release_effectAttachment',eno,'itsm.release',attachmentName,true);
					}
				}else if(_showAttachmentId=='ConfigureItem' || _showAttachmentId=='ConfigureItemDetail'){
					if(_showAttachmentId=='ConfigureItem'){
						var eno=$('#editCiId').val();
						common.tools.event.eventAttachment.editAttachment(aid,'show_configureItemEdit_effectAttachment',eno,'itsm.configureItem',attachmentName,true);
					}else{
						var eno=$('#configureItem_CiNo').val();
						common.tools.event.eventAttachment.editAttachment(aid,'show_configureItemInfo_effectAttachment',eno,'itsm.configureItem',attachmentName,true);
					}
				}else{
						$('#'+_showAttachmentId).append("<div id='"+_showAttachmentId+"_ID_"+aid+"'>"+
								iconUrl+
								"<a href='attachment!download.action?downloadAttachmentId="+aid+"' target='_blank'>"+
								attachmentName+"</a>&nbsp;&nbsp;" +
								"<a href=javascript:common.config.attachment.chooseAttachment.deleteAttachement('"+aid+"')>"+
								i18n['deletes']+"</a>" +
								"<input type='hidden' name='"+_dto+"' value='"+aid+"' />"+
								"</div>");
				}
			}
			$('#index_choose_attachment_window').dialog('close');
		},
		/**
		 * 删除附件匹配元素
		 * @param aid 附件ID
		 */
		deleteAttachement:function(aid){
			msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirm_deleteAttachment'],function(){
				$('div#'+_showAttachmentId+'_ID_'+aid).remove(); // 删除匹配元素
			});
		},
		/**
		 * 搜索附件
		 */
		search:function(){
				var type="";
			 	if(_showAttachmentId.indexOf("request")>=0){
			 		type="itsm.request";
				}else if(_showAttachmentId.indexOf("problem")>=0){
					type="itsm.problem";
				}else if(_showAttachmentId.indexOf("change")>=0){
					type="itsm.change";
				}else if(_showAttachmentId.indexOf("ConfigureItem")>=0){
					type="itsm.configureItem";
				}else if(_showAttachmentId.indexOf("release")>=0){
					type="itsm.release";
				}else if(_showAttachmentId.indexOf("Soft")>=0){
					type="itsm.soft";
				}else{
					type="itsm.knowledge";
				}
				var _url = 'attachment!findattachmentPager.action';
				$("#allType").val(type);
				var sdata = $('#topForm').getForm();
				$.extend(sdata, {'dto.attachmentName' : $("#attachmentNameSearch").val()});
				var postData = $("#attachmentGrid").jqGrid("getGridParam", "postData");     
				$.extend(postData, sdata);
				
				$('#attachmentGrid').jqGrid('setGridParam',{page:1,url:_url}).trigger('reloadGrid');
		},
		/**
		 * 搜索附件
		 */
		doSearch:function(){
		 	if(_showAttachmentId.indexOf("request")>=0){
		 		$('#searchAttachmentType').val("itsm.request");
			}else if(_showAttachmentId.indexOf("problem")>=0){
				$('#searchAttachmentType').val("itsm.problem");
			}else if(_showAttachmentId.indexOf("change")>=0){
				$('#searchAttachmentType').val("itsm.change");
			}else if(_showAttachmentId.indexOf("ConfigureItem")>=0){
				$('#searchAttachmentType').val("itsm.configureItem");
			}else if(_showAttachmentId.indexOf("release")>=0){
				$('#searchAttachmentType').val("itsm.release");
			}else if(_showAttachmentId.indexOf("Soft")>=0){
				$('#searchAttachmentType').val("itsm.soft");
			}else{
				$('#searchAttachmentType').val("itsm.knowledge");
			}
			if($('#searchAttachment form').form('validate')){
				var startSize=$('#search_startSize').val();
				var endSize=$('#search_endSize').val();
				var start="";
				var end="";
				if(endSize!=""){
					end=Math.round(endSize*1024);//四舍五入
				}
				if(startSize!=""){
					start=Math.round(startSize*1024);
				}
				$('#startSizeHiedden').val(start);
				$('#endSizeHiedden').val(end);
				var _url = 'attachment!findattachmentPager.action';
				var sdata=$('#searchAttachment form').getForm();
				var postData = $("#attachmentGrid").jqGrid("getGridParam","postData");     
				$.extend(postData, sdata);
				$('#attachmentGrid').jqGrid('setGridParam',{page:1,url:_url,postData:postData}).trigger('reloadGrid');
			}
		},
		/**
		 * 初始化方法
		 */
		init:function(){
			common.config.includes.includes.loadRelatedAttachmentIncludesFile();
		}
	}
}();
$(document).ready(common.config.attachment.chooseAttachment.init);