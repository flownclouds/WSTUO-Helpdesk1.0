$package('common.config.category')
/**  
 * @author Van  
 * @constructor WSTO
 * @description 服务目录
 * @date 2010-11-17
 * @since version 1.0 
 */ 
common.config.category.serviceCatalog=function(){
	return {
		/**
		 * @description 查询服务目录 单选.
		 * @param nameId 名称元素
		 * @param hiddenId Id元素
		 * @param callback 回调函数
		 * @param attrId 扩展属性Id元素
		 */
		selectSingleServiceDirCallback:function(nameId,hiddenId,callback,formId){
			windows('selectServicesSingleDirDiv',{width:250,height:400});
			$("#selectServicesSingleDirTreeDiv").jstree({
				json_data:{
				    ajax: {url : "event!getCategoryTree.action",
					      data:function(n){
					    	  return {'num':0,'types': 'Service','parentEventId':n.attr ? n.attr("id").replace("node_",""):0};//types is in action
					      },
					      cache:false}
				},
				plugins : [ "themes", "json_data", "ui" ]
			}).bind("select_node.jstree",function(e,data){
				msgConfirm(i18n.msg_msg,'<br/>'+i18n.msg_changeForm,function(){
					common.config.category.serviceCatalog.selectSingleNodeCallback(data,nameId,hiddenId,callback,formId);
				});
			});
			
		},
		selectSingleNodeCallback:function(data,nameId,hiddenId,callback,formId){
			var obj = data.rslt.obj;
			var id=obj.attr('id');
			var name = obj.attr('cname');
			var _formId = obj.attr('formId');
			if(id==null){//校正orgNo
				id=1;
			}
			common.config.category.serviceCatalog.fillBySingleServiceDirs(id,name,nameId,hiddenId);
			if(_formId!=null && _formId!='' && _formId!="null" && _formId !=undefined){
				$(formId).val(_formId);
			}else{
				$(formId).val(0);
			}
			if(callback!=null && callback!=''){
				callback();
			}
		},
		/**
		 * @description 查询服务目录 单选.
		 * @param nameId 名称元素
		 */
		selectSingleServiceDir:function(nameId,hiddenId,formId){
			windows('selectServicesSingleDirDiv',{width:250,height:400});
			$("#selectServicesSingleDirTreeDiv").jstree({
				json_data:{
				    ajax: {url : "event!getCategoryTree.action",
					      data:function(n){
					    	  return {'num':0,'types': 'Service','parentEventId':n.attr ? n.attr("id").replace("node_",""):0};//types is in action
					      },
					      cache:false}
				},
				plugins : [ "themes", "json_data", "ui" ]
			}).bind("select_node.jstree",function(e,data){
				common.config.category.serviceCatalog.selectSingleNode(data,nameId,hiddenId,formId);
			});
			
		},		
		selectSingleNode:function(data,nameId,hiddenId,formId){
			var obj = data.rslt.obj;
			var id=obj.attr('id');
			var name = obj.attr('cname');
			if(id==null){//校正orgNo
				id=1;
			}
			var _formId = obj.attr('formId');
			if(_formId!=null && _formId!='' && _formId!="null" && _formId !=undefined){
				$(formId).val(_formId);
			}else{
				$(formId).val(0);
			}
			common.config.category.serviceCatalog.fillBySingleServiceDirs(id,name,nameId,hiddenId);
		},
		fillBySingleServiceDirs:function(id,name,nameId,hiddenId){
			$('#selectServicesSingleDirDiv').dialog('close');
			$(nameId).val(name);
			$(hiddenId).val(id);
		},		
		/**
		 * @description 查询服务目录.
		 * @param nameId 名称元素
		 */
		selectServiceDir:function(nameId){
			windows('selectServicesDirDiv',{width:250,height:400});
			$("#selectServicesDirTreeDiv").jstree({
				json_data:{
				    ajax: {url : "event!getCategoryTree.action",
					      data:function(n){
					    	  return {'num':0,'types': 'Service','parentEventId':n.attr ? n.attr("id").replace("node_",""):0};//types is in action
					      },
					      cache:false}
				},
				checkbox:{"tie_selection":false},
				plugins : [ "themes", "json_data", "checkbox", "ui" ]
			});
			$('#selectServicesDir_getServicesdNodes').unbind('click');
			$('#selectServicesDir_getServicesdNodes').click(function(){
				common.config.category.serviceCatalog.getSelectedServiceDirNodes(nameId);
			});
		},
		selectNode_index:function(e,data){
			var obj = data.rslt.obj;
			var _parent = data.inst._get_parent();
			if($vl(obj.attr('cname'))==="More..." && obj.attr('start')!=undefined){
				startProcess();
				$.ajax({
					type:"post",
					url :"event!getCategoryTree.action?num=0&start="+obj.attr('start')+"&types=Service&parentEventId="+(_parent.attr ? _parent.attr("id").replace("node_",""):0),
	                dataType: 'json',
	                cache:false,
					success:function(data0){
						var bool=false;
						$.each(data0, function(s, n) {
							$("#selectServicesDirTreeDiv").jstree("create_node", obj, "before", n
							).bind("select_node.jstree",common.config.category.eventCategoryTree.selectNode_index,function(e,data1){
								common.config.category.serviceCatalog.selectNode_index(e,data1);
							});
							bool=n.attr.bool;
						});
						obj.attr('start',parseInt(obj.attr('start'))+1);
						if(!bool){
							msgShow("没有更多了...",'show');
							obj.attr('cname',"NoMore");
							$(treePanel).jstree("remove", obj);
						}
						endProcess();
				    }
				});
			}else if($vl(obj.attr('cname'))==="NoMore"){
				msgShow("没有更多了...",'show');
			}
		},
		/**
		 * @description 获取已经选择的.
		 * @param nameId 名称元素
		 */
		getSelectedServiceDirNodes:function(nameId){
			$("#selectServicesDirTreeDiv").jstree("get_checked",false,true).each(function (i,n) { 
				 var node = jQuery(this); 
				 var id=node.attr('id');
				 if(id==null){//校正orgNo
					 id=1;
				 }
				 common.config.category.serviceCatalog.fillByServiceDirs(id,nameId);
			});
			
			$('#selectServicesDirDiv').dialog('close');
			
		},
		/**
		 * 填充被服务目录列表.
		 * @param id Id元素
		 * @param nameId name元素
		 */
		fillByServiceDirs:function(id,nameId){
		 var url="event!findSubCategorys.action?categoryId="+id;
			 $.post(url,function(res){
					for(var i=0;i<res.length;i++){
						if('#add_request_serviceDirectory_tbody'==nameId && $("#add_request_"+res[i].eventId).val()==undefined){
							$("<tr id=add_request_"+res[i].eventId+"><td>"+res[i].eventName+"</td><td>"+res[i].scores+"</td><td><a onclick=common.knowledge.knowledgeTree.serviceRm("+res[i].eventId+")>"+i18n['deletes']+"</a><input type=hidden name=requestDTO.serviceDirIds value="+res[i].eventId+"></td></tr>").appendTo(nameId);
						}else if('#edit_request_serviceDirectory_tbody' == nameId && $("#edit_request_"+res[i].eventId).val()==undefined){
							$("<tr id=edit_request_"+res[i].eventId+"><td>"+res[i].eventName+"</td><td>"+res[i].scores+"</td><td><a onclick=common.knowledge.knowledgeTree.serviceRm("+res[i].eventId+")>"+i18n['deletes']+"</a><input type=hidden name=requestDTO.serviceDirIds value="+res[i].eventId+"></td></tr>").appendTo(nameId);
						}else if('#add_change_serviceDirectory_tbody'==nameId && $("#add_change_"+res[i].eventId).val()==undefined){
							$("<tr id=add_change_"+res[i].eventId+"><td>"+res[i].eventName+"</td><td><a onclick=common.knowledge.knowledgeTree.serviceRm("+res[i].eventId+")>"+i18n['deletes']+"</a><input type=hidden name=changeDTO.serviceDirectoryIds value="+res[i].eventId+"></td></tr>").appendTo(nameId);
						}else if('#edit_change_serviceDirectory_tbody' == nameId && $("#edit_change_"+res[i].eventId).val()==undefined){
							$("<tr id=edit_change_"+res[i].eventId+"><td>"+res[i].eventName+"</td><td><a onclick=common.knowledge.knowledgeTree.serviceRm("+res[i].eventId+")>"+i18n['deletes']+"</a><input type=hidden name=changeDTO.serviceDirectoryIds value="+res[i].eventId+"></td></tr>").appendTo(nameId);
						}else if('#add_scheduledTask_serviceDirectory_tbody'==nameId && $("#add_scheduledTask_"+res[i].eventId).val()==undefined){
							$("<tr id=add_scheduledTask_"+res[i].eventId+"><td>"+res[i].eventName+"</td><td>"+res[i].scores+"</td><td><a onclick=common.knowledge.knowledgeTree.serviceRm("+res[i].eventId+")>"+i18n['deletes']+"</a><input type=hidden name=serviceDirNos value="+res[i].eventId+"></td></tr>").appendTo(nameId);
						}else if('#edit_scheduledTask_serviceDirectory_tbody' == nameId && $("#edit_scheduledTask_"+res[i].eventId).val()==undefined){
							$("<tr id=edit_scheduledTask_"+res[i].eventId+"><td>"+res[i].eventName+"</td><td>"+res[i].scores+"</td><td><a onclick=common.knowledge.knowledgeTree.serviceRm("+res[i].eventId+")>"+i18n['deletes']+"</a><input type=hidden name=serviceDirNos value="+res[i].eventId+"></td></tr>").appendTo(nameId);
						}else if('#addKnowledge_service_name'==nameId && $("#add_knowledge_"+res[i].eventId).val()==undefined){
							$("<tr id=add_knowledge_"+res[i].eventId+"><td>"+res[i].eventName+"</td><input type=hidden name='knowledgeDto.knowledServiceNo' value="+res[i].eventId+"><td><a onclick=common.knowledge.knowledgeTree.serviceRm("+res[i].eventId+")>"+i18n['deletes']+"</a><input type=hidden name=serviceDirNos value="+res[i].eventId+"></td></tr>").appendTo(nameId);
						}else if('#editKnowledge_service_name' == nameId && $("#edit_knowledge_"+res[i].eventId).val()==undefined){
							$("<tr id=edit_knowledge_"+res[i].eventId+"><td>"+res[i].eventName+"</td><input type=hidden name='knowledgeDto.knowledServiceNo' value="+res[i].eventId+"><td><a onclick=common.knowledge.knowledgeTree.serviceRm("+res[i].eventId+")>"+i18n['deletes']+"</a><input type=hidden name=serviceDirNos value="+res[i].eventId+"></td></tr>").appendTo(nameId);
						}else if('#search_service_name' == nameId && $("#search_knowledge_"+res[i].eventId).val()==undefined){
							$("<tr id=search_knowledge_"+res[i].eventId+"><td>"+res[i].eventName+"</td><input type=hidden name='knowledgeDto.knowledServiceNo' value="+res[i].eventId+"><td><a onclick=common.knowledge.knowledgeTree.serviceRm("+res[i].eventId+")>"+i18n['deletes']+"</a><input type=hidden name=serviceDirNos value="+res[i].eventId+"></td></tr>").appendTo(nameId);
						}else if('#edit_ci_serviceDirectory_tbody' == nameId && $("#edit_ci_"+res[i].eventId).val()==undefined){
							$("<tr id=edit_ci_"+res[i].eventId+"><td>"+res[i].eventName+"</td><td><a onclick=common.knowledge.knowledgeTree.serviceRm("+res[i].eventId+")>"+i18n['deletes']+"</a><input type=hidden name=ciDto.serviceDirectoryNos value="+res[i].eventId+"></td></tr>").appendTo(nameId);
						}else if('#add_ci_serviceDirectory_tbody' == nameId && $("#add_ci_"+res[i].eventId).val()==undefined){
							$("<tr id=add_ci_"+res[i].eventId+"><td>"+res[i].eventName+"</td><td><a onclick=common.knowledge.knowledgeTree.serviceRm("+res[i].eventId+")>"+i18n['deletes']+"</a><input type=hidden name=ciDto.serviceDirectoryNos value="+res[i].eventId+"></td></tr>").appendTo(nameId);
						}else if('#add_problem_serviceDirectory_tbody' == nameId && $("#add_problem_"+res[i].eventId).val()==undefined){
							$("<tr id=add_problem_"+res[i].eventId+"><td>"+res[i].eventName+"</td><td><a onclick=common.knowledge.knowledgeTree.serviceRm("+res[i].eventId+")>"+i18n['deletes']+"</a><input type=hidden name=problemDTO.serviceDirectoryNos value="+res[i].eventId+"></td></tr>").appendTo(nameId);
						}else if('#edit_problem_serviceDirectory_tbody'==nameId && $("#edit_problem_"+res[i].eventId).val()==undefined){
							$(nameId).append("<tr id=edit_problem_"+res[i].eventId+"><td>"+res[i].eventName+"</td><td><a onclick=common.knowledge.knowledgeTree.serviceRm("+res[i].eventId+")>"+i18n['deletes']+"</a><input type=hidden name=problemDTO.serviceDirectoryNos value="+res[i].eventId+"></td></tr>");
						}
					}
			  });
		}
	}
}();
