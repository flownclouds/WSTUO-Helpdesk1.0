$package('common.config.formCustom');
$import('basics.tab.tabUtils');
$import('common.config.formCustom.formControl');
$import('common.config.formCustom.formControlImpl');
$import('itsm.request.requestCommon');
$import('itsm.cim.cimCommon');
$import('common.security.base64Util');
/**
 * 表单设计
 * @author will
 */
common.config.formCustom.formCustomDesign=function(){

	this.dragg;//右边拖拽过来的对象
	this.saveMark=true;//标记是否可以添加
	this.loadshowAttrsGrid=false;
	var opt = "";
	var tabAttrNos = "";
	var tabAttrNos_merge="";
	return {
		/**
		 * 删除字段
		 */
		deleteField:function(event){
			var parent=$(event).parent().parent();
			var obj=parent.clone();
			obj.empty();
			obj.attr("class","right_options");
			obj.text(obj.attr("label"));
			var fieldType=obj.attr("fieldType");
			var fieldset='#defaultField';
			if(fieldType==='customField')
				fieldset='#customField';
			obj.removeAttr('style');
			obj.appendTo(fieldset+"_fieldset");
			parent.remove();
			
		},
		/**
		 * 右边拖拽完成时调用的方法
		 */
		dropComplete:function(){
			
			dragg.html(common.config.formCustom.formControlImpl.control(
					common.config.formCustom.formControlImpl.getAttrs(dragg)
					));
			if(dragg.attr("attrType")=="Lob")
				dragg.attr("class","field_options_lob field_options");
			else{
				if($("#column_select").val()==1){
					dragg.attr("class","field_options_2Column field_options");
					dragg.find('.field').attr("class","field_lob");
				}else if($("#column_select").val()==2){
					dragg.attr("class","field_options");
				}
			}
			if(dragg.attr("attrType")=="Checkbox" || dragg.attr("attrType")=="Radio"){
				common.config.formCustom.formCustomDesign.fieldResize(dragg);
				//dragg.height("30px");
			}
			dragg.css({'background-color':'#99CCCC','border-radius': '5px'});
			setTimeout(function(){
				dragg.css("background-color",'');
			},800);
			
		},
		
		saveFormCustom:function(sign){
			if(saveMark){
				startProcess();
				saveMark=false;
				var formCustomName = $("#formCustomName").val();
				if(formCustomName == ""){
					formCustomName = $("#update_formCustomName").val();
				}
				var formCustomId = $("#formCustomId").val();
				var customFormDesign_type = $("#formCustomType").val();
				var customFormDesign_show_border = document.getElementById("customFormDesign_show_border").checked;
				customFormDesign_show_border=customFormDesign_show_border?1:0;
				var customFormDesign_default_form = document.getElementById("customFormDesign_default_form").checked;
				if($("#column_select").val()==1){
					$('#dashboard_column div').attr("attrColumn","2");//一列显示，占两列
				}else
					$('#dashboard_column div').not("[attrColumn='9']").attr("attrColumn","1");
				//保存json格式
				var formCustomContentJson = common.config.formCustom.formControlImpl.setFormCustomContentJson('#dashboard_column');
				
				var url = 'formCustom!saveFormCustom.action';
				var eavNo = 0;
				if(formCustomEavNo !=null && formCustomEavNo != "null"){
					eavNo = formCustomEavNo;
				}
				var _param = {'formCustomDTO.formCustomId':formCustomId,
							'formCustomDTO.formCustomName':formCustomName,
							'formCustomDTO.eavNo':eavNo,
							'formCustomDTO.ciCategoryNo':formCustom_ciCategoryNo,
							'formCustomDTO.formCustomContents':formCustomContentJson,
							'formCustomDTO.isShowBorder':customFormDesign_show_border,
							'formCustomDTO.type':customFormDesign_type,
							'formCustomDTO.isDefault':customFormDesign_default_form
							};
				var frm =$.param($("#formCustom_tabsIdForm").serializeArray());
				if(frm!==""){
					url +="?"+frm;
				}
				$.post(url,_param, function(formCustomId){
					endProcess();
					$('#formCustomGrid').trigger('reloadGrid');
					if(sign===''){
						//关闭请求详情TAB
						basics.tab.tabUtils.closeTab(i18n.formDesigner);
						basics.tab.tabUtils.addTab(i18n.formCustom,"../pages/common/config/formCustom/formCustomMain.jsp");
					}else{
						$('#formCustomId').val(formCustomId);
					}
					saveMark=true;
					msgShow(i18n.addSuccess,'show');
				});
			}
		},
		/**
		 * @description 保存自定义表单
		 * */
		saveFormCustomEdit:function(sign){
			var formCustomId = $("#formCustomId").val();
			if(formCustomId=="" || formCustomId==0){
				common.config.formCustom.formCustomDesign.saveFormCustom(sign);
			}else{
				var formCustomName = $("#formCustomName").val();
				if(formCustomName == ""){
					formCustomName = $("#update_formCustomName").val();
				}
				var customFormDesign_type = $("#formCustomType").val();
				var _url = "formCustom!isFormCustomNameExistedOnEdit.action";
				var param = $.param({'formCustomQueryDTO.type':customFormDesign_type,
					"formCustomQueryDTO.formCustomName":formCustomName,
					"formCustomQueryDTO.formCustomId":formCustomId},true);
				$.post(_url, param, function(bool){
					if(bool){
						common.config.formCustom.formCustomDesign.saveFormCustom(sign);
					}else{
						msgAlert(i18n.msg_formCustomNameExist,"info");
					}
				});
			}
		},
		/**
		 * 查看是否包含必填字段
		 */
		isRequiredField : function(){
			var count = 0;
			var finalFied='requestDTO.companyName,requestDTO.etitle,requestDTO.edesc,requestDTO.createdByName';
			var label_div_list = $("#formField .field_options");
			$.each(label_div_list,function(label_div_i,label_div){
				var attrName=$(label_div).attr("attrName");
				if(finalFied.indexOf(attrName)>-1)
					count++;
			});
			return count;
		},
		/**
		 * 预览表单
		 */
		previewFormCustom:function(){
			var formCustomContents = $("#formField").html();
			var mark=false;//记录是否需要插入空白字段
			var column=0;
			$('#previewFormCustom').html(formCustomContents);
			$('#previewFormCustom .field_options').each(function(i,event){//加上空白自段
				var obj=$(event);
				if(obj.attr('attrColumn')=="2" || obj.attr('attrType')=="Lob"|| obj.attr('attrColumn')=="9"){
					if(mark){
						mark=false;
						column=0;
						obj.before('<div class="field_options"><div class="label"></div><div class="field"></div></div>');
					}
				}else{
					column++;
					if(column==1){//为偶数时
						mark=true;
					}else if(column==2){//为偶数时
						mark=false;
						column=0;
					}
				}
			});
			
			itsm.request.requestCommon.setDefaultParamValue("previewFormCustom");
			var eles = $("#previewFormCustom :input[attrType=Lob]");
			$.each(eles,function(index,value){
				var previewId =  $(value).attr("id");
				$(value).attr("id","previewFormCustom_"+previewId);//将编辑器的id改为add_scheduledTask_edesc
			});
			var eles = $("#previewFormCustom :input[attrType=DataDictionaray]");
			$.each(eles,function(index,value){
				var previewId =  $(value).attr("id");
				$(value).attr("id","previewFormCustom_"+previewId);//将编辑器的id改为add_scheduledTask_edesc
			});
			var eles = $("#previewFormCustom :input[attrType=Date]");
			$.each(eles,function(index,value){
				var previewId =  $(value).attr("id");
				$(value).attr("id","previewFormCustom_"+previewId);//将编辑器的id改为add_scheduledTask_edesc
			});
			$.parser.parse($('#previewFormCustom'));//加上这句，必填才有用
			$("#previewFormCustom .field_opt").remove();
			$("#previewFormCustom .field_options").css('cursor','auto');
	        $('#previewFormCustom_win').dialog({
                title: i18n.formCustom_preview,
                width: 880,
                height:600,
                modal: true
            });
	        var customFormDesign_show_border = document.getElementById("customFormDesign_show_border").checked;
	        var col = $("#column_select").val();
	        if(customFormDesign_show_border){
	        	$("#previewFormCustom_win").find(".ui-resizable").css("width","");
	        	$("#formCustomDesign_css").attr("href","../styles/common/formCustomDesign-preview.css");
	        	$("#previewFormCustom_win").find("div[class=field_options] div[class=label]:odd").css("border-left","none");
	        	if(col == 2){
		        	var arrys = new Array();
			        var count_element = $("#previewFormCustom > div > div").size();
					$.each($("#previewFormCustom > div > div"),function(ind,val){
						if($(this).attr("attrtype")==="Lob" || count_element === (ind+1)){
							$(this).css("border-left","1px solid #EEE");
							for(var i = 0 ; i < arrys.length ; i ++){
								if((i+1) % 2 != 0)
									$(arrys[i]).css("border-left","1px solid #EEE");
							}
							arrys.splice(0);
						}else{
							arrys.push(this);
						}
			        });
	        	}else{
	        		$.each($("#previewFormCustom > div > div"),function(ind,val){
						$(this).css("border-left","1px solid #EEE");
			        });
	        		$("#previewFormCustom :input").css("margin-top","10px");
	        		
	        		$.each($("#previewFormCustom").find("div[attrtype='Lob']"),function(ind,val){
	        			var width = $(this).children(":first").css("width");
	        			width = parseInt(width)+5.5;
	        			$(this).children(":first").css("width",width+"px");
	        		});
	        	}
	        }else{
	        	$("#formCustomDesign_css").attr("href","../styles/common/formCustomDesign-preview-no.css");
	        	if(col ==1 )
	        		$("#previewFormCustom :input").css("margin-top","10px");
	        }
	        common.config.formCustom.formControlImpl.formCustomInit('#previewFormCustom');
	        common.config.formCustom.formControlImpl.formCustomInitByDataDictionaray('#previewFormCustom');
	        
		},
		/**
		 * 布局初始化
		 */
		layoutInit : function(){
			/*$(".column").droppable({
				accept : ".right_options",
				activeClass: "ui-state-highligh",
				hoverClass: "ui-custom-active",
				addClasses:false,
				tolerance:'intersect',
				//tolerance:'pointer',
				drop : function(event, ui) {
					dragg=ui.draggable;//拖拽的对象
					common.config.formCustom.formCustomDesign.dropComplete();
				}
			});*/
			$('#customField_fieldset,#defaultField_fieldset').sortable({
				connectWith: ".column",
				stop : function(event, ui) {
					$(".column").removeClass("ui-custom-active");
				}
			}).disableSelection();

			$('.column').sortable({
				connectWith: ".column",
				over : function(event, ui) {
					$(".column").addClass("ui-custom-active");
				},
				receive: function(event, ui) {
					dragg=ui.item;//拖拽的对象
					common.config.formCustom.formCustomDesign.dropComplete();
				},
				stop : function(event, ui) {
					$(".column").removeClass("ui-custom-active");
				}
			}).disableSelection();

			common.config.formCustom.formCustomDesign.resizeWith();
		},
		/**
		 * 加载扩展字段
		 */
		loadCustomField:function(formCustomEavNo){
			if(formCustomEavNo && formCustomEavNo!="" && formCustomEavNo != "null"){
				common.config.formCustom.formCustomDesign.fillCustomField_fieldset(formCustomEavNo);
			}else if(formCustom_ciCategoryNo && formCustom_ciCategoryNo!="" && formCustom_ciCategoryNo != "null"){
				common.config.formCustom.formCustomDesign.fillCustomField_fieldset_cim(formCustom_ciCategoryNo);
			}
		},
		fillCustomField_fieldset_cim:function(formCustom_ciCategoryNo){
			var url = "ciCategory!findCICategoryById.action?categoryNo="+formCustom_ciCategoryNo;
			$.post(url,function(data){
				formCustomEavNo=data.categoryType;
				var _param = $.param({"attrsQueryDTO.eavNo":data.categoryType},true);
				$.post("attrs!findAttributesByEavNo.action",_param,function(eavData){
					$("#customField_fieldset").empty();
					$.each(eavData,function(ind,val){
						//fieldType 字段类型，判断是扩展字段还是基础字段
						var event=$('<div class="right_options" title="'+val.attrAsName+'" >'+val.attrAsName+'</div>');
						event.attr({
							fieldType:"customField",
							attrType:val.attrType,
							label:val.attrAsName,
							attrNo:val.attrNo,
							attrName:"ciDto.attrVals[\'"+val.attrName+"\']",
							required:val.attrIsNull,
							attrdataDictionary:val.attrdataDictionary,
							attrItemName:val.attrItemName,
							attrColumn:"1"//一列显示
						});
						event.prependTo("#customField_fieldset");
					});
					common.config.formCustom.formCustomDesign.removeField();
				});
			});
		},
		fillCustomField_fieldset:function(eavNo){
			var url="attrs!findAttributesByEavNo.action?attrsQueryDTO.eavNo="+eavNo;
			$.post(url,function(data){
				$("#customField_fieldset").empty();
				if(data!=null){
					$.each(data,function(ind,val){
						//fieldType 字段类型，判断是扩展字段还是基础字段
						var event=$('<div class="right_options" title="'+val.attrAsName+'" >'+val.attrAsName+'</div>');
						event.attr({
							fieldType:"customField",
							attrType:val.attrType,
							label:val.attrAsName,
							attrNo:val.attrNo,
							attrName:"requestDTO.attrVals[\'"+val.attrName+"\']",
							required:val.attrIsNull,
							attrdataDictionary:val.attrdataDictionary,
							attrItemName:val.attrItemName,
							attrColumn:"1"//一列显示
						});
						event.prependTo("#customField_fieldset");
					});
				}
				common.config.formCustom.formCustomDesign.removeField();
			});
		},
		/**
		 * 查询相似的自定义表单
		 */
		findSimilarFormCustom : function(){
			$("#similarFormCustom").empty();
			$("#similarFormCustom").append("<option value=''>--"+i18n.pleaseSelect+"--</option>");
			if(formCustomType=="request"){
				$("#similarFormCustom").append("<option value='defaultField'>"+i18n.defaultForm+"</option><option value='basicField'>"+i18n.basicForm+"</option>");
				var url = 'formCustom!findSimilarFormCustom.action';
				var param ={"formCustomQueryDTO.eavNo":formCustomEavNo,"formCustomQueryDTO.type":formCustomType};
				$.post(url, param, function(data){
					$.each(data,function(index,formCustom_obj){
						$("#similarFormCustom").append("<option value='"+formCustom_obj.formCustomId+"'>"+formCustom_obj.formCustomName+"</option>");
					});
				});
			}else if(formCustomType=="ci")
				$("#similarFormCustom").append("<option value='cibasicField'>"+i18n.defaultForm+"</option>");
		},
		/**
		 * 获取模板的值
		 */
		getTemplateValue : function(value){
			if(value!==''){
				$("#dashboard_column").empty();
				if(value.indexOf('Field')>-1){
					$("#dashboard_column").load('common/config/formCustom/'+value+'.jsp',function(){
						//$.parser.parse($('#dashboard_column'));
						common.config.formCustom.formCustomDesign.removeField_do('defaultField_list_'+formCustomType);
						common.config.formCustom.formCustomDesign.editTextarea();
						$("#column_select").val("2");
						$("#customFormDesign_show_border").attr("checked","");
						$("#customFormDesign_default_form").attr("checked","");
					});
					//common.config.formCustom.formCustomDesign.layoutInit();
				}else{
					var url = 'formCustom!findFormCustomById.action';
					var _param = {"formCustomId" : value};
					$.post(url,_param,function(data){
						if(data!=null){
							if(data.isShowBorder==="1")
								$("#customFormDesign_show_border").attr("checked",true);
							else
								$("#customFormDesign_show_border").attr("checked",false);
							if(data.isDefault)
								$("#customFormDesign_default_form").attr("checked",true);
							else
								$("#customFormDesign_default_form").attr("checked",false);
							$("#column_select").val("2");
							//$("#formCustomId").val(data.formCustomId);
							$('#formCustomName').val(data.formCustomName);
							var formCustomContents = common.config.formCustom.formControlImpl.designHtml(data.formCustomContents);
							$("#dashboard_column").html(formCustomContents);
							if($("#dashboard_column > div[attrno=request_companyName]").hasClass("field_options_2Column")){
								$("#column_select").val("1");
							}else
								$("#column_select").val("2");
							$.each($("#formField div[attrtype=Radio],#formField div[attrtype=Checkbox]"),function(ind,obj){
								setTimeout(function(){
									common.config.formCustom.formCustomDesign.fieldResize(obj);
								},0);
							});
							common.config.formCustom.formCustomDesign.removeField_do('defaultField_list_'+formCustomType);
							common.config.formCustom.formCustomDesign.layoutInit();
						}
					});
				}
			}else if(formCustomType=="ci")
				common.config.formCustom.formCustomDesign.find();
			
		},
		showFieldMenu : function (menuId,event) {
            if ($(menuId).is(':hidden')) {
            	$(event).children("span").text('--');
            	$(menuId).show();
            }else{
            	$(event).children("span").text('+');
            	$(menuId).hide();
            }
        },
		/**
		 * 从左边拖放字段移除右边表单设计器里面有的字段
		 */
		removeField : function(attrNos){
			var label_div_list = $("#formField .field_options");
			$.each(label_div_list,function(label_div_i,label_div){
				var attrno=$(label_div).attr("attrno");
				$("#fromCustomCenter_right .right_options[attrno='"+attrno+"']").remove();
			});
			common.config.formCustom.formCustomDesign.removeByTabsAttrId(tabAttrNos);
		},
		
		removeField_do : function(daultFieldList){
			$("#defaultField_fieldset div").remove();
			$("#customField_fieldset div").remove();
			$("#defaultField_fieldset").html('');
			$("#defaultField_fieldset").append($("#"+daultFieldList).html());
			common.config.formCustom.formCustomDesign.loadCustomField(formCustomEavNo);
			var div_list = $("#defaultField_fieldset div").addClass("right_options");
			common.config.formCustom.formCustomDesign.removeField();
		},
		
		
		
		
		/**
		 * 查询要修改的自定义表单数据
		 */
		find:function(){
			var _param = {"formCustomId" : formCustomId};
			$.post('formCustom!findFormCustomById.action',_param,function(data){
				if(data!=null){
					if(data.type=="request"){
						$("#similarFormCustom").val(formCustomId);
						$("#formCustom_eavName").html(data.eavName);
						$("#customFormDesign_eavName_span").show();
						$("#customFormDesign_ciCategoryName_span").hide();
					}else{
						$("#formCustomDesign_ciCategoryName").html(data.ciCategoryName);
						$("#customFormDesign_eavName_span").hide();
						$("#customFormDesign_ciCategoryName_span").show();
					}
						
					if(data.isShowBorder==="1")
						$("#customFormDesign_show_border").attr("checked",true);
					else
						$("#customFormDesign_show_border").attr("checked",false);
					if(data.isDefault)
						$("#customFormDesign_default_form").attr("checked",true);
					else
						$("#customFormDesign_default_form").attr("checked",false);
					$("#formCustomId").val(data.formCustomId);
					$('#update_formCustomName').val(data.formCustomName);
					$("#formCustom_eavNo").val(data.eavNo);
					
					var formCustomContents = common.config.formCustom.formControlImpl.designHtml(data.formCustomContents);
					$("#dashboard_column").empty().html(formCustomContents);
					
					if($('#dashboard_column .field_options .field').length==0)
						$("#column_select").val(1);
					$.each($("#formField div[attrtype=Radio],#formField div[attrtype=Checkbox]"),function(ind,obj){
						setTimeout(function(){
							common.config.formCustom.formCustomDesign.fieldResize(obj);
						},0);
					});
					if(data.ciCategoryNo!=null&&data.ciCategoryNo!=0){
						$.post('ciCategory!findByCategoryId.action','categoryNo='+data.ciCategoryNo,function(data){
							formCustomEavNo=data;
						});
					}else{
						formCustomEavNo = data.eavNo;
					}
					common.config.formCustom.formCustomDesign.layoutInit();
					common.config.formCustom.formCustomDesign.removeField();
					var tabsDtos = data.formCustomTabsDtos;
					for ( var i = 0; i < tabsDtos.length; i++) {
						var tabId =tabsDtos[i].tabId;
						var tabName =tabsDtos[i].tabName;
						if($('#formCustom_tabsIdForm #tabsId_'+tabId).length==0){
							$('#formCustom_tabsIdForm').append(
									'<div id="tabsId_'+tabId+'">'+
									'<input name="formCustomDTO.tabsIds" type="hidden" value="'+tabId+'" />'+
									'<div>');
						}
						if($('#tabsField_fieldset #tabsField_fieldset_'+tabId).length!=0){
							$('#tabsField_fieldset #tabsField_fieldset_'+tabId).remove();
						}
						$("#tabsField_fieldset").append("<div id='tabsField_fieldset_"+tabId+"' class='right_options_tabs' ondblclick='common.config.formCustom.formCustomDesign.tabOndbclick("+tabId+")'><div style='float: left;width:80%'>"+tabName+"</div>" +
								"<div class='deleteTabs'><span><a onclick='common.config.formCustom.formCustomDesign.deleteTabs(this,"+tabId+");'>" +
								"<img src='../skin/default/images/grid_delete.png'></a><span></div></div>");
						tabAttrNos += tabsDtos[i].tabAttrNos+",";
					}
				}
				common.config.formCustom.formCustomDesign.editTextarea();
			});
		},
		removeByTabsAttrId:function(attrnos){
			var nos = attrnos.split(',');
			for ( var i = 0; i < nos.length; i++) {
				$("#fromCustomCenter_right .right_options[attrno='"+nos[i]+"']").remove();
			}
		},
		/**
		 * init div resieze
		 */
		fieldResize:function(obj){
			var fieldwidth=parseInt($("#formField").width());
			fieldwidth= fieldwidth/2;
			$(obj).resizable({
				handles : 'e',
				maxWidth : fieldwidth*2,
				minWidth : fieldwidth,
			    grid: fieldwidth,
			    create : function( event, ui ){
					if($(obj).attr("attrColumn")==="1")
						$(obj).width(fieldwidth);
					else
						$(obj).width( parseInt(fieldwidth)*2);
			    },
			    start : function( event, ui ){
			    	ui.element.css({'background-color':'#99CCCC','border-radius': '5px'});
			    },
			    stop : function( event, ui ){
			    	var columnWidth=$("#dashboard_column").width();
			    	var width=ui.element.width();
			    	if(width>columnWidth*0.5)
			    		ui.element.attr("attrColumn","9");//标记单选和复选值
			    	else
			    		ui.element.attr("attrColumn","1");
			    	setTimeout(function(){
			    		ui.element.css("background-color",'');
					},666);
			    	ui.element.height('auto');
			    }
			});
		    
		},
		editTextarea : function(){
			var textareaWidth = $("textarea").width();
			var field_options_lob_divWidth = $("textarea").parent().parent().width();
			var label_divWidth = $("textarea").parent().prev("div").width();
			if(field_options_lob_divWidth-label_divWidth<textareaWidth)
				$("textarea").css({"width":(field_options_lob_divWidth-label_divWidth-100),"resize":"none"});
		},
		resizeWith : function(){
			var width = browserProp.browserProp.getWinWidth();
			if(width<1200){
				//$("#formCustom_layout").css({"width":1060});/**,"overflow-x":"scroll"*/
				$("#formCustom_layout").closest(".panel-body").css({"overflow-x":"auto"});
				 
				$("#formCustom_layout,#formCustom_layout > .panel,#formCustom_layout > .panel > .panel-body").css({"width":1060});
			}
		},
		columnSelect : function(col){
			if(col==1){
				$("#dashboard_column .field_options").not(".field_options_lob").attr("class","field_options_2Column field_options");
				$("#dashboard_column .field_options .field").attr("class","field_lob");
			}else if(col==2){
				$("#dashboard_column .field_options").not(".field_options_lob").attr("class","field_options");
				$("#dashboard_column .field_options .field_lob").attr("class","field");
			}
		},
		showEavTabs:function(){
			opt="save";
			$("#tabs_id").val("");
			$("#tabs_name").val("");
			$("#tabAttrsConentJson").val("");
			$("#tabAttrsDecode").val("");
			$("#tabAttrsConentDecode").val("");
			$("#selectedTabAttrs").html("");
			$("#tabAttrsConent").html("");
			windows('tabsField_win', { width : 450 });
		},
		/**
		 * @description 国际化属性类型
		 * @parma value 属性字段类型
		 */
		attrTypeForma:function(value){
			if(value=='Lob')
				return i18n.longText;
			if(value=='Integer')
				return i18n.integer;
			if(value=='Double')
				return i18n.decimal;
			if(value=='Date')
				return i18n.date;
			if(value=='Radio')
				return i18n.radio;
			if(value=='Checkbox')
				return i18n.checkbox;
			if(value=='DataDictionaray'){
				return i18n.eav_realdatadictionary;
			}
			else
				return i18n.text;
		},  
		/**
		 * @description 国际化数据字典类型
		 * @parma value 数据字典dname
		 */
		attrDataDicForma:function(value){ 
			if(value!==""){
				var name="label_dc_"+value;      
				return i18n[name];           
			}
			return "";
		},     
		isNullForma:function(cellValue,options,rowObject){
			if(rowObject.attrIsNull){
				return i18n.no;
			}else{
				return i18n.yes;
			}
		},
		selectTabsAttrCustomField:function(tabsField_attrNos){
			var params = $.extend({},jqGridParams, {
				url:'attrs!find.action?attrsQueryDTO.attrNos='+tabsField_attrNos,  
				colNames:[i18n.name,i18n.label_beLongAttrGroup,'',i18n.type,i18n.eav_realdatadictionary,'','',i18n.eav_required,'',i18n.length,'','',i18n.sort,'',''],
				colModel:[
                        {name:'attrAsName',width:50,align:'center',sortable:false},
						{name:'attrGroupName',width:50,index:'attrGroup',align:'center',sortable:false},
						{name:'attrGroupNo',width:50,hidden:true},
						{name:'attrType',width:30,align:'center',formatter:common.config.formCustom.formCustomDesign.attrTypeForma,sortable:false},
						{name:'attrdataDictionaryFormatter',width:30,align:'center',formatter:common.config.formCustom.formCustomDesign.attrDataDicForma,sortable:false,hidden:true},  
						{name:'attrdataDictionary',width:30,hidden:true}, 
						{name:'attrType',width:30,hidden:true},
						{name:'attrIsNullFormatter',width:30,align:'center',formatter:common.config.formCustom.formCustomDesign.isNullForma,sortable:false},
						{name:'attrIsNull',width:30,hidden:true},
						{name:'length',width:20,align:'center',hidden:true},
						{name:'attrNo',hidden:true},
						{name:'attrCode',width:30,hidden:true},
						{name:'sortNo',width:30,hidden:false,align:'center',sortable:false},      
						{name:'attrName',hidden:true},
						{name:'attrItemName',hidden:true}
                        ],
				jsonReader: $.extend(jqGridJsonReader, {id: "attrNo"}),
				sortname:'attrNo',
				toolbar:true,
				pager:'#tabsAttr_customFieldPager'
				});
			
				$("#tabsAttr_customFieldList").jqGrid(params);
				$("#tabsAttr_customFieldList").navGrid('#tabsAttr_customFieldPager',navGridParams);
				
				$('#tabsAttr_customFieldGrid_select').click(function(){
					common.config.formCustom.formCustomDesign.selectTabsAttrGrid();
				});
				
		},
		selectTabsAttrGrid:function(){
			var rowIds = $("#tabsAttr_customFieldList").getGridParam('selarrrow');
			if(rowIds=='' || rowIds.length<=0){
				msgAlert(i18n['msg_atLeastChooseOneData'],'info');
			}else{
				for ( var i = 0; i<rowIds.length; i++) {
					var data=$("#tabsAttr_customFieldList").getRowData(rowIds[i]);
					if($('#selectedTabAttrs #tabAttrsId_'+data.attrNo).length==0){
						$('#selectedTabAttrs').append('<div id="tabAttrsId_'+data.attrNo+'">'+data.attrAsName+
								'<input name="tabAttrsId" type="hidden" value="'+data.attrNo+'" />'+
								'&nbsp;&nbsp;&nbsp;<a style="color:red;" onclick="common.config.formCustom.formCustomDesign.selectedTabAttrRemove(\''+data.attrNo+'\')">X</a>'+
								'<div>');
						var event = common.config.formCustom.formCustomDesign.attrEvent(data);
						event.prependTo("#tabAttrsConent");
					}
				}
				$('#tabsAttrDiv').dialog('close');
			}
		},
		attrEvent:function(val){
			var event=$('<div class="right_options" title="'+val.attrAsName+'" >'+val.attrAsName+'</div>');
			event.attr({
				fieldType:"customField",
				attrType:val.attrType,
				label:val.attrAsName,
				attrNo:val.attrNo,
				attrName:"ciDto.attrVals[\'"+val.attrName+"\']",
				required:val.attrIsNull,
				attrdataDictionary:val.attrdataDictionary,
				attrItemName:val.attrItemName,
				attrColumn:"1"//一列显示
			});
			return event;
		},
		selectedTabAttrRemove:function(id){
			$('#tabAttrsId_'+id).remove();
			$("#tabAttrsConent .right_options[attrno='"+id+"']").remove();
		},
		tabAttrsSave:function(){
			var tabAttrsConentJson = common.config.formCustom.formControlImpl.setFormCustomContentJson('#tabAttrsConent');
			var tabAttrsDecode = common.security.base64Util.encode($("#selectedTabAttrs").html());
			var tabAttrsConentDecode =common.security.base64Util.encode($("#tabAttrsConent").html());
			$("#tabAttrsConentJson").val(tabAttrsConentJson);
			$("#tabAttrsDecode").val(tabAttrsDecode);
			$("#tabAttrsConentDecode").val(tabAttrsConentDecode);
			var frm = $('#tabsField_form').serialize();
			var tabName = $("#tabs_name").val();
			if(!tabName || $('#tabAttrsConent div').length==0){
				msgShow(i18n.msg_tabs_name_filed_notnull,'show');
				return false;
			}
			var url = "formCustomTabs!saveFormCustomTabs.action";
			if(opt=="merge"){
				url = "formCustomTabs!updateFormCustomTabs.action";
			}
			$.post(url,frm, function(res){
				if(res){
					if($('#formCustom_tabsIdForm #tabsId_'+res).length==0){
						$('#formCustom_tabsIdForm').append(
								'<div id="tabsId_'+res+'">'+
								'<input name="formCustomDTO.tabsIds" type="hidden" value="'+res+'" />'+
								'<div>');
					}
					if($('#tabsField_fieldset #tabsField_fieldset_'+res).length!=0){
						$('#tabsField_fieldset #tabsField_fieldset_'+res).remove();
					}
					$("#tabsField_fieldset").append("<div id='tabsField_fieldset_"+res+"' class='right_options_tabs' ondblclick='common.config.formCustom.formCustomDesign.tabOndbclick("+res+")'><div style='float: left;width:80%'>"+tabName+"</div>" +
							"<div class='deleteTabs'><span><a onclick='common.config.formCustom.formCustomDesign.deleteTabs(this,"+res+");'>" +
							"<img src='../skin/default/images/grid_delete.png'></a><span></div></div>");
					$('#tabsField_win').dialog('close');
					common.config.formCustom.formCustomDesign.removeByTabsField();
					if(opt=="merge"){
						common.config.formCustom.formCustomDesign.addByTabsField();
					}
				}
			});
			
		},
		//将修改的TabsField重新加入
		addByTabsField:function(){
			var nos = tabAttrNos_merge+",";
			$("#tabsField_form input[name='tabAttrsId']").each(function(){
				 var no = $(this).val()+',';
				 nos = nos.replace(no,'');
			});
			nos=nos.substring(0,nos.length-1);
			if(nos!==""){
				$.post("attrs!find.action?attrsQueryDTO.attrNos="+nos,"",function(eavData){
					$.each(eavData.data,function(ind,val){
						//fieldType 字段类型，判断是扩展字段还是基础字段
						var event=$('<div class="right_options" title="'+val.attrAsName+'" >'+val.attrAsName+'</div>');
						event.attr({
							fieldType:"customField",
							attrType:val.attrType,
							label:val.attrAsName,
							attrNo:val.attrNo,
							attrName:"ciDto.attrVals[\'"+val.attrName+"\']",
							required:val.attrIsNull,
							attrdataDictionary:val.attrdataDictionary,
							attrItemName:val.attrItemName,
							attrColumn:"1"//一列显示
						});
						event.prependTo("#customField_fieldset");
					});
				});
			}
		},
		removeByTabsField:function(){
			var tabAttrsId = $("#selectedTabAttrs input[name='tabAttrsId']");
			$.each(tabAttrsId,function(index,label_div){
				var attrno=$(label_div).val();
				$("#fromCustomCenter_right .right_options[attrno='"+attrno+"']").remove();
			});
		},
		tabOndbclick:function(tabId){
			opt ="merge";
			var url = "formCustomTabs!findFormCustomTabsByTabsId.action";
			$.post(url,"tabsId="+tabId, function(data){
				$("#tabs_id").val(data.tabId);
				$("#tabs_name").val(data.tabName);
				tabAttrNos_merge = data.tabAttrNos;
				$("#selectedTabAttrs").html(common.security.base64Util.decode(data.tabAttrsDecode));
				$("#tabAttrsConent").html(common.security.base64Util.decode(data.tabAttrsConentDecode));
				windows('tabsField_win', { width : 450 });
			});
		},
		deleteTabs:function(event,res){
			$("#tabsId_"+res).remove();
			var parent=$(event).parent().parent().parent();
			parent.remove();
			var url = "formCustomTabs!findFormCustomTabsByTabsId.action";
			$.post(url,"tabsId="+res, function(data){
				$.post("attrs!find.action?attrsQueryDTO.attrNos="+data.tabAttrNos,"",function(eavData){
					$.each(eavData.data,function(ind,val){
						//fieldType 字段类型，判断是扩展字段还是基础字段
						var event=$('<div class="right_options" title="'+val.attrAsName+'" >'+val.attrAsName+'</div>');
						event.attr({
							fieldType:"customField",
							attrType:val.attrType,
							label:val.attrAsName,
							attrNo:val.attrNo,
							attrName:"ciDto.attrVals[\'"+val.attrName+"\']",
							required:val.attrIsNull,
							attrdataDictionary:val.attrdataDictionary,
							attrItemName:val.attrItemName,
							attrColumn:"1"//一列显示
						});
						event.prependTo("#customField_fieldset");
					});
				});
			});
		},
		findTabsFieldAttrNos:function(){
			var attrNos =[];
			var label_div_list = $("#customField_fieldset .right_options");
			$.each(label_div_list,function(label_div_i,label_div){
				var attrno=$(label_div).attr("attrno");
				attrNos.push(attrno)
			});
			return attrNos;
		},
		init:function(){
			$("#customFormDesign_eavName_span").hide();
			$("#customFormDesign_ciCategoryName_span").hide();
			common.config.formCustom.formCustomDesign.editTextarea();
			common.config.formCustom.formCustomDesign.loadCustomField(formCustomEavNo);
			if(formCustomType=="ci"){
				$("#tabsField").show();
			}else{
				$("#tabsField").hide();
			}
			if(formCustomId!=""){
				$("#updateOperate").show();
				$("#defaultField_fieldset").append($("#defaultField_list_"+formCustomType).html());
				$("#defaultField_fieldset div").addClass("right_options");
				common.config.formCustom.formCustomDesign.find();
			}else{
				if(formCustomType=="ci"){
					$("#dashboard_column").load('common/config/formCustom/ciDefaultField.jsp',function(){});
					$("#customFormDesign_eavName_span").hide();
					$("#customFormDesign_ciCategoryName_span").show();
				}else{
					$("#dashboard_column").load('common/config/formCustom/defaultField.jsp',function(){});
					$("#customFormDesign_eavName_span").show();
					$("#customFormDesign_ciCategoryName_span").hide();
				}
				$("#formCustom_eavName").html(formCustomEavName);
				$("#formCustomDesign_ciCategoryName").html(formCustom_ciCategoryName);
				common.config.formCustom.formCustomDesign.layoutInit();
			}
			common.config.formCustom.formCustomDesign.findSimilarFormCustom();
			
			if(formCustom_ciCategoryNo && formCustom_ciCategoryNo!="" && formCustom_ciCategoryNo != "null"){
				$("#customFormDesign_default_span").hide();
			}
			
			$(document).keydown(function(event){
				//Ctrl+S 保存事件
				if($('#itsmMainTab').tabs('getSelected').panel('options').title===i18n.formDesigner){//当获取焦点时才执行
					if(event!==null && event!==undefined){
						if (event.ctrlKey == true && event.keyCode == 83) {
							event.returnvalue = false;
							common.config.formCustom.formCustomDesign.saveFormCustomEdit("Ctrl+S");
							return false;
						}
					}
				}
			});
			/**浏览器窗口大小改变事件*/
			$(window).resize(function(){
				$.each($("#formField div[attrtype=Radio],#formField div[attrtype=Checkbox]"),function(ind,obj){
					common.config.formCustom.formCustomDesign.fieldResize(obj);
				});
				common.config.formCustom.formCustomDesign.resizeWith();
			});
			setTimeout(function(){
				$("#customField_fieldset").show();
			},0);
			
			$("#tabsAttr_customField_select").click(function(){
				windows("tabsAttrDiv",{width:530,maxHeight:480,padding:3});
				var tabsField_attrNos = common.config.formCustom.formCustomDesign.findTabsFieldAttrNos();
				if(tabsField_attrNos.length<=0){
					tabsField_attrNos = -1;
				}
				if(loadshowAttrsGrid==false){
					loadshowAttrsGrid=true;
					common.config.formCustom.formCustomDesign.selectTabsAttrCustomField(tabsField_attrNos);
				}else{
					$('#tabsAttr_customFieldList').jqGrid('setGridParam', {
		                url: 'attrs!find.action?attrsQueryDTO.attrNos='+tabsField_attrNos,
		                page: 1
		            }).trigger('reloadGrid');
				}
				
			});
			$("#link_tabs_save_ok").click(function(){common.config.formCustom.formCustomDesign.tabAttrsSave();});
		}
	}
	
}();
$(function(){common.config.formCustom.formCustomDesign.init();});