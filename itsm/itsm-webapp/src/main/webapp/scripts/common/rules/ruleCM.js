$package('common.rules');
$import('common.config.dictionary.dataDictionaryUtil');
$import("common.security.userUtil");
$import("common.config.category.eventCategoryTree");
$import("common.security.organizationTreeUtil");
$import("common.jbpm.processUse");
$import('common.knowledge.knowledgeTree');
$import('common.rules.ruleMain');
$import('common.config.systemGuide.systemGuide');
$import("itsm.itsop.selectCompany");
$import("common.config.category.serviceCatalog");
$import("common.config.category.serviceOrgUtils");
if(isChangeHave){
	$import("itsm.change.changeApproval");
}
if(isCIHave){
	$import("itsm.cim.configureItemUtil");
	$import("itsm.cim.ciCategoryTree");
}
/**  
 * @author Van  
 * @constructor WSTO
 * @description 规则列表主函数.common/rules/ruleMain.jsp
 * @date 2011-02-26
 * @since version 1.0 
 */
common.rules.ruleCM = function(){
	this.categoryIds=[];
	this.ruleType='requestFit';
	this.ruleGuideShareId='';
		return {
			/**
			 * @description 加载规则类型格式化
			 * @param _ruleType 规则类型
			 */
			loadType:function(_ruleType){
		
				ruleType=_ruleType;
			},
			/**
			 * @description 取得option HTML
			 * @param options 选项(规则属性)
			 */
			getOptionsHTML_i18n:function(options) {
			    var optionHTML = '';
			    $.each(options, function(k,v) {
			    	optionHTML += '<option value="'+k+'">'+i18n[v]+'</option>';
			    });
			    return optionHTML;
			},
		
			/**
			 * @description 加载条件下拉
			 */
			loadConditionHTMLs:function(){
				var options={};
				if(ruleType=='requestFit' || ruleType=='requestProce'){
					options = {
						'etitle' : 'title',
						'edesc' : 'label_sla_desc',
						'createdByNo' : 'label_sla_createBy',
						'priorityNo' : 'label_sla_priority',
						'seriousnessNo' : 'label_sla_seriousness',
						'effectRangeNo' : 'label_sla_effectRange',
						'requestCategoryNo' : 'label_sla_ecategory',
						'levelNo' : 'label_sla_level',
						'imodeNo' : 'label_sla_imode',
						'organizationNos':'lable_user_org',
						'processKey':'flow',
						'companyNo':'lable_request_org',
						/*'cicategoryNo' : 'ci_configureItemCategory',*/
						'cicategoryNos' : 'ci_configureItemCategory',
						'serviceDirIds':'label_knowledge_relatedService',
						'weekNo':'lable_created_belongs_week',
						'technologyGroup':'lable_user_Technology_Group',
						'locationNos':'lable_task_location'
						};
				}
				if(ruleType=='mailToRequest'){
					options = {
						'etitle' : 'rule_mail_mailTitle',
						'edesc' : 'rule_mail_mailContent',
						'createdByEmail' : 'rule_mail_mailFrom'
					};
				}
				
				if(ruleType=='changeApproval' || ruleType=='changeProce'){//变更审批
					options = {
						'etitle' : 'title',
						'edesc' : 'label_sla_desc',
						'categoryNo' : 'category',
						'priorityNo' : 'label_sla_priority',
						'seriousnessNo' : 'label_sla_seriousness',
						'effectRangeNo' : 'label_sla_effectRange',
						'companyNo' : 'label_belongs_client'
						};
				}
				return common.rules.ruleCM.getOptionsHTML_i18n(options);
					
			},
			/**
			 * @description 加载运算单位
			 */
			loadConditionMathematicalOperationHTML:function(){
				
				var options = {
						'matches' : 'label_sla_matches',
						'not matches' : 'label_sla_notMatches',
						'==' : 'label_sla_eq',
						'!=' : 'label_sla_notEq',
						'matches start' : 'label_sla_matchesStart',
						'matches end' : 'label_sla_matchesEnd'
					};
				
				return common.rules.ruleCM.getOptionsHTML_i18n(options);
				
			},
			/**
			 * @description 加载动作集
			 */
			loadActionHTML:function(){
				var options={};
				if(ruleType=='requestFit'){//请求规则
					options = {
						'servicesNo':'label_sla_setServiceOrg',
						'approvalNo':'label_sla_setApproval',
						'updateLevelNo':'label_sla_updateTo',
						'assigneeGroupNo':'label_sla_assignGroup',
						'technicianNo':'label_sla_assign',
						'priorityNo':'label_rule_priority',
						'seriousnessNo':'label_rule_seriousness',
						'effectRangeNo':'label_rule_effectRange',
						'processKey':'label_rule_processKey',
						'requestCategoryNo':'label_rule_cateory',
						'serviceDirIdsStr':'label_rule_rateServiceDir',
						'slaContractNo':'label_rule_matchSLA',
						'attendance':'label_rule_attendance',
						'automaticallyAssigned':'automaticallyAssigned',
						'variablesAssigneeType':'variablesAssigneeType',
						'requestClose':'label_rule_requestClose'
						};
				}
				if(ruleType=='requestProce'){//请求流程规则
					options = {
							'servicesNo':'label_sla_setServiceOrg',
							//'approvalNo':'label_sla_setApproval',
							'updateLevelNo':'label_sla_updateTo',
							//'assigneeGroupNo':'label_sla_assignGroup',
							//'technicianNo':'label_sla_assign',
							'taskAssigneeNo':'label_flow_taskAssigneeNo',
							'taskassigneeGroupNo':'label_flow_taskassigneeGroupNo',
							'priorityNo':'label_rule_priority',
							'seriousnessNo':'label_rule_seriousness',
							'effectRangeNo':'label_rule_effectRange',
							'requestCategoryNo':'label_rule_cateory',
							'attendance':'label_rule_attendance',
							'automaticallyAssigned':'automaticallyAssigned'
							
						};
				}
				if(ruleType=='mailToRequest'){//邮件转请求规则
					options = {
							'etitle' : 'label_rule_etitle',
							'edesc' : 'label_rule_desc',
							'createdByNo' : 'label_rule_requestCreator',
							'priorityNo' : 'label_rule_priority',
							'seriousnessNo' : 'label_rule_seriousness',
							'effectRangeNo' : 'label_rule_effectRange',
							'requestCategoryNo' : 'label_rule_cateory',
							'levelNo' : 'label_rule_level',
							'imodeNo' : 'label_rule_imode',
							'attendance':'label_rule_attendance',
							'requestClose':'label_rule_requestClose',
							'mailToRequest' :'ConvertMailToRequest'
						};
				}
				if(ruleType=='changeApproval'){//需要审批
					options = {
							'needApprove':'label_change_needApprove',
							'priorityNo':'label_rule_priority',
							'seriousnessNo':'label_rule_seriousness',
							'effectRangeNo':'label_rule_effectRange',
							'changeProcessKey':'label_rule_processKey',
							'approvalPeoples':'label_sla_setApproval'
							//'approvalPeople':'label_sla_setApproval'
							
						};
				}
				
				if(ruleType=='changeProce'){//需要审批
					options = {
							'needApprove':'label_change_needApprove',
							'priorityNo':'label_rule_priority',
							'seriousnessNo':'label_rule_seriousness',
							'effectRangeNo':'label_rule_effectRange',
							'approvalPeoples':'label_sla_setApproval',
							'taskAssigneeNo':'label_flow_taskAssigneeNo',
							'taskassigneeGroupNo':'label_flow_taskassigneeGroupNo'
							//'approvalPeople':'label_sla_setApproval'
							
						};
				}
				return common.rules.ruleCM.getOptionsHTML_i18n(options);
					
			},
			
			/**
			 * @description 规则名称匹配
			 * @param str 规则名称
			 */
			switchRuleName:function(str){
				var strs ={};
				if(ruleType=='mailToRequest'){//邮件转请求规则
					$.extend(strs,{
						'etitle':'rule_mail_mailTitle',
						'edesc':'rule_mail_mailContent'
					});
				}else{
					$.extend(strs,{
						'etitle' : 'title',
						'edesc' : 'label_sla_desc'
					});
				}
				$.extend(strs,{
					'createdByNo':'label_sla_createBy',
					'priorityNo':'label_sla_priority',
					'seriousnessNo':'label_sla_seriousness',
					'effectRangeNo':'label_sla_effectRange',
					'requestCategoryNo':'label_sla_ecategory',
					'levelNo':'label_sla_level',
					'imodeNo':'label_sla_imode',
					'==':'label_sla_eq',
					'!=':'label_sla_notEq',
					'in' : 'label_sla_matches',
					'notIn' : 'label_sla_notMatches',
					'matches start':'label_sla_matchesStart',
					'matches end':'label_sla_matchesEnd',
					'not matches':'label_sla_notMatches',
					'matches':'label_sla_matches',
					'organizationNos':'lable_user_org',
					'servicesNo':'label_sla_setServiceOrg',
					'approvalNo':'label_sla_setApproval',
					'ownerNo':'label_sla_updateTo',
					'technicianNo':'label_sla_assign',
					'taskAssigneeNo':'label_flow_taskAssigneeNo',
					'taskassigneeGroupNo':'label_flow_taskassigneeGroupNo',
					'assigneeGroupNo':'label_sla_assignGroup',
					'statusNo':'label_sla_struts',
					'priorityNo':'label_sla_priority',
					'seriousnessNo':'label_sla_seriousness',
					'updateLevelNo':'escalateLevel',
					'processKey':'flow',
					'companyNo':'lable_request_org',
					'mailTitle' : 'rule_mail_mailTitle',
					'mailContent' : 'rule_mail_mailContent',
					'mailFrom' : 'rule_mail_mailFrom',
					'createdByEmail' : 'rule_mail_mailFrom',
					'needApprove':'label_change_needApprove',
					'approvalPeoples':'label_sla_setApproval',
					'approvalPeople':'label_sla_setApproval',
					/*'cicategoryNo' : 'ci_configureItemCategory',*/
					'cicategoryNos' : 'ci_configureItemCategory',
					'changeProcessKey':'flow',
					'serviceDirIdsStr':'label_rule_rateServiceDir',
					'serviceDirIds':'label_rule_rateServiceDir',
					//'locationNo':'label_request_ciLoc',
					'slaContractNo':'label_rule_matchSLA',
					'categoryNo':'label_request_changeCategory',
					'organizationNo':'label_belongs_client',
					'mailToRequest' :'ConvertMailToRequest',
					'weekNo':'lable_created_belongs_week',
					'attendance':'label_rule_attendance',
					'automaticallyAssigned':'automaticallyAssigned',
					'variablesAssigneeType':'variablesAssigneeType',
					'technologyGroup':'lable_user_Technology_Group',
					'locationNos':'lable_task_location',
					'requestClose':'label_rule_requestClose'
				});
						 
			    $.each(strs, function(k,v) {
			    	str=str.replace(k,i18n[v]);
			    });
			    return str;
	
			},
			
			/**
			 * @description 加载操作HTML
			 */
			loadActionHTML_Simple:function(){
				var options = {
						'==':'label_sla_eq',
						'!=':'label_sla_notEq'
					};
				return common.rules.ruleCM.getOptionsHTML_i18n(options);
	
			},
			/**
			 * @description 加载操作HTML1
			 */
			loadActionHTML_Simple1:function(){
				var options = {
						'==':'label_sla_eq',
					};
				return common.rules.ruleCM.getOptionsHTML_i18n(options);
	
			},
			/**
			 * @description 加载操作HTML2
			 */
			loadActionHTML_Simple2:function(){
				
				return common.rules.ruleCM.loadConditionMathematicalOperationHTML();
			},
			/**
			 * @description 加载操作HTML3
			 */
			loadActionHTML_Simple3:function(){
				var options = {
						'in' : 'label_sla_matches',
						'notIn' : 'label_sla_notMatches'
					};
				return common.rules.ruleCM.getOptionsHTML_i18n(options);
			},
			
			/**
			 * @description 加载操作HTML3
			 * @param valueID 获取元素值ID
			 * @param appendID 追加元素ID
			 */
			setMathematicalOperation:function(valueID,appendID){
				
				var tp=$(valueID).val();
				$(appendID).empty();
				
				if(tp=="needApprove" || tp=='approvalPeople' || tp=='slaContractNo'){
					$(common.rules.ruleCM.loadActionHTML_Simple()).appendTo(appendID);
				}else if(tp=='categoryNo'||tp=='approvalPeoples' || tp=="locationNo"||tp=="createdByNo"||tp=='companyNo'||tp=='organizationNos'||tp=='cicategoryNos'||tp=='cicategoryNo'|| tp=='serviceDirIdsStr'|| tp=='serviceDirIds'||tp=="priorityNo"||tp=="seriousnessNo"||tp=="effectRangeNo" ||tp=="imodeNo" ||tp=="levelNo"||tp=="requestCategoryNo"||tp=="weekNo"||tp=="technologyGroup"||tp=='locationNos'){
					$(common.rules.ruleCM.loadActionHTML_Simple3()).appendTo(appendID);
				}else{
					$(common.rules.ruleCM.loadActionHTML_Simple2()).appendTo(appendID); 	
	
				}
			},
			
			/**
			 * @description 匹配数据类型
			 * @param valueID 取值ID
			 */
			switchDataType:function(valueID){
				
				var sel=$(valueID).val();
						
				if("etitle"==sel||"edesc"==sel||"mailTitle"==sel||"mailContent"==sel||"mailFrom"==sel 
						|| "processKey"==sel || "needApprove"==sel || "changeProcessKey"==sel || "convertRequest"==sel 
						|| "mailToRequest"==sel  || "automaticallyAssigned"==sel || "attendance"==sel 
						|| "variablesAssigneeType"==sel || "requestClose"==sel || "createdByEmail" ==sel ){
					
					return "String";
	
				}else if('cicategoryNos'==sel || 'serviceDirIds'==sel ||'serviceDirIdsStr'==sel 
						|| 'organizationNos'==sel||'technologyGroup'==sel || 'approvalPeoples'==sel){
					return "Long[]";
				}else{
					return "Long";
				}
				
			},
	
			/**
			 * @description 选择请求分类/位置.(规则集)
			 * @param putID 选中的分类ID赋值ID
			 * @param putName 选中的分类名称赋值ID
			 */
			selectRequestCategory:function(putID,putName,title,flag){
				if('#givenValue'==putID || '#editRule_givenValue'==putID){
					common.config.category.eventCategoryTree.requestCategory(putName,putID);
				}else{
					$('#selectClassifyDirDiv').dialog({   
				         title: title,   
				         width: 250,   
				         height: 400,   
				         modal: true  
				     });
					var type='Request';
					if(flag=="requestCategoryNo"){
						type='Request';
					}else{
						type='Location';
					}
					common.rules.ruleCM.selectClassiftyDir(type);
					$('#selectClassifyDir_getClassifydNodes').unbind().click(function(){
						common.rules.ruleCM.getSelectedClassiftDirNodes(putID,putName);
					});
				}
			},
			
		
			/**
			 * @description 请求分类信息/位置(规则集)
			 * */
			selectClassiftyDir:function(type){
				$('#selectClassifyDirTreeDiv').jstree({
					"json_data":{
					    ajax: {
					    	url : "event!getCategoryTree.action?num="+category_num,
					    	data:function(n){
						    	  return {'types': type,'parentEventId':n.attr ? n.attr("id").replace("node_",""):0};//types is in action
					    	},
						    cache:false}
					},
					"plugins" : [ "themes", "json_data", "checkbox" ]
					});
			},
			
			/**
			 * @description 请求分类信息/位置，获取选择信息(规则集)
			 * @param putID 选中的分类ID赋值ID
			 * @param putName 选中的分类名称赋值ID
			 * */
			getSelectedClassiftDirNodes:function(putID,putName){
				
				var namePut1="";
				 var classiftyid="";
				 var categoryIds=new Array();
				$("#selectClassifyDirTreeDiv").jstree("get_checked",false,true).each(function (i,n) { 
					 var node = jQuery(this); 
					 var ids=node.attr('id');
					 
					 if(ids==null){//校正orgNo
						 ids=1;
					 }		
					 categoryIds[i]=ids;
				});
				var url="event!findSubCategoryArray.action";
				if(categoryIds.length>0){
					var param = $.param({'categoryNos':categoryIds},true);
					$.post(url,param,function(res){
						 for(var i=0;i<res.length;i++){
							  namePut1+=res[i].prentFullName+" , ";
							  classiftyid+=res[i].eventId+",";
						 }
						 if(classiftyid.length>1){
							  namePut1=namePut1.substring(0,namePut1.length-3);
							  classiftyid=classiftyid.substring(0,classiftyid.length-1);
						 }
						  $(putName).val(namePut1);
						  $(putID).val(classiftyid);
						  
					});
				}
				$('#selectClassifyDirDiv').dialog('close');
			},
			
			
			/**
			 * @description 选择服务目录.(规则集)
			 * @param putID 选中服务目录ID赋值ID
			 * @param putName 选中的机构名称赋值ID
			 */
			selectRequestServiceDir:function(putID,putName){
				/*if("#givenValue"==putID)
					itsm.knowledge.knowledgeTree.selectKnowledgeServiceFilter('#knowledge_services_select_window','#knowledge_services_select_tree',putName,putID);			
				else{
					
				}*/
				common.config.category.serviceCatalog.selectSingleServiceDir(putName,putID);
				
//				windows('selectServicesDirDiv',{width:250,height:400});
//				common.rules.ruleCM.selectServicesDir();
//				$('#selectServicesDir_getServicesdNodes').unbind().click(function(){
//					common.rules.ruleCM.getSelectedServicesDirNodes(putID,putName);
//				});
			},
			/**
			 * @description 选择服务目录.显示机构信息(规则集)
			 */
			selectServicesDir:function(){
				$('#selectServicesDirTreeDiv').jstree({
					"json_data":{
					    ajax: {
					    	url : "event!getCategoryTree.action?num=0",
					    	data:function(n){
					    		return {'types': 'Service','parentEventId':n.attr ? n.attr("id").replace("node_",""):0};//types is in action
					    	},
					    	cache:false}
					},
					"plugins" : [ "themes", "json_data", "checkbox" ]
				});	
			},
			/**
			 * 
			 * @description 选择服务目录显示，获取windwos面板上选择的值(规则集)
			 * @param putID 选中服务目录ID赋值ID
			 * @param putName 选中的机构名称赋值ID
			 */
			getSelectedServicesDirNodes:function(putID,putName){
				var namePut1="";
				 var classiftyid="";
				 var categoryIds=new Array();
				$("#selectServicesDirTreeDiv").jstree("get_checked",false,true).each(function (i,n) { 
					 var node = jQuery(this); 
					 var ids=node.attr('id');
					 if(ids==null){//校正orgNo
						 ids=1;
					 }
					 categoryIds[i]=ids;
				});
				if(categoryIds.length>0){
					var url="event!findSubCategoryArray.action";
					var param = $.param({'categoryNos':categoryIds},true);
					$.post(url,param,function(res){
						 for(var i=0;i<res.length;i++){
							  namePut1+=res[i].eventName+",";
							  classiftyid+=res[i].eventId+",";
						 }
						 if(classiftyid.length>1){
							  namePut1=namePut1.substring(0,namePut1.length-1);
							  classiftyid=classiftyid.substring(0,classiftyid.length-1);
						 }
						  $(putName).val(namePut1);
						  $(putID).val(classiftyid);
						  
					});
				}
				$('#selectServicesDirDiv').dialog('close');
			},
			
			
			/**
			 * @description 配置项分类(规则集)
			 * @param id 选中的配置分类ID赋值ID
			 * @param name 选中的配置分类名称赋值ID
			 */
			selectCiCategory:function(id,name){
				//itsm.cim.ciCategoryTree.configureItemTree('search_ciCategoryTreeDiv','search_ciCategoryTreeTree',id,name,true,function(){});
				windows('selectCIsDirDiv',{width:250,height:400});
				common.rules.ruleCM.selectCIsDir();
				$('#selectCIsDir_getCIsdNodes').unbind().click(function(){			
					common.rules.ruleCM.getSelectedCIsDirNodes(id,name);
				});
			},
			/**
			 * @description 配置项分类 显示机构信息(规则集)
			 */
			selectCIsDir:function(){
				var _url="ciCategory!getConfigurationCategoryTree.action?findAll=true&userName="+userName;
				$("#selectCIsDirTreeDiv")
				.jstree({
					"json_data":{
					    ajax: {
					    	url :_url,
					    	data:function(n){
						    	  return {'parentEventId':n.attr ? n.attr("cno").replace("node_",""):0};//types is in action
						    },
					    	cache: false}
					},
					"plugins" : [ "themes", "json_data", "checkbox" ]
				});
			},
			/**
			 * @description 配置项分类 获取windwos面板上选择的值(规则集)
			 * @param id 选中的配置项ID赋值ID
			 * @param name 选中的配置项名称赋值ID
			 */
			getSelectedCIsDirNodes:function(id,name){
				 var namePut1="";
				 var classiftyid="";
				 var categoryIds=new Array();
				$("#selectCIsDirTreeDiv").jstree("get_checked",false,true).each(function (i,n) { 
					 var node = jQuery(this); 
					 var ids=node.attr('cno');
					 if(ids==null){//校正orgNo
						 ids=1;
					 }
					 categoryIds[i]=ids;
					/* namePut1+=node.attr('cname')+",";
					 classiftyid+=ids+",";
					 var url="ciCategory!findSubCategorys.action?cno="+ids;			 
					 $.post(url,function(res){
						 if(res!=null){
							  for(var i=0;i<res.length;i++){
								  namePut1+=res[i].cname+",";
								  classiftyid+=res[i].cno+",";
							 	}
							 	if(classiftyid.length>1){
								  namePut1=namePut1.substring(0,namePut1.length-1);
								  classiftyid=classiftyid.substring(0,classiftyid.length-1);
								  }					
							  };
							  $('#'+id).val(classiftyid);
							  $('#'+name).val(namePut1);
						});			*/
				});
				if(categoryIds.length>0){
					var url="ciCategory!findSubCategoryArray.action";
					var param = $.param({'cnos':categoryIds},true);
					 $.post(url,param,function(res){
						  for(var i=0;i<res.length;i++){
							  namePut1+=res[i].cname+",";
							  classiftyid+=res[i].cno+",";
						 	}
						  if(classiftyid.length>1){
							  namePut1=namePut1.substring(0,namePut1.length-1);
							  classiftyid=classiftyid.substring(0,classiftyid.length-1);
						  }	
						  $('#'+id).val(classiftyid);
						  $('#'+name).val(namePut1);
					 });
				}
				
				$('#selectCIsDirDiv').dialog('close');
			},			
			
			/**
			 * @description 变更分类(规则集)
			 * @param id 选中的变更分类ID赋值ID
			 * @param name 选中的变更分类名称赋值ID
			 * */
			changeCategory:function(id,name){
				windows('selectChangeDirDiv',{width:250,height:400});
				common.rules.ruleCM.selectChangeDir();
				 
				$('#selectChangeDir_getChangedNodes').unbind().click(function(){			
					common.rules.ruleCM.getSelectedChangeDirNodes(id,name);
				});
			},
			
			/**
			 * @description 变更分类 显示机构信息(规则集)
			 * */
			selectChangeDir:function(){
			 
				$('#selectChangeDirTreeDiv').jstree({
					"json_data":{
					    ajax: {url : "event!getCategoryTree.action?num=0",
						      data:function(n){
						    	  return {'types': 'Change','parentEventId':n.attr ? n.attr("id").replace("node_",""):0};//types is in action
						      },
						      cache:false}
					},
					"plugins" : [ "themes", "json_data", "checkbox" ]
					});
	
			},
			
			/**
			 * @description 变更分类 获取windwos面板上选择的值(规则集)
			 * @param id 选中的关联变更ID赋值ID
			 * @param name 选中的关联变更名称赋值ID
			 * */
			getSelectedChangeDirNodes:function(id,name){
				var namePut1="";
				var classiftyid="";
				var categoryIds=new Array();
				//取得所有选中的节点，返回节点对象的集合
				$("#selectChangeDirTreeDiv").jstree("get_checked").each(function (i,n) { 
					 var node = jQuery(this); 					
					 var ids=node.attr('id');
					 if(ids==null){//校正orgNo
						 ids=1;
					 }
					 categoryIds[i]=ids;
					
				});
				if(categoryIds.length>0){
					var url="event!findSubCategoryArray.action";
					var param = $.param({'categoryNos':categoryIds},true);
					 $.post(url,param,function(res){
						  for(var i=0;i<res.length;i++){
							  namePut1+=res[i].eventName+",";
							  classiftyid+=res[i].eventId+",";
						 	}
						  if(classiftyid.length>1){
							  namePut1=namePut1.substring(0,namePut1.length-1);
							  classiftyid=classiftyid.substring(0,classiftyid.length-1);
						  }	
						  $(name).val(classiftyid);
						  $(id).val(namePut1);
					 });
				}
				$('#selectChangeDirDiv').dialog('close');
			},
			
			/**
			 * @description 加载升级级别.
			 * @param select 下载框选中的值
			 */
			loadUpdateLevel:function(select){
				var url = 'updatelevel!findAllLevels.action';
				$.post(url,function(res)
				{
					if(res.data!=null){
						if(res.data.length>0){
							for(var i=0;i<res.data.length;i++){
								$('<option value="'+res.data[i].ulId+'">'+res.data[i].ulName+'('+res.data[i].approvalName+')</option>').appendTo(select);
							}
						}
					}
				});
				
			},
		
	
			/**
			 * @description 选择服务机构.
			 * @param treeDIV 树结构ID
			 * @param windowDIV 服务机构窗口ID
			 * @param orgNo_put 选中的机构ID的赋值输入框ID
			 * @param orgName_put 选中的机构名称的赋值输入框ID
			 */
			selectServiceOrg:function(treeDIV,windowDIV,orgNo_put,orgName_put){
			   windows(windowDIV.replace('#',''),{width:240,height:400});
				$(treeDIV).jstree({
					"json_data":{
					    ajax: {
					    	url : "organization!findServicesTree.action",
					    	data:function(n){
						    	  return {'parentOrgNo':n.attr ? n.attr("orgNo").replace("node_",""):0};//types is in action
							},
					    	cache:false
					    }
					},
					"plugins" : ["themes", "json_data", "ui", "crrm", "cookies", "types", "hotkeys"]
					})
					
					.bind('loaded.jstree', function(e,data){data.inst.open_all(-1);})
					.bind('select_node.jstree',function(event,data){
						if(data.rslt.obj.attr("orgName")!='ROOT'){
							$(orgNo_put).val(data.rslt.obj.attr("orgNo"));
							$(orgName_put).val(data.rslt.obj.attr("orgName")).focus();
							$(windowDIV).dialog('close');
						}
				});
			},
			
			
			/**
			 * @description 添加规则集列表.
			 */
			replaceRuleStr:function(){
				var _id = 'constraint_'+new Date().getTime();
				var str= '<tr id="\'#'+_id+'\'">'+
				    '<td style="background-color:#FFF;height:28px;text-align:center">{1}'+
				    '<input type="hidden" name="rule_condition_constraints_conNo" value="{2}"/>'+
				    '<input type="hidden" name="rule_condition_constraints_dataType" value="{3}" >'+
				    '<input type="hidden" style="border:none" name="rule_condition_constraints_propertyName" value="{4}" readonly>'+
				    '</td>'+
				    '<td style="background-color:#FFF;text-align:left">{5}'+
				    '<input type="hidden" name="rule_condition_constraints_propertyValueName" value="{6}">'+
				    '<input type="hidden" name="rule_condition_constraints_propertyValue" value="{7}">'+
				    '</td>'+
				    '<td style="background-color:#FFF;height:28px;text-align:center"><select name="rule_condition_constraints_orAnd">{8}</select>'+
				    '<input type="hidden" class="{9}" value="{10}"/>'+
				    '</td>'+
				    '<td style="background-color:#FFF;text-align:center">'+
				    '<img src="../images/icons/delete.gif" style="cursor:pointer;margin:0px" onclick="common.rules.ruleCM.removeRow(\'#'+_id+'\')"/>'+
				    ' <img id="'+_id+'" src="../images/icons/arrow_up_blue.gif" style="cursor:pointer;margin:0px" onclick="basics.tableOpt.trUp(\'#'+_id+'\')"/>'+
				    ' <img src="../images/icons/arrow_down_blue.gif" style="cursor:pointer;margin:0px" onclick="basics.tableOpt.trDown(\'#{11}_constraintsTable\',\'#'+_id+'\')"/>'+
				    '</td>'+
				  '</tr>';
				  
				  for (var i=0; i<arguments.length; i++) {
					  str = str.replace('{'+i+'}', arguments[i]);//.replace('organizationNo',i18n['title_user_org']);
				  }
				  return str;
			},
			/**
			 * @description 移除表格行.
			 * @param rowId 要删除的行ID
			 */
			removeRow:function(rowId){
				msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirmDelete'],function(){
					basics.tableOpt.trDel(rowId);
				});	
			},
			
			
			/**
			 * @description 移除表格行动作集.
			 * @param rowId 要删除的行ID
			 */
			removeRowRule:function(rowId){
				msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirmDelete'],function(){
					basics.tableOpt.trDelRule(rowId);
				});
				
				
			},
			
			/**
			 * @description 添加动作集.
			 */
			replaceActionStr:function(){
				
				
				  var str='<tr id="action{0}">'+
				    '<td style="background-color:#FFF;height:28px;text-align:center">{1}'+
				    '<input type="hidden" name="rule_actions_dataType" value="{2}">'+
				    '<input type="hidden" style="border:none" name="rule_actions_propertyName" value="{3}" readonly>'+
				    '</td>'+
				    '<td style="background-color:#FFF;text-align:center">{4}'+		   
				    '<input type="hidden" name="rule_actions_givenName" value="{5}" >'+
				    '<input type="hidden" name="rule_actions_givenValue" value="{6}" >'+
				    '</td>'+
				    '<td style="background-color:#FFF;text-align:center">'+
				    '<img src="../images/icons/delete.gif" style="cursor:pointer" onclick="common.rules.ruleCM.removeRowRule(\'{7}\')"/>'+
				    '</td>'+
				  '</tr>';
				  
				  for (var i=0; i<arguments.length; i++) {
					
					    str = str.replace('{'+i+'}', arguments[i]);
				  }
	
				  
				  return str;
				  
			},
			
			/**
			 * @description 创建规则集事件.
			 * @param panelDiv 面板ID
			 * @param flag	标识
			 * @param htmlPut 输入框ID
			 * @param nameId 文本框ID
			 * @param valueId 文本框ID
			 * @param method 回调函数
			 */
			createRuleEvent:function(panelDiv,flag,htmlPut,nameId,valueId,method){
				
				var HTML="";
				var _valueId='#'+valueId;			
				if(flag=="categoryNo"){//变更分类
					//HTML="<input id="+nameId+" style='width:160px;cursor:pointer;color:#555555' onclick=common.config.category.eventCategoryTree.changeCategory('#"+nameId+"','#"+valueId+"') readonly/><input type='hidden' id='"+valueId+"' />";
					HTML="<input id="+nameId+" style='width:145px;cursor:pointer;color:#555555' onclick=common.rules.ruleCM.changeCategory('#"+nameId+"','#"+valueId+"') readonly/><input type='hidden' id='"+valueId+"' />";
				}
				else if(flag=="mailToRequest"){//需要审批
					HTML="<select id='"+valueId+"' style='width:80%'>"
					+"<option value='true'>"+i18n['label_basicConfig_deafultCurrencyYes']+"</option>"
					+"<option value='false'>"+i18n['label_basicConfig_deafultCurrencyNo']+"</option>"
					+"</select>";
				}else if(flag=="variablesAssigneeType"){//变量指派
					HTML="<select id='"+valueId+"' style='width:80%'>"
					+"<option value='creator'>"+i18n.requester+"</option>"
					+"<option value='technician'>"+i18n.lable_bpm_nowAssignee+"</option>"
					+"<option value='groupHead'>"+i18n.lable_bpm_assigneeGroupHead+"</option>"
					+"<option value='leader_0'>"+i18n.lable_bpm_creatorGroupHeadOne+"</option>"
					+"<option value='leader_1'>"+i18n.lable_bpm_creatorGroupHeadTwo+"</option>"
					+"<option value='leader_2'>"+i18n.lable_bpm_creatorGroupHeadThr+"</option>"
					+"<option value='leader_3'>"+i18n.lable_bpm_creatorGroupHeadFour+"</option>"
					+"</select>";
				}
				else if(flag=="automaticallyAssigned" || flag == "attendance"|| flag == "requestClose"){
					HTML="<select id='"+valueId+"' style='width:80%'>"
					+"<option value='true'>"+i18n['label_basicConfig_deafultCurrencyYes']+"</option>"
					+"<option value='false'>"+i18n['label_basicConfig_deafultCurrencyNo']+"</option>"
					+"</select>";
				}
				else if(flag=="needApprove"){//邮件自动转请求
					HTML="<select id='"+valueId+"' style='width:80%'>"
					+"<option value='true'>"+i18n['label_basicConfig_deafultCurrencyYes']+"</option>"
					+"<option value='false'>"+i18n['label_basicConfig_deafultCurrencyNo']+"</option>"
					+"</select>";
				}
				else if(flag=="requestCategoryNo"){//请求分类
					HTML="<input id="+nameId+" style='width:80%;cursor:pointer;color:#555555' onclick=common.rules.ruleCM.selectRequestCategory('#"+valueId+"','#"+nameId+"',i18n.label_request_requestType,'"+flag+"') readonly/><input type='hidden' id='"+valueId+"' />";
					
				}else if(flag=="statusNo"){ //状态
					
					HTML="<select id='"+valueId+"' style='width:80%'></select>";
					flag=flag.replace(/No/g,''); 
					if(flag=='status'){
						flag='requestStatus';
					}
					common.config.dictionary.dataDictionaryUtil.loadOptionsByCode(flag,_valueId);
				}else if(flag=="locationNo"||flag=="priorityNo"||flag=="seriousnessNo"||flag=="effectRangeNo" ||flag=="imodeNo" ||flag=="levelNo"||flag=='weekNo'){
					flag=flag.replace(/No/g,''); 
					if(flag=='status'){
						flag='requestStatus';
					}
					if('#givenValue'==_valueId || '#editRule_givenValue'==_valueId){
						HTML="<select id='"+valueId+"' style='width:80%'></select>";
						common.config.dictionary.dataDictionaryUtil.loadOptionsByCode(flag,_valueId);
					}
					else{
						common.config.dictionary.dataDictionaryUtil.loadOptionsByCode(flag,'#leftop');
						if("slaRule_propertyName"==nameId){
							HTML="<input style='width:80%' id='"+valueId+"' onclick=common.rules.ruleCM.multitermWindowSla() readonly/> <input type='hidden' id='addSlaRule_propertyValueid'  />";
						}else{
						HTML="<input style='width:80%' id='"+valueId+"' onclick=common.rules.ruleCM.multitermWindow() readonly/> <input type='hidden' id='addRule_propertyValueid'  />";
						}
					}
				}else if(flag=="createdByNo"){ //请求人
					//HTML="<input id="+nameId+" style='width:80%;cursor:pointer;color:#555555' onclick=common.security.userUtil.selectUserSingleRule('#"+nameId+"','#"+valueId+"','fullName',"+companyNo+"); readonly/><input type='hidden' id='"+valueId+"' />";
					HTML="<input id='"+nameId+"' style='width:96%;cursor:pointer;color:#555555' onclick=common.security.userUtil.selectUser('#"+nameId+"','#"+valueId+"','','fullName','-1') readonly/><input type='hidden' id='"+valueId+"' />";
				}else if(flag=="assigneeNo" || flag=="technicianNo" || flag=='taskAssigneeNo'){//指派技术员
					HTML="<input id='"+nameId+"' style='width:80%;cursor:pointer;color:#555555' onclick=common.security.userUtil.selectUser('#"+nameId+"','#"+valueId+"','','fullName',"+companyNo+") readonly/><input type='hidden' id='"+valueId+"' />";
				}else if(flag=="approvalNo"){//审批人员
					HTML="<input id='"+nameId+"' style='width:80%;cursor:pointer;color:#555555' onclick=common.security.userUtil.selectUser('#"+nameId+"','#"+valueId+"','','fullName',"+companyNo+") readonly/><input type='hidden' id='"+valueId+"' />";
				}
				else if(flag=="servicesNo" ||flag=="assigneeGroupNo" || flag =="taskassigneeGroupNo"){		
					
					HTML="<input id='"+nameId+"' style='width:80%;cursor:pointer;color:#555555' onclick='"+method+"' readonly/><input type='hidden' id='"+valueId+"' />";
				}
				else if(flag=="updateLevelNo"){
					
					HTML="<select id='"+valueId+"' style='width:80%'><option value=''>-- "+i18n['pleaseSelect']+" --</option></select>";
					common.rules.ruleCM.loadUpdateLevel('#'+valueId);
				}
				else if(flag=="organizationNos"){
					HTML="<input id="+nameId+" style='width:80%;cursor:pointer;color:#555555' onclick=common.config.category.serviceOrgUtils.selectORG('#"+nameId+"','#"+valueId+"',i18n.title_user_org) readonly/><input type='hidden' id='"+valueId+"' />";
					//HTML="<input id="+nameId+" style='width:100px;cursor:pointer;color:#555555' onclick=common.security.organizationTreeUtil.selectORG('#"+nameId+"','#"+valueId+"','-1') readonly/><input type='hidden' id='"+valueId+"' />";
				}else if(flag=="companyNo"){
					
					HTML="<input id='"+nameId+"' style='width:80%;cursor:pointer;color:#555555' onclick=itsm.itsop.selectCompany.openSelectCompanyWinCheckbox('#"+valueId+"','#"+nameId+"','','loginName','-1') readonly/><input type='hidden' id='"+valueId+"' />";
				}
				
				else if(flag=="processKey"){//加载请求流程列表
					HTML="<select id='"+valueId+"' style='width:80%'></select>";
					setTimeout(function(){
						common.jbpm.processUse.getProcess(valueId,"request","Request");//请求		
					},0);
					
				}else if(flag=="cicategoryNo" || flag=="cicategoryNos"){//请求匹配配置项分类
					
					HTML="<input id="+nameId+" style='width:80%;cursor:pointer;color:#555555' onclick=common.rules.ruleCM.selectCiCategory('"+valueId+"','"+nameId+"') readonly/><input type='hidden' id='"+valueId+"' />";
				}else if(flag=="changeProcessKey"){ //
					HTML="<select id='"+valueId+"' style='width:80%'></select>";
					setTimeout(function(){
						common.jbpm.processUse.getProcess(valueId,"change","Change");//变更	
					},0);
				}else if(flag=="approvalPeople"){
					HTML="<input id="+nameId+" style='width:80%;cursor:pointer;color:#555555' onclick=common.rules.ruleCM.selectChangeApproval('"+valueId+"','"+nameId+"') readonly/><input type='hidden' id='"+valueId+"' />";
				}else if(flag=="approvalPeoples"){
					HTML="<input id="+nameId+" style='width:80%;cursor:pointer;color:#555555' onclick=common.rules.ruleCM.selectChangeApproval('"+valueId+"','"+nameId+"') readonly/><input type='hidden' id='"+valueId+"' />";
				}else if(flag=='serviceDirIdsStr'){
					HTML="<input id="+nameId+" style='width:80%;cursor:pointer;color:#555555' onclick=common.rules.ruleCM.selectRequestServiceDir('#"+valueId+"','#"+nameId+"') readonly/><input type='hidden' id='"+valueId+"' />";
				}else if(flag=='serviceDirIds'){
					HTML="<input id="+nameId+" style='width:80%;cursor:pointer;color:#555555' onclick=common.rules.ruleCM.selectRequestServiceDir('#"+valueId+"','#"+nameId+"') readonly/><input type='hidden' id='"+valueId+"' />";
				}else if(flag=='slaContractNo'){
					HTML="<input id="+nameId+" style='width:80%;cursor:pointer;color:#555555' onclick=common.rules.ruleCM.selectSLAContract('"+panelDiv+"','#"+valueId+"','#"+nameId+"') readonly/><input type='hidden' id='"+valueId+"' />";
				}else if(flag=="technologyGroup"){
					HTML="<input id="+nameId+" style='width:80%;cursor:pointer;color:#555555' onclick=common.config.category.serviceOrgUtils.selectORG('#"+nameId+"','#"+valueId+"',i18n.label_userOwnerGroup) readonly/><input type='hidden' id='"+valueId+"' />";
				}else if(flag=="locationNos"){
					HTML="<input id="+nameId+" style='width:80%;cursor:pointer;color:#555555' onclick=common.rules.ruleCM.selectRequestCategory('#"+valueId+"','#"+nameId+"',i18n.lable_task_location,'"+flag+"') readonly/><input type='hidden' id='"+valueId+"' />";
				}else{
					HTML="<input style='width:80%' id='"+valueId+"' />";
				}
				$(htmlPut).html(HTML);
			},
			/**
			 * @description 多选Windows
			 */
			multitermWindow:function(){
				var strValue="";
				var strs="";
				var strsId="";
				var strValueid="";
				$('#lefttop option').remove();
				$('#rightop option').remove();
	
				var flag=$('#addRule_team').val();
			
				if(flag!=undefined){
					common.rules.ruleCM.createRuleEvent('#addRuleDiv',flag,'#addRule_propertyValueDIV','addRule_namePut','addRule_propertyValue','');
				}else{
					flag=$('#editRule_team').val();
					common.rules.ruleCM.createRuleEvent('#editRuleDiv',flag,'#editRule_propertyValueDIV','editRule_namePut','editRule_propertyValue','');
				}
	
				strs= new Array(); //定义一数组
				strsId=new Array();
				strValue=$('#addRule_propertyValue').val();
				if(strValue==undefined)
					strValue=$('#editRule_propertyValue').val();
				strValueid=$('#addRule_propertyValueid').val();
				if(strValue!=undefined)
					strs=strValue.split(",");
				if(strValueid!=undefined)
					strsId=strValueid.split(",");
				windows('multitermSLA',{width:418});
				if(strValue!=""){
					for (i=0;i<strs.length ;i++ ) {
						$('#rightop').append("<option value='"+strsId[i]+"'>"+strs[i]+"</option>");
					}
				}
			},
			/**
			 * @description 多选SLA Windows
			 */
			multitermWindowSla:function(){
				
				var strValue="";
				var strs="";
				var strsId="";
				var strValueid="";
				windows('multitermSLA',{width:418});
				$('#rightop option').remove();
				
				
				var flagSla=$('#addSLARule_team').val();
				
				if(flagSla!=undefined){
					common.rules.ruleCM.createRuleEvent('#addRuleDiv',flagSla,'#addRule_propertyValueDIV','addRule_namePut','addRule_propertyValue','');
				}
	
				
				strs= new Array(); //定义一数组
				strsId=new Array();
				strValue=$('#addRule_propertyValue').val();
				if(strValue==undefined)
					strValue=$('#editRule_propertyValue').val();
				strValueid=$('#addSlaRule_propertyValueid').val();
				if(strValue!=undefined)
					strs=strValue.split(",");
				if(strValueid!=undefined)
					strsId=strValueid.split(",");
				
				if(strValue!=""){
					for (i=0;i<strs.length ;i++ ) {
						$('#rightop').append("<option value='"+strsId[i]+"'>"+strs[i]+"</option>");
					}
				}
			},
			
			
			
			/**
			 * @description 选择SLA
			 * @param panelDiv 面板ID
			 * @param valueId 选中的SLA ID的赋值ID
			 * @param nameId 选中的SLA 名称的赋值ID
			 */
			selectSLAContract:function(panelDiv,valueId,nameId){
				common.rules.ruleMain.select_sla_openWin(panelDiv,valueId,nameId)
			},
			selectChangeApproval:function(valueId,nameId){
				itsm.change.changeApproval.select_changeRule_openWin(valueId,nameId);
			},
			/**
			 * @description 获取规则显示值.
			 * @param flagId 标识ID
			 * @param valueId 值ID
			 * @param nameId 名称ID
			 */
			getRuleTitle:function(flagId,valueId,nameId){
				
				var flag=$(flagId).val();
				if(flag=="processKey" ||flag=="statusNo" || flag=="updateLevelNo"||flag=="needApprove"||flag=="changeProcessKey"){						
					return $(valueId).find("option:selected").text();
				}		
				if(flag=="locationNo" ||flag=="etitle" || flag =="createdByEmail" ||flag=="edesc" ||"mailTitle"==flag||"mailContent"==flag||"mailFrom"==flag){
					return $(valueId).val();					
				}
				if(flag=="createdByNo" || flag=="requestCategoryNo"|| flag=="serviceDirIds" || flag=="serviceDirIdsStr" 
					|| flag=="assigneeNo" || flag=="technicianNo" ||flag=="approvalNo"||flag=="servicesNo"||flag=="assigneeGroupNo" 
						||flag=="organizationNos" || flag=="companyNo" || flag=="categoryNo" ||flag=="cicategoryNos" 
							||flag=="cicategoryNo" || flag=="approvalPeople" || flag=="approvalPeoples" || flag=="slaContractNo"
								||flag=='technologyGroup'||flag=="locationNos" || flag=="taskAssigneeNo" || flag == "taskassigneeGroupNo"){					
					return $(nameId).val(); 
				}
				if(valueId=='#givenValue'||'#editRule_givenValue'==valueId){
					return $(valueId).find("option:selected").text();
				}else{
					return $(valueId).val();
				}
			},
			/**
			 * @description 保存规则公共方法（请求规则）.
			 * @param panelDIV 面板ID
			 * @param flag	标识
			 * @param andId 条件ID
			 * @param _flag 规则包标识
			 */
			saveRuleCommon:function(panelDIV,flag,andId,_flag){
				var orgNos=new Array();
				var constraintsNames = $(panelDIV+' form:eq(1) input[name="rule_condition_constraints_propertyName"]');
				var constraintsValues = $(panelDIV+' form:eq(1) input[name="rule_condition_constraints_propertyValue"]');
				for(i =0; i<constraintsNames.length; i++){
					if(constraintsNames[i].value.indexOf("organizationNos")>=0){
						orgNos[i]=constraintsValues[i].value;
					}
				}
				var slaContractNo='';
				var actionsNames = $(panelDIV+' form:eq(2) input[name="rule_actions_propertyName"]');
				var actionsValues = $(panelDIV+' form:eq(2) input[name="rule_actions_givenValue"]');
				for(i =0; i<actionsNames.length; i++){
					if(actionsNames[i].value.indexOf("slaContractNo")>=0){
						slaContractNo=actionsValues[i].value;
					}
				}
				var result=false;
				if(slaContractNo!=null && slaContractNo!=''){
					if(orgNos!=null && orgNos.length>0 && slaContractNo!=null && slaContractNo!=''){
						$.post('slaContractManage!findSlaById.action?contractNo='+slaContractNo,function(res){
							for(var i=0;i<orgNos.length;i++){
								if(orgNos[i]!=undefined){
									for(var j=0;j<res.byServicesNos.length;j++){
										var _orgNos = orgNos[i].split(',');
										for(var k=0;k<_orgNos.length;k++){
											if(_orgNos[k]==res.byServicesNos[j]){
												result=true;
												break;
											}
										}
									}
								}
								
							}
							if(result){
								common.rules.ruleCM.saveRule_method(panelDIV,flag,andId,_flag);
							}else{
								msgAlert(i18n['label_rule_slaRuleNotMessage'],'info');//
							}
						});
					}else{
						msgAlert(i18n['label_rule_slaRuleNotMessage'],'info');//
					}
				}else{
					common.rules.ruleCM.saveRule_method(panelDIV,flag,andId,_flag);
				}
				
				
			},
			/**
			 * @description 保存规则公共方法（请求规则）.
			 * @param panelDIV 面板ID
			 * @param flag	标识
			 * @param andId 条件ID
			 * @param _flag 规则包标识
			 */
			saveRule_method:function(panelDIV,flag,andId,_flag){
				var rule = common.rules.ruleCM.getRuleInfo(panelDIV,andId,'requestRule');	
				var url= "callBusinessRuleSet!"+flag+".action";
				startProcess();//开始进程
				$.post(url,rule,function(){
					endProcess();//结束进程
					basics.tab.tabUtils.selectTab(mailTORequestGrid,function(){
						$("#rulesGrid").trigger('reloadGrid');
					});
					
					basics.tab.tabUtils.selectTab(requestRuleGrid,function(){
						$("#rulesGrid").trigger('reloadGrid');
					});
					
					basics.tab.tabUtils.selectTab(changeApproveGrid,function(){
						$("#rulesGrid").trigger('reloadGrid');
					});
					var titilName='';
					var ruleGuideShareId = ruleGuideShareId || '';
					//显示消息
					if(flag=="merge"){		
						msgShow(i18n['label_rule_editSuccessful'],'show');
						if(_flag=="requestFit" || _flag=="requestProce"){
							ruleGuideShareId = "requestRuleGuide";
							titilName=i18n['label_rule_editRequestRule'];
						}
						if(_flag=="mailToRequest"){
							titilName=i18n['label_rule_editEmailToRequestRule'];
						}
						if(_flag=="changeApproval" || _flag=="changeProce"){
							ruleGuideShareId = "changGuide";
							titilName=i18n['label_rule_editChangeRule'];
						}
						basics.tab.tabUtils.closeTab(titilName);
					}else{
						
						msgShow(i18n['msg_add_successful'],'show');
						if(_flag=="requestFit" || _flag=="requestProce"){
							ruleGuideShareId = "requestRuleGuide";
							titilName=i18n.label_rule_addRequestRule;
						}
						if(_flag=="mailToRequest"){
							titilName=i18n['label_rule_addEmailToRequestRule'];
						}
						if(_flag=="changeApproval" || _flag=="changeProce"){
							ruleGuideShareId = "changGuide";
							titilName=i18n['label_rule_addChangeRule'];
						}
						basics.tab.tabUtils.closeTab(titilName);
					}
					/**系统向导状态保存*/
					common.config.systemGuide.systemGuide.saveSystemGuide(ruleGuideShareId);
				});
			},
			/**
			 * @description 属性值转字段串
			 * @param propertyValue 属性值
			 */
			convert_string:function(propertyValue){
				var str="'";
				var strs= new Array();
				strs=propertyValue.split(","); //字符分割
				for (i=0;i<strs.length-1;i++ ){
						str=str+strs[i]+"','";
				}
				str=str+strs[strs.length-1];
				str=str+"'";
				return str;
			},
			/**
			 * @description 获取规则相关属性.
			 * @param panelDIV 面板ID
			 * @param andId 条件ID
			 * @param flag	标识
			 */
			getRuleInfo:function(panelDIV,andId,flag){

				var form1=panelDIV+' form:eq(0)';
				var form2=panelDIV+' form:eq(1)';
				if(flag=="sla"){
					form1=form2=panelDIV+' form';
				}
				var rule = $(form1).getForm();
				
				var constraintsProps = {};
				var constraintsNames = $(form2+' input[name="rule_condition_constraints_propertyName"]');
				var constraintsOrAnd = $(form2+' select[name="rule_condition_constraints_orAnd"]');
				var constraintsNos = $(form2+' input[name="rule_condition_constraints_conNo"]');
				var constraintsValues = $(form2+' input[name="rule_condition_constraints_propertyValue"]');
				var constraintsValueNames = $(form2+' input[name="rule_condition_constraints_propertyValueName"]');
				var constraintsDataTypes = $(form2+' input[name="rule_condition_constraints_dataType"]');
				for (i =0; i<constraintsNames.length; i++) {
					constraintsProps['rule.condition.constraints['+i+'].conNo'] = constraintsNos[i].value;
					constraintsProps['rule.condition.constraints['+i+'].propertyName'] = constraintsNames[i].value;
					constraintsProps['rule.condition.constraints['+i+'].propertyValue'] = constraintsValues[i].value;
					constraintsProps['rule.condition.constraints['+i+'].propertyValueName'] = constraintsValueNames[i].value;			
					constraintsProps['rule.condition.constraints['+i+'].dataType'] =constraintsDataTypes[i].value;
					constraintsProps['rule.condition.constraints['+i+'].andOr'] =constraintsOrAnd[i].value;
					constraintsProps['rule.condition.constraints['+i+'].sequence'] = (i+1);
				}
				$.extend(rule, constraintsProps);
				var actionsProps = {};
				if(flag=="sla"){
					actionsProps['rule.actions[0].propertyName']="matchRuleName";
					actionsProps['rule.actions[0].givenValue']=$('#rule_ruleName').val();
					actionsProps['rule.actions[0].dataType']="String";
					actionsProps['rule.actions[0].sequence']= 1;
				}else{
					var actionsNames = $(panelDIV+' form:eq(2) input[name="rule_actions_propertyName"]');
					var actionsValues = $(panelDIV+' form:eq(2) input[name="rule_actions_givenValue"]');
					var actionsNos = $(panelDIV+' form:eq(2) input[name="rule_actions_actionNo"]');
					var actionsDataTypes = $(panelDIV+' form:eq(2) input[name="rule_actions_dataType"]');
					var actionsGivenNames = $(panelDIV+' form:eq(2) input[name="rule_actions_givenName"]');
	
					for(j=0;j<actionsNames.length;j++){
						
						actionsProps['rule.actions['+j+'].propertyName']=actionsNames[j].value;
						actionsProps['rule.actions['+j+'].givenValue']=actionsValues[j].value;
						actionsProps['rule.actions['+j+'].givenName']=actionsGivenNames[j].value;
						actionsProps['rule.actions['+j+'].dataType']=actionsDataTypes[j].value;
						actionsProps['rule.actions['+j+'].sequence']= (j+1);
					}
				}
				$.extend(rule,actionsProps);
				return rule;
			}
		};

}();
