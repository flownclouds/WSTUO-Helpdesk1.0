$package("common.tools.scheduledTask");
$import('common.tools.scheduledTask.scheduledTask');
$import("common.tools.schedule.setMonthly");
$import('common.security.xssUtil');
/**  
 * @author QXY  
 * @constructor users
 * @description 定期任务管理
 * @date 2011-10-11
 * @since version 1.0 
 */
common.tools.scheduledTask.scheduledTasksManage=function(){

	return {
		
		inputSetValue:function(value){
			if(value==null || value=='null')
				return '';
			else
				return value;
		},
		/**
		 * 定期任务列表
		 */
		scheduledTasksGrid:function(){
			var params = $.extend({},jqGridParams, {	
				url:'scheduledTask!findPageScheduledTask.action?queryDTO.scheduledTaskId=',
				colNames:['ID',i18n['title'],i18n['type'],'',i18n['label_scheduled_type'],i18n['title_startTime'],i18n['title_endTime'],i18n['common_createTime'],i18n['common_updateTime']],
			 	colModel:[
			 	          {name:'scheduledTaskId',align:'center',width:30},
			 	          {name:'title',align:'left',width:80,sortable:false},
			 	          {name:'scheduledTaskType',align:'left',width:80,formatter:common.tools.scheduledTask.scheduledTasksManage.scheduledTaskTypeForma},
			 	          {name:'scheduledTaskType',align:'left',width:80,hidden:true},
			 	          {name:'timeType',align:'left',width:80,formatter:common.tools.scheduledTask.scheduledTasksManage.timeTypeForma},
			 	          
			 	          {name:'taskDate',align:'center',width:80,formatter:timeFormatterOnlyData},
			 	          {name:'taskEndDate',align:'center',width:80,formatter:timeFormatterOnlyData},
			 	          {name:'createTime',align:'center',width:80,formatter:timeFormatter},
			 	          {name:'lastUpdateTime',align:'center',width:80,formatter:timeFormatter}
			 	],
				jsonReader: $.extend(jqGridJsonReader, {id:"scheduledTaskId"}),
				sortname:'scheduledTaskId',
				pager:'#scheduledTasksPager'
				});
				$("#scheduledTasksGrid").jqGrid(params);
				$("#scheduledTasksGrid").navGrid('#scheduledTasksPager',navGridParams);
				//列表操作项
				$("#t_scheduledTasksGrid").css(jqGridTopStyles);
				$("#t_scheduledTasksGrid").append($('#scheduledTaskOptItem').html());
				
				//自适应宽度
				setGridWidth("#scheduledTasksGrid","regCenter",20);
		},
		/**
		 * 周期类型格式化
		 */
		timeTypeForma:function(cellvalue){
			if(cellvalue=='day')
				return i18n['label_scheduledTask_day'];
			if(cellvalue=='weekly')
				return i18n['label_scheduledTask_weekly'];
			if(cellvalue=='month')
				return i18n['label_scheduledTask_months'];
			if(cellvalue=='cycle')
				return i18n['label_scheduledTask_cycle']+'('+i18n['label_slaRule_days']+')';
			if(cellvalue=='on_off')
				return i18n['label_scheduledTask_on_off'];
			if(cellvalue=='cycleMinute')
				return i18n['label_scheduledTask_cycle']+'('+i18n['minutes']+')';
		},
		
		scheduledTaskTypeForma:function(cellvalue){
			if(cellvalue=='sla')
				return i18n['label_scheduled_sla_task'];
			if(cellvalue=='request' || cellvalue==null || cellvalue=='')
				return i18n['label_scheduled_request_task'];
			if(cellvalue=='email')
				return i18n['label_scheduled_email_task'];
			if(cellvalue=='configureItem')
				return i18n['label_scheduled_configureItem_task'];
			if(cellvalue=='adUpdate')
				return i18n['label_ad_update'];
			if(cellvalue=='request2problem')
				return i18n['rule_requesttoproblem_t'];
			if(cellvalue=='senReport')
				return i18n['custom_report_autoSendReport'];
			
		},
		
		/**
		 * 添加定期任务
		 */
		addScheduledTask:function(){
			$('#base_scheduledTask_id').val('');
			resetForm('#scheduledTask_add_form');
			$('#scheduledTask_save_but').unbind('click');
			$('#scheduledTask_save_but').click(function(){common.tools.scheduledTask.scheduledTasksManage.saveScheduledTask('scheduledTask!saveScheduledTask.action');});
			common.tools.scheduledTask.scheduledTask.everyWhatChange('day','base_');
			$('#base_scheduledTask_hours').val(0);
			$('#base_scheduledTask_minute').val(0);
			windows('scheduledTaskTypeDiv',{width:300});
		},
		selectScheduledTaskConfirm:function(){
			
			$('#base_scheduledTask_etitle').val('');
			$('#scheduledTask_save_but,#base_scheduledTask_title_tr').show();
			$('#scheduledTask_ok_btn').hide();
			
			var _scheduledTaskType=$("input[name='scheduledTaskType']:checked").val();
			$('#base_scheduledTaskType').val(_scheduledTaskType);
			$('#scheduledTaskTypeDiv').dialog('close');
			if(_scheduledTaskType=='request'){
				basics.tab.tabUtils.refreshTab(i18n["title_add_scheuledTask"],'../pages/common/tools/scheduledTask/addScheduledTaskFormCustom.jsp');
			}
			if(_scheduledTaskType=='sla'){
				$('#base_scheduledTask_beanId').val('autoUpdateRequestJob');
				windows('scheduledTask_add_win',{title:i18n['label_scheduled_sla_task'],width:520});
			}
			if(_scheduledTaskType=='email'){
				$('#base_scheduledTask_beanId').val('scanEmailMessagesJob');
				windows('scheduledTask_add_win',{title:i18n['label_scheduled_email_task'],width:520});
			}
			if(_scheduledTaskType=='request2problem'){
				$('#base_scheduledTask_beanId').val('autoCreateProblemJob');
				windows('scheduledTask_add_win',{title:i18n['rule_requesttoproblem_t'],width:520});
			}
			
			
			if(_scheduledTaskType=='configureItem'){
				$('#base_scheduledTask_beanId').val('ciExpiredJob');
				windows('scheduledTask_add_win',{title:i18n['label_scheduled_configureItem_task'],width:520});
			}
			
			if(_scheduledTaskType=='adUpdate'){
				$('#base_scheduledTask_beanId').val('adUpdateJob');
				windows('scheduledTask_add_win',{title:i18n['label_ad_update'],width:520});
			}
			
			if(_scheduledTaskType=='senReport'){
				
				
				$('#base_scheduledTask_beanId').val('autoSendReportJob');
				windows('scheduledTask_add_win',{title:i18n['custom_report_autoSendReport'],width:520});

			}
			
			
		},
		
		/**
		 * 给定期任务赋值
		 */
		setScheduledTaskValue:function(data){
			//ID
			$('#base_scheduledTask_id').val(common.tools.scheduledTask.scheduledTasksManage.inputSetValue(data.scheduledTaskId));
			
			
			//ID
			$('#base_scheduledTask_reportId').val(common.tools.scheduledTask.scheduledTasksManage.inputSetValue(data.reportId));
			
			
			//标题
			var html_code = common.security.xssUtil.html_code(data.requestDTO.etitle);
			$('#base_scheduledTask_etitle').val(common.tools.scheduledTask.scheduledTasksManage.inputSetValue(html_code));
			$('#base_scheduledTask_companNo').val(common.tools.scheduledTask.scheduledTasksManage.inputSetValue(data.requestDTO.companyNo));
			$('#base_scheduledTaskType').val(common.tools.scheduledTask.scheduledTasksManage.inputSetValue(data.scheduledTaskType));
			
			//时间表
			$('#base_scheduledTask_startDate_input').val(common.tools.scheduledTask.scheduledTasksManage.inputSetValue(timeFormatterOnlyData(data.taskDate)));
			$('#base_scheduledTask_endDate_input').val(common.tools.scheduledTask.scheduledTasksManage.inputSetValue(timeFormatterOnlyData(data.taskEndDate)));
			if(data.timeType=='day'){//日计划
				$('#base_timeType_day').attr('checked',true);
				common.tools.scheduledTask.scheduledTask.everyWhatChange('day','base_')
				$('#base_scheduledTask_hours').val(common.tools.scheduledTask.scheduledTasksManage.inputSetValue(data.taskHour));
				$('#base_scheduledTask_minute').val(common.tools.scheduledTask.scheduledTasksManage.inputSetValue(data.taskMinute));
			}
			
			if(data.timeType=='weekly'){//周计划
				common.tools.scheduledTask.scheduledTask.everyWhatChange('weekly','base_')
				$('#base_timeType_weekly').attr('checked',true);
				$('#base_scheduledTask_hours').val(common.tools.scheduledTask.scheduledTasksManage.inputSetValue(data.taskHour));
				$('#base_scheduledTask_minute').val(common.tools.scheduledTask.scheduledTasksManage.inputSetValue(data.taskMinute));
				var weekWeeks=data.weekWeeks;
				if(weekWeeks!=null && weekWeeks!=''){
					var weekWeekArray=weekWeeks.split(',');
					for(var i=0;i<weekWeekArray.length;i++){
						$('#checkbox_'+trim(weekWeekArray[i])).attr('checked',true);
					}
				}
				
			}
			
			if(data.timeType=='month'){//月计划
				
				$('#base_timeType_month').attr('checked',true);
				common.tools.scheduledTask.scheduledTask.everyWhatChange('month','base_')
				$('#base_scheduledTask_monthDay').val(common.tools.scheduledTask.scheduledTasksManage.inputSetValue(data.monthDay));
				$('#base_scheduledTask_hours').val(common.tools.scheduledTask.scheduledTasksManage.inputSetValue(data.taskHour));
				$('#base_scheduledTask_minute').val(common.tools.scheduledTask.scheduledTasksManage.inputSetValue(data.taskMinute));
				
				var monthMonths=data.monthMonths;
				if(monthMonths!=null && monthMonths!=''){
					var monthMonths=monthMonths.split(',');
					for(var i=0;i<monthMonths.length;i++){
						$('#checkbox_'+trim(monthMonths[i])).attr('checked',true);
					}
				}
				
			}
			if(data.timeType=='cycle'){//周期性计划
				$('#base_timeType_cycle').attr('checked',true);
				common.tools.scheduledTask.scheduledTask.everyWhatChange('cycle','base_')
				$('#base_scheduledTask_cyclicalDay').val(common.tools.scheduledTask.scheduledTasksManage.inputSetValue(data.cyclicalDay));
				$('#base_scheduledTask_hours').val(common.tools.scheduledTask.scheduledTasksManage.inputSetValue(data.taskHour));
				$('#base_scheduledTask_minute').val(common.tools.scheduledTask.scheduledTasksManage.inputSetValue(data.taskMinute));
			}
			
			if(data.timeType=='on_off'){//一次性计划
				$('#base_timeType_on_off').attr('checked',true);
				common.tools.scheduledTask.scheduledTask.everyWhatChange('on_off','base_')
				$('#base_scheduledTask_hours').val(common.tools.scheduledTask.scheduledTasksManage.inputSetValue(data.taskHour));
				$('#base_scheduledTask_minute').val(common.tools.scheduledTask.scheduledTasksManage.inputSetValue(data.taskMinute));
			}
			if(data.timeType=='cycleMinute'){//周期性计划(分钟)
				$('#base_timeType_cycleMinute').attr('checked',true);
				common.tools.scheduledTask.scheduledTask.everyWhatChange('cycleMinute','base_')
				$('#base_scheduledTask_cyclicalMinute').val(common.tools.scheduledTask.scheduledTasksManage.inputSetValue(data.cyclicalMinute));
				
			}
			if(data.taskHour!=null && data.taskHour!='')
				$('#base_scheduledTask_hours').val(common.tools.scheduledTask.scheduledTasksManage.inputSetValue(data.taskHour));
			else
				$('#base_scheduledTask_hours').val(0);
			
			if(data.taskMinute!=null && data.taskMinute!='')
				$('#base_scheduledTask_minute').val(common.tools.scheduledTask.scheduledTasksManage.inputSetValue(data.taskMinute));
			else
				$('#base_scheduledTask_minute').val(0);
			
		},
		/**
		 * 编辑定期任务时获取id
		 */
		editScheduledTask:function(){
		
			checkBeforeEditGrid('#scheduledTasksGrid',common.tools.scheduledTask.scheduledTasksManage.editScheduledTaskMethod);
			
		},
		/**
		 * 编辑定期任务
		 */
		editScheduledTaskMethod:function(rowData){
			
			$('#base_scheduledTask_etitle').val('');
			$('#scheduledTask_save_but,#base_scheduledTask_title_tr').show();
			$('#scheduledTask_ok_btn').hide();
			
			$('#scheduledTask_save_but').unbind('click');
			$('#scheduledTask_save_but').click(function(){common.tools.scheduledTask.scheduledTasksManage.saveScheduledTask('scheduledTask!editScheduledTask.action')});//保存
			resetForm('#scheduledTask_add_form');
			var _url = 'scheduledTask!findScheduledTaskById.action';
			if(rowData.scheduledTaskType=='request' || rowData.scheduledTaskType==null || rowData.scheduledTaskType=='')
				$.post(_url,'queryDTO.scheduledTaskId='+rowData.scheduledTaskId,function(data){
					basics.tab.tabUtils.refreshTab(i18n["title_edit_scheuledTask"],'../pages/common/tools/scheduledTask/editScheduledTaskFormCustom.jsp?scheduledTaskId='+rowData.scheduledTaskId);
				});
			if(rowData.scheduledTaskType=='sla'){
				$.post(_url,'queryDTO.scheduledTaskId='+rowData.scheduledTaskId,function(data){
					common.tools.scheduledTask.scheduledTasksManage.setScheduledTaskValue(data);
					windows('scheduledTask_add_win',{title:i18n['label_scheduled_sla_task'],width:520});
				});
			}
			if(rowData.scheduledTaskType=='email'){
				$.post(_url,'queryDTO.scheduledTaskId='+rowData.scheduledTaskId,function(data){
					common.tools.scheduledTask.scheduledTasksManage.setScheduledTaskValue(data);
					windows('scheduledTask_add_win',{title:i18n['label_scheduled_email_task'],width:520});
				});
			}
			if(rowData.scheduledTaskType=='configureItem'){
				$.post(_url,'queryDTO.scheduledTaskId='+rowData.scheduledTaskId,function(data){
					common.tools.scheduledTask.scheduledTasksManage.setScheduledTaskValue(data);
					windows('scheduledTask_add_win',{title:i18n['label_scheduled_configureItem_task'],width:520});
				});
			}
			
			if(rowData.scheduledTaskType=='adUpdate'){
				$.post(_url,'queryDTO.scheduledTaskId='+rowData.scheduledTaskId,function(data){
					common.tools.scheduledTask.scheduledTasksManage.setScheduledTaskValue(data);
					windows('scheduledTask_add_win',{title:i18n['label_ad_update'],width:520});
				});
			}
			
			if(rowData.scheduledTaskType=='request2problem'){
				$.post(_url,'queryDTO.scheduledTaskId='+rowData.scheduledTaskId,function(data){
					common.tools.scheduledTask.scheduledTasksManage.setScheduledTaskValue(data);
					windows('scheduledTask_add_win',{title:i18n['rule_requesttoproblem_t'],width:520});
				});
			}
			if(rowData.scheduledTaskType=='senReport'){
				$.post(_url,'queryDTO.scheduledTaskId='+rowData.scheduledTaskId,function(data){
					common.tools.scheduledTask.scheduledTasksManage.setScheduledTaskValue(data);
					windows('scheduledTask_add_win',{title:i18n['custom_report_autoSendReport'],width:520});
				});
			}
		},
		
		/**
		 * 删除定期任务
		 */
		deleteScheduledTask:function(){
			checkBeforeDeleteGrid('#scheduledTasksGrid',common.tools.scheduledTask.scheduledTasksManage.deleteScheduledTaskMethod);
		},
		/**
		 * 删除定期任务
		 */
		deleteScheduledTaskMethod:function(rowsId){
			var url='scheduledTask!deleteScheduledTask.action';
			var param = $.param({'ids':rowsId},true);
			$.post(url, param, function()
			{
				//重新统计				
				$('#scheduledTasksGrid').trigger('reloadGrid');
				msgShow(i18n['msg_deleteSuccessful'],'show');
				
			});	
		},
		
		/**
		 * 搜索定期任务
		 */
		searchScheduledTask:function(){
			if($('#scheduledTaskSearchDiv form').form('validate')){
				var sdata=$('#scheduledTaskSearchDiv form').getForm();
				var postData = $("#scheduledTasksGrid").jqGrid("getGridParam", "postData");
				$.extend(postData,sdata);
				var _url = 'scheduledTask!findPageScheduledTask.action';		
				$('#scheduledTasksGrid').jqGrid('setGridParam',{url:_url}).trigger('reloadGrid',[{"page":"1"}]);
			}
		},
		/**
		 * 保存定期任务
		 */
		saveScheduledTask:function(url){
			var myDate=new Date() 
    		var month=myDate.getMonth()+1;
			if($('#scheduledTask_add_form').form('validate')){
				if($("#scheduledTask_add_form input[name='scheduledTaskDTO.timeType']:checked").val() == "month" && $("#base_scheduledTask_monthDay").val()==null){
					msgAlert(i18n.scheduledTask_monthPlan_monthDayIsNotNull,'info');
				}else{
					if(!DateComparison($('#base_scheduledTask_endDate_input').val(),myDate.getFullYear()+'-'+month+'-'+(myDate.getDate()-1))){
						$("#base_scheduledTask_etitle").val(common.security.xssUtil.html_encode($("#base_scheduledTask_etitle").val()));
						var frm = $('#scheduledTask_add_comm_form,#scheduledTask_add_form').serialize();
						//调用
						startProcess();
						$.post(url,frm, function(res){
								endProcess();
								$('#scheduledTask_add_win').dialog('close');
								$('#scheduledTasksGrid').trigger('reloadGrid');
								basics.tab.tabUtils.addTab(i18n['title_scheduled_task_manage'],'../pages/common/tools/scheduledTask/scheduledTasksManage.jsp');
								msgShow(i18n['saveSuccess'],'show');
							
						});
					}else{   //开始时间和结束时间小于当前时间
						msgAlert(i18n['tip_endTime_cannot_be_before_startTime'],'info');
					}	
				}
			}
		},
		init:function(){
			//绑定日期控件
			DatePicker97(['#scheduledTask_search_taskDate','#scheduledTask_search_taskEndDate','#scheduledTask_search_startCreateTime','#scheduledTask_search_endCreateTime','#scheduledTask_search_startUpdateTimes','#scheduledTask_search_startUpdateTime','#base_scheduledTask_startDate_input','#base_scheduledTask_endDate_input']);
			//设定月份总天数
			common.tools.schedule.setMonthly.setmonthDay('base_everyWhat_monthly','base_scheduledTask_monthDay');
			$("#scheduledTasksManage_loading").hide();
			$("#scheduledTasksManage_content").show();
			common.tools.scheduledTask.scheduledTasksManage.scheduledTasksGrid();//加载列表
			$('#link_scheduledTask_add').click(common.tools.scheduledTask.scheduledTasksManage.addScheduledTask);//添加按钮事件
			$('#link_scheduledTask_edit').click(common.tools.scheduledTask.scheduledTasksManage.editScheduledTask);//添加按钮事件
			$('#link_scheduledTask_delete').click(common.tools.scheduledTask.scheduledTasksManage.deleteScheduledTask);//添加按钮事件

			$('#scheduledTask_search').click(function(){windows('scheduledTaskSearchDiv',{width:400,modal: true})});
			$('#scheduledTask_search_doSearch').click(common.tools.scheduledTask.scheduledTasksManage.searchScheduledTask);
			
			//选择公司
			$('#scheduledTask_search_companyName').click(function(){
				itsm.itsop.selectCompany.openSelectCompanyWin('#scheduledTask_search_companyNo','#scheduledTask_search_companyName','all');
			});
		
			$('#scheduledTaskTypeAutoSend').text(i18n.AutoSendReport);
			
			//加载默认公司
			common.security.defaultCompany.loadDefaultCompany('#base_scheduledTask_companNo','');
			
			$('#scheduledTaskType_confirm').click(common.tools.scheduledTask.scheduledTasksManage.selectScheduledTaskConfirm);//确认选择的类型
			
			
		}
	}
}();
$(document).ready(common.tools.scheduledTask.scheduledTasksManage.init);