$package('common.tools.task');
$import('itsm.portal.schedule');
$import('itsm.portal.costCommon');
/**
 * @author QXY
 * @constructor task
 * @description "task"
 * @date 2010-11-17
 * @since version 1.0
 */
common.tools.task.task = function() {
	this.calendarTaskDiv = $('#calendarTaskDiv');
	this.opt;

	return {

		/**
		 * 可操作项格式化.
		 */
		taskActFormatter : function() {

			return $('#taslActFormatterDiv').html();
		},

		/**
		 * @description 状态格式化.
		 */
		taskStateFormat : function(cellvalue) {
			if (cellvalue == '0')
				return i18n['title_newCreate']
			if (cellvalue == '1')
				return i18n['task_label_pending']
			if (cellvalue == '2')
				return i18n['lable_complete']

		},
		taskTypeFormat:function(cellValue,options,rowObject){
			if(rowObject.type == ""){
				return i18n.lable_taskType;
			}else{
				return rowObject.type;
			}
		},
		/**
		 * @description 任务列表.
		 */
		showTaskGrid : function() {
			var params = $.extend({}, jqGridParams, {
				url : 'tasks!findPagerTask.action?taskQueryDto.owner=' + $('#task_owner').val()+"&taskQueryDto.cycle=false",
				colNames : ['ID', i18n['title'], i18n['lable_task_location'], i18n.label_bpm_task_type,i18n['status'], i18n['title_request_planStartTime'], i18n['title_request_palnCompleteTime'], i18n['common_owner'], i18n.title_creator, '', i18n['operateItems'],''],
				colModel : [{
					name : 'taskId',
					align : 'center',
					width : 45
				}, {
					name : 'title',
					align : 'left',
					width : 220,
					formatter : function(cellvalue, options, rowObject) {
						if (rowObject.title == null) {
							return "null";
						} else {
							return rowObject.title;
						}
					}
				}, {
					name : 'location',
					align : 'left',
					width : 220
				}, {
					name : 'type',
					align : 'left',
					width : 150,
					formatter : common.tools.task.task.taskTypeFormat
				}, {
					name : 'taskStatus',
					align : 'center',
					width : 150,
					//formatter : common.tools.task.task.taskStateFormat,
					hidden : true
				}, {
					name : 'startTime',
					align : 'center',
					width : 120,
					formatter : timeFormatter
				}, {
					name : 'endTime',
					align : 'center',
					width : 120,
					formatter : timeFormatter
				}, {
					name : 'owner',
					align : 'center',
					width : 120
				}, {
					name : 'creator',
					align : 'center',
					width : 80
				}, {
					name : 'allDay',
					hidden : true
				}, {
					name : 'act',
					width : 80,
					align : 'center',
					sortable : false,
					formatter : function(cell, event, data) {
						return $('#taslActFormatterDiv').html().replace(/{taskId}/g, data.taskId).replace(/{startTime}/g, data.startTime).replace(/{endTime}/g, data.startTime);
					}
				}, {
					name : 'introduction',
					hidden : true
				}],
				toolbar : [true, "top"],
				jsonReader : $.extend(jqGridJsonReader, {
					id : "taskId"
				}),
				ondblClickRow : function(rowId) {
					//common.tools.task.task.portalShowMyTask(rowId)
					var rowData = $('#taskGrid').jqGrid('getRowData',rowId);
					//itsm.portal.costCommon.scheduleShow1Init('false');
					//itsm.portal.costCommon.scheduleShow1(rowData.taskId,rowData.startTime,rowData.endTime);
					itsm.portal.costCommon.cycleTaskDetail(rowData.taskId);
				},
				sortname : 'taskId',
				pager : '#taskPager'
			});
			$("#taskGrid").jqGrid(params);
			$("#taskGrid").navGrid('#taskPager', navGridParams);
			// 列表操作项
			$("#t_taskGrid").css(jqGridTopStyles);
			$("#t_taskGrid").append($('#taskGridToolbar').html());
			// 自适应宽度
			setGridWidth("#taskGrid", "regCenter", 9);
		},
		portalShowMyTask1:function(taskId,start,end){
			//itsm.portal.costCommon.scheduleShow1Init('false');
			//itsm.portal.costCommon.scheduleShow1(taskId,start,end);
			itsm.portal.costCommon.cycleTaskDetail( taskId );
		},
		/**
		 * 根据id查询任务
		 * @param id 任务id
		 */
		portalShowMyTask : function(id) {
			var postUrl = "tasks!findByTaskId.action";
			var param = "taskId=" + id;
			$("#addcalendarTreatmentResultsTr").hide();
			$.post(postUrl, param, function(taskDto) {
				itsm.portal.schedule.historyShow(id);
				$('#schedule_task_process_but').show();
				$('#schedule_task_closed_but').show();
				$('#schedule_task_remark_but').show();
				$('#calendarResults_tr').hide();
				$('#calendarTask_taskId').val(id);
				if (taskDto.title != null) {
					$('#calendarTaskTitle').text(taskDto.title);
				} else {
					$('#calendarTaskTitle').text("null");
				}
				if(taskDto.type!=null && taskDto.type!=""){
					$("#taskType").text(taskDto.type);
				}else{
					$("#taskType").text(i18n.lable_taskType);
				}
				
				$('#calendarTaskLocation').text(taskDto.location);
				$('#calendarTaskIntroduction').text(taskDto.introduction);
				$('#calendarTaskCreator').text(taskDto.creator);
				$('#calendarTaskOwner').text(taskDto.owner);
				$('#calendarTaskStartTime').text(timeFormatter(taskDto.startTime));
				$('#calendarTaskEndTime').text(timeFormatter(taskDto.endTime));
				$('#calendarRealStartTime').text(taskDto.realStartTime);
				$('#calendarRealEndTime').text(taskDto.realEndTime);
				$('#calendarRealFree').text(Math.floor(taskDto.realFree * 1 / 1440) + i18n.label_slaRule_days + " " + Math.floor(taskDto.realFree * 1 % 1440 / 60) + i18n.label_slaRule_hours + " " + Math.floor(taskDto.realFree * 1 % 1440 % 60) + i18n.label_slaRule_minutes);
				if (taskDto.taskStatus == '0') {
					$('#calendarTaskStatus').text(i18n['title_newCreate']);
					$('#schedule_task_closed_but').hide();
				}
				if (taskDto.taskStatus == '1') {
					$('#calendarTaskStatus').text(i18n['task_label_pending']);
					$('#schedule_task_process_but').hide();
				}
				if (taskDto.taskStatus == '2') {
					$('#calendarTaskStatus').text(i18n['lable_complete']);
					$('#schedule_task_process_but').hide();
					$('#schedule_task_closed_but').hide();
					$('#calendarResults_tr').show();
					$('#calendarResults').text(taskDto.treatmentResults);
				}
				if (taskDto.allDay == 'true' || taskDto.allDay) {
					$('#calendarTaskAllDay').text(i18n['label_basicConfig_deafultCurrencyYes']);
				} else {
					$('#calendarTaskAllDay').text(i18n['label_basicConfig_deafultCurrencyNo']);
				}

				if (taskDto.ownerLoginName != userName) {
					$('#schedule_task_process_but').hide();
					$('#schedule_task_closed_but').hide();
				}
				$('#schedule_task_delete_but').hide();
				$('#schedule_task_edit_but').hide();
				$('#schedule_task_process_but').unbind().click(function() {
					common.tools.task.task.processCalendarTask(id)
				});
				$('#schedule_task_closed_but').unbind().click(function() {
					common.tools.task.task.closedCalendarTask_opt(id)
				});
				$('#schedule_task_remark_but').unbind().click(function() {
					common.tools.task.task.remarkCalendarTask_opt(id)
				});
				$("#addcalendarTreatmentResults").val("");
				windows('calendarTaskDiv', {
					width : 600,
					modal : true
				});
			});
		},

		/**
		 * 任务开始处理
		 * @param id 任务id
		 */
		processCalendarTask : function(id) {
			var _url = "tasks!editTaskDetail.action";
			$.post(_url, 'taskDto.taskId=' + id + "&taskDto.taskStatus=" + 1 + "&taskDto.operator=" + userName, function() {
				calendarTaskDiv.dialog('close');
				msgShow(i18n.lable_startProcess, "show");
				common.tools.task.task.portalShowMyTask(id);
				$('#taskGrid').trigger('reloadGrid');
			});
		},

		/**
		 * 关闭任务时打开面板
		 * @param id 任务id
		 */
		closedCalendarTask_opt : function(id) {
			$.post("tasks!taskRealTimeDefaultValue.action", 'taskId=' + id, function(taskDto) {
				calendarTaskDiv.dialog('close');
				resetForm("#realProcessTaskfm");
				$('#schedule_process_taskId').val(id);
				$('#schedule_process_realStartTime').val(taskDto.realStartTime);
				$('#schedule_process_realEndTime').val(taskDto.realEndTime);
				$('#schedule_process_taskCostDay').val(Math.floor(taskDto.realFree * 1 / 1440));
				$('#schedule_process_taskCostHour').val(Math.floor(taskDto.realFree * 1 % 1440 / 60));
				$('#schedule_process_taskCostMinute').val(Math.floor(taskDto.realFree * 1 % 1440 % 60));
				$("#schedule_process_realFree").val(taskDto.realFree);
				//bindControl('#schedule_process_realEndTime,#schedule_process_realStartTime');
				$('#schedule_link_task_closedTask').unbind().click(function() {
					common.tools.task.task.closedCalendarTask()
				})
				windows('realProcessTaskDiv', {
					width : 600
				});
			});

		},

		/**
		 * 关闭任务
		 */
		closedCalendarTask : function() {
			var _url = "tasks!closedTaskDetail.action";
			if ($('#realProcessTaskfm').form('validate')) {
				var _param = $('#realProcessTaskDiv form').serialize();
				if (($('#schedule_process_realEndTime').val() != "" && $('#schedule_process_realStartTime').val() != "" && $('#schedule_process_realFree').val() >= 1)) {
					startProcess();
					$.post(_url, _param + "&taskDto.taskStatus=" + 2, function() {
						calendarTaskDiv.dialog('close');
						msgShow(i18n.lable_process_closedTask, "show");
						$('#realProcessTaskDiv').dialog('close');
						var taskId = $('#schedule_process_taskId').val();
						common.tools.task.task.portalShowMyTask(taskId);
						$('#taskGrid').trigger('reloadGrid');
						endProcess(); 
					});
				} else {
					msgAlert(i18n.labe_taskClosedMsg, 'info');
				}
			}
		},

		/**
		 * 打开任务备注
		 * @param id 任务id
		 */
		remarkCalendarTask_opt : function(id) {
			$("#calendarTreatmentResults_tr").hide();
			$("#addcalendarTreatmentResultsTr").removeAttr("style");
			$("#schedule_link_task_remarkTask").unbind().click(function() {
				common.tools.task.task.remarkCalendarTask(id)
			});
		},
		/**
		 * 保存任务备注
		 * @param id 任务id
		 */
		remarkCalendarTask : function(id) {
			var _url = "tasks!remarkTaskDetail.action";
			var treatmentResults = $("#addcalendarTreatmentResults").val();
			startProcess();
			$.post(_url, 'taskDto.taskId=' + id + "&taskDto.treatmentResults=" + treatmentResults + "&taskDto.operator=" + userName, function() {
				$("#addcalendarTreatmentResultsTr").attr("style", "display:none;");
				$("#addcalendarTreatmentResults").val("");
				calendarTaskDiv.dialog('close');
				common.tools.task.task.portalShowMyTask(id);
				endProcess(); 
			});
		},

		/**
		 * @description 任务
		 */
		addEditTask_win : function() {
			resetForm('#myTaskForm');
			opt = 'saveTask';
			$("#task_taskId").val(0);
			windows('addOrEditTaskDiv', {
				width : 500,
				close : function() {
					$('#addOrEditTaskDiv').click();
				}
			});
		},
		addEditTask_win1 : function() {
			$('#schedule_type,#schedule_task_owner').removeAttr('disabled');
			$('#scheduleAddEditTaskDiv input,#scheduleAddEditTaskDiv textarea').removeAttr('disabled');
			$("#schedule_taskCycle_tr,#schedule_task_owner_tr,#schedule_type,#schedule_task_taskType_tr").show();
			$("#schedule_type_val").hide();
			itsm.portal.schedule.addEditTask_win();
		},
		/**
		 * @description 保存或编辑.
		 */
		addEditTask_opt : function() {

			var titleTask = $('#taskTitle').val();
			$('#taskTitle').val(titleTask.replace(/\s+/g, ""));

			if ($('#myTaskForm').form('validate')) {
				if(opt=='saveTask'){
					$('#task_taskRealTime').val('');
					$('#task_taskRealEndTime').val('');
					$("#task_realFree").val('');
				}
				var _param = $('#addOrEditTaskDiv form').serialize();
				var _url = 'tasks!' + opt + '.action';
				startLoading();
				if (document.getElementById("taskStatus2").checked) {
					common.tools.task.task.saveTask_opt(_url, _param);
				} else {
					$.post('tasks!timeConflict.action', _param, function(data) {
						if (data) {
							endLoading();
							msgConfirm(i18n['msg_msg'], '<br/>' + i18n['timeConflictTip'], function() {
								common.tools.task.task.saveTask_opt(_url, _param);
							});
						} else {
							common.tools.task.task.saveTask_opt(_url, _param);
						}

					})
				}
				/*
				 * }else{ common.tools.task.task.saveTask_opt(_url,_param); }
				 */

			}
		},

		/**
		 * @description 保存任务
		 * @param _url 保存后台路径
		 * @param 参数
		 */
		saveTask_opt : function(_url, _param) {
			$.post(_url, _param, function() {
				$('#addOrEditTaskDiv').dialog('close');
				$('#taskGrid').trigger('reloadGrid');
				resetForm('#myTaskForm');
				endLoading();
				msgShow(i18n['saveSuccess'], 'show');

			});
		},

		/**
		 * @description 删除任务.
		 */
		deleteTask_aff : function() {
			checkBeforeDeleteGrid('#taskGrid', common.tools.task.task.task_deleteTask1);
		},

		/**
		 * @description 删除任务.
		 * @param taskId 任务编号
		 */
		deleteTask : function(taskId) {
			msgConfirm(i18n['msg_msg'], '<br/>' + i18n['msg_confirmDelete'], function() {
				common.tools.task.task.task_deleteTask(taskId);
			});
		},

		/**
		 * @description 执行删除.
		 * @param rowIds 任务id数组
		 */
		task_deleteTask : function(rowIds) {
			var _param = $.param({
				'taskIds' : rowIds
			}, true);
			$.post("tasks!findTaskByIds.action", _param, function(res) {
				if (res || operationTaskRes) {
					var _param = $.param({
						'taskIds' : rowIds
					}, true);
					$.post("tasks!deleteTask.action", _param, function() {
						$('#taskGrid').trigger('reloadGrid');
						msgShow(i18n['deleteSuccess'], 'show');
					}, "json");
				} else {
					msgAlert(i18n.error403, 'info');
				}
			});

		},
		/**
		 * @description 执行删除.
		 * @param rowIds 任务id数组
		 */
		task_deleteTask1 : function(rowIds) {
			var taskIds = new Array();
			for ( var i in rowIds) {
				var rowData = $('#taskGrid').jqGrid('getRowData',rowIds[i]);
				taskIds[i] = rowData.taskId;
			}
			if (taskIds.length > 0) {
				var _param = $.param({
					'taskIds' : taskIds
				}, true);
				$.post("tasks!findTaskByIds.action", _param, function(res) {
					if (res || operationTaskRes) {
						var _param = $.param({
							'taskIds' : taskIds
						}, true);
						$.post("tasks!deleteTask.action", _param, function() {
							$('#taskGrid').trigger('reloadGrid');
							msgShow(i18n['deleteSuccess'], 'show');
						}, "json");
					} else {
						msgAlert(i18n.error403, 'info');
					}
				});
			}

		},
		/**
		 * @description 打开搜索公告窗口.
		 */
		search_openwindow : function() {

			$('#searchVisibleState').click(function() {
				if ($('#searchVisibleState').attr("checked")) {
					$('#searchVisibleState').val(1)
				} else
					$('#searchVisibleState').val(0)
			});

			windows('searchTask', {
				width : 450
			});
		},

		/**
		 * @description 提交查询任务.
		 */
		search_do : function() {
			if ($('#searchTask').form('validate')) {
				var sdata = $('#searchTask form').getForm();
				var postData = $("#taskGrid").jqGrid("getGridParam", "postData");
				$.extend(postData, sdata); // 将postData中的查询参数加上查询表单的参数

				$('#taskGrid').trigger('reloadGrid', [{
					"page" : "1"
				}]);
				return false;
			}
		},

		/**
		 * @description 打开编辑窗口.
		 * @param rowData 列表汗对象
		 */
		editTask_win : function(rowData) {
			$.post("tasks!findByTaskId.action", 'taskId=' + rowData.taskId, function(res) {
				if (res.taskCreator == userName || operationTaskRes) {
					resetForm('#myTaskForm');
					if (res.taskStatus == '0') {
						$('#taskStatus').attr("checked", 'true')
					} else if (res.taskStatus == '1') {
						$('#taskStatus1').attr("checked", 'true');
					} else {
						$('#taskStatus2').attr("checked", 'true');
					}
					if (res.allDay == 'true' || res.allDay) {
						$('#taskAllDay').attr("checked", 'true');
					} else {
						$('#taskAllDay').attr("checked", '');
					}
					$("#task_taskId").val(res.taskId);
					if (res.title != null) {
						$("#taskTitle").val(res.title);
					} else {
						$("#taskTitle").val("null");
					}
					$("#taskLocation").val(res.location);
					$("#taskIntroduction").val(res.introduction);
					$("#taskStartTime").val(res.startTime);
					$("#taskEndTime").val(res.endTime);
					$("#task_owner").val(res.ownerLoginName);
					$("#task_creator").val(res.taskCreator);
					$('#task_taskRealTime').val(res.realStartTime);
					$('#task_taskRealEndTime').val(res.realEndTime);
					$("#task_realFree").val(res.realFree);
					windows('addOrEditTaskDiv', {
						width : 500,
						close : function() {
							$('#addOrEditTaskDiv').click();
						}
					});
				} else {
					msgAlert(i18n.error403, 'info');
				}
			});

		},
		editTask_win1: function(rowData) {
			itsm.portal.schedule.editCalendarTask( rowData.taskId );
		},
		/**
		 * @description edit(Get the value of which reached the grid edit page)
		 */
		editTask_aff : function() {

			var rowId = jQuery("#taskGrid").jqGrid('getGridParam', 'selrow');
			if (rowId == null) {
				msgAlert(i18n['msg_atLeastChooseOneData'], 'info');
			} else {
				opt = "editTask";
				var rowData = $("#taskGrid").getRowData(rowId);
				common.tools.task.task.editTask_win1(rowData);
			}
		},
		editTask1 : function(taskId) {
			/*var rowId = taskId;
			if (rowId == null) {
				var rowData = $("#taskGrid").getRowData(rowId);
				common.tools.task.task.editTask_win1(rowData);
			}*/
			itsm.portal.schedule.editCalendarTask( taskId );
		},
		/**
		 * @description edit(Get the value of which reached the grid edit page)
		 * @param taskId 任务id
		 */
		editTask : function(taskId) {
			var rowId = taskId;
			if (rowId == null) {
				msgAlert(i18n['msg_atLeastChooseOneData'], 'info');
			} else {
				opt = "editTask";
				var rowData = $("#taskGrid").getRowData(rowId);
				common.tools.task.task.editTask_win(rowData);
			}
		},

		init : function() {
			$('input[name="taskDto.taskCycle"]').change(itsm.portal.schedule.scheduleTaskCycle);
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('taskType','#schedule_type');
			
			itsm.portal.costCommon.monthFristDay( );
			itsm.portal.costCommon.monthLastDay( );
			commonWdatePicker({dateFmt:'yyyy-MM-dd'
				,onpicked:function(){
					var st = jsStringToDate( $("#searchTaskStartTime").val() );
					var se = jsStringToDate( $("#searchTaskEndTime").val() );
					if ( st.getTime() > se.getTime() ) {
						$("#searchTaskEndTime").val( $("#searchTaskStartTime").val() );
					}
					$("#searchTaskEndTime").trigger("click");
				}
			},'#searchTaskStartTime');
			commonWdatePicker({dateFmt:'yyyy-MM-dd',minDate:'#F{$dp.$D(\'searchTaskStartTime\')}'
				},'#searchTaskEndTime');
			$("#myTask_loading").hide();
			$("#myTask_content").show();
			common.tools.task.task.showTaskGrid();
			$('#link_task_save_edit_ok').click(common.tools.task.task.addEditTask_opt);
			$('#link_task_search_ok').click(common.tools.task.task.search_do);
			bindControl('#taskEndTime,#taskStartTime');
			$('#searchOwner_link').click(function() {
				common.security.userUtil.selectUser('#searchOwner', '', '', 'fullName', '')
			});// 选择技术员
		}

	}
}();
$(document).ready(common.tools.task.task.init);
