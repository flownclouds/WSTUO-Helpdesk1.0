/**
 * FileName: import.js
 *
 * File description goes here.
 *
 * Copyright (c) 2010 wstuo. All Rights Reserved.
 *
 * @author <a href="mailto:wstuowang@msn.com">wstuo Wang</a>
 * @Version: 1.0.0
 * @DateTime: 2010-02-22
 */

function $package(ns) {
	if (typeof(ns) != "string")
		return;
	ns = ns.split(".");
	var o, ni;
	for (var i = 0, len = ns.length;i < len, ni = ns[i]; i++) {
		try {
			o = (o ? (o[ni] = o[ni] || {}) : (eval(ni + "=" + ni + "||{}")))
		} catch (e) {
			o = eval(ni + "={}")
		}
	}
}
