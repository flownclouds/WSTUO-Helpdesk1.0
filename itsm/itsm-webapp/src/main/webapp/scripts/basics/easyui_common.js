function msgAlert(msg,type){
	var content = msg;
	var confirm = i18n['Confirm'];
	switch(type){
		case "error":
			content="<div class=\"messager-icon messager-error\"></div>"+msg;
		break;
		case "info":
			content="<div class=\"messager-icon messager-info\"></div>"+msg;
		break;
		case "question":
			content="<div class=\"messager-icon messager-question\"></div>"+msg;
		break;
		case "warning":
			content="<div class=\"messager-icon messager-warning\"></div>"+msg;
		break;
	}
	
	$('#alertMSG').html(content);
	
	var buttons = {};
	 buttons[confirm] = function() {
		 $(this).dialog('close');
	 };  
	$('#alertMSG').dialog(
	        $.extend({}, jqueryDialog, 
                {
                    title:i18n['tip'],
                    width:320,
                    Open:true,
                    show: 'blind',
                    hide: 'explode',
                    modal: true,
                    close:function(){
                    },
                    buttons: buttons
                }
	        )
	    );
	
	setTimeout(function(){
        // 需要先判断按钮的文字是 确认才能获取焦点
        $('.ui-dialog-buttonpane .ui-button-text').each(function(){
            if (confirm == $(this).text()) {
                $(this).parent().focus();
            }
        });
    }, 300);
	
}
function msgConfirm(title,content,event){
	if(title==''){
		title = i18n['tip'];
	}
	var confirm = i18n['Confirm'];
	var cancel = i18n['Cancel'];
	var buttons = {};
	buttons[cancel] = function() {
		 $(this).dialog('close');
		 endProcess();
	};  
	buttons[confirm] = function() {
		$(this).dialog('close');
		event();
	};
	$('#alertMSG').html(content);
	$('#alertMSG').dialog( $.extend({},jqueryDialog, {width:380,height:'auto',title:title,autoOpen:true,show: 'blind',hide: 'explode',modal: true,
		close:function(){
			$('.ui-dialog #alertMSG').parent().find('.ui-dialog-buttonpane').remove();
			$(document).unbind('keypress');
		},buttons: buttons
	}));
	
	setTimeout(function(){
	    // 需要先判断按钮的文字是 确认才能获取焦点
	    $('.ui-dialog-buttonpane button').each(function(){
	        
	        var $button = $(this);
	        var $buttonText = $button.children('.ui-button-text').text();
	        
	        if ($buttonText == confirm) {
	            $button.focus();
	            $(document).bind('keypress',function(event){  
	                if(event.keyCode == '37') {
	                    $('.ui-dialog-buttonpane button').each(function(){
                            $(this).blur();
	                    });
	                    $button.focus();
	                }
	            });
	        } else if ($buttonText == cancel) {
	            $(document).bind('keypress',function(event){  
                    if(event.keyCode == '39') {
                        $('.ui-dialog-buttonpane button').each(function(){
                            $(this).blur();
                        });
                        $button.focus();
                    }
                });
	        }
//	        $(this).parent().focus();
//            $(document).bind('keypress',function(event){  
//                if(event.keyCode == '37') {
//                    $(this).parent().addClass('ui-state-default');
//                    $(this).parent().focus();
//                }
//            });  
	    });
	}, 300);
	
}

function msgShow(msg,type){
	var content = msg;
	switch(type){
		case "error":
			content="<div class=\"messager-icon messager-error\"></div>"+msg;
		break;
		case "info":
			content="<div class=\"messager-icon messager-info\"></div>"+msg;
		break;
		case "question":
			content="<div class=\"messager-icon messager-question\"></div>"+msg;
		break;
		case "warning":
			content="<div class=\"messager-icon messager-warning\"></div>"+msg;
		break;
	}
	content = "<div>"+content+"</div>";
	var closeIMG = "<div class='closeIMG' style='background-color: #DCE7FB;'><table width='100%' style='margin-bottom:5px;'><tr><td align='left'>"+i18n['tip']+"</td><td  align='right'><a href='#'> X </a></td></tr></table></div>";
	var str = "<div style='text-align: center;font-size: 14px;font-weight: bold;'>"+closeIMG+"<br>"+content+"</div>";
	var msgBox = $(str).floatingMessage({
         position : "bottom-right",
         width : 250,
         height:100,
         click:false
     });
	
	$('.closeIMG').click(function(){
		msgBox.floatingMessage("destroy");
	});
	 setTimeout(function(){
		 msgBox.floatingMessage("destroy");
     },2000);
}
function msgAssginShow(msg,type,page,total){
	var content = msg;
	switch(type){
		case "error":
			content="<div class=\"messager-icon messager-error\"></div>"+msg;
		break;
		case "info":
			content="<div class=\"messager-icon messager-info\"></div>"+msg;
		break;
		case "question":
			content="<div class=\"messager-icon messager-question\"></div>"+msg;
		break;
		case "warning":
			content="<div class=\"messager-icon messager-warning\"></div>"+msg;
		break;
	}
	content = "<div id='assginTipContent'>"+content+"</div>";
	var closeIMG = "<div class='closeIMG' style='background-color: #DCE7FB;font-weight: bold;'><table width='100%' style=''><tr><td align='left'>"+i18n['label_assginTip']+"</td><td  align='right'><a href='#'> X </a></td></tr></table></div>";
	var str = "<div style='font-size: 14px;'>"+closeIMG+"<br><div><div style='float:left'><img src=../images/icons/dwrMessageTip.png width=48px; height=48px;></div><div style='padding-left:5px;padding-top:5px;'>"+content+"</div></div><div style='position: fixed;bottom: 5px;'><div style='padding-left:5px;float:left;'><a id='upPageButton'>"+i18n['label_assginMessage_up']+"<a>  <span id='nowPage'>"+page+"</span>/"+total+"   <a id='nextPageButton'>"+i18n['label_assginMessage_next']+"</a></div><div style='float:right;padding-left:100px;'><a onclick='openImManage();'>"+i18n['more']+"</a></div></div></div>";
	var msgBox = $(str).floatingMessage({
         position : "bottom-right",
         width : 250,
         height:120,
         click:false
     });
	
	$('.closeIMG').click(function(){
		msgBox.floatingMessage("destroy");
	});
}

function show(title,msg,type,time){
	var content = msg;
	switch(type){
		case "error":
			content="<div class=\"messager-icon messager-error\"></div>"+msg;
		break;
		case "info":
			content="<div class=\"messager-icon messager-info\"></div>"+msg;
		break;
		case "question":
			content="<div class=\"messager-icon messager-question\"></div>"+msg;
		break;
		case "warning":
			content="<div class=\"messager-icon messager-warning\"></div>"+msg;
		break;
	}
	content = "<div>"+content+"</div>";
	if(title==''||title==null){
		var closeIMG = "<div class='closeIMG' style='background-color: #DCE7FB;'><table width='100%' style='margin-bottom:5px;'><tr><td align='left'>"+i18n['tip']+"</td><td  align='right'><a href='#'> X </a></td></tr></table></div>";
	}else{
		var closeIMG = "<div class='closeIMG' style='background-color: #DCE7FB;'><table width='100%' style='margin-bottom:5px;'><tr><td align='left'>"+title+"</td><td  align='right'><a href='#'> X </a></td></tr></table></div>";
	}
	var str = "<div style='text-align: center;font-size: 14px;font-weight: bold;'>"+closeIMG+"<br>"+content+"</div>";
	var msgBox = $(str).floatingMessage({
         position : "bottom-right",
         width : 250,
         height:100,
         click:false
     });
	
	$('.closeIMG').click(function(){
		msgBox.floatingMessage("destroy");
	});
	 setTimeout(function(){
		 msgBox.floatingMessage("destroy");
     },time);
}

function showAndHidePanel(showId,hiddenId){
	 $(hiddenId).hide();
	 $(showId).show();
}

function loadTreeView(treeId,queryUrl,selectNodeMethod){
	$(treeId).jstree({
		json_data: {
			ajax: {
				url :queryUrl,
				data:function(n){
			    	  return {'parentOrgNo':n.attr ? n.attr("orgNo").replace("node_",""):0};//types is in action
				},
				cache: false}
		},
		plugins : ["themes", "json_data", "ui", "crrm", "dnd"]
	})
	.bind('select_node.jstree',selectNodeMethod);
}

//打开进程窗口
function startProcess(){
	$('#processWindow').dialog($.extend({},loading, {open:function(){
		$('#processWindow').parent().css('height',70);
		$('#processWindow').parent().css('width',280);
		$('.ui-dialog #processWindow').parent().find('.ui-dialog-titlebar').remove();
	}}));
}

//关闭
function endProcess(){
	$('#processWindow').dialog('close');
}

//打开进程窗口
function startLoading(){
	$('#loading_window').dialog($.extend({},loading, {open:function(){
		$('#loading_window').parent().css('height',50);
		$('.ui-dialog #loading_window').parent().find('.ui-dialog-titlebar').remove();
	}}));
	
}

//关闭
function endLoading(){
	$('#loading_window').dialog('close');
}

/**
 * 调用jquery.bgiframe处理IE6下select控件显示在最前面
 * 
$.extend($.fn.window.defaults,{
	onBeforeOpen:function(){
		$(".panel").parent().bgiframe(); 
		$(".panel-header").parent().bgiframe();
	}
});
*/
//设置容器高度
function setPanelHeight(panelId){
	
	var height=document.getElementById("regCenter").style.height;
	height=height.substring(0,height.length-2);
	document.getElementById(panelId).style.height=height-35+"px";
}

//刷新JSTREE
function refreshTree(treeId){
	
	$(treeId).bind('loaded.jstree', function(e,data){data.inst.open_all(-1);
		}).jstree('refresh',-1); 
}

/**
 * 获取可操作项HTML.
 * @param sec_edit
 * @param sec_delete
 * @returns HTMLStr
 */
function actionFormat(sec_edit,sec_delete){
	
	var HTMLStr='';
	if(sec_edit=='1'){
		HTMLStr+='<a title="'+i18n['common_edit']+'" href="javascript:[edit]"><img src="../skin/default/images/grid_edit.png"/></a>';
	}
	if(sec_delete=='1'){
		HTMLStr+='<a title="'+i18n['label_tree_delete']+'" href="javascript:[delete]" style="margin-left:8px"><img src="../skin/default/images/grid_delete.png"/></a>';
	}

	
	return HTMLStr;
}
/**
 * 日期格式:2011-09-19
 * @param str
 * @returns
 */
function strDateTime(str)
{
var r = str.match(/^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/);
if(r==null)return false;
var d= new Date(r[1], r[3]-1, r[4]);
return (d.getFullYear()==r[1]&&(d.getMonth()+1)==r[3]&&d.getDate()==r[4]);
}
/**
 * 日期格式:2011-09-19 15:14:11
 * @param str
 * @returns
 */
function strDateTimeLeng(str)
{
var reg = /^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2}) (\d{1,2}):(\d{1,2}):(\d{1,2})$/;
var r = str.match(reg);
if(r==null)return false;
var d= new Date(r[1], r[3]-1,r[4],r[5],r[6],r[7]);
return (d.getFullYear()==r[1]&&(d.getMonth()+1)==r[3]&&d.getDate()==r[4]&&d.getHours()==r[5]&&d.getMinutes()==r[6]&&d.getSeconds()==r[7]);
}


function DateComparison(date1,date2){
	return Date.parse(date1.replace(/-/g,"/"))<=Date.parse(date2.replace(/-/g,"/"));
}
function NumberComparison(num1,num2){
   return num1<=num2;
} 

$.extend($.fn.validatebox.defaults.rules, {
	email: { 
        validator: function(value){ 
        	
        	var ck=/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/;
        	
        	return ck.test(value);
        }, 
        message: i18n['validate_email']
    },
	
    phone: { 
        validator: function(value){ 
        	//var ck  =/^((\+?[0-9]{2,4}\-[0-9]{3,4}\-)|([0-9]{3,4}\-))?([0-9]{5,8})(\-[0-9]+)?$/;
    		//mars add @20110705
    		var ck=/([0-9]{3,4}\-)?[0-9]{7,8}$|[0-9]{3,4}\-[0-9]{7,8}\-[0-9]{3,4}$|(\+086-)[0-9]{3,4}\-[0-9]{7,8}$/;
    		//var ck = /^(\(\d{3,4}\)|\d{3,4}-)?\d{7,8}$/;
    		//var ck=/(^(/d{2,4}[-_－—]?)?/d{3,8}([-_－—]?/d{3,8})?([-_－—]?/d{1,7})?$)|(^0?1[35]/d{9}$)/;
        	return ck.test(value);
        	//return true;
        }, 
        message: i18n['validate_mobile_fax']
    },
    mob:{
    		validator: function(value){ 
    			var ck = /^1[3,5,8,9]\d{9}$/;
    			return ck.test(value);
    		}, 
        message: i18n['validate_mobile_no']  
    },
    ip:{
		validator: function(value){ 
			var ck = /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/;
			return ck.test(value);
		}, 
    message: i18n['common_ip']  
    },
    money:{
			validator: function(value){ 
				var ck = /^(([0-9]+\\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\\.[0-9]+)|([0-9]*[1-9][0-9]*))$/;
				return ck.test(value);
			}, 
	    message: i18n['validate_price']
    },
    count:{
			validator: function(value){ 
				var ck = /^[0-9]*[1-9][0-9]*$/;
				return ck.test(value);
			}, 
			message: i18n['validate_integer'] 
    },
    
    /**验证两个日期比较*/
    DateComparison: {
        validator: function (value, param) {
        	var date1=$('#'+param[0]).val();
        	var date2=value;
        	if(date1==""){
        		return true;
        	}
        	return DateComparison(date1,date2);
//        	return true;
        },
        message:i18n['error_dateComparison']
    },
    
    /**验证两个整数比较*/
    NumberComparison: {
        validator: function (value, param) {
        	var num1=$('#'+param[0]).val();
        	var num2=value;
        	if(num1==""){
        		return true;
        	}
        	return NumberComparison(num1,num2);
//        	return true;
        },
        message:i18n['error_NumberComparison']
    },
    DateComparisonCI: {
        validator: function (value, param) {
        	var date1=$('#'+param[0]).val();
        	var date2=value;
        	if(date1==""){
        		return true;
        	}
        	return DateComparison(date1,date2);
        },
        message:i18n['error_dateComparisonCI']
    },
    DateComparisonHoliday: {
        validator: function (value, param) {
            var date1=$('#'+param[0]).val();
            var date2=value;
            if(date1==""){
                return true;
            }
            return DateComparison(date1,date2);
        },
        message:i18n['error_dateComparisonHoliday']
    },
	CHS: {
	    validator: function (value, param) {
	        return /^[\u0391-\uFFE5]+$/.test(value);
	    },
	    message: i18n['validate_chinese_letters']
	},
	ZIP: {
	    validator: function (value, param) {
	        return /^[1-9]\d{5}$/.test(value);
	    },
	    message:i18n['validate_ZIPCode']
	},
	QQ: {
	    validator: function (value, param) {
	        return /^[1-9]\d{4,10}$/.test(value);
	    },
	    message: i18n['validate_qq']
	},
	mobile: {
	    validator: function (value, param) {
	    	
	    	var ck = /^1[3,5,8,9]\d{9}$/;
			return ck.test(value);

	    },
	    message: i18n['error_mobile']
	},
	loginName: {
	    validator: function (value, param) {
	        return /^[a-zA-Z0-9][a-zA-Z0-9_/@.-]*$/.test(value);
	    },
	    message: i18n['error_loginName']
	},
	safepass: {
	    validator: function (value, param) {
	        return safePassword(value);
	    },
	    message: i18n['validate_password']
	},
	
	equalTo: {
	    validator: function (value, param) {
	        return value == $('#'+param[0]).val();
	    },
	    message: i18n['error_equalTo']
	},
	number: {
	    validator: function (value, param) {
	        return /^\d+$/.test(value);
	    },
	    message: i18n['validate_number']
	},
	decimal: {		//小数  will--20131213
	    validator: function (value, param) {
	        return /^-?\d+(\.\d{1,2})?$/.test(value);	
	    },
	    message: i18n['validate_number']
	},
	chars:{
		validator: function (value, param) {
	        return /^[A-Za-z]+$/.test(value);
	    },
	    message: i18n['validate_chars']
	},
	idcard: {
	    validator: function (value, param) {
	        return idCard(value);
	    },
	    message:i18n['validate_IDCard']
	},
	
	persent:{
		
		 validator: function (value) {
		        return value<=100;
		    },
		    message:i18n['validate_persent']
	},
	reDataTime:{
		validator:function (value) {
	        if(value!=null && value!=''){
	        	if(strDateTime(value) || strDateTimeLeng(value))
	        		return true;
		        else
		        	return false;
	        }else{
	        	return true;
	        }
	    },
	    message:i18n['msg_date_format']
	},
	/**
	 * 只能输入数字和字母
	 */
	numLetters: {
	    validator: function (value, param) {
	    	 return /^[a-zA-Z0-9]*$/.test(value);
	    },
	    message: i18n['label_onlyNumALetters']
	},
	/**验证两个日期比较*/
    DateComparisonAndDateReg: {
        validator: function (value, param) {
        	var date1=$('#'+param[0]).val();
        	var date2=value;
        	
        	if(date1==""){
        		return true;
        	}
        	if(strDateTime(value) || strDateTimeLeng(value)){
        		if(DateComparison(date1,date2)){
        			return true;
        		}else{
        			return false;
        		}
        	}else{
        		return false;
        	}
        	//return DateComparison(date1,date2);
        },
        message:i18n['error_dateComparison']+i18n['msg_date_format']
    },
    /***验证是否大于当前系统时间**/
    gtNowTimeReg:{
    	validator: function (value){
    		if(value==""){
        		return true;
        	}
    		var myDate=new Date() ;
    		var month=myDate.getMonth()+1;
        	return DateComparison(value,myDate.getFullYear()+'-'+month+'-'+myDate.getDate());
    	},
    	message:i18n['msg_gtNowTimeReg']
    },
    /**实际响应时间不能为空或计划开始时间不能早于实际响应时间*/
    ResponseDateReg: {
        validator: function (value, param) {
        	var date1=$('#'+param[0]).val();
        	var date2=value;
        	if(date1==""){
        		return false;
        	}
        	if(DateComparison(date1,date2)){
    			return true;
    		}else{
    			return false;
    		}
        	//return DateComparison(date1,date2);
        },
        message:i18n['error_responTimeNotNullandBigThanPlanStartTime!']
    },
    /*** 两时间大小比较*/
    twoTimeEq:{
    	validator: function (value,param){
    		var m=$('#'+param[1]).val()*60+$('#'+param[2]).val()*1;
    		var tm=$('#'+param[0]).val()*1;
    		if(tm<m){
    			return false
    		}else{
    			return true;
    		}
    	},
    	message:i18n['error_processingTimeEarlierStartToEndTime']
    	
    },
    /**名称不能使用特殊符号*/
    nameSpecialSymbolVerification: {
	    validator: function (value, param) {
	    	var pattern=RegExp(/[(\ )(\~)(\!)(\@)(\#)(\$)(\%)(\^)(\&)(\*)(\()(\))(\+)(\=)(\[)(\])(\{)(\})(\|)(\\)(\;)(\:)(\')(\")(\,)(\.)(\/)(\<)(\>)(\?)(\)]+/);
	        return !pattern.test(value);
	    },
	    message: i18n['ERROR_SLA_RULENAME']
	},
    /**密码安全策略验证*/
    passwordsecurity:{
	    validator: function (value) {
	    	var ls = 0;
			if(value.match(/([a-z])+/)){  
			    ls++;  
			}  
			if(value.match(/([0-9])+/)){  
			    ls++;    
			}  
			if(value.match(/([A-Z])+/)){  
			    ls++;  
			}
			if(value.length>38){
				return true;
			}else if(value.length<8){
				return false;
			}else if(ls!=3){
				return false;
			}else{
				return true;
			}
	    },
	    message: i18n['validate_password']
	},
	maxLength: {
        validator: function(value, param){
            return value.length <= param[0];
        },
        message: i18n['error_characterLengthTooLong']
    },
    nullValueValid:{
    	validator: function(value){
    		if(trim(value)=='null' || trim(value)=='NULL' || trim(value)=='' ){
    			return false;
    		}else{
    			return true;
    		}
        },
        message:i18n.validate_notNull
    },
    useEnCharPwd : {
        validator : function(value) {
            var exp= /^[\u0021-\u007e]{1,}$/g;
            return exp.test(value);
        },
        message : i18n.error_pwdSpecial
    },
    radioAndchekboxValid:{
    	validator: function(value,name){
    		return $("#"+name[0]+" input[name*="+name[1]+"]:checked").length>0?true:false;
        },
        message:i18n.common_pleaseChoose
    }

});

//验证
$.extend($.fn.validatebox.defaults, {
	required:false,validType:null,missingMessage:i18n.validate_notNull,invalidMessage:null
});