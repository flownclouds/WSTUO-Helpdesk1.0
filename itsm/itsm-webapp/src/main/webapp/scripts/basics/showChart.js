$package('basics');
 /**  
 * @author ciel  
 * @description "列表下方饼图数据显示"
 * @date 2014-11
 * @since version 1.0 
 */ 
basics.showChart = function (){
	return {
		/**
		 * 饼图数据显示
		 * @param module 模块
		 */
		showChart:function(module){
			if(module=="request"){
				if(!isAllRequest_Res){//是否允许查看全部权限
					basics.showChart.requestShowChart('myProposedRequest');
				}else{
					basics.showChart.requestShowChart('all');
				}
			}else if(module=="change"){
				basics.showChart.changeShowChart('countAllChanges');
			}else if(module=="problem"){
				basics.showChart.problemShowChart('all');
			}else if(module=="release"){
				basics.showChart.releaseShowChart("countAllRelease");
			}
		},
		/**
		 * 请求饼图数据显示
		 */
		requestShowChart:function(type){
			$.post('request!requestFlotPieData.action','requestQueryDTO.countQueryType='+type+'&requestQueryDTO.groupField=status&requestQueryDTO.loginName='+userName,function(data){
	        	basics.bottomMain.showTableChart("requestPie1",data,"request",type);
	        });
	        $.post('request!requestFlotPieData.action','requestQueryDTO.countQueryType='+type+'&requestQueryDTO.groupField=priority&requestQueryDTO.loginName='+userName,function(data){
	        	basics.bottomMain.showTableChart("requestPie2",data,"request",type);
	        });
	        $.post('request!requestFlotPieData.action','requestQueryDTO.countQueryType='+type+'&requestQueryDTO.groupField=slaState&requestQueryDTO.loginName='+userName,function(data){
	        	basics.bottomMain.showTableChart("requestPie3",data,"request",type);
	        });
		},
		/**
		 * 变更饼图数据显示
		 */
		changeShowChart:function(type){
			$.post("change!changeFlotPieData.action",'queryDTO.groupField=status&queryDTO.lastUpdater='+userName+'&queryDTO.countQuery='+type+'&queryDTO.creator='+userName,function(data){
				basics.bottomMain.showTableChart("changePie1",data,"change",type);
			});
			$.post("change!changeFlotPieData.action",'queryDTO.groupField=priority&queryDTO.lastUpdater='+userName+'&queryDTO.countQuery='+type+'&queryDTO.creator='+userName,function(data){
				basics.bottomMain.showTableChart("changePie2",data,"change",type);
			});
			$.post("change!changeFlotPieData.action",'queryDTO.groupField=category&queryDTO.lastUpdater='+userName+'&queryDTO.countQuery='+type+'&queryDTO.creator='+userName,function(data){
				basics.bottomMain.showTableChart("changePie3",data,"change",type);
			});
		},
		/**
		 * 问题饼图数据显示
		 */
		problemShowChart:function(type){
			
			$.post("problem!problemFlotPieData.action",'queryDTO.groupField=status&queryDTO.userName='+userName+'&queryDTO.countQueryType='+type,function(data){
				basics.bottomMain.showTableChart("problemPie1",data,"problem",type);
			});
			$.post("problem!problemFlotPieData.action",'queryDTO.groupField=priority&queryDTO.userName='+userName+'&queryDTO.countQueryType='+type,function(data){
				basics.bottomMain.showTableChart("problemPie2",data,"problem",type);
			});
			$.post("problem!problemFlotPieData.action",'queryDTO.groupField=category&queryDTO.userName='+userName+'&queryDTO.countQueryType='+type,function(data){
				basics.bottomMain.showTableChart("problemPie3",data,"problem",type);
			});
		},
		/**
		 * 发布饼图数据显示
		 */
		releaseShowChart:function(type){
			$.post('release!flotPieData.action','releaseQueryDTO.optType='+type+
					'&releaseQueryDTO.groupField=status&releaseQueryDTO.loginName='+userName,function(data){
				basics.bottomMain.showTableChart("releasePie1",data,"release",type);
	        });
			$.post('release!flotPieData.action','releaseQueryDTO.optType='+type+
					'&releaseQueryDTO.groupField=priority&releaseQueryDTO.loginName='+userName,function(data){
				basics.bottomMain.showTableChart("releasePie2",data,"release",type);
	        });
			$.post('release!flotPieData.action','releaseQueryDTO.optType='+type+
					'&releaseQueryDTO.groupField=category&releaseQueryDTO.loginName='+userName,function(data){
				basics.bottomMain.showTableChart("releasePie3",data,"release",type);
	        });
			
		},
		/**
		 * 配置项饼图数据显示
		 */
		cimShowChart:function(){
			$.post("ci!ciFlotPieData.action",'ciQueryDTO.groupField=status&ciQueryDTO.loginName='+userName,function(data){
				basics.bottomMain.showTableChart("configureItemPie1",data,"configureItem");
				//setTimeout(function () {basics.bottomMain.resizeWithCimChart();}, 100);
			});
			$.post("ci!ciFlotPieData.action",'ciQueryDTO.groupField=loc&ciQueryDTO.loginName='+userName,function(data){
				basics.bottomMain.showTableChart("configureItemPie2",data,"configureItem");
				//setTimeout(function () {basics.bottomMain.resizeWithCimChart();}, 100);
			});
		}
		
	};
}();