/**  
 * @fileOverview "dwr消息逆推管理"
 * @author Wstuo  
 * @constructor Wstuo
 * @description dwr消息逆推管理
 * @date 2012-12-13
 * @version 1.0  
 * @since version 1.0 
 */
var userAssgin=new function(){
	
	var g_blinkid = 0;
	var g_blinkswitch = 0;
	var g_blinktitle = document.title;
	
	blinkNewMsg=function(){
		document.title = g_blinkswitch % 2==0 ? "【　　　　】 - " + g_blinktitle : "【"+messageSize+""+i18n['label_assgin_dwr_title']+"】 - " + g_blinktitle;
		 g_blinkswitch++;
	}
	stopBlinkNewMsg=function(){
		 if(g_blinkid)
		    {
		        clearInterval(g_blinkid);
		        g_blinkid = 0;
		        document.title = g_blinktitle;
		    }
	}
	this.userAssginOut=function(data){
		var msg='';
		var title='';
		var i=0;
		var oldSize=0;
		if(data!=null){
			if(data[i].title.length>6){//标题太长处理
				title=data[i].title.substring(0,6)+"...";
			}else{
				title=data[i].title;
			}
			if(data[i].imType=='assginNotice'){
				msg="<a href=\"JavaScript:assginMessageRequestDetails("+data[i].requestEno+","+data[i].imId+")\">"+i18n['title']+"："+title+""+data[i].content.replace('的请求指派给您，请及时处理',i18n['label_request_dwrMessage'])+"</a>";
			}else{
				var content="";
				if(data[i].content.length>24){//标题太长处理
					content=data[i].content.substring(0,24)+"...";
				}else{
					content=data[i].content;
				}
				msg="<a href=\"JavaScript:instantDetails("+data[i].imId+")\">"+i18n['title']+"："+title+"<br>"+i18n['common_content']+"："+content+"</a>";
			}
			msgAssginShow(msg,'show',i+1,data.length);
			$("#nextPageButton").click(function(){//下一条
				if(i==data.length-1){
					i=0;
				}else{
					i+=1;
				}
				$('#nowPage').text(i+1);
				pageContent(i,data);
			});
			$("#upPageButton").click(function(){//上一条
				if(i==0){
					i=data.length-1;
					$('#nowPage').text(data.length);
				}else{
					$('#nowPage').text(i);
					i-=1;
				}
				pageContent(i,data);
			});
			oldSize=messageSize;
			messageSize=data.length;
			if(oldSize==0){
				g_blinkid = setInterval(function(){blinkNewMsg()}, 1000);//页面标题闪烁
			}
		}
	}
	instantDetails=function(id){
		$("#IM"+id).html("("+i18n['label_im_readed']+")");
		var postUrl="immanage!findInstantmessageById.action";
		var param="imDTO.imId="+id;
		$.post(postUrl,param,function(imDTO){
			$('#im_sendUser').text(imDTO.sendUsers);
			$('#im_sendTime').text(
					timeFormatter(imDTO.sendTime));
			$('#im_title').text(imDTO.title);
			var content='';
			if(imDTO.sendUsers=='system' && imDTO.eventType.indexOf("TaskDTO")<0){
				content='<a onclick=look_Details("'+imDTO.eventType+'",'+imDTO.requestEno+','+imDTO.imId+',"lookMessageDiv") >'+imDTO.content+'</a>';
			}else{
				content=imDTO.content;
			}
			$('#im_content').html(content);
			
			if (imDTO.status == '0') {
				$('#im_stats').text(i18n['label_im_notRead'])
			}
			if (imDTO.status == '1') {
				$('#im_stats').text(i18n['label_im_readed']);
			}
			if (imDTO.status == '2') {
				$('#im_stats').text(i18n['label_im_replyed']);
			}
			$('.WSTUO-dialog').dialog('close');
			windows('lookMessageDiv',{width: 400,modal: true});
			if(imDTO.status=='0'){
				$.post('immanage!update.action','imDTO.imId='+imDTO.imId+'&imDTO.status=1',function(){
					$('#IMjqGrid').trigger('reloadGrid');
				})
				if(messageSize==1){
					stopBlinkNewMsg();
				}
				messageSize-=1;
			}
		});
	}
	assginMessageRequestDetails=function(eno,imId){
		basics.tab.tabUtils.reOpenTab("request!requestDetails.action?eno="+eno,i18n["request_detail"]);
		var postUrl="immanage!findInstantmessageById.action";
		var param="imDTO.imId="+imId;
		$.post(postUrl,param,function(imDTO){
			if(imDTO.isReadRequest=='0'){
				$.post('immanage!update.action','imDTO.imId='+imId+'&imDTO.status=1&imDTO.isReadRequest=1',function(){
					$('#IMjqGrid').trigger('reloadGrid');
				})
				if(messageSize==1){
					stopBlinkNewMsg();
				}
				messageSize-=1;
			}
		});
	}
	pageContent=function(i,data){
		if(data[i].title.length>6){//标题太长处理
			title=data[i].title.substring(0,6)+"...";
		}else{
			title=data[i].title;
		}
		if(data[i].imType=='assginNotice'){
			msg="<a href=\"JavaScript:assginMessageRequestDetails("+data[i].requestEno+","+data[i].imId+")\">"+i18n['title']+"："+title+""+data[i].content.replace('的请求指派给您，请及时处理',i18n['label_request_dwrMessage'])+"</a>";
		}else{
			var content="";
			if(data[i].content.length>24){//标题太长处理
				content=data[i].content.substring(0,24)+"...";
			}else{
				content=data[i].content;
			}
			msg="<a href=\"JavaScript:instantDetails("+data[i].imId+")\">"+i18n['title']+"："+title+"<br>"+i18n['common_content']+"："+content+"</a>";
		}
		$('#assginTipContent').html(msg);
	}
	openImManage=function(){
		basics.tab.tabUtils.addTab(i18n['label_message_manageTitle'],'../pages/common/tools/im/im.jsp');
	}
}