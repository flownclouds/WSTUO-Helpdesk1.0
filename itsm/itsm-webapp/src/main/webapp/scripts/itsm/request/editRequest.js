$package("itsm.request");
$import("common.jbpm.processCommon");
$import("common.config.dictionary.dataDictionaryUtil");
$import("common.config.category.eventCategoryTree");
$import("common.security.organizationTreeUtil");
$import("common.security.userUtil");
$import("common.tools.event.eventAttachment");
if(isCIHave){
	$import("itsm.cim.configureItemUtil");
}
$import("common.eav.attributes");
$import('itsm.itsop.selectCompany');
$import('common.config.attachment.chooseAttachment');
$import('itsm.cim.ciCategoryTree');
$import('common.knowledge.knowledgeTree');
$import('common.config.category.serviceCatalog');
$import('common.security.includes.includes');
$import('common.security.xssUtil');
/**  
 * @author QXY  
 * @constructor requestEdit
 * @description 编辑请求函数
 * @date 2010-11-17
 * @since version 1.0 
 */
itsm.request.editRequest = function(){
	this.rowHTML='<tr id="ref_ci_CIID">'+
	'<td align="center">CINO</td>'+
	'<td align="center"><input type="hidden" name="cinos" value="CIID" />'+
	'<a href=javascript:itsm.request.editRequest.lookConfigureItemInfo(CIID)>CINAME</a>'+
	'</td>'+
	'<td align="center">CATEGORYNAME</td>'+
	'<td align="center">CISTATUS</td>'+
	'<td align="center"><a onclick=itsm.cim.configureItemUtil.removeRow("#ref_ci_CIID")>DELETE</a></td>'+
	'</tr>';
	return {
		/**
		 * @description 获取配置项信息
		 * @param ciId 配置项Id
		 */
		lookConfigureItemInfo:function(ciId){
			basics.tab.tabUtils.reOpenTab("ci!findByciId.action?ciEditId="+ciId,i18n.ci_configureItemInfo);
		},
		/**
		 * 查询请求信息
		 */
		findRequestById:function(){
			var url = 'request!findRequestById.action?requestQueryDTO.eno='+eno;
			$.post(url, function(res){
				//基本信息
				$('#requestEdit_pid').val(res.pid);
				if(res.pid!=null){
					$('#traceRequestEditBtn').attr('style','margin-right:15px;');
				}
				$('#requestEdit_eno').val(res.eno);
				$('#requestEdit_etitle').val(res.etitle);
				
				initCkeditor('requestEdit_edesc','Simple',function(){
					var oEditor = CKEDITOR.instances.requestEdit_edesc;
					oEditor.setData(res.edesc);
				});
				
				//$('#requestEdit_edesc').val(res.edesc);
				if(res.statusNo!=null){
					$('#requestEdit_status').val(res.statusNo);
				}else{
					$('#requestEdit_status').val('');
				}
				if(res.ecategoryNo!=null)
					$('#requestEdit_ecategory').val(res.ecategoryNo);
				else
					$('#requestEdit_ecategory').val('');
				
				if(res.effectRangeNo!=null && res.effectRangeNo!==0)
					$('#requestEdit_effectRange').val(res.effectRangeNo);
				else
					$('#requestEdit_effectRange').val('');
				if(res.levelNo!=null && res.levelNo!==0 )
					$('#requestEdit_level').val(res.levelNo);
				else
					$('#requestEdit_level').val('');
				if(res.priorityNo!=null && res.priorityNo!==0)
					$('#requestEdit_priority').val(res.priorityNo);
				else
					$('#requestEdit_priority').val('');
				
				if(res.seriousnessNo!=null && res.seriousnessNo!==0)
					$('#requestEdit_seriousness').val(res.seriousnessNo);
				else
					$('#requestEdit_seriousness').val('');
				if(res.imodeNo!=null && res.imodeNo!==0)
					$('#requestEdit_imode').val(res.imodeNo);
				else
					$('#requestEdit_imode').val('');
	
				$('#requestEdit_address').val(res.address);
				//请求人
				$('#RequestEdit_UserId').val(res.createdByNo);
				$('#RequestEdit_UserName').val(res.createdByName);
				if(res.createdByPhone!=null)
					$('#RequestEdit_UserPhone').val(res.createdByPhone);
				else
					$('#RequestEdit_UserPhone').val('');
				//指派
				if(res.assigneeGroupName!=null)
					$('#requestEdit_assigneeGroupName').val(res.assigneeGroupName);
				else
					$('#requestEdit_assigneeGroupName').val('');
				if(res.assigneeGroupNo!=null)
					$('#requestEdit_assigneeGroupNo').val(res.assigneeGroupNo);
				else
					$('#requestEdit_assigneeGroupNo').val('');
					
				if(res.assigneeName!=null)
					$('#requestEdit_assigneeName').val(res.assigneeName);
				else
					$('#requestEdit_assigneeName').val('');
				if(res.assigneeNo!=null)
					$('#requestEdit_assigneeNo').val(res.assigneeNo);
				else
					$('#requestEdit_assigneeNo').val('');
				
				//关系配置项
				if(res.cigDTO!=null){
					
					var cigDTO=res.cigDTO;
					for(var i=0;i<cigDTO.length;i++){
						
						var status='';
						if(cigDTO[i].status!=null)
							status=cigDTO[i].status;
						var newRowHTML=rowHTML.replace(/CIID/g,cigDTO[i].ciId)
						.replace(/CINO/g,cigDTO[i].cino)
						.replace(/CIDETAIL_TITLE/g,i18n.ci_configureItemInfo)
						.replace(/CINAME/g,cigDTO[i].ciname)
						.replace(/CATEGORYNAME/g,cigDTO[i].categoryName)
						.replace(/CISTATUS/g,status)
						.replace(/DELETE/g,i18n.deletes); 	
						$(newRowHTML).appendTo('#edit_request_relatedCIShow table');
					}
				}
				//解决方案
				if(res.solutions!=null){
					$('#requestEditSolutions').val(res.solutions);
				}
				//影响明细
				$('#requestEditEffectRemark').val(res.effectRemark);
				$('#requestEdit_requestCode,#requestEdit_requestCode_show').val(res.requestCode);
				//请求分类
				if(res.requestServiceDirNo!=null)
					$('#edit_request_ref_requestServiceDirNo').val(res.requestServiceDirNo);
				if(res.requestServiceDirName!=null)
					$('#edit_request_ref_requestServiceDirName').val(res.requestServiceDirName);
				
				//服务目录
				if(res.requestCategoryNo!=null)
					$('#editRequestCategoryNo').val(res.requestCategoryNo);
				if(res.requestCategoryName!=null)
					$('#editRequestCategoryName').val(res.requestCategoryName);
				//所属性客户
				if(res.companyName!=null){
					$('#edit_request_companyName').val(res.companyName);
					$('#edit_request_companyNo').val(res.companyNo);
				}else{
					$('#edit_request_companyName').val("");
				}
				if(res.serviceDirectory.length > 0 && res.serviceDirectory != null){
					for ( var _int = 0; _int < res.serviceDirectory.length; _int++) {
						$("<tr id=edit_request_"+res.serviceDirectory[_int].eventId+"><td>"+res.serviceDirectory[_int].eventName+"</td><td>"+res.serviceDirectory[_int].scores+"</td><td><a onclick=common.knowledge.knowledgeTree.serviceRm("+res.serviceDirectory[_int].eventId+")>"+i18n.deletes+"</a><input type=hidden name=requestDTO.serviceDirIds value="+res.serviceDirectory[_int].eventId+"></td></tr>").appendTo('#edit_request_serviceDirectory_tbody');
						$('#editRequest_serviceDirIds').val(res.serviceDirectory[_int].eventId);
						$('#editRequest_serviceDirName').val(res.serviceDirectory[_int].eventName);
					}
					
				}
				common.eav.attributes.findAttributeByEno(eno,'itsm.request',res.categoryEavId,'request_edit_eavAttributet','requestDTO');
				itsm.app.autocomplete.autocomplete.bindAutoComplete('#RequestEdit_UserName','com.wstuo.common.security.entity.User','fullName','fullName','userId','Long','#RequestEdit_UserId',companyNoReqEdit,'true');
			});
			
		},
		/**
		 * @description 选择请求分类
		 * @param showAttrId 扩展属性Id
		 * @param dtoName 实体对象名
		 * */
		selectEditRequestCategory:function(showAttrId,dtoName){
			common.config.category.eventCategoryTree.showSelectTree('#request_category_select_window',
                                                                    '#request_category_select_tree',
                                                                    'Request',
                                                                    '#editRequestCategoryName',
                                                                    '#editRequestCategoryNo',
                                                                    '',
                                                                    '',
                                                                    showAttrId,
                                                                    dtoName);
			
		},
		/**
		 * @description 选择请求子分类
		 * @param showAttrId 扩展属性Id
		 * @param dtoName 实体对象名
		 * */
		selectEditRequestCategorySub:function(showAttrId,dtoName){
			var categoryNo= $('#editRequestCategoryNo').val();
			
			if(categoryNo==="")
				msgShow(i18n.request_categorysub_alert,'show');
			else{
			common.config.category.eventCategoryTree.showSelectTreeZi('#request_category_select_window',
                                                                      '#request_category_select_tree',
                                                                      'Request',
                                                                      '#editRequestCategoryNameSub',
                                                                      '#editRequestCategoryNoSub',
                                                                      '',
                                                                      '',
                                                                      showAttrId,dtoName,categoryNo);
			}
		},
		
		/**
		 * @description 保存请求修改
		 * */
		saveRequestEdit:function(){
			var oEditor = CKEDITOR.instances.requestEdit_edesc;	
			var edesc=trim(oEditor.getData());
			edesc = edesc.replace(/\s*<\/p>\s*|\s*<\/p>\s*/, '').replace(/\s*<p>\s*|\s*<\/p>\s*/, '').replace(/&nbsp;/gi, '');
			var title= $('#requestEdit_etitle').val();
			$('#requestEdit_etitle').val(trim(title));
			if($('#requestEditForm').form('validate')){
				if($('#request_edit_eavAttributet_form').form('validate')){
					var request_edesc=(edesc===''|| edesc=="null" || edesc=="NULL")?false:true;
					if(!request_edesc){
						msgAlert(i18n.titleAndContentCannotBeNull,'info');
					}else{
						$('#requestEdit_edesc').val(edesc);
						var rNo=$('#editRequestCategoryNoSub').val();
						if(rNo!=="" && rNo!==undefined){
							$('#editRequestCategoryNo').val(rNo);
						}
						
						var frm = $('#requestEdit_contentPanel form').serialize();
						var url = 'request!updateRequest.action';
						startProcess();
						$.post(url,frm, function(){
							endProcess();
							showRequestIndex();
							$('#requestGrid').trigger('reloadGrid');
							//关闭请求详情TAB
							basics.tab.tabUtils.closeTab(i18n.title_request_editRequest);
							msgShow(i18n.editSuccess,'show');
				
						});
					}	
				}else{
					msgAlert(i18n.eavAttributet_notNull,'info');
					$('#requestEditsTab').tabs('select', request_edit_eavAttributet);
				}
			}
		},
		/**
		 * @description 创建人信息
		 * */
		selectCreator_openWindow:function(){
			
			var _RequestEdit_UserId=$('#RequestEdit_UserId').val();
			common.security.userUtil.selectUser('#RequestEdit_UserName','#RequestEdit_UserId','','fullName',$('#edit_request_companyNo').val(),function(){
				/*if(_RequestEdit_UserId!=$('#RequestEdit_UserId').val()){//如果更改了请求人，则清空相关联的配置项需要重新选择
					$('#edit_request_ref_ciname,#edit_request_ref_ciid').val('');
				}*/
			});
		},
		/**
		 * @description 删除请求附件
		 * @param eno 编号eno
		 * @param aid 附件Id
		 */
		deleteRequestAttachement:function(eno,aid){
			
			msgConfirm(i18n.msg_msg,'<br/>'+i18n.msg_confirmDelete,function(){
				var _url = "request!deleteRequestAttachement.action";
				$.post(_url,'eno='+eno+'&aid='+aid,function(){
					$('#show_edit_request_attachment #att_'+aid).remove();
					msgShow(i18n.deleteSuccess,'show');
				});
			});
		},
		/**
		 * 初始化
		 * @private
		 */
		init: function(){
			itsm.cim.includes.includes.loadSelectCIIncludesFile();//加载选择配置项
			common.security.includes.includes.loadSelectUserIncludesFile();//加载用户选择
			$("#requestEdit_loading").hide();
			$("#requestEdit_contentPanel").show();
			//绑定退回请求列表页面按钮
			$('#edit_request_backList').click(function(){
				showLeftMenu('../pages/itsm/request/leftMenu.jsp','leftMenu');		
				basics.tab.tabUtils.refreshTab(i18n.title_request_requestGrid,'../pages/itsm/request/requestMain.jsp');
			});
			//lazyInitEditor('#requestEdit_edesc','streamline');
			
			//页面点击
			 $('#requestEditsTab').tabs({
	                onSelect:function(title){
					  	if(title=title_edit_request_attr){
					  		$('#editRequestQueId').empty();
						}
			  		}
	         });
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('effectRange','#requestEdit_effectRange');
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('seriousness','#requestEdit_seriousness');
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('imode','#requestEdit_imode');
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('level','#requestEdit_level');
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('priority','#requestEdit_priority');
			//setTimeout(itsm.request.editRequest.findRequestById,0);
			//流程跟踪
			$('#traceRequestEditBtn').click(function(){common.jbpm.processCommon.showFlowChart( $('#requestEdit_pid').val(),i18n.request);});
			$('#search_Edit_UserName').click(itsm.request.editRequest.selectCreator_openWindow);
			setTimeout(function(){
				//getUploader('上传文件文本ID','上传后返回的信息字符串','显示上传成功的附件','');
				getUploader('#edit_request_file','#edit_request_attachmentStr','#show_edit_request_attachment_success','editRequestQueId',function(){
				    common.tools.event.eventAttachment.saveEventAttachment($('#attachmentCreator').val(),'show_edit_request_attachment',eno,'itsm.request','edit_request_attachmentStr',true);
				});
				//initFileUpload("_RequestEdit",eno,"itsm.request",userName,"show_edit_request_attachment","edit_request_attachmentStr");
			},0);
			common.tools.event.eventAttachment.showEventAttachment('show_edit_request_attachment',eno,'itsm.request',true);
			//设置请求下拉树的宽度
			setTimeout(function(){
				$('#edit_request_select_category_panel').css('width',$('#editRequestCategoryName').css('width'));
			},0);

			$('#edit_request_ref_ci').click(function(){//选择配置项
				itsm.cim.configureItemUtil.selectCIS('#edit_request_ref_ci','#requestEditCIId');
			});
			//服务目录
			$('#edit_request_service_edit').click(function(){
				common.config.category.serviceCatalog.selectServiceDir('#edit_request_serviceDirectory_tbody');
			});
			//绑定
			$('#edit_request_ref_ciname').click(function(){
				itsm.cim.configureItemUtil.findConfigureItemByPower('#edit_request_ref_ciname','#edit_request_ref_ciid',userName);
			});
			$('#Request_edit_ref_ci_btn').click(function(){
				if(userRoleCode =="ROLE_ENDUSER,"){//如果是终端用户，根据请求人去搜索，不需要分类权限控制； 
					itsm.cim.configureItemUtil.enduserSelectCISM('#edit_request_relatedCIShow',$('#edit_request_companyNo').val());
				}else{
					itsm.cim.configureItemUtil.requestSelectCI('#edit_request_relatedCIShow',$('#edit_request_companyNo').val(),'');
				}
			});
			
			$("#edit_request_ref_requestServiceDirName").click(function(){
				common.knowledge.knowledgeTree.selectKnowledgeServiceFilter('#knowledge_services_select_window','#knowledge_services_select_tree','#edit_request_ref_requestServiceDirName','#edit_request_ref_requestServiceDirNo');			
			});
			
			$('#editRequest_serviceDirName').click(function(){
				common.config.category.serviceCatalog.selectSingleServiceDir('#editRequest_serviceDirName','#editRequest_serviceDirIds');
			});
		}
	};
}();
//载入
$(document).ready(itsm.request.editRequest.init);