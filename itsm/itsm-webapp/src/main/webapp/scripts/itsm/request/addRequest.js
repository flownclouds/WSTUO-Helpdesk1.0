$package("itsm.request");
$import("common.config.dictionary.dataDictionaryUtil");
$import("common.config.category.eventCategoryTree");
$import("common.security.userUtil");
if(isCIHave){
	$import("itsm.cim.configureItemUtil");
	$import('itsm.cim.ciCategoryTree');
}
$import("common.eav.attributes");
$import('itsm.itsop.selectCompany');
$import('itsm.request.relatedRequestAndKnowledge');
$import('common.config.attachment.chooseAttachment');
$import('common.knowledge.knowledgeTree');
$import('common.config.category.serviceCatalog');
$import('common.config.includes.includes');
$import('common.security.includes.includes');
$import('common.security.xssUtil');
/**  
 * @author QXY  
 * @constructor WSTO
 * @description 新增请求主函数.
 * @date 2010-11-17
 * @since version 1.0 
 * @returns
 * @param select
 */
itsm.request.addRequest = function(){
	var templateId='';
	this.addRequestGrids=[];
	var temp_companyName = '';
	return {
		/**
		 * @description 选择请求者.
		 */
		selectRequestCreator:function(){
			var _addRequestUserId=$('#addRequestUserId').val();
			common.security.userUtil.selectUser('#selectCreator','#addRequestUserId','','fullName',$('#add_request_companyNo').val(),function(){
				/*if(_addRequestUserId!=$('#addRequestUserId').val()){//如果更改了请求人，则清空相关联的配置项需要重新选择
					$('#add_request_ref_ciname,#add_request_ref_ciid').val('');
				}*/
			});
		},
		/**
		 * 根据语音卡用户Id赋值选择请求人
		 */
		selectRequestvoiceCard:function(){
			$('#addRequestUserId').val(voiceCarduserId);
			$.post('user!findUserDetail.action','userDto.userId='+voiceCarduserId,function(data){
				$('#selectCreator').val(data.fullName);
			});
		},
		/**
		 * @description 选择请求分类.
		 * @param showAttrId 扩展属性Id
		 * @param dtoName 实体对象名
		 */
		selectRequestCategory:function(showAttrId,dtoName){
			common.config.category.eventCategoryTree.showSelectTree('#request_category_select_window',
                                                                    '#request_category_select_tree',
                                                                    'Request',
                                                                    '#addRequestCategoryName',
                                                                    '#addRequestCategoryNo',
                                                                    '#request_etitle',
                                                                    '#request_edesc',
                                                                    showAttrId,
                                                                    dtoName
                                                                   );
		},
		
		/**
		 * @description 选择请求子分类.
		 * @param showAttrId 扩展属性Id
		 * @param dtoName 实体对象名
		 */
		selectRequestCategoryZi:function(showAttrId,dtoName){
			var categoryNo= $('#addRequestCategoryNo').val();
			if(categoryNo==="")
				msgShow(i18n.request_categorysub_alert,'show');
			else{
			common.config.category.eventCategoryTree.showSelectTreeZi('#request_category_select_window',
                                                                      '#request_category_select_tree',
                                                                      'Request',
                                                                      '#addRequestCategoryName_zi',
                                                                      '#addRequestCategoryNo_zi',
                                                                      '#request_etitle',
                                                                      '#request_edesc',
                                                                      showAttrId,
                                                                      dtoName,
                                                                      categoryNo
                                                                     );
			}
		},
		/**
		 * @description 提交保存请求.
		 */
		saveRequest:function(){
			var oEditor = CKEDITOR.instances.request_edesc;
			var edesc=trim(oEditor.getData());
			var uid=$('#addRequestUserId').val();
			var username=$('#selectCreator').val();
			var title= $('#request_etitle').val();
			edesc = formatDescContent( edesc );
			$('#request_etitle').val(trim(title));
			if($('#request_add_eavAttributet_form').form('validate')){
				if($('#addRequestForm').form('validate')){
					//var serviceDirectory = $('#add_request_serviceDirectory_tbody').html().replace(/\s+/g, "");
					var request_edesc=(edesc===''|| edesc=="null" || edesc=="NULL")?false:true;
					if(!request_edesc){
						msgAlert(i18n.titleAndContentCannotBeNull,'info');
					}else if(username===''){
						msgAlert(i18n.ERROR_CREATE_BY_NULL,'info');
					}else{
						$('#request_edesc').val(edesc);
						var rNo=$('#addRequestCategoryNo_zi').val();
						var categoryNo=$('#addRequestCategoryNo').val();
						if(rNo!=="" && rNo!==undefined){
							$('#addRequestCategoryNo').val(rNo);
						}
						var frm = $('#addRequestDiv form').serialize();
						var url = 'request!saveRequest.action';
						//调用
						startProcess();
						$.post(url,frm, function(eno){
								basics.tab.tabUtils.closeTab(i18n.title_request_addRequest);
								showRequestIndex();
								basics.tab.tabUtils.reOpenTab("request!requestDetails.action?eno="+eno,i18n.request_detail);
								
								if($('#itsmMainTab').tabs('exists',i18n.title_request_requestGrid)){
									$('#requestGrid').trigger('reloadGrid');
									itsm.request.requestStats.countAllRquest();
								}
								msgShow(i18n.msg_request_addSuccessful,'show');
								endProcess();
						});
					}
				}
			}else{
				msgAlert(i18n.eavAttributet_notNull,'info');
				$('#addRequest_tab').tabs('select', i18n.config_extendedInfo);
			}
		},
		/**
		 * 查询相类似的请求和知识
		 * @param id 标识
		 */
		findLikeData:function(id){
			var _keyword=$(id).val();
			//console.log(_keyword);
			if(_keyword!=null && _keyword!=='' && _keyword!=' '){
				if(id=='#request_etitle'){
					itsm.request.relatedRequestAndKnowledge.findKnowledge('#findLikeKnowledgeGrid','#findLikeKnowledgePager',_keyword);
				}
				itsm.request.relatedRequestAndKnowledge.findLikeRequest('#findLikeRequestGrid','#findLikeRequestPager',_keyword);
				$('#addRequest_keyword').val(_keyword);
			}else{
				$("#findLikeKnowledgeGrid").jqGrid("clearGridData");
				$("#findLikeRequestGrid").jqGrid("clearGridData");
			}
		},
		/**
		 * 数据列表伸展
		 */
		addRequestPanelCollapseAndExpand:function(){
			 $(addRequestGrids).each(function(i, g) {
			      setGridWidth(g,'addRequest_tab', 10);
			 });
		},
		/**
		 * 数据列表自动伸展
		 */
		fitRequestAddGrids:function(){
			$('#addRequest_west,#addRequest_center').panel({
				onCollapse:itsm.request.addRequest.addRequestPanelCollapseAndExpand,
				onExpand:itsm.request.addRequest.addRequestPanelCollapseAndExpand,
				onResize:function(width, height){
					setTimeout(function(){
						itsm.request.addRequest.addRequestPanelCollapseAndExpand();
					},0);
				}
			});
		},
		/**
		 * 选择请求模板
		 */
		selectRequestTemplate:function(){
			$('<option value="">-- '+i18n.common_pleaseChoose+' --</option>').appendTo("#requestTemplate");
			$.post("template!findByTemplateType.action",{"templateDTO.templateType":"request"},function(data){
				if(data!=null && data.length>0){
					for(var i=0;i<data.length;i++){
						$('<option value="'+data[i].templateId+'">'+data[i].templateName+'</option>').appendTo("#requestTemplate");
					}
				}
			});
		},
		/**
		 * 获取模板值
		 * @param templateId 模板Id
		 */
		getTemplateValue:function(templateId){
		    
		    $('#add_request_success_attachment').html('');
		    $('#add_request_attachmentStr').val('');
		    $('#requestRelatedCIShow tbody').html('');
		    
			if(templateId!=null && templateId!==""){
				$('#saveRequestTemplateBtn').hide();
				$('#editRequestTemplateBtn').show();
				$('#add_request_serviceDirectory_tbody').html("");
				$.post("template!findByTemplateId.action",{"templateDTO.templateId":templateId},function(data){
					if(data!=null){
						$('#requestTemplate').attr('value',data.templateId);
						$('#requestTemplateNameInput').val(data.templateName);
						$('#requestTemplateId').val(data.templateId);
						$('#addRequestCategoryNo').val(data.dto.requestCategoryNo);
						$('#addRequestCategoryName').val(data.dto.requestCategoryName);
						$.post("event!findByIdCategorys.action",{"categoryId":data.dto.requestCategoryNo},function(res){
							itsm.cim.ciCategoryTree.showAttributet(res.eavId,"request_add_eavAttributet","requestDTO");
							setTimeout(function(){
								if(data.dto.attrVals!=null){
									for(var key in data.dto.attrVals){
										$("#"+key+"request_add_eavAttributet").val(data.dto.attrVals[key]);
									}
								}else{
									$('#request_add_eavAttributet').html("");
								}
							},500);
						});
						$('#request_etitle').val(data.dto.etitle);
						if(CKEDITOR.instances.request_edesc){
							var oEditor = CKEDITOR.instances.request_edesc;
							oEditor.setData(data.dto.edesc);
						}
						var serviceNos = data.dto.serviceNos;
						var serviceScores = data.dto.scores;
						if(serviceNos!=null){
							var html="";
							for(var key in serviceNos){
								html+="<tr id=add_request_"+key+"><td>"+serviceNos[key]+"</td><td>"+serviceScores[key]+"</td><td><a onclick=common.knowledge.knowledgeTree.serviceRm("+key+")>"+i18n.deletes+"</a><input type=hidden name=requestDTO.serviceDirIds value="+key+"></td></tr>";
						    }
							$("#add_request_serviceDirectory_tbody").html(html);
						}
						
						$('#request_imode').val($vl0(data.dto.imodeNo));
						$('#request_priority').val($vl0(data.dto.priorityNo));
						$('#request_level').val($vl0(data.dto.levelNo));
						$('#request_effectRange').val($vl0(data.dto.effectRangeNo));
						$('#request_seriousness').val($vl0(data.dto.seriousnessNo));
						var ciNos = data.dto.relatedCiNos;
						if(ciNos!=null && ciNos.length>0){
							var param = $.param({"ids":ciNos},true);
							$.post("ci!findByIds.action",param,function(res){
								for(var i=0;i<res.length;i++){
									var ci=res[i];
									$('<tr id="ref_ci_'+ci.ciId+'">'+
											'<td align="center" id="problemShowCIno">'+ci.cino+'</td>'+
											'<td align="center"><input id="probleShowCIId" type="hidden" name="cinos" value="'+ci.ciId+'" />'+
											'<a href=javascript:itsm.cim.configureItemUtil.lookConfigureItemInfo('+ci.ciId+') id="probleShowCIName">'+ci.ciname+'</a>'+
											'</td>'+
											'<td align="center" id="probleShowCIcategoryName">'+ci.categoryName+'</td>'+
											'<td align="center" id="probleShowCIStatus">'+ci.status+'</td>'+
											'<td align="center"><a onclick=itsm.cim.configureItemUtil.removeRow("#ref_ci_'+ci.ciId+'")>'+i18n.deletes+'</a></td>'+
											'</tr>').appendTo('#requestRelatedCIShow tbody');
								}
								
							});
						}
						$('#add_request_attachmentStr').val(data.dto.attachmentStr);
						var attrArr=data.dto.attachmentStr.split("-s-");
			    		for(var i=0;i<attrArr.length;i++){
			    			var url=attrArr[i].replace("\\", "/");
							if(url!=""){
			        			var attrArrs=url.split("==");
			        			var name=attrArrs[0];
			        			uploadSuccess(name.substr(name.lastIndexOf(".")),attrArrs[1],name,'#add_request_attachmentStr','#add_request_success_attachment',"");
							}
			    		}
					}
				});
			}else{
				$('#saveRequestTemplateBtn').show();
				$('#addRequestCategoryNo').val("");
				$('#addRequestCategoryName').val("");
				$('#request_etitle').val("");
				if(CKEDITOR.instances.request_edesc){
					var oEditor = CKEDITOR.instances.request_edesc;
					oEditor.setData("");
				}
				$('#request_imode').val("");
				$('#request_priority').val("");
				$('#request_level').val("");
				$('#request_effectRange').val("");
				$('#request_seriousness').val("");
				$('#requestRelatedCIShow tbody').html("");
				$('#add_request_serviceDirectory_tbody').html("");
				$('#request_add_eavAttributet table tbody').html("<tr><td colspan=\"2\" style=\"color: red\"><b>"+i18n.label_notExtendedInfo_Request+"</b></td></tr>");
				$('#editRequestTemplateBtn').hide();
			
			}
		},

		/**
		 * 保存请求内容模板
		 * @param type 模板类型，add为新增
		 */
		saveRequestTemplate:function(type){
			if(type=="add"){
				$('#requestTemplateId').val("");
			}
			var oEditor = CKEDITOR.instances.request_edesc;
			var edesc=trim(oEditor.getData());
			edesc = edesc.replace(/\s*<\/p>\s*|\s*<\/p>\s*/, '').replace(/\s*<p>\s*|\s*<\/p>\s*/, '').replace(/&nbsp;/gi, '');
			if($('#requestTemplateForm').form('validate') && $('#addRequestForm').form('validate')){
				
				var request_edesc=(edesc===''|| edesc=="null" || edesc=="NULL")?false:true;
				if(!request_edesc){
					msgAlert(i18n.titleAndContentCannotBeNull,'info');
					return false;
				}
				
				var _edesc=""; 
				if(CKEDITOR.instances.request_edesc){
					_edesc = oEditor.getData();
				}
				$('#request_edesc').val(_edesc);
				var frm = $('#addRequestForm,#requestTemplateNameAndType,#addRequest_center form,#requestTemplateName form').serialize();
				var url = 'request!saveRequestTemplate.action';
				//调用
				startProcess();
				$.post(url,frm, function(res){
						endProcess();
						$('#requestTemplateName').dialog('close');
						if(type=="add"){
							$('#requestTemplate').html("");
							itsm.request.addRequest.selectRequestTemplate();
							$('#templateGrid').trigger('reloadGrid');
							msgShow(i18n.saveSuccess,'show');
						}else{
							$('#templateGrid').trigger('reloadGrid');
							msgShow(i18n.editSuccess,'show');
						}
					
				});
			}
			
		},
		/**
		 * 新增请求模板框
		 */
		requestTemplateNameWin:function(){
			$('#requestTemplateNameInput').val("");
			windows('requestTemplateName');
		},
		/**
		 * 请求新增上传
		 * @param locimage 文件名对象
		 */
		addrequestupload:function(locimage){
			var FileType = "jpg,png,gif"; 
	        var FileName = locimage.value;
		    FileName = FileName.substring(FileName.lastIndexOf('.')+1, FileName.length).toLowerCase(); 
		    if (FileType.indexOf(FileName) == -1){
		    	$('#request_upload_file').val("");
		    	msgAlert(i18n.lable_addRequest_upload_file,'info');
		     }else{
		    	 itsm.request.addRequest.uploadrequestimage(FileName);
		     }
		},
		/**
		 * 上传请求图片
		 * @param FileName 文件名
		 */
		uploadrequestimage:function(FileName){
			var imageFileNametoreuqest=new Date().getTime()+"."+FileName;
			$.ajaxFileUpload({
				url:'fileUpload!fileUpload.action?imageFileNametoreuqest='+imageFileNametoreuqest,
				secureuri:false,
	            fileElementId:'request_upload_file', 
	            dataType:'String',
				success: function (data, status){
					$('#request_upload_file').val("");
					$('#request_upload_opt_info').text("../upload/request/"+imageFileNametoreuqest);
					msgShow(i18n.lable_addRequest_upload_success,'show');
				},
				error: function (data, status){
					msgShow(i18n.lable_addRequest_upload_errors,'show');
	            }
			});
		},
		/**
		 * 呼叫中心集成打开的新增请求页面处理函数(韵达语音系统集成)
		 */
		callCenterAddRequest:function(){
			$("#phones").val(callNumber);
			var partten = /^0(([1,2]\d)|([3-9]\d{2}))\d{7,8}$/;
			var url="user!updateUserByPhone.action?userDto.moblie="+callNumber;
			if(partten.test(callNumber)){
				url="user!updateUserByPhone.action?userDto.phone="+callNumber;
		     }
			//在这里要判断一下电话是手机还是座机
			$.post(url,function(data){
				if(data != null){
					$("#selectCreator").attr("value",data.fullName);
				}
			});
			//输入用户修改电话号码
			$("#edit_User_By_LoginName").bind("click",function(){
				windows('updateUserWindow');
			});
			//新增一个用户
			$("#add_User_By_Phone").bind("click",function(){
				windows('addUserByPhone');
			});
			//输入用户名修改用户手机号码
			$("#update_user_by_loginName").bind("click",function(){
				if(callNumber == null || callNumber === ""){
					msgAlert(i18n.No_incoming_number,'info');
				}else{
					var loginNames=$("#bindByLoginName").val();
					if(loginNames != null || loginNames !== ""){
						var url="user!findUserUpdatePhone.action?userDto.loginName="+loginNames+"&userDto.moblie="+callNumber;
						var partten = /^0(([1,2]\d)|([3-9]\d{2}))\d{7,8}$/;
						if(partten.test(callNumber)){
							url="user!findUserUpdatePhone.action?userDto.loginName="+loginNames+"&userDto.phone="+callNumber;
					     }
						$.post(url,function(data){
							if(data){
								var urls="user!getUserDetailByLoginName.action?userName="+loginNames;
								$.post(urls,function(datas){
									$("#selectCreator").val(datas.fullName);
									$("#addRequestUserId").val(datas.userId);
									msgShow(i18n.editSuccess,'show');
									$('#updateUserWindow').dialog('close');
								});
							}else{
								msgAlert(i18n.user_phone_is_not_find,'info');
							}
						});
					}
				}
			});
			//添加操作
			$("#add_User_Phone").bind("click",function(){
				if(callNumber == null || callNumber === ""){
					msgAlert(i18n.No_incoming_number,'info');
				}else{
					var fullNames= $("#add_loginName").val();
					var orgNo=$("#addUserOroNo").val();
					var partten = /^0(([1,2]\d)|([3-9]\d{2}))\d{7,8}$/;
					var url="user!saveuserByPhone.action";
					var frm="userDto.fullName="+fullNames+"&userDto.orgNo="+orgNo+"&userDto.moblie="+callNumber;
					if(partten.test(callNumber)){
						frm="userDto.fullName="+fullNames+"&userDto.orgNo="+orgNo+"&userDto.phone="+callNumber;
				     }
					if(orgNo != null && orgNo !== ""){
						$.post(url,frm,function(data){
							if(data != null){
								msgShow(i18n.addSuccess,'show');
								$("#selectCreator").val($("#add_loginName").val());
								$("#add_loginName").val("");
								$("#addUserOroNo").val("");
								$("#addRequestUserId").val(data.userId);
							}
						});
					}else{
						msgAlert(i18n.title_user_org+i18n.err_nameNotNull,'info');
					}
				}
			});
		},
		/**
		 * 初始化
		 * @private
		 */
		init:function() {
			$("#addRequest_loading").hide();
			$("#addRequestDiv").show();
			//判断是否有传来电号码(韵达语音系统集成)
			if(callNumber!=="" && callNumber != null || callNumber.length !== 0){
				itsm.request.addRequest.callCenterAddRequest();
			}
			//如果是内容模板，则隐藏请求保存按钮
			if(pageType=="template"){
				$('#saveRequestBtn').hide();
			}
			//加载请求模板includes文件
			itsm.request.includes.includes.loadRequestActionIncludesFile();
			common.security.includes.includes.loadSelectCustomerIncludesFile();
			common.config.includes.includes.loadCategoryIncludesFile();
			common.config.includes.includes.loadCustomFilterIncludesFile();
			//初始化请求内容描述富文本
			setTimeout(function(){
				initCkeditor('request_edesc','Simple',function(){});
			},0);
			
			//点击附件Tab执行清空
			$('#addRequest_tab').tabs({
				onSelect:function(title){
					if(title==title_add_request_attr){
						$('#addRequestQueId').empty();
					}
				}
	        });
			//加载基础数据(紧急度、影响范围、来源、等级、优先级)
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('effectRange','#request_effectRange');
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('seriousness','#request_seriousness');
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('imode','#request_imode');
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('level','#request_level');
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('priority','#request_priority');
			//绑定请求保存按钮
			$('#saveRequestBtn').click(itsm.request.addRequest.saveRequest);
			//绑定选择请求人按钮
			$('#searchCreator').click(itsm.request.addRequest.selectRequestCreator);
			
			//绑定选择指派信息按钮
			$('#selectAssigneeInfo').click(itsm.request.addRequest.selectRequestAssignee);
			if(voiceCarduserId!==""){
				if(callNumber== null || callNumber ===""){
					itsm.request.addRequest.selectRequestvoiceCard();
				}
			}
			
			//初始化附件上传控件
			setTimeout(function(){
				getUploader('#add_request_file','#add_request_attachmentStr','#add_request_success_attachment','addRequestQueId');
			},0);
			
			//绑定退回请求列表页面按钮
			$('#add_request_backList').click(function(){
				if(pageType=='template'){
					basics.tab.tabUtils.refreshTab(i18n.title_dashboard_template,'../pages/common/config/template/templateMain.jsp');
				}else{
					showLeftMenu('../pages/itsm/request/leftMenu.jsp','leftMenu');		
					basics.tab.tabUtils.refreshTab(i18n.title_request_requestGrid,'../pages/itsm/request/requestMain.jsp');
				}
			});
			
			setTimeout(function(){//设置请求下拉树的宽度
				$('#add_request_select_category_panel').css('width',$('#addRequestCategoryName').css('width'));
			},0);
			
			$('#add_search_request_companyName').click(function(){//选择公司
				itsm.itsop.selectCompany.openSelectCompanyWin('#add_request_companyNo','#add_request_companyName','#selectCreator,#addRequestUserId,#add_request_ref_ciname,#add_request_ref_ciid');
			});
			
			//加载默认公司
			common.security.defaultCompany.loadDefaultCompany('#add_request_companyNo','#add_request_companyName');
			if(fixRequestAndKnowledge=="1"){
				$('#request_etitle').keyup(function(){
					itsm.request.addRequest.findLikeData('#request_etitle');});
				$('#addRequest_keyword_search_ok').click(function(){itsm.request.addRequest.findLikeData('#addRequest_keyword');});
			}
			
			//服务目录
			$('#add_request_service_add').click(function(){
				common.config.category.serviceCatalog.selectServiceDir('#add_request_serviceDirectory_tbody');
			});
			
			$('#addRequest_serviceDirName').click(function(){
				common.config.category.serviceCatalog.selectSingleServiceDir('#addRequest_serviceDirName','#addRequest_serviceDirIds');
			});
			
			if(fixRequestAndKnowledge=="1"){
				addRequestGrids.push('#findLikeKnowledgeGrid');
				addRequestGrids.push('#findLikeRequestGrid');
				setTimeout(function(){
					$("#phoneNums").val(phoneNum);
					itsm.request.relatedRequestAndKnowledge.findKnowledge('#findLikeKnowledgeGrid','#findLikeKnowledgePager','');
					itsm.request.relatedRequestAndKnowledge.findLikeRequest('#findLikeRequestGrid','#findLikeRequestPager','');
				},500);
			}
			
			setTimeout(itsm.request.addRequest.fitRequestAddGrids,100);
			//加载请求内容模板
			itsm.request.addRequest.selectRequestTemplate();
			
			$('#saveRequestTemplateBtn').click(function(){
				itsm.request.addRequest.requestTemplateNameWin();
			});
			$('#addRequestTemplateOk').click(function(){
				itsm.request.addRequest.saveRequestTemplate("add");
			});
			$('#editRequestTemplateBtn').click(function(){
				itsm.request.addRequest.saveRequestTemplate("edit");
			});
			if(editTemplateId!==""){
				$('#requestTemplate').val(editTemplateId);
				setTimeout(function(){
					itsm.request.addRequest.getTemplateValue(editTemplateId);
				},500);
			}
			setTimeout(function(){
				if(versionType==="ITSOP")
					itsm.app.autocomplete.autocomplete.bindAutoComplete('#add_request_companyName','com.wstuo.common.security.entity.Organization','orgName','orgName','orgNo','Long','#add_request_companyNo',userName,'true');//所属客户
					itsm.app.autocomplete.autocomplete.bindAutoComplete('#selectCreator','com.wstuo.common.security.entity.User','fullName','fullName','userId','Long','#addRequestUserId','#add_request_companyNo','true');
				if(callNumber != null || callNumber !== ""){
					itsm.app.autocomplete.autocomplete.bindAutoComplete('#selectOrgName','com.wstuo.common.security.entity.Organization','orgName','orgName','orgNo','Long','#addUserOroNo','','false');//标题
					itsm.app.autocomplete.autocomplete.bindAutoComplete('#bindByLoginName','com.wstuo.common.security.entity.User','loginName','loginName','userId','Long','#addRequestUserId','#add_request_companyNo','false');
				}
			},500);
			$("#add_request_ref_requestServiceDirName").click(function(){
				common.knowledge.knowledgeTree.selectKnowledgeServiceFilter('#knowledge_services_select_window','#knowledge_services_select_tree','#add_request_ref_requestServiceDirName','#add_request_ref_requestServiceDirNo');			
			});
			
			$('#addrequestRelatedCIBtn').click(function(){
				if(userRoleCode =="ROLE_ENDUSER,"){//如果是终端用户，根据请求人去搜索，不需要分类权限控制； 
					itsm.cim.configureItemUtil.enduserSelectCISM('#requestRelatedCIShow',$('#add_request_companyNo').val());
				}else{
					itsm.cim.configureItemUtil.requestSelectCI('#requestRelatedCIShow',$('#add_request_companyNo').val(),'');
				}
			});
			temp_companyName = $('#add_request_companyName').val();
			$('#add_request_companyName').focus(function(){
				temp_companyName = $('#add_request_companyName').val();
			});
			$('#add_request_companyName').blur(function(){
				if(temp_companyName!==$('#add_request_companyName').val())
					$('#selectCreator').val('');
			});
		}
	};
}();
//载入
$(document).ready(itsm.request.addRequest.init);