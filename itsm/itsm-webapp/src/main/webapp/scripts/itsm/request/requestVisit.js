
/**  
 * @fileOverview 请求回访主函数
 * @author QXY
 * @version 1.0  
 */  
 /**  
 * @author QXY  
 * @constructor 请求回访主函数
 * @description 请求回访主函数
 * @date 2011-05-09
 * @since version 1.0 
 */
var requestVisit=function(){
	/**
	 * @description 请求回访窗口
	 */
	this.requestVisit_win=function(){
		$("#requestVisit_win table").html("");
		$.post('visit!findVisit.action?rows=10000&useStatus=true',function(data){
			if(data==null || data.data==null) return;
			
			$("#requestVisit_win table").append("<tr><td>"+i18n.label_returnVisit_satisfaction+"</td><td>"+
					"<input type='radio' value='5' name='userReturnVisitDTO.satisfaction' />"+i18n.label_returnVisit_verysatisfied+
					"<input type='radio' value='4' name='userReturnVisitDTO.satisfaction' />"+i18n.label_returnVisit_satisfied+
					"<input type='radio' value='3' name='userReturnVisitDTO.satisfaction' checked='checked' />"+i18n.label_returnVisit_general+
					"<input type='radio' value='2' name='userReturnVisitDTO.satisfaction' />"+i18n.label_returnVisit_dissatisfied+
					"<input type='radio' value='1' name='userReturnVisitDTO.satisfaction' />"+i18n.label_returnVisit_verydissatisfied+
					"</td></tr>");
			
			
			var visitItem=data.data;
			
			for(var i=0;i<visitItem.length;i++){
				if(visitItem[i].useStatus){
					var html="";
				    if(visitItem[i].visitItemType=='radio'&& visitItem[i].visitItems!=null){
				    	
						for(var j=0;j<visitItem[i].visitItems.length;j++){
						    html+= "<input type='radio' value='"+visitItem[i].visitItems[j].visitItemName+"' name='visitItemValue"+i+"' ";
						    if (j===0) {html+= "checked='checked' ";}
						    html+= "/>"+visitItem[i].visitItems[j].visitItemName;
						}
				    } else if(visitItem[i].visitItemType=='text'&& visitItem[i].visitItems!=null){
				        html="<input  id='visitItemValue"+i+"' />";
				    }else if(visitItem[i].visitItemType=='Lob'&& visitItem[i].visitItems!=null){
				    	html="<textarea type='' id='visitItemValue"+i+"' /></textarea>";
				    }else{
				    	html="<input onkeyup='chkPrice(this)' onblur='chkLast(this)' onpaste='javascript: return false;' id='visitItemValue"+i+"' />";
				    }
				    $("#requestVisit_win table").append("<tr><td>"+visitItem[i].visitName+"<input type='hidden' value='"+visitItem [i].visitName+"' id='visitItemName"+i+"' /></td><td>"+html+"</td></tr>");
				}
			}
			
		});
	},
	/**
	 * @description 回访完成关闭页面
	 */
	this.closeme=function(){
		var browserName=navigator.appName; 
		if (browserName=="Netscape"){
			window.open('','_parent','');
			window.close();
		}else if(browserName=="Microsoft Internet Explorer"){ 
			window.opener = "whocares";
			window.close();
		}
	};
	/**
	 * @description 提交回访结果
	 */
	this.requestVisit_opt=function(){
		var tab = document.getElementById("requestVisit_table") ;
	    //表格行数
	    var rows = tab.rows.length;
	    var visitRecord='';
	    for(var i=0;i<rows-1;i++){
	    	var _value = $("input[@type=radio][name=visitItemValue"+i+"][checked]").val();
	    	if(_value===undefined){
	    		_value=$('#visitItemValue'+i).val();
	    	}
	    	visitRecord=visitRecord+$('#visitItemName'+i).val()+":"+_value+"<br><hr>";
	    }
	    
		setTimeout(function(){
			$('#request_visitRecord').val(visitRecord);
			var frm = $('#requestVisit_win form').serialize();
			frm=frm.replace('undefined','').replace('undefined','').replace('undefined','').replace('undefined','').replace('undefined','');
			$.post('userReturnVisit!replyUserReturnVisit.action',frm,function(result){
				if(result){
					msgConfirm(i18n.tip,'<br/>'+i18n.msg_visitSuccess,function(){
						closeme();
					});
				}else{
					msgConfirm(i18n.tip,'<br/>'+i18n.msg_alreadyVisit,function(){
						closeme();
					});
				}
			});
		},0);
	};
	return {
		/**
		 * @description 初始化
		 */
		init:function(){
			requestVisit_win();
			$('#visit_button').click(function(){
				requestVisit_opt();
			});
		}
	};

}();
$(document).ready(requestVisit.init);