$package("itsm.request");
$import("basics.tab.tabUtils");
$import("common.jbpm.processCommon");
$import("common.security.userUtil");
$import("common.security.organizationTreeUtil");
$import("common.eav.attributes");
$import("common.knowledge.knowledgeTree");
$import('common.config.includes.includes');
$import('common.config.attachment.chooseAttachment');
$import("common.tools.event.eventAttachment");
$import("common.tools.event.eventTask");//事件任务
$import("common.tools.event.eventCost");//进展及成本
$import("common.tools.history.historyRecord");//历史记录
$import('common.tools.includes.includes');
$import("common.tools.visit.returnVisitManage");
$import('common.security.includes.includes');
$import('itsm.request.requestEmailHistory');
$import("itsm.request.requestAction");
$import("itsm.request.requestTask");
$import("itsm.request.requestEventCost");
if(isProblemHave){
	$import("itsm.problem.relatedProblem");	
}
$import('common.security.base64Util');
$import('common.config.formCustom.formControl');
$import('common.config.formCustom.formControlImpl');
$import('itsm.request.requestCommon');
$import('common.security.xssUtil');
/**  
 * @author QXY  
 * @constructor 请求详细信息主函数
 * @description 请求详细信息主函数
 * @date 2010-11-17
 * @since version 1.0 
 */
itsm.request.requestDetailsFormCustom = function() {
	//请求详细页面数据列表集合
	this.requestDetailsGrids=[];
	//加载标识
	this._requestDetail_loadTaskFlag="0";
	this._requestDetail_loadTimeDetailFlag="0";
	this._requestDetail_loadRequestHistoryOptLogFlag="0";
	this._requestDetail_loadAttachementTag="0";
	this._requestDetail_loadsolution="0";
	this._requestDetail_loademailHistoryListFlag="0";
	this._requestDetail_optLogHtml="";
	this._requestDetail_config_extendedInfo="0";
	this._requestDetail_loadSolutionsFlag="0";
	this.scores=0;
	return {
		/**
		 * @description 选择指派组
		 * @param assigneeGroupNo 指派组No
		 * @param assigneeGroupName 指派组名称
		 */
		selectAssginGroup:function(assigneeGroupNo,assigneeGroupName){
			common.security.organizationTreeUtil.showAll_2('#requestAssginGroup_win','#assginGroupTree',assigneeGroupName,assigneeGroupNo,topCompanyNo);
		},
		/**
		 * @description 选择指派信息
		 * @param userName 用户名
		 * @param userId 用户Id
		 */
		selectRequestUser_byOrgNo:function(userName,userId){
			var attendance = $('#requestDetails_attendance').val();
			var start = $('#requestDetails_createdOn').val();
			var end = $('#requestDetails_slaResponsesTime').val();
			
			if($('#requestDetail_assigneeGroupName').val()!=""){
				common.security.userUtil.againAssignSelectUser_group1(attendance,start,end,'#requestDetail_assigneeName','#requestDetail_assigneeNo','','#requestDetail_assigneeGroupNo');
			}else{
				if($('#requestDetails_assigneeNo').val()===""&&$("#requestDetails_assigneeGroupNo").val()===""){
					common.security.userUtil.selectUserHolidayTip1(attendance,start,end,userName,userId,'','fullName',topCompanyNo);
				}else {
					if($('#requestDetails_assigneeNo').val()!=""){
						common.security.userUtil.againAssignSelectUser_group1(attendance,start,end,'#requestDetail_assigneeName','#requestDetail_assigneeNo','#requestDetails_assigneeLoginName','#requestDetails_assigneeGroupNo');
					}else{
						common.security.userUtil.againAssignSelectUser_group1(attendance,start,end,'#requestDetail_assigneeName','#requestDetail_assigneeNo','','#requestDetails_assigneeGroupNo');
					}
				}
			}
		},
		/**
		 * 保存解决方案（不保存到知识库）
		 */
		saveSlutionsOnly:function(){
			tag="only";
			itsm.request.requestDetailsFormCustom.saveSlutions();
		},
		/**
		 * 保存解决方案（保存到知识库）
		 */
		saveSlutionsBoth:function(){
			tag="both";
			itsm.request.requestDetailsFormCustom.saveSlutions();
			
		},
		/**
		 * 保存解决方案method
		 */
		saveSlutions:function(){
			var oEditor = CKEDITOR.instances.request_detail_solutions;
			var content = trim(oEditor.getData());
			content = content=content.replace(/&nbsp;/g,"").replace(/<p>/g,"").replace(/<\/p>/g,"");
			content=$.trim(content);
			$('#request_detail_solutions').val(content);
			if(content === ''){
				msgAlert(i18n.title_bpm_solutions_not_empty_validate,'info');
			}else{
				if($('#requestSolutionsDiv form').form('validate')){
						var frm = $('#requestSolutionsDiv form').serialize();
						var url = 'request!saveSolutions.action';
						startProcess();
						$.post(url,frm, function()
						{
							endProcess();
							if($("#requestDetails_offmode").val()!==''){
								$("#requestDetails_offmode").attr("disabled","true");
							}else{
								$("#requestDetails_offmode").attr("disabled","");
							}
							if(tag=="both"){
								itsm.request.requestDetailsFormCustom.saveToKnowledge();
							}else{
								msgShow(i18n.msg_request_solutionsSaveSuccessful,'show');
							}
							$("#request_solutions_effect_attachments_str").val("");
						});
					}
			}
		},
		
		/**
		 * @description 解决方案转为知识
		 */
		saveToKnowledge:function(){
			var oEditor = CKEDITOR.instances.request_detail_solutions;
			var content=trim(oEditor.getData());
			if($('#requestSolutionsDiv form').form('validate')){	
				if(content === ''){
					msgAlert(i18n.title_bpm_solutions_not_empty_validate,'info');
				}else{
					$('#request_detail_solutions').val(content);
					basics.tab.tabUtils.refreshTab(i18n.title_request_newKnowledge,"request!jumpToAddKnowledge.action?eno="+$('#requestDetails_requestNo').val());
				}
			}
		},
		/**
		 * @description 查看知识
		 */
		usefullKnowledge:function(){
			basics.tab.tabUtils.refreshTab(i18n.title_request_knowledgeGrid,"../pages/common/knowledge/knowledgeMain.jsp?keyWord="+$('#requestDetailDTO_keyWord').val());
		},
		/**
		 * @description 查看请求
		 */
		sameRequest:function(){
			showLeftMenu('../pages/itsm/request/leftMenu.jsp','leftMenu');	
			basics.tab.tabUtils.refreshTab(i18n.title_request_requestGrid,"../pages/itsm/request/requestMain.jsp?keyWord="+$('#requestDetailDTO_keyWord').val());
		},
		/**
		 * @description tab点击事件
		 */
		tabClick:function(){
	            $('#requestDetailsTab').tabs({
	                onSelect:itsm.request.requestDetailsFormCustom.tabClickEvents
	            });
		},
		/**
		 * @description 点击tab后，根据tab标题加载不同的数据
		 * @param title tab标题
		 */
		tabClickEvents:function(title){
			if(title==i18n.title_request_task){//任务
				common.tools.includes.includes.loadEventTaskIncludesFile();//加载任务窗口
				if(_requestDetail_loadTaskFlag=="0"){
					var requestEventTask=new eventTask($('#requestDetails_requestNo').val(),'request');
					requestEventTask.eventTaskGrid(request_task_tool_show_hide);
					requestDetailsGrids.push('#requestEventTaskGrid');
					_requestDetail_loadTaskFlag="1";
				}
			}
			if(title==i18n.title_request_timeDetail){//进展及成本
				common.tools.includes.includes.loadEventTimeCostIncludesFile();//加载进展及成本窗口
				if(_requestDetail_loadTimeDetailFlag=="0"){
					var requestEventCost=new eventCost($('#requestDetails_requestNo').val(),'request');
					requestEventCost.eventCostGrid(request_cost_tool_show_hide);
					requestDetailsGrids.push('#requestEventCostGrid');
					_requestDetail_loadTimeDetailFlag="1";
				}
			}
			
			if(title==i18n.title_related_other_request){//关联的其他请求
				itsm.request.requestDetailsFormCustom.relatedOtherRequest();
				requestDetailsGrids.push('#relatedOtherRequestGrid');
			}
			
			if(title==i18n.title_request_history){//请求历史
				var requestHistoryRecord=new historyRecord('requestHistoryRecord',$('#requestDetails_requestNo').val(),'itsm.request');
				requestHistoryRecord.showHistoryRecord();
			}
			if(title==i18n.title_request_emailHistory){//邮件历史
				
				if(_requestDetail_loademailHistoryListFlag=="0"){
					itsm.request.requestEmailHistory.emailHistoryList("emailHistory",$('#requestDetail_requestCode').val(),$("#requestDetails_requestNo").val());
					requestDetailsGrids.push('#emailHistoryGrid');
					_requestDetail_loademailHistoryListFlag="1";
				}
			}
			if(title==i18n.attachment){//附件

				if(_requestDetail_loadAttachementTag=="0"){
				    common.tools.event.eventAttachment.showEventAttachment('show_request_attachment',$('#requestDetails_requestNo').val(),'itsm.request',false);
					_requestDetail_loadAttachementTag="1";
				}
				
			}
			if(title==i18n.historyProblem){//历史问题
				itsm.problem.relatedProblem.showRelatedProblem('request',$('#requestDetails_requestNo').val(),'#requestRelatedProblemGrid','#requestRelatedProblemPager');
				requestDetailsGrids.push('#requestRelatedProblemGrid');
			}
			if(title==i18n.title_historyChange){//历史变更
				itsm.request.requestDetailsFormCustom.requestRelatedChange();
				requestDetailsGrids.push('#requestRelatedChangeGrid');
			}
			if(title==i18n.returnItem){//回访事项
				itsm.request.requestDetailsFormCustom.returnItemRequest();
				requestDetailsGrids.push('#returnItemRequestGrid');
				//$('#request_visit_refresh').click(function(){itsm.request.requestDetailsFormCustom.showReturnVisitDetail($('#requestDetail_visitId').val());});
				//itsm.request.requestDetailsFormCustom.showReturnVisitDetail($('#requestDetail_visitId').val());
			}
			if(title==i18n.config_extendedInfo&&_requestDetail_config_extendedInfo=="0"){//扩展信息
				common.eav.attributes.showAttributeInfoByEno($('#requestDetails_requestNo').val(),$('#requestDetails_categoryEavId').val(),'request_info_eavAttributet');
				_requestDetail_config_extendedInfo="1";
			}
			if(title==i18n.label_request_relat_ci){//请求人关联的配置项
				itsm.request.requestDetailsFormCustom.showCiGrid($('#requestDetails_fullName').val());
			}
			
			if(title==i18n.label_bpm_processHistoryTask){//流程历史记录
				var flowIsEnd = false;
				var endTime=$('#requestDetails_closeTime').val();
				if(endTime!=null && endTime!==''){
					flowIsEnd = true;
				}
				var pid=$('#requestDetails_pid').val();
				common.jbpm.processCommon.getPorcessHistoryTask('requestProcessHistoryTask',pid,flowIsEnd);
				
				setTimeout(function(){
					//自助解决时显示关闭时间 Will
					pid=pid.substring(0,pid.indexOf('.'));
					if(flowIsEnd &&($('#request_HistoryTaskEndTime'+pid).html()===null || $('#request_HistoryTaskEndTime'+pid).html()==='')){
						$('#request_HistoryTaskEndTime'+pid).text(timeFormatter(endTime));
						$('#request_HistoryTaskOutcome'+pid).text(i18n.requestDirectClose);
					}
				},1000);
				
			}
			if(title==i18n.label_solutions){//解决方案
				if(_requestDetail_loadSolutionsFlag=="0"){
					$('#requestDetails_offmode').val(requestDetail_offmodeNo);
					if(requestDetail_offmodeNo!==''){
						$("#requestDetails_offmode").attr("disabled","true");
					}else{
						$("#requestDetails_offmode").attr("disabled","");
					}
					//initFileUpload("_RequestSolutions","","",userName,"","request_solutions_effect_attachments_str","request_solutions_effect_attachments_uploaded");
					getUploader('#request_solutions_effect_file','#request_solutions_effect_attachments_str','#request_solutions_effect_attachments_uploaded','request_solutions_effect_fileQueue');
					//加载解决方案附件
					itsm.request.requestDetailsFormCustom.requestSolutionAttachments();
					_requestDetail_loadSolutionsFlag="1";
				}
				
			}				
		},
		/**
		 * @description 配置項列表.
		 * @param loginName 用户登录名
		 */
		showCiGrid:function(loginName){
			requestDetailsGrids.push('#requestUserRelatedCiGrid');
			if($('#requestUserRelatedCiGrid').html()!==''){
				var ci_url ='ci!findPageConfigureItemByUser.action';
				$('#requestUserRelatedCiGrid').jqGrid('setGridParam',{page:1,url:ci_url}).trigger('reloadGrid');
			}else{
				var params = $.extend({},jqGridParams,{
//					caption:i18n['ci_configureItemList'],
					url:'ci!findPageConfigureItemByUser.action',
					postData:{'ciQueryDTO.loginName':loginName},
					colNames:['',i18n.lable_ci_assetNo,i18n.name,i18n.ci_configureItemCategory,i18n.status],
					colModel:[
					          {name:'ciId',hidden:true},
							  {name:'cino',align:'center',width:30,formatter:itsm.cim.configureItemUtil.cinoGridFormatter},
							  {name:'ciname',align:'center',width:30,formatter:itsm.cim.configureItemUtil.cinameGridFormatter},
							  {name:'categoryName',align:'center',width:20,sortable:false},
							  {name:'status',align:'center',width:10,sortable:false}
							  ],
					toolbar:false,
					jsonReader: $.extend(jqGridJsonReader,{id: "ciId"}),
					sortname:'ciId',
					multiselect:false,
					pager:'#requestUserRelatedCiPager',
					ondblClickRow:function(rowId){
						itsm.request.requestDetailsFormCustom.configureItemInfo(rowId);
					}
				});
				$('#requestUserRelatedCiGrid').jqGrid(params);
				$('#requestUserRelatedCiGrid').navGrid('#requestUserRelatedCiPager',navGridParams);
				setGridWidth("#requestUserRelatedCiGrid","requestDetails_center",9);
			}	
		},
		/**
		 * 配置项No格式化
		 */
		cinoGridFormatter:function(cell,event,data){
			return data.cino;
		},
		/**
		 * 配置项名称格式化
		 */
		cinameGridFormatter:function(cell,event,data){
			return data.ciname;
		},
		/**@description 打开配置项详细信息页面
		 * @param ciId 配置项Id
		 * */
		configureItemInfo:function(ciId){
			basics.tab.tabUtils.reOpenTab('ci!findByciId.action?ciEditId='+ciId,i18n.ci_configureItemInfo);
		},
		/**
		 * 选择负责人
		 * @param userName 用户名元素
		 * @param userId 用户Id元素
		 */
		request_selectUser:function(userName,userId){
			common.security.userUtil.selectUser(userName,userId,'fullName','',topCompanyNo);			
		},
		/**
		 * @description 打开选择用户窗口
		 * @param id1 用户名元素
		 * @param id2 用户Id元素
		 */
		requestDetails_SelectUser_win:function(id1,id2){
			assigneeName_id=id1;
			assigneeNo_id=id2;
			openSelectWindow();
		},
		/**
		 * @description 提交变更
		 */
		requestInfoToChange:function(){
			basics.tab.tabUtils.reOpenTab('../pages/itsm/change/addChange.jsp?enos='+$('#requestDetails_requestNo').val()+'&companyNo='+$('#requestDetails_companyNo').val(),i18n.titie_change_add);
		},
		/**
		 * @description 提交问题
		 */
		requestInfoToProblem:function(){
			basics.tab.tabUtils.reOpenTab('../pages/itsm/problem/addProblem.jsp?enos='+$('#requestDetails_requestNo').val()+'&companyNo='+$('#requestDetails_companyNo').val(),i18n.problem_add);
		},
		/**
		 * @description 请求编辑
		 */
		requestDetail_edit:function(){
			basics.tab.tabUtils.reOpenTab("itsm/request/editRequestFormCustom.jsp?eno="+$('#requestDetails_requestNo').val(),i18n.title_request_editRequest);
		},
		/**
		 * 数据列表伸展
		 */
		requestDetailsPanelCollapseAndExpand:function(){
			 $(requestDetailsGrids).each(function(i, g) {
			      setGridWidth(g, 'requestDetailsTab', 10);
			 });
		},
		/**
		 * 数据列表自动伸展
		 */
		fitRequestDetailsGrids:function(){
			$('#requestDetails_west,#requestDetails_center').panel({
				onCollapse:itsm.request.requestDetailsFormCustom.requestDetailsPanelCollapseAndExpand,
				onExpand:itsm.request.requestDetailsFormCustom.requestDetailsPanelCollapseAndExpand,
				onResize:function(width, height){
					setTimeout(function(){
						itsm.request.requestDetailsFormCustom.requestDetailsPanelCollapseAndExpand();
					},0);
				}
			});
		},
		/**
		 * 打开请求邮件回复窗口
		 */
		rquestActionEmailReply_win:function(){
			var requestEmailReplyTitle=i18n.rquestTreatmentProgress+':('+$('#requestCode').val()+')';
			$('#requestEmailReplyTitle').val(requestEmailReplyTitle);
			$('#requestEmailReplyUser').val($('#rquestDetailCreatedByEmail').val());
			windows('rquestActionEmailReply',{width:650});
		},
		/**
		 * 请求邮件回复
		 */
		rquestActionEmailReply:function(){
			var requestEmailReplyTitle= $('#requestEmailReplyTitle').val();
			 $('#requestEmailReplyTitle').val(trim(common.security.xssUtil.html_encode(requestEmailReplyTitle)));
			 var content= $('#content').val();
			 $('#content').val(trim(common.security.xssUtil.html_encode(content)));
			if($('#rquestActionEmailReply form').form('validate')){
				if(!checkEmail($('#requestEmailReplyUser').val())){//邮件验证是否正确
					msgAlert(i18n.emailFormatError,'info');
				}else{
					var _param = $('#rquestActionEmailReply form').serialize();
					startProcess();
					$.post('email!testEmail.action','isSend=true', function(data){
						if(data){
							var _url='email!toEmail.action';
							$.post(_url,_param,function(result){
								endProcess();
								if(result){
									$('#rquestActionEmailReply').dialog('close');
									resetForm("#rquestActionEmailReplyForm");
									msgShow(i18n.msg_sendSuccessful,'show');
								}else{
									msgShow(i18n.sendFailure,'show');
								}
									
							});
						}else{endProcess();
							msgShow(i18n['ERROR_EMAIL_ACCOUNT_ERROR'],'show');
						}
					});
				}
			}
		},
		
		/**
		 * @description 关联变更
		 */
		requestRelatedChange:function(){
			if($("#requestRelatedChangeGrid").html()===''){
				var _postData={'queryDTO.relatedEno':$('#requestDetails_requestNo').val(),'queryDTO.relatedType':'request'};
				var params = $.extend({},jqGridParams, {	
					url:'change!findRelatedChangePager.action',
					postData:_postData,
//					caption:i18n['title_changeList'],
					colNames:[i18n.title_snmp_id,i18n.number,i18n.title,i18n.status,i18n.priority,i18n.common_createTime,i18n.title_creator],
					colModel:[
				   		{name:'eno',width:60,sortable:true},
				   		{name:'changeNo',width:150},
				   		{name:'etitle',width:150},
				   		{name:'statusName',width:120,sortable:false},
				   		{name:'priorityName',width:120,sortable:false},
				   		{name:'createTime',width:120,sortable:false, formatter: timeFormatter},
				   		{name:'creator',width:150,sortable:false}
				   	],
				   	toolbar:false,
					jsonReader: $.extend(jqGridJsonReader, {id: "eno"}),
					sortname:'eno',
					multiselect:false,
					ondblClickRow:function(rowId){itsm.request.requestDetailsFormCustom.requestRelatedChangeInfo(rowId);},
					pager:'#requestRelatedChangePager'
				});
				$("#requestRelatedChangeGrid").jqGrid(params);
				$("#requestRelatedChangeGrid").navGrid('#requestRelatedChangePager',navGridParams);
				 //自适应大小
//				setGridWidth("#requestRelatedChangeGrid","requestDetails_center",9);
			}else{
				$("#requestRelatedChangeGrid").trigger('reloadGrid');
			}	
		},
		/**
		 * @description 变更详细
		 * @param id 变更eno
		 */
		requestRelatedChangeInfo:function(id){
			basics.tab.tabUtils.reOpenTab('change!changeInfo.action?queryDTO.eno='+id+'&random=',i18n.change_detail);
		},
		/**
		 * @description 相关联的其他请求
		 */
		relatedOtherRequest:function(){
			if($('#relatedOtherRequestGrid').html()===''){
				var _postData={};
				_url="request!analogousRequest.action";						
				$.extend(_postData,{'fullTextQueryDTO.alias':'RequestInfo','fullTextQueryDTO.queryString':$('#requestDetails_requestTitle').val()});
				
				var params=$.extend({},jqGridParams,{
//					caption:i18n['title_related_other_request'],
					url:_url,
					postData:_postData,
					colNames:['ID',i18n.common_id,i18n.common_title,i18n.category,i18n.requester,i18n.title_request_assignToGroup,i18n.title_request_assignToTC,i18n.priority,i18n.common_state,i18n.common_createTime],
					colModel:[
					          {name:'eno',width: 40},
							  {name:'requestCode',width:70},
							  {name:'etitle',width:100,formatter:itsm.request.requestDetailsFormCustom.requestTitleUrl},
							  {name:'requestCategoryName',width:50,index:'requestCategory',align:'center'},
							  {name:'createdByName',width:40,sortable:false,align:'center'},
							  {name:'assigneeGroupName',width:50,sortable:false,align:'center'},
							  {name:'assigneeName',width:55,sortable:false,align:'center'},
							  {name:'priorityName',width:35,align:'center',index:'priority'},
							  {name:'statusName',width:35,index:'status',align:'center'},
							  {name:'createdOn',width:80,formatter:timeFormatter,align:'center'}
							  ],
					jsonReader: $.extend({},jqGridJsonReader, {id: "eno"}),
					ondblClickRow:function(rowId){itsm.request.requestDetailsFormCustom.requestDetails(rowId);},
					sortname:'eno',
					toolbar:false,
					multiselect:false,
					pager:'#relatedOtherRequestPager'
				});
				$("#relatedOtherRequestGrid").jqGrid(params);
				$("#relatedOtherRequestGrid").navGrid('#relatedOtherRequestPager',navGridParams);
				//自适应大小
//				setGridWidth("#relatedOtherRequestGrid","requestDetails_center",9);
			}else{
				$('#relatedOtherRequestGrid').trigger('reloadGrid');
			}	
		},
		/**
		 * 请求标题格式化
		 */
		requestTitleUrl:function(cell,opt,data){
			return '<a href=javascript:itsm.request.requestDetailsFormCustom.requestDetails('+data.eno+')>'+cell+'</a>';
		},
		/**
		 * @description 请求回访详细
		 */
	    returndetailForma:function(cellvalue){
	    	return '<a href="javascript:common.tools.visit.returnVisitManage.viewUserReturnVisitDetail('+cellvalue+')" >['+i18n.return_detail+']</a>';
	    },
	    /**
	     * 请求回访事项列表
	     */
		returnItemRequest:function(){
			if($('#returnItemRequestGrid').html()===''){
				var _postData={};
				_url="userReturnVisit!findPagerReturnVisit.action?queryDTO.companyNo=-1";
				$.extend(_postData,{'queryDTO.eno':$('#requestDetails_requestNo').val()});
				var params=$.extend({},jqGridParams,{
//					caption:i18n['Visit'],
					url:_url,
					postData:_postData,
					colNames:['ID',i18n.label_returnVisit_satisfaction,i18n.label_returnVisit_state,i18n.label_returnVisit_sendTime,i18n.label_returnVisit_replyTime,i18n.label_returnVisit_object,i18n.label_returnVisit_user,i18n.return_detail],
					colModel:[
				   		{name:'visitId',align:'center',width:25,sortable:true},
				   		{name:'satisfaction',align:'center',index:'satisfaction',width:80,formatter:common.tools.visit.returnVisitManage.satisfactionForma},
				   		{name:'state',align:'center',width:150,formatter:common.tools.visit.returnVisitManage.stateForma},
				   		{name:'returnVisitSubmitTime',align:'center',width:150,formatter:timeFormatter},
				   		{name:'returnVisitTime',align:'center',width:150,formatter:timeFormatter},
				   		{name:'returnVisitUser',align:'center',width:150},
				   		{name:'returnVisitSubmitUser',align:'center',width:150},
				   		
				   		{name:'visitId',sortable:false,align:'center',width:100,formatter:itsm.request.requestDetailsFormCustom.returndetailForma},
				   	],
					jsonReader: $.extend({},jqGridJsonReader, {id: "visitId"}),
					sortname:'visitId',
					multiselect:false,
					toolbar:[false,"top"],
					pager:'#returnItemRequestPager'
				});
				$("#returnItemRequestGrid").jqGrid(params);
				$("#returnItemRequestGrid").navGrid('#returnItemRequestPager',navGridParams);
			}else{
				$('#returnItemRequestGrid').trigger('reloadGrid');
			}	
		},
		/**
		 * @description 请求详细
		 * @param eno 请求eno
		 */
		requestDetails:function(eno){
			basics.tab.tabUtils.reOpenTab('request!requestDetails.action?eno='+eno,i18n.request_detail);
		},
		/**
		 * 设置请求动作通知
		 * @param checkedId 复选框元素
		 * @param emailInputId 邮件文本框元素
		 */
		requestActionNoticeSet:function(checkedId,emailInputId){
			if($(checkedId).attr('checked'))
	    		$(emailInputId).show();
	    	else
	    		$(emailInputId).hide();
		},
		/**
		 * 选择邮件通知用户
		 * @param inputId  用户Id元素
		 * @param showType 显示类型
		 */
		selectNoticeUser:function(inputId,showType){
			common.security.userUtil.selectUserMulti(inputId,'',showType,'-1');
		},
		/**
		 * @description 请求工单
		 */
		requestToPrint:function(){			
			window.open('../pages/itsm/request/printTemplates/callTable.jsp?eno='+$('#requestDetails_requestNo').val());
		},
		/**
		 * 请求回访详情
		 * @param visitId 回访Id
		 */
		showReturnVisitDetail:function(visitId){
			if(!isNaN(visitId)){
				$.post('userReturnVisit!findUserReturnVisitById.action','queryDTO.visitId='+visitId,function(data){
					$('#requestDetail_userReturnVisit_detail_satisfaction').text(common.tools.visit.returnVisitManage.satisfactionForma(data.satisfaction));
					$('#requestDetail_userReturnVisit_detail_returnVisitDetail').html(data.returnVisitDetail);
					$('#requestDetail_userReturnVisit_detail_state').text(common.tools.visit.returnVisitManage.stateForma(data.state));
					$('#requestDetail_userReturnVisit_detail_returnVisitSubmitTime').text(timeFormatter(data.returnVisitSubmitTime));
					$('#requestDetail_userReturnVisit_detail_returnVisitTime').text(timeFormatter(data.returnVisitTime));
					$('#requestDetail_userReturnVisit_detail_returnVisitUser').text(data.returnVisitUser);
					$('#requestDetail_userReturnVisit_detail_returnVisitSubmitUser').text(data.returnVisitSubmitUser);
				});
			}
			
		},
		/**
		 * 回访事项Tab是否显示
		 */
		showReturnVisitShowHide:function(){
			var visitRecordId=$('#requestDetail_visitRecord').val();
			if(visitRecordId!=null && visitRecordId!=='' && !isNaN(visitRecordId)){
				$.post('userReturnVisit!findUserReturnVisitById.action','queryDTO.visitId='+visitRecordId,function(data){
					if(data.state==1)
						$('#requestDetails_revisit').hide();
				});
			}
		},
		/**
		 * 根据实例显示动作
		 */
		showActivityNamesByInstance:function(){
			$.post('upload!findActivityNamesByInstance.action','instanceId='+$('#requestDetails_pid').val(),function(data){
				for(i=0;i<data.length;i++){
					$('#show1').append(data[i]+',');
				}
			});
			$.post('upload!getOutcomes.action','instanceId='+$('#requestDetails_pid').val(),function(data){
				for(i=0;i<data.length;i++){
					$('#show2').append(data[i]+',');
				}
			});
		},
		/**
		 * 请求附件上传
		 */
		request_uploadifyUpload:function(){
			if($('#request_effect_fileQueue').html()!=="")
				$('#request_effect_file').uploadifyUpload();
			else
				msgShow(i18n.msg_add_attachments,'show');
		},
		/**
		 * 请求解决方案附件上传
		 */
		request_solutions_uploadifyUpload:function(){
			if($('#request_solutions_effect_fileQueue').html()!=="")
				$('#request_solutions_effect_file').uploadifyUpload();
			else
				msgShow(i18n.msg_add_attachments,'show');
		},
		/**
		 * 移除请求解决方案中关联的服务目录
		 * @param id 服务目录Id
		 * @param score 服务目录分值
		 */
		removeSolutionTr:function(id,score){
			msgConfirm(i18n.tip,i18n.msg_confirmDelete,function(){
				var ids = new Array();
				ids.push(id);
				var url = 'request!removeServiceDirectory.action';
				var param = $.param({'requestDetailDTO.eno':$('#requestDetails_requestNo').val(),'serviceDirectory':ids},true);
				$.post(url,param,function(){
					//将解决方案保从请求里去除 
					//将解决方案保存到请求里
					var total = $('#totalScores').html();
					var index = total.indexOf(':');
					total = total.substring(index+2);
					total = parseInt(total);
					total = total - parseInt(score);
					scores = total;
					$('#totalScores').html(i18n.totalScores+' : '+scores);
					$('#solution_tr_'+id).remove();
					msgShow(i18n.deleteSuccess,'show');
				});
			});		
		},
		/**
		 * 查询与请求服务目录相关的知识库
		 */
		select_service_knowledge:function(){
			itsm.request.requestDetailsFormCustom.select_GQgried_ByNames();
		},
		knowledgeGridTitleFormatter: function (cell, opt, data) {
            return "<a href=javascript:common.knowledge.knowledgeDetail.showKnowledgeDetail('" + data.kid + "')>" + cell + "</a>";
        },
		/**
		 * 查询与请求服务目录相关的知识库
		 */
		select_GQgried_ByNames:function(){
			$('#select_service_grid_show').trigger('reloadGrid');
			/*var frms =$("#serviceNosFRMs form").getForm();
			var urls='knowledgeInfo!findAllKnowledges.action';
			var _postData={};
			$.extend(_postData,frms};*/
			var sdata = $('#serviceNosFRM').serialize();
			var urls='knowledgeInfo!findAllKnowledges.action?knowledgeQueryDto.opt=&'+sdata;
			var postData = $("#select_service_grid_show").jqGrid("getGridParam");     
			var params = $.extend({},jqGridParams, {
                url:urls,
                postData:postData,
                colNames: [i18n.number,i18n.label_knowledge_relatedService,i18n.title,i18n.title_creator, i18n.knowledge_knowledgeCategory, i18n.title_createTime,i18n.check],
                colModel: [
                           {name: 'kid', width: 50,hidden: true}, 
                           {name:'knowledgeServiceName',width:120,align:'center',sortable: false},
                           {name: 'title', width: 180,formatter: itsm.request.requestDetailsFormCustom.knowledgeGridTitleFormatter}, 
                           {name: 'creatorFullName',width: 80,align: 'center'},
                           {name: 'categoryName',width: 80,align: 'center',sortable: false}, 
                           {name: 'addTime',width: 120,align: 'center',formatter: timeFormatter},
                           {name:'act',width:80,sortable:false,align:'center',formatter:itsm.request.requestDetailsFormCustom.userGridFormatter}
                ],
                jsonReader: $.extend(jqGridJsonReader, {id: "kid"}),
                sortname: 'kid',
                height:'100%',
                multiselect:false,
                ondblClickRow:function(){itsm.request.requestDetailsFormCustom.bind_request_services();},
                pager: '#select_service_grid_page'
            });
			$("#refresh_select_service_grid_show").remove();
            $("#select_service_grid_show").jqGrid(params);
            $("#select_service_grid_show").navGrid('#select_service_grid_page',navGridParams);
            $("#t_select_service_grid_show").css(jqGridTopStyles);
            $("#t_select_service_grid_show").html("");
            $("#t_select_service_grid_show").append($('#request_Detail_button').html());
			//清空列表
           /* var _param = $.param({'knowledgeQueryDto.knowledgeServiceNo':numbers,'knowledgeQueryDto.title':names},true);
			var _url="knowledgeInfo!findAllKnowledges.action";
			$.post(_url,_param,function(){
				$('#select_service_grid_show').trigger('reloadGrid');
			});*/
		},
		/**
		 * 选择知识库
		 */
		userGridFormatter:function(cell,event,data){
			return '<div style="padding:0px">'+
			'<a href="javascript:itsm.request.requestDetailsFormCustom.bind_request_services()" title="'+i18n.check+'">'+
			'<img src="../images/icons/ok.png"/></a>'+
			'</div>';
		},
		/**
		 * 请求审批发送邮件
		 */
		send_Email_appro:function(){
			var user_ID=$("#requestDetail_assigneeNo_Appro").val();
			var user_Find_Url="user!findUserById.action";
			var frms="userDto.userId="+user_ID;
			if($('#request_openGetUser_from').form('validate')){
				$.post('noticeRule!testemai.action',function(testErrorEncode){
					if(testErrorEncode===0){	
						$.post("user!setUserRandomId.action?userDto.userId="+user_ID,function(getUserData){
							var ck=/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/;
				        	if(ck.test(getUserData.email)){
				        		$.post("user!setUserRandomId.action?userDto.loginName="+userName,function(setUserData){
									if(getUserData != null && getUserData.email != null){
										var url="noticeRule!apporCountersign.action";
										var local="";
											$.post("serverUrl!findServerUrl.action",function(urls){
												if(urls.urlName==null){
													msgShow(i18n.lable_request_appor_send_email_error,'show');
												}else{
													local=urls.urlName+"/pages";
													var noticeFRM="noticeRuleDTO.noticeRuleNo=requestApprovalPlusSignNotice&templateVariableDTO.eventCode="+$("#requestCode").val()+"&noticeRuleDTO.technicianOrEmail="+getUserData.email+"&templateVariableDTO.assigneeName="+getUserData.loginName+"&templateVariableDTO.operator="+fullName+"&templateVariableDTO.edesc="+$("#appro_User").val()+"&templateVariableDTO.eno="+$("#requestDetails_requestNo").val()+"&templateVariableDTO.url="+local;
													noticeFRM+="&templateVariableDTO.randomParmone="+getUserData.randomId+"&templateVariableDTO.randomParmtwo="+setUserData.randomId;
													startProcess();
													$.post(url,noticeFRM,function(datas){
														if(datas){
															var _urls="historyRecord!saveHistoryRecord.action";
															var frm="hisotryRecordDto.operator="+userName+"&hisotryRecordDto.logTitle="+i18n.lable_request_appro_user+"&hisotryRecordDto.logDetails="+i18n.Endorsement_personnel+":"+$("#appro_User_Name").val()+"<br>"+i18n.remark+":"+$("#appro_User").val()+"&hisotryRecordDto.eno="+$("#requestDetails_requestNo").val()+"&hisotryRecordDto.eventType=itsm.request";
															$.post(_urls,frm,function(data){
																endProcess();
																msgShow(i18n.common_operation_success,'show');
																$("#request_openGetUser").dialog('close');
															});
														}else{
															msgShow(i18n.ERROR_EMAIL_ACCOUNT_ERROR,'show');
														}
													});
												}
												
											});
									}
								});
				        	}else{
				        		msgShow("未配置该加签人员的邮箱",'show');
				        		//msgAlert("未配置该加签人员的邮箱",'info');
				        	}
							
					});
				}else{
						endProcess();
						msgShow(i18n.ERROR_EMAIL_ACCOUNT_ERROR,'show');
					}
			});
			}
		},
		/**
		 * 选择已有解决方案
		 */
		selectExistSolution:function(){
			windows('select_have_services',{width:640});//打开服务目录选择窗口
			itsm.request.requestDetailsFormCustom.select_service_knowledge();
			itsm.request.requestDetailsFormCustom.selectServicesDir();
		/*	var ids = new Array();
			var categoryIds=new Array();*/
			$('#request_services').unbind().click(function(){
				itsm.request.requestDetailsFormCustom.getSelectedServicesDirNodes();
				//var str='';
				//$("#request_services_select_tree").jstree('get_checked').each(function (i,n) {
			/*	$("#request_services_select_tree").jstree("get_checked",false,true).each(function (i,n) { 
					var node = jQuery(this);
					var ids = node.attr("id");
					categoryIds[i]=ids;*/
					
					/*if($('#solution_tr_'+id).html()=='' || $('#solution_tr_'+id).html()==null){
						ids.push(id);
						$('#totalScores_tr').before('<tr id="solution_tr_'+id+'"><td>'+node.attr("cname")+'</td><td>'+node.attr('scores')+'</td>'+
								'<td><a href=javascript:itsm.request.requestDetailsFormCustom.removeSolutionTr('+id+','+node.attr('scores')+')>'+
								'<img src="../images/icons/delete.gif" border="0" /></a></td></tr>');
						scores += parseInt(node.attr('scores'));	
					}		*/		
				//});
	
				
				/*if(ids.length>0){
					$('#totalScores').html(i18n['totalScores']+' : '+scores);
					//将解决方案保存到请求里
					var url = 'request!saveServiceDirectory.action';
					var param = $.param({'requestDetailDTO.eno':$('#requestDetails_requestNo').val(),'serviceDirectory':ids},true);
					$.post(url,param,function(){
						$("#request_services_select_tree").jstree("uncheck_all");//清空
						$('#request_services_select_window').dialog('close');
					});
				}			*/
			});
		},
		
		/**
		 * 选择服务目录.显示机构信息(规则集)
		 */
		selectServicesDir:function(){
			$('#request_services_select_tree').jstree({
				"json_data":{
				    ajax: {
				    		url : "event!getCategoryTree.action?num=0",
				    		data:function(n){
					    	  return {'types': 'Service','parentEventId':n.attr ? n.attr("id").replace("node_",""):0};//types is in action
				    		},
				    		cache:false}
				},
				"plugins" : [ "themes", "json_data", "checkbox" ]
			});	
		},
		/**
		 * 
		 * 选择服务目录显示，获取windwos面板上选择的值(规则集)
		 */
		getSelectedServicesDirNodes:function(){
			var namePut1="";
			 var classiftyid="";
			 var categoryIds=new Array();
			 var idArray = "";
			$("#request_services_select_tree").jstree("get_checked",false,true).each(function (i,n) { 
				 var node = jQuery(this); 
				 var ids=node.attr('id');
				 if(ids==null){//校正orgNo
					 ids=1;
				 }
				 categoryIds[i]=ids;	
			});
			if(categoryIds.length>0){
				var url="event!findSubCategoryArray.action";
				var param = $.param({'categoryNos':categoryIds},true);
				$.post(url,param,function(res){
					 for(var i=0;i<res.length;i++){
						if($('#solution_tr_'+res[i].eventId).html()==='' || $('#solution_tr_'+res[i].eventId).html()==null){
							idArray="1";
							$('#totalScores_tr').before('<tr id="solution_tr_'+res[i].eventId+'"><td>'+res[i].eventName+'</td><td>'+res[i].scores+'</td>'+
									'<td><a href=javascript:itsm.request.requestDetailsFormCustom.removeSolutionTr('+res[i].eventId+','+res[i].scores+')>'+
									'<img src="../images/icons/delete.gif" border="0" /></a></td></tr>');
							scores += parseInt(res[i].scores);	
						}				
					 }
					 if(idArray=="1" ){
							$('#totalScores').html(i18n.totalScores+' : '+scores);
							//将解决方案保存到请求里
							var url = 'request!saveServiceDirectory.action';
							var param = $.param({'requestDetailDTO.eno':$('#requestDetails_requestNo').val(),'serviceDirectory':categoryIds},true);
							$.post(url,param,function(){
								$("#request_services_select_tree").jstree("uncheck_all");//清空
							});
						}
				});
			}
			$('#request_services_select_window').dialog('close');
				
		},
		/**
		 * 显示服务级别详情
		 */
		showSLADetail:function(){
			var slaRuleNo=$("#requestDetailSlaRuleNo").val();
			var _url="slaRule!findBySlaNo.action";
			$.post(_url,{"slaRuleDTO.ruleNo":slaRuleNo},function(data){
				$("#requestSLAName").text(data.ruleName);
				$("#requestSLAResponseTime").text(itsm.request.requestDetailsFormCustom.slaTimeFormatter(data.showRespondTime));
				$("#requestSLACompleteTime").text(itsm.request.requestDetailsFormCustom.slaTimeFormatter(data.showFinishTime)); 
				$("#requestSLAsalience").text(data.salience);
				$("#requestSLAContractName").text(data.contractName);
				$("#requestSLAStartTime").text(data.beginTime);
				$("#requestSLAEndTime").text(data.endTime);
				$("#requestSLAServiceName").text(data.companyName);
				windows('request_show_sla_window',{width:400});
			});
		},
		/**
		 * sla时间格式化
		 */
		slaTimeFormatter:function(data){
			return data.replace("DD",i18n.label_slaRule_days)
			.replace("HH",i18n.label_slaRule_hours)
			.replace("MM",i18n.label_slaRule_minutes);
		},
		/**
		 * 添加请求解决方案附件
		 */
		requestSolutionAttachments:function(){
			var eno = $('#requestDetails_requestNo').val();
			var _url='request!findCommentDTOByEno.action?eno='+eno;
			$.post(_url,function(data){
				if(data!=null){
					for ( var i = 0; i < data.attachments.length; i++) {
						$("#request_solutions_effect_attachments_uploaded").append("<div id=\"rootCauses_attr_"+data.attachments[i].aid+"\" style=\"padding:3px\"><span id=\"rootCauses_attr_icon_"+data.attachments[i].aid+"\"></span><a href=\"attachment!download.action?downloadAttachmentId="+data.attachments[i].aid+"\" target=\"_bank\">"+data.attachments[i].attachmentName+"</a> <a onclick=\"itsm.request.requestDetailsFormCustom.removeAttr('#rootCauses_attr_"+data.attachments[i].aid+"','"+data.attachments[i].aid+"');\">"+i18n.deletes+"</a></div>");
						$('#rootCauses_attr_icon_'+data.attachments[i].aid+'').html(common.tools.file.attachIcon.getIcon(data.attachments[i].attachmentName));
					}
				}
			});
		},
		/**
		 * 移除解决方案附件
		 * @param divId 解决方案元素
		 * @param aid 附件Id
		 */
		removeAttr:function(divId,aid){
			confirmBeforeDelete(function(){
				var eno = $('#requestDetails_requestNo').val();
				$(divId).remove();//移除行
    			var url = 'request!deleteAttachment.action?eno='+eno+'&aid='+aid;
    			//调用
    			startProcess();
    			$.post(url, function(res){				
    					endProcess();//销毁
    					msgShow(i18n.common_attachment_del_success,'show');
    			});
			});
		},
		/**
		 * 根据服务目录查询知识库
		 */
		bind_request_services:function(){
			checkBeforeEditGrid("#select_service_grid_show", function (rowData) {
				var oEditor = CKEDITOR.instances.request_detail_solutions;
				var url="knowledgeInfo!findKnowledgeById.action?kid="+rowData.kid;
				$.post(url,function(data){
					oEditor.setData(data.content);
					$("#knowledgeNo").val(rowData.kid);
				});
				$('#select_have_services').dialog('close');
			});
		},
		/**
		 * 查询知识
		 */
		select_knowledge_ByName:function(){
			var names=$("#t_select_service_grid_show input").attr("value");
			
			/*var postData = $("#select_service_grid_show").jqGrid("getGridParam", "postData");
			$.extend(postData, {'knowledgeQueryDto.title':names});  //将postData中的查询参数加上查询表单的参数
			$('#select_service_grid_show').trigger('reloadGrid',[{"page":"1"}]);
			itsm.request.requestDetailsFormCustom.select_GQgried_ByNames(names);*/
			var sdata = $('#serviceNosFRM').serialize();
			var _url="knowledgeInfo!findAllKnowledges.action?knowledgeQueryDto.opt=";
			if(sdata!=="")
				_url=_url+"&"+sdata;
			 $('#select_service_grid_show').jqGrid('setGridParam', {
	                url: _url,
	                postData:{'knowledgeQueryDto.title':names}
	            }).trigger('reloadGrid');
		},
		/**
		 * 获取知识标题的值
		 */
		select_Onchage_Set_Value:function(){
			var names=$("#t_select_service_grid_show input").attr("value");
			$("#knowledgeTitle").attr("value",names);
		},
		/**
		 * 更新服务目录
		 */
		updateserviceDirStr:function(){
			var newStr=trim($("#serviceDirId").text());
			var strs=newStr.substring(0,newStr.length-1);
			$("#serviceDirId").text(strs);
		},
		/**
		 * 初始化详情表单
		 */
		initDetailForm:function(){
			var htmlDivId = "detail_formField";
			var eno=$('#requestDetails_requestNo').val();
			$.post('request!findRequestById.action?requestQueryDTO.eno='+eno,function(data){
				$("#"+htmlDivId).hide();
				if(data.formId){
					var _param = {"formCustomId" : data.formId};
					$.post('formCustom!findFormCustomById.action',_param,function(res){
						var formCustomContents = common.config.formCustom.formControlImpl.editHtml(res.formCustomContents,htmlDivId);
						$("#"+htmlDivId).html(formCustomContents);
						itsm.request.requestDetailsFormCustom.loadDetailHtml(data,res.eavNo,"#"+htmlDivId);
						$("#"+htmlDivId+" .label .required_div").remove();
					});
				}else{
					$("#"+htmlDivId).load('common/config/formCustom/defaultField.jsp',function(){
						common.config.formCustom.formControlImpl.setFormCustomContentJson("#"+htmlDivId);
						 if(!data.isNewForm){
							 common.eav.attributes.findAttributeByEno_New(data.eno,'itsm.request',data.categoryEavId,'requestDTO',"detail",function(json){
								 if(json){
									var jsonStr=common.security.base64Util.encode(JSON.stringify(json));
									$("#"+htmlDivId).append(//'<div class="field_options"><div class="label"></div><div class="field"></div></div>'+
											common.config.formCustom.formControlImpl.detailHtml(jsonStr));
								 }
								 itsm.request.requestCommon.setfieldOptionsLobCss("#"+htmlDivId);
								 itsm.request.requestDetailsFormCustom.initBorder("#"+htmlDivId);
								 $("#"+htmlDivId+" :hidden[attrtypename='dataDictionaryDecode']").each(function(i,obj){
									$.post('dataDictionaryItems!findByDcode.action?groupNo='+$(obj).val(),function(dataItem){
										var str = "{DataDictionaray"+$(obj).val()+"}";
										var htmlStr = $("#"+htmlDivId).html();
										$("#"+htmlDivId).html(htmlStr.replace(str,dataItem.dname));
										//$("#assignInfo").before($("#"+htmlDivId).html());
										$(htmlDivId).append($("#assignInfo").html());
									});
								 });
							});
							 
						 }else{
							 itsm.request.requestDetailsFormCustom.initBorder("#"+htmlDivId);
						 }
						itsm.request.requestCommon.setRequestParamValueToOldFormByDetail("#"+htmlDivId,data);
						itsm.request.requestCommon.setRequestDataDictionarayValueByDetail("#"+htmlDivId,data);
						itsm.request.requestCommon.initDefaultForm(htmlDivId);
						$("#"+htmlDivId+" .label .required_div").remove();
						$("#"+htmlDivId).show();
					});
				}
				
			});
		},
		loadDetailHtml:function(data,eavNo,htmlDivId){
			itsm.request.requestCommon.setRequestFormValuesByDetail(data,htmlDivId,'itsm.request',eavNo,function(){
				itsm.request.requestDetailsFormCustom.initBorder(htmlDivId);
			});
		},
		/**
		 * 初始化边框
		 */
		initBorder:function(htmlDivId){
			setTimeout(function(){//延迟加载
				$(htmlDivId+' .label span').remove();
				$(htmlDivId).show();
				//$("#detail_formField > div[class=field_options] > div[class=label]:odd").css("border-left","none");
				var arrys = new Array();
				if($("#detail_formField > div[class='field_options']").size()==0){//判断是否是一列显示
					$("#detail_formField > div").css("border-left","1px solid #EEE");
					$(htmlDivId).prepend($('#detail_codeAndServices').html());
					$(htmlDivId).append($("#assignInfo").html());
					$("#detail_formField > div[class='field_options']").attr("class","field_options_2Column field_options").css("border-left","1px solid #EEE");
					mark=false;
				}else{
					$(htmlDivId).prepend($('#detail_codeAndServices').html());
					$(htmlDivId).append($("#assignInfo").html());
					var count_element = $("#detail_formField > div").size();
					$.each($("#detail_formField > div"),function(ind,val){
						if($(this).attr("class")==="field_options_lob field_options" || count_element === (ind+1)){
							if(arrys.length % 2 == 0)
								$(this).css("border-left","1px solid #EEE");
							for(var i = 0 ; i < arrys.length ; i ++){
								if((i+1) % 2 != 0)
									$(arrys[i]).css("border-left","1px solid #EEE");
							}
							arrys.splice(0);
						}else{
							arrys.push(this);
						}
			        });
				}
			},0);
		},
		/**
		 * 初始化详情解决方案
		 */
		loadRequestDetailSolution:function(){
			if(_requestDetail_loadsolution=="0"){
				$('#solution_tbody').html('');
				var _url='request!findSolutionByRequestId.action';
				var eno = $('#requestDetails_requestNo').val();
				var statusCode = $('#requestDetails_statusCode').val();
				var param = $.param({'requestQueryDTO.eno':eno},true);
				$.post(_url,param,function(res){
					 var str = '';
					 for(var i=0;i<res.length;i++){
						 var id = res[i].eventId;
							if(statusCode!='request_close'){
								str += '<tr id="solution_tr_'+id+'"><td>'+res[i].eventName+'</td><td>'+res[i].scores+'</td><td>'
								+'<a href=javascript:itsm.request.requestDetailsFormCustom.removeSolutionTr('+id+','+res[i].scores+')>'
								+'<img src="../images/icons/delete.gif" border="0" /></a>'
								+'</td></tr>';
							}else{
								str += '<tr id="solution_tr_'+id+'"><td>'+res[i].eventName+'</td><td>'+res[i].scores+'</td><td>'
								+'</td></tr>';
							}
							scores += parseInt(res[i].scores);
					 }
					 str +='<tr id="totalScores_tr"><td colspan="3" id="totalScores">'+i18n.totalScores+' : '+scores+'</td></tr>';
						$('#solution_tbody').html(str);
				});
				
				_requestDetail_loadsolution="1";
			}
		},
		/**
		 * 初始化
		 * @private
		 */
		init:function(){
			if(isAllowAccessRes){
				$("#requestDetail_loading").hide();
				$("#requestDetails_layout").show();
				if($('#requestDetails_requestNo').val()!=null&&$('#requestDetails_requestNo').val()!==""){
					$("#testrequest").bind("click",function(){
						$.post("request!requestTest.action",function(){
						});
					});
					//itsm.app.autocomplete.autocomplete.bindAutoComplete('#appro_User_Name','com.wstuo.common.security.entity.User','fullName','fullName','userId','Long','#requestDetail_assigneeNo_Appro',$("#requestDetails_companyNo").val(),'false');
					//$('#appro_User_Name').click(function(){common.security.userUtil.selectUser('#appro_User_Name','#requestDetail_assigneeNo_Appro','','fullName',topCompanyNo);});
					itsm.request.requestDetailsFormCustom.updateserviceDirStr();
					common.tools.includes.includes.loadEventTaskIncludesFile();
					itsm.request.includes.includes.loadRequestActionIncludesFile();
					common.security.includes.includes.loadSelectCustomerIncludesFile();
					common.config.includes.includes.loadCategoryIncludesFile();
					common.config.includes.includes.loadCustomFilterIncludesFile();
					//绑定页面加载事件
					itsm.request.requestDetailsFormCustom.tabClick();
					$('#saveToKnowledgeBtn').click(itsm.request.requestDetailsFormCustom.saveSlutionsBoth);
					$('#selectExistSolutionsBtn').click(itsm.request.requestDetailsFormCustom.selectExistSolution);
					$('#saveSolutionsBtn').click(itsm.request.requestDetailsFormCustom.saveSlutionsOnly);

					setTimeout(itsm.request.requestDetailsFormCustom.fitRequestDetailsGrids,0);

					$('#searchSimilarRequest_btn').click(function(){//查看类似请求
						common.compass.fullSearch.openFullsearchWindow($('#requestDetails_requestTitle').val(),'request');			
					});

					$('#request_email_reply').click(itsm.request.requestDetailsFormCustom.rquestActionEmailReply_win);
					$('#rquestActionEmailReply_send').click(itsm.request.requestDetailsFormCustom.rquestActionEmailReply);
					
					$('#requestDetails_edit_but').click(itsm.request.requestDetailsFormCustom.requestDetail_edit);//请求编辑

					$('#requestDetailsToPrint').click(itsm.request.requestDetailsFormCustom.requestToPrint);//工单打印

					bindControl('#requestActionHtml_responseTime,#requestActionHtml_completeTime');


					//选择任务负责人
					$('#request_taskOwnerName').click(function(){
						common.security.userUtil.selectUser('#request_taskOwnerName','#request_taskOwner','','',topCompanyNo);
					});
					//选择成本及进展技术员
					$('#request_costUserName').click(function(){
						common.security.userUtil.selectUser('#request_costUserName','#request_costUserId','#request_perHourFees','',topCompanyNo);
					});

					//提交问题
					$('#request_detail_2problem').click(itsm.request.requestDetailsFormCustom.requestInfoToProblem);
					//提交变更
					$('#submitChangeBtn_info').click(itsm.request.requestDetailsFormCustom.requestInfoToChange);
					
					$('#requestDeal_noticeEmail_input,#requestGet_noticeEmail_input,#requestAgainAssign_noticeEmail_input,#requestBack_noticeEmail_input,'+
							'#requestUpgradeApply_noticeEmail_input,#requestUpgrade_noticeEmail_input,#requestDealComplete_noticeEmail_input,'+
							'#requestReOpen_noticeEmail_input,#requestBackGroup_noticeEmail_input,#requestAssign_noticeEmail_input,#requestClose_noticeEmail_input,'+
							'#requestApproval_noticeEmail_input,#requestHang_noticeEmail_input,#requestHangRemove_noticeEmail_input,#notComprehensiveNotSubmitted_noticeEmail_input').val($('#rquestDetailCreatedByEmail').val());
					
					$('#requestAssignTwo_win_noticeEmail_input,#requestAssignThree_win_noticeEmail_input,#requestAssignFour_win_noticeEmail_input').val($('#rquestDetailCreatedByEmail').val());
					$('#requestDetail_assigneeGroupNo_two').val($('#requestDetails_assigneeGroupNo').val());
					$('#requestDetail_assigneeGroupName_two').val($('#requestDetails_assigneeGroupName').val());
					$('#requestDetail_assigneeNo_two').val($('#requestDetails_assigneeNo').val());
					$('#requestDetail_assigneeName_two').val($('#requestDetails_assigneeName').val());
					
					$('#requestDetail_assigneeGroupNo_three').val($('#requestDetails_assigneeGroupNo').val());
					$('#requestDetail_assigneeGroupName_three').val($('#requestDetails_assigneeGroupName').val());
					$('#requestDetail_assigneeNo_three').val($('#requestDetails_assigneeNo').val());
					$('#requestDetail_assigneeName_three').val($('#requestDetails_assigneeName').val());
					
					$('#requestDetail_assigneeGroupNo_four').val($('#requestDetails_assigneeGroupNo').val());
					$('#requestDetail_assigneeGroupName_four').val($('#requestDetails_assigneeGroupName').val());
					$('#requestDetail_assigneeNo_four').val($('#requestDetails_assigneeNo').val());
					$('#requestDetail_assigneeName_four').val($('#requestDetails_assigneeName').val());
					$('#requestActionHtml_responseTime_old,#requestActionHtml_responseTime').val($('#maxResponsesTimeTd').text());
					$('#requestActionHtml_completeTime_old,#requestActionHtml_completeTime').val($('#maxCompletesTimeTd').text());
					
					
					common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('offmode','#requestDetails_offmode');
					setTimeout(function(){
						//initFileUpload("_Request",$('#requestDetails_requestNo').val(),"itsm.request",userName,"show_request_effectAttachment","request_effect_attachmentStr");
						//getUploader('上传文件文本ID','上传后返回的信息字符串','显示上传成功的附件','');
						getUploader('#request_effect_file','#request_effect_attachmentStr','#request_effect_success_attachment','request_effect_fileQueue', function(){
						    common.tools.event.eventAttachment.saveEventAttachment($('#attachmentCreator').val(),'show_request_effectAttachment',$('#requestDetails_requestNo').val(),'itsm.request','request_effect_attachmentStr',true);
						});
						if(request_detail_solutions_res){
							initCkeditor('request_detail_solutions','Simple',function(){});
						}
						if(normal_action){
							document.getElementById('normal_action_start').style.display='';
						}
					},0);
					common.tools.event.eventAttachment.showEventAttachment('show_request_effectAttachment',$('#requestDetails_requestNo').val(),'itsm.request',true);
					
					$('#requestVisit_createdByName_input').val($('#requestDetails_createdByName').val());
					$('#requestVisit_createdByName_span').text($('#requestDetails_createdByName').val());
					
					$('#requestDetailsTab').tabs({
						scrollIncrement:230,
						scrollDuration:10
					});
					if($('#requestDetail_visitRecord').val()!=null)
						itsm.request.requestDetailsFormCustom.showReturnVisitShowHide();//是否已回访成功
		        	
					itsm.request.requestDetailsFormCustom.loadRequestDetailSolution();
					$("#requestSLADetail").click(function(){
						itsm.request.requestDetailsFormCustom.showSLADetail();
					});
					itsm.request.requestDetailsFormCustom.initDetailForm();
					endProcess();
				}else{
					endProcess();
					msgAlert(i18n.requestIsDelete,'info');
					basics.tab.tabUtils.closeTab(i18n.request_detail);
				}
			}else{
				endProcess();
				msgAlert(i18n.error403,'info');
				basics.tab.tabUtils.closeTab(i18n.request_detail);
			}
		}
	}
}();
//载入
$(document).ready(itsm.request.requestDetailsFormCustom.init);