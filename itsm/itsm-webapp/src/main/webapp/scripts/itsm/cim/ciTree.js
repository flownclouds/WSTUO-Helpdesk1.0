/**  
 * @author yctan  
 * @constructor citree
 * @description 选择配置项窗口
 * @date 2010-11-17
 * @since version 1.0 
 */  

var  citree=function(){
	
	/**
	 * @description 配置项分类树结构
	 * @param {String} treeWin 树窗口
	 * @param {String} treeDiv 树显示
	 * @param {String} gridDiv 配置项列表
	 * @param {String} pagerDiv 分页
	 * @param {String} sid 选择传ID值ID
	 * @param {String} sname 选择传ID
	 * @param {String} toolbar 工具栏
	 * 
	 */
	this.ciCategoryTree=function(treeWin,treeDiv,gridDiv,pagerDiv,sid,sname,toolbar){
		windows(treeWin);
		$('#select_ci_maintab').tabs();
		
		$("#"+treeDiv).jstree({
			"json_data":{
			    ajax: {
			    	url : "ciCategory!getConfigurationCategoryTree.action",
		    		data:function(n){
				    	  return {'parentEventId':n.attr ? n.attr("cno").replace("node_",""):0};//types is in action
				    },
				    cache:false}
			},
			"plugins" : ["themes", "json_data", "ui", "crrm", "cookies", "types", "hotkeys"]
			})
			//.bind('loaded.jstree', function(e,data){data.inst.open_all(-1);})
			.bind('select_node.jstree',function(e, data){
				$("#"+gridDiv).jqGrid('setGridParam',{page:1,url:'ci!cItemsFind.action?ciQueryDTO.categoryNo='+data.rslt.obj.attr('cno')+'&ciQueryDTO.loginName='+userName}).trigger('reloadGrid');
			});
		    //设置树的icon样式
		if($('#'+gridDiv).html()==""){
			ciByCategory(0,gridDiv,pagerDiv,sid,sname,treeWin,toolbar);
		}
		$("#"+gridDiv).jqGrid('setGridParam',{page:1,url:'ci!cItemsFind.action?ciQueryDTO.categoryNo=0&ciQueryDTO.loginName='+userName}).trigger('reloadGrid');
	};
	
	/**
	 * @description 根据配置分类查询配置项
	 */
	this.ciByCategory=function(ciCategoryId,gridDiv,pagerDiv,sid,sname,treeWin,toolbar){
		$('#'+gridDiv).jqGrid({
			url:'ci!cItemsFind.action?ciQueryDTO.categoryNo='+ciCategoryId+'&ciQueryDTO.loginName='+userName,
			mtype:'post',
			datatype: "json",
			colNames:[i18n['number'],i18n['category'],i18n['code'],i18n['name'],i18n['check']],
			colModel:[
					  {name:'ciId',index:'id',width:20,sorttype:'int'},
					  {name:'categoryName',width:45},
					  {name:'cino',width:50},
					  {name:'ciname',width:80},
					  {name:'act', width:40,sortable:false,align:'center'}
					  ],
			viewrecords: true,
		    autowidth:true,
		    caption:"",
			jsonReader: {
				root: "data",
				records: "totalSize",
				page: "page",
				total: "total",
				userdata: "total", 
	            repeatitems: false,
	            id: "ciId"
	        },
			sortname:'ciId',
			sortorder:'asc',
			editurl:'#',
			height: 'auto',
			rowNum:8, 
			rowList:[8,10,15],
		    pager:'#'+pagerDiv,
			gridComplete: function(){
				var ids = jQuery('#'+gridDiv).jqGrid('getDataIDs');
				for(var i=0;i < ids.length;i++){
					edit ="<div style='padding:4px'>"
							+"<a onclick=select_ci('"+ids[i]+"','"+sid+"','"+sname+"','"+treeWin+"','"+gridDiv+"') >"	
							+"<img src='../skin/default/images/grid_edit.png'/></a></div>"		
					jQuery('#'+gridDiv).jqGrid('setRowData',ids[i],{act:edit});					
				}	
	        }
		});

		
		$("#"+gridDiv).navGrid('#'+pagerDiv,{edit:false,add:false,del:false,search:false,refresh:true});		
		
	};
	
	
	/**
	 * @description 确定选择CI
	 */
	this.select_ci=function(id,sid,snme,treeWin,gridDiv){
		var row=$("#"+gridDiv).getRowData(id);
		$('#'+sid).val(id);
		$('#'+snme).val(row.ciname);
		$('#'+treeWin).dialog('close');
	}
	
	
	
	/***
	 * @description 选择查询
	 */
	this.selectQuery=function(searchForm,updateGrid){
		var sdata=$('#'+searchForm+' form').getForm();
		var postData = $("#"+updateGrid).jqGrid("getGridParam", "postData");
		$.extend(postData,sdata);  //将postData中的查询参数覆盖为空值
		var _url = 'ci!cItemsFind.action?ciQueryDTO.loginName='+userName;		
		$('#'+updateGrid).jqGrid('setGridParam',{url:_url})
			.trigger('reloadGrid',[{"page":"1"}]).jqGrid('setGridParam',{url:_url});
		
		return false;
	}
	
}()