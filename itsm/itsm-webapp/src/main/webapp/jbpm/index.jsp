<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <title>自定义流程</title>
    <meta name="description" content="wstuo.com">
	<meta name="author" content="WSTUO">
	<!-- The fav icon -->
	<link rel="shortcut icon" href="../images/logo2.png">
      <link rel="stylesheet" type="text/css" href="scripts/ext-2.0.2/resources/css/ext-all.css" />
    
     <link rel="stylesheet" type="text/css" href="styles/jbpm4.css" />
    <link rel="stylesheet" type="text/css" href="styles/org.css" />
    
    <!-- 
	<script type="text/javascript" src="../hightcharts/highcharts.src.js"></script>
	<script type="text/javascript" src="../hightcharts/exporting.src.js"></script>
	 -->
	 <!-- ext js -->
	 <script type="text/javascript" src="scripts/ext-2.0.2/ext-base.js"></script>
    <script type="text/javascript" src="scripts/ext-2.0.2/ext-all.js"></script>
    <script type="text/javascript" src="scripts/ext-2.0.2/ext-lang-zh_CN.js"></script>
    
	<script type="text/javascript" src="scripts/gef/scripts/jquery-1.4.2.min.js"></script>
	<script type="text/javascript" src="scripts/gef/scripts/geom_entry.js"></script>
	<script type="text/javascript" src="scripts/gef/scripts/create_core.js"></script>
    <script type="text/javascript" src="scripts/gef/scripts/work_core.js"></script>
    <script type="text/javascript" src="scripts/gef/scripts/tools.js"></script>
    <script type="text/javascript" src="scripts/gef/scripts/model.js"></script>
    <!-- 网页中绘制矢量图形的 Javascript 库 -->
    <script type="text/javascript" src="scripts/gef/scripts/raphael-min.js"></script>
    
    <script type="text/javascript">
	Gef.IMAGE_ROOT = 'scripts/gef/images/activities/48/';
	Gef.SETATTR_URL = 'flowProperty!updateFlowActivity.action';
	Gef.DEPLOY_URL = 'jbpm!deployJpdlFileByDrawingPage.action';
	Gef.GETXML_URL = 'jbpm!getXmlByPid.action';
	Gef.PID = '${param.pid}';
	Gef.TYPE = '${param.type}';
	Gef.VERSION = '${param.version}';
	Gef.TASKNUM = 0;//task任务节点数量
	Gef.USERNAME = '${sessionScope.loginUserName}';
	Gef.COMPANYNO='${sessionScope.companyNo}';//当前用户所在公司No
    </script>
    <script type="text/javascript" src="scripts/gef/all-editor.js"></script>
    <script type="text/javascript" src="scripts/edit-workflow.js"></script>
    <script type="text/javascript" src="scripts/gef/scripts/layout.js"></script>
     
    
    <script type="text/javascript" src="scripts/validation/all-validation.js"></script>
    <script type="text/javascript" src="scripts/form/all-forms.js"></script>
    <script type='text/javascript' src='scripts/property/all-property.js'></script>
    
	<link rel="stylesheet" href="../scripts/jquery/jquery-ui-1.8.2.custom/css/cupertino/jquery-ui-1.8.2.custom.css" />
    <script src="../scripts/jquery/jquery.min.js"></script>
    <script src="../scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.core.js"></script>
	<script src="../scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.widget.js"></script>
	<script src="../scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.mouse.js"></script>
	<script src="../scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.position.js"></script>
	<script src="../scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.autocomplete.js"></script>
	<script src="../js/basics/autocomplete.js"></script>
  </head>
  <body>
    <link rel="stylesheet" type="text/css" href="scripts/loading/loading.css" />
    <div id="loading-mask"></div>
    <div id="loading">
        <div class="loading-indicator"><img src="scripts/loading/extanim32.gif" align="absmiddle"/>loading...</div>
    </div>
	<script>
	$("document").ready(function(){
		setTimeout(function() {
			//初始化修改的信息
			App.initEditor(Gef.PID);
	    }, 1000);
	
	});
	var doc = document;
	function exportImage(){
		svg = $("#__gef_jbs_center__").html();
		//可以考虑将画布上的内容另存为页面，进而在转化成图片
		alert("exportImage---"+svg);
		//alert($("#__gef_jbs_center__").html());
		$("#textIdContent").attr("value",svg);
		//http://localhost:8080/platform/exportImage.do
		
		 window.prompt("欢迎？",svg);
	}
	</script>
  </body>
</html>

