$package("itsm.request");
$import("wstuo.dictionary.dataDictionaryUtil");
$import("wstuo.category.eventCategoryTree");
$import("wstuo.user.userUtil");
if(isCIHave){
	$import("itsm.cim.configureItemUtil");
	$import('itsm.cim.ciCategoryTree');
}
$import('itsm.itsop.selectCompany');
$import('itsm.request.relatedRequestAndKnowledge');
$import('wstuo.knowledge.knowledgeTree');
$import('wstuo.category.serviceCatalog');
$import('wstuo.includes');
$import('wstuo.sysMge.base64Util');
$import('wstuo.customForm.formControlImpl');
$import('wstuo.tools.xssUtil');
$import('itsm.request.requestCommon');
$import('itsm.request.requestStats');
$import('basics.autocomplete');
$import("wstuo.tools.eventAttachment");
$import('wstuo.tools.chooseAttachment');
/**  
 * @author QXY  
 * @constructor requestEdit
 * @description 编辑请求函数
 * @since version 1.0 
 */
itsm.request.editRequest = function(){
	this.rowHTML='<tr id="ref_ci_CIID">'+
	'<td align="center">CINO</td>'+
	'<td align="center"><input type="hidden" name="cinos" value="CIID" />'+
	'<a href=javascript:itsm.request.editRequest.lookConfigureItemInfo(CIID)>CINAME</a>'+
	'</td>'+
	'<td align="center">CATEGORYNAME</td>'+
	'<td align="center">CISTATUS</td>'+
	'<td align="center"><a onclick=itsm.cim.configureItemUtil.removeRow("#ref_ci_CIID")>DELETE</a></td>'+
	'</tr>';
	var edit_request_attrJson="";
	return {
		/**
		 * @description 获取配置项信息
		 * @param ciId 配置项Id
		 */
		lookConfigureItemInfo:function(ciId){
			basics.index.initContent("ci!findByciId.action?ciEditId="+ciId);
		},
		setEditRequestParamValue:function(res){
			$('#editField_requestCode').val(res.requestCode);
			var edit_requestServiceDirs = "";
			if(res.serviceDirectory.length > 0 && res.serviceDirectory != null){
				for ( var i = 0; i < res.serviceDirectory.length; i++) {
					edit_requestServiceDirs += res.serviceDirectory[i].eventName + ",";
				}
				edit_requestServiceDirs = edit_requestServiceDirs.substring(0,edit_requestServiceDirs.length-1);
			}
			$('#editField_requestServiceDirs').val(edit_requestServiceDirs);
			//基本信息
			$('#requestEdit_pid').val(res.pid);
			if(res.pid!=null){
				$('#traceRequestEditBtn').attr('style','margin-right:15px;');
			}
			if(res.formId!=null){
				$('#requestEdit_formId').val(res.formId);
			}else{
				$('#requestEdit_formId').val(0);
			}
			$('#requestEdit_eno').val(res.eno);
			$('#requestEdit_status').val(res.statusNo);
			if(res.createdByPhone!=null)
				$('#RequestEdit_UserPhone').val(res.createdByPhone);
			else
				$('#RequestEdit_UserPhone').val('');
			//指派
			if(res.assigneeGroupName!=null)
				$('#requestEdit_assigneeGroupName').val(res.assigneeGroupName);
			else
				$('#requestEdit_assigneeGroupName').val('');
			if(res.assigneeGroupNo!=null)
				$('#requestEdit_assigneeGroupNo').val(res.assigneeGroupNo);
			else
				$('#requestEdit_assigneeGroupNo').val('');
				
			if(res.assigneeName!=null)
				$('#requestEdit_assigneeName').val(res.assigneeName);
			else
				$('#requestEdit_assigneeName').val('');
			if(res.assigneeNo!=null)
				$('#requestEdit_assigneeNo').val(res.assigneeNo);
			else
				$('#requestEdit_assigneeNo').val('');
			
			$("#editRequest_processKey").val(res.processKey);
			//关系配置项
/*			if(res.cigDTO!=null){
				var cigDTO=res.cigDTO;
				for(var i=0;i<cigDTO.length;i++){
					var status='';
					if(cigDTO[i].status!=null)
						status=cigDTO[i].status;
					var newRowHTML=rowHTML.replace(/CIID/g,cigDTO[i].ciId)
					.replace(/CINO/g,cigDTO[i].cino)
					.replace(/CIDETAIL_TITLE/g,i18n.ci_configureItemInfo)
					.replace(/CINAME/g,cigDTO[i].ciname)
					.replace(/CATEGORYNAME/g,cigDTO[i].categoryName)
					.replace(/CISTATUS/g,status)
					.replace(/DELETE/g,i18n.deletes); 	
					$(newRowHTML).appendTo('#edit_request_relatedCIShow table');
				}
			}*/
			//解决方案
			if(res.solutions!=null){
				$('#requestEditSolutions').val(res.solutions);
			}
			$('#requestEdit_requestCode,#requestEdit_requestCode_show').val(res.requestCode);
			
			//autocomplete.bindAutoComplete('#RequestEdit_UserName','com.wstuo.common.security.entity.User','fullName','fullName','userId','Long','#RequestEdit_UserId',companyNoReqEdit,'true');
		
		},
		/**
		 * 查询请求信息
		 */
		findRequestById:function(){
			var htmlDivId = "edit_formField";
			$('#'+htmlDivId).html("");
			var url = 'request!findRequestById.action?requestQueryDTO.eno='+eno;
			$.post(url, function(res){
				if(res.formId){
					var _param = {"formCustomId" : res.formId};
					$.post('formCustom!findFormCustomById.action',_param,function(data){
						var formCustomContents = wstuo.customForm.formControlImpl.editHtml(data.formCustomContents,htmlDivId);
						$('#'+htmlDivId).html(formCustomContents);
						$('#'+htmlDivId).find('.glyphicon-trash').remove();
						itsm.request.requestCommon.setRequestFormValues(res,'#'+htmlDivId,function(){
							initCkeditor('editrequest_edesc','Simple',function(){});
							wstuo.customForm.formControlImpl.formCustomInit('#'+htmlDivId);
							wstuo.customForm.formControlImpl.formCustomInitByDataDictionaray('#'+htmlDivId);
							endProcess();
						});
						itsm.request.requestCommon.bindAutoCompleteToRequest('edit_formField');
						itsm.request.editRequest.setEditRequestParamValue(res);
					});
				}else{
					itsm.request.requestCommon.setRequestParamValueToOldForm('#edit_basicInfo_field',res);
					itsm.request.requestCommon.bindAutoCompleteToRequest('edit_formField');
					initCkeditor('editrequest_edesc','Simple',function(){});
					itsm.request.editRequest.setEditRequestParamValue(res);
					endProcess();
				}
					
			});
		},
		
		/**
		 * @description 保存请求修改
		 * */
		saveRequestEdit:function(){
			var oEditor = CKEDITOR.instances.editrequest_edesc;
			var edesc=trim(oEditor.getData());
			edesc = edesc.replace(/\s*<\/p>\s*|\s*<\/p>\s*/, '').replace(/\s*<p>\s*|\s*<\/p>\s*/, '').replace(/&nbsp;/gi, ''); 
			var request_edesc=(edesc===''|| edesc=="null" || edesc=="NULL")?false:true;
			if(!request_edesc){
				msgAlert(i18n.titleAndContentCannotBeNull,'info');
				return false;
			}
			$.each($("#edit_formField :input[attrType=textarea]"),function(index,obj){
				var oEditorObj = CKEDITOR.instances[$(obj).attr("id")];
				$(obj).val(trim(oEditorObj.getData()));
			});
			$('#edit_basicInfo_field #editrequest_edesc').val(edesc);
			var rNo=$('#editRequestCategoryNoSub').val();
			if(rNo!=="" && rNo!==undefined){
				$('#editRequestCategoryNo').val(rNo);
			}
			itsm.request.requestCommon.getFormAttributesValue("#editRequest_from");
			$.each($("#edit_formField input[attrtype='text']"),function(ind,val){
				$(this).val(wstuo.tools.xssUtil.html_encode($(this).val()));
			});
			var frm = $('#editRequest_from').serialize();
			var url = 'request!updateRequest.action';
			startProcess();
			$.post(url,frm, function(){
				endProcess();
				//showRequestIndex();
				//itsm.request.requestStats.countAllRquest();
				msgShow(i18n.editSuccess,'show');
				basics.index.initContent("request!requestDetails.action?eno="+eno);
			});
		},
		/**
		 * @description 创建人信息
		 * */
		selectCreator_openWindow:function(){
			
			var _RequestEdit_UserId=$('#RequestEdit_UserId').val();
			wstuo.user.userUtil.selectUser('#RequestEdit_UserName','#RequestEdit_UserId','','fullName',$('#edit_request_companyNo').val(),function(){
				/*if(_RequestEdit_UserId!=$('#RequestEdit_UserId').val()){//如果更改了请求人，则清空相关联的配置项需要重新选择
					$('#edit_request_ref_ciname,#edit_request_ref_ciid').val('');
				}*/
			});
		},
		/**
		 * @description 删除请求附件
		 * @param eno 编号eno
		 * @param aid 附件Id
		 */
		deleteRequestAttachement:function(eno,aid){
			
			msgConfirm(i18n.msg_msg,'<br/>'+i18n.msg_confirmDelete,function(){
				var _url = "request!deleteRequestAttachement.action";
				$.post(_url,'eno='+eno+'&aid='+aid,function(){
					$('#show_edit_request_attachment #att_'+aid).remove();
					msgShow(i18n.deleteSuccess,'show');
				});
			});
		},
		/**
		 * 初始化
		 * @private
		 */
		init: function(){
			wstuo.includes.loadSelectCIIncludesFile();//加载选择配置项
			wstuo.includes.loadSelectUserIncludesFile();//加载用户选择
			//流程跟踪
			//$('#search_Edit_UserName').click(itsm.request.editRequest.selectCreator_openWindow);
			setTimeout(function(){
				//getUploader('上传文件文本ID','上传后返回的信息字符串','显示上传成功的附件','');
				getUploader('#edit_request_file','#edit_request_attachmentStr','#show_edit_request_attachment_success','editRequestQueId',function(){
				    wstuo.tools.eventAttachment.saveEventAttachment($('#attachmentCreator').val(),'show_edit_request_attachment',eno,'itsm.request','edit_request_attachmentStr',true);
				});
				//initFileUpload("_RequestEdit",eno,"itsm.request",userName,"show_edit_request_attachment","edit_request_attachmentStr");
			},0);
			wstuo.tools.eventAttachment.showEventAttachment('show_edit_request_attachment',eno,'itsm.request',true);
			setTimeout(itsm.request.editRequest.findRequestById,0); 
			if(editRequestUser=="0"){
				//设置请求用户不可编辑,并且移除图标
				$("#edit_basicInfo_field #request_userName").attr("disabled",true);
				$("#edit_basicInfo_field #request_userName").val(fullName);
				$("#edit_basicInfo_field #request_userId").val(userId);
			}
			if(belongsClient=="0"){
				//设置所属客户不可编辑,并且移除图标
				$("#edit_basicInfo_field #request_companyName").attr("disabled",true);
			}
			/*$('#edit_request_ref_ci').click(function(){//选择配置项
				itsm.cim.configureItemUtil.selectCIS('#edit_request_ref_ci','#requestEditCIId');
			});*/
			//服务目录
			/*$('#edit_request_service_edit').click(function(){
				common.config.category.serviceCatalog.selectServiceDir('#edit_request_serviceDirectory_tbody');
			});*/
			//绑定
			/*$('#edit_request_ref_ciname').click(function(){
				itsm.cim.configureItemUtil.findConfigureItemByPower('#edit_request_ref_ciname','#edit_request_ref_ciid',userName);
			});
			$('#Request_edit_ref_ci_btn').click(function(){
				if(userRoleCode =="ROLE_ENDUSER,"){//如果是终端用户，根据请求人去搜索，不需要分类权限控制； 
					itsm.cim.configureItemUtil.enduserSelectCISM('#edit_request_relatedCIShow',$("#edit_formField #request_companyNo").val());
				}else{
					itsm.cim.configureItemUtil.requestSelectCI('#edit_request_relatedCIShow',$("#edit_formField #request_companyNo").val(),'');
				}
			});
			
			$("#edit_request_ref_requestServiceDirName").click(function(){
				common.knowledge.knowledgeTree.selectKnowledgeServiceFilter('#knowledge_services_select_window','#knowledge_services_select_tree','#edit_request_ref_requestServiceDirName','#edit_request_ref_requestServiceDirNo');			
			});
			
			$('#editRequest_serviceDirName').click(function(){
				common.config.category.serviceCatalog.selectSingleServiceDir('#editRequest_serviceDirName','#editRequest_serviceDirIds');
			});*/
		}
	};
}();
//载入
$(document).ready(itsm.request.editRequest.init);