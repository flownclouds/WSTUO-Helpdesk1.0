$package('itsm.itsop');
//$import('wstuo.user.user')
 /**  
 * @fileOverview 选择所属客户主函数.
 * @author QXY
 * @version 1.0  
 */  
itsm.itsop.selectCompany=function(){
	
	this.orgNameId='';
	this.orgNoId='';
	this.cleanId='';
	this.itsopNamePut='';
	this.itsopValuePut='';
	return {
		/**
		 * 打开选择外包客户窗口
		 * @param namePut 保存选中外包客户名称的input控件id
		 * @param idPut 保存选中外包客户id的input控件id
		 */
		selectITSOP:function(namePut,idPut){
			itsopValuePut=idPut;
			itsopNamePut=namePut;
				
			windows('selectITSOP_Window',{width:500});
			itsm.itsop.selectCompany.showITSOPGrid(namePut,idPut);
		},
		/**
		 * 选择外包客户按钮格式化
		 */
		checkSelectITSOPFormatter:function(cell,event,data){
			
			return '<div style="padding:0px">'+
			'&nbsp;&nbsp;<a href="javascript:itsm.itsop.selectCompany.checkSelectITSOP(\''
			+(data.orgName)+'\',\''+(data.orgNo)+'\')" title="'+i18n['check']+'">'+
			'<img src="../images/icons/ok.png"/></a>'+
			'</div>';
			
		},
		/**
		 * 选择外包客户
		 * @param orgName 外包客户名称
		 * @param orgNo 外包客户id
		 */
		checkSelectITSOP:function(orgName,orgNo){
			
			$(itsopNamePut).val(orgName);
			$(itsopValuePut).val(orgNo);
			$('#selectITSOP_Window').dialog('close');
			
		},
		/**
		 * 加载外包客户列表
		 * @param namePut 保存选中外包客户名称的input控件id
		 * @param idPut 保存选中外包客户id的input控件id
		 */
		showITSOPGrid:function(namePut,idPut){
			var params = $.extend({},jqGridParams, {
				url:'itsopUser!findITSOPUserPager.action',
				colNames:[i18n['common_name'],i18n['common_phone'],i18n['common_email'],i18n['common_action'],i18n['common_address'],'',''],
				colModel:[
						  {name:'orgName',width:30,align:'center',sortable:false},
						  {name:'officePhone',width:25,align:'center',sortable:false},
						  {name:'email',width:25,align:'center',sortable:false},
						  {name:'action',width:20,align:'center',sortable:false,formatter:itsm.itsop.selectCompany.checkSelectITSOPFormatter},
						  
						  {name:'address',hidden:true},
						  {name:'orgNo',hidden:true,sortable:false},
						  {name:'orgType',hidden:true}
						  ],
				jsonReader: $.extend(jqGridJsonReader,{id:"orgNo"}),
				sortname:'orgNo',
				pager:'#selectCompany_gridPager',
				multiselect:false,
				ondblClickRow:function(rowId){
					var data=$('#selectITSOPGrid').getRowData(rowId);
					$(namePut).val(data.orgName);
					$(idPut).val(data.orgNo);
					$('#selectITSOP_Window').dialog('close');
				}
				
			});
			$("#selectITSOPGrid").jqGrid(params);
		},
		/**
		 * 打开公司选择窗口
		 * @param input_orgNoId 显示公司ID的ID
		 * @param input_orgNameId 显示公司名称的ID
		 * @param _cleanId 要清空内容的ID
		 */
		openSelectCompanyWin:function(input_orgNoId,input_orgNameId,_cleanId){
			orgNameId=input_orgNameId;
			orgNoId=input_orgNoId;
			if(_cleanId!=null || _cleanId!=undefined || _cleanId!=''){
				cleanId=_cleanId;
			}else{
				cleanId='';
			}
			itsm.itsop.selectCompany.loadCompanyGrid();
			windowsAuto('selectCompanyWin',{width:550},'#selectCompany_grid',5);
		},
		
		/**
		 * 打开公司选择窗口 多选
		 * @param input_orgNoId 显示公司ID的ID
		 * @param input_orgNameId 显示公司名称的ID
		 * @param _cleanId 要清空内容的ID
		 */
		openSelectCompanyWinCheckbox:function(input_orgNoId,input_orgNameId,_cleanId,name){
			orgNameId=input_orgNameId;
			orgNoId=input_orgNoId;
			if(_cleanId!=null || _cleanId!=undefined || _cleanId!=''){
				cleanId=_cleanId;
			}else{
				cleanId='';
			}
			windows('selectCompanyDiv',{width:550});
			itsm.itsop.selectCompany.loadCompanyGridCheckbox();
			
		},
		
		/**
		 * 选定列格式化
		 */
		companyGridFormatter:function(cell,event,data){
			return '<div style="padding:0px">'+
			'&nbsp;&nbsp;<a href="javascript:itsm.itsop.selectCompany.confirmCheck(\''
			+(data.orgName)+'\',\''+(data.orgNo)+'\',\''+(data.orgType)+'\')" title="'+i18n['check']+'">'+
			'<i class="glyphicon glyphicon-ok"></i></a>'+
			'</div>';
			
			
		},
		/**
		 * 加载外包客户列表
		 */
		loadCompanyGrid:function(){
			if(basics.ie6.htmlIsNull("#selectCompany_grid")){
				$("#selectCompany_grid").trigger("reloadGrid"); 
			}else{	
				var _postData={'itsopUserQueryDTO.loginName':'0'};
				if(userName!=null){
					$.extend(_postData,{'itsopUserQueryDTO.loginName':userName});
				}
				var params = $.extend({},jqGridParams, {
					url:'itsopUser!findMySupportITSOPUserPager.action',
					caption:'',
					postData:_postData,
					colNames:[i18n['common_name'],i18n['common_phone'],i18n['common_email'],i18n['common_address'],'','',i18n['check']],
					colModel:[
							  {name:'orgName',width:20,align:'center'},
							  {name:'officePhone',width:20,align:'center'},
							  {name:'email',width:20,align:'center'},
							  {name:'address',width:20,align:'center'},
							  {name:'orgNo',hidden:true,sortable:false},
							  {name:'orgType',hidden:true},
							  {name:'act',sortable:false,align:'center',width:20,formatter:itsm.itsop.selectCompany.companyGridFormatter}
							  ],
					jsonReader: $.extend(jqGridJsonReader,{id:"orgNo"}),
					sortname:'orgNo',
					multiselect:false,
					pager:'#selectCompany_gridPager',
					ondblClickRow:function(rowId){
						var data=$('#selectCompany_grid').getRowData(rowId);
						itsm.itsop.selectCompany.confirmCheck(data.orgName,data.orgNo,data.orgType);
					}
					
				});
				$("#selectCompany_grid").jqGrid(params);
				$("#selectCompany_grid").navGrid('#selectCompany_gridPager',navGridParams);
				//列表操作项
				$("#t_selectCompany_grid").css(jqGridTopStyles);
				$("#t_selectCompany_grid").append($('#selectMyCompanyToolbar').html());
			}
		},
		
		/**
		 * 加载外包客户列表  带有复选框
		 */
		loadCompanyGridCheckbox:function(){
			if(basics.ie6.htmlIsNull("#selectCompanySLA_grid")){
				$("#selectCompanySLA_grid").trigger("reloadGrid"); 
			}else{	
				var _postData={'itsopUserQueryDTO.loginName':'0'};
				if(userName!=null){
					$.extend(_postData,{'itsopUserQueryDTO.loginName':userName});
				}
				var params = $.extend({},jqGridParams, {
					url:'itsopUser!findMySupportITSOPUserPager.action',
					caption:'',
					postData:_postData,
					colNames:[i18n['common_name'],i18n['common_phone'],i18n['common_email'],i18n['common_address'],'',''],
					colModel:[
							  {name:'orgName',width:20,align:'center',sortable:false},
							  {name:'officePhone',width:20,align:'center',sortable:false},
							  {name:'email',width:20,align:'center',sortable:false},
							  {name:'address',width:20,align:'center',sortable:false},
							  {name:'orgNo',hidden:true,sortable:false},
							  {name:'orgType',hidden:true},
							 // {name:'act',sortable:false,align:'center',width:20,formatter:itsm.itsop.selectCompany.companyGridFormatter}
							  ],
					jsonReader: $.extend(jqGridJsonReader,{id:"orgNo"}),
					sortname:'orgNo',
					multiselect:true,
					pager:'#selectCompany_gridPagerDiv',
					toolbar:false,
					ondblClickRow:function(rowId){
						var data=$('#selectCompanySLA_grid').getRowData(rowId);
						itsm.itsop.selectCompany.confirmCheck(data.orgName,data.orgNo,data.orgType);
					}
					
				});
				$("#selectCompanySLA_grid").jqGrid(params);
				
				$("#selectCompanySLA_grid").navGrid('#selectCompany_gridPagerDiv',navGridParams);
				$("#t_selectCompany_grid").css(jqGridTopStyles);
				$("#t_selectCompany_grid").append($('#CompanyGridToolbar').html());
			}
		},
		
		/**
		 * 选定
		 * @param orgName 客户名称
		 * @param orgNo 客户id
		 * @param orgType 客户类型
		 */
		confirmCheck:function(orgName,orgNo,orgType){
			//清空
			if(cleanId!=''){
				if($(orgNoId).val()!=orgNo){
					$(cleanId).val('');
					$(cleanId).html('');
				}
			}
			$(orgNameId).val(orgName);
			$(orgNoId).val(orgNo);
			$(orgNameId+'_OrgType').val(orgType);
			$('#selectCompanyWin').dialog('close');
			
		},searchcompanyByName:function(){
			var roleData ={'itsopUserQueryDTO.orgName':$("#selectCompanyWin #selectCompanyNameByJqgrid").val()};
			var postData = $("#selectCompany_grid").jqGrid("getGridParam", "postData");
			$.extend(postData,roleData);  //将postData中的查询参数覆盖为空值
			$('#selectCompany_grid').trigger('reloadGrid',[{"page":"1"}]);
		}
		,
		
		/**
		 * 打开选择公司的面板，带回调函数方法
		 * @param input_orgNoId 显示公司ID的ID
		 * @param input_orgNameId 显示公司名称的ID
		 * @param _cleanId 要清空内容的ID
		 * @param name name=all,查询全部
		 * @param  method 回调函数
		 */
		openSelectCompanyWin_method:function(input_orgNoId,input_orgNameId,_cleanId,name,method){
			orgNameId=input_orgNameId;
			orgNoId=input_orgNoId;
			if(_cleanId!=null || _cleanId!=undefined || _cleanId!=''){
				cleanId=_cleanId;
			}else{
				cleanId='';
			}
			itsm.itsop.selectCompany.loadCompanyGrid_method(method);
			windows('selectCompanyWin',{width:500});
		},
		/**
		 * 加载外包客户列表
		 * @param  method 回调函数
		 */
		loadCompanyGrid_method:function(method){
			if(basics.ie6.htmlIsNull("#selectCompany_grid")){
				$("#selectCompany_grid").trigger("reloadGrid"); 
			}else{	
				var _postData={'itsopUserQueryDTO.loginName':'0'};
				if(userName!=null){
					$.extend(_postData,{'itsopUserQueryDTO.loginName':userName});
				}
				var params = $.extend({},jqGridParams, {
					url:'itsopUser!findMySupportITSOPUserPager.action',
					caption:'',
					postData:_postData,
					colNames:[i18n['common_name'],i18n['common_phone'],i18n['common_email'],i18n['common_address'],'',''],
					colModel:[
							  {name:'orgName',width:20,align:'center',sortable:false},
							  {name:'officePhone',width:20,align:'center',sortable:false},
							  {name:'email',width:20,align:'center',sortable:false},
							  {name:'address',width:20,align:'center',sortable:false},
							  {name:'orgNo',hidden:true,sortable:false},
							  {name:'orgType',hidden:true}
							  ],
					jsonReader: $.extend(jqGridJsonReader,{id:"orgNo"}),
					sortname:'orgNo',
					pager:'#selectCompany_gridPager',
					multiselect:false,
					ondblClickRow:function(rowId){
						var data=$('#selectCompany_grid').getRowData(rowId);
						itsm.itsop.selectCompany.confirmCheck_method(data.orgName,data.orgNo,data.orgType,method);
					}
					
				});
				$("#selectCompany_grid").jqGrid(params);
				$("#selectCompany_grid").navGrid('#selectCompany_gridPager',navGridParams);
				
				$("#t_selectCompany_grid").css(jqGridTopStyles);
				$("#t_selectCompany_grid").append($('#selectMyCompanyToolbar_AD').html());
			}
		},
		/**
		 * 选定
		 * @param orgName 客户名称
		 * @param orgNo 客户id
		 * @param orgType 客户类型
		 * @param  method 回调函数
		 */
		confirmCheck_method:function(orgName,orgNo,orgType,method){
			//清空
			if(cleanId!=''){
				if($(orgNoId).val()!=orgNo){
					$(cleanId).val('');
					$(cleanId).html('');
				}
			}
			$(orgNameId).val(orgName);
			$(orgNoId).val(orgNo);
			$(orgNameId+'_OrgType').val(orgType);
			method(orgType);
			$('#selectCompanyWin').dialog('close');
			
		},

		selectMyCompanyConfirm:function(){
			$(orgNoId).val(companyNo);
			$(orgNameId).val(companyName);
//			$(input_userId).val(userId);
//			$(input_fullName).val(fullName);
			$('#selectCompanyWin').dialog('close');
		},
		selectMyCompanyConfirm_AD:function(){
			$(orgNoId).val(companyNo);
			$(orgNameId).val(companyName);
			//wstuo.user.user.add_loadRole(-1,"roleSet",false);
			$('#selectCompanyWin').dialog('close');
		}
	}
}();