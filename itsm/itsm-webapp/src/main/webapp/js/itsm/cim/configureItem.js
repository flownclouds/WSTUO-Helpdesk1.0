$package("itsm.cim"); 
$import('itsm.cim.ciCategoryTree');
$import("common.security.userUtil");
$import('itsm.itsop.selectCompany');
$import('common.config.dictionary.dataDictionaryUtil');
$import('itsm.cim.leftMenu');
$import('common.security.acl');
$import('basics.bottomMain');
$import('basics.showChart');
/**  
 * @fileOverview 配置项列表主函数
 * @author yctan
 * @version 1.0  
 */  
 /**  
 * @author yctan  
 * @constructor 配置项列表主函数
 * @description 配置项列表主函数
 * @date 2010-11-17
 * @since version 1.0 
 */  
itsm.cim.configureItem=function(){
	this.ci_search_selectData_flag=true;
	var _categoryName=""; 
	var options={};
	if(isITSOPUser){		
		$.extend(options,{'companyNo_companyNo': 'label_belongs_client'});
	}
	this.importWmiDataLoadFlag=true;
	
	$.extend(options,{
		//变量名_类型:字段名(过滤器)
		'cino_String': 'lable_ci_assetNo',
		'ciname_String':'title_asset_name',
		'category.cno_CIeventId' :'category',
		'department_orgNo':'ci_department',
		//'model_String':'productType',
		//'barcode_String':'barcode',
		//'serialNumber_String':'serialNumber',
		//'poNo_String':'purchaseNo',
		//'sourceUnits_String':'ci_productProvider',
		//'usePermissions_String':'ci_usePermission',
		//'workNumber_String':'label_ci_businessEntity',
		//'project_String':'label_ci_project',
		//'CDI_String':'remark',
		//'financeCorrespond_Boolean':'ci_FIMapping',
		'useStatus.dcode_dcode':'status',
		//'brand.dcode_dcode' : 'brands',
		//'supplier.dcode_dcode':'supplier',
		'location.eventName_eventName':'location',
		'buyDate_Data' : 'purchaseDate',
		//'wasteTime_Data' : 'ci_expiryDate',
		//'borrowedTime_Data' : 'ci_lendTime',
		//'recoverTime_Data' : 'ci_recycleTime',
		//'expectedRecoverTime_Data' : 'ci_plannedRecycleTime',
		'arrivalDate_Data' : 'arrivalDate',
		'warningDate_Data' : 'warningDate',
		'assetsOriginalValue_Double' : 'ci_assetOriginalValue',
		'lifeCycle_Int' : 'lifeCycle',
		'warranty_Int' : 'warrantyDate',
		'userName_loginName' : 'use',
		'owner_loginName' : 'common_owner',
		//'originalUser_loginName' : 'ci_originalUser',
		'createTime_Data':'common_createTime',
		'lastUpdateTime_Data':'common_updateTime',
		//'systemPlatform.dcode_dcode' : 'label_systemPlatform'
	});
	return{
		/**
		 * @description 配置项列表名称格式化
		 **/
		ciNameUrlFrm:function(cellvalue, options, rowObject){
			return '<a href="JavaScript:itsm.cim.configureItem.configureItemInfo('+rowObject.ciId+')">'+cellvalue+'</a>'
		},
		ciNoUrlFrm:function(cellvalue, options, rowObject){
			return rowObject.cino;
		},
		/**
		 * @description 国际化
		 **/
		fimappingFrm:function(cellvalue, options){
			if(cellvalue==true){
				return i18n['label_basicConfig_deafultCurrencyYes'];
			}
			else{
				return i18n['label_basicConfig_deafultCurrencyNo'];
			}
		},
		/**
		 * @description 构造配置项列表列
		 * @param url 加载配置项的路径
		 **/	
		createList:function(url){
			//根据扩展属性构建列
			if(_categoryNo==undefined && _categoryNo==''){
				_categoryNo="";
			};
			var colNames=['ID',
			              i18n['label_belongs_client'],
			              i18n['CICategory'],
			              i18n['lable_ci_assetNo'],
			              i18n['title_asset_name'],
			              //'IP',
			              //i18n['label_systemPlatform'],
			              //i18n['productType'],
			              i18n['status'],
			              i18n['location'],
			              i18n['use'],
			              //i18n['brands'],
			              //i18n['label_ci_businessEntity'],
			             // i18n['label_ci_project'],
			             // i18n['remark'],
			             // i18n['ci_productProvider'],
			              i18n.common_createTime,
			              i18n.common_updateTime,
			              i18n.ci_depreciationIsZeroYears
			];
			var colModel=[
						  {name:'ciId',width:55,align:'center'},
						  {name:'companyName',index:'companyNo',width:80,sortable:true,hidden:hideCompany},
						  {name:'categoryName',width:100,index:'category',align:'center'},
						  {name:'cino',width:100,formatter:itsm.cim.configureItem.ciNoUrlFrm},
						  {name:'ciname',width:140,formatter:itsm.cim.configureItem.ciNameUrlFrm},
						  //{name:'ipAddress',width:140,sortable:false},
						  /*{name:'systemPlatform',width:100,formatter:function(cellvalue, options, rowObject){
	  							 return colorFormatter(rowObject.systemPlatformColor,cellvalue);
						   }},
						  {name:'model',width:80},*/
						  {name:'status',width:65,align:'center',formatter:function(cellvalue, options, rowObject){
	  							 return colorFormatter(rowObject.statusColor,cellvalue);
						   }},
						  {name:'loc',width:80,align:'center',formatter:function(cellvalue, options, rowObject){
	  							 return colorFormatter(rowObject.locColor,cellvalue);
						   }},
						   {name:'userName',width:65,align:'center'},
						  /*{name:'brandName',index:'brand',width:65,hidden:true,formatter:function(cellvalue, options, rowObject){
	  							 return colorFormatter(rowObject.brandNameColor,cellvalue);
						   }},
						  {name:'workNumber',width:65},
						  {name:'project',width:65},
						  {name:'CDI',width:65},
						  {name:'sourceUnits',width:65},*/
						  
						   {name:'createTime'},
						   {name:'lastUpdateTime'},
						   {name:'depreciationIsZeroYears',width:65,formatter:function(cellvalue, options, rowObject){
	  							 return rowObject.depreciationIsZeroYears+i18n.calendar_years;
						   }}
						  ];
			
			//拥有者
			if(owner_res){
				colNames.push(i18n['common_owner']);
				colModel.push({name:'owner',width:65,hidden:true,align:'center'});
			}
			//部门
			if(department_res){
				colNames.push(i18n['ci_department']);
				colModel.push({name:'department',width:65,hidden:true});
			}
			
			//资源原值
			if(assetsOriginalValue_res){
				colNames.push(i18n['ci_assetOriginalValue']);
				colModel.push({name:'assetsOriginalValue',width:50,align:'center',hidden:true});
			}
			/*//条列码
			if(barcode_res){
				colNames.push(i18n['barcode']);
				colModel.push({name:'barcode',width:60,align:'center',hidden:true});
			}
			//供应商
			if(supplier_res){
				colNames.push(i18n['supplier']);
				colModel.push({name:'providerName',index:'provider',width:60,align:'center',hidden:true,formatter:function(cellvalue, options, rowObject){
						 return colorFormatter(rowObject.providerNameColor,cellvalue);
				   }});
			}*/
			//购买日期
			if(purchaseDate_res){
				colNames.push(i18n['purchaseDate']);
				colModel.push({name:'buyDate',width:65,formatter:timeFormatterOnlyData,hidden:true});
			}
			//到货日期
			if(arrivalDate_res){
				colNames.push(i18n['arrivalDate']);
				colModel.push({name:'arrivalDate',width:65,formatter:timeFormatterOnlyData,hidden:true});
			}
			//预警日期
			if(warningDate_res){
				colNames.push(i18n['warningDate']);
				colModel.push({name:'warningDate',width:65,formatter:timeFormatterOnlyData,hidden:true});
			}
			//生命周期
			if(lifeCycle_res){
				colNames.push(i18n['lifeCycle']);
				colModel.push({name:'lifeCycle',width:65,hidden:true});
			}
			//保修期
			if(warrantyDate_res){
				colNames.push(i18n['warrantyDate']);
				colModel.push({name:'warranty',width:65,hidden:true});
			}
			//采购单号
			/*if(purchaseNo_res){
				colNames.push(i18n['purchaseNo']);
				colModel.push({name:'poNo',width:65,hidden:true});
			}*/
			
			
			
			//序列号
			/*if(serialNumber_res){
				colNames.push(i18n['serialNumber']);
				colModel.push({name:'serialNumber',width:80,hidden:true});
			}*/
			
			//与财务对应
			/*if(fimapping_res){
				colNames.push(i18n['ci_FIMapping']);
				colModel.push({name:'financeCorrespond',width:65,formatter:itsm.cim.configureItem.fimappingFrm,hidden:true});
			}*/
			//报废时间
			/*if(expiryDate_res){
				colNames.push(i18n['ci_expiryDate']);
				colModel.push({name:'wasteTime',width:65,formatter:timeFormatterOnlyData,hidden:true});
			}
			//借出时间
			if(lendTime_res){
				colNames.push(i18n['ci_lendTime']);
				colModel.push({name:'borrowedTime',width:65,formatter:timeFormatterOnlyData,hidden:true});
			}
			//原使用者
			if(originalUser_res){
				colNames.push(i18n['ci_originalUser']);
				colModel.push({name:'originalUser',width:65,hidden:true});
			}
			//回收时间
			if(recycleTime_res){
				colNames.push(i18n['ci_recycleTime']);
				colModel.push({name:'recoverTime',width:65,formatter:timeFormatterOnlyData,hidden:true});
			}
			//预计回收时间
			if(plannedRecycleTime_res){
				colNames.push(i18n['ci_plannedRecycleTime']);
				colModel.push({name:'expectedRecoverTime',width:65,formatter:timeFormatterOnlyData,hidden:true});
			}
			//使用权限
			if(usePermission_res){
				colNames.push(i18n['ci_usePermission']);
				colModel.push({name:'usePermissions',width:65,hidden:true});
			}*/
			
			$.post('ci!findAttributesByCategoryNo.action','categoryNo='+_categoryNo,function(data){//根据不同分类加载不同的扩展属性列
				if(data!=null && data!='[]' ){
					for(var i=0;i<data.length;i++){
						colNames.push(data[i].attrAsName);
						var cname = 'attrVals.'+data[i].attrName;
						colModel.push({name: cname,width:80,sortable:false,hidden:true});
						if(data.length==i+1){
							itsm.cim.configureItem.loadConfigureItemGrid(colNames,colModel,url);
						}
					}
				}else{
					itsm.cim.configureItem.loadConfigureItemGrid(colNames,colModel,url);
				}
			});
		},	
		/**
		 * 加载配置项列表
		 * @param colNames Grid列的名称
		 * @param colModel Grid列的值
		 * @param url 后台查询配置项的路径
		 */
		loadConfigureItemGrid:function(colNames,colModel,url){
			var _postData={};
			colNames.push(i18n['operateItems']);
			colModel.push({name:'act',width:100,sortable:false,align:'center',formatter:function(cell,event,data){
          	   return $('#configureItemFrmBarDiv').html().replace(/{ciId}/g,data.ciId);
            }});
			
			if(filterId!=null && filterId!='' && filterId!=-1){
				url='ci!findCItemsByCustomFilter.action?ciQueryDTO.filterId='+filterId;
			}
			if(countQueryType=='dynamicSearch'){//报表可链接数据传递
				url='ci!findConfigureItemsByCrosstabCell.action';
				$.extend(_postData,{'ktd.rowValue':rowValue,'ktd.colValue':colValue,'ktd.rowKey':rowKey,'ktd.colKey':colKey,'ktd.customFilterNo':customFilterNo});
			}
			var params = $.extend({},jqGridParamsTen,{
				url:url,
				postData:_postData,
				colNames:colNames,
				colModel:colModel,
				ondblClickRow:function(rowId){itsm.cim.configureItem.configureItemInfo(rowId)},
				jsonReader: $.extend(jqGridJsonReader, {id: "ciId"}),
				sortname:'ciId',
				shrinkToFit:false,
				height:'100%',
				pager:'#ciPager'
			});
			$("#configureGrid").jqGrid(params);
			$("#configureGrid").navGrid('#ciPager',navGridParams);
			//列表操作项
			$("#t_configureGrid").css(jqGridTopStyles);
			$("#t_configureGrid").append($('#CIGridToolbar').html());
			//自定义列
			defaultLoadColumn("#configureGrid",_categoryNo);
			$("#configureGrid").jqGrid('navButtonAdd','#ciPager',{
			    caption:"",
			    title:i18n['label_set_column'],
			    onClickButton : function (){
			    	loadColumnChooserItem('configureGrid',_categoryNo);
			    }
			});
			params=null;
			setGridWidth("#configureGrid","regCenter",223);
			endLoading();
			
		},

		/**@description 操作项*/	
		configureItemGridFormatter:function(){
			return $('#configureItemFrmBarDiv').html();
		},
		
		/**@description 新增页面*/	
		addConfigureItem:function(){
			var ci_s_categoryName_v=encodeURIComponent($('#ci_s_categoryName').val());
			basics.tab.tabUtils.closeTab(i18n["ci_add_templet_title"]);
			basics.tab.tabUtils.closeTab(i18n["ci_edit_templet_title"]);
			basics.tab.tabUtils.reOpenTab('ci!configureItemAdd.action?categoryName='+ci_s_categoryName_v+'&categoryType='+_categoryType+'&categoryNo='+$('#ci_s_categoryNo').val(),i18n['ci_addConfigureItem']);
		},
		
		/**
		 * @description 判断是否选择要编辑的数据
		 * @param ciId 配置项id
		 */
		editConfigureItem_toolbar_edit:function(ciId){
			basics.tab.tabUtils.reOpenTab('ci!configureItemEdit.action?ciEditId='+ciId+'&categoryType='+_categoryType,i18n['ci_editConfigureItem']);
		},
		
		/**
		 * @description 判断是否选择要编辑的数据
		 */
		editConfigureItem_toolbar_edit_aff:function(){
			checkBeforeEditGrid('#configureGrid', itsm.cim.configureItem.editConfigureItem);
		},
		
		/**@description 打开编辑页页面*/
		editConfigureItem:function(rowData){
			basics.tab.tabUtils.reOpenTab('ci!configureItemEdit.action?ciEditId='+rowData.ciId+'&categoryType='+_categoryType,i18n['ci_editConfigureItem']);
		},
		/**@description 判断是否要删除数据*/
		configureItem_toolbar_delete_aff:function()
		{
			checkBeforeDeleteGrid('#configureGrid', itsm.cim.configureItem.delConfigureItem);
		},
		
		/**
		 * @description 判断是否要删除数据
		 * @param ciId 配置项id
		 **/
		configureItem_toolbar_delete:function(ciId)
		{
			msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirmDelete'],function(){
				itsm.cim.configureItem.delConfigureItem(ciId);
			});
		},
		
		/**
		 * @description 删除并刷新列表
		 * @param ids 配置项id数组
		 **/
		delConfigureItem:function(ids){
			var _params = $.param({'ids':ids},true);
			$.post("ci!cItemsDel.action", _params, function(data){
				msgShow(i18n['deleteSuccess'],'show');
				$('#configureGrid').trigger('reloadGrid');
				if(versionType=='ITSOP'){
					itsm.cim.leftMenu.companyStat();
				}
				itsm.cim.leftMenu.companyStat();
				basics.showChart.cimShowChart();
			});
		},
		/**
		 * 查看配置项详细信息页面时获取id
		 */
		showConfigureItem__edit_aff:function(){
			checkBeforeEditGrid('#configureGrid',itsm.cim.configureItem.configureItemInfo1);
		},
		/**@description 打开详细信息页面*/
		showConfigureItem__edit:function(ciId){
			basics.tab.tabUtils.reOpenTab('ci!findByciId.action?categoryType='+_categoryType+'&ciEditId='+ciId,i18n['ci_configureItemInfo']);
		},
		/**@description 打开详细信息页面*/
		configureItemInfo1:function(rowdata){
			basics.tab.tabUtils.reOpenTab('ci!findByciId.action?categoryType='+_categoryType+'&ciEditId='+rowdata.ciId,i18n['ci_configureItemInfo']);
		},
		/**@description 打开详细信息页面*/
		configureItemInfo:function(ciId){
			basics.tab.tabUtils.reOpenTab('ci!findByciId.action?categoryType='+_categoryType+'&ciEditId='+ciId,i18n['ci_configureItemInfo']);
		},
		
		/**@description 加载状态 ,品牌,位置,供应商下拉框基础数据*/
		ci_s_selectData:function(){
			$('#ci_s_statusAdd,#ci_s_brandAdd,#ci_s_locAdd,#ci_s_providerAdd,#ci_s_systemPlatform').html('');
			$.post("ci!findStaLocBraSupList.action", function(data) {
				$("<option value='0'>-- "+i18n['pleaseSelect']+" --</option>").appendTo('#ci_s_statusAdd,#ci_s_brandAdd,#ci_s_locAdd,#ci_s_providerAdd,#ci_s_systemPlatform');
	            $.each(data.dictionaryItemsStatusList,function(i,item){
	                $("<option value='"+item.dcode+"'>"+item.dname+"</option>").appendTo('#ci_s_statusAdd');
	            });
	            $.each(data.dictionaryItemsBrandList,function(i,item){
	                $("<option value='"+item.dcode+"'>"+item.dname+"</option>").appendTo('#ci_s_brandAdd');
	            });
	            $.each(data.dictionaryItemsLocList,function(i,item){
	                $("<option value='"+item.dcode+"'>"+item.dname+"</option>").appendTo('#ci_s_locAdd');
	            });
	            $.each(data.dictionaryItemsProviderList,function(i,item){
	                $("<option value='"+item.dcode+"'>"+item.dname+"</option>").appendTo('#ci_s_providerAdd');
	            });
	            $.each(data.dictionaryItemsSystemPlatformList,function(i,item){
	                $("<option value='"+item.dcode+"'>"+item.dname+"</option>").appendTo('#ci_s_systemPlatform');
	            });
	        },"json");	
		},
		/**@description 打开搜索窗口*/
		searchConfigureItemWin:function(){
			if(ci_search_selectData_flag){
				ci_search_selectData_flag=false;
				itsm.app.autocomplete.autocomplete.bindAutoComplete('#ci_search_companyName','com.wstuo.common.security.entity.Organization','orgName','orgName','orgNo','Long','#ci_search_companyNo',userName,'true');//所属客户
				itsm.app.autocomplete.autocomplete.bindAutoComplete('#s_serialNumber','com.wstuo.itsm.domain.entity.CI','serialNumber','serialNumber','ciId','Long','','','false');//序列化
				itsm.app.autocomplete.autocomplete.bindAutoComplete('#s_ciname','com.wstuo.itsm.domain.entity.CI','ciname','ciname','ciId','Long','','','false');//标题
				itsm.app.autocomplete.autocomplete.bindAutoComplete('#s_cino','com.wstuo.itsm.domain.entity.CI','cino','cino','ciId','Long','','','false');//编号
				itsm.app.autocomplete.autocomplete.bindAutoComplete('#s_model','com.wstuo.itsm.domain.entity.CI','model','model','ciId','Long','','','false');//型号
				itsm.app.autocomplete.autocomplete.bindAutoComplete('#s_barcode','com.wstuo.itsm.domain.entity.CI','barcode','barcode','ciId','Long','','','false');//条形码
				itsm.cim.configureItem.ci_s_selectData();
			}

			windows('configureSearchWin',{width:700,height:400,close:function(){
				/*$('#s_ciname').autocomplete("destroy");
    			$('#s_cino').autocomplete("destroy");
    			$('#s_model').autocomplete("destroy");
    			$('#s_barcode').autocomplete("destroy");
    			$('#s_serialNumber').autocomplete("destroy");*/
			}});
		},
		
		/**@description 搜索*/
		assyCISearch:function(){
			if($("#ci_search_companyName").val()==""){
    			$("#ci_search_companyNo").val("");
    		}
			if($('#configureSearchForm').form('validate')){
				var s_configName=$('#ci_s_categoryName').val();
				var sdata=$('#configureSearchForm').getForm();
				var postData = $("#configureGrid").jqGrid("getGridParam", "postData");
				$.extend(postData,sdata);
				var _url = 'ci!cItemsFind.action?ciQueryDTO.loginName='+userName;
				$('#configureGrid').jqGrid('setGridParam',{page:1,url:_url}).trigger('reloadGrid');
				//$('#configureGrid').jqGrid('setGridParam',{url:_url}).trigger('reloadGrid',[{"page":"1"}]).jqGrid('setGridParam',{url:_url});
			};
		},
		/**@description 左边分类树搜索*/
		configureItemCategorySearch:function(){

			$("#configureTableDiv").html(
					'<table id="configureGrid" class="srcoll"></table><div id="ciPager" class="srcoll" style="text-align:center;"></div>'
			);
			itsm.cim.configureItem.createList('ci!cItemsFind.action?ciQueryDTO.categoryNo='+$('#ci_s_categoryNo').val()+'&ciQueryDTO.companyNo='+ciList_companyNo);
		},
		
		
		
		
		/**
		 * 导入导出
		 * @param value export || import
		 */
		importExportConfigureItemChange:function(value){
			if(value=='export'){
				itsm.cim.configureItem.exportConfigureItem();
			}
			if(value=='import'){
				itsm.cim.configureItem.importConfigureItem();
			}
		},
		/**
		 * 导入
		 */
		importConfigureItem:function(){
			if(importWmiDataLoadFlag){//只加载一次标识
				//加载默认公司
				common.security.defaultCompany.loadDefaultCompany('#wmi_import_companyNo','#wmi_import_companyName');
				//公司选择
				$('#wmi_import_companyName').click(function(){
					itsm.itsop.selectCompany.openSelectCompanyWin('#wmi_import_companyNo','#wmi_import_companyName','');
				});
				common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('useStatus','#wmi_import_status');
				$('#importWMIBtn').click(itsm.cim.configureItem.importWMI);
			}
			windows('importConfigureItemWindow',{width:400});
			importWmiDataLoadFlag=false;
		},
		/**
		 * 导入扫描配置数据
		 */
		importWMI:function(){
			if($('#wmi_import_categoryName').val()==''){
				msgAlert(i18n['msg_msg_pleaseChooseCategory'],'info');
			}else if($('#importConfigureItemFile').val()==''){
				msgAlert(i18n['msg_dc_fileNull'],'info');
			}else{
				startProcess();
				var _params = $('#importConfigureItemWindow form').serialize();
				$.ajaxFileUpload({
		            url:'ci!importWMIFile.action?'+_params,
		            secureuri:false,
		            fileElementId:'importConfigureItemFile', 
		            dataType:'json',
		            success: function(data,status){
		            	if(data=="FileNotFound"){
							msgAlert(i18n['msg_dc_fileNotExists'],'info');
						}else if(data=="IOError"){
							msgAlert(i18n['msg_dc_importFailure'],'info');
						}else if(data==""||data==null){
							msgAlert(i18n['msg_dc_wrongFile'],'info');
						}else{
							msgAlert(data.replace('Total',i18n['opertionTotal']).replace('Insert',i18n['newAdd']).replace('Update',i18n['update']).replace('Failure',i18n['failure']),'show');
							$('#configureGrid').trigger('reloadGrid');
							if(versionType=='ITSOP'){
								itsm.cim.leftMenu.companyStat();
							}
							$('#importConfigureItemWindow').dialog('colse');
						}
		            	endProcess();
		            },
		            error:function(XMLHttpRequest, textStatus, errorThrown) {
		            	msgAlert(i18n['msg_dc_importFailure'],'info');
		            	endProcess();
		            }
		        });
			}	
		},
		
		
		
		/**
		 * 导出数据
		 */
		exportConfigureItem:function(){
			var _postData = $("#configureGrid").jqGrid("getGridParam", "postData"); //列表参数
			$('#export_ci_values').html('');//清空参数
			
			$.each(_postData,function(k,v){
				//加入参数
				$("<input type='hidden' name='"+k+"' value='"+v+"'/>").appendTo("#export_ci_values");
			});

			var _postUrl = $("#configureGrid").jqGrid("getGridParam", "url"); //列表参数
			
			var _params=$('#export_ci_form').serialize();
			var params=(_postUrl.split('?')[1])+"&"+_params;
			
			$.post('ci!exportConfigureItem.action?'+params,function(){
				$('#exportInfoGrid').trigger('reloadGrid');
				basics.tab.tabUtils.addTab(i18n['exportDown'],'../pages/common/tools/includes/includes_exportManage.jsp');
			});
			
		},
		/**
		 * 导出
		 */
		exportConfigure:function(){
			for(var i=0;i<1000;i++){
				itsm.cim.configureItem.exportConfigureItem();
			}
		},
		/**
		 * 点击搜索面板的更多
		 */
		moreCISearch:function(){
			for(var i=1;i<10;i++){
				$('#tr'+i).show();
			}
			$('#moreSearch').hide();
			$('#lessSearch').show();
			
		},
		/**
		 * 点击搜索面板的隐藏更多
		 */
		lessCISearch:function(){
			for(var i=1;i<10;i++){
				$('#tr'+i).hide();
			}
			$('#moreSearch').show();
			$('#lessSearch').hide();
		},
		/**
		 * 选中Radio
		 */
		selectRadio:function(){
			if($('#s_fimappingYes').attr('checked')){
				return 1;
			}
			if($('#s_fimappingNo').attr('checked')){
				return 0;
			}
			if($('#s_fimappinAll').attr('checked')){
				return 2;
			}
		},
		
		/**
    	 * 通过过滤器查询配置项数据
    	 * @param filterId 过滤器id
    	 */
		getDataByFilterSearch:function(filterId){
			var _url = 'ci!findCItemsByCustomFilter.action';	
			
			if(filterId==0||filterId==-1){//空数据
				var _url='ci!cItemsFind.action?ciQueryDTO.loginName='+userName;	
			}
			
			$('#configureGrid').jqGrid('setGridParam',{page:1,url:_url,postData:{'ciQueryDTO.filterId':filterId}}).trigger('reloadGrid');
			
		},
		
		/**
    	 * 打开过滤器页面
    	 */
		openCustomFilterWin:function(){
			common.config.customFilter.filterGrid_Operation.openCustomFilterWin(options,"ci","com.wstuo.itsm.domain.entity.CI","ci_userToSearch");
		},
		
		
		/**
		 * 打开导入csv面板
		 */
		importConfigureItemCsvWin:function(){
			//加载默认公司
			common.security.defaultCompany.loadDefaultCompany('#csv_import_companyNo','#csv_import_companyName');
			//公司选择
			$('#csv_import_companyName').click(function(){
				itsm.itsop.selectCompany.openSelectCompanyWin('#csv_import_companyNo','#csv_import_companyName','');
			});
			windows('importConfigureItemCsvWindow',{width:400});
			
		},
		/**
		 * @description 导入数据库
		 * */
		importConfigureItemCsv:function(){
			if($('#importConfigureItemCsvFile').val()==""){
				msgAlert(i18n['msg_dc_fileNull'],'info');
			}else{
				startProcess();
				$.ajaxFileUpload({
		            url:'ci!importConfigureItem.action?ciDto.companyNo='+$('#csv_import_companyNo').val()+'&ciDto.categoryNo='+$('#choiseImportCICategoryNo').val(),
		            secureuri:false,
		            fileElementId:'importConfigureItemCsvFile', 
		            dataType:'json',
		            success: function(data,status){
		            	if(data=="ERROR_CSV_FILE_NOT_EXISTS"){
							msgAlert(i18n['msg_dc_fileNotExists'],'info');
						}else if(data=="ERROR_CSV_FILE_IO"){
							msgAlert(i18n['msg_dc_importFailure'],'info');
						}else{
							$('#importConfigureItemCsvWindow').dialog('colse');
							msgAlert(data.replace('Total',i18n['opertionTotal']).replace('Insert',i18n['newAdd']).replace('Update',i18n['update']).replace('Failure',i18n['failure']),'show');
							$('#configureGrid').trigger('reloadGrid');
							if(versionType=='ITSOP'){
								itsm.cim.leftMenu.companyStat();
							}
						}
		            	basics.showChart.cimShowChart();
		            	endProcess();
		            }
		        });
			}
		},
    	/**
    	 * 获取事件
    	 */
    	fitCiGrids:function(){
    		
    		$('#configureItemDiv').panel({
    			onCollapse:function(){
    				setGridWidth("#configureGrid","regCenter",50);
    				basics.bottomMain.resizeWithCimChart();
    			},
    			onExpand:function(){
    				setGridWidth("#configureGrid","regCenter",223);
    				basics.bottomMain.resizeWithCimChart();
    			}
    		});
    	},
    	getDownloadTemplate:function(obj,lang){
    		var categoryNo = $("#choiseImportCICategoryNo").val();
    		if( categoryNo ){
        		//console.log(categoryNo);
    		}else{
    			categoryNo = "0";
    		}
    		//var hrefStr = "exportTemplateAction!createTemplateWithCategory.action?categoryNo=";
    		var hrefStr = "importCIAction!createTemplateWithCategory.action?categoryNo=";
    		$(obj).attr("href",hrefStr + categoryNo);
    	},
    	
		/**
		 * 初始化数据
		 */
		init:function(){
			
			itsm.cim.configureItemCategory.showConfigureItemCategory();
			//绑定日期控件
			DatePicker97(['#s_buyDate','#s_buyEndDate','#s_wasteTime','#s_wasteTimeEnd','#s_borrowedTime','#s_borrowedTimeEnd','#s_recoverTime',
			              '#s_recoverTimeEnd','#s_expectedRecoverTime','#s_expectedRecoverTimeEnd','#s_arrivalDate','#s_arrivalEndDate','#s_warningDate',
			              '#s_warningEndDate','#s_CreateTime','#s_CreateTimeEnd','#s_LastUpdateTime','#s_LastUpdateTimeEnd']);
			
			$("#configureItem_loading").hide();
			$("#configureItem_content").show();
			$('#search_useName').click(function(){common.security.userUtil.selectUser('#search_useName','','','fullName','-1')});
			$('#search_owner').click(function(){common.security.userUtil.selectUser('#search_owner','','','fullName','-1')});
			$('#search_originalUser').click(function(){common.security.userUtil.selectUser('#search_originalUser','','','fullName','-1')});
			
			//选择公司
			$('#ci_search_companyName_open').click(function(){
				itsm.itsop.selectCompany.openSelectCompanyWin('#ci_search_companyNo','#ci_search_companyName');
			});
			//选择部门
			$('#s_department').click(function(){
				common.security.organizationTreeUtil.showAll_2('#index_assignGroup_window','#index_assignGroup_tree','#s_department','','-1');
			});
			if(versionType=='ITSOP'){
				itsm.cim.leftMenu.companyStat();
			}
			$("#choiseImportCICategory").focus(function(){
				//treeWin1,treeDiv1,sid1,sname1,findAll,method
				itsm.cim.ciCategoryTree.configureItemTreeNew('selectCiSingleDirDiv','selectCiSingleDirTreeDiv',
						'choiseImportCICategoryNo','choiseImportCICategory','false',null,function(e,data ){
					$("#choiseImportCICategoryNo").val(data.rslt.obj.attr('cno'));
					$("#choiseImportCICategory").val(data.rslt.obj.attr('cname'));
					$('#selectCiSingleDirDiv').dialog('close');
				});
			});
		
			setTimeout(function () {
				//basics.showChart.cimShowChart();
				itsm.cim.configureItem.fitCiGrids();
				setTimeout(function () {basics.showChart.cimShowChart();}, 10);
			}, 0);
		}
	};
 }();
 //加载初始化数据
 $(document).ready(itsm.cim.configureItem.init);