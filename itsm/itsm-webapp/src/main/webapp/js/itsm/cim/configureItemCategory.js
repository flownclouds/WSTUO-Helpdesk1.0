$package('itsm.cim');
$import('itsm.cim.cimTop');
/**  
 * @fileOverview 配置项分类树.
 * @author QXY
 * @version 1.0  
 */  
 /**  
 * @author QXY  
 * @constructor 
 * @description 配置项分类树主函数.
 * @date 2010-04-28
 * @since version 1.0 
 * @returns
 * @param select
 */
itsm.cim.configureItemCategory=function(){
	return {
		
		/** 
		 * @description 显示配置项分类树
		 **/
		showConfigureItemCategory:function(){
			$("#configureItemCategoryTree").jstree({
				"json_data":{
				    ajax: {
				    	url : "ciCategory!getConfigurationCategoryTree.action?findAll=false&userName="+userName+"&simpleDto=true",
				    	data:function(n){
					    	  return {'parentEventId':n.attr ? n.attr("cno").replace("node_",""):0};//types is in action
					    },
				    	cache: false}
				},
				"plugins" : ["themes", "json_data", "ui", "crrm", "cookies", "types", "hotkeys"]
			})
			//.bind('loaded.jstree', function(e,data){data.inst.open_all(-1);})
			.bind('select_node.jstree',itsm.cim.configureItemCategory.configureItemCategoryTree_selectNode);
		},
		
		/**
		 * @description 配置项树 
		 **/
		configureItemCategoryTree_selectNode:function(e, data){
			var _parent = data.inst._get_parent();
			var _categoryNo=data.rslt.obj.attr('cno');
			var _categoryName=data.rslt.obj.attr('cname');
			var categoryType=data.rslt.obj.attr('categoryType');
			
			if(_parent==-1){
				_categoryNo='';
				_categoryName='';
			}
			var _postData={};
			$.extend(_postData,{'ciQueryDTO.categoryNo':_categoryNo,'ciQueryDTO.categoryName':_categoryName,'ciQueryDTO.loginName':userName});
			var _url='ci!cItemsFind.action';
			//$('#configureGrid').jqGrid('setGridParam',{postData:_postData,page:1,url:_url}).trigger('reloadGrid');
			basics.tab.tabUtils.closeTab(i18n['ci_configureItemAdmin']);
			basics.tab.tabUtils.addTab(i18n['ci_configureItemAdmin'],'../pages/itsm/cim/configureItem.jsp?categoryNo='+_categoryNo+'&companyNo='+ciList_companyNo,function(){
				itsm.cim.cimTop.includesLoading();
			});
		}
	}
	
}();
