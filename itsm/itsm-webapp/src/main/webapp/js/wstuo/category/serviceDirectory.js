$package('common.config.category')
 /**  
 * @author QXY  
 * @constructor 服务目录管理主函数
 * @description 服务目录管理主函数
 * @date 2010-11-17
 * @since version 1.0 
 */  
common.config.category.serviceDirectoryAdmin=function(){
	this.opt;
	return {
		/**
		 * 服务目录列表
		 */
		serviceDirectoryList:function(){
			var params = $.extend({},jqGridParams, {	
				url:'sd!findPagerServiceDirectory.action',
				caption:i18n['title_service_directory'],
				colNames:[i18n['number'],i18n['name'],i18n['description'],i18n['operateItems']],
				colModel:[
			   		{name:'serviceDirectoryId',width:60},
			   		{name:'serviceDirectoryName',width:150},
			   		{name:'serviceDirectoryDesc',width:200,sortable:false},
			   		{name:'act',align:'center',sortable:false,formatter:common.config.category.serviceDirectoryAdmin.sdActFormatter}
			   	],		
				jsonReader: $.extend(jqGridJsonReader, {id: "serviceDirectoryId"}),
				sortname:'serviceDirectoryId',
				onSelectRow:function(rowId){common.config.category.serviceDirectoryAdmin.subServiceListReload(rowId)},
				pager:'#serviceDirectoryPager'
			});
			$("#serviceDirectoryGrid").jqGrid(params);
			$("#serviceDirectoryGrid").navGrid('#serviceDirectoryPager',navGridParams);
			//列表操作项
			$("#t_serviceDirectoryGrid").css(jqGridTopStyles);
			$("#t_serviceDirectoryGrid").append($('#serviceDirectoryGridToolbar').html());
			//自适应大小
			setGridWidth("#serviceDirectoryGrid","regCenter",20);
		},
		/**
		 * 服务目录操作项
		 */
		sdActFormatter:function(cell,opt,data){
			return $('#serviceDirectoryActFormatterDiv').html();
		},
		/**
		 * 服务目录添加窗口
		 */
		openSdAddWin:function(){
			$("#serviceDirectoryId").val('');
			$("#serviceDirectoryName").val('');
			$("#serviceDirectoryDesc").val('');
			opt='saveServiceDirectory';
			windows('serviceDirectoryAddOrEditDiv',{width:350});
		},
		/**
		 * 服务目录编辑窗口
		 * @param rowData 服务目录数据
		 */
		openSdEditWin:function(rowData) {
			$("#serviceDirectoryName").val(rowData.serviceDirectoryName);
			$("#serviceDirectoryDesc").val(rowData.serviceDirectoryDesc);
			$("#serviceDirectoryId").val(rowData.serviceDirectoryId);
			opt='editServiceDirectory';
			windows('serviceDirectoryAddOrEditDiv',{width:350});
		},
		/**
		 * 编辑服务目录
		 */
		sd_edit_aff:function(){
			checkBeforeEditGrid('#serviceDirectoryGrid', common.config.category.serviceDirectoryAdmin.openSdEditWin);
		},
		/**
		 * 服务目录添加编辑保存
		 */
		sdAddOrEditOpt:function(){
			if($('#serviceDirectoryAddOrEditDiv form').form('validate')){
				var _params = $('#serviceDirectoryAddOrEditDiv form').serialize();
				$.post('sd!'+opt+'.action', _params, function(){
					$('#serviceDirectoryAddOrEditDiv').dialog('close');
					$('#serviceDirectoryGrid').trigger('reloadGrid');
					msgShow(i18n['saveSuccess'],'show');
				})
			}
		},
		/**
		 * 删除服务目录
		 */
		sd_delete_aff:function(){
			checkBeforeDeleteGrid('#serviceDirectoryGrid',common.config.category.serviceDirectoryAdmin.deleteSd);
		},
		/**
		 * 执行删除
		 * @param  rowIds 行编号
		 */
		deleteSd:function(rowIds) {
			var _param = $.param({'ids':rowIds},true);
			$.post("sd!deleteServiceDirectory.action", _param, function(){
				$('#serviceDirectoryGrid').trigger('reloadGrid');
				msgShow(i18n['deleteSuccess'],'show');
				
				$('#subServiceId_serviceDirectoryId').val('');
				$('#subServiceGrid').jqGrid('setGridParam',{url:'sd!findPageSubService.action'}).trigger('reloadGrid',[{"page":"1"}]);
				

			}, "json");
		},
		/**
		 * 查看服务
		 */
		viewSubService:function(){
			var rowId = $('#serviceDirectoryGrid').getGridParam('selrow');
			if(rowId==null){
				msgAlert('<br/>'+i18n['msg_atLeastChooseOneData'],'info');
			}else{
				var rowData=$('#serviceDirectoryGrid').getRowData(rowId);
				basics.tab.tabUtils.reOpenTab('../pages/config/serviceDirectory/subService.jsp?serviceDirectoryId='+rowData.serviceDirectoryId,i18n['title_adminSubService']);
			}
		},
		/**
		 * 子服务目录列表
		 * @param id 子服务目录Id
		 */
		subServiceListReload:function(id){
			$('#subServiceId_serviceDirectoryId').val(id);
			var _url = 'sd!findPageSubService.action?ssqDto.serviceDirectoryId='+id;		
			$('#subServiceGrid').jqGrid('setGridParam',{url:_url})
				.trigger('reloadGrid',[{"page":"1"}]).jqGrid('setGridParam',{url:_url});
		},
		/**
		 * 服务列表
		 */
		subServiceList:function(){
			var params = $.extend({},jqGridParams, {	
				url:'sd!findPageSubService.action',
				caption:i18n['title_subService'],
				colNames:[i18n['number'],i18n['name'],i18n['description'],i18n['operateItems']],
				colModel:[
			   		{name:'subServiceId',width:60,sortable:false},
			   		{name:'subServiceName',width:150,sortable:false},
			   		{name:'subServiceDesc',width:200,sortable:false},
			   		{name:'act',hidden:true}
			   	],		
				jsonReader: $.extend(jqGridJsonReader, {id: "subServiceId"}),
				sortname:'subServiceId',
				pager:'#subServicePager'
			});
			$("#subServiceGrid").jqGrid(params);
			$("#subServiceGrid").navGrid('#subServicePager',navGridParams);
			//列表操作项
			$("#t_subServiceGrid").css(jqGridTopStyles);
			$("#t_subServiceGrid").append($('#subServiceGridToolbar').html());
			//自适应大小
			setGridWidth("#subServiceGrid","regCenter",20);
		},
		/**
		 * 服务添加窗口
		 */
		openSubServiceAddOrEditWin:function(){
			opt='saveSubService';
			if($('#subServiceId_serviceDirectoryId').val()!=''){
				$('#subServiceName').val('');
				$('#subServiceDesc').val('');
				windows('subServiceAddDiv',{width:350});
			}else{
				
				msgAlert(i18n['msg_msg_pleaseChooseDirectory'],'info');
			}
		},
		/**
		 * 服务添加编辑保存
		 */
		subServiceAddOrEditOpt:function(){
			if($('#subServiceAddDiv form').form('validate')){
				var _params = $('#subServiceAddDiv form').serialize();
				$.post('subService!'+opt+'.action', _params, function(){
					$('#subServiceAddDiv').dialog('close');
					$('#subServiceGrid').trigger('reloadGrid');
					msgShow(i18n['saveSuccess'],'show');
				})
			}
		},
		/**
		 * 服务编辑窗口
		 * @param  rowData 行数据
		 */
		openSubServiceEditWin:function(rowData) {
			$("#subServiceId").val(rowData.subServiceId);
			$("#subServiceName").val(rowData.subServiceName);
			$("#subServiceDesc").val(rowData.subServiceDesc);
			opt='editSubService';
			windows('subServiceAddDiv',{width:350});
		},
		/**
		 * 编辑子服务目录
		 */
		subService_edit_aff:function(){
			checkBeforeEditGrid('#subServiceGrid', common.config.category.serviceDirectoryAdmin.openSubServiceEditWin);
		},
		
		/**
		 * 删除服务目录
		 */
		subService_delete_aff:function(){
			checkBeforeDeleteGrid('#subServiceGrid', common.config.category.serviceDirectoryAdmin.deleteSubService);
		},
		
		/**
		 * 执行删除
		 * @param  rowIds 行编号
		 */
		deleteSubService:function(rowIds) {
			var _param = $.param({'ids':rowIds},true);
			$.post("subService!deleteSubService.action", _param+"&sdId="+$('#subServiceId_serviceDirectoryId').val(), function(){
				msgShow(i18n['deleteSuccess'],'show');
				$('#subServiceGrid').trigger('reloadGrid');
			}, "json");
		},
	
		/**
		 * 导入或导出数据到Excel
		 * @param value import为导入；export为导出
		 */
		importExportServiceDirectoryData:function(value){
			if(value=="import"){
				common.config.category.serviceDirectoryAdmin.importServiceDirectoryData();
			}
			if(value=="export"){
				common.config.category.serviceDirectoryAdmin.exportServiceDirectoryData();
			}
		},
		/**
		 * 导出
		 * @private
		 */
		exportServiceDirectoryData:function(){
			var usData = $("#serviceDirectoryGrid").jqGrid("getGridParam", "postData");
			$('#exportServiceDirectory_sidx').val(usData.sidx);
			$('#exportServiceDirectory_sord').val(usData.sord);
			$('#exportServiceDirectory_page').val(usData.page);
			$('#exportServiceDirectory_rows').val(usData.rows);
			$("#exportServiceDirectoryWindow form").submit();
		},
		
		/**
		 * 导入数据.
		 * @private
		 */
		importServiceDirectoryData:function(){
			windows('importServiceDirectoryDataWindow',{width:400});
		},
		/**
		 * @description CSV导入数据
		 */
		importServiceDirectoryCSV:function(){
				startProcess();
				$.ajaxFileUpload({
		            url:'sd!importServiceDirectoryData.action',
		            secureuri:false,
		            fileElementId:'importServiceDirectoryFile', 
		            dataType:'json',
		            success: function(data,status){
		             	$('#importServiceDirectoryDataWindow').dialog('close');
		             	
		             	if(data=="FileNotFound"){
							msgAlert(i18n['msg_dc_fileNotExists'],'info');
						}else if(data=="IOError"){
							msgAlert(i18n['msg_dc_importFailure'],'info');
						}else{
							$('#serviceDirectoryGrid').trigger('reloadGrid');
							msgAlert(i18n['msg_dc_importSuccess'].replace('N','<br>['+data+']<br>'),'show');
						}
		             	endProcess();
		            }
		        });
		},
		/**
		 * 初始化
		 * @private
		 */
		init:function(){
			showAndHidePanel("#serviceDirectoryManage_panel","#serviceDirectoryManage_loading");
			common.config.category.serviceDirectoryAdmin.serviceDirectoryList();
			//页面事件
			$('#serviceDirectory_add_but').click(common.config.category.serviceDirectoryAdmin.openSdAddWin);
			$('#serviceDirectory_edit_but').click(common.config.category.serviceDirectoryAdmin.sd_edit_aff);
			$('#serviceDirectory_delete_but').click(common.config.category.serviceDirectoryAdmin.sd_delete_aff);
			$('#serviceDirectory_add_opt_but').click(common.config.category.serviceDirectoryAdmin.sdAddOrEditOpt);
			$('#serviceDirectoryImportData').click(common.config.category.serviceDirectoryAdmin.importServiceDirectoryData)
			$('#serviceDirectoryExportData').click(common.config.category.serviceDirectoryAdmin.exportServiceDirectoryData)
		}	
	}
}();
$(document).ready(common.config.category.serviceDirectoryAdmin.init);