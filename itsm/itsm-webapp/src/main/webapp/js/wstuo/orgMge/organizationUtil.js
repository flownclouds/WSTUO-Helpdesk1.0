
$package('wstuo.orgMge');
/**  
 * @author WSTUO
 */
wstuo.orgMge.organizationUtil = function() {
	return {
		/**
		 * 根据公司信息填充信息.
		 * @param  idPut  选中机构（id）的赋值ID
		 * @param  namePut  选中机构（name）的赋值ID
		 */
		fillCompanyData:function(companyNo,namePut,idPut){
			$.post('organization!findCompany.action?companyNo='+companyNo,function(res){
				$(namePut).val(res.orgName);
				$(idPut).val(res.orgNo);
			});
		},
		/**
		 * @description 选择服务机构.
		 * @param treeDIV 树DIV的id
		 * @param windowDIV 窗体的DIV编号
		 * @param  orgNo_put  选中机构（id）的赋值ID
		 * @param  orgName_put  选中机构（name）的赋值ID
		 */
		selectServiceOrg:function(treeDIV,windowDIV,orgNo_put,orgName_put){
			windows(windowDIV.replace('#',''),{width:240,height:400});
			$(treeDIV).jstree({
				"json_data":{
				    ajax: {
				    	url : "organization!findServicesTree.action",
				    	data:function(n){
					    	  return {'parentOrgNo':n.attr ? n.attr("orgNo").replace("node_",""):0};//types is in action
						},
				    	cache:false
				    }
				},
				"plugins" : ["themes", "json_data", "ui", "crrm", "cookies", "types", "hotkeys"]
				})
				
				//.bind('loaded.jstree', function(e,data){data.inst.open_all(-1);})
				.bind('select_node.jstree',function(event,data){
					if(data.rslt.obj.attr("orgName")!='ROOT'){
						$(orgNo_put).val(data.rslt.obj.attr("orgNo"));
						$(orgName_put).val(data.rslt.obj.attr("orgName"));
						$(windowDIV).dialog('close');
					}
			});
		}
		
	};

}();