$package('wstuo.customForm');
$import('wstuo.customForm.formControlImpl');
$import('wstuo.sysMge.base64Util');
$import('wstuo.category.eventCategoryTree');
/**
 * 表单设计
 * @author will
 */
wstuo.customForm.customFormDesign=function(){

	this.dragg;//右边拖拽过来的对象
	this.saveMark=true;//标记是否可以添加
	this.loadshowAttrsGrid=false;
	var opt = "";
	var tabAttrNos = "";
	var tabAttrNos_merge="";
	this.formCustomType='';
	return {
		/**
		 * 删除字段
		 */
		deleteField:function(event){
			var parent=$(event).parent();
			var obj=parent.clone();
			obj.empty();
			obj.attr("class","right_options btn btn-success");
			obj.text(obj.attr("label"));
			obj.removeAttr('style');
			obj.appendTo("#customField_fieldset");
			parent.remove();
			
		},
		/**
		 * 右边拖拽完成时调用的方法
		 */
		dropComplete:function(){
			
			dragg.html(wstuo.customForm.formControlImpl.control(
					wstuo.customForm.formControlImpl.getAttrs(dragg)
					));
			dragg.attr('class','form-group col-md-6');
			if(dragg.attr("attrType")=="Lob")
				dragg.attr("class","field_options_lob field_options");
			else{
				if($("#column_select").val()==1){
					dragg.attr("class","field_options_2Column field_options");
					dragg.find('.field').attr("class","field_lob");
				}else if($("#column_select").val()==2){
					dragg.attr("class","field_options");
				}
			}
			if(dragg.attr("attrType")=="Checkbox" || dragg.attr("attrType")=="Radio"){
				wstuo.customForm.customFormDesign.fieldResize(dragg);
				//dragg.height("30px");
			}
			dragg.css({'background-color':'rgb(238, 238, 238)','border-radius': '5px'});
			setTimeout(function(){
				dragg.css("background-color",'#fff');
			},800);
		},
		
		saveFormCustom:function(sign){
			var formCustomName = $("#update_formCustomName").val();
			if(!formCustomName){
				msgShow('请输入表单名称！','info');
				return false;
			}
			if(saveMark){
				startProcess();
				saveMark=false;
				
				var formCustomId = $("#formCustomId").val();
				var customFormDesign_type = $("#formCustomType").val();
				var customFormDesign_default_form = document.getElementById("customFormDesign_default_form").checked;
				//保存json格式
				var formCustomContentJson = wstuo.customForm.formControlImpl.setFormCustomContentJson('#dashboard_column');
				var url = 'formCustom!saveFormCustom.action';
				var _param = {'formCustomDTO.formCustomId':formCustomId,
							'formCustomDTO.formCustomName':formCustomName,
							'formCustomDTO.formCustomContents':formCustomContentJson,
							'formCustomDTO.type':customFormDesign_type,
							'formCustomDTO.isDefault':customFormDesign_default_form
							};
				/*var frm =$.param($("#formCustom_tabsIdForm").serializeArray());
				if(frm!==""){
					url +="?"+frm;
				}*/
				$.post(url,_param, function(formCustomId){
					endProcess();
					$('#formCustomId').val(formCustomId);
					saveMark=true;
					msgShow(i18n.saveSuccess,'show');
				});
			}
		},
		/**
		 * @description 保存自定义表单
		 * */
		saveFormCustomEdit:function(sign){
			var formCustomId = $("#formCustomId").val();
			if(formCustomId=="" || formCustomId==0){
				wstuo.customForm.customFormDesign.saveFormCustom(sign);
			}else{
				var formCustomName = $("#formCustomName").val();
				if(formCustomName == ""){
					formCustomName = $("#update_formCustomName").val();
				}
				var customFormDesign_type = $("#formCustomType").val();
				var _url = "formCustom!isFormCustomNameExistedOnEdit.action";
				var param = $.param({'formCustomQueryDTO.type':customFormDesign_type,
					"formCustomQueryDTO.formCustomName":formCustomName,
					"formCustomQueryDTO.formCustomId":formCustomId},true);
				$.post(_url, param, function(bool){
					if(bool){
						wstuo.customForm.customFormDesign.saveFormCustom(sign);
					}else{
						msgAlert(i18n.msg_formCustomNameExist,"info");
					}
				});
			}
		},
		/**
		 * 查看是否包含必填字段
		 */
		isRequiredField : function(){
			var count = 0;
			var finalFied='requestDTO.companyName,requestDTO.etitle,requestDTO.edesc,requestDTO.createdByName';
			var label_div_list = $("#formField .field_options");
			$.each(label_div_list,function(label_div_i,label_div){
				var attrName=$(label_div).attr("attrName");
				if(finalFied.indexOf(attrName)>-1)
					count++;
			});
			return count;
		},
		/**
		 * 预览表单
		 */
		previewFormCustom:function(){
			var formCustomContents = $("#dashboard_column").html();
			var mark=false;//记录是否需要插入空白字段
			var column=0;
			$('#previewFormCustom').html(formCustomContents);
			var eles = $("#previewFormCustom :input");
			$.each(eles,function(index,value){
				var previewId =  $(value).attr("id");
				$(value).attr("id","previewFormCustom_"+previewId);//将编辑器的id改为add_scheduledTask_edesc
			});

			$("#previewFormCustom i").remove();
			$("#previewFormCustom .form-group").css('cursor','auto');
	        $('#previewFormCustom_win').dialog({
                title: i18n.formCustom_preview,
                width: 880,
                height:600,
                modal: true
            });
	        
	        wstuo.customForm.formControlImpl.formCustomInit('#previewFormCustom');
	        wstuo.customForm.formControlImpl.formCustomInitByDataDictionaray('#previewFormCustom');
	        
		},
		/**
		 * 布局初始化
		 */
		layoutInit : function(){
			/*$(".column").droppable({
				accept : ".right_options",
				activeClass: "ui-state-highligh",
				hoverClass: "ui-custom-active",
				addClasses:false,
				tolerance:'intersect',
				//tolerance:'pointer',
				drop : function(event, ui) {
					dragg=ui.draggable;//拖拽的对象
					wstuo.customForm.customFormDesign.dropComplete();
				}
			});*/
			$('#customField_fieldset,#defaultField_fieldset').sortable({
				connectWith: ".column",
				stop : function(event, ui) {
					$(".column").removeClass("ui-custom-active");
				}
			}).disableSelection();

			$('.column').sortable({
				connectWith: ".column",
				over : function(event, ui) {
					$(".column").addClass("ui-custom-active");
				},
				receive: function(event, ui) {
					dragg=ui.item;//拖拽的对象
					wstuo.customForm.customFormDesign.dropComplete();
				},
				stop : function(event, ui) {
					$(".column").removeClass("ui-custom-active");
				}
			}).disableSelection();

			wstuo.customForm.customFormDesign.resizeWith();
		},
		/**
		 * 加载扩展字段
		 */
		loadCustomField:function(module){
			formCustomType=module;
			var url="field!findAllField.action?field.module="+module;
			$.post(url,function(data){
				$("#customField_fieldset").empty();
				if(data!=null){
					$.each(data,function(ind,val){
						var event=$('<div class="right_options btn btn-success" title="'+val.fieldName+'" >'+val.fieldName+'</div>');
						event.attr({
							attrType:val.type,
							label:val.fieldName,
							attrNo:val.id,
							attrName:val.name,
							requireds:val.required,
							//attrItemName:val.attrItemName,
							//attrItemNo:val.attrItemNo,
							attrdataDictionary:val.dataDictionary
							
						});
						event.prependTo("#customField_fieldset");
					});
				}
				wstuo.customForm.customFormDesign.removeField();
			});
		},
		/**
		 * 查询相似的自定义表单
		 */
		findSimilarFormCustom : function(){
			$("#similarFormCustom").empty();
			$("#similarFormCustom").append("<option value=''>--"+i18n.pleaseSelect+"--</option>");
			if(formCustomType=="request"){
				$("#similarFormCustom").append("<option value='defaultField'>"+i18n.defaultForm+"</option><option value='basicField'>"+i18n.basicForm+"</option>");
				var url = 'formCustom!findSimilarFormCustom.action';
				var param ={"formCustomQueryDTO.eavNo":formCustomEavNo,"formCustomQueryDTO.type":formCustomType};
				$.post(url, param, function(data){
					$.each(data,function(index,formCustom_obj){
						$("#similarFormCustom").append("<option value='"+formCustom_obj.formCustomId+"'>"+formCustom_obj.formCustomName+"</option>");
					});
				});
			}else if(formCustomType=="ci")
				$("#similarFormCustom").append("<option value='cibasicField'>"+i18n.defaultForm+"</option>");
		},
		/**
		 * 获取模板的值
		 */
		getTemplateValue : function(value){
			if(value!==''){
				$("#dashboard_column").empty();
				if(value.indexOf('Field')>-1){
					$("#dashboard_column").load('common/config/formCustom/'+value+'.jsp',function(){
						//$.parser.parse($('#dashboard_column'));
						wstuo.customForm.customFormDesign.removeField_do('defaultField_list_'+formCustomType);
						wstuo.customForm.customFormDesign.editTextarea();
						$("#column_select").val("2");
						$("#customFormDesign_show_border").attr("checked","");
						$("#customFormDesign_default_form").attr("checked","");
					});
					//wstuo.customForm.customFormDesign.layoutInit();
				}else{
					var url = 'formCustom!findFormCustomById.action';
					var _param = {"formCustomId" : value};
					$.post(url,_param,function(data){
						if(data!=null){
							if(data.isShowBorder==="1")
								$("#customFormDesign_show_border").attr("checked",true);
							else
								$("#customFormDesign_show_border").attr("checked",false);
							if(data.isDefault)
								$("#customFormDesign_default_form").attr("checked",true);
							else
								$("#customFormDesign_default_form").attr("checked",false);
							$("#column_select").val("2");
							//$("#formCustomId").val(data.formCustomId);
							$('#formCustomName').val(data.formCustomName);
							var formCustomContents = wstuo.customForm.formControlImpl.designHtml(data.formCustomContents);
							$("#dashboard_column").html(formCustomContents);
							if($("#dashboard_column > div[attrno=request_companyName]").hasClass("field_options_2Column")){
								$("#column_select").val("1");
							}else
								$("#column_select").val("2");
							$.each($("#formField div[attrtype=Radio],#formField div[attrtype=Checkbox]"),function(ind,obj){
								setTimeout(function(){
									wstuo.customForm.customFormDesign.fieldResize(obj);
								},0);
							});
							wstuo.customForm.customFormDesign.removeField_do('defaultField_list_'+formCustomType);
							wstuo.customForm.customFormDesign.layoutInit();
						}
					});
				}
			}else if(formCustomType=="ci")
				wstuo.customForm.customFormDesign.find();
			
		},
		showFieldMenu : function (menuId,event) {
            if ($(menuId).is(':hidden')) {
            	$(event).children("span").text('--');
            	$(menuId).show();
            }else{
            	$(event).children("span").text('+');
            	$(menuId).hide();
            }
        },
		/**
		 * 从左边拖放字段移除右边表单设计器里面有的字段
		 */
		removeField : function(attrNos){
			var label_div_list = $("#dashboard_column div");
			var str='';
			$.each(label_div_list,function(label_div_i,label_div){
				var attrno=$(label_div).attr("attrno");
				if($("#customField_fieldset .right_options[attrno='"+attrno+"']").length>0)
					$("#customField_fieldset .right_options[attrno='"+attrno+"']").remove();
				else{
					str+="、"+$(label_div).attr("label");
					$(label_div).remove();
				}
			});
			if(str!=='')msgShow(str.substr(1)+"字段已被删除！");
			wstuo.customForm.customFormDesign.removeByTabsAttrId(tabAttrNos);
		},
		
		removeField_do : function(daultFieldList){
			$("#defaultField_fieldset div").remove();
			$("#customField_fieldset div").remove();
			$("#defaultField_fieldset").html('');
			$("#defaultField_fieldset").append($("#"+daultFieldList).html());
			wstuo.customForm.customFormDesign.loadCustomField(formCustomEavNo);
			var div_list = $("#defaultField_fieldset div").addClass("right_options");
			wstuo.customForm.customFormDesign.removeField();
		},
		
		
		
		
		/**
		 * 查询要修改的自定义表单数据
		 */
		findCustomForm:function(){
			var _param = {"formCustomId" : formCustomId};
			$.post('formCustom!findFormCustomById.action',_param,function(data){
				if(data!=null){
					$("#formCustomType").val(data.type);
					wstuo.customForm.customFormDesign.loadCustomField(data.type);
					if(data.isDefault)
						$("#customFormDesign_default_form").attr("checked",true);
					else
						$("#customFormDesign_default_form").attr("checked",false);
					$("#formCustomId").val(data.formCustomId);
					$('#update_formCustomName').val(data.formCustomName);
					
					var formCustomContents = wstuo.customForm.formControlImpl.designHtml(data.formCustomContents);
					$("#dashboard_column").empty().html(formCustomContents);
					
					$.each($("#formField div[attrtype=Radio],#formField div[attrtype=Checkbox]"),function(ind,obj){
						setTimeout(function(){
							wstuo.customForm.customFormDesign.fieldResize(obj);
						},0);
					});
					wstuo.customForm.customFormDesign.layoutInit();
					/*var tabsDtos = data.formCustomTabsDtos;
					for ( var i = 0; i < tabsDtos.length; i++) {
						var tabId =tabsDtos[i].tabId;
						var tabName =tabsDtos[i].tabName;
						if($('#formCustom_tabsIdForm #tabsId_'+tabId).length==0){
							$('#formCustom_tabsIdForm').append(
									'<div id="tabsId_'+tabId+'">'+
									'<input name="formCustomDTO.tabsIds" type="hidden" value="'+tabId+'" />'+
									'<div>');
						}
						if($('#tabsField_fieldset #tabsField_fieldset_'+tabId).length!=0){
							$('#tabsField_fieldset #tabsField_fieldset_'+tabId).remove();
						}
						$("#tabsField_fieldset").append("<div id='tabsField_fieldset_"+tabId+"' class='right_options_tabs' ondblclick='wstuo.customForm.customFormDesign.tabOndbclick("+tabId+")'><div style='float: left;width:80%'>"+tabName+"</div>" +
								"<div class='deleteTabs'><span><a onclick='wstuo.customForm.customFormDesign.deleteTabs(this,"+tabId+");'>" +
								"<img src='../skin/default/images/grid_delete.png'></a><span></div></div>");
						tabAttrNos += tabsDtos[i].tabAttrNos+",";
					}*/
				}
				wstuo.customForm.customFormDesign.editTextarea();
			});
		},
		removeByTabsAttrId:function(attrnos){
			var nos = attrnos.split(',');
			for ( var i = 0; i < nos.length; i++) {
				$("#fromCustomCenter_right .right_options[attrno='"+nos[i]+"']").remove();
			}
		},
		/**
		 * init div resieze
		 */
		fieldResize:function(obj){
			var fieldwidth=parseInt($("#formField").width());
			fieldwidth= fieldwidth/2;
			$(obj).resizable({
				handles : 'e',
				maxWidth : fieldwidth*2,
				minWidth : fieldwidth,
			    grid: fieldwidth,
			    create : function( event, ui ){
					if($(obj).attr("attrColumn")==="1")
						$(obj).width(fieldwidth);
					else
						$(obj).width( parseInt(fieldwidth)*2);
			    },
			    start : function( event, ui ){
			    	ui.element.css({'background-color':'#99CCCC','border-radius': '5px'});
			    },
			    stop : function( event, ui ){
			    	var columnWidth=$("#dashboard_column").width();
			    	var width=ui.element.width();
			    	if(width>columnWidth*0.5)
			    		ui.element.attr("attrColumn","9");//标记单选和复选值
			    	else
			    		ui.element.attr("attrColumn","1");
			    	setTimeout(function(){
			    		ui.element.css("background-color",'');
					},666);
			    	ui.element.height('auto');
			    }
			});
		    
		},
		editTextarea : function(){
			var textareaWidth = $("textarea").width();
			var field_options_lob_divWidth = $("textarea").parent().parent().width();
			var label_divWidth = $("textarea").parent().prev("div").width();
			if(field_options_lob_divWidth-label_divWidth<textareaWidth)
				$("textarea").css({"width":(field_options_lob_divWidth-label_divWidth-100),"resize":"none"});
		},
		resizeWith : function(){
			var width = browserProp.browserProp.getWinWidth();
			if(width<1200){
				//$("#formCustom_layout").css({"width":1060});/**,"overflow-x":"scroll"*/
				$("#formCustom_layout").closest(".panel-body").css({"overflow-x":"auto"});
				 
				$("#formCustom_layout,#formCustom_layout > .panel,#formCustom_layout > .panel > .panel-body").css({"width":1060});
			}
		},
		columnSelect : function(col){
			if(col==1){
				$("#dashboard_column .field_options").not(".field_options_lob").attr("class","field_options_2Column field_options");
				$("#dashboard_column .field_options .field").attr("class","field_lob");
			}else if(col==2){
				$("#dashboard_column .field_options").not(".field_options_lob").attr("class","field_options");
				$("#dashboard_column .field_options .field_lob").attr("class","field");
			}
		},
		showEavTabs:function(){
			opt="save";
			$("#tabs_id").val("");
			$("#tabs_name").val("");
			$("#tabAttrsConentJson").val("");
			$("#tabAttrsDecode").val("");
			$("#tabAttrsConentDecode").val("");
			$("#selectedTabAttrs").html("");
			$("#tabAttrsConent").html("");
			windows('tabsField_win', { width : 450 });
		},
		/**
		 * @description 国际化属性类型
		 * @parma value 属性字段类型
		 */
		attrTypeForma:function(value){
			if(value=='Lob')
				return i18n.longText;
			if(value=='Integer')
				return i18n.integer;
			if(value=='Double')
				return i18n.decimal;
			if(value=='Date')
				return i18n.date;
			if(value=='Radio')
				return i18n.radio;
			if(value=='Checkbox')
				return i18n.checkbox;
			if(value=='DataDictionaray'){
				return i18n.eav_realdatadictionary;
			}
			else
				return i18n.text;
		},  
		/**
		 * @description 国际化数据字典类型
		 * @parma value 数据字典dname
		 */
		attrDataDicForma:function(value){ 
			if(value!==""){
				var name="label_dc_"+value;      
				return i18n[name];           
			}
			return "";
		},     
		isNullForma:function(cellValue,options,rowObject){
			if(rowObject.attrIsNull){
				return i18n.no;
			}else{
				return i18n.yes;
			}
		},
		selectTabsAttrCustomField:function(tabsField_attrNos){
			var params = $.extend({},jqGridParams, {
				url:'attrs!find.action?attrsQueryDTO.attrNos='+tabsField_attrNos,  
				colNames:[i18n.name,i18n.label_beLongAttrGroup,'',i18n.type,i18n.eav_realdatadictionary,'','',i18n.eav_required,'',i18n.length,'','',i18n.sort,'',''],
				colModel:[
                        {name:'attrAsName',width:50,align:'center',sortable:false},
						{name:'attrGroupName',width:50,index:'attrGroup',align:'center',sortable:false},
						{name:'attrGroupNo',width:50,hidden:true},
						{name:'attrType',width:30,align:'center',formatter:wstuo.customForm.customFormDesign.attrTypeForma,sortable:false},
						{name:'attrdataDictionaryFormatter',width:30,align:'center',formatter:wstuo.customForm.customFormDesign.attrDataDicForma,sortable:false,hidden:true},  
						{name:'attrdataDictionary',width:30,hidden:true}, 
						{name:'attrType',width:30,hidden:true},
						{name:'attrIsNullFormatter',width:30,align:'center',formatter:wstuo.customForm.customFormDesign.isNullForma,sortable:false},
						{name:'attrIsNull',width:30,hidden:true},
						{name:'length',width:20,align:'center',hidden:true},
						{name:'attrNo',hidden:true},
						{name:'attrCode',width:30,hidden:true},
						{name:'sortNo',width:30,hidden:false,align:'center',sortable:false},      
						{name:'attrName',hidden:true},
						{name:'attrItemName',hidden:true}
                        ],
				jsonReader: $.extend(jqGridJsonReader, {id: "attrNo"}),
				sortname:'attrNo',
				toolbar:true,
				pager:'#tabsAttr_customFieldPager'
				});
			
				$("#tabsAttr_customFieldList").jqGrid(params);
				$("#tabsAttr_customFieldList").navGrid('#tabsAttr_customFieldPager',navGridParams);
				
				$('#tabsAttr_customFieldGrid_select').click(function(){
					wstuo.customForm.customFormDesign.selectTabsAttrGrid();
				});
				
		},
		selectTabsAttrGrid:function(){
			var rowIds = $("#tabsAttr_customFieldList").getGridParam('selarrrow');
			if(rowIds=='' || rowIds.length<=0){
				msgAlert(i18n['msg_atLeastChooseOneData'],'info');
			}else{
				for ( var i = 0; i<rowIds.length; i++) {
					var data=$("#tabsAttr_customFieldList").getRowData(rowIds[i]);
					if($('#selectedTabAttrs #tabAttrsId_'+data.attrNo).length==0){
						$('#selectedTabAttrs').append('<div id="tabAttrsId_'+data.attrNo+'">'+data.attrAsName+
								'<input name="tabAttrsId" type="hidden" value="'+data.attrNo+'" />'+
								'&nbsp;&nbsp;&nbsp;<a style="color:red;" onclick="wstuo.customForm.customFormDesign.selectedTabAttrRemove(\''+data.attrNo+'\')">X</a>'+
								'<div>');
						var event = wstuo.customForm.customFormDesign.attrEvent(data);
						event.prependTo("#tabAttrsConent");
					}
				}
				$('#tabsAttrDiv').dialog('close');
			}
		},
		attrEvent:function(val){
			var event=$('<div class="right_options" title="'+val.attrAsName+'" >'+val.attrAsName+'</div>');
			event.attr({
				attrType:val.attrType,
				label:val.attrAsName,
				attrNo:val.attrNo,
				attrName:val.attrName,
				requireds:val.attrIsNull,
				attrdataDictionary:val.attrdataDictionary,
				attrItemName:val.attrItemName,
				attrColumn:"1"//一列显示
			});
			return event;
		},
		selectedTabAttrRemove:function(id){
			$('#tabAttrsId_'+id).remove();
			$("#tabAttrsConent .right_options[attrno='"+id+"']").remove();
		},
		tabAttrsSave:function(){
			var tabAttrsConentJson = wstuo.customForm.formControlImpl.setFormCustomContentJson('#tabAttrsConent');
			var tabAttrsDecode = wstuo.sysMge.base64Util.encode($("#selectedTabAttrs").html());
			var tabAttrsConentDecode =wstuo.sysMge.base64Util.encode($("#tabAttrsConent").html());
			$("#tabAttrsConentJson").val(tabAttrsConentJson);
			$("#tabAttrsDecode").val(tabAttrsDecode);
			$("#tabAttrsConentDecode").val(tabAttrsConentDecode);
			var frm = $('#tabsField_form').serialize();
			var tabName = $("#tabs_name").val();
			if(!tabName || $('#tabAttrsConent div').length==0){
				msgShow(i18n.msg_tabs_name_filed_notnull,'show');
				return false;
			}
			var url = "formCustomTabs!saveFormCustomTabs.action";
			if(opt=="merge"){
				url = "formCustomTabs!updateFormCustomTabs.action";
			}
			$.post(url,frm, function(res){
				if(res){
					if($('#formCustom_tabsIdForm #tabsId_'+res).length==0){
						$('#formCustom_tabsIdForm').append(
								'<div id="tabsId_'+res+'">'+
								'<input name="formCustomDTO.tabsIds" type="hidden" value="'+res+'" />'+
								'<div>');
					}
					if($('#tabsField_fieldset #tabsField_fieldset_'+res).length!=0){
						$('#tabsField_fieldset #tabsField_fieldset_'+res).remove();
					}
					$("#tabsField_fieldset").append("<div id='tabsField_fieldset_"+res+"' class='right_options_tabs' ondblclick='wstuo.customForm.customFormDesign.tabOndbclick("+res+")'><div style='float: left;width:80%'>"+tabName+"</div>" +
							"<div class='deleteTabs'><span><a onclick='wstuo.customForm.customFormDesign.deleteTabs(this,"+res+");'>" +
							"<img src='../skin/default/images/grid_delete.png'></a><span></div></div>");
					$('#tabsField_win').dialog('close');
					wstuo.customForm.customFormDesign.removeByTabsField();
					if(opt=="merge"){
						wstuo.customForm.customFormDesign.addByTabsField();
					}
				}
			});
			
		},
		//将修改的TabsField重新加入
		addByTabsField:function(){
			var nos = tabAttrNos_merge+",";
			$("#tabsField_form input[name='tabAttrsId']").each(function(){
				 var no = $(this).val()+',';
				 nos = nos.replace(no,'');
			});
			nos=nos.substring(0,nos.length-1);
			if(nos!==""){
				$.post("attrs!find.action?attrsQueryDTO.attrNos="+nos,"",function(eavData){
					$.each(eavData.data,function(ind,val){
						var event=$('<div class="right_options" title="'+val.attrAsName+'" >'+val.attrAsName+'</div>');
						event.attr({
							attrType:val.attrType,
							label:val.attrAsName,
							attrNo:val.attrNo,
							attrName:val.attrName,
							requireds:val.attrIsNull,
							attrdataDictionary:val.attrdataDictionary,
							attrItemName:val.attrItemName,
							attrColumn:"1"//一列显示
						});
						event.prependTo("#customField_fieldset");
					});
				});
			}
		},
		removeByTabsField:function(){
			var tabAttrsId = $("#selectedTabAttrs input[name='tabAttrsId']");
			$.each(tabAttrsId,function(index,label_div){
				var attrno=$(label_div).val();
				$("#fromCustomCenter_right .right_options[attrno='"+attrno+"']").remove();
			});
		},
		tabOndbclick:function(tabId){
			opt ="merge";
			var url = "formCustomTabs!findFormCustomTabsByTabsId.action";
			$.post(url,"tabsId="+tabId, function(data){
				$("#tabs_id").val(data.tabId);
				$("#tabs_name").val(data.tabName);
				tabAttrNos_merge = data.tabAttrNos;
				$("#selectedTabAttrs").html(wstuo.sysMge.base64Util.decode(data.tabAttrsDecode));
				$("#tabAttrsConent").html(wstuo.sysMge.base64Util.decode(data.tabAttrsConentDecode));
				windows('tabsField_win', { width : 450 });
			});
		},
		deleteTabs:function(event,res){
			$("#tabsId_"+res).remove();
			var parent=$(event).parent().parent().parent();
			parent.remove();
			var url = "formCustomTabs!findFormCustomTabsByTabsId.action";
			$.post(url,"tabsId="+res, function(data){
				$.post("attrs!find.action?attrsQueryDTO.attrNos="+data.tabAttrNos,"",function(eavData){
					$.each(eavData.data,function(ind,val){
						var event=$('<div class="right_options" title="'+val.attrAsName+'" >'+val.attrAsName+'</div>');
						event.attr({
							attrType:val.attrType,
							label:val.attrAsName,
							attrNo:val.attrNo,
							attrName:val.attrName,
							requireds:val.attrIsNull,
							attrdataDictionary:val.attrdataDictionary,
							attrItemName:val.attrItemName,
							attrColumn:"1"//一列显示
						});
						event.prependTo("#customField_fieldset");
					});
				});
			});
		},
		findTabsFieldAttrNos:function(){
			var attrNos =[];
			var label_div_list = $("#customField_fieldset .right_options");
			$.each(label_div_list,function(label_div_i,label_div){
				var attrno=$(label_div).attr("attrno");
				attrNos.push(attrno)
			});
			return attrNos;
		},
		loadModule:function(){
			$("#formCustomType").html('');
			var url="module!findAll.action";
			$.post(url,function(dataDictionary){     
				$.each(dataDictionary,function(key,value){
					$("<option value='"+value.moduleName+"'>"+value.title+"</option>").appendTo("#formCustomType");
				});
			});
		},
		changeModule:function(module){
			if($("#dashboard_column:has(div)").length>0){
				msgConfirm(i18n.msg_msg,"自定义表单内容将被清空，确定要切换吗？",function(){
					$("#dashboard_column").empty();
					wstuo.customForm.customFormDesign.loadCustomField(module);
				},function(){
					$("#formCustomType").val(formCustomType);
				});
			}else{
				$("#dashboard_column").empty();
				wstuo.customForm.customFormDesign.loadCustomField(module);
			}
		},
		init:function(){
			wstuo.customForm.customFormDesign.loadModule();
			$("#tabsField").show();
			if(formCustomId!=""){
				wstuo.customForm.customFormDesign.findCustomForm();
			}else{
				wstuo.customForm.customFormDesign.loadCustomField('request');
				//$("#dashboard_column").load('wstuo/customForm/basicField.jsp',function(){});
				wstuo.customForm.customFormDesign.layoutInit();//初始化布局
			}
			//wstuo.customForm.customFormDesign.findSimilarFormCustom();
			
			$(document).keydown(function(event){
				//Ctrl+S 保存事件
				if(event!==null && event!==undefined){
					if (event.ctrlKey == true && event.keyCode == 83) {
						event.returnvalue = false;
						wstuo.customForm.customFormDesign.saveFormCustomEdit("Ctrl+S");
						return false;
					}
				}
			});
			/**浏览器窗口大小改变事件*/
			$(window).resize(function(){
				$.each($("#formField div[attrtype=Radio],#formField div[attrtype=Checkbox]"),function(ind,obj){
					wstuo.customForm.customFormDesign.fieldResize(obj);
				});
				wstuo.customForm.customFormDesign.resizeWith();
			});
			
			$("#tabsAttr_customField_select").click(function(){
				windows("tabsAttrDiv",{width:530,maxHeight:480,padding:3});
				var tabsField_attrNos = wstuo.customForm.customFormDesign.findTabsFieldAttrNos();
				if(tabsField_attrNos.length<=0){
					tabsField_attrNos = -1;
				}
				if(loadshowAttrsGrid==false){
					loadshowAttrsGrid=true;
					wstuo.customForm.customFormDesign.selectTabsAttrCustomField(tabsField_attrNos);
				}else{
					$('#tabsAttr_customFieldList').jqGrid('setGridParam', {
		                url: 'attrs!find.action?attrsQueryDTO.attrNos='+tabsField_attrNos,
		                page: 1
		            }).trigger('reloadGrid');
				}
				
			});
			$("#link_tabs_save_ok").click(function(){wstuo.customForm.customFormDesign.tabAttrsSave();});
		}
	}
	
}();
$(function(){wstuo.customForm.customFormDesign.init();});