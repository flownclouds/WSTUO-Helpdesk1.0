$package('wstuo.onlinelog');
$import("wstuo.user.userUtil");
//$import('itsm.itsop.selectCompany');
//$import('common.report.defaultTimeAndBind');
/**  
 * @author Van  
 * @constructor WSTO
 * @description 用户在线日志.
 * @date 2010-11-17
 * @since version 1.0 
 */
wstuo.onlinelog.userOnlineLog = function(){
	return {
		/**
		 * @description 创建在线列表
		 */
		 showUserOnlineGrid:function(){
				var params = $.extend({},jqGridParams, {	
					url:'log!userOnlinePage.action',
					colNames:['ID',i18n['title_backup_fileName'],i18n['title_backup_fileSize'],i18n['common_createTime'],i18n['common_download']],
					colModel:[
					          {name:'id',width:30,align:'center',sortable:false},
					          {name:'fileName',width:80,align:'center',sortable:false,formatter:function(cellvalue,options,rowOjbect){
									 return '<a href="javascript:wstuo.onlinelog.userOnlineLog.downloadOnlineLogFile(\''+cellvalue+'\')">'+cellvalue+'</a>';
								 }},
							  {name:'fileSize',width:80,align:'center',sortable:false},
							  {name:'time',width:100,align:'center',sortable:false},
							  {name:'fileName',width:80,align:'center',sortable:false,formatter:wstuo.onlinelog.userOnlineLog.lnkName}
							  ],
					jsonReader: $.extend(jqGridJsonReader, {id: "id"}),
					sortname:'id',
					multiselect:false,
					pager:'#userOnlineGridPager'
				});
				$("#userOnlineGrid").jqGrid(params);
				$("#userOnlineGrid").navGrid('#userOnlineGridPager',navGridParams);
				//列表操作项
				$("#t_userOnlineGrid").css(jqGridTopStyles);
				$("#t_userOnlineGrid").append($('#userOnlineGridToolbar').html());
				//自适应宽度
				setGridWidth("#userOnlineGrid",15);
		},
		downloadOnlineLogFile:function(fileName){
			 location.href="log!downloadOnlineLog.action?fileName="+fileName;
		 }
		 ,
		 lnkName:function(cellvalue,options,rowOjbect){
			 return '<a href="javascript:wstuo.onlinelog.userOnlineLog.downloadOnlineLogFile(\''+cellvalue+'\')" title="下载"><i class="glyphicon glyphicon-download-alt"></i></a>';
		 }
		 ,
		/**
		 * @description 选择用户
		 */
		selectUserOnlineUserOpenWinow:function(){
			wstuo.user.userUtil.selectUser('#useronlinelog_user_Name','','','loginName','-1');
		},
		/**
		 * @description 搜索在线用户
		 */
		searchUserOnline_do:function(){		
			if($('#searchUserOnlineLog form').form('validate')){
				//获得表单对象
				var sdata = $('#searchUserOnlineLog form').getForm();
				var postData = $('#userOnlineGrid').jqGrid('getGridParam', 'postData');       
				$.extend(postData, sdata);		
				var _url = 'useronlinelog!findPager.action';	
				$('#userOnlineGrid').jqGrid('setGridParam',{url:_url,page:1}).trigger('reloadGrid');
			}
		},
		
		/**
		 * @description 刷新在线用户用户列表
		 */
		refreshUserOnlineLogjqGrid:function(){
			$('#useronlinelog_keyWord').val("");
			$('#onlineTime').val("");
			$('#onlineEndTime').val("");
			$('#offlineTime').val("");
			$('#offlineEndTime').val("");
	
			//获得表单对象
			var sdata = $('#searchUserOnlineLog form').getForm();		
			var postData = $('#userOnlineGrid').jqGrid('getGridParam', 'postData');       
			$.extend(postData, sdata); 
			var _url = 'useronlinelog!findPager.action';	
			
			$('#userOnlineGrid').jqGrid('setGridParam',{url:_url,page:1}).trigger('reloadGrid');
		},
		/**
		 * 初始化
		 * @private
		 */
		init:function(){
			DatePicker97(['#zipStartTime','#zipEndTime']);
			$("#UserOnlineLog_loading").hide();
			$("#UserOnlineLog_content").show();
			wstuo.onlinelog.userOnlineLog.showUserOnlineGrid();
			$('#userOnlineGrid_search').click(function(){
				bindControl('#onlineTime,#onlineEndTime,#offlineTime,#offlineEndTime');
				windows('searchUserOnlineLog',{width:450,colse:function(){
					$('#searchUserOnlineLog').click();
				}});
			});
			$("#userOnlineGrid_download").click(function(){
				//bindControl('#zipStartTime,#zipEndTime');
				windows('zipUserOnlineLog',{width:450});
			});
			$("#userOnlineGrid_downloadzip").click(function(){
				startProcess();
				var startTime = $("#zipStartTime").val();
				var endTime = $("#zipEndTime").val();
				$.post("log!multiZip.action","zipType=onlineLog&startTime="+startTime+"&endTime="+endTime,function(data){
					if(data){
						location.href="log!downloadMultiZip.action?fileName="+data;
						endProcess();
						$.post("log!deleteNultiZip.action","fileName="+data);
					}
				});
			});
			
			
			$('#useronlinelog_user_Name_select').click(wstuo.onlinelog.userOnlineLog.selectUserOnlineUserOpenWinow);
			$('#userOnlineGrid_doSearch').click(wstuo.onlinelog.userOnlineLog.searchUserOnline_do);
			//选择公司
			$('#useronlinelog_companyName').click(function(){
				//itsm.itsop.selectCompany.openSelectCompanyWin('#useronlinelog_companyNo','#useronlinelog_companyName','','all');
			});
			//加载默认公司
			//common.security.defaultCompany.loadDefaultCompany('#useronlinelog_companyNo','#useronlinelog_companyName');
		}
	}
 }();
 $(document).ready(wstuo.onlinelog.userOnlineLog.init);