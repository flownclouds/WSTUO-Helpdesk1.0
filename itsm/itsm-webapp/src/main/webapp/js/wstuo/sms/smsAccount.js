$package('wstuo.sms');
$import('wstuo.sysMge.base64Util');
/**  
 * @author Van  
 * @constructor smsAccount
 * @description 短信账户设置主函数（公司信息）.
 * @date 2011-02-25
 * @since version 1.0 
 */
wstuo.sms.smsAccount = function() 
{
	this.tempVer = "";
	this.tempVerChar = "";
	return {
				
		/**
		 * @description 查询短信账户信息.
		 * @param smsInstance
		 */
		findSMSAccount:function(smsInstance){
			
			var url="sms!findSMSAccount.action";
			
			$.post(url,function(res){
				if(smsInstance=='' || smsInstance==res.smsInstance){
					if(res.smsInstance!=''){
						$('#smsAccountDTO_smsInstance').val($vl(res.smsInstance));	
					}
					if(res.smsInstance=='PanZhiSMSHelper'){
						$('#smsAccountDTO_orgId_tr').show();
					}
					
					$('#smsAccountDTO_said').val($vl(res.said));
					$('#smsAccountDTO_orgId').val($vl(res.orgId));
					$('#smsAccountDTO_userName').val($vl(res.userName));
					wstuo.sysMge.base64Util.setingPassword(
							$vl(res.pwd),'#smsAccountDTO_pwd','#smsAccountDTO_pwd_reality');
					tempVer = $('#smsAccountDTO_pwd_reality').val();
					tempVerChar = $('#smsAccountDTO_pwd').val();
				}
			});
		},
		
		
		/**
		 * @description 测试短信账户.
		 */
		testSMSAccount:function(){
			
			//if($('#SMSAccountForm').form('validate')){
				wstuo.sysMge.base64Util.encodePassword('#smsAccountDTO_pwd','#smsAccountDTO_pwd_reality');
				wstuo.sms.smsAccount.checkIsUpdate();
				var url="sms!testSMSAccount.action";
				var frm = $('#SMSAccountForm').serialize();
				$.post(url,frm, function(res){
					var msg='';
					if(res==true || res=='true'){	
						if($('#smsAccountDTO_smsInstance').val()=='PanZhiSMSHelper'){
							msg=i18n['msg_sms_server_ok'];
						}else{
							msg=i18n['msg_sms_success'];
						}
					}else{
						msg=i18n['msg_sms_failure'];
					}
					msgShow(msg,'show');
				});
			//}
		},
		/**
		 * @description 保存短信账户信息.
		 */
		saveSMSAccount:function(){
				//if($('#SMSAccountForm').form('validate')){
					wstuo.sysMge.base64Util.encodePassword('#smsAccountDTO_pwd','#smsAccountDTO_pwd_reality');
					wstuo.sms.smsAccount.checkIsUpdate();
					var urltest="sms!testSMSAccount.action";
					var frmtest = $('#SMSAccountForm').serialize();
					$.post(urltest,frmtest, function(Confirm){
						if(Confirm==true || Confirm=='true'){
							var frm = $('#SMSAccountDiv form').serialize();
							var sburl="sms!saveOrUpdateSMSAccount.action";
							$.post(sburl,frm, function(){				
								msgShow(i18n['msg_sms_accountConfigSuccessful'],'show');
								wstuo.sms.smsAccount.findSMSAccount();
							});
						}else{
							msgShow(i18n['msg_sms_accountConfigError'],'show');
						}
					});
				//}
		},
		checkIsUpdate:function(){
			var valPwd = $('#smsAccountDTO_pwd').val();
			//如果输入的密码是服务端返回来后编码的字符；
			if(tempVerChar !== "" && tempVerChar === valPwd ){
				$('#smsAccountDTO_pwd_reality').val(tempVer);
			}
		},
		
		
		/**
		 * 加载
		 */
		init: function(){
			wstuo.sms.smsAccount.findSMSAccount('');
			//动态创建事件
			$('#saveSMSAccountBtn').click(wstuo.sms.smsAccount.saveSMSAccount);
			$('#testSMSAccountBtn').click(wstuo.sms.smsAccount.testSMSAccount);
		}
		
		
	};

}();
//载入
$(document).ready(wstuo.sms.smsAccount.init);

