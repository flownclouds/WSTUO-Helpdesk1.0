$package('wstuo.tools');
/**  
 * @fileOverview 附件主函数
 * @author QXY
 * @version 1.0  
 */  
wstuo.tools.eventAttachment=function(){
	return {
		/**
		 * @description 获取附件
		 * @param eventAttachmentTable 显示附件div的id
		 * @param eno id
		 * @param eventType 模块类型
		 * @param delshow 是否可以删除
		 */
		showEventAttachment:function(eventAttachmentTable,eno,eventType,delshow){
			if(eno!=null && eno!=""){
				$('#'+eventAttachmentTable+' table tbody').html('');
				var url = 'eventAttachment!findAllEventAttachment.action';
				$.post(url,'eventAttachmentDto.eno='+eno+'&eventAttachmentDto.eventType='+eventType, function(res){
					var arr=res;
					if(arr!=null && arr.length>0){
						for(var i=0;i<arr.length;i++){
							var file=arr[i].url;
							var fileName=file.substring(file.lastIndexOf("/")+1);
							var fileFix=fileName.substring(fileName.indexOf(".")).toLowerCase();
							var iconUrl="<img src='../images/attachicons/";
							if(fileFix==".rar"){
								iconUrl=iconUrl+"rar.gif'";
							}
							if(fileFix==".zip"){
								iconUrl=iconUrl+"zip.gif'";
							}
							if(fileFix==".gif"||fileFix==".jpg"||fileFix==".bmp"||fileFix==".png"){
								iconUrl=iconUrl+"image.gif'";
							}
							if(fileFix==".doc"||fileFix==".docx"||fileFix==".xls"||fileFix==".ppt"||fileFix==".pptx"||fileFix==".xlsx"){
								iconUrl=iconUrl+"msoffice.gif'";
							}
							if(fileFix==".pdf"){
								iconUrl=iconUrl+"pdf.gif'";
							}
							if(fileFix==".swf"){
								iconUrl=iconUrl+"flash.gif'";
							}
							if(fileFix==".txt"){
								iconUrl=iconUrl+"text.gif'";
							}
							else{
								iconUrl=iconUrl+"unknown.gif'";
							}
							iconUrl=iconUrl+" width='14px' height='14px'/>&nbsp;";
							var attachmetstr="<tr id={id}>" +
									"<td>{no}</td>" +
									"<td style='text-align: left;' >{name}</td>" +
									"<td>{download}</td>" +
									"</tr>";
							attachmetstr=attachmetstr.replace('{id}',eventAttachmentTable+'eventAttachment_ID_'+arr[i].aid)
													 .replace('{no}',i*1+1)
													 .replace('{name}',iconUrl+'<a href=attachment!download.action?downloadAttachmentId='+arr[i].aid+' target=_blank>'+arr[i].attachmentName.substring(arr[i].attachmentName.lastIndexOf("/")+1)+'</a>')
													 .replace('{url}','<a href=attachment!download.action?downloadAttachmentId='+arr[i].aid+' target=_blank>'+arr[i].url+'</a>')
							if(delshow){
								attachmetstr=attachmetstr.replace('{download}','<a href=attachment!download.action?downloadAttachmentId='+arr[i].aid+' target=_blank>['+i18n['common_download']+']</a>'+
										 '&nbsp;&nbsp;<a href=javascript:wstuo.tools.eventAttachment.deleteEventAttachment("'+eventAttachmentTable+'",'+arr[i].aid+','+eno+')>['+i18n['deletes']+']</a>')
							}else{
								attachmetstr=attachmetstr.replace('{download}','<a href=attachment!download.action?downloadAttachmentId='+arr[i].aid+' target=_blank>['+i18n['common_download']+']</a>')
							}
							//$('#'+eventAttachmentTable+' table thead').remove();
							$('#'+eventAttachmentTable+' table tbody').append(attachmetstr)
						}
					}else{
						$('#'+eventAttachmentTable+' table thead').remove();
						$('#'+eventAttachmentTable+' table tbody').append("<tr><td style='color:red;font-size:16px' colspan='4'>"+i18n['noData']+"</td></tr>");
					}
				});
			}else{
				$('#'+eventAttachmentTable+' table thead').remove();
				$('#'+eventAttachmentTable+' table tbody').append("<tr><td style='color:red;font-size:16px' colspan='4'>"+i18n['noData']+"</td></tr>");
			}
			
		},
		/**
		 * @description 保存附件
		 * @param creator 创建人
		 * @param eventAttachmentTable 显示附件div的id
		 * @param eno 
		 * @param eventType 模块类型
		 * @param attachmentStr 包含附件信息的字符串
		 * @param delshow 是否可以删除
		 */
		saveEventAttachment:function(creator,eventAttachmentTable,eno,eventType,attachmentStr,delshow){
			$.post('eventAttachment!saveEventAttachment.action','eno='+eno+'&eventType='+eventType+'&attachmentStr='+$('#'+attachmentStr).val()+'&creator='+creator,function(){
				$('#'+attachmentStr).val('');
				wstuo.tools.eventAttachment.showEventAttachment(eventAttachmentTable,eno,eventType,delshow);
			})
		},
		/**
		 * @description 保存选择的已有附件
		 * @param aid 附件id
		 * @param eventAttachmentTable 显示附件div的id
		 * @param eno 
		 * @param eventType 模块类型
		 * @param attachmentStr 包含附件信息的字符串
		 * @param delshow 是否可以删除
		 */
		editEventAttachment:function(aid,eventAttachmentTable,eno,eventType,attachmentStr,delshow){
			if($('#'+eventAttachmentTable+' #'+eventAttachmentTable+'eventAttachment_ID_'+aid).length>0){
				msgAlert(i18n['attachment_have'],'error');
			}else{
				$.post('eventAttachment!saveEventAttachment.action','eno='+eno+'&eventType='+eventType+'&attachmentStr='+$('#'+attachmentStr).val()+'&ids='+aid,function(){
					$('#'+attachmentStr).val('');
					wstuo.tools.eventAttachment.showEventAttachment(eventAttachmentTable,eno,eventType,delshow);
				});
			}
		},
		/**
		 * @description 删除附件
		 * @param rowIds 附件列表id
		 * @param eno 
		 */
		deleteEventAttachment:function(eventAttachmentTable,rowIds,eno){
			msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirmDelete'],function(){
				var _param = $.param({'ids':rowIds,'eno':eno},true);
				$.post('eventAttachment!deleteEventAttachment.action',_param,function(){
					var event='tbody #'+eventAttachmentTable+'eventAttachment_ID_'+rowIds;
					var tbody = $(event).parent();
					$(event).remove();
					msgShow(i18n['deleteSuccess'],'show');
					if($(tbody).html()== ""){
						tbody.append("<tr><td style='color:red;font-size:16px' colspan='4'>"+i18n['noData']+"</td></tr>");
					}
				})
			});
		},
		/**
		 * @description edit附件
		 * @param aid 附件id
		 * @param eventAttachmentTable 显示附件div的id
		 * @param ciId 配置项id 
		 * @param eventType 模块类型
		 * @param attachmentStr 包含附件信息的字符串
		 * @param delshow 是否可以删除
		 */
		editAttachment:function(aid,eventAttachmentTable,ciId,eventType,attachmentStr,delshow){
			if($('#'+eventAttachmentTable+' #'+eventAttachmentTable+'eventAttachment_ID_'+aid).length>0){
				msgAlert(i18n['attachment_have'],'error');
			}else{
				$.post('ci!saveAttachment.action','ciId='+ciId+'&eventType='+eventType+'&aid='+aid,function(){
					$('#'+attachmentStr).val('');
					wstuo.tools.eventAttachment.showAttachment(eventAttachmentTable,ciId,eventType,delshow);
				})
			}
		},
		/**
		 * @description 配置项保存附件
		 * @param eventAttachmentTable 显示附件div的id
		 * @param ciId 配置项id 
		 * @param eventType 模块类型
		 * @param attachmentStr 包含附件信息的字符串
		 * @param delshow 是否可以删除
		 */
		saveAttachment:function(eventAttachmentTable,ciId,eventType,attachmentStr,delshow){
			$.post('ci!saveAttachment.action','ciId='+ciId+'&eventType='+eventType+'&attachmentStr='+$('#'+attachmentStr).val(),function(){
				$('#'+attachmentStr).val('');
				wstuo.tools.eventAttachment.showAttachment(eventAttachmentTable,ciId,eventType,delshow);
			})
		},
		/**
		 * @description 显示附件
		 * @param eventAttachmentTable 显示附件div的id
		 * @param ciId 配置项id 
		 * @param eventType 模块类型
		 * @param delshow 是否可以删除
		 */
		showAttachment:function(eventAttachmentTable,ciId,eventType,delshow){
			if(ciId!=null && ciId!=""){
				$('#'+eventAttachmentTable+' table tbody').html('');
				var url = 'ci!findAllAttachment.action';
				$.post(url,'ciId='+ciId+'&eventType='+eventType, function(res){
					var arr=res;
					if(arr!=null && arr.length>0){
						for(var i=0;i<arr.length;i++){
							var file=arr[i].url;
							var fileName=file.substring(file.lastIndexOf("/")+1);
							var fileFix=fileName.substring(fileName.indexOf(".")).toLowerCase();
							var iconUrl="<img src='../images/attachicons/";
							if(fileFix==".rar"){
								iconUrl=iconUrl+"rar.gif'";
							}
							if(fileFix==".zip"){
								iconUrl=iconUrl+"zip.gif'";
							}
							if(fileFix==".gif"||fileFix==".jpg"||fileFix==".bmp"||fileFix==".png"){
								iconUrl=iconUrl+"image.gif'";
							}
							if(fileFix==".doc"||fileFix==".docx"||fileFix==".xls"||fileFix==".ppt"||fileFix==".pptx"||fileFix==".xlsx"){
								iconUrl=iconUrl+"msoffice.gif'";
							}
							if(fileFix==".pdf"){
								iconUrl=iconUrl+"pdf.gif'";
							}
							if(fileFix==".swf"){
								iconUrl=iconUrl+"flash.gif'";
							}
							if(fileFix==".txt"){
								iconUrl=iconUrl+"text.gif'";
							}
							
							else{
								iconUrl=iconUrl+"unknown.gif'";
							}
							iconUrl=iconUrl+" width='14px' height='14px'/>&nbsp;";
							var attachmetstr="<tr id={id}>" +
										"<td>{no}</td>" +
										"<td style='text-align: left;' >{name}</td>" +
										"<td style='text-align: left;' >&nbsp;&nbsp;{url}</td>" +
										"<td>{download}</td>" +
										"</tr>";
							attachmetstr=attachmetstr.replace('{id}',''+eventAttachmentTable+'eventAttachment_ID_'+arr[i].aid)
													 .replace('{no}',i*1+1)
													 .replace('{name}',iconUrl+'<a href=attachment!download.action?downloadAttachmentId='+arr[i].aid+' target=_blank>'+arr[i].attachmentName.substring(arr[i].attachmentName.lastIndexOf("/")+1)+'</a>')
													 .replace('{url}','<a href=attachment!download.action?downloadAttachmentId='+arr[i].aid+' target=_blank>'+arr[i].url+'</a>')
							if(delshow){
								attachmetstr=attachmetstr.replace('{download}','<a href=attachment!download.action?downloadAttachmentId='+arr[i].aid+' target=_blank>['+i18n['common_download']+']</a>'+
										 '&nbsp;&nbsp;<a href=javascript:wstuo.tools.eventAttachment.deleteAttachement("'+eventAttachmentTable+'",'+arr[i].aid+','+ciId+')>['+i18n['deletes']+']</a>')
							}else{
								attachmetstr=attachmetstr.replace('{download}','<a href=attachment!download.action?downloadAttachmentId='+arr[i].aid+' target=_blank>['+i18n['common_download']+']</a>')
							}
							
							$('#'+eventAttachmentTable+' table tbody').append(attachmetstr)

						}
					}else{
						
						$('#'+eventAttachmentTable+' table thead').remove();
						$('#'+eventAttachmentTable+' table tbody').append("<tr><td style='color:red;font-size:16px' colspan='4'>"+i18n['noData']+"</td></tr>");
						
					}
				});
			}else{
				$('#'+eventAttachmentTable+' table thead').remove();
				$('#'+eventAttachmentTable+' table tbody').append("<tr><td style='color:red;font-size:16px' colspan='4'>"+i18n['noData']+"</td></tr>");
			}
			
		},
		/**
		 * @description 删除附件
		 * @param aid 附件id 
		 * @param kid 配置项id
		 */
		deleteAttachement:function(eventAttachmentTable,aid,kid){
			msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirmDelete'],function(){
				var _url = "ci!deleteAttachement.action?ciEditId="+kid+"&aid="+aid;
				$.post(_url,function(){
					$('tbody #'+eventAttachmentTable+'eventAttachment_ID_'+aid).remove();
					msgShow(i18n['deleteSuccess'],'show');
				});
			});
		},
		init:function(){
		}
	}
}();
//$(document).ready(wstuo.tools.eventAttachment.init);


