$package('wstuo.tools');

 /**  
 * @author wstuo 
 * @constructor 获取附件图标
 * @description 获取附件图标
 * @since version 1.0 
 */
wstuo.tools.attachIcon=function(){
	
	
	return {
		/**
		 * @description 根据附件后缀获取对应图标
		 * @param 附件名称
		 */
		getIcon:function(attName){
			var fileFix=attName.substring(attName.indexOf(".")).toLowerCase();
			var iconUrl="<img src='../images/attachicons/";
			
			if(fileFix==".rar"){
				iconUrl=iconUrl+"rar.gif'";
			}
			else if(fileFix==".zip"){
				iconUrl=iconUrl+"zip.gif'";
			}
			else if(fileFix==".gif"||fileFix==".jpg"||fileFix==".bmp"||fileFix==".png"){
				iconUrl=iconUrl+"image.gif'";
			}
			else if(fileFix==".doc"||fileFix==".docx"||fileFix==".xls"||fileFix==".ppt"||fileFix==".pptx"||fileFix==".xlsx"){
				iconUrl=iconUrl+"msoffice.gif'";
			}
			else if(fileFix==".pdf"){
				iconUrl=iconUrl+"pdf.gif'";
			}
			else if(fileFix==".swf"){
				iconUrl=iconUrl+"flash.gif'";
			}
			else if(fileFix==".txt"){
				iconUrl=iconUrl+"text.gif'";
			}
			else{
				iconUrl=iconUrl+"unknown.gif'";
			}
			
			iconUrl=iconUrl+" width='14px' height='14px' align='top'/>&nbsp;";
			
			return iconUrl;
			
		}
	};
}();


