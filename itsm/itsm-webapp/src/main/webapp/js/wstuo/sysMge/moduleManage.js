$package('wstuo.sysMge');

/**  
 * @author Wstuo 
 * @description 模块管理主函数.
 * @since version 1.0 
 */
wstuo.sysMge.moduleManage = function() {

	this.opt='saveModule';
	return {
		/**
		 * @description 模板管理格式化.
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 */
		moduleManageGridFormatter:function(cell,opt,data){
			var enable="";
			if(data.dataFlag==99){
				enable='&nbsp;&nbsp;&nbsp;<a href="javascript:wstuo.sysMge.moduleManage.unInstall(\''+(data.moduleId)+'\')"  title='+i18n.common_enable+'>'+
				'<i class="glyphicon glyphicon-ok-circle"></i></a>';
			}else if(data.dataFlag==0){
				enable='&nbsp;&nbsp;&nbsp;<a href="javascript:wstuo.sysMge.moduleManage.unInstall(\''+(data.moduleId)+'\')"  title='+i18n.common_disable+'>'+
				'<i class="glyphicon glyphicon-ban-circle"></i></a>';
			}
			return '<div style="padding:0px">'+
			'&nbsp;&nbsp;&nbsp;<a href="javascript:wstuo.sysMge.moduleManage.editModuleInLine(\''+(data.moduleId)+'\')"  title='+i18n.common_edit+'>'+
			'<i class="glyphicon glyphicon-edit"></i></a>'+enable+
			'</div>';
		},
		/**
		 * @description 加载模块列表.
		 */
		showModuleManageGrid:function(){
			var params = $.extend({},jqGridParams, {
				url:'module!findPager.action',
				colNames:['ID','模块名称','描述','模块编码','标识','pid',i18n.common_createTime,i18n.sort,i18n.common_action],
				colModel:[
				          {name:'moduleId',width:30,align:'center'},
				          {name:'title',width:100,align:'center'},
				          {name:'description',width:180,align:'center'},
				          {name:'moduleName',align:'center',width:180},
				          {name:'dataFlag',hidden:true},
				          {name:'pid',hidden:true},
				          {name:'createTime',width:120,align:'center'},
				          {name:'showSort',width:30,align:'center'},
				          {name:'act', width:80,align:'center',sortable:false,formatter:wstuo.sysMge.moduleManage.moduleManageGridFormatter}
						  ],
				jsonReader: $.extend(jqGridJsonReader, {id:"moduleId"}),
				sortname:'moduleId',
				pager:'#moduleManageGridPager'
				});
				$("#moduleManageGrid").jqGrid(params);
				$("#moduleManageGrid").navGrid('#moduleManageGridPager',navGridParams);
				//列表操作项
				$("#t_moduleManageGrid").css(jqGridTopStyles);
				$("#t_moduleManageGrid").append($('#moduleManageGridToolbar').html());
				
				//自适应宽度
				setGridWidth("#moduleManageGrid",20);
				
		},
		/**
		 * 打开安装窗口
		 */
		addopenWindow:function(){
			opt='saveModule';
			resetForm("#editModuleDiv form");
			windows('editModuleDiv',{width: 400});
		},
		/**
		 * 搜索窗口
		 */
		search_openWindow:function(){
			windows('searchModuleDiv',{width:400,modal: false});
		},
		/**
		 * 分页查询模块
		 */
		doSearch:function(){
			var _url = 'module!findPager.action';
			var sdata = $('#searchModuleDiv form').getForm();
			var postData = $("#moduleManageGrid").jqGrid("getGridParam", "postData");     
			$.extend(postData, sdata);
			$('#moduleManageGrid').jqGrid('setGridParam',{page:1,url:_url}).trigger('reloadGrid');
		},
		/**
		 * 卸载
		 */
		unInstall:function(id){
			var rowData=$('#moduleManageGrid').getRowData(id);
			var enable="";
			if(rowData.dataFlag==99){
				enable='确认启用该模块（'+rowData.title+'）吗？';
			}else if(rowData.dataFlag==0){
				enable='确认禁用该模块（'+rowData.title+'）吗？';
			}else {
				msgAlert("该模块禁止【禁用】操作！");return false;
			}
			msgConfirm(i18n['msg_msg'],'<br/>'+enable,function(){
				wstuo.sysMge.moduleManage.doUnInstall(id);
			});
		},
		unInstall_aff:function(){
			checkBeforeEditGrid("#moduleManageGrid",function(rowData){
				var enable="";
				if(rowData.dataFlag==99){
					enable='确认启用该模块（'+rowData.title+'）吗？';
				}else if(rowData.dataFlag==0){
					enable='确认禁用该模块（'+rowData.title+'）吗？';
				}else {
					msgAlert("该模块禁止[禁用]操作！");return false;
				}
				msgConfirm(i18n['msg_msg'],'<br/>'+enable,function(){
					wstuo.sysMge.moduleManage.doUnInstall(rowData.moduleId);
				});
			});
		},
		/**
		 * 卸载模块
		 * @param id 编号
		 */
		doUnInstall:function(id){
			startProcess();
			$.post("module!moduleDisable.action",{"dto.moduleId":id},function(){
				endProcess();
				msgAlert(i18n.common_operation_success,"info");
				$('#moduleManageGrid').trigger('reloadGrid');
			})
		},
		/**
		 * 行内编辑
		 * @param rowId 行编号
		 */
		editModuleInLine:function(rowId){
			
			var rowData=$('#moduleManageGrid').getRowData(rowId);
			wstuo.sysMge.moduleManage.editOpt(rowData);
		},
		/**
		 * 修改显示
		 */
		edit_openWindow:function(){
			opt='editModule';
			resetForm("#editModuleDiv form");
			checkBeforeEditGrid("#moduleManageGrid",wstuo.sysMge.moduleManage.editOpt);
		},
		editOpt:function(rowData){
			$("#editModuleId").val(rowData.moduleId);
			$("#editShowSort").val(rowData.showSort);
			$("#editTitle").val(rowData.title);
			$("#editmoduleName").val(rowData.moduleName);
			$("#editmoduleName").attr("readonly","readonly");
			$("#editdescription").val(rowData.description);
			$("#editPid").val(rowData.pid);
			windows('editModuleDiv',{width: 400});
		},
		/**
		 * 修改模块
		 */
		editModule:function(){
			var frm = $('#editModuleDiv form').serialize();
			$.post("module!"+opt+".action",frm,function(){
				$('#editModuleDiv').dialog('close');
				$('#moduleManageGrid').trigger('reloadGrid');
				msgAlert(i18n.common_operation_success,"info");
			});
		},
		getProcess:function(){
			$('#editPid').html('');
			$('<option value="">--'+i18n['pleaseSelect']+'--</option>').appendTo('#editPid');
			$.post('jbpm!findPageProcessDefinitions.action?rows=10000',function(process){
				if(process!=null){
					var _data = process.data;
					if(_data.length>0){
						for(var i=0;i<_data.length;i++){
							$('<option value="'+_data[i].id+'">'+_data[i].name+'(Ver:'+_data[i].version+')</option>').appendTo('#editPid');
						}
					}
				}
			});
		},
		/**
		 * 初始化加载
		 */
		init:function(){
			//加载列表editPid
			wstuo.sysMge.moduleManage.showModuleManageGrid();
			wstuo.sysMge.moduleManage.getProcess();
			$("#searchModuleBtn_OK").click(function(){
				wstuo.sysMge.moduleManage.doSearch();
			});
		}
	};
}();


//载入
$(document).ready(wstuo.sysMge.moduleManage.init);