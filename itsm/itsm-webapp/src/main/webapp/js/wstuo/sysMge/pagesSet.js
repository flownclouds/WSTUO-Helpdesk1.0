$package('wstuo.sysMge')
/**  
 * @author QXY  
 * @constructor WSTO
 * @description "pagesSet"
 * @since version 1.0 
 */ 
wstuo.sysMge.pagesSet=function(){
	return {
		/**
		 * 更新
		 */
		update:function(){
			var i=$('#pagesSysName').val();
			
			var url="pagesSet!updatePagesSet.action";
			$.post(url,{'entity.paName':i},function(data){
			});
		
		},
		/**
		 * 加载logo
		 */
		loadImg:function(){
			var url='pagesSet!showPagesSet.action';
			$.post(url,function(data){
				$('#showLoginLogo').html('<img src="organization!getImageUrlforimageStream.action?imgFilePath='+data.imgname+'" width="60px" height="30px" />');
			})
		}
	}
}();