$package('wstuo.slaMge');
$import('wstuo.slaMge.slaDetail_ruleGrid_Operation');
$import('wstuo.rules.ruleCM');
 /**  
 * @author WSTUO 
 * @constructor SLAServiceManage
 * @description SLA详细信息主函数.

 */

wstuo.slaMge.slaDetail_ruleGrid=function(){
	
	
	return{

		
		/**
		 * @description 时间格式化
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 * @private
		 */
		slaTimeFormatter:function(cell,event,data){
		
			return cell.replace("DD",i18n['label_slaRule_days'])
			.replace("HH",i18n['label_slaRule_hours'])
			.replace("MM",i18n['label_slaRule_minutes']);
		},
		
		
		/**
		 * @description 系统数据显示格式化
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 * @private
		 */
		dataFlagFormatter:function(cell,event,data){
			
			if(data.dataFlag==1){
				return "<span style='color:#ff0000'>[System]</span>&nbsp;"+data.ruleName;
			}else{
				return data.ruleName;
			}
		},
		
		/**
		 * @description 加载SLA规则列表.
		 */
		showSLARuleGrid:function(){
	
			var params = $.extend({},jqGridParams, {	
			url:'slaRule!find.action?contractNo='+_contractNo,
			colNames:[i18n['title_sla_name'],i18n['rule_salience'],i18n['title_sla_requestTime'],i18n['title_sla_completeTime'],'','',''],
			colModel:[
					  {name:'ruleNamePanel',width:80,align:'center',index:'ruleName',formatter:wstuo.slaMge.slaDetail_ruleGrid.dataFlagFormatter},
					  {name:'salience',width:25,align:'center'},
					  {name:'showRespondTime',width:30,align:'center',sortable:false,formatter:wstuo.slaMge.slaDetail_ruleGrid.slaTimeFormatter},
					  {name:'showFinishTime',width:30,align:'center',sortable:false,formatter:wstuo.slaMge.slaDetail_ruleGrid.slaTimeFormatter},
					  {name:'ruleNo',hidden:true},
					  {name:'dataFlag',hidden:true},
					  {name:'ruleName',hidden:true}
					  ],
			jsonReader: $.extend(jqGridJsonReader, {id:"ruleNo"}),
			sortname:'ruleNo',
			pager:'#slaDetail_ruleGridPager'
			});
			
			$("#slaDetail_ruleGrid").jqGrid(params);
			$("#slaDetail_ruleGrid").navGrid('#slaDetail_ruleGridPager',navGridParams);
			//列表操作项
			$("#t_slaDetail_ruleGrid").css(jqGridTopStyles);
			$("#t_slaDetail_ruleGrid").append($('#slaDetail_ruleGridToolbar').html());
			//自适应宽度
			setGridWidth("#slaDetail_ruleGrid",15);
			slaDetailGrids.push('#slaDetail_ruleGrid');
			
			
		},
		
	
		/**
		 * @description 删除SLA规则.
		 */
		deleteSLARule:function(){
	
			checkBeforeDeleteGrid('#slaDetail_ruleGrid',function(rowIds){
				
				var _param = $.param({'ruleNos':rowIds},true);
				$.post("slaRule!delete.action",_param,function(){
					$("#slaDetail_ruleGrid").trigger('reloadGrid');
					msgShow(i18n['msg_deleteSuccessful'],'show');
				},"json");
				
			});
		},
		/**
		 * @description 执行SLA规则导入
		 */
		doImport:function(){
			
			$.ajaxFileUpload({
	            url:'slaRule!importSLARule.action',
	            secureuri:false,
	            fileElementId:'importFile', 
	            dataType:'json',
	            success: function(data,status){
					$('#index_import_excel_window').dialog('close');
					$('#slaDetail_ruleGrid').trigger('reloadGrid');
	            	msgShow(i18n['msg_dc_dataImportSuccessful'],'show');
	            	resetForm('#index_import_excel_window form');
	            }
	      });
		},
		
		/**
		 * @description 执行导入.
		 */
		doImport_drl:function(){
			
			$.ajaxFileUpload({
	            url:'slaRule!importSLARule_drl.action?contractNo='+_contractNo,
	            secureuri:false,
	            fileElementId:'importFile_drl', 
	            dataType:'json',
	            success: function(data,status){
				
				
					$('#index_import_drl_window').dialog('close');
					$('#rulesGrid').trigger('reloadGrid');
	            	resetForm('#index_import_drl_window form');
	            	
	            	var msg="";
	            	if(data=="success"){
						msg=i18n['msg_dc_dataImportSuccessful'];
					}else{
						msg=i18n["msg_dc_importFailure"];
					}
	            	
	            	msgShow(msg,'show');
	            }
	      });
		},
		
		/**
		 * @description 初始化
		 */
		init:function(){
			
			
			wstuo.rules.ruleCM.loadType("requestFit");
			
			wstuo.slaMge.slaDetail_ruleGrid.showSLARuleGrid();
			
			wstuo.slaMge.slaDetail_ruleGrid_Operation.addSLARule_ruleTermSet();
			$('#rule_salience_default').hide();
			$('#slaRuleSalience').show();
			//绑定事件
			$('#slaDetail_ruleGrid_add').click(wstuo.slaMge.slaDetail_ruleGrid_Operation.addSLARuleOpenWindow);
			$('#slaDetail_ruleGrid_edit').click(wstuo.slaMge.slaDetail_ruleGrid_Operation.editSLARuleOpenWindow);
			$('#slaDetail_ruleGrid_delete').click(wstuo.slaMge.slaDetail_ruleGrid.deleteSLARule);
			
			//导出
			$('#SLARuleExport').click(function(){
				
				window.location='slaRule!exportSLARule.action?contractNo='+_contractNo;
			});
			
			//导入数据
			$('#SLARuleImport').click(function(){
				windows('index_import_excel_window',{width:400});
				$("#index_import_confirm").unbind(); //清空事件      				
				$('#index_import_confirm').click(wstuo.slaMge.slaDetail_ruleGrid.doImport);
			});
			
			//导出
			$('#SLARuleExport_drl').click(function(){
				window.location='callBusinessRule!exportRules.action?rulePackageNo='+_rulePackageNo;
			});
			
			//导入数据
			$('#SLARuleImport_drl').click(function(){
				windows('index_import_drl_window',{width:400});
				$("#index_import_drl_confirm").unbind(); //清空事件      				
				$('#index_import_drl_confirm').click(wstuo.slaMge.slaDetail_ruleGrid.doImport_drl);
			});
			
			
			$('#addSLARule_team').change(function(){		
				wstuo.slaMge.slaDetail_ruleGrid_Operation.setSLAPVHTML();
				wstuo.slaMge.slaDetail_ruleGrid_Operation.addSLARule_setTerm();
			});
			
			$('#slaDetail_ruleGrid_addRuleToList').click(wstuo.slaMge.slaDetail_ruleGrid_Operation.addToSLARuleList);
			$('#slaDetail_ruleGrid_saveRule').click(function(){
				$('#slaDetail_ruleGrid_saveRule').focus();
				wstuo.slaMge.slaDetail_ruleGrid_Operation.saveSLARule();
			});						
			
		}
	};

 }();
