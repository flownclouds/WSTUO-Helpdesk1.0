 /**  
 * @author WSTUO 
 * @constructor flowPropertyMain
 * @description 流程流转验证

 **/

var validResult = false;
/**
 * 解决方案不能为空验证
 * @returns {Boolean}
 */
function SolutionsNotEmptyValidate(){
	var sl=$('#request_detail_solutions').val();
	if(sl!=undefined && sl.replace(/ /g,'')==''){
		$('#requestDetailsTab').tabs('select', i18n['label_solutions']);
		msgAlert(i18n['msg_input_solution'],'info');
		$('#process_handle_window').dialog('close');
		return false;
	}else{
		return true;
	}
}


/**
 * 问题解决方案不能为空验证
 * @returns {Boolean}
 */
function ProblemSolutionsNotEmptyValidate(){
	var sl=$('#problem_details_solutions').val();
	if(sl.replace(/ /g,'')==''){
		$('#problemDetailsMainTab').tabs('select', i18n['label_solutions']);
		msgAlert(i18n['msg_input_problemn_solution'],'info');
		$('#problemDetails_process_handle_window').dialog('close');
		return false;
	}else{
		return true;
	}
}



/**
 * 变更审批成员不允许为空验证
 * @returns {String}
 */
function ChangeApproverNotNullValidate(){
	$('#changeDetails_process_handle_window').dialog('close');
	return 'ChangeApproverNotNullValidate';
}


/**
 * 变更计划不能为空验证
 * @returns {Boolean}
 */
function ChangePlanNotNullValidate(){
	if($('#changeInfo_effect_text').val()=='' 
			&& $('#changeInfo_onLinePlan_text').val()==''
			&& $('#changeInfo_returnPlan_text').val()==''
			&& $('#changeInfo_checklist_text').val()==''){
		msgAlert(i18n['err_changePlanNotNull'],'info');
		$('#changeDetails_process_handle_window').dialog('close');
		$('#changeInfoTab').tabs('select', i18n['title_changePlan']);
		return false;
	}else{
		return true;
	}
}

/**
 * 发布关联的变更全部关闭验证
 * @returns {Boolean}
 */
function ReleaseRelatedChangeAllCloseValidate(){
	var eno=$('#releaseDetails_eno').val();
	var result = true;
	$.ajax({
		type:'post',
		async:false,
		url:'release!checkChangeProessClose.action?eno='+eno,
		success:function (msg){
			if(msg){
				$('#flow_handle_win').dialog('close');
				$('#releaseDetailsTab').tabs('select', i18n['title_release_relatedChange']);
				msgAlert(i18n['msg_releaseRelatedChangeAllCloseValidateFailure'],'info');
				result = false;
			}else
				result = true;
		}
	});
		return result;
	}

/**
 * 变更：流程指派的技术员不能与变更指派的技术员一致验证
 * @returns {Boolean}
 */
function FlowAssignUnEqualChangeAssignValidate(){
	if($('#flow_handle_assigneeNo').val()==$('#changeInfo_technicianNo').val()){
		msgAlert(i18n['tip_flowAssign_unEqual_changeAssign'],'info');
		return false; 
	}else{
		return true;
	}
}

/**
 * 判断分类、影响范围、紧急度必填项验证
 * @returns {Boolean}
 */
function RequestRequiredVerification(){
	if($('#requestDetails_requestCategoryNo').val()!='' && 
			$('#requestDetails_effectRange').val()!='' && 
			$('#requestDetails_seriousness').val()!=''){
		return true;
	}else{
		msgAlert('分类、影响范围、紧急度不允许为空!','info');
		return false;
	}
}



