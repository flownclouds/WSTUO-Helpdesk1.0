$package('wstuo.jbpmMge');
/**  
 * @author WSTUO
 * @constructor mailReturn
 * @description 流程邮件处理

 */  
wstuo.jbpmMge.mailReturn = function() {
	
	return {
		//根据当前下一步节点属性设置流程任务框属性
		setFlowHandleByNextNode:function(data,module){
			if(data!=null){
				$('#flow_mailHandle_activityType').val(data.activityType);
				$('#flow_mailHandle_assignType').val($vl(data.assignType));
				$('#flow_mailHandle_variablesAssigneeType').val(data.variablesAssigneeType);
				$('#flow_mailHandle_leaderNum').val(data.leaderNum);
				$('#flow_mailHandle_variablesAssigneeGroupNo').val(data.variablesAssigneeGroupNo);
				$('#flow_mailHandle_variablesAssigneeGroupName').val(data.variablesAssigneeGroupName);
				//验证方法
				$('#flow_mailHandle_validMethod').val($vl(data.validMethod));
			}
		},
		//根据当前节点属性设置流程任务框属性
		setFlowHandleByCurrentNode:function(data){
			if(data!==null){
				//处理备注是否要验证
				if(data.remarkRequired===true || data.remarkRequired=='true'){
					$('#flow_mailHandle_remark').validatebox({
						required:true
					});
				}else{
					$('#flow_mailHandle_remark').validatebox({
						required:false
					});
				}
				$('#flow_mailHandle_approverTask').val(data.approverTask);
				$('#flow_mailHandle_mailHandlingProcess').val(data.mailHandlingProcess);
			}
		},
		/**
		 * 加载流程处理信息
		 * @param module 对应模块
		 * @param eno 编号eno
		 * @param pid 流程pid
		 * @param outcome 流程出口
		 * @param taskId 流程taskId
		 * @param taskName 任务名称
		 * @param flowActivityId 流程属性Id
		 */
		loadFlowHandle:function(module,eno,pid,outcome,taskId,taskName,flowActivityId){
			//清空处理备注
			$('#flow_mailHandle_remark').val('');
			//任务ID
			$('#flow_mailHandle_taskId').val(taskId);
			//流程出口
			$('#flow_mailHandle_nextStep').val(outcome);
			//流程活动ID
			$('#flow_mailHandle_activity_id').val(flowActivityId);
			//流程PID
			$('#flow_mailHandle_pid').val(pid);
			$('#flow_mailHandle_eno').val(eno);
			$('#flow_mailHandle_module').val(module);
			var frm ={"processHandleDTO.pid":pid,"processHandleDTO.taskName":taskName,
					"processHandleDTO.taskId":taskId,"processHandleDTO.outcome":outcome,
					"processHandleDTO.eno":eno,"processHandleDTO.module":module};
			$.post(module+'!findProcessAssignDTO.action',frm,function(res){
				$('#flow_mailHandle_assigneeGroupNo').val($vl(res.groupId));
				$('#flow_mailHandle_assigneeGroupName').val($vl(res.groupName));
				$('#flow_mailHandle_assigneeNo').val($vl(res.assigneeId));
				$('#flow_mailHandle_assigneeName').val($vl(res.assigneeName));
				wstuo.jbpmMge.mailReturn.setFlowHandleByCurrentNode(res.flowActivityDTO);
				wstuo.jbpmMge.mailReturn.setFlowHandleByNextNode(res.nextFlowActivityDTO,module);
				$('#processMailHandleButton').unbind().click(function(){wstuo.jbpmMge.mailReturn.processMailHandle(module);});
			});
		},
		/**
		 * 流程处理
		 * @param module 对应模块
		 */
		processMailHandle:function(module){
			$.post("jbpm!ifExistTaskId.action","taskId="+mailTaskId,function(res){
				if(res){
					$('#flow_mailHandle_div').removeAttr("style");
					$('#mailHandingRepeatDiv').attr("style","display:none;");
					var validMethod = $('#flow_mailHandle_validMethod').val();
					var var_group=$('#flow_mailHandle_assigneeGroupName').val();
					var var_assignee=$('#flow_mailHandle_assigneeName').val();
					var dis=$('#flow_mailHandle_group_tr').css('display');
					var dis2=$('#flow_mailHandle_assignee_tr').css('display');
					var frm = $('#flow_mailHandle_from').serialize();
					startProcess();
					$.post(module+'!processHandle.action',frm,function(){
						//隐藏进程
						endProcess();
						msgConfirm(i18n.tip,'<br/>'+i18n.lable_mail_taskCompletionifCloseWindow,function(){
							window.close();
						});
					});
				}else{
					$('#mailHandingRepeatDiv').removeAttr("style");
					$('#flow_mailHandle_div').attr("style","display:none;");
				}
			});
				
		},
		/**
		 * 加载流程详细
		 * @param mailPid 流程pid
		 */
		loading:function(mailPid){
			$.post("jbpm!processDetail.action","pid="+mailPid,function(data){
				for ( var i = 0; i < data.length; i++) {
					var processDetail = data[i];
					for ( var j = 0; j < processDetail.flowTaskDTO.length; j++) {
						var flowTask = processDetail.flowTaskDTO[j];
						$("#mailHandingBaseInfo").append(flowTask.taskName+" ——> "+flowTask.outcomes[ocIndex]);
						wstuo.jbpmMge.mailReturn.loadFlowHandle(moduleType,mailEno,mailPid,flowTask.outcomes[ocIndex],flowTask.taskId,flowTask.taskName,flowTask.flowActivityDTO.id);
					}
				}
			});
			
		},
		/**
		 * 加载请求信息
		 * @param mailEno 编号eno
		 */
		loadingRequestInfo:function(mailEno){
			var _url="";
			if(moduleType=="request"){
				_url='request!showRequestInfo.action';
			}else if(moduleType=="problem"){
				_url='problem!problemToPrint.action';
			}else if(moduleType=="change"){
				_url='change!changeToPrint.action';
			}else if(moduleType=="release"){
				_url='release!findById.action';
			}
			$.post(_url,'eno='+mailEno,function(data){
				if(moduleType=="request"){
					$('#flow_mailHandle_Code').html(data.requestCode);
					$('#flow_mailHandle_creator').html(data.fullName);
				}else if(moduleType=="problem"){
					$('#flow_mailHandle_Code').html(data.problemNo);
					$('#flow_mailHandle_creator').html(data.createdByName);
				}else if(moduleType=="change"){
					$('#flow_mailHandle_Code').html(data.changeNo);
					$('#flow_mailHandle_creator').html(data.creator);
				}else if(moduleType=="release"){
					$('#flow_mailHandle_Code').html(data.eventCode);
					$('#flow_mailHandle_creator').html(data.createdByName);
				}
				$('#flow_mailHandle_title').html(data.etitle);
				$('#flow_mailHandle_edesc').html(data.edesc);
			});
		},
		/**
		 * 检查处理人是否存在
		 */
		checkPower:function(){
			$.post("user!findUserByRandomNum.action","userDto.randomId="+random,function(res){
				if(res!==null){
					$('#flow_mailHandle_operator').val(res.loginName);
					$.post("jbpm!ifExistTaskId.action","taskId="+mailTaskId,function(bool){
						if(bool){
							$('#flow_mailHandle_div').removeAttr("style");
							$('#mailHandingRepeatDiv').attr("style","display:none;");
							wstuo.jbpmMge.mailReturn.loadingRequestInfo(mailEno);
							wstuo.jbpmMge.mailReturn.loading(mailPid);
						}else{
							$('#mailHandingRepeatDiv').removeAttr("style");
							$('#flow_mailHandle_div').attr("style","display:none;");
						}
					});
				}else{
					window.open("../../../pages/errors/403.jsp");
					window.close();
				}
			});
		},
		/**
		 * 初始化
		 * @private
		 */
		init:function(){
			wstuo.jbpmMge.mailReturn.checkPower();
		}
	}
}();
//载入
$(document).ready(wstuo.jbpmMge.mailReturn.init);