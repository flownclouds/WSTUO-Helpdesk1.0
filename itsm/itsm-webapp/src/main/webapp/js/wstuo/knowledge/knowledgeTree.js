$package('wstuo.knowledge');

/**  
 * @author Van  
 * @constructor createKnowledgeCategoryTree
 * @description 知识库页面的树结构主函数
 * @date 2010-11-17
 * @since version 1.0 
 */
wstuo.knowledge.knowledgeTree=function(){
	
	return {
		
		/**
		 * 显示知识库分类树.
		 * @param treeDIV 树结构所在的div：$(treeDIV)
		 * @param selectNode 选择节点的回调函数
		 */
		showKnowledgeTree:function(treeDIV,selectNode){
			
			 $(treeDIV).jstree({
					json_data: {
						ajax: {
						  type:"post",
						  url : "event!getCategoryTree.action?num="+category_num,
					      data:function(n){
					    	  return {'types': 'Knowledge','parentEventId':n.attr ? n.attr("id").replace("node_",""):0};//types is in action
					      },
		                  dataType: 'json',
		                  cache:false
					 	}
					},
				"plugins" : [ "themes", "json_data", "ui", "crrm","dnd", "search", "types", "hotkeys"]
			})
			//.bind('loaded.jstree', function(e,data){data.inst.open_all(-1);})
			.bind("select_node.jstree", function(e,data){
				selectNode(e,data);
			 });
		},
		
		/**
		 * 显示知识库服务目录
		 * @param treeDIV 树结构所在的div：$(treeDIV)
		 * @param selectNode 选择节点的回调函数
		 */
		showKnowledgeServiceTree:function(treeDIV,selectNode){
			 $(treeDIV).jstree({
					json_data: {
						ajax: {
						  type:"post",
						  url : "event!getCategoryTree.action?num="+category_num,
					      data:function(n){
					    	  return {'types': 'Service','parentEventId':n.attr ? n.attr("id").replace("node_",""):0};//types is in action
					      },
		                  dataType: 'json',
		                  cache:false
					 	}
					},
				"plugins" : ["themes", "json_data","cookies", "ui", "crrm"]
			})
			.bind("select_node.jstree", function(e,data){
				selectNode(e,data);
			 });
		},
		/**
		 * 显示知识库服务目录
		 * @param treeDIV 树结构所在的div：$(treeDIV)
		 * @param selectNode 选择节点的回调函数
		 */
		showKnowledgeServiceTree_check:function(treeDIV,selectNode){
			
			 $(treeDIV).jstree({
					json_data: {
						ajax: {
						  type:"post",
						  url : "event!getCategoryTree.action",
					      data:function(n){
					    	  return {'types': 'Service','parentEventId':n.attr ? n.attr("id").replace("node_",""):0};//types is in action
					      },
		                  dataType: 'json',
		                  cache:false
					 	}
					},
					"plugins" : [ "themes", "json_data", "checkbox" ] 
				}).bind('loaded.jstree', function(e,data){});
		},
		
		
		
		/**
		 * 显示知识库分类树,只读
		 * @param treeDIV 树结构所在的div：$(treeDIV)
		 * @param selectNode 选择节点的回调函数
		 */
		showKnowledgeTree_view:function(treeDIV,selectNode){
			
			 $(treeDIV).jstree({
					json_data: {
						ajax: {
						  type:"post",
						  url : "event!getCategoryTree.action?flag=view&num="+category_num,
					      data:function(n){
					    	  return {'types': 'Knowledge','parentEventId':n.attr ? n.attr("id").replace("node_",""):0};//types is in action
					      },
		                  dataType: 'json',
		                  cache:false
					 	}
					},
				"plugins" : ["themes", "json_data", "ui", "crrm", "types", "hotkeys"]
			})
			//.bind('loaded.jstree', function(e,data){data.inst.open_all(-1);})
			.bind("select_node.jstree", function(e,data){
				selectNode(e,data);
			 });
		},
		
		
		
		/**
		 * 显示知识库分类.
		 * @param windowId 显示窗口的id，
		 * @param treeId 树结构的id
		 * @param nameId 回调函数存放选中值得表单name属性(id)
		 * @param valueId 
		 */
		selectKnowledgeCategory:function(windowId,treeId,nameId,valueId){
			windows(windowId.replace('#',''),{width:240});
			wstuo.knowledge.knowledgeTree.showKnowledgeTree(treeId,function(e,data){
				wstuo.knowledge.knowledgeTree.showKnowledgeServiceTree_selectNode(data,treeId,function(obj){
	                 $(nameId).val(obj.attr('cname')).focus();
	                 $(valueId).val(obj.attr('id'));
					 $(windowId).dialog('close');
				});
			});
		},
		/**
		 * 根据id移除指定服务目录
		 * @param id 服务目录编号
		 */
		serviceRm:function(id){
			msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirmDelete'],function(){
				$("#add_request_"+id).remove();
				$("#edit_request_"+id).remove();
				$("#add_change_"+id).remove();
				$("#edit_change_"+id).remove();
				$("#add_scheduledTask_"+id).remove();
				$("#edit_scheduledTask_"+id).remove();
				$("#add_knowledge_"+id).remove();
				$("#edit_knowledge_"+id).remove();
				$("#search_knowledge_"+id).remove();
				$("#edit_ci_"+id).remove();
				$("#add_ci_"+id).remove();
				$("#add_problem_"+id).remove();
				$("#edit_problem_"+id).remove();
				$("#add_scheduledTask_serviceDirectory_tbody_"+id).remove();
				$("#edit_scheduledTask_serviceDirectory_tbody_"+id).remove();
				msgShow(i18n['deleteSuccess'],'show');
    		});
		},
		
		/**
		 * 显示知识库服务目录.
		 * @param windowId 显示窗口的id，
		 * @param treeId 树结构的id
		 * @param nameId 回调函数存放选中值得表单name属性(id)
		 * @param valueId 选中的值
		 */
		selectKnowledgeService:function(windowId,treeId,nameId,valueId){
			windows(windowId.replace('#',''),{width:240});
			wstuo.knowledge.knowledgeTree.showKnowledgeServiceTree(treeId,function(e,data){
				wstuo.knowledge.knowledgeTree.showKnowledgeServiceTree_selectNode(data,treeId,function(obj){
					if($("#problem_"+obj.attr('id')).val()==undefined){
						if('#add_problem_serviceDirectory_tbody'==nameId)
							$("<tr id=problem_"+obj.attr('id')+"><td>"+obj.attr('cname')+"</td><td><a href='#' onclick=wstuo.knowledge.knowledgeTree.serviceRm("+obj.attr('id')+")>"+i18n['deletes']+"</a><input type=hidden name=problemDTO.serviceDirectoryNos value="+obj.attr('id')+"></td></tr>").appendTo('#add_problem_serviceDirectory_tbody');
					}
					if($("#ci_"+obj.attr('id')).val()==undefined){
						if('#add_ci_serviceDirectory_tbody'==nameId)
							$("<tr id=ci_"+obj.attr('id')+"><td>"+obj.attr('cname')+"</td><td><a href='#' onclick=wstuo.knowledge.knowledgeTree.serviceRm("+obj.attr('id')+")>"+i18n['deletes']+"</a><input type=hidden name=ciDto.serviceDirectoryNos value="+obj.attr('id')+"></td></tr>").appendTo('#add_ci_serviceDirectory_tbody');
					}
					if($("#change_"+obj.attr('id')).val()==undefined){ 
						if('#add_change_serviceDirectory_tbody'==nameId)
							$("<tr id=change_"+obj.attr('id')+"><td>"+obj.attr('cname')+"</td><td><a href='#' onclick=wstuo.knowledge.knowledgeTree.serviceRm("+obj.attr('id')+")>"+i18n['deletes']+"</a><input type=hidden name=changeDTO.serviceDirectoryIds value="+obj.attr('id')+"></td></tr>").appendTo('#add_change_serviceDirectory_tbody');
					}
					if($("#Service"+obj.attr('id')).val()==undefined){ 
						if('#addKnowledge_service_name'==nameId)
							$(nameId).append("<div id=Service"+obj.attr('id')+"> "+obj.attr('cname')+"<a href='#' onclick='wstuo.knowledge.knowledgeTree.serviceRm("+obj.attr('id')+")' style='float:right;'><strong> X </strong>&nbsp;&nbsp;&nbsp;&nbsp;</a><input type=hidden name='knowledgeDto.knowledServiceNo' value="+obj.attr('id')+"></div>");
					}
					if($("#request_"+obj.attr('id')).val()==undefined){
						if('#add_request_serviceDirectory_tbody'==nameId || '#edit_request_serviceDirectory_tbody' == nameId){
							$("<tr id=request_"+obj.attr('id')+"><td>"+obj.attr('cname')+"</td><td>"+obj.attr('scores')+"</td><td><a href='#' onclick=wstuo.knowledge.knowledgeTree.serviceRm("+obj.attr('id')+")>"+i18n['deletes']+"</a><input type=hidden name=requestDTO.serviceDirIds value="+obj.attr('id')+"></td></tr>").appendTo(nameId);
						}
					}	
					if($(nameId+"_"+obj.attr('id')).val()==undefined){
						if('#add_scheduledTask_serviceDirectory_tbody'==nameId || '#edit_scheduledTask_serviceDirectory_tbody' == nameId){
							if('#add_scheduledTask_serviceDirectory_tbody'==nameId)
								$("<tr id="+nameId.substring(1)+"_"+obj.attr('id')+"><td>"+obj.attr('cname')+"</td><td>"+obj.attr('scores')+"</td><td><a href='#' onclick=wstuo.knowledge.knowledgeTree.serviceRm("+obj.attr('id')+")>"+i18n['deletes']+"</a><input type=hidden name=serviceDirNos value="+obj.attr('id')+"></td></tr>").appendTo(nameId);
							else
								$("<tr id="+nameId.substring(1)+"_"+obj.attr('id')+"><td>"+obj.attr('cname')+"</td><td>"+obj.attr('scores')+"</td><td><a href='#' onclick=wstuo.knowledge.knowledgeTree.serviceRm("+obj.attr('id')+")>"+i18n['deletes']+"</a><input type=hidden name=serviceDirNos value="+obj.attr('id')+"></td></tr>").appendTo(nameId);
						}
					}
					 $(windowId).dialog('close');
				});
			});
			
		},
		/**
		 * 编辑显示知识库服务目录.
		 * @param windowId 显示窗口的id，
		 * @param treeId 树结构的id
		 * @param nameId 回调函数存放选中值得表单name属性(id)
		 * @param valueId 存放树结构中选中的内容
		 */
		selectKnowledgeServiceEdit:function(windowId,treeId,nameId,valueId){
			
			windows(windowId.replace('#',''),{width:240});
			wstuo.knowledge.knowledgeTree.showKnowledgeServiceTree(treeId,function(e,data){
				wstuo.knowledge.knowledgeTree.showKnowledgeServiceTree_selectNode(data,treeId,function(obj){
					if($("#Service"+obj.attr('id')).val()==undefined){
						if("edit_change_serviceDirectory_tbody"==valueId)
							$(nameId).append("<tr id=Service"+obj.attr('id')+"><td>"+obj.attr('cname')+"</td><td><a href='#' onclick=wstuo.knowledge.knowledgeTree.serviceRm("+obj.attr('id')+")>"+i18n['deletes']+"</a><input type=hidden name=changeDTO.serviceDirectoryIds value="+obj.attr('id')+"></td></tr>");
						else if("edit_problem_serviceDirectory_tbody"==valueId)
							$(nameId).append("<tr id=Service"+obj.attr('id')+"><td>"+obj.attr('cname')+"</td><td><a href='#' onclick=wstuo.knowledge.knowledgeTree.serviceRm("+obj.attr('id')+")>"+i18n['deletes']+"</a><input type=hidden name=problemDTO.serviceDirectoryNos value="+obj.attr('id')+"></td></tr>");
						else if("edit_ci_serviceDirectory_tbody"==valueId)
							$(nameId).append("<tr id=Service"+obj.attr('id')+"><td>"+obj.attr('cname')+"</td><td><a href='#' onclick=wstuo.knowledge.knowledgeTree.serviceRm("+obj.attr('id')+")>"+i18n['deletes']+"</a><input type=hidden name=ciDto.serviceDirectoryNos value="+obj.attr('id')+"></td></tr>");
						else
							$(nameId).append("<div id=Service"+obj.attr('id')+">"+obj.attr('cname')+"<a href='#' onclick='wstuo.knowledge.knowledgeTree.serviceRm("+obj.attr('id')+")' style='float:right;'><strong> X </strong>&nbsp;&nbsp;&nbsp;&nbsp;</a><input type=hidden  name='knowledgeDto.knowledServiceNo' value="+obj.attr('id')+"></div>");
					}
					$(windowId).dialog('close');
				});
			});
			
		},
		showKnowledgeServiceTree_selectNode:function(data,treeId,func){
			var obj = data.rslt.obj;
			var _parent = data.inst._get_parent();
			if($vl(obj.attr('cname'))==="More..." && obj.attr('start')!=undefined){
				$.ajax({
					type:"post",
					url :"event!getCategoryTree.action?flag=dataDic&num="+category_num+"&start="+obj.attr('start')+"&types=Knowledge&parentEventId="+(_parent.attr ? _parent.attr("id").replace("node_",""):0),
	                dataType: 'json',
	                cache:false,
					success:function(data0){
						var bool=false;
						$.each(data0, function(s, n) {
							$(treeId).jstree("create_node", obj, "before", n
							).bind("select_node.jstree",function(e,data1){
								wstuo.category.eventCategoryTree.showKnowledgeServiceTree_selectNode(data1,treeId,func);
							});
							bool=n.attr.bool;
						});
						//$(treeId).jstree("_append_json_data",obj,data0);
						obj.attr('start',parseInt(obj.attr('start'))+1);
						if(!bool){
							msgShow("没有更多了...",'show');
							obj.attr('cname',"NoMore");
						}
				    }
				});
			}else if($vl(obj.attr('cname'))==="NoMore"){
				msgShow("没有更多了...",'show');
			}else{
				func(obj);
			}
			
		},
		/**选择知识库服务目录过滤
		 * @param windowId 显示窗口的id，
		 * @param treeId 树结构的id
		 * @param nameId 回调函数存放选中值得表单name属性(id)
		 * @param valueId 存放树结构中选中的内容
		 * */
		selectKnowledgeServiceFilter:function(windowId,treeId,nameId,valueId){
			
			windows(windowId.replace('#',''),{width:240});
			wstuo.knowledge.knowledgeTree.showKnowledgeServiceTree(treeId,function(e,data){
				wstuo.knowledge.knowledgeTree.showKnowledgeServiceTree_selectNode(data,treeId,function(obj){
					$(nameId).val(obj.attr('cname'));
					$(valueId).val(obj.attr('id'));
					$(windowId).dialog('close');
				});
			});
		},
		
		/**
		 * 选择知识库分类
		 * @param nameId 回调函数存放选中值得表单name属性(id)
		 * @param valueId 存放树结构中选中的内容
		 */
		selectCategory:function(nameId,valueId){		
			wstuo.knowledge.knowledgeTree.selectKnowledgeCategory('#knowledge_category_select_window','#knowledge_category_select_tree',nameId,valueId);			
		}
		
	}
	
}();

