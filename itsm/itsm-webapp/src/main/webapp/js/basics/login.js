	
	/**
	 * 回车登录.
	 */
	document.onkeydown = function(evt){
		
		var evt = window.event?window.event:evt;
		if(evt.keyCode==13){
			$('#userLogin').submit();
		}
	}
	
	//刷新验证码
	function refreshCheckCode(){
	     // $("#validateCode_img").attr({"src":contextPath+"/security/captcha.jpg?random="+new Date()}); 
	}
	
	/**
	 * 验证表单.
	 */
	function checkBoforeLogin(){
		
		/*if($('#userLogin').form('validate')){
			//$('#j_username_value').val($('#j_username').val()+"<>"+$('#j_password').val());
			$("#userLogin").submit();
		}*/
	}
	
	
	/**
	 * 设置COOKIE
	 */
	function setCookie(c_name,value,exdays){
		var exdate=new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
		document.cookie=c_name + "=" + c_value;
	}
	
	/**
	 * 取得COOKIE
	 */
	function getCookie(c_name){
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++){
			  x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
			  y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
			  x=x.replace(/^\s+|\s+$/g,"");
			  if(x==c_name){
			    return unescape(y);
			  }
		  }
	}

	/**
	 * 重置表单.
	 */
	function resetLoginForm(){
		
		$('#j_username,#j_password,#j_validate').val('');
	}
	
	function login(){
		if($("#_spring_security_remember_me").attr("checked")){
			setCookie('cookieUserName',$('#j_username').val(),14);
			setCookie('cookieUserPassword',$('#j_password').val(),14);
		}else{
			setCookie('cookieUserName',null,0);
			setCookie('cookieUserPassword',null,0);
		}
		document.getElementById('userLogin').submit();
		//location.href=contextPath+"/j_spring_security_check?"+$('#userLogin').serialize();
	}
	
	function chkFlash() {
		  var isIE = (navigator.appVersion.indexOf("MSIE") >= 0);
		  var hasFlash = true;

		  if(isIE) {
		    try{
		      var objFlash = new ActiveXObject("ShockwaveFlash.ShockwaveFlash");
		    } catch(e) {
		      hasFlash = false;
		    }
		  } else {
		    if(!navigator.plugins["Shockwave Flash"]) {
		      hasFlash = false;
		    }
		  }
		  return hasFlash;
	}
	
	
	/**
	 * 加载.
	 */
	$(document).ready(function(){
		$("#userLogin").validate({
			errorPlacement: function ( error, element ) {
				$(element).tooltip({
					placement:'bottom',
					title:error.text(),
				});
			},
			success: function ( label, element ) {
				$(element).tooltip('destroy');
			},
			submitHandler: function () {
				login();
			}
		});
		//加载验证码
		refreshCheckCode();
		//隐显面板
		setTimeout(function(){
			//自动登录
			if(getCookie("cookieUserName")!=null && getCookie("cookieUserPassword")!=null){
				$('#j_username').val(getCookie('cookieUserName'));
				$('#j_password').val(getCookie('cookieUserPassword'));
				$("#_spring_security_remember_me").attr("checked",true);
				//$("#userLogin").submit();
			}
		},0);
		/*if(!chkFlash()){
			$('#flashCheckResult').text(i18n.msg_flash_player_not_installed);
		}*/

	});