$package('basics');

/**  
 * @fileOverview 有关时间操作包
 * @author yctan
 * @version 1.0  
 */  
 /**  
 * @author yctan  
 * @constructor 有关时间操作包
 * @description 有关时间操作的所有函数
 * @date 2012-08-20
 * @since version 1.0 
 */  

basics.dateUtility = function(){
	
	return {
		/** 处理上下周，上下月*/
		nextWeekMonth:function(type,opt,firstDates){
			var FIRSTDATE='';
			var LASTDATE=''; 
			if(type=="MONTHS"&&opt=="next"){ 
				var Nowdate   =   new   Date(Date.parse(firstDates.replace(/-/g,   "/"))); 
				var v=Nowdate.getMonth()+1; 
				Nowdate.setMonth(v);  
				var now=Nowdate.format('yyyy-MM-dd'); 
				FIRSTDATE=basics.dateUtility.showMonthFirstDay(now);
				LASTDATE=basics.dateUtility.showMonthLastDay(now); 
			}else if(type=="MONTHS"&&opt=="m"){ 
				var Nowdate   =   new   Date(Date.parse(firstDates.replace(/-/g,   "/"))); 
				var v=Nowdate.getMonth()-1;  
				Nowdate.setMonth(v);  
				var now=Nowdate.format('yyyy-MM-dd');
				FIRSTDATE=basics.dateUtility.showMonthFirstDay(now); 
				LASTDATE=basics.dateUtility.showMonthLastDay(now);
			}else if(type=="WEEKS"){
				if(type=="WEEKS"&&opt=="next"){
					var Nowdate = new Date(Date.parse(firstDates.replace(/-/g, "/")));  /**根据当前星期一处理上下周*/
					Nowdate.setDate(Nowdate.getDate()+7) ;
					var now=Nowdate.format('yyyy-MM-dd'); 
					FIRSTDATE=basics.dateUtility.showWeekFirstDay(now).format('yyyy-MM-dd');
					LASTDATE=basics.dateUtility.showWeekLastDay(now).format('yyyy-MM-dd'); 
				}else if(type=="WEEKS"&&opt=="w"){   
					var Nowdate = new Date(Date.parse(firstDates.replace(/-/g, "/"))); 
					Nowdate.setDate(Nowdate.getDate()-7) ; 
					var now=Nowdate.format('yyyy-MM-dd');  
					
					FIRSTDATE=basics.dateUtility.showWeekFirstDay(now).format('yyyy-MM-dd');  
					LASTDATE=basics.dateUtility.showWeekLastDay(now).format('yyyy-MM-dd');
				}  
			}else if(type=="YEARS"){
				if(type=="YEARS"&&opt=="next"){
					var Nowdate   =   new   Date(Date.parse(firstDates.replace(/-/g, "/")));  /**根据当前年处理上下年*/
					Nowdate.setFullYear(Nowdate.getFullYear()+1) ;
					var now=Nowdate.format('yyyy-MM-dd'); 
					FIRSTDATE=basics.dateUtility.showYearFirstDay(now);
					LASTDATE=basics.dateUtility.showYearLastDay(now); 
				}else if(type=="YEARS"&&opt=="y"){   
					var Nowdate   =   new   Date(Date.parse(firstDates.replace(/-/g, "/"))); 
					Nowdate.setFullYear(Nowdate.getFullYear()-1) ; 
					var now=Nowdate.format('yyyy-MM-dd'); 
					FIRSTDATE=basics.dateUtility.showYearFirstDay(now);  
					LASTDATE=basics.dateUtility.showYearLastDay(now);
				}  
			}
			return FIRSTDATE+'&'+LASTDATE;
		},
		
		
		//本周第一天
		showWeekFirstDay:function(s){
			var Nowdate = new Date(Date.parse(s.replace(/-/g, "/")));   
			var WeekFirstDay = new Date(Nowdate-(Nowdate.getDay())*86400000); 
			return WeekFirstDay;
		},

		//本周第七天 
		showWeekLastDay:function(s){
			var Nowdate = new Date(Date.parse(s.replace(/-/g, "/"))); 
			var WeekFirstDay=new Date(Nowdate-(Nowdate.getDay())*86400000); 
			var WeekLastDay=new Date((WeekFirstDay/1000+6*86400)*1000);  
			return WeekLastDay; 
		},
		 
		//本月第一天
		showMonthFirstDay:function(s) {
		    var myDate = new Date(Date.parse(s.replace(/-/g,"/")));
		    var year = myDate.getFullYear();
		    var month = myDate.getMonth()+1;
		    if (month<10)
		        month = "0"+month;
		    var firstDay =year+"-"+month+"-01";
		    return firstDay;
		},

		//本月最后一天 
		showMonthLastDay:function(s) {    
		    var myDate = new Date(Date.parse(s.replace(/-/g, "/")));
		    var year = myDate.getFullYear();
		    var month = myDate.getMonth()+1;
		    if (month<10)
		        month = "0"+month;
		    myDate = new Date(year,month,0);
		    var lastDay = year+"-"+month+"-"+myDate.getDate();
		    return lastDay;
		},
		
		//本年第一天
		showYearFirstDay:function(s) {    
		    var myDate = new Date(Date.parse(s.replace(/-/g,"/")));
		    var year = myDate.getFullYear();
		    var firstDay =year+"-01-01";
		    return firstDay;
		},
		//本年最后一天
		showYearLastDay:function(s) {    
		    var myDate = new Date(Date.parse(s.replace(/-/g, "/")));
		    var year = myDate.getFullYear();
		    var lastDay = year+"-12-31";
		    return lastDay;
		},
		//当天
		showDayFirstDay:function(s) {    
			var myDate = new Date(Date.parse(s.replace(/-/g, "/")));
			//获取本年
		    var year = myDate.getFullYear();
		    //获取本月
		    var month = myDate.getMonth()+1;
		    if (month<10)
		        month = "0"+month;
		    //获取本日
		    var day = myDate.getDate();
		    if (day<10)
		    	day = "0"+day;
		    var firstDay = year+"-"+month+"-"+day;
		    return firstDay;
		},
		//明天
		showDayLastDay:function(s) {
			var myDate = new Date(Date.parse(s.replace(/-/g, "/")));
			//获取本年
		    var year = myDate.getFullYear();
		    //获取本月
		    var month = myDate.getMonth()+1;
		    if (month<10)
		        month = "0"+month;
		    //获取明天
		    var dd = new Date();
		    dd.setDate(dd.getDate()+1);
		    var day = dd.getDate();
		    if (day<10)
		    	day = "0"+day;
		    var lastDay = year+"-"+month+"-"+day;
		    return lastDay;
		}
	}
}();
