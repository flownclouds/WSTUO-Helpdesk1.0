/**
 * FileName: import.js
 *
 * File description goes here.
 *
 * Copyright (c) 2010 wstuo. All Rights Reserved.
 *
 * @author wstuo.com
 */

function $import(packageName){
    try {
        eval(packageName);
    } 
    catch (e) {
        var xmlHttp;
        if (window.ActiveXObject) {
            xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else 
            if (window.XMLHttpRequest) {
                xmlHttp = new XMLHttpRequest();
            }
        var regexp = /\b\.\b/g;
        if (typeof baseURL == 'undefined') {
            baseURL = '../';
        }
        packageName = baseURL + 'js/' + packageName.replace(regexp, '/') + '.js';
        xmlHttp.open("get", packageName, false);
        xmlHttp.send(null);
        var code = xmlHttp.responseText;
        try {
            if (window.execScript) {
                window.execScript(code, "JavaScript");
            }
            else {
                window.eval(code, "JavaScript");
            }
        } 
        catch (e) {
            alert("Error when import '" + packageName + "' !" + e);
        }
        return;
    }
    
    if (typeof eval(packageName) == 'undefined') {
        var xmlHttp;
        if (window.ActiveXObject) {
            xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else 
            if (window.XMLHttpRequest) {
                xmlHttp = new XMLHttpRequest();
            }
        var regexp = /\b.\b/g;
        if (typeof baseURL == 'undefined') {
            baseURL = '../';
        }
        packageName = baseURL+'js/' + packageName.replace(regexp, '/') + '.js';
        xmlHttp.open("get", packageName, false);
        xmlHttp.send(null);
        var code = xmlHttp.responseText;
        try {
            if (window.execScript) {
                window.execScript(code, "JavaScript");
            }
            else {
                window.eval(code, "JavaScript");
            }
        } 
        catch (e) {
            alert("Error when import '" + packageName + "' !" + e);
        }
    }
}
