<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@page import="org.springframework.beans.factory.annotation.Autowired"%>
<%@page import="com.wstuo.common.security.service.IUserInfoService"%>
<%@page import="java.util.Map"%>
<%@page import="com.wstuo.common.security.service.UserInfoService"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../language.jsp" %>
<%
    WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(session.getServletContext());
	IUserInfoService userInfoService = (IUserInfoService)ctx.getBean("userInfoService");
	if(session.getAttribute("loginUserName")!=null){
	 	Map<String,Object> resAuth=userInfoService.findResAuthByUserName(session.getAttribute("loginUserName").toString(),new String[]{
			  "CofnigureItem_AssetOriginalValue_Res",
			  "CofnigureItem_Barcode_Res",
			  "CofnigureItem_Supplier_Res",
			  "CofnigureItem_PurchaseDate_Res",
			  "CofnigureItem_ArrivalDate_Res",
			  "CofnigureItem_PurchaseNo_Res",
			  "CofnigureItem_Owner_Res",
			  "CofnigureItem_LifeCycle_Res",
			  "CofnigureItem_WarrantyDate_Res",
			  "CofnigureItem_WarningDate_Res",
			  "CofnigureItem_SerialNumber_Res",
			  "CofnigureItem_Department_Res",
			  "CofnigureItem_Fimapping_Res",
			  "CofnigureItem_ExpiryDate_Res",
			  "CofnigureItem_LendTime_Res",
			  "CofnigureItem_OriginalUser_Res",
			  "CofnigureItem_RecycleTime_Res",
			  "CofnigureItem_PlannedRecycleTime_Res",
			  "CofnigureItem_UsePermission_Res",
			  "/pages/ci!cItemSave.action",
			  "/pages/ci!cItemsDel.action",
			  "/pages/ci!cItemUpdate.action"
			  });
	 	request.setAttribute("resAuth",resAuth);
	}
	String rowValue="";
	String colValue="";
	if(request.getParameter("rowValue")!=null){
		rowValue=request.getParameter("rowValue");
		rowValue=java.net.URLDecoder.decode(rowValue, "utf-8");
	}
	if(request.getParameter("colValue")!=null){
		colValue=request.getParameter("colValue");
		colValue=java.net.URLDecoder.decode(colValue, "utf-8");
	}
	request.setAttribute("rowValue",rowValue);
	request.setAttribute("colValue",colValue);
%>
<script>
var assetsOriginalValue_res=${resAuth.CofnigureItem_AssetOriginalValue_Res};
var barcode_res=${resAuth.CofnigureItem_Barcode_Res};
var supplier_res=${resAuth.CofnigureItem_Supplier_Res};
var purchaseDate_res=${resAuth.CofnigureItem_PurchaseDate_Res};
var arrivalDate_res=${resAuth.CofnigureItem_ArrivalDate_Res};
var purchaseNo_res=${resAuth.CofnigureItem_PurchaseNo_Res};
var owner_res=${resAuth.CofnigureItem_Owner_Res};
var lifeCycle_res=${resAuth.CofnigureItem_LifeCycle_Res};
var warrantyDate_res=${resAuth.CofnigureItem_WarrantyDate_Res};
var warningDate_res=${resAuth.CofnigureItem_WarningDate_Res};
var serialNumber_res=${resAuth.CofnigureItem_SerialNumber_Res};
var department_res=${resAuth.CofnigureItem_Department_Res};
var fimapping_res=${resAuth.CofnigureItem_Fimapping_Res};
var expiryDate_res=${resAuth.CofnigureItem_ExpiryDate_Res};
var lendTime_res=${resAuth.CofnigureItem_LendTime_Res};
var originalUser_res=${resAuth.CofnigureItem_OriginalUser_Res};
var recycleTime_res=${resAuth.CofnigureItem_RecycleTime_Res};
var plannedRecycleTime_res=${resAuth.CofnigureItem_PlannedRecycleTime_Res};
var usePermission_res=${resAuth.CofnigureItem_UsePermission_Res};

var rowValue="${rowValue}";
var colValue="${colValue}";
var rowKey='${param.rowKey}';
var colKey='${param.colKey}';
var customFilterNo='${param.customFilterNo}';
var countQueryType='${param.countQueryType}';
var filterId='${param.filterId}';
var _categoryType='${param.categoryType}';
var _categoryNo='${param.categoryNo}';
var cItemSave="0";
var cItemsDel="0";
var cItemUpdate="0";
var ciList_companyNo='${param.companyNo}';
$('#ci_search_companyNo').val(ciList_companyNo);
$('#ci_s_categoryNo').val(_categoryNo);
var _searchType = '${param.searchType}';
var _fullName = '';
if(_searchType=='myCi')
	_fullName = "${sessionScope.fullName}";
</script>
<c:if test="${resAuth['/pages/ci!cItemSave.action'] }">
	<script>cItemSave="1";</script>
</c:if>
<c:if test="${resAuth['/pages/ci!cItemsDel.action'] }">
	<script>cItemsDel="1";</script>
</c:if>
<c:if test="${resAuth['/pages/ci!cItemUpdate.action'] }">
	<script>cItemUpdate="1";</script>
</c:if>

<script src="${pageContext.request.contextPath}/scripts/itsm/cim/configureItem.js?random=<%=new java.util.Date().getTime()%>"></script>
<script type="text/javascript">
	//加载过滤器
	common.config.customFilter.filterGrid_Operation.loadFilterByModule("#ci_userToSearch","ci");
	//配置项列表
	setTimeout(function(){
		itsm.cim.configureItem.createList('ci!cItemsFind.action?ciQueryDTO.categoryNo='+_categoryNo+'&ciQueryDTO.companyNo='+ciList_companyNo+'&ciQueryDTO.loginName='+userName);
	},0);
</script>
<div id="configureItemMain_layout"  border="false" class="easyui-layout" fit="true" style="height:100%">	
<!-- 分类树-->
	<div id="configureItemDiv" border="true" region="east" title="<fmt:message key="title.request.CICategory"/>"  style="width:200px;padding:5px;padding-bottom: 15px;height:650px;">
		<sec:authorize url="configItem_menu_configItemAssort">
		<div id="configureItemCategoryTree"  style="overflow: auto;height: 100%;"></div>
		</sec:authorize>
	</div>
	
	<div region="center" border="false" style="padding: 3px;height:100%" id="configureItem_content">
		<div id="configureTableDiv">
			<table id="configureGrid" ></table>
			<div id="ciPager"></div>
		</div>
		<div id="CIGridToolbar" style="display:none">
		<c:if test="${resAuth['/pages/ci!cItemSave.action'] }">
			<a class="easyui-linkbutton" plain="true" icon="icon-add" onclick="itsm.cim.configureItem.addConfigureItem()" title="<fmt:message key="common.add"/>"></a>
		</c:if>
		<c:if test="${resAuth['/pages/ci!cItemUpdate.action'] }">
			<a class="easyui-linkbutton" plain="true" icon="icon-edit" onclick="itsm.cim.configureItem.editConfigureItem_toolbar_edit_aff()" title="<fmt:message key="common.edit"/>"></a> 
		</c:if>
		<c:if test="${resAuth['/pages/ci!cItemsDel.action'] }">
			<a class="easyui-linkbutton" plain="true" icon="icon-cancel" onclick="itsm.cim.configureItem.configureItem_toolbar_delete_aff()" title="<fmt:message key="common.delete"/>"></a>
		</c:if>
		<a class="easyui-linkbutton" plain="true" icon="icon-search" id="link_ci_search" onclick="itsm.cim.configureItem.searchConfigureItemWin()" title="<fmt:message key="common.search"/>"></a>
		
		<sec:authorize url="configItemGrid_export">
		&nbsp;&nbsp;<a icon="icon-export" onclick="itsm.cim.configureItem.exportConfigureItem()" class="easyui-linkbutton" plain="true" title="<fmt:message key="label.dc.exportCurrentView"/>"></a>
		</sec:authorize>
		
		<%--
		&nbsp;|&nbsp;<a href="javascript:showHelpFilm('ci.flv')" plain="true" class="easyui-linkbutton" icon="icon-help"></a>	--%>
		<sec:authorize url="configItemGrid_import">
		&nbsp;&nbsp;<a icon="icon-import" onclick="itsm.cim.configureItem.importConfigureItemCsvWin()" class="easyui-linkbutton" plain="true" title="<fmt:message key="label.dc.importFromCsv" />"></a>	
		</sec:authorize>
		<%-- <sec:authorize url="configItemGrid_import">
		&nbsp;&nbsp;<a icon="icon-scanImport" onclick="itsm.cim.configureItem.importConfigureItem()" class="easyui-linkbutton" plain="true" title="<fmt:message key="label_scan_result_import" />"></a>
		</sec:authorize> --%>
		
		<sec:authorize url="/pages/ci!CustomFilterWin.action">
		&nbsp;&nbsp;<a icon="icon-filter" class="easyui-linkbutton" plain="true" id="ci_customFilter" onclick="itsm.cim.configureItem.openCustomFilterWin()" title="<fmt:message key="lable.customFilter.self_defined" />"></a>
		&nbsp;&nbsp;<select id="ci_userToSearch"  onchange="itsm.cim.configureItem.getDataByFilterSearch(this.value)"></select>
		</sec:authorize>
		
		<!-- 配置项授权设置 -->
		<%-- <sec:authorize url="CONFIGITEM_EMPOWER_SET_RES">
		&nbsp;&nbsp;<a onclick="common.security.acl.openAclSetWind('configureGrid','com.wstuo.itsm.domain.entity.CI')" class="easyui-linkbutton" plain="true"><fmt:message key="label.acl.authorize.set" /></a>
		</sec:authorize> --%>
		</div>
		<div id="configureItemFrmBarDiv" style="display: none;"><a href="javascript:itsm.cim.configureItem.showConfigureItem__edit('{ciId}')" title="<fmt:message key="common.detailInfo" />"><img src="${pageContext.request.contextPath}/skin/default/images/detail.png" border="0" /></a>&nbsp;&nbsp;<sec:authorize url="/pages/ci!cItemUpdate.action"><a href="javascript:itsm.cim.configureItem.editConfigureItem_toolbar_edit('{ciId}')" title="<fmt:message key="common.edit" />"><img src="${pageContext.request.contextPath}/skin/default/images/grid_edit.png" border="0"/></a></sec:authorize>&nbsp;&nbsp;<sec:authorize url="/pages/ci!cItemsDel.action"><a href="javascript:itsm.cim.configureItem.configureItem_toolbar_delete('{ciId}')" title="<fmt:message key="common.delete" />"><img src="${pageContext.request.contextPath}/skin/default/images/grid_delete.png" border="0"/></a></sec:authorize></div>	

	<!-- 扫描工具扫描数据导入-->
	<div id="importConfigureItemWindow" class="WSTUO-dialog" title="<fmt:message key="label_scan_result_import"/>" style="width:400px;height:auto">
	<form id="importConfigureItemForm">
	   <div class="lineTableBgDiv" >
		   <table  class="lineTable"  width="100%" cellspacing="1"> 
		        <tr>
		            <td width="100px">
		            	<fmt:message key="label.dc.filePath"/>
		            </td>
		            <td>
		           		<input type="file" class="easyui-validatebox input" required="true" name="importFile" id="importConfigureItemFile" onchange="checkFileType(this,'txt')" />
		            </td>
		        </tr>
				
				<!-- 所属客户  -->
				<tr <c:if test="${versionType!='ITSOP'}"><c:out value="style=display:none"></c:out></c:if>>
					<td><fmt:message key="label.belongs.client" />&nbsp;<span style="color:red">*</span></td>
					<td>
						<input type="hidden" name="ciDto.companyNo" id="wmi_import_companyNo" />
						<input id="wmi_import_companyName" class="easyui-validatebox input" required="true" readonly />
					</td>
				</tr>
	            <tr>
	                <td style="width:40%"><fmt:message key="common.category"/>&nbsp;<span style="color:red">*</span></td>
					<td style="width:60%">
						<input type="hidden" name="ciDto.categoryNo" id="wmi_import_categoryNo"  />
						<script>function ciBask(){}</script>
					  	<input name="ciDto.categoryName" id="wmi_import_categoryName" class="easyui-validatebox input" required="true" readonly style="cursor:pointer" onclick="itsm.cim.ciCategoryTree.configureItemTree('search_ciCategoryTreeDiv','search_ciCategoryTreeTree','wmi_import_categoryNo','wmi_import_categoryName','false')" />
	                </td>
	            </tr>
	            
	            <tr>
	                <td ><fmt:message key="common.status"/></td>
	                <td>
		                <select name="ciDto.statusId" id="wmi_import_status" class="input">
		                	
		                </select>
	                </td>
	           </tr>
				
		        <tr>
		            <td colspan="2">
		                <div style="padding-top:8px;">
		                	<a id="importWMIBtn" class="easyui-linkbutton"   icon="icon-import"><fmt:message key="label.dc.import"/></a>
		                </div>
		       		 </td>
		        </tr>
		    </table>
	    </div>
	   </form>
	</div>
	
	<!-- CSV导入数据 -->
	<div id="importConfigureItemCsvWindow"  class="WSTUO-dialog" title="<fmt:message key="label.dc.importFromCsv"/>" style="width:400px;height:auto">
	<form>
	   <div class="lineTableBgDiv" >
		   <table  class="lineTable"  width="100%" cellspacing="1"> 
		   		<!-- 所属客户  -->
				<tr <c:if test="${versionType!='ITSOP'}"><c:out value="style=display:none"></c:out></c:if>>
					<td><fmt:message key="label.belongs.client" />&nbsp;<span style="color:red">*</span></td>
					<td>
						<input type="hidden" id="csv_import_companyNo" />
						<input id="csv_import_companyName" class="easyui-validatebox input" required="true" readonly />
					</td>
				</tr>
				<tr>
		            <td width="100px">
		            	<fmt:message key="title.request.CICategory"></fmt:message>
		            </td>
		            <td>
		           		<input class="easyui-validatebox input" id="choiseImportCICategory" value="<fmt:message key="title.request.CICategory"></fmt:message>"/>
		           		<input type="hidden" id="choiseImportCICategoryNo">
		            </td>
		        </tr>
		        <tr>
		            <td width="100px">
		            	<fmt:message key="label.dc.filePath"/>
		            </td>
		            <td>
		           		<input type="file" class="easyui-validatebox input" required="true" name="importFile" id="importConfigureItemCsvFile" onchange="checkFileType(this,'csv')"/>
		            </td>
		        </tr>
				<tr>
		            <td colspan="2" align="center">
		            	<%-- <c:choose>  
						   <c:when test="${lang eq 'en_US'}">
						   		<a href="../importFile/en/ConfigureItem.zip" target="_blank">[<fmt:message key="label.csv.template.download"></fmt:message>]</a>
						   </c:when>
						   <c:otherwise>
						   		<a href="../importFile/ConfigureItem.zip" target="_blank">[<fmt:message key="label.csv.template.download"></fmt:message>]</a>
						   </c:otherwise>
						</c:choose> --%>
		           		<a href="javascript:;" onclick="itsm.cim.configureItem.getDownloadTemplate(this,'${lang}');"
		           			target="_blank">[<fmt:message key="label.csv.template.download"></fmt:message>]</a>
		            </td>
		        </tr>
		        <tr>
		            <td colspan="2">
		                <div style="padding-top:8px;">
		                	<a id="importCiCategoryBtn" class="easyui-linkbutton" onclick="itsm.cim.configureItem.importConfigureItemCsv()"  icon="icon-import"><fmt:message key="label.dc.import"/></a>
		                </div>
		       		 </td>
		        </tr>
		    </table>
	    </div>
	   </form>
	</div>
	
	
	 <form action="ci!exportConfigureItem.action" method="post" id="export_ci_form">
		<div id="export_ci_values"></div>
	</form>
	<div id="exportDataWin" title="<fmt:message key="label.export.downloadFile"/>" class="WSTUO-dialog" style="width:280px; height:80px;">
	
	</div>
	
	<!-- 搜索 配置项-->
	<div id="configureSearchWin" class="WSTUO-dialog" title='<fmt:message key="common.search"/>-<fmt:message key="title.mainTab.configItem" />' >

		<form id="configureSearchForm" action="ci!exportConfigureItem.action" method="post">
		 <div class="lineTableBgDiv" style="width: 98%">
			<table style="width:100%" class="lineTable" cellspacing="1">
			
			<!-- 所属客户  -->
			<tr <c:if test="${versionType!='ITSOP'}"><c:out value="style=display:none"></c:out></c:if>>
				<td><fmt:message key="label.belongs.client" /></td>
				<td>
					<input type="hidden" name="ciQueryDTO.companyNo" id="ci_search_companyNo" />
					<input id="ci_search_companyName" style="width:52%" />
					<a id="ci_search_companyName_open" title="<fmt:message key='common.update' />">
					<img style="vertical-align:middle;" src="../images/icons/userpicker_disabled.gif"></a>&nbsp;&nbsp;&nbsp;
					<a class="easyui-linkbutton" plain="true" icon="icon-clean" onclick="cleanIdValue('ci_search_companyNo','ci_search_companyName')" title="<fmt:message key="label.request.clear" />"></a>
				</td>
				<td colspan="2"></td>
				<%-- <td><fmt:message key="label.dc.systemPlatform"/></td>
	              <td>
	              	<select id="ci_s_systemPlatform" name="ciQueryDTO.systemPlatformId"><option value="0"></option></select>                                  
	              </td> --%>
			</tr>
			
			<tr>
	              <td><fmt:message key="title.asset.name"/></td>
	              <td>
	              	<input name="ciQueryDTO.ciname" id="s_ciname" style="width:77%"/>
	              </td>
	              <td><fmt:message key="lable.ci.assetNo"/></td>
	              <td>
	                <input name="ciQueryDTO.cino" id="s_cino" style="width:77%"/>
	              </td>     
	          </tr>
			<tr>
	            <td><fmt:message key="common.category"/></td>
				<td>
			  		<input type="hidden" name="ciQueryDTO.categoryNo" id="ci_s_categoryNo" />
			  		
			  		<input name="ciQueryDTO.categoryName"  style="width:65%" id="ci_s_categoryName"  readonly="true" onclick="itsm.cim.ciCategoryTree.configureItemTree('search_ciCategoryTreeDiv','search_ciCategoryTreeTree','ci_s_categoryNo','ci_s_categoryName','false')" />
			   		<a  class="easyui-linkbutton" icon="icon-clean" plain="true"   onclick="itsm.cim.ciCategoryTree.cleanSelect('search_ciCategoryTreeDiv','ci_s_categoryNo','ci_s_categoryName')" title="<fmt:message key="label.request.clear"/>"></a>
	              </td>
	           <td><fmt:message key="ci.department" /></td>
	          <td><input name="ciQueryDTO.department" id="s_department"  style="width:77%"/>
	          <a class="easyui-linkbutton" plain="true" icon="icon-clean" onclick="cleanIdValue('s_department')" style="cursor: pointer;" title="<fmt:message key="label.request.clear"/>"></a>
			  </td>
	          </tr>
	          
	          
	         <%--  <tr>
	              <td><fmt:message key="ci.productType"/></td>
	              <td>
	              	<input name="ciQueryDTO.model" id="s_model" style="width:77%"/>
	              </td>  
	              <td><fmt:message key="ci.barcode"/></td>
	              <td>
	             	    <input name="ciQueryDTO.barcode" id="s_barcode" style="width:77%"/>
	              </td>
	          </tr> --%>
	          
	         <%--  <tr  style="display:none" id="tr1">
	              <td><fmt:message key="ci.serialNumber"/></td>
	              <td>
	              	<input name="ciQueryDTO.serialNumber" id="s_serialNumber" style="width:77%"/>
	              </td>
	              
	              <td><fmt:message key="ci.purchaseNo"/></td>
	              <td>
	             	 	<input name="ciQueryDTO.poNo" id="s_poNo" style="width:77%"/>
	              </td>

	          </tr> --%>
	               
	          <%-- <tr  style="display:none" id="tr2">
	              <td><fmt:message key="ci.productProvider" /></td>
	              <td>
	             	 	<input name="ciQueryDTO.sourceUnits" id="s_sourceUnits" style="width:77%"/>
	              </td>
	              
	             <td><fmt:message key="ci.usePermission" /></td>
	              <td>
	             	    <input name="ciQueryDTO.usePermissions" id="s_usePermissions" style="width:77%"/>
	              </td>
	          </tr> --%>
	          
	          <%-- <tr  style="display:none" id="tr3">
	              <td><fmt:message key="label.ci.businessEntity" /></td>
	              <td>
	             	 	<input name="ciQueryDTO.workNumber" id="s_workNumber" style="width:77%"/>
	              </td>
	              
	              <td><fmt:message key="label.ci.project" /></td>
	              <td>
	             	    <input name="ciQueryDTO.project" id="s_project" style="width:77%"/>
	              </td>
	          </tr> --%>
	          
	     
	          
	          <%-- <tr   id="tr4">
	              <td><fmt:message key="label.role.roleMark" /></td>
	              <td>
	             	    <input name="ciQueryDTO.CDI" id="s_CDI" style="width:77%"/>
	              </td>
	              
	              <td><fmt:message key="ci.FIMapping"/></td>
	            	<td>
	            	<select name="ciQueryDTO.financeCorrespond" style="width:79%" >
	            		<option value="0"><fmt:message key="tool.affiche.all" /></option>
	            		<option value="1"><fmt:message key="tool.affiche.yes" /></option>
	            		<option value="2"><fmt:message key="tool.affiche.no" /></option>
	            	</select>
	            	</td>
	          </tr> --%>
	           
	          <tr>
	              <td><fmt:message key="common.status"/></td>
	              <td>
	              	<select id="ci_s_statusAdd" name="ciQueryDTO.statusId" style="width:79%"><option value="0"></option></select>
	              </td>
	              <td><fmt:message key="ci.location"/></td>
	              <td>
	              <input type="hidden" name="ciQueryDTO.locId" id="ci_s_locId" />
			  	  <input name="ciQueryDTO.loc" id="ci_s_loc" style="width:77%"  readonly="true" onclick="common.config.category.eventCategoryTree.selectlocation('#ci_s_loc','#ci_s_locId')" />
	              &nbsp;<a href="javascript:void(0)"  class="easyui-linkbutton" icon="icon-clean" plain="true"    onclick="$('#ci_s_loc,#ci_s_locId').val('')" title="<fmt:message key="label.report.eliminate"/>"></a>
	              </td>
	          </tr>
	          <%-- <tr>
	              <td><fmt:message key="label.supplier"/></td>
	              <td>
	              	<select id="ci_s_providerAdd" name="ciQueryDTO.providerId" style="width:79%"><option value="0"></option></select>              
	              </td>
	              <td><fmt:message key="ci.brands"/></td>
	              <td>
	              	<select id="ci_s_brandAdd" name="ciQueryDTO.brandId" style="width:79%"><option value="0"></option></select>                                  
	              </td>
	          </tr> --%>
	          
	            <tr>
	              <td><fmt:message key="ci.purchaseDate"/></td>
	              <td>
	  				<input id="s_buyDate"  name="ciQueryDTO.buyDate" class="input"  style="width:70px" readonly/>&nbsp;<fmt:message key="setting.label.to" />
	        		<input id="s_buyEndDate" name="ciQueryDTO.buyEndDate" class="input easyui-validatebox"  style="width:70px" validType="DateComparison['s_buyDate']" readonly/>
	        		<a class="easyui-linkbutton" plain="true" icon="icon-clean" onclick="cleanIdValue('s_buyDate','s_buyEndDate')" title="<fmt:message key="label.request.clear" />"></a>
	  			 </td>
	  			 <td><fmt:message key="ci.assetOriginalValue"/></td>
	              <td>
	              	<input name="ciQueryDTO.assetsOriginalValue" id="s_assetsOriginalValuea" class="easyui-validatebox easyui-numberbox" style="width:70px" min="0"/>
	              	<fmt:message key="setting.label.to" />
	              	<input name="ciQueryDTO.assetsOriginalEndValue" id="s_assetsOriginalEndValue" class="easyui-validatebox easyui-numberbox" validType="NumberComparison['s_assetsOriginalValue']" style="width:70px" min="0"/>
	             </td>
	          </tr>
	          
	          <%-- <tr  id="tr5">
	             <td><fmt:message key="ci.expiryDate" /></td>
	              <td>
	  				<input id="s_wasteTime"  name="ciQueryDTO.wasteTime" class="input"  style="width:70px" readonly/>&nbsp;<fmt:message key="setting.label.to"/>
	        		<input id="s_wasteTimeEnd" name="ciQueryDTO.wasteTimeEnd" class="input easyui-validatebox"  style="width:70px" validType="DateComparison['s_wasteTime']" readonly/>
	        		<a class="easyui-linkbutton" plain="true" icon="icon-clean" onclick="cleanIdValue('s_wasteTime','s_wasteTimeEnd')" title="<fmt:message key="label.request.clear" />"></a>
	  			</td>
	            <td><fmt:message key="ci.lendTime" /></td>
	               <td>
	  				<input id="s_borrowedTime"  name="ciQueryDTO.borrowedTime" class="input"  style="width:70px" readonly/>&nbsp;<fmt:message key="setting.label.to" />
	        		<input id="s_borrowedTimeEnd" name="ciQueryDTO.borrowedTimeEnd" class="input easyui-validatebox"  style="width:70px" validType="DateComparison['s_borrowedTime']" readonly/>
	        		<a class="easyui-linkbutton" plain="true" icon="icon-clean" onclick="cleanIdValue('s_borrowedTime','s_borrowedTimeEnd')" title="<fmt:message key="label.request.clear" />"></a>
	  			  </td>
	          </tr> --%>
	          
	          <%-- <tr   id="tr6">
	             <td><fmt:message key="ci.recycleTime" /></td>
	              <td>
	  				<input id="s_recoverTime"  name="ciQueryDTO.recoverTime" class="input"  style="width:70px" readonly/>&nbsp;<fmt:message key="setting.label.to" />
	        		<input id="s_recoverTimeEnd" name="ciQueryDTO.recoverTimeEnd" class="input easyui-validatebox"  style="width:70px" validType="DateComparison['s_recoverTime']" readonly/>
	        		<a class="easyui-linkbutton" plain="true" icon="icon-clean" onclick="cleanIdValue('s_recoverTime','s_recoverTimeEnd')" title="<fmt:message key="label.request.clear" />"></a>
	  			</td>
	              <td><fmt:message key="ci.plannedRecycleTime" /></td>
	               <td>
	  				<input id="s_expectedRecoverTime"  name="ciQueryDTO.expectedRecoverTime" class="input"  style="width:70px" readonly/>&nbsp;<fmt:message key="setting.label.to" />
	        		<input id="s_expectedRecoverTimeEnd" name="ciQueryDTO.expectedRecoverTimeEnd" class="input easyui-validatebox"  style="width:70px" validType="DateComparison['s_expectedRecoverTime']" readonly/>
	        		<a class="easyui-linkbutton" plain="true" icon="icon-clean" onclick="cleanIdValue('s_expectedRecoverTime','s_expectedRecoverTimeEnd')" title="<fmt:message key="label.request.clear" />"></a>
	  			  </td>
	          </tr> --%>
	          
	          <tr   id="tr7">
	             <td><fmt:message key="ci.arrivalDate"/></td>
	              <td>
	  				<input id="s_arrivalDate"  name="ciQueryDTO.arrivalDate" class="input"  style="width:70px" readonly/>&nbsp;<fmt:message key="setting.label.to" />
	        		<input id="s_arrivalEndDate" name="ciQueryDTO.arrivalEndDate" class="input easyui-validatebox"  style="width:70px" validType="DateComparison['s_arrivalDate']" readonly/>
	        		<a class="easyui-linkbutton" plain="true" icon="icon-clean" onclick="cleanIdValue('s_arrivalDate','s_arrivalEndDate')" title="<fmt:message key="label.request.clear" />"></a>
	  			</td>
	              <td><fmt:message key="ci.warningDate"/></td>
	               <td>
	  				<input id="s_warningDate"  name="ciQueryDTO.warningDate" class="input" style="width:70px" readonly/>&nbsp;<fmt:message key="setting.label.to" />
	        		<input id="s_warningEndDate" name="ciQueryDTO.warningEndDate" class="input easyui-validatebox"  style="width:70px" validType="DateComparison['s_warningDate']" readonly/>
	        		<a class="easyui-linkbutton" plain="true" icon="icon-clean" onclick="cleanIdValue('s_warningDate','s_warningEndDate')" title="<fmt:message key="label.request.clear" />"></a>
	  			  </td>
	         </tr>
	         
	          <tr  id="tr8">
	              <td><fmt:message key="ci.lifeCycle"/></td>
	              <td>
	              	<input name="ciQueryDTO.lifeCycle" id="s_lifeCycle" maxlength="9" class="easyui-numberbox" style="width:70px" min="0"/>
	              	<fmt:message key="setting.label.to" />
	              	<input name="ciQueryDTO.lifeEndCycle" id="s_lifeEndCycle" maxlength="9" class="easyui-numberbox" validType="NumberComparison['s_lifeCycle']" style="width:70px" min="0"/>
	              </td>
	              
	              <td><fmt:message key="ci.warrantyDate"/></td>
	              <td>
	              	<input name="ciQueryDTO.warranty" id="s_warranty" maxlength="9" class="easyui-numberbox" style="width:70px" min="0"/>
	              	<fmt:message key="setting.label.to" />
	              	<input name="ciQueryDTO.endWarranty" id="s_endWarranty" maxlength="9" class="easyui-numberbox" validType="NumberComparison['s_warranty']"  style="width:70px" min="0"/>
	              </td>
	          </tr>
	          
	          <tr>
	              <td><fmt:message key="ci.use"/></td>
	              <td>
	              <input name="ciQueryDTO.userName" id="search_useName" style="width:65%"/>&nbsp;
	              <a class="easyui-linkbutton" plain="true" icon="icon-clean" onclick="cleanIdValue('search_useName')"title="<fmt:message key="label.request.clear"/>"></a>
	              
	              </td>
	              <td><fmt:message key="ci.owner"/></td>
	              <td>
	              <input name="ciQueryDTO.owner" id="search_owner" style="width:77%"/>&nbsp;
	             <a class="easyui-linkbutton" plain="true" icon="icon-clean" onclick="cleanIdValue('search_owner')" title="<fmt:message key="label.request.clear"/>" ></a>
	              </td>  
	          </tr>
	          <%-- <tr  id="tr9">
				 <td><fmt:message key="ci.originalUser"  /></td>
	              <td>
	              <input name="ciQueryDTO.originalUser" id="search_originalUser" style="width:77%"/>
	              <a class="easyui-linkbutton" plain="true" icon="icon-clean" onclick="cleanIdValue('search_originalUser')" title="<fmt:message key="label.request.clear"/>"></a>
	              </td>
	              
	          </tr> --%>
	          <tr>
	          	<td><fmt:message key="common.cerateTime"/></td>
	              <td>
	              	<input id="s_CreateTime"  name="ciQueryDTO.createTime" class="input" style="width:70px" readonly/>&nbsp;<fmt:message key="setting.label.to" />
	        		<input id="s_CreateTimeEnd" name="ciQueryDTO.createTimeEnd" class="input easyui-validatebox"  style="width:70px" validType="DateComparison['s_CreateTime']" readonly/>
	        		<a class="easyui-linkbutton" plain="true" icon="icon-clean" onclick="cleanIdValue('s_CreateTime','s_CreateTimeEnd')" title="<fmt:message key="label.request.clear" />"></a>
	              </td>
	          	<td><fmt:message key="common.updateTime" /></td>
	          	<td >
	          		<input id="s_LastUpdateTime"  name="ciQueryDTO.lastUpdateTimeEnd" class="input" style="width:70px" readonly/>&nbsp;<fmt:message key="setting.label.to" />
	        		<input id="s_LastUpdateTimeEnd" name="ciQueryDTO.lastUpdateTimeEnd" class="input easyui-validatebox"  style="width:70px" validType="DateComparison['s_LastUpdateTime']" readonly/>
	        		<a class="easyui-linkbutton" plain="true" icon="icon-clean" onclick="cleanIdValue('s_LastUpdateTime','s_LastUpdateTimeEnd')" title="<fmt:message key="label.request.clear" />"></a>
	          	</td>
	          </tr>
	           <%-- <tr>
	              <td colspan="4">
	               <a id="moreSearch" onclick="itsm.cim.configureItem.moreCISearch()"><font color="#FF0000"><fmt:message key="label.moreSearch"/></font></a>
	                <a id="lessSearch" onclick="itsm.cim.configureItem.lessCISearch()" style="display:none;"><font color="#FF0000"><fmt:message key="label.hide"/></font></a>
	              </td>
	                
	          </tr> --%>
	          <tr>
	          	<td colspan="4">
	          		<a class="easyui-linkbutton" plain="false" icon="icon-search" onclick="itsm.cim.configureItem.assyCISearch()"><fmt:message key="common.search"/></a>
	          		&nbsp;&nbsp;&nbsp;<a onclick="$('#configureSearchWin input').val('');$('#configureSearchWin select').val('0')" class="easyui-linkbutton" icon="icon-reset">
				<fmt:message key="i18n.reset"/></a>
	          	    <input id="exportConf_sidx" name="sidx" class="input" style="display: none;">
				    <input id="exportConf_sord" name="sord" class="input" style="display: none;">
				    <input id="exportConf_page" name="page" class="input" style="display: none;">
				    <input id="exportConf_rows" name="rows" class="input" style="display: none;">
				    
	          	</td>
	          </tr>
	        </table>
	        </div>
		</form>
	</div> 
	<%-- 自定义列设置窗口 --%>
	<div id="columnChooserWin_configureGrid"  class="WSTUO-dialog" title="<fmt:message key="label.set.column"/>" style="width:250px;height:300px;padding:1px;">
		<div id="columnChooserDiv_configureGrid" class="hisdiv">
			<table class="histable" style="width:100%" cellspacing="1">
				<thead>
				<tr>
					<td colspan="2">
						<a id="columnChooserTopBut_configureGrid" plain="true" class="easyui-linkbutton" icon="icon-ok"><fmt:message key="label.determine"/></a>
						<a id="columnChooserDefaultBut_configureGrid" plain="true" class="easyui-linkbutton" icon="icon-ok"><fmt:message key="label.dashboard.defautl.show"/></a>
						<span id="columnChooserStatus_configureGrid"></span>
					</td>
				</tr>
				
				<tr>
					<th style="padding-left:11px;"><input type="checkbox" id="colAllUnSelect_configureGrid"  onclick="colAllUnSelect('configureGrid')" /></th>
					<th><fmt:message key="label.colName" /></th>
				</tr>
				</thead>
				<tbody>
				</tbody>			
			</table>
		</div>
	</div>
	
	<div id="configureItemMainBottom" class="mainBottom">
		<div id ="configureItemStatsDiv" style="height:290px;float: left;width:100%">
	     	<div class="statsDivSub" >
	      		<div class="statsDivTitle" ><fmt:message key="label.configureItem.count" /></div>
	      	</div>
	      	<div style="padding:0px 0px;">
		      	<div id="configureItemPieDiv" style="height: 290px;">
			      	<div id="configureItemPieDiv1" class="pie pieBorderLeft" style="float: left;height:288px">
		      			<div class="pieChartDescription"><fmt:message key="common.state" /></div>
		      			<div  align="center" style="margin: 0px auto;height: 290px;">
						<table style="width: 100%">
		      			<tbody id="configureItemPie1">
		      			
		      			</tbody>
						</table>
						</div>
					</div>
					<div id="configureItemPieDiv2" class="pie" style="float: left;height:290px">
						<div class="pieChartDescription"><fmt:message key="label.dc.location" /></div>
						<div align="center" style="margin: 0px auto;height: 290px;">
						<table style="width: 100%">
		      			<tbody id="configureItemPie2">
		      			
		      			</tbody>
						</table>
						</div>
					</div>
		      	</div>
			</div>
    	</div> 
    	<div id ="configureItemCompanyDiv" class="companyDiv" <c:if test="${versionType!='ITSOP'}"><c:out value="style=display:none"></c:out></c:if>>
     	<div class="statsDivSub"  >
      		<div  class="statsDivTitle" ><fmt:message key="title.customer.stat" /><a style="color: #15428b" href="javascript:itsm.cim.leftMenu.companyStat()">[<fmt:message key="common.refresh" />]</a></div>
      	</div>
      	<div id="companyConfigureItemDataCount" class="dataCount" >
		
		</div>
		</div>
	</div>
</div>
</div>

