<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../language.jsp" %>


<script src="${pageContext.request.contextPath}/scripts/itsm/cim/softwareMain.js"></script>

<div class="loading" id="softwareMain_loading"><img src="${pageContext.request.contextPath}/images/icons/loading.gif" /></div>

<div class="content" id="softwareMain_content">

<%-- 数据列表 --%>
<table id="softwareMainGrid" ></table>
<div id="softwareMainPager"></div>

<%-- 菜单 --%>
<div id="softwareMainToolbar" style="display:none">
<sec:authorize url="/pages/ciSoftwareAction!saveSoftware.action">
<a class="easyui-linkbutton" plain="true" icon="icon-add" id="link_softwareMain_add"><fmt:message key="common.add"/></a>
</sec:authorize>
<sec:authorize url="/pages/ciSoftwareAction!updateSoftware.action">
	<a class="easyui-linkbutton" plain="true" icon="icon-edit" id="link_softwareMain_edit"><fmt:message key="common.edit"/></a> 
</sec:authorize>
<sec:authorize url="/pages/ciSoftwareAction!deleteSoftware.action">
	<a class="easyui-linkbutton" plain="true" icon="icon-cancel" id="link_softwareMain_delete"><fmt:message key="common.delete"/></a>
</sec:authorize>
<a class="easyui-linkbutton" plain="true" icon="icon-search"  id="link_softwareMain_search"><fmt:message key="common.search"/></a>
</div>
<%-- 新增、编辑 --%>
<div id="softwareMainAddWin" title="<fmt:message key="label_scan_software" />-<fmt:message key="common.add" />/<fmt:message key="common.edit" />" class="WSTUO-dialog" style="width:600px; height:auto;padding:3px">
<form>
	<input name="softwareDTO.softwareId" id="software_add_softwareTypeId" type="hidden" />
	
	<div class="lineTableBgDiv">
	<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td style="width:20%"><fmt:message key="label_software_name" /></td>
			<td style="width:80%"><input name="softwareDTO.softwareName" id="software_add_softwareName" class="input easyui-validatebox" required="true"/></td>
		</tr>
		<tr>
			<td><fmt:message key="label_software_version" /></td>
			<td><input name="softwareDTO.softwareVersion" id="software_add_softwareVersion" class="input" /></td>
		</tr>
		<tr>
			<td><fmt:message key="label_software_type" /></td>
			<td>
				<select id="software_add_softwareType" name="softwareDTO.softwareTypeId" class="input"></select>
			</td>
		</tr>
		<tr>
			<td><fmt:message key="label_software_category" /></td>
			<td>
				<input name="softwareDTO.softwareCategoryName" id="software_add_softwareCategoryName" class="choose" readonly="readonly"/>
				<input name="softwareDTO.softwareCategoryId" id="software_add_softwareCategoryId" type="hidden" />
			</td>
		</tr>
		<tr>
			<td><fmt:message key="title.user.selectSupplier" /></td>
			<td>
				<select id="software_add_softwareProvider" name="softwareDTO.softwareProviderId" class="input"></select>
			</td>
		</tr>
		<tr>
			<td><fmt:message key="config_operatingSystem_installDate" /></td>
			<td>
				<input name="softwareDTO.installDate" id="software_add_installDate"  class="input" readonly="readonly" />
			</td>
		</tr>
		<tr>
			<td><fmt:message key="label.user.description" /></td>
			<td><textarea style="height: 100px;" name="softwareDTO.description" id="software_add_description" class="input" ></textarea></td>
		</tr>
		<tr>
			<td colspan="2" align="center">
			<a class="easyui-linkbutton" icon="icon-save" id="link_softwareMain_save"><fmt:message key="common.save" /></a>
			</td>
		</tr>
	</table>
	</div>
</form>
</div>
<%-- 搜索 --%>
<div id="softwareMainSearchWin" title="<fmt:message key="common.search" />" class="WSTUO-dialog" style="width:600px; height:auto;padding:3px">
<form>
	<div class="lineTableBgDiv">
	<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td><fmt:message key="label_software_name" /></td>
			<td><input name="qdto.softwareName" class="input" /></td>
		</tr>
		<tr>
			<td><fmt:message key="label_software_version" /></td>
			<td><input name="qdto.softwareVersion" id="software_search_softwareVersion" class="input" /></td>
		</tr>
		<tr>
			<td><fmt:message key="label_software_type" /></td>
			<td>
				<select id="software_search_softwareType" name="qdto.softwareTypeId" class="input"></select>
			</td>
		</tr>
		<tr>
			<td><fmt:message key="label_software_category" /></td>
			<td>
				<input name="qdto.softwareCategoryName" id="software_search_softwareCategoryName" class="choose" style="width: 85%" readonly="readonly"/>
				<input name="qdto.softwareCategoryId" id="software_search_softwareCategoryId" type="hidden" />
				<a class="easyui-linkbutton" plain="true" icon="icon-clean" onclick="cleanIdValue('software_search_softwareCategoryId','software_search_softwareCategoryName')" title="<fmt:message key="label.request.clear" />"></a>
			</td>
		</tr>
		<tr>
			<td><fmt:message key="title.user.selectSupplier" /></td>
			<td>
				<select id="software_search_softwareProvider" name="qdto.softwareProviderId" class="input"></select>
			</td>
		</tr>
		<tr>
			<td><fmt:message key="label.user.description" /></td>
			<td><input name="qdto.description" class="input" /></td>
		</tr>
		<tr>
			<td colspan="2" align="center">
			<a class="easyui-linkbutton" icon="icon-search" id="link_softwareMain_search_opt"><fmt:message key="common.search" /></a>
			</td>
		</tr>
	</table>
	</div>
</form>
</div>

		<%-- 自定义列设置窗口 --%>
	<div id="columnChooserWin_softwareMainGrid"  class="WSTUO-dialog" title="<fmt:message key="label.set.column"/>" style="width:250px;height:300px;padding:1px;">
		<div id="columnChooserDiv_softwareMainGrid" class="hisdiv">
			<table class="histable" style="width:100%" cellspacing="1">
				<thead>
				<tr>
					<td colspan="2">
						<a id="columnChooserTopBut_softwareMainGrid" plain="true" class="easyui-linkbutton" icon="icon-ok"><fmt:message key="label.determine"/></a>
						<a id="columnChooserDefaultBut_softwareMainGrid" plain="true" class="easyui-linkbutton" icon="icon-ok"><fmt:message key="label.dashboard.defautl.show"/></a>
						<span id="columnChooserStatus_softwareMainGrid"></span>
					</td>
				</tr>
				
				<tr>
					<th style="padding-left:11px;"><input type="checkbox" id="colAllUnSelect_softwareMainGrid"  onclick="colAllUnSelect('softwareMainGrid')" /></th>
					<th><fmt:message key="label.colName" /></th>
				</tr>
				</thead>
				<tbody>
				</tbody>			
			</table>
		</div>
	</div>

</div>