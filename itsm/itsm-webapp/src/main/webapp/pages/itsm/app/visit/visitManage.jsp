<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../../language.jsp" %>

<script src="../scripts/itsm/app/visit/visitManage.js?random=<%=new java.util.Date().getTime()%>"></script>

<div class="loading" id="visitManage_loading"><img src="../images/icons/loading.gif" /></div>

<div id="visitManage_panel" class="content">

<table id="visitList"></table>
<div id="visitPager"></div>
<div id="visitToolbar" style="display:none">
	<sec:authorize url="/pages/dataDictionaryItems!save.action">
	<a class="easyui-linkbutton" plain="true" icon="icon-add" onclick="itsm.app.visit.visitManage.addvisit_win()" title="<fmt:message key="common.add"/>"></a>
	</sec:authorize>
	<sec:authorize url="/pages/dataDictionaryItems!merge.action">
	<a class="easyui-linkbutton" plain="true" icon="icon-edit" onclick="itsm.app.visit.visitManage.editvisit_win()" title="<fmt:message key="common.edit"/>"></a>
	</sec:authorize>
	<sec:authorize url="/pages/dataDictionaryItems!delete.action">
	<a class="easyui-linkbutton" plain="true" icon="icon-cancel" onclick="itsm.app.visit.visitManage.deletevisit_opt()" title="<fmt:message key="common.delete"/>"></a>
	</sec:authorize>
	
	<a class="easyui-linkbutton" plain="true" icon="icon-search" onclick="itsm.app.visit.visitManage.searchvisit_win()" title="<fmt:message key="common.search"/>"></a>
	
</div>

<!--添加回访事项 -->
<div id=addvisit_win class="WSTUO-dialog" title="<fmt:message key="titie.sys.addVisit"/>" style="width:350px;height:auto">
		<form id="addvisitForm">	
		 <div class="lineTableBgDiv" >
    			<table class="lineTable" cellspacing="1"> 
				<tr>
					<td><fmt:message key="label.name"/></td>
					<td><input name="visitDTO.visitName" id="add_visitName" class="easyui-validatebox input" validType="nullValueValid" required="true"/></td>
				</tr>
				<tr>
					<td><fmt:message key="label.type"/></td>
					<td>
						<select name="visitDTO.visitItemType" class="select">
							<option value="radio"><fmt:message key="label.radio"/></option>
							<option value="text"><fmt:message key="label.text"/></option>
							<option value="Lob"><fmt:message key="label.longText"/></option>
							<option value="Integer"><fmt:message key="label.integer"/></option>
						</select>
					</td>
				</tr>
				<tr>
					<td><fmt:message key="common.sort"/></td>
					<td><input name="visitDTO.visitOrder" class="easyui-numberbox input" /></td>
				</tr>
				<tr>
				<td><fmt:message key="common.status"/></td>
				<td>
					<input type="radio" name="visitDTO.useStatus" id="roleState" value="true" checked="checked" /><fmt:message key="common.enable"/>
					<input type="radio" name="visitDTO.useStatus" id="roleState1" value="false" /><fmt:message key="common.disable"/>
				</td>
				</tr>
				<tr>
					<td colspan="2">
						<br>
						<a class="easyui-linkbutton" plain="false" icon="icon-save" onclick="itsm.app.visit.visitManage.addvisit_opt()" ><fmt:message key="common.save"/></a>
					</td>
				</tr>
			</table>
			</div>
		</form>
</div>



<!--编辑回访事项 -->
<div id=editvisit_win class="WSTUO-dialog" title="<fmt:message key="titie.sys.editVisit"/>" style="width:350px;height:auto">
		<form id="editvisitForm">
		<div class="lineTableBgDiv" >
    			<table class="lineTable" cellspacing="1"> 
    				
				<tr>
					<td><fmt:message key="label.name"/></td>
					<td><input name="visitDTO.visitName" id="visitName" class="easyui-validatebox input"  validType="nullValueValid" required="true"/></td>
				</tr>
				<tr>
					<td><fmt:message key="label.type"/></td>
					<td>
						<select name="visitDTO.visitItemType" id="visitItemType" class="select">
							<option value="radio"><fmt:message key="label.radio"/></option>
							<option value="text"><fmt:message key="label.text"/></option>
							<option value="Lob"><fmt:message key="label.longText"/></option>
							<option value="Integer"><fmt:message key="label.integer"/></option>
						</select>
					</td>
				</tr>
				<tr>
					<td><fmt:message key="common.sort"/></td>
					<td><input name="visitDTO.visitOrder" id="visitOrder"  class="easyui-numberbox input" /></td>
				</tr>
				<tr>
				<td><fmt:message key="common.status"/></td>
				<td>
					<input type="radio" name="visitDTO.useStatus" id="visitState" value="true" checked="checked" /><fmt:message key="common.enable"/>
					<input type="radio" name="visitDTO.useStatus" id="visitState1" value="false" /><fmt:message key="common.disable"/>
				</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="hidden" name="visitDTO.visitNo" id="visitNo"/>
						<a class="easyui-linkbutton" plain="false" icon="icon-save" onclick="itsm.app.visit.visitManage.editvisit_opt()" ><fmt:message key="common.save"/></a>
					</td>
				</tr>
			</table>
			</div>
		</form>
</div>


<!-- 搜索 -->
	<div id="searchvisit_win" class="WSTUO-dialog" title="<fmt:message key="common.search"/>" style="width:400px;height:auto">
		<form id="searchvisitForm">	
			 <div class="lineTableBgDiv" >
    			<table class="lineTable" cellspacing="1"> 
				<tr>
					<td><fmt:message key="label.name"/></td>
					<td><input name="visitDTO.visitName" class="input" /></td>
				</tr>
				<tr>
					<td colspan="2">
						<br>
						<a class="easyui-linkbutton" plain="false" icon="icon-search" onclick="itsm.app.visit.visitManage.searchvisit_opt()"><fmt:message key="common.search"/></a>
					</td>
				</tr>
			</table>
			</div>
		</form>
	</div>



<!-- 单选复选选择值 -->
<div style="height:3px"></div>
<table id="visitItemList"></table>
<div id="visitItemPager"></div>
<div id="visitItemToolbar" style="display:none">
	<sec:authorize url="/pages/dataDictionaryItems!save.action">
	<a class="easyui-linkbutton" plain="true" icon="icon-add" onclick="itsm.app.visit.visitManage.addVisitItem_win()" title="<fmt:message key="common.add"/>"></a>
	</sec:authorize>
	<sec:authorize url="/pages/dataDictionaryItems!merge.action">
	<a class="easyui-linkbutton" plain="true" icon="icon-edit" onclick="itsm.app.visit.visitManage.editVisitItem_win()" title="<fmt:message key="common.edit"/>"></a>
	</sec:authorize>
	<sec:authorize url="/pages/dataDictionaryItems!delete.action">
	<a class="easyui-linkbutton" plain="true" icon="icon-cancel" onclick="itsm.app.visit.visitManage.deleteVisitItem_opt()" title="<fmt:message key="common.delete"/>"></a>
	</sec:authorize>
</div>


<!-- 新增选项值 -->
<div id=addVisitItem_win class="WSTUO-dialog" title="<fmt:message key="tiile.sys.addVisitItem"/>" style="width:300px;height:auto">
	<form id="addVisitItemForm">
		<div class="lineTableBgDiv" >
    			<table class="lineTable" cellspacing="1"> 
			<tr>
				<td><fmt:message key="setting.label.optionValue"/></td>
				<td><input name="visitItemDTO.visitItemName" id="edit_visitItemName" class="easyui-validatebox input"  validType="nullValueValid"  required="true"></input></td>
			</tr>
			<tr>
				<td colspan="2">
					
					<a class="easyui-linkbutton" plain="false" icon="icon-save" onclick="itsm.app.visit.visitManage.addVisitItem_opt()" ><fmt:message key="common.save"/></a>
				</td>
			</tr>
		</table>
		</div>
	</form>
</div>	

<!-- 编辑选项值 -->
<div id=editVisitItem_win class="WSTUO-dialog" title="<fmt:message key="tiile.sys.editVisitItem"/>" style="width:300px;height:auto">
	<form id="editVisitItemForm">
	<div class="lineTableBgDiv" >
    			<table class="lineTable" cellspacing="1"> 
		
			<tr>
				<td><fmt:message key="setting.label.optionValue"/></td>
				<td><input name="visitItemDTO.visitItemName" id="visitItemName" class="easyui-validatebox input"  validType="nullValueValid" required="true"></input></td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="hidden" name="visitItemDTO.visitItemNo" id="visitItemNo" class="input"/>
					<a class="easyui-linkbutton" plain="false" icon="icon-save" onclick="itsm.app.visit.visitManage.editVisitItem_opt()" ><fmt:message key="common.save"/></a>
				</td>
			</tr>
		</table>
		</div>
	</form>
</div>	





</div>