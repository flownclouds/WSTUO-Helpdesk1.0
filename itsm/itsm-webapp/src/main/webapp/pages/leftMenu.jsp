<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="com.wstuo.moduleManage.dto.ModuleManageToolDTO"%>
<%@page import="com.wstuo.moduleManage.service.IModuleManageService"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div id="index_leftMenu" onselectstart="return false;" >
    <!-- 一级菜单 -->
    <div id="leftMenu_first">
        <div id="leftMenu_menuUp"></div>
        <ul class="leftMenu_first_items">
            <!-- 一级菜单我的首页 -->
             <li id="protal_link" class="ico-home" title="<fmt:message key="title.my.home"/>">
                <span icon="ico-index" ></span>
            </li> 
            <!-- 一级菜单 快速新建 -->
            <li id="firstMenu_new" class="ico-new" tips="<fmt:message key="title.newCreate"/>" title="<fmt:message key="title.newCreate"/>">
                <div class="hasSubItem"></div>
            </li>
            <%
            ApplicationContext acx = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
            IModuleManageService moduleManageService=(IModuleManageService)acx.getBean("moduleManageService");
            ModuleManageToolDTO toolDto=moduleManageService.showModule(session.getAttribute("loginUserName").toString());
            request.setAttribute("showModules", toolDto.getModuleDtos());
            request.setAttribute("moduleRes", toolDto.getModuleRes());
            request.setAttribute("moduleHasShow", toolDto.getModuleHasShow());
            %>
            <!-- 一级菜单 请求问题变更发布配置项 -->
            <c:forEach var="module" items="${showModules}">
            <script>$import('itsm.${module.moduleName}.${module.moduleName}Top');</script>
            <c:set var="moduleRes1" value="${module.moduleName1}_MAIN"></c:set>
            <c:set var="moduleRes2" value="/pages/${module.moduleName}!find${module.moduleName2}Pager.action"></c:set>
            <c:if test="${moduleHasShow[module.moduleName] eq true}">
                <c:if test="${moduleRes[moduleRes1]}">
                    <c:if test="${moduleRes[moduleRes2]}">
                        <li class="ico-${module.moduleName} tag" id="firstMenu_${module.moduleName}" onclick="itsm.${module.moduleName}.${module.moduleName}Top.menuClick();" tips="<fmt:message key="title.mainTab.${module.moduleName}.manage"/>">
                        </li>
                    </c:if>
                </c:if>
            </c:if>
            </c:forEach>
            <!-- 一级菜单 SLA协议管理 -->
            <sec:authorize url="SLA_XIEYIGUANLI">
                <sec:authorize url="/pages/slaContractManage!find.action">
                    <li   class="ico-sla tag" id="firstMenu_sla" tips="<fmt:message key="title.mainTab.sla"/>">
                    </li>
                </sec:authorize>
            </sec:authorize>
            <!-- 一级菜单 知识库 -->
            <sec:authorize url="KNOWLEDGEBASE_MAIN">
                <sec:authorize url="KNOWLEGEINFO_VIEWKNOWLEGE">
                    <li class="ico-knowledge tag" id="firstMenu_Knowledge" tips="<fmt:message key="title.mainTab.knowledgeBase"/>">
                    </li>
                </sec:authorize>
            </sec:authorize>
            <!-- 一级菜单 报表管理 -->
            <sec:authorize url="REPORT_MAIN">
                <li class="ico-10 tag" id="reportTopMenu" title="<fmt:message key="title.mainTab.report.export"/>" tips="<fmt:message key="title.mainTab.report.export"/>">
                    <div class="hasSubItem"></div>
                </li>
            </sec:authorize>
            <!-- 一级菜单 辅助工具 -->
            <li class="ico-tools tag" id="toolsTopMenu" title="<fmt:message key="title.mainTab.tool"/>" tips="<fmt:message key="title.mainTab.tool"/>">
                    <div class="hasSubItem"></div>
            </li>
            <!-- 一级菜单 基础设置 -->
            <sec:authorize url="BASICSET_MAIN">
                <li class="ico-systemSetting tag" id="basicSetTopMenu" tips="<fmt:message key="title.mainTab.systemSet"/>">
                </li>
            </sec:authorize>
        </ul>
        <div id="leftMenu_menuDown"></div>
    </div>
</div>




