<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../language.jsp" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><fmt:message key="i18n.mainTitle"/></title>

	<link rel="stylesheet" type="text/css" href="/itsm-webapp/scripts/jquery/easyui/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="/itsm-webapp/scripts/jquery/easyui/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="/itsm-webapp/scripts/jquery/jqgrid/redmond/jquery-ui-1.8.1.custom.css" />
	<link rel="stylesheet" type="text/css" href="/itsm-webapp/scripts/jquery/jqgrid/css/jqgrid.css" /> 
    <link rel="stylesheet" type="text/css" href="/itsm-webapp/styles/style.css">
	<link rel="stylesheet" type="text/css" href="/itsm-webapp/styles/comm.css">
	<link rel="stylesheet" type="text/css" href="/itsm-webapp/scripts/jquery/easyui/datetimepicker/jquery-calendar.css"/>
	<link rel="stylesheet" type="text/css" href="/itsm-webapp/scripts/jquery/easyui/datetimepicker/styles.css"/>
	<!--jquery ui 样式-->
	<script src="/itsm-webapp/scripts/jquery/jquery-1.4.2.min.js"></script>
	<script src="/itsm-webapp/scripts/jquery/jquery-ui-1.8.2.custom/js/jquery-ui-1.8.2.custom.min.js"></script>
	<script src="/itsm-webapp/scripts/jquery/easyui/jquery.easyui.min.js"></script>

	

</head>

<body>

<div class="WSTUO-dialog" title="<fmt:message key="msg.tips"/>" style="width:350px;height:auto">
    
<div class="lineTableBgDiv">
		<table style="width:100%" class="lineTable" cellspacing="1">
	<tr>
		
		<td>
			<div style="padding:10px;color:#FF0000;line-height:25px"><fmt:message key="msg.606"/></div>
		</td>
	</tr>
	
	<tr>
		<td >

			<a class="easyui-linkbutton" icon="icon-undo" onclick="javascript:window.location='${pageContext.request.contextPath}/pages/login.jsp'"><fmt:message key="common.login"/></a>
			
		</td>
	</tr>
</table>
</div>
    
    
</div>


</body>

</html>