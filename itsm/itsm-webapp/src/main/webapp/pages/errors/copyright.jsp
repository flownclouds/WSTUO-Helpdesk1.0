<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../language.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="i18n.mainTitle"/></title>
<style>
.lineTableBgDiv{
	background:#eee;
	margin:3px;
}
</style>
</head>
<body>
<div class="lineTableBgDiv" >
	<table style="width:100%;padding-top:20px;padding-bottom:20px;" class="lineTable" cellspacing="1">
		<tr>
			<td style="line-height:26px;font-size:22px;color:#FF0000;">
				<fmt:message key="copyright_warning_sys" /><br/>
				<fmt:message key="copyright_warning_iolation" /><br/>
				<fmt:message key="copyright_warning_website" />
				<fmt:message key="copyright_warning_thank" /><br/>
			</td>
		</tr>
	</table>
</div>
</body>
</html>