<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.opensymphony.xwork2.ActionContext" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../../language.jsp" %>
<script type="text/javascript">
startProcess();
<!--
var scheduledTaskId='${param.scheduledTaskId}';
//-->
</script>
<link id="editRequestFormCustom_is_scheduled" href="../styles/common/editScheduledTaskFormCustom.css"  rel="stylesheet" type="text/css"/> 
<link id="editRequestFormCustom_no_scheduled" href="../styles/common/editScheduledTaskFormCustom-no.css"  rel="stylesheet" type="text/css"/> 
<script src="${pageContext.request.contextPath}/scripts/common/tools/scheduledTask/editScheduledTaskFormCustom.js"></script>
<div class="loading" id="editScheduledTask_loading"><img src="${pageContext.request.contextPath}/images/icons/loading.gif" /></div>

	<div class="easyui-layout" fit="true" id="editScheduledTask_layout" style="display: none">
		<div region="north" border="false" class="panelTop">
			<!-- 新的面板 start-->
			<div id="editScheuledTaskDiv" style="padding: 2px">
			<a id="editScheduledTaskBtn" class="easyui-linkbutton" plain="true" icon="icon-save" style="margin-right:15px" title="<fmt:message key="common.save"/>"></a>
			<a id="edit_scheduledTask_backList" class="easyui-linkbutton" plain="true" icon="icon-undo" style="margin-right:15px" title="<fmt:message key="common.returnToList"/>"></a>
			</div>
		</div>
		<!-- 扩展信息 start -->
		<div region="center" class="easyui-panel" fit="true" title="">
			<div>
				<form id="editScheduledTaskForm">
					<input id="edit_scheduledTask_scheduledTaskId" type="hidden" name="scheduledTaskDTO.scheduledTaskId" value="${param.scheduledTaskId}" />
					<input id="edit_scheduledTask_requestServiceDirNos" type="hidden" name="requestDTO.requestServiceDirNos" />
					<input id="editRequestScheduledTask_formId" type="hidden" name ="requestDTO.formId" />
					<input id="scheduledTaskType" type="hidden" name="scheduledTaskDTO.scheduledTaskType">
					<input id="editRequestScheduledTask_isNewForm" type="hidden" name ="requestDTO.isNewForm" />
					<div style="background-color: #E0ECFF;float:left;width:100%;margin-top:5px;padding:5px;font-weight: bold;"><fmt:message key="common.basicInfo" /></div>
					<div id="editScheduledTaskRequest_formField" style="width: 90%;margin:0 auto;">
						
						
					</div>
				</form>
			</div>
			<div class="easyui-tabs" fit="true" border="false" id="editScheduledTask_tab">

				<%-- <div title="<fmt:message key="title.request.requesterInfo" />/<fmt:message key="title.request.withCI" />" style="overflow:hidden;">
					<div class="lineTableBgDiv">
						<table style="width:100%" class="lineTable" cellspacing="1">
							 
							
						</table>
					  </div>
					  
				</div> --%>
				<c:if test="${cimHave eq true}">
				<div title='<fmt:message key="label.request.withAssets" />'>
				<form>
				<div class="hisdiv">
						<table class="histable" width="100%" cellspacing="1" id="scheduledTask_edit_relatedCIShow">
							<thead>
							<tr>
								<td colspan="5" style="text-align:left">
									<a id="edit_scheduledTask__ref_ci_btn" class="easyui-linkbutton" plain="true" icon="icon-add" title="<fmt:message key="common.add" />"></a>
								</td>
							</tr>
							<tr height="20px">
								<th align="center"><fmt:message key="lable.ci.assetNo" /></th>
								<th align="center"><fmt:message key="label.name"/></th>
								<th align="center"><fmt:message key="common.category" /></th>
								<th align="center"><fmt:message key="common.state" /></th>
								<th align="center"><fmt:message key="ci.operateItems" /></th>
							</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div></form>
				</div>
				</c:if>
				
					<%-- 		<div title='<fmt:message key="label.ci.ciServiceDir" />'>
						<form>
					<div class="hisdiv">
						<table class="histable" id="edit_scheduledTask_serviceDirectory" style="width:100%" cellspacing="1">
						<thead>
						<tr>
							<td colspan="3" style="text-align:left">
								<a id="edit_scheduledTask_service_add" class="easyui-linkbutton" plain="true" icon="icon-add" title="<fmt:message key="common.add" />"></a>
							</td>
						</tr>
						<tr height="20px">
							<th align="center"><fmt:message key="common.id"/></th>
							<th align="center"><fmt:message key="relevance.service.name"/> </th>
							<th align="center"><fmt:message key="label.service.scores"/> </th>
							<th align="center"><fmt:message key="label.rule.operation"/> </th>
						</tr>
						</thead>
						<!-- 服务目录显示 -->
						<tbody id="edit_scheduledTask_serviceDirectory_tbody">
							
						</tbody>
						
						</table>
					</div></form>
				</div> --%>
				<%-- <div title="<fmt:message key="config.extendedInfo"/>" style="overflow:hidden;">
					<form>
					<div class="hisdiv" id="scheduledTask_edit_eavAttributet">
						<table style="width:100%" class="histable" cellspacing="1">
							<thead>
								<tr>
									<!-- <th><fmt:message key="common.attributeName" /></th><th><fmt:message key="common.attributeValue" /></th> -->
								</tr>
							</thead>
							<tbody></tbody>
              			</table>
                	</div></form>
				</div> --%>
			
				<div title="<fmt:message key="common.attachment" />" style="padding:3px;">
					<form id="edit_scheduledTask_eavAttributet_form">
					<div class="lineTableBgDiv" id="edit_scheduledTask_attachment_div" >
							<input type="hidden" name="scheduledTaskDTO.requestDTO.attachmentStr" id="edit_scheduledTask_attachmentStr"/>
							<table width="100%" cellspacing="1" class="lineTable">
								<tr>
									<td> 
									<div style="color:#ff0000;line-height:28px"><fmt:message key="msg.attachment.maxsize" /></div>
									<div style="height:5px"></div>
										<div class="diyLinkbutton">
											<div style="float: left;cursor: pointer;">
											<input type="file"  name="filedata" id="edit_scheduledTask_file">
											</div>
											
											<div style="float: left;margin-left: 15px;"><a class="easyui-linkbutton" icon="icon-upload" href="javascript:$('#edit_scheduledTask_file').uploadifyUpload()" ><fmt:message key="label.attachment.upload" /></a>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div style="padding-top:3px">
							              <div id="edit_scheduledTask_success_attachment" style="line-height:25px;color:#555"></div>
							            </div>
									</td>
								</tr>
							</table>
					</div></form>
				</div>
				<div title="*<fmt:message key="title.scheduled.time.settings" />" style="padding:0px;" id="editScheduledTimeSettings">
				<form>
					<div class="lineTableBgDiv">
						<table style="width:100%" class="lineTable" cellspacing="1">
							<tr>
								<td>
									<div class="lineTableBgDiv" class="lineTable" cellspacing="1">
									<table width="100%">
										<tr>
											<td>
												<input type="radio" name="scheduledTaskDTO.timeType" id="timeType_day" value="day" onclick="common.tools.scheduledTask.scheduledTask.everyWhatChange(this.value,'edit_')" checked="checked">
												<fmt:message key="title.scheduled.day.plan" />
												<input type="radio" name="scheduledTaskDTO.timeType" id="timeType_weekly" value="weekly" onclick="common.tools.scheduledTask.scheduledTask.everyWhatChange(this.value,'edit_')">
												<fmt:message key="title.scheduled.week.plan" />
												<input type="radio" name="scheduledTaskDTO.timeType" id="timeType_month" value="month" onclick="common.tools.scheduledTask.scheduledTask.everyWhatChange(this.value,'edit_')">
												<fmt:message key="title.scheduled.month.plan" />
												<input type="radio" name="scheduledTaskDTO.timeType" id="timeType_cycle" value="cycle" onclick="common.tools.scheduledTask.scheduledTask.everyWhatChange(this.value,'edit_')">
												<fmt:message key="title.scheduled.cycle.plan" />
												<%-- 周期性计划(分钟) --%>
												<input type="radio" name="scheduledTaskDTO.timeType" id="timeType_cycleMinute" value="cycleMinute" onclick="common.tools.scheduledTask.scheduledTask.everyWhatChange(this.value,'edit_')">
												<fmt:message key="title.scheduled.cycle.plan" />(<fmt:message key="label.sla.minute" />)
												<input type="radio" name="scheduledTaskDTO.timeType" id="timeType_on_off" value="on_off" onclick="common.tools.scheduledTask.scheduledTask.everyWhatChange(this.value,'edit_')">
												<fmt:message key="title.scheduled.on-off.plan" />
											</td>
										</tr>
										<tr>
											<td>
												<b>[<span id="edit_everyWhat_show"><fmt:message key="label.scheduledTask.day" /></span>]</b><br>
												
												<div id="edit_everyWhat_weekly" style="display: none;" class="lineTableBgDiv">
													<!--每周定时维护： -->
													
													
													<table width="100%" class="lineTableBgDiv" class="lineTable" cellspacing="1">
														<tr><td colspan="4"><input type="checkbox" id="edit_scheduledTask_weekWeeks_all" value="1" onclick="common.tools.scheduledTask.scheduledTask.checkAll('edit_scheduledTask_weekWeeks_all','edit_everyWhat_weekly','scheduledTaskDTO.weekWeeks')" />
														<fmt:message key="label.date.weekly" />：</td></tr>
														<tr>
															<td><input type="checkbox" id="request_checkbox_SUN" value="SUN" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.sunday" /></td>
															<td><input type="checkbox" id="request_checkbox_MON" value="MON" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.monday" /></td>
															<td><input type="checkbox" id="request_checkbox_TUE" value="TUE" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.tuesday" /></td>
															<td><input type="checkbox" id="request_checkbox_WED" value="WED" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.wednesday" /></td>
														</tr>
														<tr>
															<td><input type="checkbox" id="request_checkbox_THU" value="THU" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.thursday" /></td>
															<td><input type="checkbox" id="request_checkbox_FRI" value="FRI" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.friday" /></td>
															<td><input type="checkbox" id="request_checkbox_SAT" value="SAT" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.saturday" /></td>
															<td></td>
														</tr>
													</table>
												</div>
												
												<div id="edit_everyWhat_monthly" style="display: none;"  class="lineTableBgDiv">
													<!-- 每月定时维护：  -->
													
													<table width="100%" class="lineTableBgDiv" class="lineTable" cellspacing="1">
														<tr><td colspan="4"><input type="checkbox" value="1" id="edit_scheduledTask_monthMonths_all" onclick="common.tools.scheduledTask.scheduledTask.checkAll('edit_scheduledTask_monthMonths_all','edit_everyWhat_monthly','scheduledTaskDTO.monthMonths')" />
														<fmt:message key="label.date.month" />：</td></tr>
														<tr>
															<td><input type="checkbox" id="request_checkbox_JAN" value="JAN" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Jan" /></td>
															<td><input type="checkbox" id="request_checkbox_FEB" value="FEB" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Feb" /></td>
															<td><input type="checkbox" id="request_checkbox_MAR" value="MAR" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Mar" /></td>
															<td><input type="checkbox" id="request_checkbox_APR" value="APR" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Apr" /></td>
														</tr>
														<tr>
															<td><input type="checkbox" id="request_checkbox_MAY" value="MAY" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.May" /></td>
															<td><input type="checkbox" id="request_checkbox_JUN" value="JUN" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Jun" /></td>
															<td><input type="checkbox" id="request_checkbox_JUL" value="JUL" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Jul" /></td>
															<td><input type="checkbox" id="request_checkbox_AUG" value="AUG" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Aug" /></td>
														</tr>
														<tr>
															<td><input type="checkbox" id="request_checkbox_SEP" value="SEP" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Sep" /></td>
															<td><input type="checkbox" id="request_checkbox_OCT" value="OCT" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Oct" /></td>
															<td><input type="checkbox" id="request_checkbox_NOV" value="NOV" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Nov" /></td>
															<td><input type="checkbox" id="request_checkbox_DEC" value="DEC" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Dec" /></td>
														</tr>
														<tr><td colspan="4">
															<fmt:message key="common.date"></fmt:message>：
															<select name="scheduledTaskDTO.monthDay" id="edit_scheduledTask_monthDay">
															</select>
														</td></tr>
													</table>

													
												</div>
												
												<div id="edit_everyWhat_cycle" style="display: none;">
													<!-- 周期性地维护： ：  -->
													<hr>
													<fmt:message key="label.scheduledTask.every"></fmt:message>&nbsp;&nbsp;<input type="text" width="12px" class="easyui-numberbox" value="1" id="edit_scheduledTask_cyclicalDay" name="scheduledTaskDTO.cyclicalDay">
													<fmt:message key="label.scheduledTask.day.one"></fmt:message>
												</div>
												
												<%-- 周期性执行(分钟) --%>
												<div id="edit_everyWhat_cycleMinute" style="display: none;">
													
													<hr>
													<fmt:message key="label.scheduledTask.every"></fmt:message>&nbsp;&nbsp;
													<input type="text" width="12px" class="easyui-numberbox" value="1" id="edit_scheduledTask_cyclicalMinute" name="scheduledTaskDTO.cyclicalMinute">
													<fmt:message key="label.sla.minute" />
													
												</div>
												
											</td>
										</tr>
										<tr>
											<td>
													<div id="edit_scheduledTask_taskDate" style="width: 100%">
													<span id="edit_scheduledTask_startTime"><fmt:message key="label.scheduledTask.form" /></span>&nbsp;&nbsp;
													<input type="text" style="width:75%" id="edit_scheduledTask_startDate_input" name="scheduledTaskDTO.taskDate" class="input" readonly>
													
													<div id="edit_scheduledTask_endTime_div">
													<br>
													<span id="edit_scheduledTask_endTime"><fmt:message key="label.sla.slaEndTime" /></span>&nbsp;&nbsp;
													<input type="text" style="width:75%" name="scheduledTaskDTO.taskEndDate" id="edit_scheduledTask_endDate_input" class="input easyui-validatebox" validType="DateComparison['edit_scheduledTask_startDate_input']" readonly></div>
													
													<hr></div>
													
													<div id="edit_scheduledTask_taskHour_taskMinute">
														<fmt:message key="label.scheduledTask.specific.time" />：
														<select name="scheduledTaskDTO.taskHour" id="edit_scheduledTask_hours">
															<c:forEach var="i" begin="0" end="23" step="1"> 
																<option value="${i}">${i}</option>
															</c:forEach>
														</select>
														<fmt:message key="label.orgSettings.hour" />
														<!-- 分 -->
														
														<select name="scheduledTaskDTO.taskMinute" id="edit_scheduledTask_minute">
															<c:forEach var="i" begin="0" end="59" step="1"> 
																<option value="${i}">${i}</option>
															</c:forEach>
														</select>
														<fmt:message key="label.orgSettings.minute" />
													</div>
											</td>
										</tr>
									</table>
									</div>
								</td>
							</tr>
						</table>
					</div>	</form>
				</div>
				
			</div>
		</div>	
		<!-- 扩展信息 end -->

	</div>

	<div id="editScheduledTask_codeAndServices" style="display: none">
		<div class="field_options"><div class="label"><fmt:message key="label.ci.ciServiceDir" /></div><div class="field"><input  id="editScheduledTaskField_requestServiceDirs"  style="width: 90%;" disabled="disabled" value=""  class="easyui-validatebox input control-img validatebox-text ui-autocomplete-input" type="text"></div></div>
		<div class="field_options"><div class="label"></div><div class="field"></div></div>
	</div>
	<script type="text/javascript">
		$(function(){
			setTimeout(function(){
				common.tools.scheduledTask.editScheduledTaskFormCustom.findScheduledTaskById(scheduledTaskId);// 加载定期任务详细
			},0);
		});
	</script>
