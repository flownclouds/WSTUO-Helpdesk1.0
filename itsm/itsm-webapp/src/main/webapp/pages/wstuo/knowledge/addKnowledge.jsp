<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ include file="../../language.jsp" %>
<script type="text/javascript">
var pageType="${param.pageType}";
var editTemplateId="${param.templateId}";
var problem_eno='${param.problemEno}';
$("#addKnowledge_attachments").val("");
</script>
<script src="${pageContext.request.contextPath}/js/wstuo/knowledge/addKnowledge.js?random=<%=new java.util.Date().getTime()%>"></script>

<form id="addKnowledgeForm" event="wstuo.knowledge.addKnowledge.checkKnowledgeTitle">
<div class="row" id="addKnowledge_contentPanel">
   	<div class="box col-md-12">
		<div class="box-inner">
		<div class="box-header well" data-original-title="">
			<h2>
				<i class="glyphicon glyphicon-plus"></i>新增知识
			</h2>
			<div class="box-icon"></div>
		</div>
		<div class="box-content">
		  <table style="width:100%;" cellspacing="1">
		  <tr>
			    <td colspan="4">
					<button type="submit" class="btn btn-default btn-xs" id="saveKnowledgeBtn"  style="margin-right:15px" ><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<fmt:message key="common.save" /></button>
					<button type="button" class="btn btn-default btn-xs" style="margin-right:15px" id="add_backToKnowledgeIndex" ><i class="glyphicon glyphicon-share-alt"></i>&nbsp;<fmt:message key="common.returnToList" /></button>
					<%-- <span><fmt:message key="label.template.knowledge" />&nbsp;&nbsp;<select id="knowledgeTemplate" onchange="javascript:wstuo.knowledge.addKnowledge.getTemplateValue(this.value)"></select> </span>
					<input type="hidden" name="templateDTO.templateType" value="knowledge"  />
					<sec:authorize url="TEMPLATE_MAIN">
					<button type="button" class="btn btn-default btn-xs" id="saveKnowledgeTemplateBtn"  style="margin-right:15px"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<fmt:message key="label.template.saveTemplate" /></button>
					<button type="button" class="btn btn-default btn-xs" id="editKnowledgeTemplateBtn"  style="display: none;"><fmt:message key="label.template.editTemplate" /></button>
					</sec:authorize> --%>
					<sec:authorize url="/pages/knowledgeInfo!passKW.action">
						<label><input type="checkbox" value="1" name="knowledgeDto.knowledgeStatus" id="addKnowledge_knowledgeStatus">&nbsp;&nbsp;<fmt:message key="label_knowledge_approved" /></label>
			  		</sec:authorize>
			  </td>
			</tr>
		    </table>
		 <input type="hidden" name="knowledgeDto.creator" value="${loginUserName}"  />
		 <table style="width:100%;" cellspacing="1" >	
				<tr style="height: 40px;">
					<td style="width:10%;padding-left: 15px;" ><fmt:message key="knowledge.label.knowledgeTitle" />&nbsp;<span style="color:red">*</span></td>
					<td  >
						<input attrtype='String' name="knowledgeDto.title"  class="form-control" required="true" id="addKnowledge_title" value="${requestDetailDTO.etitle}"></input>
					</td>
					<td style="width:10%;padding-left: 15px;" ><fmt:message key="knowledge.label.knowledgeSort" />&nbsp;<span style="color:red">*</span></td>
					<td  >
						<input type=hidden name="knowledgeDto.categoryNo" id="addKnowledge_categoryNo">
              	        <input id="addKnowledge_Category"  class="form-control choose" required="true" readonly/>
              	        <div id="addKnowledge_CategoryDiv" class="easyui-panel" closed="true"  style="position:absolute;float:left;width:260px;height:300px;background-color:#FFF;display: none;"></div>
					</td>
				</tr>
			  
			  <tr style="height: 40px;">
					<td style="width:10%;padding-left: 15px;" ><fmt:message key="label.keywords" /></td>
					<td  >
						<input attrtype='String' id="addKnowledge_keyWords" name="knowledgeDto.keyWords"  class="form-control" />
					</td>
					
					<td style="width:10%;padding-left: 15px;" ><fmt:message key="label.knowledge.relatedService" /></td>
					<td  >
						 <div style="padding-top:3px;">
				              <a href="#" id="addKnowledge_service_dir_items_name"><fmt:message key="lable.knowled.serviceTitle"/></a>
				         </div>
			             <div style="color:#ff0000;padding: 4px;"  id="addKnowledge_service_name">
			           			<input id="requestToKnowledge_serviceDirNo" type="hidden" value="${requestDetailDTO.serviceDirectory[0].eventId }" />
			             </div>
					</td>
			</tr>
			
			<script type="text/javascript">
			   initCkeditor("addKnowledgeCon",'full');
			</script>
			<tr>
		       <td colspan="4" ><br/>
		       <textarea id="addKnowledge_Content" name="knowledgeDto.content" style="display:none"></textarea>
		             <input id="is_request_to_knowledge" type="hidden" value="${requestDetailDTO.eno }">
					 <textarea id="addKnowledgeCon" style="width:100%;height:440px">
					 	 ${requestDetailDTO.edesc}
					 	<hr>
					 	${requestDetailDTO.solutions} 
					 </textarea>
		       </td>
			</tr>
		 </table>
		
		
		</div>
		</div>
	</div></div>
	<div class="row">
   				<div class="box col-md-12">
   				    <div class="box-inner" >	
						<div class="box-content" >
	   				 <ul class="nav nav-tabs" id="myTab">
	                    <li><a href="#attachment"><fmt:message key="common.attachment" /></a></li>
	                </ul>
	
	                <div id="myTabContent" class="tab-content">
						<div class="tab-pane" id="attachment">
							 <input type="hidden" name="knowledgeDto.attachmentStr" id="addKnowledge_attachments" value=""/>
							 <div id="addKnowledge_olduploadedAttachments" style="line-height:25px;color:#555;display: none"></div>
							 <div class="form-group" >
							   <input type="file"  name="filedata" id="addKnowledge_uploadAttachments" multiple class="file" data-overwrite-initial="false" data-min-file-count="1">
							 </div>
							 <div class="form-group">
								 <a href="javascript:wstuo.tools.chooseAttachment.showAttachmentGrid('addKnowledge_olduploadedAttachments','knowledgeDto.aids')" class="btn btn-default btn-xs" id="chooseAtt">
								 <fmt:message key="common.attachmentchoose" /></a>
							 </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</form>
	<!-- 知识库内容模版名称start -->
	<div id="knowledgeTemplateName" class="WSTUO-dialog" title="<fmt:message key="label.template.name"/>" style="width:auto;height:auto">
		<div class="panel panel-default">
			<div class="panel-body">
				<form class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.template.knowledgeName" /></label>
						<div class="col-sm-5">
						<input type="hidden" id="knowledgeTemplateId" name="templateDTO.templateId"/>
						<input id="knowledgeTemplateNameInput" name="templateDTO.templateName" class="form-control" required="true" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-4">
						  <button type="button" class="btn btn-primary btn-sm" id="addKnowledgeTemplateOk" ><fmt:message key="common.save"/></button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- 知识库内容模版名称end -->
