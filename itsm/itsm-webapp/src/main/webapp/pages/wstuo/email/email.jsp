<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.opensymphony.xwork2.ActionContext" %>    
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>    
<%@ include file="../../language.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:if test="${cimHave eq true}">
<script src="${pageContext.request.contextPath}/js/itsm/cim/ciTree.js?random=<%=new java.util.Date().getTime()%>"></script>
</c:if>
<script>
	var emailToEmail="0";
	var emailDelete="0";
</script>

<sec:authorize url="/pages/email!toEmail.action">
	<script>emailToEmail="1";</script>
</sec:authorize>
<sec:authorize url="/pages/email!delete.action">
	<script>emailDelete="1";</script>
</sec:authorize>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/wstuo/email/email.js?random=<%=new java.util.Date().getTime()%>"></script>

<div class="tab-pane active" >
	<div class="box-inner">
		<div class="box-header well" data-original-title="">
	        <h2><i class="glyphicon glyphicon-list"></i>&nbsp;邮件管理</h2>	  
	        <div class="box-icon"></div>
        </div>
	<div id="email_content" style="padding:3px;display:none">
		<table id="emailGrid"></table>
		<div id="emailPager"></div>
		<div id="emailGridToolbar" style="display: none">
			<div class="panelBar">					
				<sec:authorize url="/pages/email!toEmail.action">		
					<a id="newMail" class="btn btn-default btn-xs"  plain="true" onclick="javascript:wstuo.email.email.toolbar_addEmail()" ><i class="glyphicon glyphicon-plus"></i>&nbsp;<fmt:message key="tool.mail.addEmail"/></a> 
				</sec:authorize>
			    <!-- 回复邮件 -->
				<sec:authorize url="mail_manage_restoreMail">
					<a id="replyMail" class="btn btn-default btn-xs"  plain="true" onclick="javascript:wstuo.email.email.toolbar_replyEmail()" ><i class=" glyphicon glyphicon-share-alt"></i>&nbsp;<fmt:message key='tool.mail.replyEmail'/></a> 
				</sec:authorize>
				<sec:authorize url="/pages/email!delete.action">
					<a class="btn btn-default btn-xs"  plain="true" onclick="wstuo.email.email.deleteEmailMessage_aff()" ><i class="glyphicon glyphicon-trash"></i>&nbsp;<fmt:message key="common.delete"/></a> 
				</sec:authorize>
				<sec:authorize url="/pages/email!find.action">
					<a class="btn btn-default btn-xs"  plain="true" onclick="wstuo.email.email.openEmailMessageSearchWin()" ><i class="glyphicon glyphicon-search"></i>&nbsp;<fmt:message key="common.search"/></a> 
				</sec:authorize>
				<sec:authorize url="/pages/request!saveRequest.action">
					<a class="btn btn-default btn-xs"  plain="true" icon="icon-emailToRequest" onclick="wstuo.email.email.emailToRequest_aff()" ><i class="glyphicon glyphicon-share-alt"></i>&nbsp;<fmt:message key="title.email.mailToRequest"/></a> 
				</sec:authorize>
				<sec:authorize url="mail_manage_scanMailbox">
					<a onclick="wstuo.email.email.scanEmailServer()"  plain="true" class="btn btn-default btn-xs"  id="email_scanemail_but" ><i class="glyphicon glyphicon-zoom-in"></i>&nbsp;<fmt:message key="common.scan"/></a>
				</sec:authorize>
				<span id="email_scanemail_alert" style="font-size:14px;color:#ff0000;"></span>
		
			</div>
		</div>
	</div>
	</div>		

<!-- 写邮件 -->
<div id="addEmail"  class="WSTUO-dialog" title="<fmt:message key="tool.mail.addEmail"/>" style="width:650px;height:auto">
<form id="addEmailForm" event="wstuo.email.email.add_email">
	<input type="hidden" name="emailConnectionDto.host" id="emailConnectionHost" />
	<input type="hidden" name="emailConnectionDto.username" id="emailConnectionUserName"  />
	<input type="hidden" name="emailConnectionDto.password" id="emailConnectionPassword"/>
	<input type="hidden" name="emailConnectionDto.port" id="emailConnectionPort" />
	
		<table style="width:100%" cellspacing="1" class="table">
	    	<tr>
	            <td ><fmt:message key="tool.mail.receiveAddress"/>&nbsp;<span style="color:red">*</span></td>
	            <td ><input id="receiveAddress_to" name="emailDto.receiveAddress" class="form-control"  required="true" style="width:98%" />
	            	<span style="color: red;"><fmt:message key="label.email.address.format" /></span>
	            	<br>
	            </td>
	        </tr>
	     	
	        <tr>
	            <td ><fmt:message key="tool.mail.emailTitle"/>&nbsp;<span style="color:red">*</span></td>
	            <td ><input id="subject" name="emailDto.subject" class="form-control" required="true"  style="width:98%" /></td>
	        </tr>
	        
	          <tr>
	            <td ><fmt:message key="tool.mail.emailContent"/></td>
	             <td ><textarea id="emailContent" name="emailDto.content" style="width:98%;height:180px;padding-top: 5px;" class="form-control"></textarea>
	           </td>
	        </tr>
	       
	          <tr style="display: none">
	            <td ><fmt:message key="tool.mail.remark"/></td>
	             <td ><input id="remarks" style="width:96%" /></td>
	        </tr>
	        
	         <tr style="display: none">
	            <td ><fmt:message key="tool.mail.emialDescription"/></td>
	             <td ><input id="description" name="emailDto.description" style="width:96%" class="form-control"/></td>
	        </tr>
	        <tr>
	            <td colspan="2" style="padding-top:8px;text-align: center;">
	            	<input type="submit" class="btn btn-primary btn-sm"  value="<fmt:message key="tool.mail.confirmSend"/>"/>
	        	</td>
	        </tr>
	    </table>

    </form>
</div>

<!-- 回复邮件 -->
<div id="replayEmail"  class="WSTUO-dialog" title="<fmt:message key="tool.mail.addEmail"/>" style="width:650px;height:auto">
	<form id="replayEmailForm" event="wstuo.email.email.replay_email">

		<table style="width:100%" class="table" cellspacing="1">
	    	<tr>
	            <td ><fmt:message key="tool.mail.receiveAddress"/>&nbsp;<span style="color:red">*</span></td>
	            <td><input id="receivereplayress_to" name="emailDto.receiveAddress" class="form-control" required="true" style="width:96%" />
	            	<span><fmt:message key="label.email.address.format" /></span>
	            </td>
	        </tr>
	     	
	        <tr>
	            <td ><fmt:message key="tool.mail.emailTitle"/>&nbsp;<span style="color:red">*</span></td>
	            <td ><input id="replaysubject" name="emailDto.subject" class="form-control" required="true"  style="width:96%" /></td>
	        </tr>
	        
	          <tr>
	            <td ><fmt:message key="tool.mail.emailContent"/></td>
	             <td ><textarea id="replayemailContent" name="emailDto.content" style="width:96%;height:180px" class="form-control"></textarea>
	           </td>
	        </tr>
	       
	          <tr style="display: none">
	            <td ><fmt:message key="tool.mail.remark"/></td>
	             <td ><input id="replayremarks" style="width:96%" /></td>
	        </tr>
	        
	         <tr style="display: none">
	            <td ><fmt:message key="tool.mail.emialDescription"/></td>
	             <td ><input id="replaydescription" name="emailDto.description" style="width:96%" /></td>
	        </tr>
	        <tr>
	            <td colspan="2" style="padding-top:8px;">                
	        		<input type="submit" class="btn btn-primary btn-sm"  value="<fmt:message key="tool.mail.confirmSend"/>"/>
	        	</td>
	        </tr>
	    </table>

    </form>
</div>

<!--邮件转请求-->
<div id="logCall_win"  class="WSTUO-dialog" title="<fmt:message key="title.email.mailToRequest"/>" style="width:750px;height:auto;">
	<form id="mailTrqForm" event="wstuo.email.email.emailToRequest_opt">
		<input type="hidden" name="requestDTO.actionName" value="<fmt:message key="common.add" />"  />
		<input type="hidden" name="requestDTO.creator" value="${loginUserName}"  />
		<input  id="email_emailMessageId" type="hidden" name="requestDTO.emailMessage" />
		<input type="hidden" name="requestDTO.attachmentStr" id="email_request_attachmentStr"/>
		<input type="hidden" id="email_request_attachmentAid"/>
		<table style="width:100%" class="lineTable" cellspacing="1">
			<!-- 所属客户  -->
			<tr>
				<td ><fmt:message key="label.belongs.client" />&nbsp;<span style="color:red">*</span></td>
				<td colspan="3" >
					<input type="hidden" name="requestDTO.companyNo" id="email_toRequest_companyNo" />
					<input id="email_toRequest_companyName" class="form-control" style="width: 90%;" required="true"  readonly />
				</td>
			</tr>
			<tr>
				<td  style="width:20%"  ><fmt:message key="tool.mail.toReuqest.title"/>&nbsp;<span style="color:red">*</span></td>
				<td  style="width:80%" colspan="3" >
					<input name="requestDTO.etitle" id="email_request_etitle" value="${requestDTO.etitle}" class="form-control" required="true" />
				</td>
			</tr>
			<tr>
				<td ><fmt:message key="label.common.desc"/>&nbsp;<span style="color:red">*</span></td>
				<td colspan="3" class="lineTableTdCkeditor">
				<textarea id="email_request_edesc" name="requestDTO.edesc" style="height:200px;" class="form-control"></textarea>
				</td>
			</tr>
			<tr>
				<td ><fmt:message key="tool.mail.toReuqest.createByUser"/></td>
				<td  colspan="3">
					<input  id="email_selectCreator" class="form-control" required="true" style="cursor:pointer;width:85%;" onclick="wstuo.email.email.selectRequestCreator()"
					value="${loginUserName}"/>
					<input  id="email_addRequestUserId" type="hidden" name="requestDTO.createdByNo" style="width:98%;color:#555;" />
				</td>
			</tr>
			<tr>
				<td style="width: 20%;"><fmt:message key="tool.mail.toReuqest.category"/></td>
				<td style="width: 30%;">
					<input type="hidden" name="requestDTO.requestCategoryNo" id="mailToRequestCategoryNo" />
					<input name="requestDTO.requestCategoryName"  id="mailToRequestCategoryName" class="form-control" style="width:96%;cursor:pointer" readonly/>					
				</td>	
				<td style="text-align: right;width: 20%;"><fmt:message key="label.request.complexity"/>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td style="width: 30%;">
					<select id="email_request_level" name="requestDTO.levelNo" class="form-control" style="width: 96%">
					</select>
				</td>
				
			</tr>
			<tr>
				<td ><fmt:message key="tool.mail.toReuqest.effect"/></td>
				<td >
					<select id="email_request_effectRange" name="requestDTO.effectRangeNo" class="form-control" style="width: 96%"></select>
				</td>
				<td style="text-align: right;width: 20%;"><fmt:message key="tool.mail.toReuqest.emergencyDegree"/>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td >
					<select id="email_request_seriousness" name="requestDTO.seriousnessNo" class="form-control" style="width: 96%"></select>
				</td>
			</tr>
			<tr>
				<td ><fmt:message key="tool.mail.toReuqest.priority"/></td>
				<td >
					<select id="email_request_priority" name="requestDTO.priorityNo" class="form-control" style="width: 96%"></select>
				</td>
				<td style="text-align: right;width: 20%;"><fmt:message key="tool.mail.toReuqest.form"/>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td >
					<select id="email_request_imode" name="requestDTO.imodeNo" class="form-control" style="width: 96%"></select>
				</td>
			</tr>			
			<tr>			
				<td colspan="4" >
			   
			   		<input type="hidden" name="requestDTO.technicianName" value="${loginUserName}"  />
			    	
			  		<input id="link_addcall_ok" type="submit" class="btn btn-primary btn-sm"  value="<fmt:message key="common.save"/>"/>	     
				</td>
			</tr>
		</table>

	</form>
</div>

<!--邮件信息搜索-->
<div id="emailMessageSearch_win"  class="WSTUO-dialog" title="<fmt:message key="common.search"/>"  style="width:600px;height:auto;">
	<form id="emailMessageSearchForm">

		<table style="width:100%" cellspacing="1">
			<tr style="height: 25px;">
				<td style="width: 23%;"><fmt:message key="tool.mail.emailTitle"/>：</td>
				<td><input name="emailMessageQueryDto.subject" style="width: 95%" class="form-control"/></td>
			</tr>
			<tr style="height: 25px;">
				<td style="width: 23%;"><br/><fmt:message key="tool.mail.emailContent"/>：</td>
				<td><br/><input name="emailMessageQueryDto.content" style="width: 95%" class="form-control"/></td>
			</tr>
			<tr style="height: 25px;">
				<td style="width: 23%;"><br/><fmt:message key="request.email.fromUser"/>：</td>
				<td><br/><input name="emailMessageQueryDto.fromUser" style="width: 95%" class="form-control"/></td>
			</tr>
			<tr style="height: 25px;">
				<td style="width: 23%;"><br/><fmt:message key="request.email.toUser"/>：</td>
				<td><br/><input name="emailMessageQueryDto.toUser" style="width: 95%" class="form-control"/></td>
			</tr>
			<tr style="height: 25px;">
				<td style="width: 23%;"><br/><fmt:message key="request.email.receiveDate"/>：</td>
				<td><br/>
					<input id="receiveStartDate" name="emailMessageQueryDto.receiveStartDate" class="form-control" validType="date" style="width: 130px;float: left;" readonly/>
					<h5 style="float: left;padding-left: 5px;padding-right: 5PX;"><fmt:message key="setting.label.to" /></h5>
					
					<input id="receiveEndDate" name="emailMessageQueryDto.receiveEndDate"  class="form-control" validType="DateComparison['receiveStartDate']" style="width:130px;float: left;" readonly/>
					<h5 style="float: left;padding-left: 8px;">
						<a href="#" onclick="cleanIdValue('receiveStartDate','receiveEndDate')" title="<fmt:message key="label.request.clear" />">
							<i class=" glyphicon glyphicon-trash"></i>
						</a>
					</h5>
				</td>
			</tr>
			<tr style="height: 25px;">
				<td style="width: 23%;"><br/><fmt:message key="request.email.sendDate"/>：</td>
				<td><br/>					
					<input id="sendStartDate" name="emailMessageQueryDto.sendStartDate" class="form-control" validType="date" style="width: 130px;float: left;" readonly/>
					<h5 style="float: left;padding-left: 5px;padding-right: 5PX;"><fmt:message key="setting.label.to" /></h5>
					
					<input id="sendEndDate" name="emailMessageQueryDto.sendEndDate" class="form-control" validType="DateComparison['sendStartDate']" style="width:130px;float: left;" readonly/>
					<h5 style="float: left;padding-left: 8px;">
						<a href="#" onclick="cleanIdValue('sendStartDate','sendEndDate')" title="<fmt:message key="label.request.clear" />">
							<i class=" glyphicon glyphicon-trash"></i>
						</a>
					</h5>
			
				</td>
			</tr>
			<tr style="height: 25px;">
				<td style="width: 23%;"><br/><fmt:message key="label.emailType"/>：</td>
				<td><br/>
					<select name="emailMessageQueryDto.folderName" class="form-control" style="width: 50%">
						<option value="">--<fmt:message key="common.pleaseSelect" />--</option>
						<option value="OUTBOX" id="folderNameEmailSelect"></option>
						<option value="INBOX"><fmt:message key="label.receive" /></option>
					</select>
			</tr>
			<tr>			
				<td colspan="2">    
				<br/>	
		    		<input style="margin-left: 30px;" type="button"  id="link_addcall_ok"  class="btn btn-primary btn-sm" onclick="wstuo.email.email.emailMessageSearch()" value="<fmt:message key="common.search"/>"/>	   	    
		   	        <br/>					
				</td>
			</tr>
		</table>
	
	</form>
</div>
</div>				 