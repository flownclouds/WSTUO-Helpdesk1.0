<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	<!-- 当前任务详细 -->
	<div id="calendarTaskDiv" class="WSTUO-dialog" title="<fmt:message key="common.detailInfo" />" style="width: 400px; height:auto; padding:0px;">
		<form id="calendarTaskForm">
			<input id="calendarTaskId" type="hidden" /> 
			<div class="lineTableBgDiv">
				<table style="width:100%" class="lineTable" cellspacing="1">
				<tr id="task_info_win">
					<td colspan="2" align="right">
						<a class="l-btn l-btn-plain" id="schedule_task_edit_but">
							<span class="l-btn-left"><span class="l-btn-text"><fmt:message key="common.edit"/></span></span></a>
							
						<a class="l-btn l-btn-plain" id="schedule_task_delete_but">
							<span class="l-btn-left"><span class="l-btn-text"><fmt:message key="common.delete"/></span></span></a>	
					</td>
				</tr>
				<tr>
					<td><fmt:message key="task.ownerName"/></td>
					<td>
						<span id="calendarTaskOwner"></span>
					</td>
				</tr>
				<tr>
					<td><fmt:message key="tool.affiche.creator" /></td>
					<td>
						<span id="calendarTaskCreator"></span>
					</td>
				</tr>
				<tr>
					<td style="width:28%"><fmt:message key="common.title"/></td>
					<td style="width:72%">
						<span id="calendarTaskTitle"></span>
						<!--<input name="taskDto.title" id="calendarTaskTitle"  class="easyui-validatebox input" required="true" />-->
					</td>
				</tr>
				<tr>
					<td><fmt:message key="lable.task.location"/></td>
					<td>
						<span id="calendarTaskLocation"></span>
						<!--<input name="taskDto.location" id="calendarTaskLocation" class="easyui-validatebox input" />-->
					</td>
				</tr>
				<tr>
					<td><fmt:message key="label.request.taskDescription"/></td>
					<td style="word-break:break-all;word-wrap:break-word;">
						<span id="calendarTaskIntroduction"></span>
						<!--<textarea name="taskDto.introduction" id="calendarTaskIntroduction"  style="height:50px" class="easyui-validatebox input" ></textarea>-->
					</td>
				</tr>
				<tr>
					<td><fmt:message key="title.task.taskStatus"/></td>
					<td>
						<span id="calendarTaskStatusSpan"></span>
						</td>
				</tr>
				<tr>
					<td><fmt:message key="label.orgSettings.startTime"/></td>
					<td>
						<span id="calendarTaskStartTime"></span>
						<!--<input name="taskDto.startTime" id="calendarTaskStartTime" class="easyui-datebox easyui-validatebox input" required="true"  style="cursor:pointer" />-->
					</td>
				</tr>
				<tr>
					<td><fmt:message key="label.orgSettings.endTime"/></td>
					<td>
						<span id="calendarTaskEndTime"></span>
						<!--<input name="taskDto.endTime" id="calendarTaskEndTime" class="easyui-datebox easyui-validatebox input" required="true" validType="DateComparison['calendarTaskStartTime']" style="cursor:pointer" />-->
					</td>
				</tr>
				<tr>
					<td><fmt:message key="label.orgSettings.allDay"/></td>
					<td>
						<span id="calendarTaskAllDay"></span>
						<!--<input type="checkbox" name="taskDto.allDay" value="true" id="calendarTaskAllDay"  />-->
					</td>
				</tr>
			
				<tr style="display: none">
					<td colspan="2">
					    <input type="hidden" name="taskDto.taskId" id="calendarTask_taskId" />
						<input type="hidden" id="calendarTask_owner" name="taskDto.owner" value="${loginUserName}"/>
						<a onclick="javascript:itsm.portal.calendar.saveCalendarTask('editTask')" class="easyui-linkbutton" icon="icon-edit" title="<fmt:message key="common.edit"/>"></a>
						<a onclick="javascript:itsm.portal.calendar.saveCalendarTask('saveTask')" class="easyui-linkbutton" icon="<fmt:message key="common.add"/>" title=""></a>
					</td>
				</tr>
			</table>
			</div>
		</form>
	</div>
	
	<!--查看公告消息 -->
	<div id="afficheDiv1">
	<div id="afficheDiv" class="WSTUO-dialog" title="<fmt:message key="common.detailInfo"/>" style="width: 400px; height:auto; padding: 3px; line-height: 22px; ">
		<form id="afficheForm">
		<div class="lineTableBgDiv">
		<table style="width: 100%" class="lineTable" cellspacing="1">
			<tr>
				<td style="width:30%"><fmt:message key="label.title"/></td>
				<td style="width:70%">
					<span id="portal_affTitle"></span>
				</td>
			</tr>

			<tr>
				<td><fmt:message key="label.orgSettings.startTime"/></td>
				<td>
					<span id="portal_affStart"></span>
				</td>
			</tr>
			
			<tr>
				<td><fmt:message key="label.orgSettings.endTime"/></td>
				<td>
					<span id="portal_affEnd"></span>
				</td>
			</tr>
			
			<tr>
				<td><fmt:message key="common.cerateTime"/></td>
				<td>
					<span id="portal_cerateTime"></span>
				</td>
			</tr>
			
			<tr>
				<td><fmt:message key="tool.affiche.content"/></td>
				<td>
					<span id="portal_affContents"></span>
				</td>
			</tr>
		</table>
		</div>
		</form>
	</div>
	</div>
	<!--查看IM消息 -->
	<div id="lookMessageDiv" class="WSTUO-dialog" title="<fmt:message key="common.detailInfo"/>" style="width: 400px; height:auto; padding: 3px; line-height: 22px; ">
		<form id="lookMessageForm">
	 	<div  class="lineTableBgDiv">
			<table style="width:100%" class="lineTable" cellspacing="1" >
				<tr>
					<td style="width:30%"><fmt:message key="tool.im.sendUser"/></td>
					<td style="width:70%"><span id="im_sendUser"></span></td>
				</tr>  
				<tr>
					<td><fmt:message key="tool.im.sendTime"/></td>
					<td><span id="im_sendTime"></span></td>
				</tr>
				<tr>
					<td><fmt:message key="tool.im.imTitle"/></td>
					<td><span id="im_title"></span></td>
				</tr>  
				 
				<tr>
					<td><fmt:message key="common.state"/></td>
					<td>
						<span id="im_stats"></span>
					</td>
				</tr>  
				
				<tr>
					<td><fmt:message key="tool.im.imContent"/></td>
					<td><span id="im_content"></span></td>
				</tr>
				
			 
			</table>
		</div>
		</form>
	</div>
