<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<script src="../js/wstuo/onlinelog/userOnlineLog.js?random=<%=new java.util.Date().getTime()%>"></script>
<div id="UserOnlineLog_content" class="content">
<!--row start  -->
	<div class="row">
		<div class="box col-md-12">
			<div class="box-inner">
				<div class="box-header well" data-original-title="">
					<h2><i class="glyphicon glyphicon-list-alt"></i> 用户在线日志列表</h2>
					<div class="box-icon">
						<!-- <a href="#" class="btn btn-minimize btn-round btn-default"><i
							class="glyphicon glyphicon-chevron-up"></i></a> -->
					</div>
				</div>

				<div class="box-content ">
					 <sec:authorize url="/pages/useronlinelog!findPager.action">
						<table id="userOnlineGrid" width="100%"></table>
						<div id="userOnlineGridPager"></div>
						<div id="userOnlineGridToolbar" style="display:none;background-color: #EEEEEE; height: 28px;">
						    <div class="panelBar">
							     <button class="btn btn-default btn-xs" id="userOnlineGrid_download" >
							      <i class="glyphicon glyphicon-download-alt"></i><fmt:message key="label.problem.attachmentDownload"/>
							     </button>
						     </div>
						</div> 
					</sec:authorize>
				</div>
			</div>
		</div>
	</div>
	<!--row end  -->
	
	<!-- 批量下载压缩包 -->
	<div id="zipUserOnlineLog" title="<fmt:message key="batch_download"/>"
		class="WSTUO-dialog" style="width: 380px; height: auto">
		<form>
			<div class="lineTableBgDiv">
				<table style="width: 100%" class="lineTable" cellspacing="1">
                     <tr>
						<td><fmt:message key="label.customReport.date" />&nbsp;</td>
						<td>
						   <input id="zipStartTime" name="startTime"  class="form-control" readonly>
						</td>
						<td width="20px" align="center">
						  <fmt:message key="setting.label.to" />
						</td>
						<td>
							<input id="zipEndTime" name="zipEndTime" validType="DateComparison['zipStartTime']"  class="form-control" readonly>
						</td>
					</tr>

					<tr>
						<td colspan="4">
							<button  type="button" class="btn btn-primary btn-sm" id="userOnlineGrid_downloadzip">
							  <fmt:message key="label.problem.attachmentDownload" />
							</button>
						</td>
					</tr>
				</table>
			</div>
		</form>
	</div>
<!-- 批量下载压缩包 end-->
</div>
	
