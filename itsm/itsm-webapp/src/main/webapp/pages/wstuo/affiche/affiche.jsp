<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.opensymphony.xwork2.ActionContext"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ include file="../../language.jsp" %>

<script src="${pageContext.request.contextPath}/js/wstuo/affiche/affiche.js?random=<%=new java.util.Date().getTime()%>"></script>
<sec:authorize url="/pages/affiche!update.action">
	<script>afficheUpdate="1";</script>
</sec:authorize>
<sec:authorize url="/pages/affiche!delete.action">
	<script>afficheDelete="1";</script>
</sec:authorize>


<div class="tab-pane active" >
	<div class="box-inner">
		<div class="box-header well" >
	        <h2><i class="glyphicon glyphicon-list"></i>&nbsp;公告管理</h2>
        </div>
		<div id="affiche_content" style="padding: 3px;">
			<table id="afficheGrid" ></table>
			<div id="affichePager"></div>	
			<div id="afficheActFormatterDiv"  style="display: none"><sec:authorize url="/pages/affiche!update.action"><a href="javascript:wstuo.affiche.affiche.toolbar_edit('{affId}')" title="<fmt:message key="common.edit"/>"><i class="glyphicon glyphicon-edit"></i></a></sec:authorize>&nbsp;&nbsp;<sec:authorize url="/pages/affiche!delete.action"><a href="javascript:wstuo.affiche.affiche.toolbar_delete('{affId}')" title="<fmt:message key="common.delete"/>"><i class="glyphicon glyphicon-trash"></i></a></sec:authorize></div>	
			<div id="afficheGridToolbar"  style="display: none">
				<div class="panelBar">	
				
					<sec:authorize url="/pages/affiche!save.action">
						<a class="btn btn-default btn-xs"  onClick="wstuo.affiche.affiche.add_openwindow()" title="<fmt:message key="common.add"/>"><i class="glyphicon glyphicon-plus"></i>&nbsp;<fmt:message key="common.add" /></a>
					</sec:authorize> 
				
					<sec:authorize url="/pages/affiche!update.action">
						<a class="btn btn-default btn-xs" onClick="wstuo.affiche.affiche.toolbar_edit_aff()" title="<fmt:message key="common.edit"/>"><i class="glyphicon glyphicon-edit"></i>&nbsp;<fmt:message key="common.edit" /></a>
					</sec:authorize> 
				
					<sec:authorize url="/pages/affiche!delete.action">
						<a class="btn btn-default btn-xs" onClick="wstuo.affiche.affiche.toolbar_delete_aff()" title="<fmt:message key="common.delete"/>"><i class="glyphicon glyphicon-trash"></i>&nbsp;<fmt:message key="common.delete" /></a>
					</sec:authorize> 
				
					<a class="btn btn-default btn-xs" onClick="wstuo.affiche.affiche.search_openwindow()" title="<fmt:message key="common.search"/>"><i class="glyphicon glyphicon-search"></i>&nbsp;<fmt:message key="common.search" /></a>
					
				</div>
			</div>
		</div>
	</div>

<div style="height: 0;overflow: hidden;position:relative">
<div id="addAffiche" class="WSTUO-dialog" title="<fmt:message key="common.add"/>-<fmt:message key="tool.affiche"/>" style="width:520px;height:auto;padding: 0px;">
	<form id="addAfficheForm" event="wstuo.affiche.affiche.add_affiche">
	<%-- 显示表单错误验证信息--%>
	<input type="hidden" name="afficheDTO.companyNo" id="affiche_add_companyNo" />
	<table style="width: 100%" class="lineTable" cellspacing="1">
		<tr>
			<td style="width: 20%;">&nbsp;<fmt:message key="label.title"/>&nbsp;<span style="color:red">*</span></td>
			<td ><input id="affTitle" name="afficheDTO.affTitle" class="form-control" required="true" style="width: 96%;"/></td>
		</tr>
		
		<tr>
			<td style="width: 20%;">&nbsp;<fmt:message key="tool.affiche.effectiveDate"/>&nbsp;<span style="color:red">*</span></td>
			<td ><div style="margin-top: 8px;">
				<span style="float: left;">
				<input id="add_affStart" name="afficheDTO.affStart" class="form-control" required="true" style="width:170px;" readonly />
				</span>
				<span style="float: left;margin-left: 3px;margin-right: 3px;">
				<fmt:message key="setting.label.to"/>&nbsp;
				</span>
				<span style="float: left;">
				<input id="add_affEnd" name="afficheDTO.affEnd" class="form-control" required="true" validType="DateComparison['add_affStart']" style="width:170px" readonly/>
				</span>
				</div>
			</td>
		</tr>			
		<tr>
			<td style="width: 20%;">&nbsp;<fmt:message key="tool.affiche.content"/></td>
			<td>
				<div style="width:96%;margin-top: 8px;">				
					<textarea id="affContents" name="afficheDTO.affContents" style="height:150px;" class="form-control"></textarea>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2" >
				<input type="hidden" id="affCreator" name="afficheDTO.affCreator" value="${fullName}" />
				<button type="submit" class="btn btn-primary btn-sm" style="margin-right: 15px;margin-top: 8px;margin-bottom: 10px;"><fmt:message key="common.save"/></button>
		
			</td>
		</tr>
	</table>
	</form>
</div>

<!-- Edit Affiche -->
<div id="editAffiche" class="WSTUO-dialog" title="<fmt:message key="common.edit"/>-<fmt:message key="tool.affiche"/>" style="width:520px;height:auto;padding: 0px;">
	<form id="editAfficheForm" event="wstuo.affiche.affiche.edit_affiche">
	<div class="ui-formular-error"></div>
	<input type="hidden" name="afficheDto.companyNo" id="affiche_edit_companyNo" />
	<table style="width: 100%" cellspacing="1" class="lineTable">
		<tr>
			<td style="width: 20%;">&nbsp;<fmt:message key="label.title"/>&nbsp;<span style="color:red">*</span></td>
			<td >
				<input id="editTitle" name="afficheDto.affTitle" class="form-control" style="width: 96%;" required="true" />
			</td>
		</tr>
		<tr>
			<td style="width: 20%;">&nbsp;<fmt:message key="tool.affiche.effectiveDate"/>&nbsp;<span style="color:red">*</span></td>
			<td >
				<div style="margin-top: 8px;">
					<span style="float: left;">
					<input id="edit_editStart" name="afficheDto.affStart" class="form-control"  required="true" style="width:170px"  readonly />
					</span>
					<span style="float: left;margin-left: 3px;margin-right: 3px;">
					<fmt:message key="setting.label.to"/>				
					</span>
					<span style="float: left;">
					<input id="edit_editEnd" name="afficheDto.affEnd" class="form-control"  required="true" validType="DateComparison['edit_editStart']" style="width:170px" readonly />
					</span>
				</div>
			</td>
		</tr>
		<tr>
			<td style="width: 20%;">&nbsp;<fmt:message key="tool.affiche.content"/></td>
			<td>
				<div style="width:96%;margin-top: 8px;">	
				<textarea id="editContents" name="afficheDto.affContents" class="form-control" style="height:150px;width: 96%"></textarea>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input id="editId" name="afficheDto.affId" type="hidden" /> 
				<input type="hidden" id="edit_affCreator" name="afficheDto.affCreator" />				
				<button type="submit" class="btn btn-primary btn-sm" style="margin-right: 15px;margin-top: 8px;margin-bottom: 10px;"><fmt:message key="common.save"/></button>		
			</td>
		</tr>
	</table>
	</form>
</div>

<!-- Search Affiche -->
<div id="searchAffiche" class="WSTUO-dialog" title="<fmt:message key="common.search"/>-<fmt:message key="tool.affiche"/>" style="width:410px;;height:auto;padding: 0px;">
	<form event="wstuo.affiche.affiche.search_do">
	<div class="lineTableBgDiv" style="overflow: hidden;">
	<table style="width: 100%" class="lineTable" cellspacing="1">
		<tr>
			<td style="width: 20%;">&nbsp;<fmt:message key="tool.affiche"/><fmt:message key="label.title"/></td>
			<td><input id="searchTitle" name="afficheQueryDto.affTitle" style="width:90%" class="form-control"/></td>
		</tr>
		<tr>
			<td style="width: 20%;">&nbsp;<fmt:message key="tool.affiche.effectiveDate"/></td>
			<td>
				<div style="margin-top: 8px;">
				<span style="float: left;">
				<input id="searchStart1" name="afficheQueryDto.affStart" class="form-control" style="width:120px" readonly="readonly"/>
				</span>
				<span style="float: left;margin-left: 3px;margin-right: 3px;">
					<fmt:message key="setting.label.to"/>
				</span>
				<span style="float: left;">		
				<input id="searchEnd1" name="afficheQueryDto.affEnd" class="form-control" validType="DateComparison['searchStart1']"  style="width:120px" readonly="readonly"/>
				</span>
				<h4 style="float: left;margin-left: 5px;">
					<a href="#" onclick="cleanIdValue('searchStart1','searchEnd1')" title="<fmt:message key="label.request.clear" />"><i class="glyphicon glyphicon-trash"></i></a>				
				</h4>	
				</div>
			</td>				
		</tr>
		<tr>
			<td colspan="2" >			
				<button type="submit" class="btn btn-primary btn-sm" style="margin-right: 35px;margin-top: 8px;margin-bottom: 10px;"><fmt:message key="common.search"/></button>					
			</td>
		</tr>
	</table>
	</div>
	</form>
</div>

</div>

</div>