﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<script> 
var _contractNo = '${param.contractNo}';
var _rulePackageNo='${slaContractDTO.rulePackageNo}';
var slaDetail='<fmt:message key="title.sla.slaDetail"/>';
var fitRule='<fmt:message key="title.sla.fitRule"/>';
</script>
<script src="../js/wstuo/jbpmMge/processUse.js"></script>
<script src="../js/wstuo/slaMge/slaDetail.js?random=<%=new java.util.Date().getTime()%>"></script>
<script src="../js/wstuo/slaMge/slaDetail_ruleGrid.js?random=<%=new java.util.Date().getTime()%>"></script>
<script src="../js/wstuo/slaMge/slaDetail_updateGrid.js?random=<%=new java.util.Date().getTime()%>"></script>
<script src="../js/wstuo/slaMge/slaDetail_OverdueGrid.js?random=<%=new java.util.Date().getTime()%>"></script>


<div class="tab-pane active" >
	<div class="box-inner">
		<div class="box-header well" data-original-title="">
	        <h2><i class="glyphicon glyphicon-list">&nbsp;</i>SLA详情</h2>
        </div>
	<div class="content" border="none" fit="true" style="height: 98%;">
 	<div style="margin-left: 10px;margin-top: 10px;">	
		<a  class="btn btn-default btn-xs" id="sla_returnToList" >
		<i class=" glyphicon glyphicon-share-alt"></i><fmt:message key="common.returnToList" />
		</a>
	</div>
		<!-- 新的面板 start-->
		<div id="SLAServiceManage_content_layout"  >
			<!-- 左边信息start -->			
			<div class="row">
			<div class="box col-md-3" >
			<div class="box-inner" style="padding-left: 5px;">
		        <div class="box-content" id="slaDetailWest" region="west" split="true">	
		        <input type="hidden" name="rule.rulePackage.rulePackageNo" value="${slaContractDTO.rulePackageNo}" />         
            		<div class="box-header well" data-original-title="">
			       		<h2><fmt:message key="common.basicInfo" /></h2>
		        	</div>	
		           	<table style="width:100%" cellspacing="1" class="table table-bordered">
						<tr>
							<td style="width: 30%" height="32"><fmt:message key="label.sla.contractName"/></td>
							<td style="width: 70%">${slaContractDTO.contractName}</td>
						</tr>
						<tr>
							<td height="32"><fmt:message key="label.sla.slaStartTime"/></td>
							<td><fmt:formatDate type="date" value="${slaContractDTO.beginTime}"  pattern="yyyy-MM-dd HH:mm:ss" /></td>
						</tr>
						<tr>
							<td height="32"><fmt:message key="label.sla.slaEndTime"/></td>
							<td><fmt:formatDate type="date" value="${slaContractDTO.endTime}"  pattern="yyyy-MM-dd HH:mm:ss" /></td>
						</tr>
						<tr>
							<td height="32"><fmt:message key="label.sla.ownerOrg"/></td>
							<td>${slaContractDTO.serviceOrgName}</td>
						</tr>
						<tr>
							<td height="32"><fmt:message key="label.sla.slaServiceAgreement"/></td>
							<td>${slaContractDTO.agreement}</td>
						</tr>
					</table>		               	
		        </div>		          
		      </div>
		    </div>		
		    <!-- 左边信息 end-->
		    <!-- 右边详细信息 start -->
		    <div class="box col-md-9">
        		<div class="box-inner" id="slaDetailTab">	      		
				<div class="box-content" id="slaDetailMainTab">
					<ul class="nav nav-tabs" id="myTab">
						<li class="active"><a href="#list1"><span><fmt:message key="title.sla.expecteCompleted"/></span></a></li>	
						<li><a href="#list2"><span><fmt:message key="title.sla.autoUpdate"/></span></a></li>	
						<li><a href="#list3"><span><fmt:message key="sla.request.overdue"/></span></a></li>	
	             	</ul>
             		<div id="myTabContent" class="tab-content">
						<div region="center" class="tab-pane active" id="list1">
							<table id="slaDetail_ruleGrid"></table>
							<div id="slaDetail_ruleGridPager" class="scroll" style="text-align:center;"></div>
							<div id="slaDetail_ruleGridToolbar" style="display:none">
							<sec:authorize url="/pages/slaRule!save.action">
								<a class="btn btn-default btn-xs"  plain="true" id="slaDetail_ruleGrid_add"  title="<fmt:message key="common.add"/>">
									<i class="glyphicon glyphicon-plus"></i>&nbsp;<fmt:message key="common.add"/>
								</a> 
							</sec:authorize>
							<sec:authorize url="/pages/slaRule!edit.action">
								<a class="btn btn-default btn-xs"  plain="true" id="slaDetail_ruleGrid_edit" title="<fmt:message key="common.edit"/>">
									<i class="glyphicon glyphicon-edit"></i>&nbsp;<fmt:message key="common.edit"/>
								</a>
							</sec:authorize>
							<sec:authorize url="/pages/slaRule!delete.action">
								<a class="btn btn-default btn-xs"  plain="true" id="slaDetail_ruleGrid_delete" title="<fmt:message key="common.delete"/>">
									<i class="glyphicon glyphicon-trash"></i>&nbsp;<fmt:message key="common.delete"/>
								</a> 
							</sec:authorize>
						
							</div>
						</div>
						<sec:authorize url="/pages/slaRule!autoUpdate.action">
						<div class="tab-pane" id="list2">
	                		<table id="autoUpdateGrid"></table>
							<div id="autoUpdateGridPager" class="scroll" style="text-align:center;"></div>
							<div id="autoUpdateGridToolbar" style="display:none">
								<a class="btn btn-default btn-xs"  plain="true" id="autoUpdateGrid_add"  title="<fmt:message key="common.add"/>">
									<i class="glyphicon glyphicon-plus"></i>&nbsp;<fmt:message key="common.add"/>
								</a> 
								<a class="btn btn-default btn-xs"  plain="true" id="autoUpdateGrid_edit"   title="<fmt:message key="common.edit"/>">
									<i class="glyphicon glyphicon-edit"></i>&nbsp;<fmt:message key="common.edit"/>
								</a>
								<a class="btn btn-default btn-xs"  plain="true" id="autoUpdateGrid_delete"  title="<fmt:message key="common.delete"/>">
									<i class="glyphicon glyphicon-trash"></i>&nbsp;<fmt:message key="common.delete"/>
								</a> 
							</div>
						</div>  
						</sec:authorize>
						<sec:authorize url="/pages/slaRule!overdue.action">
	  					<div class="tab-pane" id="list3">
	                		<table id="autoOverdueGrid"></table>
							<div id="autoOverdueGridPager" class="scroll" style="text-align:center;"></div>
							<div id="autoOverdueGridToolbar" style="display:none">
								<a class="btn btn-default btn-xs"  plain="true" id="autoOverdueGrid_add"  title="<fmt:message key="common.add"/>">
									<i class="glyphicon glyphicon-plus"></i>&nbsp;<fmt:message key="common.add"/>
								</a> 
								<a class="btn btn-default btn-xs"  plain="true" id="autoOverdueGrid_edit"   title="<fmt:message key="common.edit"/>">
									<i class="glyphicon glyphicon-edit"></i>&nbsp;<fmt:message key="common.edit"/>
								</a>
								<a class="btn btn-default btn-xs"  plain="true" id="autoOverdueGrid_delete"  title="<fmt:message key="common.delete"/>">
									<i class="glyphicon glyphicon-trash"></i>&nbsp;<fmt:message key="common.delete"/>
								</a> 
							</div>
						</div>  
				        </sec:authorize>
                    </div>
				</div>     
          		</div>
			</div>
		</div>
		</div>
	</div>
</div>
</div>

<!-- </div> -->
<!-- 新增/编辑SLA规则 -->
<div style="width: 0xp;height: 0px;overflow: hidden;position:relative">
	<div id="addSLARuleDivWindow" title="<fmt:message key="title.sla.addAutoUpdate"/>" >
	<div id="addSLARuleDiv">
		<form id="addSLARuleForm">
			<input type="hidden" id="contractNo" name="rule.slaContract.contractNo" value="${param.contractNo}" />
			<s:hidden name="rule.dialect" value="mvel" />
			<s:hidden name="rule.condition.patternBinding" value="dto" />
			<s:hidden name="rule.condition.patternType" value="RequestDTO" />
			<input type="hidden" id="sla_rule_condition_rulePatternNo" name="rule.condition.rulePatternNo" />
			<input type="hidden" name="rule.rulePackage.rulePackageNo" value="${slaContractDTO.rulePackageNo}" />
			<input type="hidden" name="rule.ruleNo" id="sla_rule_ruleNo" />
			<input type="hidden" name="rule.dataFlag" value="0" id="sla_rule_dataFlag" />
			<div class="row">
			<div class="box-inner" >	
			<div class="box-content" >
				<ul class="nav nav-tabs" id="myTab">
					<li class="active"><a href="#list5"><span><fmt:message key="title.sla.slaDetail"/></span></a></li>	
					<li><a href="#list6"><span><fmt:message key="title.sla.fitRule"/></span></a></li>				
             	</ul>
            	<div id="slaTab_main" class="tab-content" style="height:100%;width: 100%;" border="none">
            		<!-- SLA 明细 stard -->
					<div region="center" class="tab-pane active" id="list5" >
						<table style="width:100%;margin-top: 8px;"  cellspacing="1" class="table">
							<tr>
								<td style="width:30%" ><fmt:message key="label.sla.ruleName"/></td>
								<td style="width:68%" >
									<input type="text" name="rule.ruleName" id="rule_ruleName" class="form-control" validType="nameSpecialSymbolVerification" style="width:90%" required="true" />
								</td>
							</tr>
							<tr>
								<td style="width: 30%"><fmt:message key="label.rule.salience" /></td>
								<td style="width: 68%">
									<input name="rule.salience" id="rule_salience_default" class="form-control"/>
									<input  id="rule_salience" type="hidden"/>
									<select name="rule.salience" id="slaRuleSalience" class="form-control" style="width:90%">
									</select>
								</td>
							</tr>
							<tr>
								<td><fmt:message key="label.sla.requestTime"/></td>
								<td>
								  <input class="form-control" style="width:60px;float: left;" name="rule.rday" id="rule_rday" value="0" min="0" max="10000" >
					              <h5 style="float: left;padding-left: 5px;padding-right: 5px;text-align: center;">
					              		<fmt:message key="label.sla.day"/>
								  </h5>
								  <input class="form-control" style="width:60px;float: left;" name="rule.rhour" id="rule_rhour" value="0" min="0" max="23">
					              <h5 style="float: left;padding-left: 5px;padding-right: 5px;text-align: center;">
					              <fmt:message key="label.sla.hour"/>
					              </h5>
					              <input class="form-control" style="width:60px;float: left;" name="rule.rminute" id="rule_rminute" value="0" min="0" max="59">
					              <h5 style="float: left;padding-left: 5px;text-align: center;">
					              <fmt:message key="label.sla.minute"/>
								  </h5>
								</td>
							</tr>
							<tr>
								<td><fmt:message key="label.sla.completeTime"/></td>
								<td>
								  <input class="form-control" style="width:60px;float: left;" name="rule.fday" id="rule_fday" value="0" min="0" max="10000">
					              <h5 style="float: left;padding-left: 5px;padding-right: 5px;text-align: center;">
					              <fmt:message key="label.sla.day"/>
								  </h5>
								  <input class="form-control" style="width:60px;float: left;" name="rule.fhour" id="rule_fhour" value="0" min="0" max="23">
					               <h5 style="float: left;padding-left: 5px;padding-right: 5px;text-align: center;">
					              <fmt:message key="label.sla.hour"/>
					              </h5>
					              <input class="form-control" style="width:60px;float: left;" name="rule.fminute" id="rule_fminute" value="0" min="0" max="59">
					               <h5 style="float: left;padding-left: 5px;text-align: center;">
					              <fmt:message key="label.sla.minute"/>
								</h5>
								</td>
							</tr>
							<tr>
								<td><fmt:message key="label.sla.responseRate"/></td>
								<td>
									<input type="text" name="rule.responseRate" id="rule_responseRate" style="width:50%;float: left;" class="form-control" validType="persent" />
									<h5 style="float: left;padding-left: 5px;padding-right: 5px;text-align: center;">
									<fmt:message key="label.sla.percentRate"/>
									</h5>
								</td>
							</tr>
							<tr>
								<td><fmt:message key="label.sla.completeRate"/></td>
								<td>
									<input type="text" name="rule.completeRate" id="rule_completeRate" style="width:50%;float: left;" class="form-control" validType="persent"/>
									<h5 style="float: left;padding-left: 5px;text-align: center;">
									<fmt:message key="label.sla.percentRate"/>
									</h5>
								</td>
							</tr>
							<tr>
								<td><fmt:message key="label.sla.allDayBase"/></td>
								<td>
									<input type="hidden" name="rule.includeHoliday" id="rule_includeHolidayVl">
									<input type="radio" name="rule_includeHoliday" value="true"><fmt:message key="label.sla.yes"/>
									<input type="radio" name="rule_includeHoliday" value="false" checked><fmt:message key="label.sla.no"/>
								</td>
							</tr>
						</table>
					</div>
					<!-- SLA 明细 End -->					
					<div class="tab-pane" id="list6">
						<div id="sla_fitRule_action_tab">
							<table style="width:100%;min-width: 625px;margin-top: 8px;" cellspacing="1" class="table table-bordered">
								<tr>
									<td><fmt:message key="label.sla.ruleDetail"/></td>
									<td>
										<table style="width:100%;">
											<tr>
											<td style="width:20%;">
												<select id="addSLARule_team" style="width:90%" class="form-control">
												</select> 
											</td>
											<td style="width:20%;">
												<select id="addSLARule_matical" class="form-control" style="width:90%">
												</select> 
											</td>
											<td style="width:30%">
												<div id="addSLARule_propertyValueDIV" >
													<input id="addSLARule_propertyValue" size="15" value="" class="form-control" style="width:90%"/>  
												</div>
											</td>
											<td style="width:25%" id="slaDetail_ruleGrid_addRuleToList_tr">
												<a class="btn btn-default btn-xs" plain="true"  id="slaDetail_ruleGrid_addRuleToList" style="margin-right: 40px;min-width:50px;">
												<i class="glyphicon glyphicon-ok"></i>&nbsp;
												<fmt:message key="label.rule.confirm"/></a>											
											</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td style="width:20%"><fmt:message key="lable.customFilter.relation"/></td>
									<td style="width:80%">
										<label class="radio-inline">
											<input id="addSLARule_or" type="radio" name="orand" value="or" checked/>or
										</label>
               							<label class="radio-inline">
											<input id="addSLARule_and" type="radio" name="orand" value="and" />and
										</label>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<table id="addRule_constraintsTable" style="width:100%" class="table table-bordered" >
											<thead>
											<tr>
												<th style="width: 20%;  text-align: center; "><fmt:message key="label.sla.condition"/></th>
												<th style="width: 40%;  text-align: center; "><fmt:message key="label.sla.ruleContent"/></th>
												<th style="width: 20%;  text-align: center; "><fmt:message key="lable.customFilter.relation" /></th>
												<th style="width: 20%;  text-align: center; "><fmt:message key="label.sla.operation"/></th>
											</tr>
											</thead>
											<tbody></tbody>
										</table>
									</td>								
								</tr>
							</table>
						</div>
					</div>  
				</div>
			
			<div style="height:30px;text-align: center;">
				<input type="button" class="btn btn-primary btn-sm"  style="margin-right:15px" id="slaDetail_ruleGrid_saveRule" value="<fmt:message key="common.save"/>"/>
			</div>	
			</div>	
			</div>
			</div>
		</form>
		</div>
		
	</div>		
</div>
	
<!-- 新增升级策略 -->
<div id="autoUpdateWindow" class="WSTUO-dialog" closed="true" collapsible="false" minimizable="false" maximizable="false" modal="true" title="<fmt:message key="title.sla.addEditAutoUpdate"/>" style="width:420px;height:auto">
	<form id="autoUpdateForm">
		<div class="lineTableBgDiv">
			<table style="width:100%"  cellspacing="1" class="table">
				<tr>
					<td style="width:100px"><fmt:message key="label.sla.autoUpdateName"/></td>
					<td>
						<input type="text" name="promoteRuleDTO.ruleName" id="promoteRule_ruleName" class="form-control" required="true" style="color:#555;width:96%" validtype="length[1,200]"/>
					</td>
				</tr>
				<tr>
					<td><fmt:message key="label.sla.updateBase"/></td>
					<td>
						<input type="radio" name="promoteRuleDTO.referType" value="response" checked><fmt:message key="label.sla.updateBase.response"/>&nbsp;
						<input type="radio" name="promoteRuleDTO.referType" value="complete"><fmt:message key="label.sla.updateBase.complete"/>
					</td>
				</tr>
				<tr>
					<td><fmt:message key="label.sla.updateStyle"/></td>
					<td>
						<input type="radio" name="promoteRuleDTO.beforeOrAfter" value="true" checked> <fmt:message key="label.sla.updateAfter"/>&nbsp;
						<input type="radio" name="promoteRuleDTO.beforeOrAfter" value="false"><fmt:message key="label.sla.updateBefore"/>
					</td>
				</tr>
				<tr>
					<td><fmt:message key="label.sla.updateTime"/></td>
					<td>
						<input class="form-control" style="width:70px;float: left;" class="form-control" name="promoteRuleDay" id="promoteRuleDay" value="0" min="0" max="10000">
			            <h5 style="float: left;padding-left: 5px;padding-right: 5px;text-align: center;">
			            <fmt:message key="label.sla.day"/>
					  	</h5>
					  	<input class="form-control" style="width:70px;float: left;" class="form-control" name="promoteRuleHour" id="promoteRuleHour" value="0" min="0" max="23">
			            <h5 style="float: left;padding-left: 5px;padding-right: 5px;text-align: center;">
			            <fmt:message key="label.sla.hour"/>
			            </h5>
			            <input class="form-control" style="width:70px;float: left;" class="form-control" name="promoteRuleMinute" id="promoteRuleMinute" value="0" min="0" max="59">
			            <h5 style="float: left;padding-left: 5px;padding-right: 5px;text-align: center;">
			            <fmt:message key="label.sla.minute"/>
						</h5>
					</td>
				</tr>		
				<tr>
					<td><fmt:message key="label.sla.updateTo"/></td>
					<td>
						<input id="SLA_AutoUpdateLevel" onclick="javascript:wstuo.slaMge.slaDetail_updateGrid.selectNoticeUser('#SLA_AutoUpdateLevel','#SLA_AutoupdateLevelNo')" class="form-control" style="width:50%;" readonly="readonly"/>
				        <input type="hidden" id="SLA_AutoupdateLevelNo" name="promoteRuleDTO.updateLevelNo" >
					</td>
				</tr>
				<tr>
					<td style="height:50px" colspan="2">
						<input type="hidden" name="promoteRuleDTO.ruleNo" id="promoteRule_ruleNo">
						<input type="hidden" name="promoteRuleDTO.ruleType" value="promote">
						<input type="hidden" name="promoteRuleDTO.contractNo" value="${param.contractNo}">
						<input type="button" class="btn btn-primary btn-sm"  style="margin-right:15px" id="autoUpdateGrid_save" value="<fmt:message key="common.save"/>"/>						
					</td>
				</tr>
			</table>
		</div>
	</form>
</div>
	
<!-- SLA逾期通知技术员 -->
<div id="autoOverdueWindow" class="WSTUO-dialog" closed="true" collapsible="false" minimizable="false" maximizable="false" modal="true" title="<fmt:message key="sla.request.overdue"/>" style="width:430px;height:auto">
	<form id="autoOverdueForm">
		<table style="width:100%"  cellspacing="1" class="table">
			<tr>
				<td style="width:100px"><fmt:message key="sla.request.NotificationName"/></td>
				<td>
					<input type="text" name="promoteRuleDTO.ruleName" id="overdueRule_ruleName" class="form-control" required="true" style="color:#555;width:96%" validtype="length[1,200]"/>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="sla.request.NotificationReason"/></td>
				<td>
					<input type="radio" name="promoteRuleDTO.referType" value="response" checked><fmt:message key="label.sla.updateBase.response"/>&nbsp;
					<input type="radio" name="promoteRuleDTO.referType" value="complete"><fmt:message key="label.sla.updateBase.complete"/>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="sla.request.NotificationWay"/></td>
				<td>
					<input type="radio" name="promoteRuleDTO.beforeOrAfter" value="true" checked> <fmt:message key="label.sla.noticeAfter"/>&nbsp;
					<input type="radio" name="promoteRuleDTO.beforeOrAfter" value="false"><fmt:message key="label.sla.noticeBefore"/>	
				</td>			
			</tr>
			<tr>
				<td><fmt:message key="sla.request.NotificationTime"/></td>
				<td>
				  <input class="form-control" style="width:70px;float: left;" class="form-control" name="promoteRuleDay" id="overdueRuleDay" value="0" min="0" max="10000">
	              <h5 style="float: left;padding-left: 5px;padding-right: 5px;text-align: center;">
	              <fmt:message key="label.sla.day"/>
				  </h5>
				  <input class="form-control" style="width:70px;float: left;" class="form-control" name="promoteRuleHour" id="overdueRuleHour" value="0" min="0" max="23">
	              <h5 style="float: left;padding-left: 5px;padding-right: 5px;text-align: center;">
	              <fmt:message key="label.sla.hour"/>
	              </h5>
	              <input class="form-control" style="width:70px;float: left;" class="form-control" name="promoteRuleMinute" id="overdueRuleMinute" value="0" min="0" max="59">
	              <h5 style="float: left;padding-left: 5px;padding-right: 5px;text-align: center;">
	              <fmt:message key="label.sla.minute"/>
				  </h5>
				</td>
			</tr>
			<tr>
				<td style="height:50px" colspan="2">
					<input type="hidden" name="promoteRuleDTO.ruleNo" id="overdueRule_ruleNo">
					<input type="hidden" name="promoteRuleDTO.contractNo" value="${param.contractNo}">
					<input type="hidden" name="promoteRuleDTO.ruleType" value="overdue">					
					<input type="button" class="btn btn-primary btn-sm"  style="margin-right:15px" id="autoOverdueGrid_save" value="<fmt:message key="common.save"/>"/>						
				</td>
			</tr>
		</table>
	</form>
</div> 
