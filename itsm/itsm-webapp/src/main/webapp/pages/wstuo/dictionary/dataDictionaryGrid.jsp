<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../../language.jsp" %>

<script> var _groupCode = "${param.groupCode}";</script>
<script type="text/javascript" src="../scripts/jquery/icolor/iColorPicker.js"></script>
<script src="../scripts/common/config/dictionary/dataDictionaryGrid.js?random=<%=new java.util.Date().getTime()%>"></script>


<div class="loading" id="dataDicionaryMain_loading_${param.groupCode}">
	<img src="../images/icons/loading.gif" />
</div>

<div id="dataDicionaryMain_content_${param.groupCode}" class="content"> 


<div region="center"  >
   
	<table id="dictionaryItemGrid_${param.groupCode}"></table>
    <div id="dictionaryItemGridPager_${param.groupCode}"></div>
    <div id="dataDictionaryGridToolbar" style="display:none">
    
    
<sec:authorize url="/pages/dataDictionaryItems!save.action">
<a class="easyui-linkbutton" plain="true" icon="icon-add" onclick="common.config.dictionary.dataDictionaryGrid.addDictionaryItem()" title="<fmt:message key="common.add"/>" ></a>
</sec:authorize>

<sec:authorize url="/pages/dataDictionaryItems!merge.action">
	<a class="easyui-linkbutton" plain="true" icon="icon-edit" onclick="common.config.dictionary.dataDictionaryGrid.editDictionary()"  title="<fmt:message key="common.edit"/>" ></a> 
</sec:authorize>

<sec:authorize url="/pages/dataDictionaryItems!delete.action">
<a class="easyui-linkbutton" plain="true" icon="icon-cancel" onclick="common.config.dictionary.dataDictionaryGrid.delDictionary()" title="<fmt:message key="common.delete"/>" ></a> 
</sec:authorize>

<a class="easyui-linkbutton" plain="true" icon="icon-search" onclick="common.config.dictionary.dataDictionaryGrid.openSearchWindow()"  title="<fmt:message key="common.search"/>" ></a>

<sec:authorize url="dataDictionary_import">
<a id="importDataDc" class="easyui-linkbutton" plain="true" icon="icon-import" title="<fmt:message key="label.dc.import"/>"></a>
</sec:authorize>

<!-- 字典导出 DATADICTIONARY_EXPORT -->
<sec:authorize url="DATADICTIONARY_EXPORT">
<a id="exportDataDc" class="easyui-linkbutton" plain="true" icon="icon-export" title="<fmt:message key="label.dc.export"/>"></a>
</sec:authorize>

</div>
</div>

<!-- 新增 -->
<div id="addEditDictionaryWindow_${param.groupCode}" class="WSTUO-dialog" title="<fmt:message key="label.dc.addDc"/>" style="width:400px;height:auto">
	<form id="addDataDictionaryForm_${param.groupCode}">
	<input type="hidden" name="dataDictionaryItemsDto.dno" id="dno_${param.groupCode}" />
	<div class="lineTableBgDiv">
    <table  class="lineTable"  width="100%" cellspacing="1"> 
     
     <tr>
            <td>
         <fmt:message key="label.name"/>
            </td>
            <td>
            
            <input id="dname_${param.groupCode}" name="dataDictionaryItemsDto.dname" required="true" class="easyui-validatebox input"  validType="nullValueValid" />
            </td> 
      </tr>
           
        <tr>
            <td>
           <fmt:message key="label.common.desc"/>
            </td>
            <td>
            <textarea id="description_${param.groupCode}" name="dataDictionaryItemsDto.description" class="easyui-validatebox textarea" validType="length[1,200]"></textarea>
            </td>
        </tr>
         <tr>
            <td width="100px">
           <fmt:message key="common.remark"/>
            </td>
            <td>
           <textarea id="remark_${param.groupCode}" name="dataDictionaryItemsDto.remark" class="easyui-validatebox textarea" validType="length[1,200]"></textarea>
            </td>
        </tr>
        <tr>
            <td width="100px">
           <fmt:message key="label.color"/>
            </td>
            <td>
            	
				 <input id="color_${param.groupCode}" name="dataDictionaryItemsDto.color"  value="#ffcc00" class="iColorPicker" class="input" />
            </td>
        </tr>
        
        <tr>
            <td colspan="2">
                <div style="padding-top:8px;">
                <input id="dcode_${param.groupCode}" name="dataDictionaryItemsDto.dcode" type="hidden"/>
                <input type="hidden" id="groupCode_${param.groupCode}" name="dataDictionaryItemsDto.groupCode" value="${param.groupCode}" />
                <a id="saveDataDictionaryBtn"  class="easyui-linkbutton" icon="icon-save" ><fmt:message key="common.save"/></a>
                </div>
        </td>
        </tr>
    </table>
    </div>
    </form>
</div>

<!-- 搜索 -->
<div id="searchDataDictionaryDiv_${param.groupCode}"  class="WSTUO-dialog" title="<fmt:message key="common.search"/>" style="width:380px;height:auto">
<form action="dataDictionaryItems!exportDictionaryItemData.action" method="post">
   <div class="lineTableBgDiv" >
    <table  class="lineTable"  width="100%" cellspacing="1"> 
        <tr>
            <td width="100px">
          <fmt:message key="label.name"/>
        
            </td>
            <td>
           <input id="dataDictionaryQueryDto_dname" name="dataDictionaryQueryDto.dname" style="width:263px" />
            </td>
        </tr>
       <tr>
            <td width="100px">
           <fmt:message key="label.common.desc"/>
            </td>
            <td>
           <input id="dataDictionaryQueryDto_description" name="dataDictionaryQueryDto.description" required="true" style="width:263px" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div style="padding-top:8px;">
                	<input name="dataDictionaryQueryDto.groupCode" type="hidden" value="${param.groupCode}"/>
                	<a id="searchDataDictionaryBtn" class="easyui-linkbutton" icon="icon-search" title="" ><fmt:message key="common.search"/></a>
                </div>
        </td>
        </tr>
    </table>
    </div>
    </form>
</div>
<!-- 导入数据 -->
<div id="importDataDictionaryWindow_${param.groupCode}"  class="WSTUO-dialog" title="<fmt:message key="label.dc.import"/>" style="width:400px;height:auto">
	<form>
   <div class="lineTableBgDiv" >
    <table  class="lineTable"  width="100%" cellspacing="1"> 
        <tr>
            <td width="100px">
            <fmt:message key="label.dc.filePath"/>
        
            </td>
            <td>
           	<input type="file" class="easyui-validatebox input" required="true" name="dataDictionaryQueryDto.importFile" id="dataDictionaryImportFile${param.groupCode}" onchange="checkFileType(this,'csv')"/>
           	
            </td>
        </tr>
		<tr>
		       <td colspan="2" align="center">
		      		 <c:choose>  
					   <c:when test="${lang eq 'en_US'}">
					   		<a href="../importFile/en/DataDictionary.zip" target="_blank">[<fmt:message key="label.csv.template.download"></fmt:message>]</a>
					   </c:when>
					     
					   <c:otherwise>
					   		<a href="../importFile/DataDictionary.zip" target="_blank">[<fmt:message key="label.csv.template.download"></fmt:message>]</a>
					   </c:otherwise>
					</c:choose>
		        </td>
		</tr>
        <tr>
            <td colspan="2">
                <div style="padding-top:8px;">
                	<a id="importDataDictionaryBtn" class="easyui-linkbutton" icon="icon-ok" onclick="common.config.dictionary.dataDictionaryGrid.importExcel()" ><fmt:message key="label.dc.import"/></a>
                </div>
        </td>
        </tr>
    </table>
    </div>
    </form>
</div>

</div>