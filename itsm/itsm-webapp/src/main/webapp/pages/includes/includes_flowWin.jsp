<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../language.jsp" %>    

<script src="../js/wstuo/orgMge/organizationUtil.js"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>流程公用Windows文件</title>
</head>
<body>

	<%-- 任务指派窗口 start --%>
	<div id="task_assignee_window"  class="WSTUO-dialog" title="<fmt:message key="title.bpm.task.assign" />" style="width:450px;height:200px">
		<form id="task_assignee_form">
			<input type="hidden" name="processHandleDTO.pid" id="task_assignee_pid" />
			<input type="hidden" name="processHandleDTO.taskId" id="task_assignee_taskId" />
			<input type="hidden" name="processHandleDTO.eno" id="task_assignee_eno" />
			<input type="hidden" name="processHandleDTO.module" id="task_assignee_module" />
			<input type="hidden" name="processHandleDTO.operator" value="${loginUserName}">
			<input type="hidden" name="processHandleDTO.handleType" id="task_assignee_handleType" />
			<input type="hidden" name="processHandleDTO.assignType" id="task_assignee_assignType">
			<input type="hidden" name="processHandleDTO.allowUseDynamicAssignee" id="task_assignee_allowUseDynamicAssignee" >
			<input type="hidden" name="processHandleDTO.allowUseVariablesAssignee" id="task_assignee_allowUseVariablesAssignee" >
			
			<table style="width:100%" class="lineTable" cellspacing="1">
				<tr id="task_assignee_tr">
					<td><fmt:message key="label.request.technician" />&nbsp;<span style="color:red">*</span></td>
					<td>
						<input type="hidden"  name="processHandleDTO.assigneeNo" id="task_assignee_assigneeNo" value="${sessionScope.userId}" />
						<input id="task_assignee_assigneeName"  readonly="readonly" required="true" class="form-control" style="width: 90%;float: left;" value="${sessionScope.fullName}"
						onclick="wstuo.jbpmMge.processCommon.taskAssignee_assigneeName()"/>
						<h5 style="float: left;padding-left: 8px;"><a onclick="cleanIdValue('task_assignee_assigneeNo','task_assignee_assigneeName')" title="<fmt:message key="label.request.clear" />">
						<i class=" glyphicon glyphicon-trash"></i></a></h5>
					</td>
				</tr>
				<tr>
					<td><fmt:message key="label.request.processMark" /></td>
					<td>
						<textarea style="width: 90%;height: 85px" name="processHandleDTO.remark" class="form-control" validType="maxLength[255]"></textarea>
					</td>
				</tr>
				
				<tr>
					<td colspan="2">
						<span style="margin-right: 35px;" class="btn btn-primary btn-sm" id="task_assignee_submit_button" ><fmt:message key="common.submit" /></span>
					</td>
				</tr>
			</table>
		</form>
	</div>
	<%-- 任务指派窗口 end --%>
	
	
	<%-- 流程处理窗口 start --%>
	<div id="flow_handle_win" class="WSTUO-dialog"  style="width:550px;height:auto">
		<form id="flow_handle_from">
			<input type="hidden" id="flow_handle_validMethod" />
			<input type="hidden" name="processHandleDTO.pid" id="flow_handle_pid" />
			<input type="hidden" name="processHandleDTO.taskId" id="flow_handle_taskId" />
			<input type="hidden" name="processHandleDTO.eno" id="flow_handle_eno" />
			<input type="hidden" name="processHandleDTO.operator" value="${loginUserName}">
			<input type="hidden" name="processHandleDTO.assignType" id="flow_handle_assignType">
			<input type="hidden" name="processHandleDTO.outcome" id="flow_handle_nextStep" >
			<input type="hidden" name="processHandleDTO.nextActivityType" id="flow_handle_activityType" >
			<input type="hidden" name="processHandleDTO.allowUseDynamicAssignee" id="flow_handle_allowUseDynamicAssignee" >
			<input type="hidden" name="processHandleDTO.flowActivityId" id="flow_handle_activity_id" >
			<input type="hidden" name="processHandleDTO.approverTask" id="flow_handle_approverTask">
			<input type="hidden" name="processHandleDTO.module" id="flow_handle_module" />
			<input type="hidden" name="processHandleDTO.variablesAssigneeType" id="flow_handle_variablesAssigneeType" />
			<input type="hidden" name="processHandleDTO.leaderNum" id="flow_handle_leaderNum" />
			<input type="hidden" name="processHandleDTO.allowUseVariablesAssignee" id="flow_handle_allowUseVariablesAssignee" >
			<input type="hidden" name="processHandleDTO.variablesAssigneeGroupNo" id="flow_handle_variablesAssigneeGroupNo" >
			<input type="hidden" name="processHandleDTO.variablesAssigneeGroupName" id="flow_handle_variablesAssigneeGroupName" >
			<input type="hidden" name="processHandleDTO.mailHandlingProcess" id="flow_handle_mailHandlingProcess" >
			<input type="hidden" name="processHandleDTO.activityName" id="task_assignee_activityName" >
			<table style="width:100%" cellspacing="1">
				<tr>
					<td style="width: 25%;"><fmt:message key="label.request.processMark" /></td>
					<td>
						<textarea style="width: 90%;height: 85px" id="flow_handle_remark" name="processHandleDTO.remark" class="form-control"  required="true" validType="maxLength[255]"></textarea>
					</td>
				</tr>
				<tr id="flow_handle_group_tr" style="display: none">
					<td style="width: 25%;"><br/><fmt:message key="label.request.group" />&nbsp;</td>
					<td><br/>
					<input type="hidden" name="processHandleDTO.groupNo" id="flow_handle_assigneeGroupNo"/>
					<input name="assigneeGroupName" id="flow_handle_assigneeGroupName" class="form-control" style="width: 90%;float: left;"  readonly="readonly"
					onclick="wstuo.orgMge.organizationUtil.selectServiceOrg('#index_selectServiceOrg_window_tree','#index_selectServiceOrg_window','#flow_handle_assigneeGroupNo','#flow_handle_assigneeGroupName')" />					
					<h5 style="float: left;padding-left: 8px;"><a id="groupNameCleanButton" onclick="cleanIdValue('flow_handle_assigneeGroupNo','flow_handle_assigneeGroupName')" title="<fmt:message key="label.request.clear" />"  >
					<i class="glyphicon glyphicon-trash"></i></a></h5>
					</td>
				</tr>
				<tr id=flow_handle_assignee_tr style="display: none">
					<td style="width: 25%;"><br/><fmt:message key="label.request.technician" />&nbsp;</td>
					<td><br/>
					<input type="hidden" name="processHandleDTO.assigneeNo" id="flow_handle_assigneeNo" />
					<input name="assigneeName" id="flow_handle_assigneeName"  readonly="readonly" class="form-control" style="width: 90%;float: left;"
					onclick="wstuo.jbpmMge.processCommon.processHandle_assigneeName()" />
					<h5 style="float: left;padding-left: 8px;"><a id="assigneeNameCleanButton" onclick="cleanIdValue('flow_handle_assigneeNo','flow_handle_assigneeName')" title="<fmt:message key="label.request.clear" />">
					<i class=" glyphicon glyphicon-trash"></i></a></h5>
					</td>
				</tr>
				<tr id="flow_handle_approversMemberSet_tr" style="display: none">
					<td style="width: 25%;"><br/><fmt:message key="title.change.memberApprover" /></td>
					<td><br/>
					<input name="processHandleDTO.approversMemberSet" id="flow_handle_approversMemberSet_show"  readonly="readonly" class="form-control" style="width: 90%;float: left;" />
					<h5 style="float: left;padding-left: 8px;"><a class="easyui-linkbutton" plain="true" icon="icon-clean"  onclick="cleanIdValue('flow_handle_approversMemberSet_show','')" title="<fmt:message key="label.request.clear" />">
					<i class=" glyphicon glyphicon-trash"></i></a></h5>
					</td>
				</tr>
				<tr id=flow_handle_approverTask_tr style="display: none">
					<td style="width: 25%;"><br/><fmt:message key="title.bpm.approverTask" /></td>
					<td><br/>
						<input type="radio" name="processHandleDTO.approverResult" value="true" checked="checked" /><fmt:message key="label.request.requestApprovalAgree" />
						<input type="radio" name="processHandleDTO.approverResult" value="false" /><fmt:message key="label.request.requestApprovalDisagree" />
					</td>
				</tr>
				<tr id=flow_handle_closeNoticeEmailAddress_tr style="display: none">
					<td style="width: 25%;"><br/><fmt:message key="lable.notice.noticeAssignMial" /></td>
					<td><br/>
						<input id="closeNoticeEmailAddress" name="processHandleDTO.emailAddress" class="form-control" style="width: 90%;float: left;"/>				
						<h4 style="float: left;padding-left: 8px;"><a href="javascript:wstuo.user.userUtil.selectUserMulti('#closeNoticeEmailAddress','','email',-1)">
						<i class="glyphicon glyphicon-user"></i></a></h4>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<span class="btn btn-primary btn-sm" id="processHandleButton" style="margin-top:10px;margin-right:20px;display: none" ><fmt:message key="common.submit" /></span>
						
						<span class="btn btn-primary btn-sm" id="openProcessButton" style="margin-top:10px;margin-right:15px;display: none" ><fmt:message key="common.submit" /></span>
					</td>
				</tr>
			</table>
		</form>
	</div>
	<%-- 流程处理窗口 end --%>

</body>
</html>