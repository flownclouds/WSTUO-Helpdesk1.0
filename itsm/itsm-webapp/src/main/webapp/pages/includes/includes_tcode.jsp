<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../language.jsp" %>


<script type="text/javascript">

	var tcode_title_security_companyInfo='<fmt:message key="title.security.companyInfo"/>';
	var tcode_title_security_orgSet='<fmt:message key="title.security.orgSet"/>';
	var tcode_title_security_customerManager='<fmt:message key="title.security.customerManager"/>';
	var tcode_title_security_supplierManager='<fmt:message key="title.security.supplierManager"/>';
	var tcode_label_itsop_customer='<fmt:message key="label.itsop.customer"/>';
	var tcode_label_edit_user_password='<fmt:message key="label.edit.user.password"/>';
	var tcode_title_security_userManager='<fmt:message key="title.security.userManager"/>';
	var tcode_title_security_roleManager='<fmt:message key="title.security.roleManager"/>';
	var tcode_title_security_userOnlineLog='<fmt:message key="title.security.userOnlineLog"/>';
	var tcode_title_security_userOptLog='<fmt:message key="title.security.userOptLog"/>';
	var tcode_title_security_slaContract='<fmt:message key="title.security.slaContract"/>';
	var tcode_title_security_processManage='<fmt:message key="title.security.processManage"/>';
	var tcode_title_security_processHistoryManage='<fmt:message key="title.security.processHistoryManage"/>';
	var tcode_title_security_processUseSet='<fmt:message key="title.security.processUseSet"/>';
	var tcode_label_basicConfig_currencySet='<fmt:message key="label.basicConfig.currencySet"/>';
	var tcode_label_historyData_dataRecovery='<fmt:message key="label.historyData.dataRecovery"/>';
	var tcode_title_dashboard_setting='<fmt:message key="title.dashboard.setting" />';
	var tcode_title_tool_affiche='<fmt:message key="title.tool.affiche"/>';
	var tcode_title_tool_sms='<fmt:message key="title.tool.sms"/>';
	var tcode_title_tool_email='<fmt:message key="title.tool.email"/>';
	var tcode_title_tool_im='<fmt:message key="title.tool.im"/>';
	var tcode_title_personTask='<fmt:message key="common.myTask"/>';
	var tcode_title_security_roleManager='<fmt:message key="title.security.roleManager"/>';
	var tcode_title_security_resourceManager='<fmt:message key="title.security.resourceManager"/>';
	var tcode_title_security_LDAPManager='<fmt:message key="title.security.LDAPManager"/>';
	var tcode_title_security_areaUser='<fmt:message key="title.security.areaUser"/>';
	var tcode_title_ldap_authentication_setting='<fmt:message key="ldap_authentication_setting"/>';
	var tcode_title_export_error_log='<fmt:message key="label.export.error.log"/>';
	var tcode_title_rule_ruleGrid='<fmt:message key="title.rule.ruleGrid" />';
	var tcode_title_process_custom='<fmt:message key="label.process.custom"/>';
	var tcode_title_task_manage='<fmt:message key="title.scheduled.tasks.manage" />';
	var tcode_title_back_database='<fmt:message key="label.backup.database" />';
	var tcode_title_dashboard_template='<fmt:message key="title.dashboard.template" />';
	var tcode_title_logo_set='<fmt:message key="label.logo.set"/>';
	var tcode_title_timeZone_setting='<fmt:message key="title.timeZone.setting" />';
	var tcode_title_scheduleManage='<fmt:message key="title.scheduleManage" />';
	var tcode_title_report_export='<fmt:message key="title.mainTab.report.export"/>';
	var reporti18n = $('#reportLang').val();
	var t_code_map={
			<c:if test="${requestHave eq true}">
			<sec:authorize url="/pages/request!findRequestPager.action">
				"VIRL":{"title":i18n['title_request_requestGrid'],"url":"../pages/itsm/request/requestMain.jsp?countQueryType=all&currentUser="+userName},
			</sec:authorize>
			</c:if>
			<c:if test="${requestHave eq true}">
			<sec:authorize url="/pages/request!saveRequest.action">
				"CRNR":{"title":i18n["title_request_addRequest"],"url":"../pages/itsm/request/addRequestFormCustom.jsp"},
			</sec:authorize>
			</c:if>
			
			<c:if test="${problemHave eq true}">
			<sec:authorize url="/pages/problem!findProblemPager.action">
				"VIPL":{"title":i18n['problem_grid'],"url":"../pages/itsm/problem/problemMain.jsp?countQueryType=all&currentUser="+userName},
			</sec:authorize>
			</c:if>			
			<c:if test="${problemHave eq true}">
			<sec:authorize url="/pages/problem!saveProblem.action">
				"CRNP":{"title":i18n['problem_add'],"url":"../pages/itsm/problem/addProblem.jsp"},
			</sec:authorize>
			</c:if>
				
			<c:if test="${changeHave eq true}">
		 	<sec:authorize url="/pages/change!findChangePager.action">
				"VLCL":{"title":i18n['title_changeList'],"url":"../pages/itsm/change/changeMain.jsp?countQuery=all&creator="+userName},
			</sec:authorize>
			</c:if>
			<c:if test="${changeHave eq true}">
			<sec:authorize url="/pages/change!saveChange.action">
				"CRNC":{"title":i18n['titie_change_add'],"url":"../pages/itsm/change/addChange.jsp"},
			</sec:authorize>
			</c:if>

			<sec:authorize url="KNOWLEGEINFO_VIEWKNOWLEGE">
				"VLKL":{"title":i18n['title_request_knowledgeGrid'],"url":"../pages/common/knowledge/knowledgeMain.jsp"},
			</sec:authorize>
			<sec:authorize url="KNOWLEDGEINFO_ADDKNOWLEDGE">
				"CRNK":{"title":i18n['title_request_newKnowledge'],"url":"../pages/common/knowledge/addKnowledge.jsp"},
			</sec:authorize>
				<c:if test="${releaseHave eq true}">	
			<sec:authorize url="/pages/release!findReleasePager.action">
				"RLLS":{"title":i18n['release_grid'],"url":"../pages/itsm/release/releaseMain.jsp"},
			</sec:authorize>
				</c:if>
				<c:if test="${releaseHave eq true}">
			<sec:authorize url="RELEASE_MANAGE_addIssue">
				"CRRL":{"title":i18n['release_add'],"url":"../pages/itsm/release/addRelease.jsp"},
			</sec:authorize>
				</c:if>
			<c:if test="${cimHave eq true}">
			<sec:authorize url="/pages/ci!cItemSave.action">
				"CIMG":{"title":i18n['ci_configureItemAdmin'],"url":"../pages/itsm/cim/configureItem.jsp"},
			</sec:authorize>
			</c:if>
			<c:if test="${cimHave eq true}">
			<sec:authorize url="/pages/ci!cItemSave.action">
				"CRNCI":{"title":i18n['ci_addConfigureItem'],"url":"ci!configureItemAdd.action"},
			</sec:authorize>
			</c:if>

			<sec:authorize url="COMPANY">
				"CPIF":{"title":tcode_title_security_companyInfo,"url":"../pages/common/security/company.jsp"},
			</sec:authorize>
			
			<sec:authorize url="ORGMG">
				"OGST":{"title":tcode_title_security_orgSet,"url":"../pages/common/security/organization.jsp"},
			</sec:authorize>

/* 			<sec:authorize url="/pages/customer!findByCompany.action">	
				"CSMGR":{"title":tcode_title_security_customerManager,"url":"../pages/common/security/customerMain.jsp"},
			</sec:authorize>

			<sec:authorize url="/pages/supplier!findByCompany.action">
				"SLMGR":{"title":tcode_title_security_supplierManager,"url":"../pages/common/security/supplierMain.jsp"},
			</sec:authorize> */
				
			<sec:authorize url="ITSOP_MAIN">	
				"IUMGR":{"title":tcode_title_security_customerManager,"url":"../pages/itsm/itsop/ITSOPUserMain.jsp"},
			</sec:authorize>
			<sec:authorize url="MODIFY_PASSWORD_MODIFY">
				"EDPWD":{"title":tcode_label_edit_user_password,"url":"../pages/common/security/editUserPassword.jsp"},
			</sec:authorize>
			<sec:authorize url="/pages/user!find.action">
				"USMGR":{"title":tcode_title_security_userManager,"url":"../pages/common/security/user.jsp"},
			</sec:authorize>
			<sec:authorize url="/pages/role!find.action">
				"RLMGR":{"title":tcode_title_security_roleManager,"url":"../pages/common/security/role.jsp"},
			</sec:authorize>
			/* <sec:authorize url="/pages/operation!find.action">	
				"RSMNG":{"title":tcode_title_security_resourceManager,"url":"../pages/common/security/resource.jsp"},
			</sec:authorize> */
			<sec:authorize url="LDAP_manage_queryList">	
				"LDAPA":{"title":tcode_title_security_LDAPManager,"url":"../pages/common/security/ldap.jsp"},
			</sec:authorize>				
			<sec:authorize url="AREAUSER_ROES">	
				"DMUL":{"title":tcode_title_security_areaUser,"url":"../pages/common/security/adUser.jsp"},
			</sec:authorize>
			<sec:authorize url="CREATE_LDAP_ROSE">	
				"ATST":{"title":tcode_title_ldap_authentication_setting,"url":"../pages/common/security/ldapAuth.jsp"},
			</sec:authorize>
			
			<sec:authorize url="TABUTITILITY_REZHI">	
				"EPEL":{"title":tcode_title_export_error_log,"url":"../pages/common/config/onlinelog/errorLogMain.jsp"},
			</sec:authorize>
				
			<sec:authorize url="operation_rule_notifyRule">	
				"NTRL":{"title":i18n['notitceRule'],"url":"../pages/common/tools/noticeRule/noticeRule.jsp"},
			</sec:authorize>
			<sec:authorize url="/pages/useronlinelog!findPager.action">		
				"USOL":{"title":tcode_title_security_userOnlineLog,"url":"../pages/common/config/onlinelog/userOnlineLog.jsp"},
			</sec:authorize>

			<sec:authorize url="/pages/useroptlog!findPager.action">
				"UOOG":{"title":tcode_title_security_userOptLog,"url":"../pages/common/config/onlinelog/userOptLog.jsp"},
			</sec:authorize>

			<c:if test="${requestHave eq true}">
			<sec:authorize url="/pages/slaContractManage!find.action">
				"SLAM":{"title":tcode_title_security_slaContract,"url":"../pages/common/sla/slaMain.jsp"},
			</sec:authorize>
			</c:if>
			
			<sec:authorize url="/pages/jbpm!findPageProcessDefinitions.action">
				"PSMGR":{"title":tcode_title_security_processManage,"url":"../pages/common/jbpm/ProcessDefined.jsp"},
			</sec:authorize>
				
			<sec:authorize url="/pages/upload!showHistories.action">
				"PHMGR":{"title":tcode_title_security_processHistoryManage,"url":"../pages/common/jbpm/showProcessHistories.jsp"},
			</sec:authorize>
			<sec:authorize url="/pages/jbpm!findPageProcessDefinitions.action">
				"PUMGR":{"title":tcode_title_security_processUseSet,"url":"../pages/common/jbpm/processUse.jsp"},
			</sec:authorize>
			<sec:authorize url="/pages/utility!flowDesigner.action">
				"CSTP":{"title":tcode_title_process_custom,"url":"../pages/common/jbpm/flowDesigner.jsp"},
			</sec:authorize>

			<sec:authorize url="CURRENCYSET_RES">
				"CUSL":{"title":tcode_label_basicConfig_currencySet,"url":"../pages/common/config/basicConfig/currencyMain.jsp"},
			</sec:authorize>

			/* <sec:authorize url="DATARECOVERY_RES">
				"DTRS":{"title":tcode_label_historyData_dataRecovery,"url":"../pages/historyData/historyDataMain.jsp"},
			</sec:authorize> */

			<sec:authorize url="PORTAL_DASHBOARD_RES">
				"PPSL":{"title":tcode_title_dashboard_setting,"url":"userCustom!portalDashboardPreview.action?queryDTO.customType=1&queryDTO.loginName=${sessionScope.loginUserName}"},
			</sec:authorize>
				
			<sec:authorize url="SCHEDULEDTASK_RES">
				"SDTM":{"title":tcode_title_task_manage,"url":"../pages/common/tools/scheduledTask/scheduledTasksManage.jsp"},
			</sec:authorize>
				
			/* <sec:authorize url="database_main">
				"DTBS":{"title":tcode_title_back_database,"url":"../pages/common/config/backup/backupMain.jsp"},
			</sec:authorize> */
				<sec:authorize url="TEMPLATE_MAIN">
				"CTTM":{"title":tcode_title_dashboard_template,"url":"../pages/common/config/template/templateMain.jsp"},
			</sec:authorize>
			
			<sec:authorize url="timeZone_set">
				"TZST":{"title":tcode_title_timeZone_setting,"url":"../pages/common/config/basicConfig/timeZoneMain.jsp"},
			</sec:authorize>
			<sec:authorize url="REPORT_MAIN">
				"RPTS":{"title":tcode_title_report_export,"url":""},
			</sec:authorize>
			<sec:authorize url="TOOLS_VIEWAFFICHE">
				"VLAL":{"title":tcode_title_tool_affiche,"url":"../pages/common/tools/affiche/affiche.jsp"},
			</sec:authorize>
				
			<sec:authorize url="/pages/sms!sendSMS.action">
				"SMSM":{"title":tcode_title_tool_sms,"url":"../pages/common/tools/sms/sms.jsp"},
			</sec:authorize>
				
			<sec:authorize url="/pages/email!find.action">
				"EMMG":{"title":tcode_title_tool_email,"url":"../pages/common/tools/email/email.jsp"},
			</sec:authorize>
				
			<sec:authorize url="IMSL">
				"IMMG":{"title":tcode_title_tool_im,"url":"../pages/common/tools/im/im.jsp"},
			</sec:authorize>
			
			"PTMG":{"title":tcode_title_personTask,"url":"../pages/common/tools/task/task.jsp"},
				
			<sec:authorize url="/pages/scheduled!findScheduleRequest.action">
				"PRPI":{"title":tcode_title_scheduleManage,"url":"../pages/common/tools/schedule/scheduleMain.jsp"},
			</sec:authorize>
	};
	var tCodeJSON=[{"label":"HELP","value":"HELP"}];
	$.each(t_code_map,function(k,v){
		tCodeJSON.push({"label":k+"（"+v["title"]+"）","value":k});
	});
	itsm.app.autocomplete.autocomplete.bindTcodeAutoComplete('#t_code_cmd',tCodeJSON);  //T_code
</script>


