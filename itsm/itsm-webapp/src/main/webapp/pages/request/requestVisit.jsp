<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.opensymphony.xwork2.ActionContext" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../language.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-1.4.2.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/comm.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/scripts/jquery/easyui/themes/icon.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/scripts/jquery/jqgrid/redmond/jquery-ui-1.8.1.custom.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">

<style>.tabs li{margin-left:5px}</style>
<script src="${pageContext.request.contextPath}/scripts/i18n/i18n_${lang}.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery.xml2json.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/easyui/plugins/jquery.form.js"></script>
<%-- autocomplete--%>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.core.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.widget.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.position.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.datepicker.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.dialog.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.button.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/easyui/plugins/jquery.resizable.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/easyui/plugins/jquery.validatebox.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery.floatingmessage.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/easyui/locale/easyui-lang-${lang}.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jstree/js/lib/jquery.cookie.js?random=${currentTime}"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jstree/js/lib/jquery.hotkeys.js?random=${currentTime}"></script>
<script src="${pageContext.request.contextPath}/scripts/core/import.js?random=${currentTime}"></script>
<script src="${pageContext.request.contextPath}/scripts/core/package.js?random=${currentTime}"></script>
<script src="${pageContext.request.contextPath}/scripts/basics/jqgrid_common.js"></script>
<script src="${pageContext.request.contextPath}/scripts/basics/jquery_common.js"></script>
<script src="${pageContext.request.contextPath}/scripts/basics/easyui_common.js"></script>
<script src="${pageContext.request.contextPath}/scripts/basics/initUploader.js?random=${currentTime}"></script>
<script src="${pageContext.request.contextPath}/scripts/basics/ie6.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/uploadify/swfobject.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-json.js"></script>
<script src="${pageContext.request.contextPath}/scripts/itsm/request/requestVisit.js"></script>
<script>
//只允许输入数字    
function chkPrice(obj){
obj.value = obj.value.replace(/[^\d.]/g,"");
//必须保证第一位为数字而不是.
obj.value = obj.value.replace(/^\./g,"");
//保证只有出现一个.而没有多个.
obj.value = obj.value.replace(/\.{2,}/g,"");
//保证.只出现一次，而不能出现两次以上
obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$","");
}
function chkLast(obj){
// 如果出现非法字符就截取掉
if(obj.value.substr((obj.value.length - 1), 1) == '.')
obj.value = obj.value.substr(0,(obj.value.length - 1));
} 
</script>
<fmt:setLocale value="${param.language}"/>
<fmt:setBundle basename="i18n.itsmbest"/>
<c:set var="lang" value="${param.language}"/>
<title><fmt:message key="label.request.requestVisit"/></title>
</head>
<body>
<div id="requestVisit_win" class="easyui-panel" title="<fmt:message key="label.request.requestVisit"/>">
	<form>
		<div>
		
		<input type="hidden" name="userReturnVisitDTO.returnVisitDetail" id="request_visitRecord"/>
		<input type="hidden" name="userReturnVisitDTO.visitId" id="requestVisit_visitId" value="${param.visitId}"/>
	
		<div class="lineTableBgDiv">
			<div>
			<table style="width:100%;height: 40px;" id="requestVisit_table"  class="lineTable" cellspacing="1">
				
			
			</table>
			</div>
			
		</div>
		
		</div>
		
		
	</form>
	<div id="visit_opt" style="padding: 5px;background-color: #dae6fc;p">
		<input type="button" id="visit_button"  value="<fmt:message key="common.submit"/>">
	</div>
</div>
<div style="height: 0px;overflow: hidden;width: 0px;position:relative">
<div id="alertMSG" title="<fmt:message key="msg.tips" />">
	
</div>
</div>
</body>

</html>
