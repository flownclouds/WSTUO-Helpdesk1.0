<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="../../language.jsp" %>       

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>选择关联请求includes文件</title>
</head>
<body>
<!-- 选择关联请求 -->
<div id="index_related_request_window" class="WSTUO-dialog" title="<fmt:message key="title.change.selectRelatedReuqest" />" style="width:auto;height:auto;padding:3px;">
	<table id="index_related_request_grid"></table>
  	<div id="index_related_request_grid_pager"></div>
  		
  	<div id="index_related_request_grid_toolbar"  style="display:none;padding:3px;">
	<form id="index_related_request_grid_form">
		<select id="related_request_search_type" onchange="itsm.request.requestUtil.relatedRequestSearchTypeChange(this.value)">
		<option value="0"><fmt:message key="lable.searchType.db" /></option>
		<option value="1"><fmt:message key="lable.searchType.index" /></option>
		</select>
		<span id="index_related_request_grid_requestCode">
		&nbsp;<fmt:message key="common.id" />&nbsp;
		<input name="requestQueryDTO.requestCode"  style="width:95px"/>
		</span>
		&nbsp;<fmt:message key="label.keywords" />&nbsp;
		<input name="requestQueryDTO.etitle" id="index_related_request_grid_etitle"/>
		&nbsp;
		<a class="easyui-linkbutton" plain="true" icon="icon-search" id="index_related_request_grid_search" title="<fmt:message key="common.search" />"></a>
		<a class="easyui-linkbutton" plain="true" icon="icon-ok" id="index_related_request_grid_select" title="<fmt:message key="common.select"/>"></a>
	</form>
	</div>  
</div>
</body>
</html>