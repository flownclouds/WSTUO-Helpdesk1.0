<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../language.jsp" %>

<%
String keyWord="";
if(request.getParameter("keyWord")!=null){
	keyWord=new String(request.getParameter("keyWord").getBytes("iso8859-1"),"UTF-8"); 
}
String rowValue="";
String colValue="";
if(request.getParameter("rowValue")!=null){
	rowValue=request.getParameter("rowValue");
	rowValue=java.net.URLDecoder.decode(rowValue, "utf-8");
}
if(request.getParameter("colValue")!=null){
	colValue=request.getParameter("colValue");
	colValue=java.net.URLDecoder.decode(colValue, "utf-8");
}
request.setAttribute("rowValue",rowValue);
request.setAttribute("colValue",colValue);
%>
<script>
var rowValue="${rowValue}";
var colValue="${colValue}";
var rowKey='${param.rowKey}';
var colKey='${param.colKey}';
var customFilterNo='${param.customFilterNo}';
var sec_editRequest="0";
var _eavId='${param.eavId}';
var sec_deleteRequest="0";
var filterId='${param.filterId}';
var userName="${loginUserName}";
var keyWord="<%=keyWord%>";
var countQueryType='${param.countQueryType}';
var currentUser='${loginUserName}'
var request_fullSearchFlag='${param.fullsearch}';
var requestMain_companyNo='${param.companyNo}';
var tempUserName='${param.tempUserName}'
var tabValue="";
if(tempUserName!=''){
	currentUser=tempUserName;
}
</script>
<script>var allRequest_Res=false;</script>
<sec:authorize url="AllRequest_Res">
<script>allRequest_Res=true;</script>
</sec:authorize>
<sec:authorize url="/pages/request!updateRequest.action">
<script>sec_editRequest="1";</script>
</sec:authorize>
<sec:authorize url="/pages/request!deleteRequests.action">
<script>sec_deleteRequest="1";</script>
</sec:authorize>
<script src="../js/itsm/request/requestMain.js"></script>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-list-alt"></i> 请求列表</h2>
            </div>
            <div class="box-content buttons" id="">
                <table id="requestGrid" ></table>
    			<div id="requestGridPager"></div>
            </div>
        </div>
    </div>
</div>
   	
    
    <div id="requestGridToolbar" style="display: none;">
    &nbsp;
    <sec:authorize url="/pages/request!saveRequest.action">
	<a class="btn btn-default btn-xs" onclick="itsm.request.requestMain.showAddRequest()" title="<fmt:message key="common.add"/>"><i class="glyphicon glyphicon-plus"></i> <fmt:message key="common.add"/></a>
	</sec:authorize>
	<sec:authorize url="/pages/request!updateRequest.action">
	<a class="btn btn-default btn-xs"  onclick="itsm.request.requestMain.editRequest_aff()" title="<fmt:message key="common.edit"/>"><i class="glyphicon glyphicon-edit"></i> <fmt:message key="common.edit"/></a> 
	</sec:authorize>
	<sec:authorize url="/pages/request!deleteRequests.action">
	<a class="btn btn-default btn-xs" onclick="itsm.request.requestMain.deleteRequest_aff()" title="<fmt:message key="common.delete"/>"><i class="glyphicon glyphicon-trash"></i> <fmt:message key="common.delete"/></a> 
	</sec:authorize>
	
	<a class="btn btn-default btn-xs" onclick="itsm.request.requestMain.searchRequestOpenWindow()" title="<fmt:message key="common.search"/>"><i class="glyphicon glyphicon-search"></i> <fmt:message key="common.search"/></a>

	
	<sec:authorize url="rquestGrid_export">
	<a onclick="itsm.request.requestMain.exportRequestView()" class="btn btn-default btn-xs"  title="<fmt:message key="label.dc.exportCurrentView"/>"><i class="glyphicon glyphicon-share"></i> <fmt:message key="label.dc.exportCurrentView"/></a>
	</sec:authorize>
	
	
	<c:if test="${problemHave eq true}">
	<sec:authorize url="/pages/problem!saveProblem.action">
	<sec:authorize url="request_manage_getProblem">
	<a class="btn btn-default btn-xs" onclick="itsm.request.requestMain.request2Problem()"  title="<fmt:message key="label.problem.submitProblem"/>"><i class="glyphicon glyphicon-share"></i> <fmt:message key="label.problem.submitProblem"/></a>
	</sec:authorize>
	</sec:authorize>
	</c:if>
	
	<c:if test="${changeHave eq true}">
	<sec:authorize url="/pages/change!saveChange.action">
	<sec:authorize url="request_manage_getChange">
	<a class="btn btn-default btn-xs" onclick="itsm.request.requestMain.requestMainToChange()" title="<fmt:message key="title.change.submit"/>"><i class="glyphicon glyphicon-share"></i> <fmt:message key="title.change.submit"/></a>
	</sec:authorize>
	</sec:authorize>
	</c:if> 
	
	<sec:authorize url="request_manage_order">
	<a class="btn btn-default btn-xs" onclick="itsm.request.requestMain.requestToPrint()" title="<fmt:message key="request.workOrder"/>"><i class="glyphicon glyphicon-list-alt"></i> <fmt:message key="request.workOrder"/></a>
	</sec:authorize>
	<sec:authorize url="request_manage_BatchClose">
	<a class="btn btn-default btn-xs" onclick="itsm.request.requestMain.requestBatchClose_win()" title="<fmt:message key="lable.requestBatchClose"/>"><i class="glyphicon glyphicon-remove-circle"></i> <fmt:message key="lable.requestBatchClose"/></a>
	</sec:authorize>
	
	<sec:authorize url="itsm_request_requestMain_getDataByFilterSearch">
	<a class="btn btn-default btn-xs" onclick="itsm.request.requestMain.openCustomFilterWin()"  title="<fmt:message key="lable.customFilter.self_defined"/>"><i class="glyphicon glyphicon-filter"></i> <fmt:message key="lable.customFilter.self_defined"/></a>
	&nbsp;<select id="request_userToSearch" onchange="itsm.request.requestMain.getDataByFilterSearch(this.value)"></select>
	</sec:authorize>
	<%--  
	&nbsp;|&nbsp;<a href="javascript:showHelpFilm('request_workflow.flv')" class="btn btn-default btn-xs" icon="icon-help"></a>
		
	
	|<a class="btn btn-default btn-xs" onclick="javascript:itsm.request.requestMain.importRequestData()" icon=""><fmt:message key="common.import" /></a>
  --%>
	</div>

<div id="requestGridFormatterDiv" style="display: none;">&nbsp;&nbsp;<a href="JavaScript:itsm.request.requestMain.requestDetails('{eno}')" title="<fmt:message key="common.detailInfo" />"><i class="glyphicon glyphicon-list"></i></a>&nbsp;&nbsp;<sec:authorize url="/pages/request!updateRequest.action"><a href="javascript:itsm.request.requestMain.editRequest('{eno}','{statusDno}')" title="<fmt:message key="common.edit" />"><i class="glyphicon glyphicon-edit"></i></a></sec:authorize>&nbsp;&nbsp;<sec:authorize url="/pages/request!deleteRequests.action"><a href="javascript:itsm.request.requestMain.deleteRequest('{eno}')" title="<fmt:message key="common.delete" />"><i class="glyphicon glyphicon-trash"></i></a></sec:authorize>&nbsp;&nbsp;</div>
	<div id="request_reopen_opt_div" style="display: none;"><a id="request_reopen_opt_but"><fmt:message key="label.request.reStartRequest" /></a></div>

	<form action="request!exportRequest.action" method="post" id="export_request_form">
	<div id="export_request_values">
	
	</div>
	</form>
	<!-- 请求搜索 start -->
	<div id="searchRequestWindow" title="<fmt:message key="title.request.searchRequest" />" class="WSTUO-dialog" style="width:600px; height:auto;padding:2px">
	<form action="request!exportRequest.action" method="post" id="searchRequestForm">
        <input type="hidden" name="requestQueryDTO.countQueryType" id="request_countQueryType"  value="${param.countQueryType}" />
	    <input type="hidden" name="requestQueryDTO.currentUser" id="request_currentUser" value="${param.currentUser}" /> 
		<div class="panel panel-default">
			<div class="panel-body form-horizontal">
				
                       <div class="form-group" <c:if test="${versionType!='ITSOP'}"><c:out value="style=display:none"></c:out></c:if>>
                          <label class="col-sm-2 control-label" ><fmt:message key="label.belongs.client" /></label>
                          <div class="col-sm-4">
                            <input type="hidden" value="-1" name="requestQueryDTO.companyNo" id="request_search_companyNo" />
							<input id="main_request_companyName" class="form-control"/>
							<a id="icon_search_request_companyName" title="<fmt:message key="common.update" />">
								<span class="glyphicon glyphicon-user form-control-feedback choose"></span>
							</a>
							<a onclick="cleanIdValue('request_search_companyNo','main_request_companyName')" title="<fmt:message key="label.request.clear" />">
							 <span class="glyphicon glyphicon-trash form-control-clean choose"></span>
							</a>
                          </div>
                          <label class="col-sm-2 control-label" for="ds_name"><fmt:message key="title.requestor.org" /></label>
                          <div class="col-sm-4">
                             <input id="search_requestorOrgNo" name="requestQueryDTO.requestorOrgNo" type="hidden"/>
					         <input  id="search_requestorOrgName" class="form-control"/>
					         <a  onclick="cleanIdValue('search_requestorOrgNo','search_requestorOrgName')" title="<fmt:message key="label.request.clear"/>">
					         <span class="glyphicon glyphicon-trash form-control-feedback choose"></span>
					         </a>		 
                          </div>
                       </div>
                       
                       <div class="form-group">
                          <label class="col-sm-2 control-label" ><fmt:message key="common.title" /></label>
                          <div class="col-sm-4">
                             <input name="requestQueryDTO.etitle" id="searchRequest_etitle" class="form-control" />
                          </div>
                          <label class="col-sm-2 control-label" ><fmt:message key="common.id" /></label>
                          <div class="col-sm-4">
                             <input id="searchRequest_requestCode" name="requestQueryDTO.requestCode" class="form-control" >
                          </div>
                       </div>
                       
                        <div class="form-group">
                          <label class="col-sm-2 control-label" ><fmt:message key="common.category" /></label>
                          <div class="col-sm-4">
                                <input type="hidden" id="searchRequest_ecategoryNo" name="requestQueryDTO.ecategoryNo" >
								<input id="searchRequest_ecategoryName" class="form-control" readonly >
								<a onclick="cleanIdValue('searchRequest_ecategoryNo','searchRequest_ecategoryName')" title="<fmt:message key="label.request.clear" />">
								<span class="glyphicon glyphicon-trash form-control-feedback choose"></span>
								</a>
                          </div>
                          <label class="col-sm-2 control-label" ><fmt:message key="common.state" /></label>
                          <div class="col-sm-4">
                             <select id="searchRequest_statusNo" name="requestQueryDTO.statusNo" class="form-control">
								<option value=""></option>
							</select>
                          </div>
                       </div>
				
				      <div class="form-group">
                          <label class="col-sm-2 control-label" ><fmt:message key="label.request.priority" /></label>
                          <div class="col-sm-4">
                             <select id="searchRequest_priority" name="requestQueryDTO.priorityNo" class="form-control" ></select>
                          </div>
                           <label class="col-sm-2 control-label" ><fmt:message key="label.dc.SLAStatus" /></label>
                          <div class="col-sm-4">
                             <select id="searchRequest_slaStateNo" name="requestQueryDTO.slaStateNo" class="form-control"></select>
                          </div>
                       </div>
				
				<sec:authorize url="/pages/user!find.action">	
				     <div class="form-group">
                          <label class="col-sm-2 control-label" ><fmt:message key="common.Requester"/></label>
                          <div class="col-sm-4">
                             <input name="requestQueryDTO.createdByFullName" id="search_createdByName" class="form-control" />
		                     <a onclick="cleanIdValue('search_createdByName')" title="<fmt:message key="label.request.clear" />">
		                     <span class="glyphicon glyphicon-trash form-control-feedback choose"></span>
		                     </a>
                          </div>
                          <label class="col-sm-2 control-label" ><fmt:message key="task.ownerName"/></label>
                          <div class="col-sm-4">
                            <input name="requestQueryDTO.ownerFullName" id="search_ownerName" class="form-control" />
		                    <a  onclick="cleanIdValue('search_ownerName')" title="<fmt:message key="label.request.clear" />">
		                    <span class="glyphicon glyphicon-trash form-control-feedback choose"></span>
		                    </a>
                          </div>
                       </div>
                       
                       <div class="form-group">
                          <label class="col-sm-2 control-label" ><fmt:message key="title.change.assigneGroup" /></label>
                          <div class="col-sm-4">
                             <input id="search_assigneGroupNo" name="requestQueryDTO.assigneeGroupNo" type="hidden"/>
					         <input  id="search_assigneGroupName" readOnly class="form-control"/>
					         <a onclick="cleanIdValue('search_assigneGroupNo','search_assigneGroupName')" title="<fmt:message key="label.request.clear"/>">
					         <span class="glyphicon glyphicon-trash form-control-feedback choose"></span>
					         </a>		 
                          </div>
                          <label class="col-sm-2 control-label" ><fmt:message key="label.request.theTechnician"/></label>
                          <div class="col-sm-4">
	                          <input name="requestQueryDTO.assigneeFullName" id="search_assigneeName" class="form-control"/>
			    	          <a onclick="cleanIdValue('search_assigneeName')" title="<fmt:message key="label.request.clear" />">
			    	          <span class="glyphicon glyphicon-trash form-control-feedback choose"></span>
			    	          </a>
                          </div>
                       </div>
				
				</sec:authorize>
				      <%--  <div class="form-group">
                          <label class="col-sm-2 control-label" ><fmt:message key="ci.location"/></label>
                          <div class="col-sm-4">
                             <input id="search_location" name="requestQueryDTO.locationId" type="hidden"/>
					         <input  id="search_locationName" onclick="wstuo.category.eventCategoryTree.selectlocation('#search_locationName','#search_location')"  readOnly class="form-control"/>
					         <a  onclick="cleanIdValue('search_location','search_locationName')" title="<fmt:message key="label.request.clear"/>">
					         <span class="glyphicon glyphicon-trash form-control-feedback choose"></span>
					         </a>		 
                          </div>
                          <label class="col-sm-2 control-label" ><fmt:message key="setting.serviceDirectory"/></label>
                          <div class="col-sm-4">
                             <input id="search_serviceId" name="requestQueryDTO.serviceDirIds" type="hidden"/>
					         <input  id="search_serviceName" onclick="wstuo.category.serviceCatalog.selectSingleServiceDir('#search_serviceName','#search_serviceId')"  readOnly class="form-control"/>
					         <a onclick="cleanIdValue('search_serviceId','search_serviceName')" title="<fmt:message key="label.request.clear"/>">
					         <span class="glyphicon glyphicon-trash form-control-feedback choose"></span>
					         </a>		 
                          </div>
                       </div> --%>
				    
				     <div class="form-group">
                          <label class="col-sm-2 control-label" ><fmt:message key="label.dateRange" /></label>
                          <div class="col-sm-4">
                            <input id="request_search_startTime"  name="requestQueryDTO.startTime" class="form-control"  readonly/>
                          </div>
                          <label class="col-sm-2 control-label" ><fmt:message key="setting.label.to"/></label>
                          <div class="col-sm-4">
                            <input id="request_search_endTime" name="requestQueryDTO.endTime" class="form-control"  validType="DateComparison['request_search_startTime']" readonly/>
		                    <a  onclick="cleanIdValue('request_search_startTime','request_search_endTime')" title="<fmt:message key="label.request.clear" />">
		                    <span class="glyphicon glyphicon-trash form-control-feedback choose"></span>
		                    </a>
                          </div>
                      </div>

					<div class="form-group" id="doSearchRequestDiv">
						<div class="col-sm-9 col-sm-offset-4">
							<input type="hidden" name="requestQueryDTO.keyWord" id="requestQueryDTO_keyWord">
							<button onclick="$('#searchRequestWindow input,#searchRequestWindow select').val('')" class="btn btn-default btn-sm" >
							<fmt:message key="i18n.reset"/></button>
							<button type="button" id="doSearchRequestBtn"class="btn btn-primary btn-sm"><fmt:message key="common.search" /></button>
						</div>
					</div>
				
			</div>
		</div>
	</form>
	</div>
<!-- 请求搜索 end -->
	<!-- 导入数据 -->
	<div id="importRequestDataWindow"  class="WSTUO-dialog" title="<fmt:message key="label.dc.import"/>" style="width:400px;height:auto">
		<form>
	   <div class="lineTableBgDiv" >
	    <table  class="lineTable"  width="100%" cellspacing="1" >
	        <tr>
	            <td width="100px">
	            <fmt:message key="label.dc.filePath"/>
	        
	            </td>
	            <td>
	           	<input type="file" class="easyui-validatebox input" required="true" id="importRequestFile" name="file" onchange="checkFileType(this,'csv')"/>
	            </td>
	        </tr>
	        <tr>
	        	<td colspan="2">
	                <div style="padding-top:8px;">
	                	<a id="searchUserBtn" class="btn btn-default btn-xs" onclick="itsm.request.requestMain.importRequest()"><fmt:message key="label.dc.import"/></a>
	                </div>
	        	</td>
	        </tr>
	    </table>
	    </div>
	    </form>
	</div>
	<%-- 自定义列设置窗口 --%>
	<div id="columnChooserWin_requestGrid"  class="WSTUO-dialog" title="<fmt:message key="label.set.column"/>" style="width:250px;height:300px;padding:1px;">
		<div id="columnChooserDiv_requestGrid" class="">
			<div class="box col-md-12">
                <div ><a href="#" id="columnChooserTopBut_requestGrid" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-ok"></i> <fmt:message key="label.determine"/></a>
                &nbsp;
                <a href="#"  id="columnChooserDefaultBut_requestGrid" class="btn btn-default btn-xs"><fmt:message key="label.dashboard.defautl.show"/></a>
                <span id="columnChooserStatus_requestGrid"></span></div>
                <table class="table">
                    <thead>
                    <tr>
                        <th><input type="checkbox" id="colAllUnSelect_requestGrid" onclick="colAllUnSelect('requestGrid')" /></th>
                        <th class="center"><fmt:message key="label.colName" /></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
		</div>
	</div>
	
	<div id="requestBatchCloseWindow"  class="WSTUO-dialog" title="<fmt:message key="lable.requestBatchClose"/>" style="width:400px;height:auto">
	   <div class="lineTableBgDiv" >
	    <table  class="lineTable"  width="100%" cellspacing="1" >
	        <%-- <tr>
	            <td width="100px">
	            <fmt:message key="report.request.num"/>
	            </td>
	            <td>
	           	<input type="text" class="easyui-validatebox input" id="requestBatchClose_input" name="requestBatchClose"/>
	            </td>
	        </tr> --%>
	         <tr>
	            <td width="100px">
	            <fmt:message key="common.remark"/>
	            </td>
	            <td>
	           		<textarea id="requestDTO_remark" class="form-control"></textarea>
	            </td>
	        </tr>
	        <tr>
	        	<td colspan="2">
	                <span  class="btn btn-primary btn-sm" onclick="itsm.request.requestMain.requestBatchClose()" ><fmt:message key="label.determine"/></span>
	        	</td>
	        </tr>
	    </table>
	    </div>
	</div>

<style>
.col-sm-4 {
    width: 35%;
}
.col-sm-2 {
    width: 15%;
}
.form-control-clean{
    position: absolute;
    right: 0;
    z-index: 2;
    display: block;
    height: 34px;
    line-height: 34px;
    text-align: center;
}
</style>