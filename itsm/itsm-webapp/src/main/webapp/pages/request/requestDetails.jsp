<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../language.jsp" %>
<%-- 详细页面权限加载 --%>
<%@ include file="requestDetailsPermission.jsp" %>

<script>
var slaLevelName="${requestDetailDTO.slaLevelName}";
var request_cost_tool_show_hide=false;
var request_task_tool_show_hide=false;
var requestDetail_offmodeNo="${requestDetailDTO.offmodeNo}";
var operationTaskRes = false;
var isAllowAccessRes = ${isAllowAccessRes};
//标识是否需要初始化解决方案编辑器
var request_detail_solutions_res = false;
var normal_action = false;
</script>

<sec:authorize url="OPERATIONTASKRES">
<script>operationTaskRes=true;</script>
<script src="../js/wstuo/jbpmMge/validMethod.js"></script>
<script src="../js/itsm/request/requestDetail.js"></script>
</sec:authorize>
	<input type="hidden" id="requestDetailStatusNo" value="${requestDetailDTO.statusNo }" />
	<input type="hidden" id="requestDetails_statusCode" value="${requestDetailDTO.statusCode }" />
	<input type="hidden" id="requestDetails_requestNo" value="${requestDetailDTO.eno}"/>
	<input type="hidden" id="rquestDetailCreatedByEmail" value="${requestDetailDTO.createdByEmail}"/>
	
	<input type="hidden" id="rquestDetailCreatedByLoginName" value="${requestDetailDTO.createdByLoginName}"/>
	<input type="hidden" id="requestDetails_requestTitle" value="${requestDetailDTO.etitle}"/>
	<input type="hidden" id="requestDetails_companyNo" value="${requestDetailDTO.companyNo}"/>
	<input type="hidden" id="requestDetails_assigneeGroupNo" value="${requestDetailDTO.assigneeGroupNo}"/>
	<input type="hidden" id="requestDetails_assigneeGroupName" value="${requestDetailDTO.assigneeGroupName}"/>
	<input type="hidden" id="requestDetails_assigneeNo" value="${requestDetailDTO.assigneeNo}"/>
	<input type="hidden" id="requestDetails_assigneeName" value="${requestDetailDTO.assigneeName}"/>
	<input type="hidden" id="requestDetails_assigneeLoginName" value="${requestDetailDTO.assigneeLoginName}"/>
	<input type="hidden" id="requestDetails_maxResponsesTime" value="${requestDetailDTO.maxResponsesTime}"/>
	<input type="hidden" id="requestDetails_maxCompletesTime" value="${requestDetailDTO.maxCompletesTime}"/>
	<input type="hidden" id="requestDetails_createdByName" value="${requestDetailDTO.createdByName}"/>
	<input type="hidden" id="requestDetails_pid" value="${requestDetailDTO.pid}"/>
	<input type="hidden" id="requestDetail_visitRecord" value="${requestDetailDTO.visitRecord}"> 
	<input type="hidden" id="requestDetails_fullName" value="${requestDetailDTO.fullName}"/>
	<input type="hidden" id="requestDetails_categoryEavId" value="${requestDetailDTO.categoryEavId}"/>
	<input type="hidden" id="requestDetails_closeTime" value="${requestDetailDTO.closeTime}"/>
	<input type="hidden" id="requestDetail_responsesTime" value="<fmt:formatDate type="date" value="${requestDetailDTO.responsesTime}"  pattern="yyyy-MM-dd HH:mm:ss" />">
	<input type="hidden" id="requestDetails_requestCategoryNo" value="${requestDetailDTO.requestCategoryNo}"/>
	<input type="hidden" id="requestDetails_effectRange" value="${requestDetailDTO.effectRangeNo}"/>
	<input type="hidden" id="requestDetails_seriousness" value="${requestDetailDTO.seriousnessNo}"/>
	<input type="hidden" id="requestDetails_processKey" value="${requestDetailDTO.processKey}"/>
	<input type="hidden" id="requestDetails_formId" value="${requestDetailDTO.formId}"/>
	<input type="hidden" id="requestDetails_attendance" value="${requestDetailDTO.attendance}"/>
	<input type="hidden" id="requestDetails_createdOn" value="<fmt:formatDate type="date" value="${requestDetailDTO.createdOn}"  pattern="yyyy-MM-dd HH:mm:ss" />"/>
	<input type="hidden" id="requestDetails_slaResponsesTime" value="<fmt:formatDate type="date" value="${requestDetailDTO.maxResponsesTime}"  pattern="yyyy-MM-dd HH:mm:ss" />"/>
	
	
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-list"></i> 请求详细</h2>
            </div>
            <div class="box-content buttons">
            	<div class="row">
   				 	<div class="box col-md-12" id="act_div">
					<div align="left">
					<c:set var="loginUserName" value="${loginUserName}"/>
					<c:set var="request_assignMyGroup" value="false"/>
					<c:set var="request_startFlow" value="false"/>
					<%-- 常规动作start --%>
				  	<c:if test="${(empty requestDetailDTO.closeTime and resAuth['/pages/request!updateRequest.action']) or !empty requestDetailDTO.pid or resAuth['FNULLTEXTSEARCH_RES']}">
				  		<script>normal_action=true;</script>
				  	</c:if>
				  	<c:if test="${empty requestDetailDTO.closeTime}">
				  	<sec:authorize url="/pages/email!toEmail.action">
						<script>normal_action=true;</script>
					</sec:authorize>
					</c:if>
					<sec:authorize url="request_manage_order">
						<script>normal_action=true;</script>
					</sec:authorize>
					<div style="width:100%;margin-left: 8px;">
					<span id="normal_action_start"><fmt:message key="label.itsop.normal.action" />：</span>
				  	<!-- 如果请求未关闭 ,则可以进行编辑-->
					<c:if test="${empty requestDetailDTO.closeTime}">
						<script>
							request_cost_tool_show_hide = true;
							request_task_tool_show_hide = true;
						</script>
						<c:if test="${resAuth['/pages/request!updateRequest.action'] }">
							&nbsp;&nbsp;<a class="btn btn-default btn-xs" id="requestDetails_edit_but"><i class="glyphicon glyphicon-edit"></i> <fmt:message key="common.edit" /></a> 
						</c:if>
						
						<sec:authorize url="/pages/email!toEmail.action">
						&nbsp;&nbsp;<a class="btn btn-default btn-xs" id="request_email_reply" ><i class="glyphicon glyphicon-envelope"></i> <fmt:message key="label.email.reply" /></a>
						</sec:authorize>
						
				    </c:if>
				    <sec:authorize url="request_detail_flowTrack"></sec:authorize><!--不需求控制，能看到请求肯定能跟踪流程 -->
					<c:choose>
						<c:when test="${requestDetailDTO.flowStart eq true}">
						&nbsp;&nbsp;<a class="btn btn-default btn-xs" onclick="wstuo.jbpmMge.processCommon.showFlowChart('${requestDetailDTO.pid}','<fmt:message key="title.mainTab.request" />')"  ><i class="glyphicon glyphicon-random"></i> <fmt:message key="label.request.processTracking" /></a>
						</c:when>
						<c:otherwise>
						&nbsp;&nbsp;<a class="btn btn-default btn-xs" onclick="wstuo.jbpmMge.processCommon.showFlowChartByNoStart('${requestDetailDTO.processKey}','<fmt:message key="title.mainTab.request" />')"  ><i class="glyphicon glyphicon-random"></i> <fmt:message key="label.request.processTracking" /></a>
						</c:otherwise>
					</c:choose>
					
					<c:if test="${problemHave eq true}">
					<c:if test="${resAuth['/pages/problem!saveProblem.action'] }">
						<sec:authorize url="request_manage_getProblem">
				    	&nbsp;&nbsp;<a class="btn btn-default btn-xs" id="request_detail_2problem" ><fmt:message key="label.problem.submitProblem" /></a>
						</sec:authorize>
					</c:if>
					</c:if>
					<c:if test="${requestDetailDTO.statusCode ne 'request_close' }">
					<c:if test="${changeHave eq true}">
					<c:if test="${resAuth['/pages/change!saveChange.action'] }">
						<sec:authorize url="request_manage_getChange">
						&nbsp;&nbsp;<a class="btn btn-default btn-xs" id="submitChangeBtn_info"><fmt:message key="title.change.submit" /></a>
						</sec:authorize>
					</c:if>
					</c:if></c:if>
					<sec:authorize url="request_manage_order">
					&nbsp;&nbsp;<a class="btn btn-default btn-xs" id="requestDetailsToPrint"><i class="glyphicon glyphicon-print"></i> <fmt:message key="request.print.workOrder" /></a>
					</sec:authorize>
					<%-- 
					<c:if test="${resAuth['FNULLTEXTSEARCH_RES'] }">
						&nbsp;&nbsp;<a id="searchSimilarRequest_btn" class="btn btn-default btn-xs"> <i class="glyphicon glyphicon-search"></i> <fmt:message key="label.fullsearch.searchRelatedItems" /></a>
					</c:if>
					 --%>
					</div>
				  	<%-- 常规动作end --%>
				  <div style="float:left;width:99.5%;margin-top: 7px;margin-bottom: 5px; margin-left: 8px;border-bottom: 1px solid #EBEBEB;">
				  	<%-- 流程动作start --%>
				  	<fmt:message key="label.itsop.process.action" />：
				   	<%-- 描述不全退回 --%>
			   		 <c:if test="${requestDetailDTO.hang eq false and empty requestDetailDTO.responsesTime}">
			          	<c:if test="${resAuth['RequestNotComprehensiveNotSubmitted_Res'] }">
			            	<c:if test="${requestDetailDTO.statusCode ne 'request_notComprehensiveNotSubmitted' }">
			            		<a onclick="itsm.request.requestAction.openRequestActionWin('request_notComprehensiveNotSubmitted_win')"><i style="color:green" class="glyphicon glyphicon-stop"></i><fmt:message key="label.notComprehensiveNotSubmitted" /></a>
			          		</c:if>
			          	 </c:if>
			            
			            <%-- 描述不全修改重新提交 --%>
			            <c:if test="${requestDetailDTO.statusCode eq 'request_notComprehensiveNotSubmitted'}">
			            	<c:if test="${requestDetailDTO.createdByName eq fullName}">
			            		<a onclick="itsm.request.requestAction.openRequestActionWin('request_resubmit_win')"><i style="color:green" class="glyphicon glyphicon-stop"></i><fmt:message key="label.desc.complete.resubmit" /></a>
			            	</c:if>
			            </c:if>
			         </c:if>   
				         
				   		<!-- 请求备注 -->
			       	    <c:if test="${resAuth['RequestDealRemark_Res'] }">
							<a onclick="itsm.request.requestAction.openRequestActionWin('requestDealRemark_win')"><i style="color:green" class="glyphicon glyphicon-stop"></i><fmt:message key="label.request.processMark" /></a>
				       	</c:if>
				       	
						<c:if test="${requestDetailDTO.hang eq false and empty requestDetailDTO.closeTime}">
					   		<!-- 自助解决 -->
					   	    <c:if test="${resAuth['SELFHELPRESOLVE_RES'] }">
					       		<c:if test="${empty requestDetailDTO.closeTime and requestDetailDTO.statusCode ne 'request_notComprehensiveNotSubmitted' and requestDetailDTO.createdByName eq fullName}">
					       			<c:if test="${requestDetailDTO.approvalState ne '待审批' or requestDetailDTO.approvalState ne 'WaitApproval' or empty requestDetailDTO.approvalState}">
					        			<a onclick="itsm.request.requestAction.openRequestActionWin('self_help_resolve_win')"><i style="color:green" class="glyphicon glyphicon-stop"></i><fmt:message key="label.request.self.help.resolve" /></a>
				       	   			</c:if>
				       	   		</c:if>
				       	    </c:if>
		                    <!-- 重新匹配服务水平 -->
				       	    <c:if test="${empty requestDetailDTO.closeTime and requestDetailDTO.statusCode ne 'request_notComprehensiveNotSubmitted'}">
				           		<c:if test="${resAuth['RequestReFitSLA'] }">
				           			<a onclick="itsm.request.requestAction.open_refit_sla_window(),findSLAContract()"><i style="color:green" class="glyphicon glyphicon-stop"></i><fmt:message key="title.refitSLA.refitServiceLevel" /></a>
				           		</c:if>
				           	</c:if>
				       	    <!-- 挂起 -->
							<c:if test="${resAuth['REQUESTHANG_RES'] }">
								<c:if test="${requestDetailDTO.statusCode ne 'request_complete'}">
									<c:if test="${requestDetailDTO.statusCode ne 'request_notComprehensiveNotSubmitted'}">
										<c:if test="${requestDetailDTO.approvalState ne '待审批' or requestDetailDTO.approvalState ne 'WaitApproval' or empty requestDetailDTO.approvalState}">
											<a onclick="itsm.request.requestAction.openRequestActionWin('requestHang_win')"><i style="color:green" class="glyphicon glyphicon-stop"></i><fmt:message key="request.action.hang" /></a>
										</c:if>
									</c:if>
								</c:if>
							</c:if>
							
							
							<!-- 请求响应 -->
				           	<c:if test="${empty requestDetailDTO.responsesTime}">
				           		<c:if test="${resAuth['RequestDeal_Res'] and userId==requestDetailDTO.assigneeNo}">
				           			<c:if test="${requestDetailDTO.statusCode ne 'request_notComprehensiveNotSubmitted'}">
					           			<c:if test="${requestDetailDTO.approvalState ne '待审批' or requestDetailDTO.approvalState ne 'WaitApproval' or empty requestDetailDTO.approvalState}">
					                		<a onclick="itsm.request.requestAction.openRequestActionWin('requestDeal_win')"><i style="color:green" class="glyphicon glyphicon-stop"></i><fmt:message key="label.sla.updateBase.response" /></a>
					               		</c:if>
				               		</c:if>
				               	</c:if>	
				            </c:if>
							
							<!-- 请求指派 -->
				       	    <c:if test="${resAuth['RequestAssign_Res'] and requestDetailDTO.statusCode ne 'request_notComprehensiveNotSubmitted' }">
								<a onclick="itsm.request.requestAction.openRequestActionWin('requestAssign_win')"><i style="color:green" class="glyphicon glyphicon-stop"></i><fmt:message key="label.request.requestAssign" /></a>
					       	</c:if>
					       	
				       		<!-- 再指派-->
			               	<c:if test="${requestDetailDTO.statusCode ne 'request_notComprehensiveNotSubmitted' and resAuth['RequestAgainAssign_Res'] }">
				               	<c:if test="${not empty requestDetailDTO.assigneeGroupName or not empty requestDetailDTO.assigneeName or not empty requestDetailDTO.assigneeGroupName }">
				               		<c:if test="${empty requestDetailDTO.requestResolvedTime}">
				               			<c:if test="${requestDetailDTO.assigneeLoginName eq loginUserName or requestDetailDTO.agentActionShow}">
				               			<a onclick="itsm.request.requestAction.openRequestActionWin('requestAgainAssign_win')"><i style="color:green" class="glyphicon glyphicon-stop"></i><fmt:message key="label.request.requestAssignAgain" /></a>
				               			</c:if>
				               		</c:if>	
				               	</c:if>
			               	</c:if>
			               	
			               	<!-- 提取 -->
							<c:if test="${resAuth['RequestGet_Res'] and requestDetailDTO.statusCode ne 'request_notComprehensiveNotSubmitted' }">
								<c:if test="${empty requestDetailDTO.assigneeName and requestDetailDTO.statusCode ne 'request_complete'}">
							  		<c:if test="${requestDetailDTO.approvalState ne '待审批' or requestDetailDTO.approvalState ne 'WaitApproval' or empty requestDetailDTO.approvalState}">
							  			<c:if test="${requestDetailDTO.statusCode ne 'request_approval'}">
											<a onclick="itsm.request.requestAction.openRequestActionWin('requestGet_win')"><i style="color:green" class="glyphicon glyphicon-stop"></i><fmt:message key="label.request.requestExtraction" /></a>
										</c:if>
									</c:if>
								</c:if>
							</c:if>
							
							
				       	    
			       	    </c:if>
			       	    <c:if test="${requestDetailDTO.hang eq true }">
							<!-- 解除挂起 -->
							<c:if test="${resAuth['REQUESTHANGREMOVE_RES'] }">
								<a onclick="itsm.request.requestAction.openRequestActionWin('requestHangRemove_win')"><i style="color:green" class="glyphicon glyphicon-stop"></i><fmt:message key="request.action.hangRemove" /></a>
							</c:if>
							
							
						</c:if>
				   		
			            <!-- 回访(默认在请求关闭后回访) -->
						<c:if test="${resAuth['RequestVisit_Res'] and not empty requestDetailDTO.closeTime }">
				       			<s:if test="requestDetailDTO.visitRecord==null"><!-- 邮件回访 -->
				           			<a onclick="itsm.request.requestAction.requestVisit_win('requestVisit')"><i style="color:green" class="glyphicon glyphicon-stop"></i><fmt:message key="label.request.requestVisit" /></a>
				           		</s:if>
				           		<s:else><!-- 重新回访 -->
				           		<sec:authorize url="request_action_anewRevisit">
				           			<span id="requestDetails_revisit"><a onclick="itsm.request.requestAction.requestVisit_win('requestVisit_re')"><i style="color:green" class="glyphicon glyphicon-stop"></i><fmt:message key="title.request.revisit" /></a></span>
				           		</sec:authorize>
				           		</s:else>
			       		</c:if>
		     			<c:if test="${requestDetailDTO.assigneeNo eq sessionScope.userId}">
							<c:set var="request_startFlow" value="true"/>
						</c:if>
						<c:forEach items="${sessionScope.technologyGroupNo}" var="aobj">  
		                   <c:if test="${requestDetailDTO.assigneeGroupNo eq aobj}">
							<c:set var="request_startFlow" value="true"/>
						   </c:if> 
		                </c:forEach>  
		                <c:if test="${requestDetailDTO.assigneeGroupNo eq sessionScope.orgNo}">
							<c:set var="request_startFlow" value="true"/>
						</c:if>
			       		<%--开启流程 --%>
			       		<c:if test="${resAuth['REQUEST_OPEN_PROCESS']  and requestDetailDTO.statusCode ne 'request_notComprehensiveNotSubmitted' and  requestDetailDTO.statusCode ne 'request_close'}">
			       			<c:if test="${requestDetailDTO.flowStart eq false}">
				       		<c:if test="${request_startFlow}">
				          		<a onclick="javascript:wstuo.jbpmMge.processCommon.openProcess('request','requestDetails_requestNo')" ><i style="color:blue" class="glyphicon glyphicon-stop"></i><fmt:message key="label.requestDetail.openProcess" /></a>
					   		</c:if>
					   		</c:if>
				   		</c:if>
						<%-- 流程处理动作start --%>
						<c:if test="${requestDetailDTO.hang eq false and empty requestDetailDTO.closeTime}">
						<s:iterator value="processDetailDTO" id="pd">
							<s:iterator value="#pd.flowTaskDTO" id="flowTask">
								<s:iterator value="#flowTask.participationDTO" id="participations">
									<c:if test="${participations.groupId eq sessionScope.groupId}">
										<c:set var="request_assignMyGroup" value="true"/>
									</c:if>
									<c:set var="sessionScopeUserId">${sessionScope.userId}</c:set>
									<c:if test="${participations.userId eq sessionScopeUserId}">
										<c:set var="request_assignMyGroup" value="true"/>
									</c:if>
									<c:forEach items="${sessionScope.technologyGroup}" var="aobj">  
		                                <c:if test="${participations.groupId eq aobj}">
											<c:set var="request_assignMyGroup" value="true"/>
										</c:if> 
		                            </c:forEach>
									
								</s:iterator>
								<%-- 任务提取(指派给我组并且任务指派为空的)--%>
								<c:if test="${empty flowTask.assignee and request_assignMyGroup}">
									<a href="javascript:wstuo.jbpmMge.processCommon.openTaskWindow('request','TaskTake','${requestDetailDTO.eno}','${requestDetailDTO.pid}','${flowTask.taskId}','${flowTask.flowActivityDTO.id}')"><i style="color:blue" class="glyphicon glyphicon-stop"></i><fmt:message key="title.bpm.task.take" /></a>
								</c:if>
								
								
								<%-- 如果任务已指派,则只能由指派的技术员可以查看 --%>
								<c:if test="${not empty flowTask.assignee}">
									
									<!-- 任务重新指派 start -->
									<sec:authorize url="REQUEST_AGAINASSIGNTASK">
										<a href="javascript:wstuo.jbpmMge.processCommon.openTaskWindow('request','TaskReAssigne','${requestDetailDTO.eno}','${requestDetailDTO.pid}','${flowTask.taskId}','${flowTask.flowActivityDTO.id}')"><i style="color:blue" class="glyphicon glyphicon-stop"></i><fmt:message key="title.bpm.task.re.assign" /></a>
									</sec:authorize>
									<%-- <c:if test="${resAuth['RequestAgainAssign_Res'] }">
										<c:if test="${fn:toUpperCase(loginUserName) eq fn:toUpperCase(flowTask.assignee) or requestDetailDTO.agentActionShow}">
										</c:if>
									</c:if> --%>
									<!-- 任务重新指派 end -->
									<c:if test="${fn:toUpperCase(loginUserName) eq fn:toUpperCase(flowTask.assignee) or requestDetailDTO.agentActionShow}">
										
										<%-- 业务动作start --%>
										<c:if test="${not empty flowTask.flowActivityDTO}">
											<s:iterator value="#flowTask.flowActivityDTO.taskActions" id="taskAction">
												
												<!-- 请求升级 -->
									            <c:if test="${empty requestDetailDTO.requestResolvedTime}">
									            	<c:if test="${requestDetailDTO.approvalState ne '待审批' or requestDetailDTO.approvalState ne 'WaitApproval' or empty requestDetailDTO.approvalState}">
										            	<c:if test="${taskAction eq 'requestUpgradeApply' }">
											            	<c:if test="${resAuth['RequestUpgradeApply_Res'] }">
											            		<a onclick="itsm.request.requestAction.openRequestActionWin('requestUpgradeApply_win')"><i style="color:green" class="glyphicon glyphicon-stop"></i><fmt:message key="label.request.upgradeApplication" /></a>
											            	</c:if>
										            	</c:if>
										            	<c:if test="${taskAction eq 'requestUpgrade' }">
											            	<c:if test="${resAuth['RequestUpgrade_Res'] }">	
											           	 		<a onclick="itsm.request.requestAction.openRequestActionWin('requestUpgrade_win')"><i style="color:green" class="glyphicon glyphicon-stop"></i><fmt:message key="label.request.requestUpdate" /></a>
											           	 	</c:if>
										           	 	</c:if>
										        <!-- 审批加签 -->
										       <sec:authorize url="APPOR_USER">
										       		<c:if test="${taskAction eq 'requestApporUser' }">
									           	 		<a onclick="itsm.request.requestAction.openRequestActionWin('request_openGetUser')"><i style="color:green" class="glyphicon glyphicon-stop"></i><fmt:message key="lable.request.appor.user" /></a>
									           	 	</c:if>
										       </sec:authorize>
										           	 	
									           	 	</c:if>
									           	</c:if>
									           	
									           	<!-- 回访 -->
												<c:if test="${resAuth['RequestVisit_Res'] and taskAction eq 'requestVisit' }">
										       			<s:if test="requestDetailDTO.visitRecord==null"><!-- 邮件回访 -->
										           			<a onclick="itsm.request.requestAction.requestVisit_win('requestVisit')"><i style="color:green" class="glyphicon glyphicon-stop"></i><fmt:message key="label.request.requestVisit" /></a>
										           		</s:if>
										           		<s:else><!-- 重新回访 -->
										           			<span id="requestDetails_revisit"><a onclick="itsm.request.requestAction.requestVisit_win('requestVisit_re')"><i style="color:green" class="glyphicon glyphicon-stop"></i><fmt:message key="title.request.revisit" /></a></span>
										           		</s:else>
									       		</c:if>
											</s:iterator>
										</c:if>
										<%-- 业务动作end --%>
										
										
										<%-- 任务流程start --%>
										<s:iterator value="#flowTask.outcomes" id="oc">
											<a href="javascript:wstuo.jbpmMge.processCommon.openFlowHandleWin('request','${requestDetailDTO.eno}','${requestDetailDTO.pid}','${oc}','${flowTask.taskId}','${flowTask.taskName}','${flowTask.flowActivityDTO.id}')">
											<i style="color:blue" class="glyphicon glyphicon-stop"></i>${oc}</a>
										</s:iterator>
										<%-- 任务流程end --%>
										
									</c:if>
								</c:if>
								
								
							</s:iterator>
							
						</s:iterator>
						</c:if>
						<%-- 流程处理动作end --%>
		
				   	<%-- 请求流程重新开启--%>
			       	   <c:if test="${requestDetailDTO.statusCode eq 'request_close' }">
			       	   <c:if test="${resAuth['REQUESTPROCESSREOPEN_RES'] }">
			       	   <a onclick="itsm.request.requestAction.openRequestActionWin('request_reopen_win')"><i style="color:green" class="glyphicon glyphicon-stop"></i><fmt:message key="label.flow.re.open" /></a>
					   </c:if>
					   </c:if>
		   <%-- 流程动作end --%>
		   </div></div>
			   <div class="row">
				   <div class="box col-md-12" id="">
				   		 <div class="form-group col-md-6 ui-sortable-handle">
							<label class="form-control-label"><fmt:message key="common.id" />：</label>
							${requestDetailDTO.requestCode}
						</div>
						<div class="form-group col-md-6 ui-sortable-handle">
							<label class="form-control-label"><fmt:message key="label.ci.ciServiceDir" />：</label>
							${ requestDetailDTO.requestServiceDirName }
						</div>
				    	<div class="form-group col-md-6 ui-sortable-handle">
							<label class="form-control-label"><fmt:message key="label.belongs.client"/>：</label>
							${requestDetailDTO.companyName }
						</div>
						<div class="form-group col-md-6 ui-sortable-handle">
							<label class="form-control-label"><fmt:message key="label.request.requestUser"/>：</label>
							${requestDetailDTO.createdByName }
						</div>
						<div class="form-group col-md-6 ui-sortable-handle">
							<label class="form-control-label"><fmt:message key="setting.requestCategory"/>：</label>
							${requestDetailDTO.requestCategoryName }
						</div>
						<div class="form-group col-md-6 ui-sortable-handle">
							<label class="form-control-label"><fmt:message key="common.title"/>：</label>
							${requestDetailDTO.etitle }
						</div>
						<div class="form-group col-md-12 ui-sortable-handle">
							<label class="form-control-label"><fmt:message key="label.common.desc"/>：</label>${requestDetailDTO.edesc }
						</div>
						<%-- <div class="form-group col-md-6 ui-sortable-handle">
							<label class="form-control-label"><fmt:message key="label.request.requestUpdateTo" />：</label>
							${requestDetailDTO.ownerName}
						</div> --%>
						<div class="form-group col-md-6 ui-sortable-handle">
							<label class="form-control-label"><fmt:message key="label.request.assignToGroup" />：</label>
							${requestDetailDTO.assigneeGroupName}
						</div>
						<div class="form-group col-md-6 ui-sortable-handle">
							<label class="form-control-label"><fmt:message key="label.request.assignToTechnician" />：</label>
							${requestDetailDTO.assigneeName}
						</div>
						<div class="form-group col-md-12 ui-sortable-handle"><hr/></div>
					</div>
				</div>
				
				<div class="row" id="detail_formField_row">
				    <div class="box col-md-12" id="detail_formField">
				    	
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
<div class="row">
    <div class="box col-md-12">
    <div class="box-inner" >	
			<div class="box-content" >
         <ul class="nav nav-tabs" id="myTab">
             <li class="active"><a href="#detailInfo"><fmt:message key="common.detailInfo" /></a></li>
             <c:if test="${cimHave eq true}">
             <li><a href="#cim_div" onclick="itsm.request.requestDetail.cimtabClick()"><fmt:message key="label.request.withAssets" /></a></li></c:if>
             <li><a href="#common_attachment" onclick="itsm.request.requestDetail.attachmenttabClick()"><fmt:message key="common.attachment" /></a></li>
             <c:if test="${requestDetailDTO.flowStart eq true}">
	  		 <c:if test="${resAuth['VIEW_REQUEST_PROCESSHISTORYTASK_RES'] }">
		   		<!-- 流程历史任务  -->
	   		<li><a href="#processHistoryTask" onclick="itsm.request.requestDetail.processHistorytabClick()"><fmt:message key="label.bpm.processHistoryTask" /></a></li></c:if></c:if>
	   		<c:if test="${resAuth['VIEW_REQUEST_HISTORYRECORD_RES'] }">
	   		<li><a href="#requestHistory" onclick="itsm.request.requestDetail.requestHistorytabClick()"><fmt:message key="title.request.requestHistory" /></a></li></c:if>
	   		<c:if test="${resAuth['VIEW_REQUEST_SOLUTIONS_RES'] }">
			<script>request_detail_solutions_res = true;</script>
			<li><a href="#requestDetails_solutions" onclick="itsm.request.requestDetail.solutionstabClick()"><fmt:message key="title.request.solutions" /></a></li></c:if>
			
			<c:if test="${resAuth['VIEW_HISTORYEMAIL_RES'] }">
       		<li><a href="#emailHistory" onclick="itsm.request.requestDetail.emailtabClick()"><fmt:message key="title.request.emailHistory" /></a></li></c:if>
       		
       		<c:if test="${resAuth['VIEW_VISITRECORD_RES'] }">
           <s:if test="requestDetailDTO.visitRecord!=null">
        	<li><a href="#returnItem" onclick="itsm.request.requestDetail.visitRecordtabClick()"><fmt:message key="setting.title.returnItem" /></a></li></s:if></c:if>
        	
        	<c:if test="${resAuth['VIEW_RELATED_OTHER_REQUEST_RES'] }">
			<li><a href="#related_request" onclick="itsm.request.requestDetail.related_requesttabClick()"><fmt:message key="title.related.other.request" /></a></li></c:if>
          </ul>
	
	     <div id="requestDetailsTab" class="tab-content">
			<div class="tab-pane active" id="detailInfo">			
				<div class="hisdiv"><br/>
				<table style="width:100%" class="table table-bordered" cellspacing="1">
				<tr>
				<td style="width:15%;text-align:left" ><fmt:message key="common.state" /></td>
				<td style="width:35%;text-align:left" >${requestDetailDTO.statusName}</td>
				<td style="width:15%;text-align:left" ><fmt:message key="title.security.serviceLevel" /></td>
				<td style="width:35%;text-align:left" >
					<a id="requestSLADetail">${requestDetailDTO.slaLevelName}</a>
					<input type="hidden" id="requestDetailSlaRuleNo" value="${requestDetailDTO.slaRuleNo}"/>
				</td>
				</tr>
				<tr>
				<td style="width:15%;text-align:left" ><fmt:message key="label.request.slaReuqestTime" /></td>
				<td style="width:35%;text-align:left" >
				<fmt:formatDate type="date" value="${requestDetailDTO.maxResponsesTime}"  pattern="yyyy-MM-dd HH:mm:ss" />
				</td>
				<td style="width:15%;text-align:left" ><fmt:message key="label.request.slaCompleteTime" /></td>
				<td style="width:35%;text-align:left" >
				<fmt:formatDate type="date" value="${requestDetailDTO.maxCompletesTime}"  pattern="yyyy-MM-dd HH:mm:ss" />
				</td>
				</tr>
				<tr>
				<td style="width:15%;text-align:left" ><fmt:message key="label.request.slaExpired" /></td>
				<td style="width:35%;text-align:left;color: red;">
						<c:if test="${requestDetailDTO.slaExpired}">
							<fmt:message key="label.dynamicReports.yes" />
						</c:if>
						<c:if test="${!requestDetailDTO.slaExpired}">
							<fmt:message key="label.dynamicReports.no" />
						</c:if></td>
					<td style="width:15%;text-align:left"><fmt:message key="common.cerateTime" /></td>
					<td style="width:35%;text-align:left" ><fmt:formatDate type="date" value="${requestDetailDTO.createdOn}"  pattern="yyyy-MM-dd HH:mm:ss" /></td>
				</tr>
				<tr>
					<td style="width:15%;text-align:left"><fmt:message key="label.request.requestTime" /></td>
					<td style="width:35%;text-align:left" ><fmt:formatDate type="date" value="${requestDetailDTO.responsesTime}"  pattern="yyyy-MM-dd HH:mm:ss" /></td>
					<td style="width:15%;text-align:left"><fmt:message key="label.request.completeTime" /></td>
					<td style="width:35%;text-align:left" ><fmt:formatDate type="date" value="${requestDetailDTO.closeTime}"  pattern="yyyy-MM-dd HH:mm:ss" /></td>
				</tr>
				</table>	
			</div>
			<%-- <!-- 请求人 -->
			<div class="hisdiv">
			<div id="serviceNosFRMs">
			<form id="serviceNosFRM">
						<c:forEach items="${requestDetailDTO.serviceDirectory}" var="serviceDIRe">
							<input type="hidden" id="serviceDireId" value="${serviceDIRe.eventId}" name="knowledgeQueryDto.knowledgeServiceNo">
						</c:forEach>
						<!-- <input type="hidden" value="" name="knowledgeQueryDto.title" id="knowledgeTitle"> -->
			</form> 
			</div>--%>
					<%-- <table style="width:100%" class="histable" cellspacing="1">
						
						<!-- 关联配置项 -->
						<thead>
							<tr>
								<th style="text-align:left" colspan="2"><fmt:message key="title.request.withCI" /></th>
							</tr>
						</thead>
						<tr>
							<td style="width:30%;text-align:left" ><fmt:message key="title.request.withCI" /></td>
							<td style="width:70%;text-align:left">
							<c:if test="${requestDetailDTO.ciName ne ''}">
							<a href="javascript:itsm.request.requestDetail.configureItemInfo(${requestDetailDTO.ciId})">${requestDetailDTO.ciName}</a>
							</c:if>
							<c:if test="${requestDetailDTO.ciName eq null || requestDetailDTO.ciName eq '' }">
								<span style="color:red"><fmt:message key="request.not.related.ci" /></span>
							</c:if>
							</td>
						</tr>
						<!-- 关联服务-->
						<thead>
							<tr>
								<th style="text-align:left" colspan="2"><fmt:message key="label.ci.ciServiceDir" /></th>
							</tr>
						</thead>
						<tr>
							<td style="width:30%;text-align:left" ><fmt:message key="label.ci.ciServiceDir" /></td>
							<td style="width:70%;text-align:left" id="serviceDirId">
							<c:if test="${requestDetailDTO.serviceDirectory[0].eventId ne ''}">
							<c:forEach items="${requestDetailDTO.serviceDirectory }" var="servicesDir"  >${servicesDir.eventName }，</c:forEach>
							</c:if>
							<c:if test="${requestDetailDTO.serviceDirectory[0].eventId eq 0 || requestDetailDTO.serviceDirectory[0].eventId eq '' }">
								<span style="color:red"><fmt:message key="request.not.related.serviceDir" /></span>
							</c:if>
							</td>
						</tr>
						<!-- 指派 -->
					
             			</table> 
               	</div>--%>
			</div>
			<c:if test="${cimHave eq true}">
				<div class="tab-pane" id="cim_div">	
					<div class="hisdiv">
						<table width="100%" cellspacing="1" class="histable">
							<c:if test="${not empty requestDetailDTO.cigDTO}">
							<thead>
							
							<tr>
								<th><fmt:message key="lable.ci.assetNo" /></th>
								<th><fmt:message key="label.name"/> </th>
								<th><fmt:message key="common.category" /></th>
								<th><fmt:message key="common.state" /></th>
								<th><fmt:message key="ci.operateItems" /></th>
							</tr>
							</thead>
							<tbody>
							
							<c:forEach items="${requestDetailDTO.cigDTO}" var="ci">
							<tr>
								<td>${ci.cino}</td>
								<td><a href="javascript:basics.tab.tabUtils.reOpenTab('ci!findByciId.action?ciEditId=${ci.ciId}','<fmt:message key="ci.configureItemInfo" />')">${ci.ciname}</a></td>
								<td>${ci.categoryName}</td>
								<td>${ci.status}</td>
								<td><a onclick="itsm.cim.configureItemUtil.ciRelevanceTree(${ci.ciId},'requestDetails_ci_relevance_win','requestDetails_ci_relevance_tree')">[<fmt:message key="ci.ciRelevanceTreeView" />]</a></td>
							</tr>
							
							</c:forEach>
							</tbody>
							</c:if>
							
							<c:if test="${empty requestDetailDTO.cigDTO}">
								<tr><td style="color:#FF0000;font-size:16px"><fmt:message key="commom.noData" /></td></tr>
							</c:if>
							
						</table>
					</div>
				</div>
				</c:if>
			
			<%-- 附件 --%>
			<div class="tab-pane" id="common_attachment">
					<div class="row">
    					<div class="box col-md-12">
						<input type="hidden" name="problemDTO.effectAttachmentStr" id="request_effect_attachmentStr"/>
						<table width="100%" cellspacing="1" class="lineTable">
							<tr>
								<td>
								<div id="request_effect_success_attachment" style="line-height:25px;color:#555;display: none"></div>
								<div class="hisdiv" id="show_request_effectAttachment">
									<table style="width:100%" class="histable" cellspacing="1">
										<thead>
											<tr>
												<th><fmt:message key="common.id" /></th>
												<th><fmt:message key="label.problem.attachmentName" /></th>
												<th><fmt:message key="label.sla.operation" /></th>
											</tr>
										</thead>
										<tbody></tbody>
				             			</table>
				               	</div>
								</td>
							</tr>
 							
							<%-- <tr>
								<td>
                                    <div class="diyLinkbutton">
                                        <div style="float: left;cursor: pointer;">
                                            <input type="file" name="filedata" id="request_effect_file">
                                        </div>
    									<div style="float: left;margin-left: 15px;">
    										<a class="btn btn-primary btn-sm" icon="icon-upload" href="javascript:itsm.request.requestDetail.request_uploadifyUpload()" ><fmt:message key="label.attachment.upload" /></a>
    						            </div>
    						            <div style="float:left;width: auto;margin-left: 15px;">
                                            <a href="javascript:common.config.attachment.chooseAttachment.showAttachmentGrid('requestDetail','edit')" class="btn btn-primary btn-sm" icon="icon-selectOldFile" id="chooseAtt"><fmt:message key="common.attachmentchoose" /></a>
                                        </div>
                                    </div>
                                    <div style="clear: both;"></div>   
					             	<div id="request_effect_fileQueue"></div>
								</td>
							</tr> --%>
						</table>
						<input type="hidden" name="problemDTO.effectAttachmentStr" id="request_effect_attachmentStr"/>
		                <div class="form-group"  >
		                    <input id="request_effect_file" type="file" name="filedata" multiple class="file" data-overwrite-initial="false" data-min-file-count="1">
		                </div>
						<div class="form-group">
                                <a href="javascript:wstuo.tools.chooseAttachment.showAttachmentGrid('requestDetail','edit')" class="btn btn-default btn-xs" icon="icon-selectOldFile" id="chooseAtt">
                                    <fmt:message key="common.attachmentchoose" />
                                </a>
                            </div>
					<%-- <form id="uploadForm_Request" method="post" enctype="multipart/form-data">
					  <table border="0" cellspacing="1" class="fu_list">
			              <tr height="40px">
				        	<td colspan="2">
				        	<table border="0" cellspacing="0"><tr><td >
					       			<a href="javascript:void(0);" class="files" id="idFile_Request"></a>
					       		</td><td>
							        <a class="btn btn-primary btn-sm" style="float:left" plain="true"   id="idBtnupload_Request"><fmt:message key="label.startUpload" /></a>
							         &nbsp;&nbsp;&nbsp;
									<a class="btn btn-default btn-xs" icon="icon-cancel" onclick="javascript:void(0);" id="idBtndel_Request"><fmt:message key="label.allcancel" /></a>
									 &nbsp;&nbsp;&nbsp;
									<a plain="true" href="javascript:common.config.attachment.chooseAttachment.showAttachmentGrid('requestDetail','edit')" class="btn btn-primary btn-sm"  id="chooseAtt"><fmt:message key="common.attachmentchoose" /></a>
								</td></tr></table>
							</td>
				      		</tr>
				          <tbody id="idFileList_Request">
				          </tbody>
						</table>
					</form> --%>
				</div></div>
		   </div>
			<%-- </c:if> --%>
		   <!-- 操作历史 -->
		   
		   <c:if test="${requestDetailDTO.flowStart eq true}">
		   <c:if test="${resAuth['VIEW_REQUEST_PROCESSHISTORYTASK_RES'] }">
		   <!-- 流程历史任务 start -->
			<div class="tab-pane" id="processHistoryTask">
				<div class="row">
    				<div class="box col-md-12">
					<table id="requestProcessHistoryTask" class="table table-bordered" cellspacing="1">
						<thead>
							<tr>
							<th style="width:25px">ID</th>
							<th style="width:10%"><fmt:message key="title.request.assign" /></th>
							<th style="width:10%"><fmt:message key="common.cerateTime" /></th>
							<th style="width:10%"><fmt:message key="title.bpm.duration" />(m)</th>
							<th style="width:10%"><fmt:message key="notice.currentActionName" /></th>
							<th style="width:10%"><fmt:message key="label.sla.slaEndTime" /></th>
							<th style="width:20%"><fmt:message key="label.bpm.outcome" /></th>
							<th style="width:15%"><fmt:message key="common.state" /></th>
							</tr>
						</thead>
						<tbody></tbody>
					</table></div>
				</div>
        	</div>
			<!-- 流程历史任务 end -->
			</c:if>
			</c:if>
		   <c:if test="${resAuth['VIEW_REQUEST_HISTORYRECORD_RES'] }">
           <div class="tab-pane" id="requestHistory" >
				<div class="row">
    				<div class="box col-md-12">
					<table id="requestHistoryRecord" class="table table-bordered" style="width:100%;" cellspacing="1">
						<thead>
							<tr >
							<th style="width:10%;text-align: center;"><fmt:message key="label.steps" /></th>
							<th style="width:20%;text-align: center;"><fmt:message key="label.request.requestAction" /></th>
							<th style="width:40%;text-align: center;"><fmt:message key="label.details" /></th>
							<th style="width:10%;text-align: center;"><fmt:message key="label.operator" /></th>
							<th style="width:20%;text-align: center;"><fmt:message key="label.operateTime" /></th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div></div>
        	</div>
			</c:if>
			<%-- 请求任务 
			<c:if test="${resAuth['VIEW_REQUEST_TASK_RES'] }">
			<div id="requestEventTaskGrid_div" title='<fmt:message key="title.request.task" />' style="padding:3px">
				<table id="requestEventTaskGrid"></table>
	    		<div id="requestEventTaskGridPager"></div>
			</div>
			</c:if>
			<%-- 进展及成本
			<c:if test="${resAuth['VIEW_REQUEST_COST_RES'] }">
			<div title="<fmt:message key="title.request.timeDetail" />" style="padding:3px;width:100%">
				<table id="requestEventCostGrid"></table>
				<div id="requestEventCostGridPager"></div>
        	</div>
			</c:if>
			--%>
			<%-- 解决方案 --%>
			<c:if test="${resAuth['VIEW_REQUEST_SOLUTIONS_RES'] }">
			<script>request_detail_solutions_res = true;</script>
			<div class="tab-pane" id="requestDetails_solutions">
			    <!-- 解决方案 start -->
				<div class="row">
    				<div class="box col-md-12">
						<div id="requestSolutionsDiv" align="left" >
						<form>
						<input type="hidden" name="requestDTO.attachmentStr" id="request_solutions_effect_attachments_str" />
						<input type="hidden" name="requestDTO.knowledgeNo" id="knowledgeNo" value="0"/>
						<table style="width:100%" class="table table-bordered" cellspacing="1">
							<%-- <tr>
							<td style="width: 20%;"><fmt:message key="label.dc.offmode" /></td>
							<td style="width:80%">
							<select style="width:80%;" id="requestDetails_offmode" name="requestDTO.offmodeNo" class="input"></select>
							</td></tr> --%>
							<tr>
								<td style="width: 15%;padding-top: 100px;"><fmt:message key="label.problem.solutionDesc" /></td>
								<td style="width:85%">
								<textarea style="width:90%; height:150px;" name="requestDTO.solutions" id="request_detail_solutions" class="easyui-validatebox" required="true">${requestDetailDTO.solutions}</textarea>
								<input type="hidden" name="requestDTO.eno" value="${param.eno}" />
								</td>
							</tr>
							<tr>
								<td style="padding-top: 10px;"><fmt:message key="common.attachment" /></td>
								<td class="diyLinkbutton" style="width:85%">
									<c:if test="${requestDetailDTO.statusCode ne 'request_close'}">
									<c:if test="${resAuth['ADD_REQUEST_SOLUTIONS_RES'] }">
									<div style="color:#ff0000;line-height:28px;width:95%"><fmt:message key="msg.attachment.maxsize" /></div>
									</c:if></c:if>
									
									<c:if test="${requestDetailDTO.statusCode ne 'request_close'}">
									<c:if test="${resAuth['ADD_REQUEST_SOLUTIONS_RES'] }">
									    <div style="float: left;cursor: pointer;width:98%">
	                                        <input type="file"  name="filedata" id="request_solutions_effect_file">
	                                    </div>
									</c:if>
									</c:if>
									<div id="request_solutions_effect_attachments_uploaded" style="clear: both;"></div>
								</td>
							</tr> 
						</table>
						</form>
						<%-- <c:if test="${requestDetailDTO.statusCode ne 'request_close'}">
								<c:if test="${resAuth['ADD_REQUEST_SOLUTIONS_RES'] }">
						<form id="uploadForm_RequestSolutions" method="post" enctype="multipart/form-data">
					  <table border="0" cellspacing="1" class="fu_list">
					  <tr> <td colspan="2"><fmt:message key="common.attachment" /></td></tr>
			              <tr height="40px">
				        	<td colspan="2">
				        	<table border="0" cellspacing="0">
				        	<tr><td colspan="2"><div id="request_solutions_effect_attachments_uploaded"></div></td></tr>
				        	<tr><td >
					       			<a href="javascript:void(0);" class="files" id="idFile_RequestSolutions"></a>
					       		</td><td>
							        <a class="btn btn-primary btn-sm" style="float:left" plain="true"   id="idBtnupload_RequestSolutions"><fmt:message key="label.startUpload" /></a>
							         &nbsp;&nbsp;&nbsp;
									<a class="btn btn-default btn-xs" icon="icon-cancel" onclick="javascript:void(0);" id="idBtndel_RequestSolutions"><fmt:message key="label.allcancel" /></a>
									 &nbsp;&nbsp;&nbsp;
									<a plain="true" href="javascript:common.config.attachment.chooseAttachment.showAttachmentGrid('request','edit')" class="btn btn-primary btn-sm"  id="chooseAtt"><fmt:message key="common.attachmentchoose" /></a>
								</td></tr></table>
							</td>
				      		</tr>
				          <tbody id="idFileList_RequestSolutions">
				          </tbody>
						</table>
					</form></c:if></c:if> --%>
						</div>
					</div>
					<c:if test="${requestDetailDTO.statusCode ne 'request_close' and (!(empty requestDetailDTO.assigneeName) || flowTask.assignee eq loginUserName)}">
					<c:if test="${resAuth['ADD_REQUEST_SOLUTIONS_RES'] }">
						<div style="padding: 10px; text-align: center; margin-bottom: 18px; margin-top: 8px">
							<a class="btn btn-primary btn-sm" id="saveSolutionsBtn"  style="margin-right: 15px"><fmt:message key="common.save" /></a> 
	<%-- 						<c:if test="${resAuth['KNOWLEDGEINFO_ADDKNOWLEDGE'] }"> --%>
							<a class="btn btn-primary btn-sm" id="saveToKnowledgeBtn" ><fmt:message key="label.request.saveAndAddToKnowledge" /></a>
	<%-- 						</c:if> --%>
	                        <!-- 选择已有解决方案 begin -->
	                        <%-- <c:if test="${resAuth['SELECT_REQUEST_SOLUTIONS_RES'] }"></c:if> --%>
	                        <%-- <sec:authorize url="SELECT_REQUEST_SOLUTIONS_RES"> --%>
							<a class="btn btn-primary btn-sm" id="selectExistSolutionsBtn" style="margin-right: 15px"><fmt:message key="button.selectExistSolution" /></a>
	                        <%-- </sec:authorize> --%>
	                        <!-- 选择已有解决方案 end -->
						</div>
					</c:if>
					</c:if>
					<div style="width: 10;height: 10px; "></div>
					<div id="showSolutions" class="hisdiv" style="display: none;">
						<table id="showSolutions_tb" class="histable" style="width:100%" cellspacing="1">
							<thead>
							<tr>
								<th><fmt:message key="title.request.solutions" /></th>
								<th><fmt:message key="label.service.scores" /></th>
								<th><fmt:message key="label.sla.operation" /></th>
							</tr>
							</thead>
							<tbody id="solution_tbody">
							</tbody>
						</table>
					</div>
				</div>
				<!-- 解决方案 end  -->
			</div>
			</c:if>
			
			<%-- 历史邮件 --%>
			<c:if test="${resAuth['VIEW_HISTORYEMAIL_RES'] }">
       		<div id="emailHistory" class="tab-pane">         
                	<div class="row">
    				<div class="box col-md-12">
    				<div class="box-content ">
	    				<table id="emailHistoryGrid"></table>
						<div id="emailHistoryPager" ></div>
					</div>
					</div>
					</div>
           </div>
           </c:if>
           <%-- 回访记录  --%>
           <c:if test="${resAuth['VIEW_VISITRECORD_RES'] }">
           <s:if test="requestDetailDTO.visitRecord!=null">
        	<div id="returnItem"  class="tab-pane">
	        	<div class="row">
    				<div class="box col-md-12">
    					<div class="box-content">
	    					<table id="returnItemRequestGrid"></table>
							<div id="returnItemRequestPager"></div>
						</div>
					</div>
				</div>
        	</div>
        	</s:if>
        	</c:if>
        	<%-- 关联的其他请求  --%>
        	<c:if test="${resAuth['VIEW_RELATED_OTHER_REQUEST_RES'] }">
			<div id="related_request" class="tab-pane">
				<div class="row">
    				<div class="box col-md-12"><div class="box-content ">
    				<table id="relatedOtherRequestGrid"></table>
				<div id="relatedOtherRequestPager"></div></div></div></div>
        	</div>
			</c:if>
			<%-- 相关联的问题  
			<c:if test="${resAuth['VIEW_RELATED_PROBLEM_RES'] }">
			<c:if test="${problemHave eq true}">
			<div title="<fmt:message key="title.problem.historyProblem" />" style="padding:3px">
				<table id="requestRelatedProblemGrid"></table>
				<div id="requestRelatedProblemPager"></div>
        	</div>
        	</c:if>
			</c:if>
			<%-- 相关联的变更  
			<c:if test="${resAuth['VIEW_RELATED_CHANGE_RES'] }">
			<c:if test="${changeHave eq true}">
			<div title="<fmt:message key="title.change.historyChange" />" style="padding:3px">
				<table id="requestRelatedChangeGrid"></table>
				<div id="requestRelatedChangePager"></div>
        	</div>
        	</c:if>
			</c:if>--%>
			<%-- 相关联的配置项  --%>
			<%-- <c:if test="${resAuth['VIEW_RELATED_CI_RES'] }">
			<c:if test="${cimHave eq true}">
			<div title="<fmt:message key="label.request.relat.ci" />" style="padding:3px">
				<table id="requestUserRelatedCiGrid"></table>
				<div id="requestUserRelatedCiPager"></div>
        	</div>
        	</c:if>
			</c:if> --%>
    	</div>
    	</div>
    	</div>
		<!-- 扩展信息 end -->
		</div></div>
<!-- 新的面板 end-->
<!-- 选择配置项 -->
<div id="requestDetail_ci_select_win" class="WSTUO-dialog" title="<fmt:message key="title.request.chooseCI" />" style="width:650px;height:350px;padding:5px;">
    	<div class="easyui-layout"  style="width:630px;height:300px;">
    		<div region="west" split="true" title="<fmt:message key="title.request.CICategory" />" style="width:180px;padding:10px;">
    			<div id="requestDetail_ci_select_tree"></div>
    		</div>
    		
    		<div region="center" title="<fmt:message key="title.request.CI" />" style="width:400px;padding:4px;">
    			<table id="requestDetail_ci_select_grid"></table>
    			<div id="requestDetail_ci_select_pager"></div>
    		</div>
    	</div>
</div>
<%-- <div id="requestDetail_ci_select_Toolbar" style="display:none">
	<form>
	<input id="requestDetail_ci_select_toolbar_search" name="ciQueryDTO.ciname" />
	<a class="btn btn-default btn-xs" icon="icon-search" onclick="selectQuery('requestDetail_ci_select_Toolbar','requestDetail_ci_select_grid')"><fmt:message key="common.search" /></a>
	</form>
</div> --%>



<!-- 公共值 -->
<div id="requestActionComm">
	<form>
		<input type="hidden" name="requestDTO.eno" value="${requestDetailDTO.eno }" />
		<input type="hidden" name="historyRecordDto.operator" id="attachmentCreator" value="${loginUserName}" />
		<input type="hidden" name="requestDTO.currentUser" value="${loginUserName}" />
		<input type="hidden" name="requestDTO.pid" value="${requestDetailDTO.pid}" />
		<input type="hidden" id="requestCode" name="requestDTO.requestCode" value="${requestDetailDTO.requestCode}" />
		<input type="hidden" name="requestDTO.requestCode" id="requestDetail_requestCode" value="${requestDetailDTO.requestCode}" />
		<input type="hidden" id="processNextStep" name="requestDTO.processNextStep" />
	</form>
</div>





<!-- 选择组 -->
<div id="requestAssginGroup_win" class="WSTUO-dialog" title="<fmt:message key="title.request.chooseGroup" />" style="max-height:380px;overflow:auto;width:240px;height:auto;padding:5px;">
<div id="assginGroupTree" ></div>
</div>

	<!-- 任务start -->	
	<div id="requestEventTaskActFormatterDiv" style="display: none"><a href="javascript:itsm.request.requestTask.editEventTask_affs('{taskId}')" title="<fmt:message key="common.edit"/>"><i class="glyphicon glyphicon-edit"></i></a>&nbsp;&nbsp;<a href="javascript:itsm.request.requestTask.deleteEventTask_aff('{taskId}')" title="<fmt:message key="common.delete"/>"><i class="glyphicon glyphicon-delete"></i> </a></div>
	<div id="requestEventTaskGridToolbar" style="display: none">
		<c:if test="${not empty requestDetailDTO.closeTime or (!resAuth['ADD_REQUEST_TASK_RES'] and !resAuth['EDIT_REQUEST_TASK_RES'] and !resAuth['DELETE_REQUEST_TASK_RES']) }">
				<script>request_task_tool_show_hide = false;</script>
		</c:if>	
		<c:if test="${resAuth['ADD_REQUEST_TASK_RES'] }">
			<a class="btn btn-default btn-xs" icon="icon-add" onClick="itsm.request.requestTask.addEventTask()" title="<fmt:message key="common.add"/>"></a>
		</c:if>
		<c:if test="${resAuth['EDIT_REQUEST_TASK_RES'] }">
			<a class="btn btn-default btn-xs" icon="icon-edit" onClick="itsm.request.requestTask.editEventTask()" title="<fmt:message key="common.edit"/>"></a>	
		</c:if>
		<c:if test="${resAuth['DELETE_REQUEST_TASK_RES'] }">
			<a class="btn btn-default btn-xs" icon="icon-cancel" onClick="itsm.request.requestTask.deleteEventTask()" title="<fmt:message key="common.delete"/>"></a>
		</c:if>	
	</div>
	
	<!-- 成本start -->	
	<div id="requestEventCostActFormatterDiv" style="display: none"><a href="javascript:itsm.request.requestEventCost.editEventCost_aff('{progressId}')" title="<fmt:message key="common.edit"/>"><i class="glyphicon glyphicon-edit"></i></a>&nbsp;&nbsp;<a href="javascript:itsm.request.requestEventCost.deleteEventCost_aff('{progressId}')" title="<fmt:message key="common.delete"/>"><i class="glyphicon glyphicon-delete"></i></a></div>
	<div id="requestEventCostGridToolbar" style="display: none">
		<c:if test="${not empty requestDetailDTO.closeTime or (!resAuth['ADD_REQUEST_COST_RES'] and !resAuth['EDIT_REQUEST_COST_RES'] and !resAuth['DELETE_REQUEST_COST_RES']) }">
				<script>request_cost_tool_show_hide = false;</script>
		</c:if>
		<c:if test="${resAuth['ADD_REQUEST_COST_RES'] }">
			<a class="btn btn-default btn-xs" icon="icon-add" onClick="itsm.request.requestEventCost.addEventCost()" title="<fmt:message key="common.add"/>"></a>
		</c:if>
		<c:if test="${resAuth['EDIT_REQUEST_COST_RES'] }">
			<a class="btn btn-default btn-xs" icon="icon-edit" onClick="itsm.request.requestEventCost.editEventCost()" title="<fmt:message key="common.edit"/>"></a>
		</c:if>
		<c:if test="${resAuth['DELETE_REQUEST_COST_RES'] }">
			<a class="btn btn-default btn-xs" icon="icon-cancel" onClick="itsm.request.requestEventCost.deleteEventCost()" title="<fmt:message key="common.delete"/>"></a>
		</c:if>
	</div>
	
	
	<!-- 任务选择 -->
	<div id="request_relatedEventTaskGrid_win" class="WSTUO-dialog" title="<fmt:message key="title.change.selectRelatedTask" />" style="width:520px;height:auto;padding: 2px;">
		<table id="request_relatedEventTaskGrid"></table>
   		<div id="request_relatedEventTaskGridPager"></div>
	</div>
	<!-- 邮件通知请求人 -->
<%-- 	<div id="emailNoticeInfoDiv" style="display: none;">
		<form id="emailNoticeInfoForm">
			<table cellspacing="0">
				<tr>
					<td>
						<input name="emailTemplatesDTO.templateVariableDTO.eventType" value="itsm.request">
						<input name="emailTemplatesDTO.templateVariableDTO.eno" value="${requestDetailDTO.eno}">
						<input name="emailTemplatesDTO.templateVariableDTO.ecode" value="${requestDetailDTO.requestCode}">
						<input name="emailTemplatesDTO.templateVariableDTO.etitle" value="${requestDetailDTO.etitle}">
						<input name="emailTemplatesDTO.templateVariableDTO.edesc" value="${requestDetailDTO.edesc}">
						<input name="emailTemplatesDTO.templateVariableDTO.solutions" value="${requestDetailDTO.solutions}">
						<input name="emailTemplatesDTO.emailAddress" id="emailAddress">
						<input name="emailTemplatesDTO.tempDir" id="requestAction_tempDir" value="requestActionEmailNotice.ftl">
						<input name="emailTemplatesDTO.emailTitleTemp"  id="requestAction_emailTitleTemp" value="requestActionEmailNotice_title.ftl">
						
						<input name="loginName" value="${requestDetailDTO.createdByName}">
					</td>
				</tr>
			</table>
		</form>
	</div> --%>

<!-- 邮件回复 -->
	<div id="rquestActionEmailReply"  class="WSTUO-dialog" title="<fmt:message key="tool.mail.addEmail"/>" style="width:650px;height:auto">
	<form id="rquestActionEmailReplyForm" event="itsm.request.requestDetail.rquestActionEmailReply">
		<input type="hidden"  name="emailDto.moduleCode"  value="${requestDetailDTO.requestCode}">
		<div class="lineTableBgDiv">
			<table style="width:100%" class="lineTable" cellspacing="1">
		    	<tr>
		            <td><fmt:message key="tool.mail.receiveAddress"/></td>
		            <td><input  id="requestEmailReplyUser" name="emailDto.receiveAddress" class="form-control"  required="true" style="width:96%;" />
		            	<span style="color:red;">&nbsp;&nbsp;<fmt:message key="label.email.address.format" /></span>
		            </td>
		        </tr>
		     	
		        <tr>
		            <td><fmt:message key="tool.mail.emailTitle"/></td>
		             <td><input id="requestEmailReplyTitle" name="emailDto.subject" class="form-control"  required="true"  style="width:96%" /></td>
		        </tr>
		        
		          <tr>
		            <td><fmt:message key="tool.mail.emailContent"/></td>
		             <td><textarea id="content" name="emailDto.content" style="width:96%;height:180px" class="form-control"  required="true"  ></textarea>
		           </td>
		        </tr>
		       
		          <tr style="display: none">
		            <td><fmt:message key="tool.mail.remark"/></td>
		            <td><input id="remarks" style="width:96%" class="form-control"/></td>
		        </tr>
		        
		         <tr style="display: none">
		            <td><fmt:message key="tool.mail.emialDescription"/></td>
		            <td><input id="description" name="emailDto.description" style="width:96%" class="form-control"/></td>
		        </tr>
		        <tr>
		            <td colspan="2" style="padding-top:8px;">
		                <button class="btn btn-primary btn-sm" style="margin-right: 20px;"><fmt:message key="tool.mail.confirmSend"/></button>
		        </td>
		        </tr>
		    </table>
	    </div>
	    </form>
	</div>
	<%--重新匹配SLA Start --%>
	<div id="refitSLA_window"  class="WSTUO-dialog" title='<fmt:message key="title.refitSLA.refitServiceLevel"/>' style="width:380px;height:auto">
		<form>	
			<table style="width:100%"  cellspacing="1">			
				<tr>
		            <td style="width: 30%;"><fmt:message key="title.refitSLA.fitType"/></td>
		            <td>
		            <select id="refit_fitType" name="fitType" onchange="showHideFitType(this.value)" class="form-control">
		            	<option value="0"><fmt:message key="title.refitSLA.manually"/></option>
		            	<option value="1"><fmt:message key="title.refitSLA.auto"/></option>
		            </select>
		            </td>
		        </tr>
		    	<tr class="fitbyselect">
		            <td style="width: 30%;"><br/><fmt:message key="title.refitSLA.sla"/></td>
		            <td><br/>
		            <select id="refit_slaContracts" onchange="findSLARuleByContract(this.value)" class="form-control">
		            	<option value="0">--<fmt:message key="common.pleaseSelect"/>--</option>
		            </select>
		            </td>
		        </tr>
		        
		        <tr class="fitbyselect">
		            <td style="width: 30%;"><br/><fmt:message key="title.sla.expecteCompleted"/></td>
		            <td><br/>
		            <select id="refit_slaRules" name="slaNo" class="form-control">
		            	<option value="0">--<fmt:message key="common.pleaseSelect"/>--</option>
		            </select>
		            </td>
		        </tr>
		        <tr>
		            <td colspan="2" style="padding-top:8px;">
		             <input type="hidden" name="historyRecordDto.logDetails" value='<fmt:message key="title.refitSLA.history.detail"/>'>
		             <input type="hidden" name="historyRecordDto.logTitle" value='<fmt:message key="title.refitSLA.history.title"/>'>
		             <input type="hidden" name="historyRecordDto.operator" value="${loginUserName}">
		            	 <input type="hidden" name="requestId" value="${requestDetailDTO.eno}">
		                 <input type="button" class="btn btn-primary btn-sm"  onclick="itsm.request.requestAction.save_refit_sla()" value="<fmt:message key="common.submit"/>" />
		             </td>
		        </tr>
		    </table>
		</form>
	</div>
	<%--重新匹配SLA End --%>

<!--SLA详细信息 -->
<div id="request_show_sla_window" class="WSTUO-dialog" title='<fmt:message key="label.request.slaDetail" />'>
		<div class="hisdiv">
			<table style="width: 100%" class="histable" cellspacing="1">
				<tr>
					<th style="text-align:left" colspan="2"><fmt:message key="label.request.slaContractInfo" /></th>
				</tr>
				<tr>
					<td width="50%">	
						<fmt:message key="title.refitSLA.sla" />
					</td>
					<td width="50%">
						<span id="requestSLAContractName"></span>
					</td>
				</tr>
				<tr>
					<td width="50%">	
						<fmt:message key="label.sla.serviceOrg" />
					</td>
					<td width="50%">
						<span id="requestSLAServiceName"></span>
					</td>
				</tr>
				<tr>
					<td width="50%">	
						<fmt:message key="label.sla.slaStartTime" />
					</td>
					<td width="50%">
						<span id="requestSLAStartTime"></span>
					</td>
				</tr>
				<tr>
					<td width="50%">	
						<fmt:message key="label.sla.slaEndTime" />
					</td>
					<td width="50%">
						<span id="requestSLAEndTime"></span>
					</td>
				</tr>
			</table>
		</div>
	<div style="height: 5px;"></div>
		<div class="hisdiv">
			<table style="width: 100%" class="histable" cellspacing="1">
				<tr>
					<th style="text-align:left" colspan="2"><fmt:message key="label.request.slaRuleInfo" /></th>
				</tr>
				<tr>
					<td width="50%">
						<fmt:message key="label.sla.ruleName" />
					</td>
					<td width="50%">
						<span id="requestSLAName"></span>
					</td>
				</tr>
				<tr>
					<td width="50%">	
						<fmt:message key="label.sla.requestTime" />
					</td>
					<td width="50%">
						<span id="requestSLAResponseTime"></span>
					</td>
				</tr>
				<tr>
					<td width="50%">	
						<fmt:message key="label.sla.completeTime" />
					</td>
					<td width="50%">
						<span id="requestSLACompleteTime"></span>
					</td>
				</tr>
				<tr>
					<td width="50%">	
						<fmt:message key="label.rule.salience" />
					</td>
					<td width="50%">
						<span id="requestSLAsalience"></span>
					</td>
				</tr>
			</table>
		</div>
</div>

<!-- 回访详细信息 -->
<div id="userReturnVisit_detail_win" class="WSTUO-dialog" title="<fmt:message key="common.detailInfo" />" style="width: 400px; height:auto;">
	<div class="lineTableBgDiv" >
   		<table style="width:100%" class="lineTable" cellspacing="1">
			<tr>
   				<td><fmt:message key="label.returnVisit.satisfaction" /></td>
   				<td><span id="userReturnVisit_detail_satisfaction"></span></td>
   			</tr>
   			<tr>
   				<td><fmt:message key="lable.ReturnVisitDetail" /></td>
   				<td><span id="userReturnVisit_detail_returnVisitDetail"></span></td>
   			</tr>
   			<tr>
   				<td><fmt:message key="label.returnVisit.state" /></td>
   				<td><span id="userReturnVisit_detail_state"></span></td>
   			</tr>

   			
   			<tr>
   				<td><fmt:message key="label.returnVisit.sendTime" /></td>
   				<td><span id="userReturnVisit_detail_returnVisitSubmitTime"></span></td>
   			</tr>
   			<tr>
   				<td><fmt:message key="label.returnVisit.replyTime" /></td>
   				<td><span id="userReturnVisit_detail_returnVisitTime"></span></td>
   			</tr>
   			
   			<tr>
   				<td><fmt:message key="label.returnVisit.object" /></td>
   				<td><span id="userReturnVisit_detail_returnVisitUser"></span></td>
   			</tr>
   			<tr>
   				<td><fmt:message key="label.request.assignToTechnician"/> </td>
   				<td><span id="userReturnVisit_detail_returnVisitAssignee"></span></td>
   			</tr>
   			<tr>
   				<td><fmt:message key="label.operator" /></td>
   				<td><span id="userReturnVisit_detail_returnVisitSubmitUser"></span></td>
   			</tr>
   		</table>
   </div>
</div>

		<!-- 关联配置项树 -->
	<div id="requestDetails_ci_relevance_win" title="<fmt:message key="ci.ciRelevanceTreeView" />" class="WSTUO-dialog" style="width:200px;height:auto">
		<div id="requestDetails_ci_relevance_tree"></div>
	</div>
	
	<!-- 知识库服务目录 -->
<%-- <div id="request_services_select_window" class="WSTUO-dialog" title="<fmt:message key="setting.serviceDirectory"/>" style="max-height:500px;overflow:auto;width:320px;padding:3px;min-height:300px;max-height:500">
	<div id="request_services_select_tree"></div>
	<a id="request_services" class="btn btn-primary btn-sm" icon="icon-save" style="margin-left: 20px"><fmt:message key="label.determine"/></a>
</div> --%>
	<!-- 选择已有解决方案 -->
    <%-- <sec:authorize url="KNOWLEGEINFO_VIEWKNOWLEGE"> --%>
    <div id="select_have_services" class="WSTUO-dialog" title="<fmt:message key="lable.solution.select" />" style="width:260px;padding:3px;">
        <div style="text-align: right;">
            <div id="request_Detail_button" class="ui-userdata ui-state-default" style="display: none; height: 15px; padding-top: 5px; padding-bottom: 10px; border-top: medium none; background: none repeat scroll 0% 0% rgb(238, 238, 238); text-align: left; overflow: auto;">
                <fmt:message key="lable.solution.name" />
                :&nbsp;<input type="text" id="service_value" onchange="javascript:itsm.request.requestDetail.select_Onchage_Set_Value(this.value)"></input> 
                <a class="btn btn-default btn-xs" href="javascript:itsm.request.requestDetail.select_knowledge_ByName()"> <i class="glyphicon glyphicon-search" ></i> <fmt:message key="common.search" /></a>
            </div>
            <table id="select_service_grid_show" style="margin-bottom: 10px;">
            </table>
            <div id="select_service_grid_page"></div>
        </div>
    </div>

    <%-- </sec:authorize> --%>
    <div id="processTracelnstanceWin"  class="WSTUO-dialog" title="流程跟踪" ></div>
    <!-- 选择已有解决方案 end -->
    
	<div id="requesterContactInfo_win"  class="WSTUO-dialog" title='<fmt:message key="title.request.requesterInfo"/>' >
		<div class="hisdiv">
			<table style="width:100%" class="histable" cellspacing="1">
				<tbody>
				
				<tr>
					<td style="width:30%;text-align:left"><fmt:message key="label.request.requestUser" /></td>
					<td style="width:70%;text-align:left">
					<input type="hidden" id="requestCreateFullName" value="${requestDetailDTO.fullName }">
					${requestDetailDTO.fullName }
					</td>
				</tr>
				<tr>
					<td style="text-align:left"><fmt:message key="label.request.phone" /></td>
					<td style="text-align:left">${requestDetailDTO.createdByPhone}
					<c:if test="${requestDetailDTO.createdByPhone ne null && requestDetailDTO.createdByPhone ne '' && requestDetailDTO.createByMoblie ne null && requestDetailDTO.createByMoblie ne '' }">/</c:if>
					<c:if test="${requestDetailDTO.createByMoblie ne null}">${requestDetailDTO.createByMoblie }</c:if>
					</td>
				</tr>
				<tr>
					<td style="width:30%;text-align:left"><fmt:message key="setting.lable.email" /></td>
					<td style="width:70%;text-align:left">
					<a href="mailto:${requestDetailDTO.createdByEmail}" target="_bank">${requestDetailDTO.createdByEmail }</a>
					</td>
				</tr>
				<tr>
					<td style="width:30%;text-align:left"><fmt:message key="label.user.workAddress" /></td>
					<td style="width:70%;text-align:left">
					${requestDetailDTO.createdByOfficeAddress}
					</td>
				</tr>
				<tr>
					<td style="width:30%;text-align:left"><fmt:message key="label.user.position" /></td>
					<td style="width:70%;text-align:left">
					${requestDetailDTO.createByPosition }
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	</div>
<script type="text/javascript">
	function findSLAContract(){
		$('#refit_slaRules').html('<option value="0">--'+i18n["pleaseSelect"]+'--</option>');
		$.post('slaContractManage!findAll.action',function(data){
			if(data!=null && data.length>0){
				$('#refit_slaContracts').html('<option value="0">--'+i18n["pleaseSelect"]+'--</option>');
				$.each(data,function(k,v){
					$('#refit_slaContracts').append('<option value='+v.contractNo+'>'+v.contractName+'</option>');
				});
			}
		});
	}
	function findSLARuleByContract(id){
		if(id==0){
			$('#refit_slaRules').html('<option value="0">--'+i18n["pleaseSelect"]+'--</option>');
		}else{
			$('#refit_slaRules').html('<option value="0">--'+i18n["pleaseSelect"]+'--</option>');
			$.post('slaRule!findAll.action?contractNo='+id,function(data){
				if(data!=null && data.length>0){
					$.each(data,function(k,v){
						$('#refit_slaRules').append('<option value='+v.ruleNo+'>'+v.ruleName+'</option>');
					});
				}
			});
		}
	}
	function showHideFitType(flag){
		if(flag=="0"){
			$('.fitbyselect').show();
		}else{
			$('.fitbyselect').hide();
		}
	}
</script>
<style>
a {;cursor: pointer;}
#act_div a {color: #000; text-decoration:none;cursor: pointer;} //未访问：蓝色、无下划线
#act_div a:active:{color: blue; } //激活：红色
#act_div a:visited {color:purple;text-decoration:none;} //已访问：purple、无下划线
#act_div a:hover {color: red; text-decoration:none;} //鼠标移近：红色、下划线 
</style>
