<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.opensymphony.xwork2.ActionContext" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>

<script src="../scripts/itsm/request/requestGroup.js?random=<%=new java.util.Date().getTime()%>"></script>

<script>
var sec_editRequest="0";
var sec_deleteRequest="0";
var userName="${loginUserName}";

</script>

<sec:authorize url="/pages/request!updateRequest.action">
<script>sec_editRequest="1";</script>
</sec:authorize>

<sec:authorize url="/pages/request!deleteRequests.action">
<script>sec_deleteRequest="1";</script>
</sec:authorize>

<script>
$(document).ready(function(){
	 $("#requestGroup_loading").hide();
	 $("#requestGroup_content").show();
}); 
</script>
<div style="padding:10px" id="requestGroup_loading"><img src="../images/icons/loading.gif" /></div>

<div style="padding:3px;display:none;" id="requestGroup_content">

   	<table id="requestGroupGrid"></table>
    <div id="requestGroupPager"></div>
    
    <div id="requestGroupToolbar" style="display: none">
    <sec:authorize url="/pages/request!saveRequest.action">
	<a class="easyui-linkbutton" plain="true" icon="icon-add" id="rg_addRequestBtn"><fmt:message key="common.add" /></a>
	</sec:authorize>
	<sec:authorize url="/pages/request!updateRequest.action">
	<a class="easyui-linkbutton" plain="true" icon="icon-edit" id="rg_editRequestBtn"><fmt:message key="common.edit" /></a> 
	</sec:authorize>
	<sec:authorize url="/pages/request!deleteRequests.action">
	<a class="easyui-linkbutton" plain="true" icon="icon-cancel" id="rg_deleteRequestBtn"><fmt:message key="common.delete" /></a> 
	</sec:authorize>
	<a class="easyui-linkbutton" plain="true" icon="icon-search"id="rg_searchRequestBtn"><fmt:message key="common.search" /></a>
	</div>
	
	<!-- 搜索请求 -->
	<div id="rg_searchRequestWindow" class="WSTUO-dialog" title="<fmt:message key="title.request.searchRequest" />"
		style="width: 410px; height: 200px; padding: 10px; line-height: 20px;">
		<form>
		<input name="requestQueryDTO.loginName" type="hidden" value="${loginUserName}" />
		<table width="98%" border="0" align="center" cellspacing="5">
			<tr>
				<td><fmt:message key="label.request.requestCategory" /></td>
				<td>
					<select id="rg_searchRequest_ecategoryNo" name="requestQueryDTO.ecategoryNo" style="width: 300px">
					</select>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="label.request.requestTitle" /></td>
				<td>
					<input name="requestQueryDTO.etitle" class="easyui-validatebox" style="width: 295px" />
				</td>
			</tr>
		
		
			<tr>
				<td><fmt:message key="label.request.requestState" /></td>
				<td>
					<select id="rg_searchRequest_statusNo" name="requestQueryDTO.statusNo" style="width: 300px">
					</select>
				</td>
			</tr>
		
			<tr>
				<td colspan="2">
					<a id="gp_doSearchRequestBtn" class="easyui-linkbutton" icon="icon-search" style="margin-top: 8px;"><fmt:message key="common.search" /></a>
				</td>
			</tr>
		</table>
		
		</form>
	
	</div>
</div>	







