<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<li class="nav-header">快捷菜单</li>
<li><a class="ajax-link" href="request/addRequest.jsp"><i class="glyphicon glyphicon-plus"></i><span> 添加请求</span></a></li>
<li><a class="ajax-link" href="wstuo/knowledge/addKnowledge.jsp"><i class="glyphicon glyphicon-plus"></i><span> 添加知识库</span></a></li>
<li class="nav-header">常用菜单</li>


<script type="text/javascript">
if(common_url!=''){
	var urls=common_url.split('@').sort();
	$.each(urls,function(i,obj){
		if(obj && i<10){
			var curl=obj.split('=');
			if($("#main-menu").find('.ajax-link[href="'+curl[0]+'"]').length==0)
				$("#main-menu").append('<li><a class="ajax-link" href="'+curl[0]+'"><span>'+curl[1]+'</span></a></li>');
		}
		
	});
}
</script>  