function calculateFunction(func) {
    if (typeof func === 'string') {
        // support obj.func1.func2
        var fs = func.split('.');
        if (fs.length > 1) {
            func = window;
            $.each(fs, function (i, f) {
                func = func[f];
            });
        } else {
            func = window[func];
        }
    }
    if (typeof func === 'function') {
        return func;
    }
};
function DateBox97(elements,format){
	if (format == null || format === '') {format = 'yyyy-MM-dd HH:mm:ss';}
	$(elements).click(function(){WdatePicker({dateFmt:format});});
};
function msgAlert(msg){
	$("#my-alert .am-modal-bd").text(msg);
	$("#my-alert").modal();
}
/**
 * 判断空值并返回.
 * @param value
 * @returns
 */
function $vl(vl) {
	if(vl==null || vl=='' || vl=='null' || vl=='undefined'){
		return '';
	}else{
		return vl;
	}
}

function msgConfirm(title,content,event,cancel){
	if(title==''){
		title = i18n['tip'];
	}
	$("#my-confirm .am-modal-bd").text(title);
	var $confirm = $('#your-confirm');
	var $confirmBtn = $confirm.find('[data-am-modal-confirm]');
	var $cancelBtn = $confirm.find('[data-am-modal-cancel]');
	$confirmBtn.off('click.confirm.modal.amui').on('click', function() {
		event();
	});
	$cancelBtn.off('click.cancel.modal.amui').on('click', function() {
		 if (typeof cancel === 'function') 
			 cancel();
	});
	$('#my-confirm').modal();
}
this.timeFormatter=function(cell,opt,data){
	if(cell!=null){
		return cell.replace("T"," ").replace('.0','');
	}else{
		return '';
	}
};
function startProcess(){
	$("#my-modal-loading").modal();
}
function endProcess(){
	$("#my-modal-loading").modal('close');
}
this.ltrim=function(str) {
	var pattern = new RegExp("^[\\s]+","gi");
	return str.replace(pattern,"");
};
//End

//String函数之RTrim(),用于清除字符串结尾的空格\换行符\回车等
this.rtrim=function(str) {
	var pattern = new RegExp("[\\s]+$","gi");
	return str.replace(pattern,"");
};
//End

//清除两边空格
this.trim=function(str) {
	return rtrim(ltrim(str));
};
this.initValidate=function(){
	$("form[event]").each(function(){
		var rules='{';
		$(this).find('[validType]').each(function(){
			var name=$(this).attr('name');
			var valid=$(this).attr('validType');
			if(valid.indexOf("[")>0){
				rules+="'"+name+"':{"+valid.substring(0,valid.indexOf("["))+":"+valid.substring(valid.indexOf("["))+"},";
			}
			else
				rules+="'"+name+"':{"+valid+":true},"
		});
		if(rules.length>1){
			rules=rules.substring(0, rules.length-1)+"}";
		}else
			rules+="}";
		$(this).submit(function(){return false});
		$(this).validate({
		    rules:eval('('+rules+')'),
			errorPlacement: function ( error, element ) {
				$(element).attr("title",error.text());
			},
			success: function ( label, element ) {
				$(element).attr("title",'');
			},
			submitHandler: function (e) {
				eval($(e).attr("event")+"()");
			}
		});
	});
};
this.initNavbar=function(){
	$(".am-navbar-more").click(function(){
		var index=$(".am-navbar-more").index($(this));
		if($(this).hasClass('am-active')){
			$('.am-navbar-actions').eq(index).removeClass('am-active');
			$(this).find('.am-icon-angle-down').attr('class','am-icon-angle-up');
			$(this).removeClass('am-active');
		}else{
			$('.am-navbar-actions').eq(index).addClass('am-active');
			$(this).find('.am-icon-angle-down').attr('class','am-icon-angle-down');
			$(this).addClass('am-active');
		}
	});
};


function checkPhone(value){
	var ck = /^1[3,5,8,9,7]\d{9}$/;
	return ck.test(value);
}