/**  
 * @author QXY  
 * @constructor requestAction
 * @description 请求动作主函数
 * @since version 1.0 
 */
requestAction=function(){
	return {
		/**
		 * @description 重新打开详细信息页面.
		 */
		reOpeRequestInfo:function(){
			location.reload();
		},
		/**
		 * @description 加载升级级别.
		 * @param select 升级级别下拉框元素
		 */
		loadUpdateLevel:function(select){
			$(select).html('');
			var url = 'updatelevel!findAllLevels.action';
			$.post(url,function(res){
				if(res.data!=null){
					if(res.data.length>0){
						for(var i=0;i<res.data.length;i++){
							$('<option value="'+res.data[i].approvalNo+'">'+res.data[i].ulName+'('+res.data[i].approvalName+')</option>').appendTo(select);
						}
					}
				}
			});
			
		},
		/**
		 * 请求动作是否邮件通知
		 * @param isEmailNoticeId 是否进行邮件通知
		 * @param requestRemarkId 请求备注元素
		 */
		requestActionIsEmailNotice:function(isEmailNoticeId,requestRemarkId){
			if($("#"+isEmailNoticeId).attr("checked")){
				$('#'+requestRemarkId).val($('#'+requestRemarkId).val()+'<br>['+i18n.is_request_action_notice+']');
			}
		},
		/**
		 * 开始请求处理
		 */
		requestDeal_opt:function(){
			if($("#startRequestDeal_isEmailNotice").attr("checked") && !checkEmail($('#requestDeal_noticeEmail_input').val())){//邮件验证是否正确
				msgAlert(i18n.emailFormatError,'info');
			}else{
				requestAction.requestActionIsEmailNotice('startRequestDeal_isEmailNotice','requestDeal_value');
				var frm = $('#requestDeal_win form,#requestActionComm form').serialize();
				//显示进程
				startProcess();
				$.post('request!requestAction.action',frm,function(){
					//隐藏进程
					endProcess();
					$('#requestDeal_win').modal('close');
					if($("#startRequestDeal_isEmailNotice").attr("checked")){
						requestAction.emailNotice('startRequestDeal_action',$('#requestDeal_noticeEmail_input').val());
					}
					requestAction.reOpeRequestInfo();
					msgAlert(i18n.msg_request_startSuccessful);
				});
			}
		},
		/**
		 * @description 打开请求动作窗口.
		 * @param id 动作标识
		 * @param processNextStep 流程下一步动作
		 */
		openRequestActionWin:function(id,processNextStep){
			if(id=='requestUpgrade_win')//请求升级加载升级级别
				itsm.request.requestAction.loadUpdateLevel('#requestUpgrade_level');
			if(id=="requestClose_win"){
				id="requestClose_win";
				var myDate = new Date();
				$('#requestCloseTime').val(myDate.getFullYear()+"-"+(myDate.getMonth()+1)+"-"+myDate.getDate()+" "+myDate.toLocaleTimeString());
			}
			if(id=='requestAssign_win'){//请求指派
				$('#requestDetail_assigneeGroupNo').val($('#requestDetails_assigneeGroupNo').val());
				$('#requestDetail_assigneeGroupName').val($('#requestDetails_assigneeGroupName').val());
				$('#requestDetail_assigneeNo').val($('#requestDetails_assigneeNo').val());
				$('#requestDetail_assigneeName').val($('#requestDetails_assigneeName').val());
			}
			if(id=='requestAgainAssign_win'){//再指派
				$('#requestDetail_assigneeNo_Again').val($('#requestDetails_assigneeNo').val());
				$('#requestDetail_assigneeName_Again').val($('#requestDetails_assigneeName').val());
			}
			$('#processNextStep').val(processNextStep);
			
			//清空一些数据
			$('#'+id+" textarea").val('');
			$('#'+id).modal();
		},
		/**
		 * 请求提取
		 */
		requestGet_opt:function(){
				if($("#requestGet_isEmailNotice").attr("checked") && !checkEmail($('#requestGet_noticeEmail_input').val())){//邮件验证是否正确
					msgAlert(i18n.emailFormatError,'info');
				}else{
					requestAction.requestActionIsEmailNotice('requestGet_isEmailNotice','requestGet_value');
					var frm = $('#requestGet_win form,#requestActionComm form').serialize();
					//显示进程
					startProcess();
					$.post('request!requestAction.action',frm,function(){
						//隐藏进程
						endProcess();
						$('#requestGet_win').modal('close');
						if($("#requestGet_isEmailNotice").attr("checked")){
							requestAction.emailNotice('requestExtraction_action',$('#requestGet_noticeEmail_input').val());
						}
						requestAction.reOpeRequestInfo();
						msgAlert(i18n.msg_request_getSuccessful);
				
					});
			}
			
		},
		/**
		 * @description 进行指派操作（请求指派）.
		 */
		requestAssign_opt_s:function(){
			var orgName = $('#requestDetail_assigneeGroupNo').val();
			var tecName = $('#requestDetail_assigneeNo').val();
			if(orgName===undefined){
				orgName='';
			}
			if(tecName===undefined){
				tecName='';
			}
			//判断指派组和技术员不能同时为空
			if(orgName==='' && tecName===''){
				msgAlert(i18n.msg_request_assignCanNotNull,'info');
			}else{
				if($("#requestAssign_isEmailNotice").attr("checked") && !checkEmail($('#requestAssign_noticeEmail_input').val())){//邮件验证是否正确
					msgAlert(i18n.emailFormatError,'info');
				}else{
					var frm = $('#requestAssign_win form,#requestActionComm form').serialize();
					var logDetails="historyRecordDto.logDetails="+i18n.label_sla_assignGroup+":"+$('#requestDetail_assigneeGroupName').val()+"<br>"+i18n.title_request_assignToTC+":"+$('#requestDetail_assigneeName').val()+"<br>"+i18n.remark+":"+$('#assignRemark').val();
					if($("#requestAssign_isEmailNotice").attr("checked")){
						logDetails+='<br>['+i18n.is_request_action_notice+']';
					}
					//显示进程
					startProcess();
					$.post('request!requestAction.action',frm+"&"+logDetails,function(){
						//隐藏进程
						endProcess();
						$('#requestAssign_win').modal('close');
						if($("#requestAssign_isEmailNotice").attr("checked")){
							requestAction.emailNotice('requestAssign_action',$('#requestAssign_noticeEmail_input').val());
						}
						requestAction.reOpeRequestInfo();
						msgAlert(i18n.msg_request_assignSuccessful);
						
					});
				}	
			}
		},
		/**
		 * @description 请求动作：进行二线\三线\四线指派.
		 * @param line 指派标识
		 * @param div 是否邮件通知元素
		 */
		requestAssign_opt:function(line,div){
			if($('#requestDetail_assigneeGroupName_'+line).val()==='' && $('#requestDetail_assigneeName_'+line).val()===''){
				msgAlert('<span>'+i18n.msg_request_assignCanNotNull+'</span>','info');
			}else{
				if($("#"+div+'_isEmailNotice').attr("checked") && !checkEmail($("#"+div+'_noticeEmail_input').val())){//邮件验证是否正确
					msgAlert(i18n.emailFormatError,'info');
				}else{
					var frm = $('#'+div+' form,#requestActionComm form').serialize();
					var assigneeGroupName=$('#requestDetail_assigneeGroupName_'+line).val();
					if(assigneeGroupName==null || assigneeGroupName==='')
						assigneeGroupName=i18n.msg_no_assigneeGroupName;
					var assigneeName=$('#requestDetail_assigneeName_'+line).val();
					if(assigneeName==null || assigneeName==='')
						assigneeName=i18n.msg_no_assigneeName;
					var logDetails="historyRecordDto.logDetails="+i18n.label_request_assignGroup+":"+assigneeGroupName+"<br>"+i18n.label_request_assigner+":"+assigneeName+"<br>"+i18n.remark+":"+$('#assignRemark_'+line).val();
					if($("#"+div+'_isEmailNotice').attr("checked")){
						logDetails+='<br>['+i18n.is_request_action_notice+']';
					}
					//显示进程
					startProcess();
					$.post('request!requestAction.action',frm+"&"+logDetails,function(){
						//隐藏进程
						endProcess();
						if($("#"+div+'_isEmailNotice').attr("checked")){
							requestAction.emailNotice(div+'_action',$("#"+div+'_noticeEmail_input').val());
						}
						$('#'+div).modal('close');
						requestAction.reOpeRequestInfo();
						msgAlert(i18n.msg_request_assignSuccessful);
					});
				}	
			}
		},
		/**
		 * 请求在指派选择用户
		 * @param name 用户名元素
		 * @param no 用户Id元素
		 */
		requestAgainAssignSelectUser:function(name,no){
			var _statusCode=$('#requestDetails_statusCode').val();
			var _roleCode='ROLE_HELPDESKENGINEER;ROLE_SECONDLINEENGINEER;ROLE_THIRDLINEENGINEER;ROLE_FOURTHLINEENGINEER';
//			if(_statusCode=='request_one')
//				_roleCode='ROLE_HELPDESKENGINEER'
//			if(_statusCode=='request_two')
//				_roleCode='ROLE_SECONDLINEENGINEER'
//			if(_statusCode=='request_three')
//				_roleCode='ROLE_THIRDLINEENGINEER'
//			if(_statusCode=='request_four')
//				_roleCode='ROLE_FOURTHLINEENGINEER'
			wstuo.user.userUtil.selectUserByRole(_roleCode,name,no);	
		},
		/**
		 * @description 进行请求再指派.
		 */
		requestAgainAssign_opt:function(){
			if($("#requestAgainAssign_isEmailNotice").attr("checked") && !checkEmail($("#requestAgainAssign_noticeEmail_input").val())){//邮件验证是否正确
				msgAlert(i18n.emailFormatError,'info');
			}else{
				var frm = $('#requestAgainAssign_win form,#requestActionComm form').serialize();
				var logDetails="historyRecordDto.logDetails="+i18n.title_request_assignToTC+":"+$('#requestDetail_assigneeName_Again').val()+"<br>"+i18n.remark+":"+$('#assignRemark_Again').val();
				if($("#requestAgainAssign_isEmailNotice").attr("checked")){
					logDetails+='<br>['+i18n.is_request_action_notice+']';
				}
				//显示进程
				startProcess();
				$.post('request!requestAction.action',frm+"&"+logDetails,function(){
					
					//显示进程
					endProcess();
					if($("#requestAgainAssign_isEmailNotice").attr("checked")){
						requestAction.emailNotice('requestAssignAgain_action',$('#requestAgainAssign_noticeEmail_input').val());
					}
					$('#requestAgainAssign_win').modal('close');
					requestAction.reOpeRequestInfo();
					msgAlert(i18n.msg_request_assignSuccessful);
				});
			}
		},
		/**
		 * @description 执行请求回退操作.
		 */
		requestBack_opt:function(){

				if($("#requestBack_isEmailNotice").attr("checked") && !checkEmail($("#requestBack_noticeEmail_input").val())){//邮件验证是否正确
					msgAlert(i18n.emailFormatError,'info');
				}else{
					requestAction.requestActionIsEmailNotice('requestBack_isEmailNotice','requestBackReason_value');
					
					var frm = $('#requestBack_win form,#requestActionComm form').serialize();
					//显示进程
					startProcess();
					$.post('request!requestAction.action',frm,function(){
						//显示进程
						endProcess();
						if($("#requestBack_isEmailNotice").attr("checked")){
							requestAction.emailNotice('backToFirstLine_action',$('#requestBack_noticeEmail_input').val());
						}
						$('#requestBack_win').modal('close');
						requestAction.reOpeRequestInfo();
						msgAlert(i18n.msg_request_backSuccessful);
					});
				}	
		
		},
		/**
		 * @description 执行请求升级操作.
		 */
		requestUpgrade_opt:function(){
			if($('#requestUpgrade_level').val()!=null){
				if($("#requestUpgrade_isEmailNotice").attr("checked") && !checkEmail($("#requestUpgrade_noticeEmail_input").val())){//邮件验证是否正确
					msgAlert(i18n.emailFormatError,'info');
				}else{
					var frm = $('#requestUpgrade_win form,#requestActionComm form').serialize();
					var logDetails="historyRecordDto.logDetails="+i18n.label_request_updateTo+":"+$('#requestUpgrade_level').find('option:selected').text()+"<br>"+i18n.label_request_updateCause+":"+$('#requestUpgradeReason').val();
					if($("#requestUpgrade_isEmailNotice").attr("checked")){
						logDetails+='<br>['+i18n.is_request_action_notice+']';
					}
					//显示进程
					startProcess();
					$.post('request!requestAction.action',frm+"&"+logDetails,function(){
						
						//隐藏进程
						endProcess();
						if($("#requestUpgrade_isEmailNotice").attr("checked")){
							requestAction.emailNotice('requestUpdate_action',$('#requestUpgrade_noticeEmail_input').val());
						}
						$('#requestUpgrade_win').modal('close');
						requestAction.reOpeRequestInfo();
						
						msgAlert(i18n.msg_request_updateSuccessful);
						
					});
				}	
			}else{
				msgAlert(i18n.msg_request_chooseUpdateOwner,'info');
			}
		},
		/**
		 * @description 进行请求升级操作.
		 */
		requestUpgradeApply_opt:function(){
			if($("#requestUpgradeApply_isEmailNotice").attr("checked") && !checkEmail($("#requestUpgradeApply_noticeEmail_input").val())){//邮件验证是否正确
				msgAlert(i18n.emailFormatError,'info');
			}else{
				requestAction.requestActionIsEmailNotice('requestUpgradeApply_isEmailNotice','requestUpgradeApplyReason_value');
				
				var frm = $('#requestUpgradeApply_win form,#requestActionComm form').serialize();
				//显示进程
				startProcess();
				$.post('request!requestAction.action',frm,function(){
					//隐藏进程
					endProcess();
					if($("#requestUpgradeApply_isEmailNotice").attr("checked")){
						requestAction.emailNotice('upgradeApplication_action',$('#requestUpgradeApply_noticeEmail_input').val());
					}
					$('#requestUpgradeApply_win').modal('close');
					requestAction.reOpeRequestInfo();
	
					msgAlert(i18n.msg_request_submitSucceccful);
				});
			}	
		},
		/**
		 * 进行请求关闭
		 */
		requestClose_opt:function(){
			var sl=$('#request_detail_solutions').val();
			if(sl!==undefined && sl.replace(/ /g,'')===''){
				$('#requestDetailsTab').tabs('select', i18n.label_solutions);
				msgAlert(i18n.msg_input_solution,'info');
			}else{
				if($("#requestClose_isEmailNotice").attr("checked") && !checkEmail($("#requestClose_noticeEmail_input").val())){//邮件验证是否正确
					msgAlert(i18n.emailFormatError,'info');
				}else{
					requestAction.requestActionIsEmailNotice('requestClose_isEmailNotice','requestClose_value');
					
					var frm = $('#requestClose_win form,#requestActionComm form').serialize();
					//显示进程
					startProcess();
					$.post('request!requestAction.action',frm,function(){
						//隐藏进程
						endProcess();
						if($("#requestClose_isEmailNotice").attr("checked")){
							requestAction.emailNotice('requestClose_action',$('#requestClose_noticeEmail_input').val());
						}
						$('#requestClose_win').modal('close');
						requestAction.reOpeRequestInfo();
						msgAlert(i18n.msg_request_closeSuccessful);
					});
				}	
			}
		},
		/**
		 * 处理备注
		 */
		requestDealRemark_opt:function(){
			$('#requestDealComplete_win').val($('#rquestDetailCreatedByEmail').val());	
				requestAction.requestActionIsEmailNotice('requestDealRemark_isEmailNotice','requestDealRemark_value');
				var frm = $('#requestDealRemark_win form,#requestActionComm form').serialize();
				//显示进程
				startProcess();
				$.post('request!requestAction.action',frm,function(){
					//隐藏进程
					endProcess();
					if($("#requestDealRemark_isEmailNotice").attr("checked")){
						requestAction.emailNotice('requestProcessMrak_action',$('#requestDealRemark_noticeEmail_input').val());
					}
					$('#requestDealRemark_win').modal('close');
					requestAction.reOpeRequestInfo();
					msgAlert(i18n.msg_request_addSucceccful);
					
				});
		},
		/**
		 * 处理完成
		 */
		requestDealComplete_opt:function(){
			if($("#requestDealComplete_isEmailNotice").attr("checked") && !checkEmail($("#requestDealComplete_noticeEmail_input").val())){//邮件验证是否正确
				msgAlert(i18n.emailFormatError,'info');
			}else{
				requestAction.requestActionIsEmailNotice('requestDealComplete_isEmailNotice','requestDealComplete_value');
				
				var frm = $('#requestDealComplete_win form,#requestActionComm form').serialize();
				//显示进程
				startProcess();
				$.post('request!requestAction.action',frm,function(){
					//隐藏进程
					endProcess();
					if($("#requestDealComplete_isEmailNotice").attr("checked")){
						requestAction.emailNotice('processComplete_action',$('#requestDealComplete_noticeEmail_input').val());
					}
					$('#requestDealComplete_win').modal('close');
					requestAction.reOpeRequestInfo();
					msgAlert(i18n.msg_request_submitSucceccful);
	
				});
			}
		},
		/**
		 * 请求重新开启
		 */
		requestReOpen_opt:function(){
			if($("#requestReOpen_isEmailNotice").attr("checked") && !checkEmail($("#requestReOpen_noticeEmail_input").val())){//邮件验证是否正确
				msgAlert(i18n.emailFormatError,'info');
			}else{
				requestAction.requestActionIsEmailNotice('requestReOpen_isEmailNotice','requestReOpen_value');
				var frm = $('#requestReOpen_win form,#requestActionComm form').serialize();
				//显示进程
				startProcess();
				$.post('request!requestAction.action',frm,function(){
					//隐藏进程
					endProcess();
					$('#requestReOpen_win').modal('close');
					if($("#requestReOpen_isEmailNotice").attr("checked")){
						requestAction.emailNotice('reStartRequest_action',$('#requestReOpen_noticeEmail_input').val());
					}
					requestAction.reOpeRequestInfo();
					msgAlert(i18n.msg_request_restartSuccessful);
				});
			}
		
		},
		/**
		 * 回退组
		 */
		requestBackGroup_opt:function(){
			if($("#requestBackGroup_isEmailNotice").attr("checked") && !checkEmail($("#requestBackGroup_noticeEmail_input").val())){//邮件验证是否正确
				msgAlert(i18n.emailFormatError,'info');
			}else{
				requestAction.requestActionIsEmailNotice('requestBackGroup_isEmailNotice','requestBackGroup_value');
				
				var frm = $('#requestBackGroup_win form,#requestActionComm form').serialize();
				//显示进程
				startProcess();
				$.post('request!requestAction.action',frm,function(){
					//隐藏进程
					endProcess();
					$('#requestBackGroup_win').modal('close');
					if($("#requestBackGroup_isEmailNotice").attr("checked")){
						requestAction.emailNotice('requestBackToGroup_action',$('#requestBackGroup_noticeEmail_input').val());
					}
					requestAction.reOpeRequestInfo();
					msgAlert(i18n.msg_request_backSuccessful);
				});
			}
			
		},
		/**
		 * @description 请求回访
		 * @param value 回访标识，标识回访动作
		 */
		requestVisit_win:function(value){
			$('#requestVisit_optType').val(value);
			$('#requestVisit_remark').val(i18n.label_returnVisit_object+':'+$('#requestDetails_createdByName').val());
			$('#requestVisit_win').modal();
		},
		/**
		 * @description 对请求进行回访
		 */
		requestVisit_opt:function(){
			var tab = document.getElementById("requestVisit_table") ;
		    //表格行数
		    var rows = tab.rows.length;
		    var visitRecord='';
		    for(var i=0;i<rows;i++){
		    	var _value = $("input[@type=radio][name=visitItemValue"+i+"][checked]").val();
		    	if(_value===undefined){
		    		_value=$('#visitItemValue'+i).val();
		    	}
		    	visitRecord=visitRecord+$('#visitItemName'+i).val()+":"+_value+"<br><hr>";
		    }
			setTimeout(function(){
				$('#request_visitRecord').val(visitRecord);
				var frm = $('#requestVisit_win form,#requestActionComm form').serialize();
				startProcess();
				$.post('request!requestAction.action',frm+'&requestDTO.visitState=1',function(){
					endProcess();
					$('#requestVisit_win').modal('close');
					requestAction.reOpeRequestInfo();
					msgAlert(i18n.msg_request_reViewSuccessful);
				});
			},0);
		},
		/**
		 * 请求邮件回访
		 */
		requestEmailVisit_opt:function(){
			startProcess();
			$.post('noticeRule!testemai.action',function(testErrorEncode){
				if(testErrorEncode===0){
					$.post('noticeRule!verifiReturnVisit.action',function(data){
						if(data){
							var userName=$('#rquestDetailCreatedByLoginName').val();
							var _param='userName='+userName
							$.post('user!findByUserName.action',_param,function(data){
								if(data.email===''){
									endProcess();
									msgAlert(i18n.visitObjectEmailNull,'info');
								}else{
									var language='zh_CN';
									if(i18n.returnItem=='Retrospect Items'){
										language='en_US';
									}
									if(i18n.returnItem=='回訪事項'){
										language='zh_TW';
									}
									$('#request_visitRecord').val(i18n.msg_request_visit_email_title.replace('xxx',$('#requestCode').val()));
									
									var frm = $('#requestVisit_win form,#requestActionComm form').serialize();
									$.post('request!requestAction.action',frm+'&language='+language,function(){
										endProcess();
										$('#requestVisit_win').modal('close');
										requestAction.reOpeRequestInfo();
										msgAlert(i18n.msg_request_reViewSuccessful);
									});
								}
							});
							
							
							
						}else{
							endProcess();
							msgAlert(i18n.label_siteReturnVisit);
						}
					});
				}else{
					endProcess();
					msgAlert(i18n.ERROR_EMAIL_ACCOUNT_ERROR);
				}
			});
		},
		/**
		 * @description 请求审批
		 * @param result 审批结果
		 */
		requestApproval_opt:function(result){
			if($("#requestApproval_isEmailNotice").attr("checked") && !checkEmail($("#requestApproval_noticeEmail_input").val())){//邮件验证是否正确
				msgAlert(i18n.emailFormatError,'info');
			}else{
				if($("#requestApproval_isEmailNotice").attr("checked")){
					if(result=='同意')
						$('#requestApprovalRemark').val($('#requestApprovalRemark').val()+'<br>'+i18n.title_approverResult+':'+i18n.title_agree+'<br>['+i18n.is_request_action_notice+']');
					else
						$('#requestApprovalRemark').val($('#requestApprovalRemark').val()+'<br>'+i18n.title_approverResult+':'+i18n.title_refused+'<br>['+i18n.is_request_action_notice+']');
				}else{
					if(result=='同意')
						$('#requestApprovalRemark').val($('#requestApprovalRemark').val()+'<br>'+i18n.title_approverResult+':'+i18n.title_agree);
					else
						$('#requestApprovalRemark').val($('#requestApprovalRemark').val()+'<br>'+i18n.title_approverResult+':'+i18n.title_refused);
				}
				var frm = $('#requestApproval_win form,#requestActionComm form').serialize();
				//显示进程
				startProcess();
				$.post('request!requestAction.action',frm+"&requestDTO.approvalResult="+result,function(){
					//隐藏进程
					endProcess();
					if($("#requestApproval_isEmailNotice").attr("checked")){     //邮件通知请求人
						requestAction.emailNotice('requestApproval_action',$('#requestApproval_noticeEmail_input').val());
					}
					$('#requestApproval_win').modal('close');
					requestAction.reOpeRequestInfo();
					msgAlert(i18n.msg_request_appSuccessful);
				
				});
			}	
		},
		/**
		 *自助解决_操作
		 */
		self_help_resolve_opt:function(){
			var solution = $('#requestSelfHelpLogDetails_solutions').val();
			 $('#requestSelfHelpLogDetails_solutions').val(trim(solution));
				var remark=i18n.remark+":"+$('#requestSelfHelpLogDetails').val();
				$("#requestSelftHelpHiddenLog").val(remark);
				var _frm = $('#self_help_resolve_win form,#requestActionComm form').serialize();
				//显示进程
				startProcess();
				$.post('request!requestAction.action',_frm,function(){
					//隐藏进程
					endProcess();
					
					$('#self_help_resolve_win').modal('close');
					requestAction.reOpeRequestInfo();
					msgAlert(i18n.msg_request_submitSucceccful);
				});
		},
		
		/**
		 * 请求挂起
		 */
		requestHang_opt:function(){

			if($("#requestHang_isEmailNotice").attr("checked") && !checkEmail($("#requestHang_noticeEmail_input").val())){//邮件验证是否正确
				msgAlert(i18n.emailFormatError,'info');
			}else{
				var frm = $('#requestHang_win form,#requestActionComm form').serialize();
				//显示进程
				startProcess();
				$.post('request!requestAction.action',frm,function(){
					//隐藏进程
					endProcess();
					if($("#requestHang_isEmailNotice").attr("checked")){
						requestAction.emailNotice('requestHang_action',$('#requestHang_noticeEmail_input').val());
					}
					$('#requestHang_win').modal('close');
					requestAction.reOpeRequestInfo();
					msgAlert(i18n.msg_request_submitSucceccful);
				});
			}			
		},
		/**
		 * 解除挂起
		 */
		requestHangRemove_opt:function(){
			if($("#requestHangRemove_isEmailNotice").attr("checked") && !checkEmail($("#requestHangRemove_noticeEmail_input").val())){//邮件验证是否正确
				msgAlert(i18n.emailFormatError,'info');
			}else{
				var frm = $('#requestHangRemove_win form,#requestActionComm form').serialize();
				//显示进程
				startProcess();
				$.post('request!requestAction.action',frm,function(){
					//隐藏进程
					endProcess();
					if($("#requestHangRemove_isEmailNotice").attr("checked")){
						requestAction.emailNotice('requestHangRemove_action',$('#requestHangRemove_noticeEmail_input').val());
					}
					$('#requestHangRemove_win').modal('close');
					requestAction.reOpeRequestInfo();
					msgAlert(i18n.msg_request_submitSucceccful);
				
				});
			}	
		},
		/**
		 * 修改SLA时间
		 */
		editRequestSlaTime_opt:function(){
				var frm = $('#editRequestSlaTime_win form,#requestActionComm form').serialize();
				var remark='';
				//判断是否修改时间
				if($('#requestActionHtml_responseTime_old').val()!=$('#requestActionHtml_responseTime').val()){
					remark=remark+i18n.label_sla_response_time+':'+$('#requestActionHtml_responseTime_old').val()+'->'+$('#requestActionHtml_responseTime').val()+'<br>';
				}
				if($('#requestActionHtml_completeTime_old').val()!=$('#requestActionHtml_completeTime').val()){
					remark=remark+i18n.label_sla_complete_time+':'+$('#requestActionHtml_completeTime_old').val()+'->'+$('#requestActionHtml_completeTime').val()+'<br>';
				}
				remark=remark+i18n.remark+':'+$('#editRequestSlaTime_remark').val();
				//显示进程
				startProcess();
				$.post('request!requestAction.action',frm+'&historyRecordDto.logDetails='+remark,function(){
					//隐藏进程
					endProcess();
					$('#editRequestSlaTime_win').modal('close');
					requestAction.reOpeRequestInfo();
					msgAlert(i18n.msg_request_submitSucceccful);
				
				});
		},
		/**
		 * 请求重新开启
		 */
		reopen_opt:function(){

				var remark=i18n.remark+":"+$("#requestReopenLogDetails").val();
				$("#requestReopenLogDetailshidden").val(remark);
				var frm = $('#request_reopen_win form,#requestActionComm form').serialize();
				//显示进程
				startProcess();
				$.post('request!requestAction.action',frm,function(){
					//隐藏进程
					endProcess();
					$('#request_reopen_win').modal('close');
					requestAction.reOpeRequestInfo();
					msgAlert(i18n.msg_request_submitSucceccful);
				
				});
		
		},
		/**
		 * 请求关闭审核
		 * @param result 审批结果
		 */
		closeAudit_opt:function(result){
			var frm = $('#request_close_audit_win form,#requestActionComm form').serialize();
			//显示进程
			startProcess();
			$.post('request!requestAction.action',frm+'&requestDTO.auditResult='+result,function(){
				//隐藏进程
				endProcess();
				$('#request_close_audit_win').modal('close');
				requestAction.reOpeRequestInfo();
				msgAlert(i18n.msg_request_submitSucceccful);
			
			});
		},
		/**
		 *请求描述不全、未提交
		 */
		notComprehensiveNotSubmitted_opt:function(){
			if($("#notComprehensiveNotSubmitted_isEmailNotice").attr("checked") && !checkEmail($("#notComprehensiveNotSubmitted_noticeEmail_input").val())){//邮件验证是否正确
				msgAlert(i18n.emailFormatError,'info');
			}else{
				var frm = $('#request_notComprehensiveNotSubmitted_win form,#requestActionComm form').serialize();
				//显示进程
				startProcess();
				$.post('request!requestAction.action',frm,function(){
					//隐藏进程
					endProcess();
					if($("#notComprehensiveNotSubmitted_isEmailNotice").attr("checked")){
						$('#requestAction_tempDir').val('requestReturnEmailNotice.ftl');
						$('#requestAction_emailTitleTemp').val('requestReturnEmailNotice_title.ftl');
						requestAction.emailNotice('notComprehensiveNotSubmitted_action',$('#notComprehensiveNotSubmitted_noticeEmail_input').val());
					}
					$('#request_notComprehensiveNotSubmitted_win').modal('close');
					requestAction.reOpeRequestInfo();
					msgAlert(i18n.msg_request_submitSucceccful);
				
				});
			}
		},
		/**
		 *  请求描述完全，重新提交
		 */
		resubmit_opt:function(){
			var frm = $('#request_resubmit_win form,#requestActionComm form').serialize();
			//显示进程
			startProcess();
			$.post('request!requestAction.action',frm,function(){
				//隐藏进程
				endProcess();
				$('#request_resubmit_win').modal('close');
				requestAction.reOpeRequestInfo();
				msgAlert(i18n.msg_request_submitSucceccful);
			
			});
		},
		/**
		 * 打开流程处理窗口
		 * @param title 下一步任务
		 */
		processHandle_win:function(title){
			$('#request_logTitle,#processNextStep').val(title);
			$('#request_process_handle_win').modal();
		},
		/**
		 * 请求流程处理
		 */
		processHandle_opt:function(){
			var frm = $('#request_process_handle_win form,#requestActionComm form').serialize();
			//显示进程
			startProcess();
			$.post('request!requestAction.action',frm,function(){
				//隐藏进程
				endProcess();
				$('#request_process_handle_win').modal('close');
				requestAction.reOpeRequestInfo();
				msgAlert(i18n.msg_request_submitSucceccful);
			
			});
		},
		/**
		 * 打开请求审批窗口
		 * @param title 下一步动作
		 */
		requestApprovalResult_win:function(title){
			$('#requestApprovalResult_logTitle,#processNextStep').val(title);
			$('#requestApprovalResult_win').modal();
		},
		/**
		 * 请求审批结果保存
		 * @param result 审批结果
		 */
		requestApprovalResult_opt:function(result){
			var frm = $('#requestApprovalResult_win form,#requestActionComm form').serialize();
			//显示进程
			startProcess();
			$.post('request!requestAction.action',frm+'&requestDTO.auditResult='+result,function(){
				//隐藏进程
				endProcess();
				$('#requestApprovalResult_win').modal('close');
				requestAction.reOpeRequestInfo();
				msgAlert(i18n.msg_request_submitSucceccful);
			
			});
		},
		/**
		 * 打开重新匹配sla窗口
		 */
		open_refit_sla_window:function(){
			$('#refitSLA_window').modal();
		},
		/**
		 * 进行重新匹配sla动作
		 */
		save_refit_sla:function(){
			if($("#refit_fitType").val()=="0" && $("#refit_slaRules").val()=="0"){
				msgAlert(i18n.msg_refitSLA_alert,"info");
			}else{
				var frm = $('#refitSLA_window form').serialize();
				//显示进程
				startLoading();
				$.post('request!reFitSLA.action',frm,function(){
					//隐藏进程
					endLoading();
					$('#refitSLA_window').modal('close');
					requestAction.reOpeRequestInfo();
					
					msgAlert(i18n.msg_request_submitSucceccful);
				});
			}
		},
		/**
		 * 打开流程处理窗口
		 * @param taskId 任务Id
		 * @param next 下一步
		 * @param optType 操作类型
		 */
		openProcessHandleWin:function(taskId,next,optType){
			$('#processHandle_assigneeName').val(userName);
			$('#processHandle_optType').val(optType);
			$('#processNextStep,#processHandle_action').val(next);
			$('#processHandle_taskId').val(taskId);
			$('#processHandleDiv').modal();
//			$('#processHandleDiv').window('open');
		},
		/**
		 * 请求流程处理
		 */
		processHandle:function(){
			var frm = $('#processHandleDiv form,#requestActionComm form').serialize();
			//显示进程
			startProcess();
			$.post('request!requestAction.action',frm,function(){
				//隐藏进程
				endProcess();
				$('#processHandleDiv').modal('close');
				requestAction.reOpeRequestInfo();
				msgAlert(i18n.msg_request_submitSucceccful);
			});
		}
	};
}();