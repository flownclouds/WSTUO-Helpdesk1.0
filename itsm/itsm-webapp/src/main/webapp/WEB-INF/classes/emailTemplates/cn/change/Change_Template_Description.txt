序号，规则编号，描述
1、newChangeNotice:变更创建成功通知
2、changeAssignNotice:变更指派通知
3、changeCloseNoticeTechnician:变更被关闭时通知以下技术员
4、changeCloseNoticeCMDB:变更被关闭时通知CMDB
5、theCiTodayMultipleChangeNotice:同一配置项在同一天有多个变更冲突通知
6、changeFlowActionNotice：变更流程动作通知
7、changeApproveNoticeCAB:变更进行审批动作时通知CAB