<style>
    .table_border{width:100%}.table_border{ border-collapse:collapse; border:solid
    black; border-width:1px 0 0 1px;}.table_border caption {font-size:14px;font-weight:bolder;}.table_border
    th,.table_border td {border:solid black;border-width:0 1px 1px 0;padding-left:5px;}
</style>
<table width="550">
    <#escape x as x!>
        <tr>
            <td colspan="2">
                您好:
                </b>
                </font>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                您有一個新任務，請及時查看。
                <br/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                任務標題：${variables.etitle}
                <br/>
                任務描述：${variables.edesc}
                <br/>
                任務計劃時間：${variables.planTime}
                <br/>
                任務創建人：${variables.createdByName}
		 <br/>
                創建時間: ${variables.createdOn?string("yyyy-MM-dd HH:MM:ss")}
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br>
                謝謝！
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Wstuo Team
            </td>
        </tr>
    </#escape>
</table>
<br>
-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
<br>
這是一封系統的郵件，請勿直接回復！
<br>
-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~