<style>
    .table_border{width:100%}.table_border{ border-collapse:collapse; border:solid
    black; border-width:1px 0 0 1px;}.table_border caption {font-size:14px;font-weight:bolder;}.table_border
    th,.table_border td {border:solid black;border-width:0 1px 1px 0;padding-left:5px;}
</style>
<table width="550">
    <#escape x as x!>
        <tr>
            <td colspan="2">
                <#if (userName??)>
                    ${userName}，
                </#if>
                您好:
                </b>
                </font>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                新的髮佈[${variables.ecode}]創建成功通知
            </td>
        </tr>
        <#if (historyRecordDTO?exists && historyRecordDTO?size>
            0) >
            <tr>
                <td colspan="2">
                    髮佈[${variables.ecode}]歷史記录:
                    <table class="table_border">
                        <tr>
                            <td>
                                動作
                            </td>
                            <td>
                                詳細
                            </td>
                            <td>
                                操作者
                            </td>
                            <td>
                                操作時間
                            </td>
                        </tr>
                        <#list historyRecordDTO as historyRecord>
                            <tr>
                                <td width="20%">
                                    ${historyRecord.logTitle}
                                </td>
                                <td style="word-wrap:break-word;word-break:break-all;" width="40%">
                                    ${historyRecord.logDetails}
                                </td>
                                <td width="20%">
                                    ${historyRecord.operator}
                                </td>
                                <td width="20%">
                                    ${historyRecord.createdTime?string("yyyy-MM-dd HH:mm:ss")}
                                </td>
                            </tr>
                        </#list>
                    </table>
                </td>
            </tr>
        </#if>
        <tr>
            <td colspan="2">
                <br>
                謝謝！
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Wstuo Team
            </td>
        </tr>
    </#escape>
</table>
<br>
-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
<br>
這是一封係統的郵件，請勿直接迴複！
<br>
-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~