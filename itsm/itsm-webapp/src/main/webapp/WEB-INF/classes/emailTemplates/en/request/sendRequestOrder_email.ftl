<style>
    .table_border{width:100%}
	.table_border{ border:solid black; border-width:1px 0 0 1px;}
	.table_border caption {font-size:14px;font-weight:bolder;}
	.table_border th,.table_border td {border:solid black;border-width:0 1px 1px 0;padding-left:5px;}
    .table_border a{text-decoration:none}
</style>
<table width="550">
    <#escape x as x!>
        <tr>
            <td colspan="2">
                Hello:
                </b>
                </font>
            </td>
        </tr>
        <tr>
            <td colspan="2">
  &nbsp;&nbsp;This is No.${variables.ecode} request.
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table width="732px">
                    <thead>
                        <tr style="font-size:12px;" align=left>
                            <th width="40%" align="left">
                                <th align="left">
                                    <br/>
                                    <font size="6px">
                                        Work Order from Request
                                    </font>
                                </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr height=30px>
                            <td>
                                Work Order No:
                                <span id="request_printCode">
				${variables.ecode}
                                </span>
                            </td>
                            <td align=right>
                                Create Time:
                                <span id="request_printCreatedOn">
                                     ${variables.createdOn?string("yyyy-MM-dd HH:MM:ss")}
                                </span>
                                 
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="table_border" border="1" cellspacing="0px" style="border-collapse:collapse;width:732px;height:1026px;border:1px solid black;">
                    <tbody>
                        <tr style="font-size:12px;background:#ccc" align="left">
                            <th style="padding:5px; text-align: left" colspan="4">
                             Basic Info
                            </th>
                        </tr>
                        <tr height=30>
                            <td width="108px" style="border:1px solid black;">
                                Submitter
                            </td>
                            <td width="208px">
                                <span>
                                    ${variables.createdByName}
                                </span>
                            </td>
                            <td width="109px">
                               Mobile
                            </td>
                            <td width="208px">
                                <span>
                                    ${variables.createdByPhone}
                                </span>
                            </td>
                        </tr>
                        <tr style="font-size:12px;background:#ccc;" align="left">
                            <th style="padding:5px;text-align: left" colspan="4">
                              Request Details
                            </th>
                        </tr>
                        <tbody>
                            <tr>
                                <td>
                                   Title
                                </td>
                                <td colspan="3">
                                    <span style="text-align:left;font-size:12px;">
                                        ${variables.etitle}
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Content
                                </td>
                                <td height="200px" colspan="3">
                                    ${variables.edesc}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Influence
                                </td>
                                <td>
                                    <span style="text-align:left;font-size:12px;">
                                         ${variables.effectRangeName}
                                    </span>
                                </td>
                                <td>
                                  Status
                                </td>
                                <td>
                                    <span style="text-align:left;font-size:12px;">
                                        ${variables.statusName}
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Request Category
                                </td>
                                <td>
                                    <span style="text-align:left;font-size:12px;">
                                        ${variables.ecategoryName}
                                    </span>
                                </td>
                                <td>
                                    Complexity
                                </td>
                                <td>
                                    <span style="text-align:left;font-size:12px;">
                                        ${variables.complexity}
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Priority
                                </td>
                                <td>
                                    <span style="text-align:left;font-size:12px;">
                                        ${variables.priorityName}
                                    </span>
                                </td>
                                <td>
                                   Seriousness
                                </td>
                                <td>
                                    <span style="text-align:left;font-size:12px;">
                                        ${variables.seriousnessName}
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Receiving Way
                                </td>
                                <td>
                                    <span style="text-align:left;font-size:12px;">
                                        ${variables.imodeName}
                                    </span>
                                </td>
                                <td>
                                    Related CI
                                </td>
                                <td>
                                    <span style="text-align:left;font-size:12px;">
                                        ${variables.ciname}
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    SLA Response Time
                                </td>
                                <td>
                                    <span style="text-align:left;font-size:12px;">
                                        ${variables.maxResponsesTime?string("yyyy-MM-dd HH:MM:ss")}
                                    </span>
                                </td>
                                <td>
                                    SLA Finish Time
                                </td>
                                <td>
                                    <span style="text-align:left;font-size:12px;">
                                        ${variables.maxCompletesTime?string("yyyy-MM-dd HH:MM:ss")}
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Actual Response Time
                                </td>
                                <td>
                                    <span style="text-align:left;font-size:12px;">
                                        ${variables.responsesTime?string("yyyy-MM-dd HH:MM:ss")}
                                    </span>
                                </td>
                                <td>
                                    Actual Finish Time
                                </td>
                                <td>
                                    <span style="text-align:left;font-size:12px;">
                                        ${variables.closeTime?string("yyyy-MM-dd HH:MM:ss")}
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Historic Records
                                </td>
                                <td height="130px" colspan="3">
                                    <span style="text-align:left;font-size:12px;">
                                        <#if (historyRecordDTO?exists && historyRecordDTO?size>
                                            0) >
                                            <#list historyRecordDTO as historyRecord>
                                                <span class=wspan>
                                                    <span>
                                                    </span>
                                                    ${historyRecord.logTitle}: ${historyRecord.logDetails}&nbsp;&nbsp;(${historyRecord.operator})${historyRecord.createdTime?string("yyyy-MM-dd
                                                    HH:mm:ss")}
                                                    <br/>
                                                </span>
                                            </#list>
                                        </#if>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   Solutions
                                </td>
                                <td height="269px" colspan="3">
                                    <span style="text-align:left;font-size:12px;">
                                        ${variables.solutions}
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br>
            Thank you very much!
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Wstuo Team
            </td>
        </tr>
    </#escape>
</table>
<br>
-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
<br>
This is an automatic email by the system, please do NOT reply!
<br>
-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~