package com.wstuo.common.createmq;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.jms.support.converter.MessageConverter;

import com.wstuo.common.activemq.service.IActiveMQService;
import com.wstuo.itsm.request.dto.RequestHttpDTO;

/**
 * request create Message Converter
 * @author will
 *
 */
public class RequestCreateMessageConverter implements MessageConverter {
	@Autowired
	private IActiveMQService activeMQService;
	public RequestHttpDTO fromMessage(Message msg) throws JMSException,
			MessageConversionException {
		return (RequestHttpDTO) activeMQService.fromMessage(msg);
	}

	/**
	 * 发送信息
	 */
    public Message toMessage(Object obj, Session session) throws JMSException,MessageConversionException {
    	return activeMQService.toMessage(obj,session);
	}

}
