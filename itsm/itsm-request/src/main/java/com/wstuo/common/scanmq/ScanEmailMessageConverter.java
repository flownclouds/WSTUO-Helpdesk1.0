package com.wstuo.common.scanmq;


import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.jms.support.converter.MessageConverter;

import com.wstuo.common.activemq.service.IActiveMQService;
import com.wstuo.common.tools.dto.ExportQueryDTO;

/**
 * 扫描信息转换
 * @author QXY
 *
 */
public class ScanEmailMessageConverter implements MessageConverter {
	@Autowired
	private IActiveMQService activeMQService;
	public ExportQueryDTO fromMessage(Message msg) throws JMSException,
			MessageConversionException {
		return (ExportQueryDTO) activeMQService.fromMessage(msg);
	}

	/**
	 * 发送信息
	 */
    public Message toMessage(Object obj, Session session) throws JMSException,MessageConversionException {
    	System.err.println("scan");
    	return activeMQService.toMessage(obj,session);
	}

}
