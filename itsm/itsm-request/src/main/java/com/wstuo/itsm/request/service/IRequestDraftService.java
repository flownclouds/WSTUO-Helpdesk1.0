package com.wstuo.itsm.request.service;

import com.wstuo.itsm.request.dto.RequestDTO;
import com.wstuo.itsm.request.dto.RequestDraftQueryDTO;
import com.wstuo.itsm.request.entity.RequestDraft;
import com.wstuo.common.dto.PageDTO;



public interface IRequestDraftService {
	void saveRequestDraft(RequestDraft requestDraft,RequestDTO requestDTO);
	PageDTO findPageRequestDraft(RequestDraftQueryDTO queryDTO);
	RequestDTO findRequestDraftById(Long ids);
	boolean deleteRequestDraftById(Long ids);
	boolean editRequestDraft(RequestDraft draft);
	
}
