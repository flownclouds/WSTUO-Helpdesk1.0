package com.wstuo.itsm.request.dto;

import com.wstuo.common.util.StringUtils;

/**
 * 请求简单信息DTO
 * @author QXY
 *
 */
public class RequestSimpleDTO{

    private Long eno;
    private String etitle;
    private String createdByName;
    
	public Long getEno() {
		return eno;
	}
	
	public void setEno(Long eno) {
		this.eno = eno;
	}
	public String getEtitle() {
		return StringUtils.cutStr(etitle, 20,StringUtils.TOP_PATH);
	}
	public void setEtitle(String etitle) {
		this.etitle = etitle;
	}
	public String getCreatedByName() {
		return StringUtils.cutStr(createdByName, 8,StringUtils.TOP_PATH);
	}
	public void setCreatedByName(String createdByName) {
		this.createdByName = createdByName;
	}
    
    
    

	
}
