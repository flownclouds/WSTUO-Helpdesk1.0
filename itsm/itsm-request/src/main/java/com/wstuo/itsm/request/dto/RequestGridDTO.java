package com.wstuo.itsm.request.dto;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 请求列表DTO.
 * 
 * @author QXY
 * 
 */
@SuppressWarnings("serial")
public class RequestGridDTO extends EventDTO {
	private Long imodeNo;
	private Long levelNo;
	private Long assigneeNo;
	private Long assigneeGroupNo;

	// View
	private String assigneeGroupName;
	private String assigneeName;
	private Long technicianNo;
	private String technicianName;
	private String slaState;

	private String requestCode;
	private String remarkStatusName;
	private Long upgradeApplySign;
	private String requestCategoryName;

	private Long ownerNo;
	private String ownerName;

	private String companyName;// 客户名称
	private Long companyNo;// 客户No
	private Boolean hang = false;// 挂起(20110831-QXY)
	private String mode;// 来源
	private String levelName;
	private Date maxResponsesTime;// SLA响应时间
	private Date maxCompletesTime;// SLA完成时间
	private Date responsesTime;// 实际响应时间
	private Date closeTime;// 实际完成时间
	private Date lastUpdateTime;

	private String requesterGroup;// 请求人所在组
	private Boolean isConvertdToProblem = false;// 已经转换为问题
	private Boolean isConvertdToChange = false;// 已经转换为变更
	private Long requestServiceDirNo;
	private String requestServiceDirName;
	// 背景色
	private String modeColor;// 来源颜色
	private String slaStateColor;// SLA状态背景色
	private String priorityColor; // 优先级背景色
	private String seriousnessColor; // 紧急度背景色
	private String effectRangeColor;// 影响背景色
	private String levelColor;// 级别背景色
	private String statusColor;// 状态背景色
	private String closeCode;// 关闭Code
	private String slaStateDno;// 状态代码
	private Map<String,String> attrVals =new HashMap<String, String>();
	
	public Map<String, String> getAttrVals() {
		return attrVals;
	}

	public void setAttrVals(Map<String, String> attrVals) {
		this.attrVals = attrVals;
	}

	public Long getRequestServiceDirNo() {
		return requestServiceDirNo;
	}

	public void setRequestServiceDirNo(Long requestServiceDirNo) {
		this.requestServiceDirNo = requestServiceDirNo;
	}

	public String getRequestServiceDirName() {
		return requestServiceDirName;
	}

	public void setRequestServiceDirName(String requestServiceDirName) {
		this.requestServiceDirName = requestServiceDirName;
	}

	public String getPriorityColor() {
		return priorityColor;
	}

	public void setPriorityColor(String priorityColor) {
		this.priorityColor = priorityColor;
	}

	public String getSeriousnessColor() {
		return seriousnessColor;
	}

	public void setSeriousnessColor(String seriousnessColor) {
		this.seriousnessColor = seriousnessColor;
	}

	public Long getOwnerNo() {
		return ownerNo;
	}

	public void setOwnerNo(Long ownerNo) {
		this.ownerNo = ownerNo;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getSlaState() {
		return slaState;
	}

	public void setSlaState(String slaState) {
		this.slaState = slaState;
	}

	public Long getImodeNo() {
		return imodeNo;
	}

	public void setImodeNo(Long imodeNo) {
		this.imodeNo = imodeNo;
	}

	public Long getLevelNo() {
		return levelNo;
	}

	public void setLevelNo(Long levelNo) {
		this.levelNo = levelNo;
	}

	public Long getAssigneeNo() {
		return assigneeNo;
	}

	public void setAssigneeNo(Long assigneeNo) {
		this.assigneeNo = assigneeNo;
	}

	public String getSlaStateDno() {
		return slaStateDno;
	}

	public void setSlaStateDno(String slaStateDno) {
		this.slaStateDno = slaStateDno;
	}

	public Long getAssigneeGroupNo() {
		return assigneeGroupNo;
	}

	public void setAssigneeGroupNo(Long assigneeGroupNo) {
		this.assigneeGroupNo = assigneeGroupNo;
	}

	public String getAssigneeGroupName() {
		return assigneeGroupName;
	}

	public void setAssigneeGroupName(String assigneeGroupName) {
		this.assigneeGroupName = assigneeGroupName;
	}

	public String getAssigneeName() {
		return assigneeName;
	}

	public void setAssigneeName(String assigneeName) {
		this.assigneeName = assigneeName;
	}

	public String getRequestCode() {
		return requestCode;
	}

	public void setRequestCode(String requestCode) {
		this.requestCode = requestCode;
	}

	public String getRemarkStatusName() {
		return remarkStatusName;
	}

	public void setRemarkStatusName(String remarkStatusName) {
		this.remarkStatusName = remarkStatusName;
	}

	public Long getUpgradeApplySign() {
		return upgradeApplySign;
	}

	public void setUpgradeApplySign(Long upgradeApplySign) {
		this.upgradeApplySign = upgradeApplySign;
	}

	public String getRequestCategoryName() {
		return requestCategoryName;
	}

	public void setRequestCategoryName(String requestCategoryName) {
		this.requestCategoryName = requestCategoryName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Long getCompanyNo() {
		return companyNo;
	}

	public void setCompanyNo(Long companyNo) {
		this.companyNo = companyNo;
	}

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	public Date getMaxResponsesTime() {
		return maxResponsesTime;
	}

	public void setMaxResponsesTime(Date maxResponsesTime) {
		this.maxResponsesTime = maxResponsesTime;
	}

	public Date getMaxCompletesTime() {
		return maxCompletesTime;
	}

	public void setMaxCompletesTime(Date maxCompletesTime) {
		this.maxCompletesTime = maxCompletesTime;
	}

	public Date getResponsesTime() {
		return responsesTime;
	}

	public void setResponsesTime(Date responsesTime) {
		this.responsesTime = responsesTime;
	}

	public Date getCloseTime() {
		return closeTime;
	}

	public void setCloseTime(Date closeTime) {
		this.closeTime = closeTime;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	public String getSlaStateColor() {
		return slaStateColor;
	}

	public void setSlaStateColor(String slaStateColor) {
		this.slaStateColor = slaStateColor;
	}

	public String getRequesterGroup() {
		return requesterGroup;
	}

	public void setRequesterGroup(String requesterGroup) {
		this.requesterGroup = requesterGroup;
	}

	public Boolean getHang() {
		return hang;
	}

	public void setHang(Boolean hang) {
		this.hang = hang;
	}

	public Boolean getIsConvertdToProblem() {
		return isConvertdToProblem;
	}

	public void setIsConvertdToProblem(Boolean isConvertdToProblem) {
		this.isConvertdToProblem = isConvertdToProblem;
	}

	public Boolean getIsConvertdToChange() {
		return isConvertdToChange;
	}

	public void setIsConvertdToChange(Boolean isConvertdToChange) {
		this.isConvertdToChange = isConvertdToChange;
	}

	public String getModeColor() {
		return modeColor;
	}

	public void setModeColor(String modeColor) {
		this.modeColor = modeColor;
	}

	public String getEffectRangeColor() {
		return effectRangeColor;
	}

	public void setEffectRangeColor(String effectRangeColor) {
		this.effectRangeColor = effectRangeColor;
	}

	public String getLevelColor() {
		return levelColor;
	}

	public void setLevelColor(String levelColor) {
		this.levelColor = levelColor;
	}

	public String getStatusColor() {
		return statusColor;
	}

	public void setStatusColor(String statusColor) {
		this.statusColor = statusColor;
	}

	public String getCloseCode() {
		return closeCode;
	}

	public void setCloseCode(String closeCode) {
		this.closeCode = closeCode;
	}

	public Long getTechnicianNo() {
		return technicianNo;
	}

	public void setTechnicianNo(Long technicianNo) {
		this.technicianNo = technicianNo;
	}

	public String getTechnicianName() {
		return technicianName;
	}

	public void setTechnicianName(String technicianName) {
		this.technicianName = technicianName;
	}

}
