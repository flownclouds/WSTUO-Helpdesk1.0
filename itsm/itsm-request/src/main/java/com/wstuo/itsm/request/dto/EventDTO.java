package com.wstuo.itsm.request.dto;

import com.wstuo.common.dto.BaseDTO;

import java.util.Date;

/**
 * 请求DTO
 * @author QXY
 *
 */
@SuppressWarnings( "serial" )
public class EventDTO
    extends BaseDTO
{
    private Long eno;
    private Long ecategoryNo;
    private String ecategoryName;
    private String etitle;
    private String edesc;
    private Long effectRangeNo;
    private String effectRangeName;
    private String effectRemark;
    private Long seriousnessNo;
    private String seriousnessName;
    private Long priorityNo;
    private String priorityName;
    private Long statusNo;
    private String statusName;
    private String statusDno;
    private String address;
    private Date createdOn;
    private Long createdByNo;
    private String createdByName;
    private Long technicianNo;
    private String technicianName;
    private String pid;
    private Long ciId;
    private String ciName;
    private String solutions;
    private Date slaDoneDate;
    private Date slaResponseDate;
    private String keyWord;
    

    public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public String getSolutions() {
		return solutions;
	}

	public void setSolutions(String solutions) {
		this.solutions = solutions;
	}

	public Long getCiId() {
		return ciId;
	}

	public void setCiId(Long ciId) {
		this.ciId = ciId;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public EventDTO(  )
    {
        super(  );
    }

    public Long getEno(  )
    {
        return eno;
    }

    public void setEno( Long eno )
    {
        this.eno = eno;
    }

    public Long getEcategoryNo(  )
    {
        return ecategoryNo;
    }

    public void setEcategoryNo( Long ecategoryNo )
    {
        this.ecategoryNo = ecategoryNo;
    }

    public String getEcategoryName(  )
    {
        return ecategoryName;
    }

    public void setEcategoryName( String ecategoryName )
    {
        this.ecategoryName = ecategoryName;
    }

    public String getEtitle(  )
    {
        return etitle;
    }

    public void setEtitle( String etitle )
    {
        this.etitle = etitle;
    }

    public String getEdesc(  )
    {
        return edesc;
    }

    public void setEdesc( String edesc )
    {
        this.edesc = edesc;
    }

    public Long getEffectRangeNo(  )
    {
        return effectRangeNo;
    }

    public void setEffectRangeNo( Long effectRangeNo )
    {
        this.effectRangeNo = effectRangeNo;
    }

    public String getEffectRangeName(  )
    {
        return effectRangeName;
    }

    public void setEffectRangeName( String effectRangeName )
    {
        this.effectRangeName = effectRangeName;
    }

    public String getEffectRemark(  )
    {
        return effectRemark;
    }

    public void setEffectRemark( String effectRemark )
    {
        this.effectRemark = effectRemark;
    }

    public Long getSeriousnessNo(  )
    {
        return seriousnessNo;
    }

    public void setSeriousnessNo( Long seriousnessNo )
    {
        this.seriousnessNo = seriousnessNo;
    }

    public String getSeriousnessName(  )
    {
        return seriousnessName;
    }

    public void setSeriousnessName( String seriousnessName )
    {
        this.seriousnessName = seriousnessName;
    }

    public Long getPriorityNo(  )
    {
        return priorityNo;
    }

    public void setPriorityNo( Long priorityNo )
    {
        this.priorityNo = priorityNo;
    }

    public String getPriorityName(  )
    {
        return priorityName;
    }

    public void setPriorityName( String priorityName )
    {
        this.priorityName = priorityName;
    }

    public Long getStatusNo(  )
    {
        return statusNo;
    }

    public void setStatusNo( Long statusNo )
    {
        this.statusNo = statusNo;
    }

    public String getStatusName(  )
    {
        return statusName;
    }

    public void setStatusName( String statusName )
    {
        this.statusName = statusName;
    }

    public String getAddress(  )
    {
        return address;
    }

    public void setAddress( String address )
    {
        this.address = address;
    }

    public Date getCreatedOn(  )
    {
        return createdOn;
    }

    public void setCreatedOn( Date createdOn )
    {
        this.createdOn = createdOn;
    }

    public Long getCreatedByNo(  )
    {
        return createdByNo;
    }

    public void setCreatedByNo( Long createdByNo )
    {
        this.createdByNo = createdByNo;
    }

    public String getCreatedByName(  )
    {
        return createdByName;
    }

    public void setCreatedByName( String createdByName )
    {
        this.createdByName = createdByName;
    }

    public Long getTechnicianNo(  )
    {
        return technicianNo;
    }

    public void setTechnicianNo( Long technicianNo )
    {
        this.technicianNo = technicianNo;
    }

    public String getTechnicianName(  )
    {
        return technicianName;
    }

    public void setTechnicianName( String technicianName )
    {
        this.technicianName = technicianName;
    }

	public String getCiName() {
		return ciName;
	}

	public void setCiName(String ciName) {
		this.ciName = ciName;
	}

	public Date getSlaDoneDate() {
		return slaDoneDate;
	}

	public void setSlaDoneDate(Date slaDoneDate) {
		this.slaDoneDate = slaDoneDate;
	}

	public Date getSlaResponseDate() {
		return slaResponseDate;
	}

	public void setSlaResponseDate(Date slaResponseDate) {
		this.slaResponseDate = slaResponseDate;
	}

	public String getStatusDno() {
		return statusDno;
	}

	public void setStatusDno(String statusDno) {
		this.statusDno = statusDno;
	}

    
}
