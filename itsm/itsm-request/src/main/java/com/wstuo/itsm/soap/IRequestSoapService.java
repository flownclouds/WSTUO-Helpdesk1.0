package com.wstuo.itsm.soap;

import javax.jws.WebService;

/**
 * 请求SOAP协议接口
 * @author QXY
 *
 */
@WebService
public interface IRequestSoapService {
	/**
	 * 保存请求
	 * @param endto
	 * @param meassage
	 * @return String
	 */
	public String soapSaveRequest(String endto,String meassage);
}
