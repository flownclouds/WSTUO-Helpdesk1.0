package com.wstuo.itsm.updateRequest.service;

import com.wstuo.itsm.updateRequest.dto.UpdateRequestDTO;
/**
 * 升级时间Service interface class
 * @author Will
 *
 */
public interface IUpdateRequestService {

	/**
	 * 保存升级时间
	 * @param dto
	 */
	void merge(UpdateRequestDTO dto);
	/**
	 * 查询升级时间
	 * @return UpdateRequestDTO
	 */
	UpdateRequestDTO find();
	/**
	 * 新增一条默认数据
	 */
	void saveUpdateRequest();
}
