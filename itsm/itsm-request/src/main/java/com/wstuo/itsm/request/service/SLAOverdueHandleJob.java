package com.wstuo.itsm.request.service;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;

import com.wstuo.common.exception.ApplicationException;
import com.wstuo.common.util.AppliactionBaseListener;

/**
 * SLA逾期处理(升级及通知)
 * @author WSTUO_QXY
 *
 */
public class SLAOverdueHandleJob implements Job {
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		ApplicationContext ctx =AppliactionBaseListener.ctx;
		ISLAOverdueHandleService autoUpdateRequestService=(ISLAOverdueHandleService)ctx.getBean("autoUpdateRequestService");
		try {
			autoUpdateRequestService.slaOverdueHandle();
		} catch (Exception e) {
			throw new ApplicationException("SLAOverdueHandleJob_Error/n",e);
		}
	}

}
