package com.wstuo.itsm.request.service;

import com.wstuo.common.config.category.entity.EventCategory;
import com.wstuo.itsm.request.entity.EventCount;

public interface IEventCountService {
	/**
	 * 生成事件编码
	 * @param module
	 * @param eventCategroy
	 * @return 返回事件编码
	 */
	String generateEventCode(String module , EventCategory eventCategory);
	
	/**
	 * 保存事件统计记录
	 * @param entity
	 */
	void save(EventCount entity);
}
