package com.wstuo.common.util;

import java.text.DecimalFormat;
import java.util.Date;

import com.wstuo.itsm.cim.dto.CIDTO;

public class DepreciationUtil {

	
	/**
	 * 折旧率算法：已使用时间=当前时间-硬件到货日期=12个月 折旧率 =1- 已使用时间 /多少年折旧为0
	 */
	public static double calculateCIDepreciationRate(CIDTO ciDTO) {
		Date todayDate = new Date();
		Date arrivalDate = ciDTO.getArrivalDate();
		long dayCount = ciDTO.getDepreciationIsZeroYears() * 365;
		long alreadyDay = todayDate.getTime() / 1000 / 60 / 60 / 24
				- arrivalDate.getTime() / 1000 / 60 / 60 / 24;
		double rate = 100 - (double) alreadyDay * 100 / dayCount;
		DecimalFormat df = new DecimalFormat("#######.##");
		rate = Double.parseDouble(df.format(rate));
		return rate;
	}
}
