package com.wstuo.itsm.cim.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.itsm.cim.dto.CIRelevanceDTO;
import com.wstuo.itsm.cim.dto.CiRelevanceTreeViewDTO;
import com.wstuo.itsm.cim.service.ICIRelevanceService;


/**
 *
 * Action CI Relevance
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class CIRelevanceAction extends ActionSupport {

    @Autowired
    private ICIRelevanceService ciRelevanceService;
    private PageDTO ciRelevance;
    private Long[] ids;
    private int page = 1;
    private int rows = 10;
    private CIRelevanceDTO ciRelevanceDTO;
    private Long ciRelevanceId;
    private CiRelevanceTreeViewDTO ciReleTreeDto;
    private boolean releResult;
    private String relateForward;
    private String relateBack;
    private String sord;
    private String sidx;
    public PageDTO getCiRelevance() {

        return ciRelevance;
    }

    public void setCiRelevance(PageDTO ciRelevance) {

        this.ciRelevance = ciRelevance;
    }

    public Long[] getIds() {

        return ids;
    }

    public void setIds(Long[] ids) {

        this.ids = ids;
    }

    public int getPage() {

        return page;
    }

    public void setPage(int page) {

        this.page = page;
    }

    public int getRows() {

        return rows;
    }

    public void setRows(int rows) {

        this.rows = rows;
    }

    public CIRelevanceDTO getCiRelevanceDTO() {

        return ciRelevanceDTO;
    }

    public void setCiRelevanceDTO(CIRelevanceDTO ciRelevanceDTO) {

        this.ciRelevanceDTO = ciRelevanceDTO;
    }
    /**
     * 关联编号
     * @return String
     */
    public Long getCiRelevanceId() {

        return ciRelevanceId;
    }

    public void setCiRelevanceId(Long ciRelevanceId) {

        this.ciRelevanceId = ciRelevanceId;
    }

    

	public CiRelevanceTreeViewDTO getCiReleTreeDto() {
		return ciReleTreeDto;
	}

	public void setCiReleTreeDto(CiRelevanceTreeViewDTO ciReleTreeDto) {
		this.ciReleTreeDto = ciReleTreeDto;
	}
	/**
	 * 判断当前关联配置项是否已关联
	 * @return String
	 */
	public boolean isReleResult() {
		return releResult;
	}

	public void setReleResult(boolean releResult) {
		this.releResult = releResult;
	}

	/**
	 * 关联的配置项名称
	 * @return String
	 */
	public String getRelateForward() {
		return relateForward;
	}

	public void setRelateForward(String relateForward) {
		this.relateForward = relateForward;
	}
	/**
	 * 被关联的配置项名称
	 * @return String
	 */
	public String getRelateBack() {
		return relateBack;
	}

	public void setRelateBack(String relateBack) {
		this.relateBack = relateBack;
	}
	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	/**
     * ci relevance pager query
     * @return String
     */
    public String find() {
        int start = (page - 1) * rows;
        ciRelevance = ciRelevanceService.configureItemRelevanceFindPager(ciRelevanceId, start,
                rows,sord,sidx);
        ciRelevance.setRows(rows);
        ciRelevance.setPage(page);
        return SUCCESS;
    }

    /**
     * ci relevance save
     * @return String
     */
    public String save() {
        ciRelevanceService.configureItemRelevanceAdd(ciRelevanceDTO);
        return SUCCESS;
    }
    
    /**
     * update ciRelevance
     * @return String
     */
    public String update() {
        ciRelevanceService.configureItemRelevanceUpdate(ciRelevanceDTO);
        return SUCCESS;
    }
    
    /**
     * delete ciRelevance
     * @return String
     */
    public String delete() {
        ciRelevanceService.configureItemRelevanceDelete(ids);
        return SUCCESS;
    }
    
    /**
     * ciReleTree
     * @return String
     */
    public String ciReleTree(){
    	ciReleTreeDto=ciRelevanceService.ciReleTree(ciRelevanceId,relateForward,relateBack);
    	return "ciResultTreeDto";
    }
    
    /**
     * isCIRelevance
     * @return String
     */
    public String isCIRelevance(){
    	releResult=ciRelevanceService.isCIRelevance(ciRelevanceDTO);
    	return "result";
    }
    
    /**
     * 根据ID获取关联配置项
     * @return String
     */
    public String findCiRelevanceById(){
    	ciRelevanceDTO=ciRelevanceService.findCiRelevanceById(ciRelevanceId);
    	return "ciRelevanceDTO";
    };
    
}