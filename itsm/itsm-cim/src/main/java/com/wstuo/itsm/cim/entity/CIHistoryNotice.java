package com.wstuo.itsm.cim.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.wstuo.common.entity.BaseEntity;

/**
 * 配置项历史通知表
 * @author WSTUO_QXY
 *
 */
@SuppressWarnings({ "serial", "rawtypes" })
@Entity
@Table(name="cihistorynotice")
public class CIHistoryNotice extends BaseEntity{
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
	private Long id;
	@Column(nullable=false)
	private Long ciId;//配置项ID
	@Column(nullable=true)
	private Long type;//通知类型,预警(0)、生命周期到期(1)、保修期到期(2)
	@Column(nullable=true)
	private Date arrivalDate;//到货日期
	@Column(nullable=true)
	private Date warningDate;//预警时间
	@Column(nullable=true)
	private Integer lifeCycle;//生命周期
	@Column(nullable=true)
	private Integer warranty;//保修期
	@Column(nullable=true)
	private String title;//标题
	@Column(nullable=true)
	private String content;//内容
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getCiId() {
		return ciId;
	}
	public void setCiId(Long ciId) {
		this.ciId = ciId;
	}
	public Long getType() {
		return type;
	}
	public void setType(Long type) {
		this.type = type;
	}
	public Date getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public Date getWarningDate() {
		return warningDate;
	}
	public void setWarningDate(Date warningDate) {
		this.warningDate = warningDate;
	}

	public Integer getLifeCycle() {
		return lifeCycle;
	}
	public void setLifeCycle(Integer lifeCycle) {
		this.lifeCycle = lifeCycle;
	}
	public Integer getWarranty() {
		return warranty;
	}
	public void setWarranty(Integer warranty) {
		this.warranty = warranty;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
}
