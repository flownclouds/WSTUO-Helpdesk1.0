package com.wstuo.itsm.cim.dao;

import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.itsm.cim.dto.CIDTO;
import com.wstuo.itsm.cim.entity.BehaviorModification;

public interface IBehaviorModificationDAO  extends IEntityDAO<BehaviorModification>{
	/**
	 * 修改行为
	 * @param ciid
	 * @return
	 */
	PageDTO findReleasePager(CIDTO cidto);
}