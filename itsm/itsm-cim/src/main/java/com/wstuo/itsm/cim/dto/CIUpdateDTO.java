package com.wstuo.itsm.cim.dto;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.json.annotations.JSON;

import com.wstuo.common.config.attachment.entity.Attachment;
import com.wstuo.common.dto.BaseDTO;
import com.wstuo.common.util.TimeUtils;

/**
 * 配置项更新信息DTO
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class CIUpdateDTO extends BaseDTO{
	    /**
	     * Configuration item sheet auto identity column
	     */
	    private Long ciId;

	    /**
	     * Configuration item NO.
	     */
	    private String cino;

	    /**
	     * Configuration item name
	     */
	    private String ciname;

	    /**
	     * Product Type
	     */
	    private String model;

	    /**
	     * serial number
	     */
	    private String serialNumber;

	    /**
	     * barcode
	     */
	    private String barcode;

	    /**
	     * buy time
	     */
	    private Date buyDate;

	    /**
	     * Arrival time
	     */
	    private Date arrivalDate;

	    /**
	     * Purchase Order No.
	     */
	    private String poNo;

	    /**
	     * Warning time
	     */
	    private Date warningDate;

	    /**
	     * provider id
	     */
	    private Long providerId;

	    /**
	     * providers name
	     */
	    private String providerName;

	    /**
	     * Life Cycle
	     */
	    private Integer lifeCycle;

	    /**
	     * Warranty
	     */
	    private Integer warranty;

	    /**
	     * User
	     */
	    private String userName;

	    /**
	     * Owner
	     */
	    private String owner;

	    /**
	     * status id
	     */
	    private Long statusId;

	    /**
	     * status
	     */
	    private String status;

	    /**
	     * Location id
	     */
	    private Long locId;

	    /**
	     * Location
	     */
	    private String loc;

	    /**
	     * brand id
	     */
	    private Long brandId;

	    /**
	     * brand Name
	     */
	    private String brandName;

	    /**
	     * category No.
	     */
	    private Long categoryNo;

	    /***
	     * category Name
	     */
	    private String categoryName;
	    
	    private Long eavNo;
	    
	    private Map<String,String> attrVals =new HashMap<String, String>(); 
	    
	    /**
	     * 附件list
	     */
	    private List<Attachment> attachments;
	    
	    /**
	     * 附件字符串
	     */
	    private String attachmentStr;
	    
	    //20110709-QXY
	    private String department;//部门
	    private String CDI;//CDI
	    private String workNumber;//Work Number
	    private String project;//Project
	    private String sourceUnits;//来源单位
	    private Boolean financeCorrespond=false;//与财务对应
	    private Double assetsOriginalValue; //资产原值
	    private Date wasteTime;//报废时间
	    private Date borrowedTime;//借出时间
	    private String originalUser;//原使用者
	    private Date recoverTime;//回收时间
	    private Date expectedRecoverTime;//预计回收时间
	    private String usePermissions;//使用权限
	    private Byte dataFlag;
	    private String companyName;//客户名称
	    private Long companyNo;//客户No
	    private String lastUpdater;//最后修改人
	    private String computerName;//计算机名
	    private Long [] aids;
	    
	    

		public Long[] getAids() {
			return aids;
		}

		public void setAids(Long[] aids) {
			this.aids = aids;
		}

		public String getComputerName() {
			if(computerName==null)
				computerName="";
			return computerName;
		}

		public void setComputerName(String computerName) {
			
			this.computerName = computerName;
		}

		public String getBrandName(  )
	    {
			if(brandName==null)
				brandName="";
	        return brandName;
	    }

	    public Long getCategoryNo(  )
	    {
	        return categoryNo;
	    }

	    public void setCategoryNo( Long categoryNo )
	    {
	        this.categoryNo = categoryNo;
	    }

	    public String getCategoryName(  )
	    {
	    	if(categoryName==null)
	    		categoryName="";
	        return categoryName;
	    }

	    public void setCategoryName( String categoryName )
	    {
	        this.categoryName = categoryName;
	    }

	    public void setBrandName( String brandName )
	    {
	        this.brandName = brandName;
	    }

	    public Long getStatusId(  )
	    {
	        return statusId;
	    }

	    public void setStatusId( Long statusId )
	    {
	        this.statusId = statusId;
	    }

	    public Long getLocId(  )
	    {
	        return locId;
	    }

	    public void setLocId( Long locId )
	    {
	        this.locId = locId;
	    }

	    public Long getBrandId(  )
	    {
	        return brandId;
	    }

	    public void setBrandId( Long brandId )
	    {
	        this.brandId = brandId;
	    }

	    public String getLoc(  )
	    {
	    	if(loc==null)
	    		loc="";
	        return loc;
	    }

	    public void setLoc( String loc )
	    {
	    	
	        this.loc = loc;
	    }

	    public Long getCiId(  )
	    {
	        return ciId;
	    }

	    public void setCiId( Long ciId )
	    {
	        this.ciId = ciId;
	    }

	    public String getCino(  )
	    {
	    	if(cino==null)
	    		cino="";
	        return cino;
	    }

	    public void setCino( String cino )
	    {
	        this.cino = cino;
	    }

	    public String getModel(  )
	    {
	    	if(model==null)
	    		model="";
	        return model;
	    }

	    public void setModel( String model )
	    {
	        this.model = model;
	    }

	    public String getSerialNumber(  )
	    {
	    	if(serialNumber==null)
	    		serialNumber="";
	        return serialNumber;
	    }

	    public void setSerialNumber( String serialNumber )
	    {
	        this.serialNumber = serialNumber;
	    }

	    public String getBarcode(  )
	    {
	    	if(barcode==null)
	    		barcode="";
	        return barcode;
	    }

	    public void setBarcode( String barcode )
	    {
	        this.barcode = barcode;
	    }

	    @JSON(format=TimeUtils.DATE_PATTERN)
	    public Date getBuyDate(  )
	    {
	        return buyDate;
	    }

	    public void setBuyDate( Date buyDate )
	    {
	        this.buyDate = buyDate;
	    }

	    @JSON(format=TimeUtils.DATE_PATTERN)
	    public Date getArrivalDate(  )
	    {
	        return arrivalDate;
	    }

	    public void setArrivalDate( Date arrivalDate )
	    {
	        this.arrivalDate = arrivalDate;
	    }

	    public String getPoNo(  )
	    {
	    	if(poNo==null)
	    		poNo="";
	        return poNo;
	    }

	    public void setPoNo( String poNo )
	    {
	        this.poNo = poNo;
	    }

	    @JSON(format=TimeUtils.DATE_PATTERN)
	    public Date getWarningDate(  )
	    {
	        return warningDate;
	    }

	    public void setWarningDate( Date warningDate )
	    {
	        this.warningDate = warningDate;
	    }

	    public String getStatus(  )
	    {
	    	if(status==null)
	    		status="";
	        return status;
	    }

	    public void setStatus( String status )
	    {
	        this.status = status;
	    }

	    public Long getProviderId(  )
	    {
	        return providerId;
	    }

	    public void setProviderId( Long providerId )
	    {
	        this.providerId = providerId;
	    }

	    public String getProviderName(  )
	    {
	    	if(providerName==null)
	    		providerName="";
	        return providerName;
	    }

	    public void setProviderName( String providerName )
	    {
	        this.providerName = providerName;
	    }

	    public Integer getLifeCycle(  )
	    {
	        return lifeCycle;
	    }

	    public void setLifeCycle( Integer lifeCycle )
	    {
	        this.lifeCycle = lifeCycle;
	    }

	    public Integer getWarranty(  )
	    {
	        return warranty;
	    }

	    public void setWarranty( Integer warranty )
	    {
	        this.warranty = warranty;
	    }

	    public String getUserName(  )
	    {
	    	if(userName==null)
	    		userName="";
	        return userName;
	    }

	    public void setUserName( String userName )
	    {
	        this.userName = userName;
	    }

	    public String getOwner(  )
	    {
	    	if(owner==null)
	    		owner="";
	        return owner;
	    }

	    public void setOwner( String owner )
	    {
	        this.owner = owner;
	    }

	    public String getCiname(  )
	    {
	    	if(ciname==null)
	    		ciname="";
	        return ciname;
	    }

	    public void setCiname( String ciname )
	    {
	        this.ciname = ciname;
	    }

		public Long getEavNo() {
			
			return eavNo;
		}

		public void setEavNo(Long eavNo) {
			this.eavNo = eavNo;
		}

		public Map<String, String> getAttrVals() {
			return attrVals;
		}

		public void setAttrVals(Map<String, String> attrVals) {
			this.attrVals = attrVals;
		}

		public List<Attachment> getAttachments() {
			return attachments;
		}

		public void setAttachments(List<Attachment> attachments) {
			this.attachments = attachments;
		}

		public String getAttachmentStr() {
			if(attachmentStr==null)
				attachmentStr="";
			return attachmentStr;
		}

		public void setAttachmentStr(String attachmentStr) {
			this.attachmentStr = attachmentStr;
		}

		public String getDepartment() {
			if(department==null)
				department="";
			return department;
		}

		public void setDepartment(String department) {
			this.department = department;
		}

		public String getCDI() {
			if(CDI==null)
				CDI="";
			return CDI;
		}

		public void setCDI(String cDI) {
			CDI = cDI;
		}

		public String getWorkNumber() {
			if(workNumber==null)
				workNumber="";
			return workNumber;
		}

		public void setWorkNumber(String workNumber) {
			this.workNumber = workNumber;
		}

		public String getProject() {
			if(project==null)
				project="";
			return project;
		}

		public void setProject(String project) {
			this.project = project;
		}

		public String getSourceUnits() {
			if(sourceUnits==null)
				sourceUnits="";
			return sourceUnits;
		}

		public void setSourceUnits(String sourceUnits) {
			this.sourceUnits = sourceUnits;
		}

		public Boolean getFinanceCorrespond() {
			return financeCorrespond;
		}

		public void setFinanceCorrespond(Boolean financeCorrespond) {
			this.financeCorrespond = financeCorrespond;
		}

		public Double getAssetsOriginalValue() {
			return assetsOriginalValue;
		}

		public void setAssetsOriginalValue(Double assetsOriginalValue) {
			this.assetsOriginalValue = assetsOriginalValue;
		}

		public Date getWasteTime() {
			return wasteTime;
		}

		public void setWasteTime(Date wasteTime) {
			this.wasteTime = wasteTime;
		}

		public Date getBorrowedTime() {
			return borrowedTime;
		}

		public void setBorrowedTime(Date borrowedTime) {
			this.borrowedTime = borrowedTime;
		}

		public String getOriginalUser() {
			if(originalUser==null)
				originalUser="";
			return originalUser;
		}

		public void setOriginalUser(String originalUser) {
			this.originalUser = originalUser;
		}

		public Date getRecoverTime() {
			return recoverTime;
		}

		public void setRecoverTime(Date recoverTime) {
			this.recoverTime = recoverTime;
		}

		public Date getExpectedRecoverTime() {
			return expectedRecoverTime;
		}

		public void setExpectedRecoverTime(Date expectedRecoverTime) {
			this.expectedRecoverTime = expectedRecoverTime;
		}

		public String getUsePermissions() {
			if(usePermissions==null)
				usePermissions="";
			return usePermissions;
		}

		public void setUsePermissions(String usePermissions) {
			this.usePermissions = usePermissions;
		}

		public Byte getDataFlag() {
			return dataFlag;
		}

		public void setDataFlag(Byte dataFlag) {
			this.dataFlag = dataFlag;
		}

		public String getCompanyName() {
			if(companyName==null)
				companyName="";
			return companyName;
		}

		public void setCompanyName(String companyName) {
			this.companyName = companyName;
		}

		public Long getCompanyNo() {
			return companyNo;
		}

		public void setCompanyNo(Long companyNo) {
			this.companyNo = companyNo;
		}

		public CIUpdateDTO(){
			
		}
		
		public String getLastUpdater() {
			return lastUpdater;
		}

		public void setLastUpdater(String lastUpdater) {
			this.lastUpdater = lastUpdater;
		}

		public CIUpdateDTO(Long ciId, String cino, String ciname, String model,
				String serialNumber, String barcode, Date buyDate,
				Date arrivalDate, String poNo, Date warningDate, Long providerId,
				String providerName, Integer lifeCycle, Integer warranty,
				String userName, String owner, Long statusId, String status,
				Long locId, String loc, Long brandId, String brandName,
				Long categoryNo, String categoryName, Long eavNo,
				Map<String, String> attrVals, List<Attachment> attachments,
				String attachmentStr,Long companyNo) {
			super();
			this.ciId = ciId;
			this.cino = cino;
			this.ciname = ciname;
			this.model = model;
			this.serialNumber = serialNumber;
			this.barcode = barcode;
			this.buyDate = buyDate;
			this.arrivalDate = arrivalDate;
			this.poNo = poNo;
			this.warningDate = warningDate;
			this.providerId = providerId;
			this.providerName = providerName;
			this.lifeCycle = lifeCycle;
			this.warranty = warranty;
			this.userName = userName;
			this.owner = owner;
			this.statusId = statusId;
			this.status = status;
			this.locId = locId;
			this.loc = loc;
			this.brandId = brandId;
			this.brandName = brandName;
			this.categoryNo = categoryNo;
			this.categoryName = categoryName;
			this.eavNo = eavNo;
			this.attrVals = attrVals;
			this.attachments = attachments;
			this.attachmentStr = attachmentStr;
			this.companyNo=companyNo;
			
		}

}
