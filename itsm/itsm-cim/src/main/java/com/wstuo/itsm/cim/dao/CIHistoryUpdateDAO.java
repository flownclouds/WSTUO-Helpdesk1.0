package com.wstuo.itsm.cim.dao;


import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.itsm.cim.entity.CIHistoryUpdate;
/**
 * 配置项历史记录
 * @author QXY
 *
 */
public class CIHistoryUpdateDAO  extends BaseDAOImplHibernate<CIHistoryUpdate> implements ICIHistoryUpdateDAO{
	/**
	 * 查询配置项历史更新记录
	 */
	public PageDTO findReleasePager(Long ciid) {
        final DetachedCriteria dc = DetachedCriteria.forClass(CIHistoryUpdate.class);
        dc.add(Restrictions.eq("ciId", ciid));
        dc.addOrder(Order.desc("updateDate"));
        return super.findPageByCriteria(dc, 0, 10000);
	}
}
