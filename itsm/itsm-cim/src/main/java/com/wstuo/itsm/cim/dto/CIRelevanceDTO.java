package com.wstuo.itsm.cim.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 *
 * 关联配置项基本属性DTO类
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class CIRelevanceDTO extends BaseDTO{
    private Long relevanceId;
    private String relevanceDesc;
    private Long ciRelevanceId;
    private String ciRelevanceName;
    private Long unCiRelevanceId;
    private String unCiRelevanceName;
    private Long ciRelationTypeNo;//配置项CI的关系类型
    private String ciRelationTypeName;//配置项CI的关系类型
    private String ciRelationTypeNameColor;//配置项CI的关系类型背景色
    
    public String getCiRelationTypeNameColor() {
		return ciRelationTypeNameColor;
	}

	public void setCiRelationTypeNameColor(String ciRelationTypeNameColor) {
		this.ciRelationTypeNameColor = ciRelationTypeNameColor;
	}

	public CIRelevanceDTO()
    {
        super();
    }

    public Long getRelevanceId()
    {
        return relevanceId;
    }

    public void setRelevanceId( Long relevanceId )
    {
        this.relevanceId = relevanceId;
    }

    public String getRelevanceDesc(  )
    {
        return relevanceDesc;
    }

    public void setRelevanceDesc( String relevanceDesc )
    {
        this.relevanceDesc = relevanceDesc;
    }

    public Long getCiRelevanceId(  )
    {
        return ciRelevanceId;
    }

    public void setCiRelevanceId( Long ciRelevanceId )
    {
        this.ciRelevanceId = ciRelevanceId;
    }

    public String getCiRelevanceName(  )
    {
        return ciRelevanceName;
    }

    public void setCiRelevanceName( String ciRelevanceName )
    {
        this.ciRelevanceName = ciRelevanceName;
    }

    public Long getUnCiRelevanceId(  )
    {
        return unCiRelevanceId;
    }

    public void setUnCiRelevanceId( Long unCiRelevanceId )
    {
        this.unCiRelevanceId = unCiRelevanceId;
    }

    public String getUnCiRelevanceName(  )
    {
        return unCiRelevanceName;
    }

    public void setUnCiRelevanceName( String unCiRelevanceName )
    {
        this.unCiRelevanceName = unCiRelevanceName;
    }

	public Long getCiRelationTypeNo() {
		return ciRelationTypeNo;
	}

	public void setCiRelationTypeNo(Long ciRelationTypeNo) {
		this.ciRelationTypeNo = ciRelationTypeNo;
	}

	public String getCiRelationTypeName() {
		return ciRelationTypeName;
	}

	public void setCiRelationTypeName(String ciRelationTypeName) {
		this.ciRelationTypeName = ciRelationTypeName;
	}
    
}
