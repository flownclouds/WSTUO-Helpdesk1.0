package com.wstuo.itsm.cim.dao;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.itsm.cim.dto.CIHistoryNoticeQueryDTO;
import com.wstuo.itsm.cim.entity.CIHistoryNotice;

/**
 * 配置项历史通知DAO CLASS
 * @author WSTUO_QXY
 *
 */
public class CIHistoryNoticeDAO extends BaseDAOImplHibernate<CIHistoryNotice> implements ICIHistoryNoticeDAO {
	/**
	 * 分页查询
	 * @param queryDTO
	 * @return String
	 */
	public Integer findPagerCiHistoryNotice(CIHistoryNoticeQueryDTO queryDTO){
		final DetachedCriteria dc =DetachedCriteria.forClass(CIHistoryNotice.class);
		if(queryDTO!=null){
			if(queryDTO.getCiId()!=null){
				dc.add(Restrictions.eq("ciId", queryDTO.getCiId()));
			}
			if(queryDTO.getType()!=null){
				dc.add(Restrictions.eq("type", queryDTO.getType()));
			}
			if(queryDTO.getArrivalDate()!=null){
				dc.add(Restrictions.eq("arrivalDate", queryDTO.getArrivalDate()));
			}
			if(queryDTO.getLifeCycle()!=null){
				dc.add(Restrictions.eq("lifeCycle", queryDTO.getLifeCycle()));
			}
			if(queryDTO.getWarranty()!=null){
				dc.add(Restrictions.eq("warranty", queryDTO.getWarranty()));
			}
			if(queryDTO.getWarningDate()!=null){
				dc.add(Restrictions.eq("warningDate", queryDTO.getWarningDate()));
			}
		}
		PageDTO page=super.findPageByCriteria(dc, 0, 10);
		return page.getTotalSize();
	}
}
