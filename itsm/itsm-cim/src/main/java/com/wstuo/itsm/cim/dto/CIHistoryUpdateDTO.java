package com.wstuo.itsm.cim.dto;

import java.util.Date;

import javax.persistence.Column;

import com.wstuo.common.dto.BaseDTO;

/**
 * 配置项历史记录更新
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class CIHistoryUpdateDTO extends BaseDTO{
	private Long id;       			//ID
	
	private String cihistoryInfo;   //更新内容
	private String userName;   //操作人
	private Date updateDate;   //更新时间
	private Long revision;  //版本

	private CIDetailDTO ciDTO;
	@Column(nullable=true)
	private Long beId;//修改行为；
	
	public Long getBeId() {
		return beId;
	}
	public void setBeId(Long beId) {
		this.beId = beId;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCihistoryInfo() {
		return cihistoryInfo;
	}
	public void setCihistoryInfo(String cihistoryInfo) {
		this.cihistoryInfo = cihistoryInfo;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public Long getRevision() {
		return revision;
	}
	public void setRevision(Long revision) {
		this.revision = revision;
	}
	public CIDetailDTO getCiDTO() {
		return ciDTO;
	}
	public void setCiDTO(CIDetailDTO ciDTO) {
		this.ciDTO = ciDTO;
	}
	
	

}
