package com.wstuo.itsm.cim.dto;

import com.wstuo.common.dto.BaseDTO;
/**
 * 硬件信息DTO
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class HardwareSimpleDTO  extends BaseDTO{
	private Long hardwareId;
	//计算机系统
	private String computerSystem_name;//计算机名
	private String computerSystem_model;//计算机型号
	private String computerSystem_domain;//所在域
	private String computerSystem_userName;//用户名 
	//操作系统
	private String operatingSystem_serialNumber;//序列号
	private String operatingSystem_caption;//系统名称
	private String operatingSystem_version;//系统版本
	private String operatingSystem_osArchitecture;//系统框架
	private String operatingSystem_csdVersion;//补丁版本
	private String operatingSystem_installDate;//安装时间
	private String operatingSystem_lastBootUpTime;//最后启动时间
	private String operatingSystem_totalVisibleMemorySize;//总的物理内存
	private String operatingSystem_totalVirtualMemorySize;//总的虚拟内存
	//主板
	private String baseBoard_name;//主板名称
	private String baseBoard_serialNumber;//主板序列号
	private String baseBoard_manufacturer;//主板制造商
	//BIOS
	private String bios_serialNumber;//BIOS序列号
	private String bios_name;//BIOS序列号
	private String bios_manufacturer;//BIOS制造商
	private String bios_version;//BIOS版本
	private String bios_releaseDate;//BIOS发布时间
	//处理器
	private String processor_name;//CPU名称
	private String processor_maxClockSpeed;//CPU速度
	private String processor_l2CacheSize;//CPU二级缓存
	private String processor_l3CacheSize;//CPU名称三级缓存
	private String processor_level;//CPU分级
	//网卡信息
	private String netWork_ip;//IP
	private String netWork_mac;//MAC
	private String netWork_name;//网卡
	private String netWork_dhcp;//DHCP
	private String netWork_dhcpServer;//DHCP服务器
	//监视器
	private String desktopMonitor_name;
	private String desktopMonitor_screenHeight;//显示器高度
	private String desktopMonitor_screenWidth;//显示器宽度
	private String desktopMonitor_monitorManufacturer;//显示器制造商
	private String desktopMonitor_serialNumber;//序列号
	//鼠标
	private String pointingDevice_Name;//名称
	private String pointingDevice_description;//描述
	private String pointingDevice_serialNumber;//序列号
	private String pointingDevice_manufacturer;//制造商
	//键盘
	private String keyboard_Name;//名称
	private String keyboard_description;//描述
	private String keyboard_numberOfFunctionKeys;//功能键
	private String keyboard_serialNumber;//序列号
	private String keyboard_manufacturer;//制造商
	public Long getHardwareId() {
		return hardwareId;
	}
	public void setHardwareId(Long hardwareId) {
		this.hardwareId = hardwareId;
	}
	public String getComputerSystem_name() {
		return computerSystem_name;
	}
	public void setComputerSystem_name(String computerSystem_name) {
		this.computerSystem_name = computerSystem_name;
	}
	public String getComputerSystem_model() {
		return computerSystem_model;
	}
	public void setComputerSystem_model(String computerSystem_model) {
		this.computerSystem_model = computerSystem_model;
	}
	public String getComputerSystem_domain() {
		return computerSystem_domain;
	}
	public void setComputerSystem_domain(String computerSystem_domain) {
		this.computerSystem_domain = computerSystem_domain;
	}
	public String getComputerSystem_userName() {
		return computerSystem_userName;
	}
	public void setComputerSystem_userName(String computerSystem_userName) {
		this.computerSystem_userName = computerSystem_userName;
	}
	public String getOperatingSystem_serialNumber() {
		return operatingSystem_serialNumber;
	}
	public void setOperatingSystem_serialNumber(String operatingSystem_serialNumber) {
		this.operatingSystem_serialNumber = operatingSystem_serialNumber;
	}
	public String getOperatingSystem_caption() {
		return operatingSystem_caption;
	}
	public void setOperatingSystem_caption(String operatingSystem_caption) {
		this.operatingSystem_caption = operatingSystem_caption;
	}
	public String getOperatingSystem_version() {
		return operatingSystem_version;
	}
	public void setOperatingSystem_version(String operatingSystem_version) {
		this.operatingSystem_version = operatingSystem_version;
	}
	public String getOperatingSystem_osArchitecture() {
		return operatingSystem_osArchitecture;
	}
	public void setOperatingSystem_osArchitecture(
			String operatingSystem_osArchitecture) {
		this.operatingSystem_osArchitecture = operatingSystem_osArchitecture;
	}
	public String getOperatingSystem_csdVersion() {
		return operatingSystem_csdVersion;
	}
	public void setOperatingSystem_csdVersion(String operatingSystem_csdVersion) {
		this.operatingSystem_csdVersion = operatingSystem_csdVersion;
	}
	public String getOperatingSystem_installDate() {
		return operatingSystem_installDate;
	}
	public void setOperatingSystem_installDate(String operatingSystem_installDate) {
		this.operatingSystem_installDate = operatingSystem_installDate;
	}
	public String getOperatingSystem_lastBootUpTime() {
		return operatingSystem_lastBootUpTime;
	}
	public void setOperatingSystem_lastBootUpTime(
			String operatingSystem_lastBootUpTime) {
		this.operatingSystem_lastBootUpTime = operatingSystem_lastBootUpTime;
	}
	public String getOperatingSystem_totalVisibleMemorySize() {
		return operatingSystem_totalVisibleMemorySize;
	}
	public void setOperatingSystem_totalVisibleMemorySize(
			String operatingSystem_totalVisibleMemorySize) {
		this.operatingSystem_totalVisibleMemorySize = operatingSystem_totalVisibleMemorySize;
	}
	public String getOperatingSystem_totalVirtualMemorySize() {
		return operatingSystem_totalVirtualMemorySize;
	}
	public void setOperatingSystem_totalVirtualMemorySize(
			String operatingSystem_totalVirtualMemorySize) {
		this.operatingSystem_totalVirtualMemorySize = operatingSystem_totalVirtualMemorySize;
	}
	public String getBaseBoard_name() {
		return baseBoard_name;
	}
	public void setBaseBoard_name(String baseBoard_name) {
		this.baseBoard_name = baseBoard_name;
	}
	public String getBaseBoard_serialNumber() {
		return baseBoard_serialNumber;
	}
	public void setBaseBoard_serialNumber(String baseBoard_serialNumber) {
		this.baseBoard_serialNumber = baseBoard_serialNumber;
	}
	public String getBaseBoard_manufacturer() {
		return baseBoard_manufacturer;
	}
	public void setBaseBoard_manufacturer(String baseBoard_manufacturer) {
		this.baseBoard_manufacturer = baseBoard_manufacturer;
	}
	public String getBios_serialNumber() {
		return bios_serialNumber;
	}
	public void setBios_serialNumber(String bios_serialNumber) {
		this.bios_serialNumber = bios_serialNumber;
	}
	public String getBios_name() {
		return bios_name;
	}
	public void setBios_name(String bios_name) {
		this.bios_name = bios_name;
	}
	public String getBios_manufacturer() {
		return bios_manufacturer;
	}
	public void setBios_manufacturer(String bios_manufacturer) {
		this.bios_manufacturer = bios_manufacturer;
	}
	public String getBios_version() {
		return bios_version;
	}
	public void setBios_version(String bios_version) {
		this.bios_version = bios_version;
	}
	public String getBios_releaseDate() {
		return bios_releaseDate;
	}
	public void setBios_releaseDate(String bios_releaseDate) {
		this.bios_releaseDate = bios_releaseDate;
	}
	public String getProcessor_name() {
		return processor_name;
	}
	public void setProcessor_name(String processor_name) {
		this.processor_name = processor_name;
	}
	public String getProcessor_maxClockSpeed() {
		return processor_maxClockSpeed;
	}
	public void setProcessor_maxClockSpeed(String processor_maxClockSpeed) {
		this.processor_maxClockSpeed = processor_maxClockSpeed;
	}
	public String getProcessor_l2CacheSize() {
		return processor_l2CacheSize;
	}
	public void setProcessor_l2CacheSize(String processor_l2CacheSize) {
		this.processor_l2CacheSize = processor_l2CacheSize;
	}
	public String getProcessor_l3CacheSize() {
		return processor_l3CacheSize;
	}
	public void setProcessor_l3CacheSize(String processor_l3CacheSize) {
		this.processor_l3CacheSize = processor_l3CacheSize;
	}
	public String getProcessor_level() {
		return processor_level;
	}
	public void setProcessor_level(String processor_level) {
		this.processor_level = processor_level;
	}
	public String getNetWork_ip() {
		return netWork_ip;
	}
	public void setNetWork_ip(String netWork_ip) {
		this.netWork_ip = netWork_ip;
	}
	public String getNetWork_mac() {
		return netWork_mac;
	}
	public void setNetWork_mac(String netWork_mac) {
		this.netWork_mac = netWork_mac;
	}
	public String getNetWork_name() {
		return netWork_name;
	}
	public void setNetWork_name(String netWork_name) {
		this.netWork_name = netWork_name;
	}
	public String getNetWork_dhcp() {
		return netWork_dhcp;
	}
	public void setNetWork_dhcp(String netWork_dhcp) {
		this.netWork_dhcp = netWork_dhcp;
	}
	public String getNetWork_dhcpServer() {
		return netWork_dhcpServer;
	}
	public void setNetWork_dhcpServer(String netWork_dhcpServer) {
		this.netWork_dhcpServer = netWork_dhcpServer;
	}
	public String getDesktopMonitor_name() {
		return desktopMonitor_name;
	}
	public void setDesktopMonitor_name(String desktopMonitor_name) {
		this.desktopMonitor_name = desktopMonitor_name;
	}
	public String getDesktopMonitor_screenHeight() {
		return desktopMonitor_screenHeight;
	}
	public void setDesktopMonitor_screenHeight(String desktopMonitor_screenHeight) {
		this.desktopMonitor_screenHeight = desktopMonitor_screenHeight;
	}
	public String getDesktopMonitor_screenWidth() {
		return desktopMonitor_screenWidth;
	}
	public void setDesktopMonitor_screenWidth(String desktopMonitor_screenWidth) {
		this.desktopMonitor_screenWidth = desktopMonitor_screenWidth;
	}
	public String getDesktopMonitor_monitorManufacturer() {
		return desktopMonitor_monitorManufacturer;
	}
	public void setDesktopMonitor_monitorManufacturer(
			String desktopMonitor_monitorManufacturer) {
		this.desktopMonitor_monitorManufacturer = desktopMonitor_monitorManufacturer;
	}
	public String getDesktopMonitor_serialNumber() {
		return desktopMonitor_serialNumber;
	}
	public void setDesktopMonitor_serialNumber(String desktopMonitor_serialNumber) {
		this.desktopMonitor_serialNumber = desktopMonitor_serialNumber;
	}
	public String getPointingDevice_Name() {
		return pointingDevice_Name;
	}
	public void setPointingDevice_Name(String pointingDevice_Name) {
		this.pointingDevice_Name = pointingDevice_Name;
	}
	public String getPointingDevice_description() {
		return pointingDevice_description;
	}
	public void setPointingDevice_description(String pointingDevice_description) {
		this.pointingDevice_description = pointingDevice_description;
	}
	public String getPointingDevice_serialNumber() {
		return pointingDevice_serialNumber;
	}
	public void setPointingDevice_serialNumber(String pointingDevice_serialNumber) {
		this.pointingDevice_serialNumber = pointingDevice_serialNumber;
	}
	public String getPointingDevice_manufacturer() {
		return pointingDevice_manufacturer;
	}
	public void setPointingDevice_manufacturer(String pointingDevice_manufacturer) {
		this.pointingDevice_manufacturer = pointingDevice_manufacturer;
	}
	public String getKeyboard_Name() {
		return keyboard_Name;
	}
	public void setKeyboard_Name(String keyboard_Name) {
		this.keyboard_Name = keyboard_Name;
	}
	public String getKeyboard_description() {
		return keyboard_description;
	}
	public void setKeyboard_description(String keyboard_description) {
		this.keyboard_description = keyboard_description;
	}
	public String getKeyboard_numberOfFunctionKeys() {
		return keyboard_numberOfFunctionKeys;
	}
	public void setKeyboard_numberOfFunctionKeys(
			String keyboard_numberOfFunctionKeys) {
		this.keyboard_numberOfFunctionKeys = keyboard_numberOfFunctionKeys;
	}
	public String getKeyboard_serialNumber() {
		return keyboard_serialNumber;
	}
	public void setKeyboard_serialNumber(String keyboard_serialNumber) {
		this.keyboard_serialNumber = keyboard_serialNumber;
	}
	public String getKeyboard_manufacturer() {
		return keyboard_manufacturer;
	}
	public void setKeyboard_manufacturer(String keyboard_manufacturer) {
		this.keyboard_manufacturer = keyboard_manufacturer;
	}
	@Override
	public String toString() {
	    
	    StringBuffer result = new StringBuffer();
	    result.append("HardwareSimpleDTO ")
        .append("[")
        .append("hardwareId=").append(hardwareId).append(", ")
        .append("computerSystem_name=").append(computerSystem_name).append(", ")
        .append("computerSystem_model=").append(computerSystem_model).append(", ")
        .append("computerSystem_domain=").append(computerSystem_domain).append(", ")
        .append("computerSystem_userName=").append(computerSystem_userName).append(", ")
        .append("operatingSystem_serialNumber=").append(operatingSystem_serialNumber).append(", ")
        .append("operatingSystem_caption=").append(operatingSystem_caption).append(", ")
        .append("operatingSystem_version=").append(operatingSystem_version).append(", ")
        .append("operatingSystem_osArchitecture=").append(operatingSystem_osArchitecture).append(", ")
        .append("operatingSystem_csdVersion=").append(operatingSystem_csdVersion).append(", ")
        .append("operatingSystem_installDate=").append(operatingSystem_installDate).append(", ")
        .append("operatingSystem_lastBootUpTime=").append(operatingSystem_lastBootUpTime).append(", ")
        .append("operatingSystem_totalVisibleMemorySize=").append(operatingSystem_totalVisibleMemorySize).append(", ")
        .append("operatingSystem_totalVirtualMemorySize=").append(operatingSystem_totalVirtualMemorySize).append(", ")
        .append("baseBoard_name=").append(baseBoard_name).append(", ")
        .append("baseBoard_serialNumber=").append(baseBoard_serialNumber).append(", ")
        .append("baseBoard_manufacturer=").append(baseBoard_manufacturer).append(", ")
        .append("bios_serialNumber=").append(bios_serialNumber).append(", ")
        .append("bios_name=").append(bios_name).append(", ")
        .append("bios_manufacturer=").append(bios_manufacturer).append(", ")
        .append("bios_version=").append(bios_version).append(", ")
        .append("bios_releaseDate=").append(bios_releaseDate).append(", ")
        .append("processor_name=").append(processor_name).append(", ")
        .append("processor_maxClockSpeed=").append(processor_maxClockSpeed).append(", ")
        .append("processor_l2CacheSize=").append(processor_l2CacheSize).append(", ")
        .append("processor_l3CacheSize=").append(processor_l3CacheSize).append(", ")
        .append("processor_level=").append(processor_level).append(", ")
        .append("netWork_ip=").append(netWork_ip).append(", ")
        .append("netWork_mac=").append(netWork_mac).append(", ")
        .append("netWork_name=").append(netWork_name).append(", ")
        .append("netWork_dhcp=").append(netWork_dhcp).append(", ")
        .append("netWork_dhcpServer=").append(netWork_dhcpServer).append(", ")
        .append("desktopMonitor_name=").append(desktopMonitor_name).append(", ")
        .append("desktopMonitor_screenHeight=").append(desktopMonitor_screenHeight).append(", ")
        .append("desktopMonitor_screenWidth=").append(desktopMonitor_screenWidth).append(", ")
        .append("desktopMonitor_monitorManufacturer=").append(desktopMonitor_monitorManufacturer).append(", ")
        .append("desktopMonitor_serialNumber=").append(desktopMonitor_serialNumber).append(", ")
        .append("pointingDevice_Name=").append(pointingDevice_Name).append(", ")
        .append("pointingDevice_description=").append(pointingDevice_description).append(", ")
        .append("pointingDevice_serialNumber=").append(pointingDevice_serialNumber).append(", ")
        .append("pointingDevice_manufacturer=").append(pointingDevice_manufacturer).append(", ")
        .append("keyboard_Name=").append(keyboard_Name).append(", ")
        .append("keyboard_description=").append(keyboard_description).append(", ")
        .append("keyboard_numberOfFunctionKeys=").append(keyboard_numberOfFunctionKeys).append(", ")
        .append("keyboard_serialNumber=").append(keyboard_serialNumber).append(", ")
        .append("keyboard_manufacturer=").append(keyboard_manufacturer)
        .append("]");
	    
		return result.toString();
	}
	
	
}
