package com.wstuo.itsm.cim.action;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.wstuo.common.config.attachment.entity.Attachment;
import com.wstuo.common.config.authorization.entity.Authorization;
import com.wstuo.common.config.authorization.service.IAuthorizationService;
import com.wstuo.common.config.dictionary.dto.DataDictionaryItemsDTO;
import com.wstuo.common.config.dictionary.service.IDataDictionaryItemsService;
import com.wstuo.common.util.StringUtils;
import com.wstuo.itsm.cim.dto.CIDetailDTO;
import com.wstuo.itsm.cim.dto.HardwareDTO;
import com.wstuo.itsm.cim.dto.PhysicalMemoryDTO;
import com.wstuo.itsm.cim.service.ICIService;

/**
 * 客户端配置项信息管理
 * 
 * @author QXY
 * 
 */
public class ClentCiInfoAction {
	private static final Logger LOGGER = Logger.getLogger(ClentCiInfoAction.class);
	@Autowired
	private ICIService ciService;
	@Autowired
	private IDataDictionaryItemsService dataDictionaryItemsService;
	@Autowired
	private IAuthorizationService authorizationService;
	private DataDictionaryItemsDTO dictionaryItemsDto;
	private CIDetailDTO ciDetailDTO;
	private List<Attachment> attach;
	private long ciEditId;
	private HardwareDTO hardwareDTOs;
	private List<PhysicalMemoryDTO> listPhy = new ArrayList<PhysicalMemoryDTO>();
	private List<PhysicalMemoryDTO> listLogi = new ArrayList<PhysicalMemoryDTO>();
	private List<PhysicalMemoryDTO> listIDE = new ArrayList<PhysicalMemoryDTO>();
	private List<PhysicalMemoryDTO> listUSB = new ArrayList<PhysicalMemoryDTO>();
	private List<PhysicalMemoryDTO> listUSBHub = new ArrayList<PhysicalMemoryDTO>();
	private List<PhysicalMemoryDTO> listCOM = new ArrayList<PhysicalMemoryDTO>();
	private List<PhysicalMemoryDTO> listLPT = new ArrayList<PhysicalMemoryDTO>();
	private List<PhysicalMemoryDTO> listCD = new ArrayList<PhysicalMemoryDTO>();
	private String ciValidate = "";

	/**
	 * 配置项验证
	 * 
	 * @return String
	 */
	public String getCiValidate() {
		return ciValidate;
	}

	public void setCiValidate(String ciValidate) {
		this.ciValidate = ciValidate;
	}

	public List<Attachment> getAttach() {
		return attach;
	}

	public void setAttach(List<Attachment> attach) {
		this.attach = attach;
	}

	public DataDictionaryItemsDTO getDictionaryItemsDto() {
		return dictionaryItemsDto;
	}

	public void setDictionaryItemsDto(DataDictionaryItemsDTO dictionaryItemsDto) {
		this.dictionaryItemsDto = dictionaryItemsDto;
	}

	public CIDetailDTO getCiDetailDTO() {
		return ciDetailDTO;
	}

	public void setCiDetailDTO(CIDetailDTO ciDetailDTO) {
		this.ciDetailDTO = ciDetailDTO;
	}


	public long getCiEditId() {
		return ciEditId;
	}

	public void setCiEditId(long ciEditId) {
		this.ciEditId = ciEditId;
	}

	public HardwareDTO getHardwareDTOs() {
		return hardwareDTOs;
	}

	public void setHardwareDTOs(HardwareDTO hardwareDTOs) {
		this.hardwareDTOs = hardwareDTOs;
	}

	public List<PhysicalMemoryDTO> getListCD() {
		return listCD;
	}

	public void setListCD(List<PhysicalMemoryDTO> listCD) {
		this.listCD = listCD;
	}

	public List<PhysicalMemoryDTO> getListUSBHub() {
		return listUSBHub;
	}

	public void setListUSBHub(List<PhysicalMemoryDTO> listUSBHub) {
		this.listUSBHub = listUSBHub;
	}

	public List<PhysicalMemoryDTO> getListCOM() {
		return listCOM;
	}

	public void setListCOM(List<PhysicalMemoryDTO> listCOM) {
		this.listCOM = listCOM;
	}

	public List<PhysicalMemoryDTO> getListLPT() {
		return listLPT;
	}

	public void setListLPT(List<PhysicalMemoryDTO> listLPT) {
		this.listLPT = listLPT;
	}

	public List<PhysicalMemoryDTO> getListLogi() {
		return listLogi;
	}

	public void setListLogi(List<PhysicalMemoryDTO> listLogi) {
		this.listLogi = listLogi;
	}

	public List<PhysicalMemoryDTO> getListIDE() {
		return listIDE;
	}

	public void setListIDE(List<PhysicalMemoryDTO> listIDE) {
		this.listIDE = listIDE;
	}

	public List<PhysicalMemoryDTO> getListUSB() {
		return listUSB;
	}

	public void setListUSB(List<PhysicalMemoryDTO> listUSB) {
		this.listUSB = listUSB;
	}

	public List<PhysicalMemoryDTO> getListPhy() {
		return listPhy;
	}

	public void setListPhy(List<PhysicalMemoryDTO> listPhy) {
		this.listPhy = listPhy;
	}

	/**
	 * 根据ID获取配置项
	 * 
	 * @return showisItemsInfo
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	public String findByciIdClent() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		Authorization entity = authorizationService.findByAuthType("ciType");
		if (entity != null && entity.getAuthPawd().equals(ciValidate)) {
			ciDetailDTO = ciService.findConfigureItemById(ciEditId);
			hardwareDTOs = ciService.findConfigureItenHardware(ciEditId);
			// 解析JSON硬件信息
			ciHardwareJson();
			// 查询关联的附件
			attach = ciService.findConfigureItemById(ciEditId).getAttachments();

			return "showCiInfoClent";
		} else {

			return "showCiInfoClentError";
		}
	}

	/**
	 * 解析JSON硬件信息
	 */
	public void ciHardwareJson() {
		if(hardwareDTOs!=null){
			// 物理内存
			if (!"null".equals(hardwareDTOs.getPhysicalMemory()) && StringUtils.hasText(hardwareDTOs.getPhysicalMemory())) {
				try {
					JSONArray jsonArray = JSONArray.fromObject(hardwareDTOs.getPhysicalMemory());
					int iSize = jsonArray.size();
					if (iSize > 0) {
						for (int i = 0; i < iSize; i++) {
							JSONObject jsonObj = jsonArray.getJSONObject(i);
							PhysicalMemoryDTO phy = new PhysicalMemoryDTO();
							phy.setpName(jsonObj.optString("Caption"));
							phy.setpInfo(jsonObj.optString("Speed"));
							phy.setpSystem(jsonObj.optString("DeviceLocator"));
							phy.setpAvailable(jsonObj.optString("Capacity"));
							listPhy.add(phy);
						}
					}
				} catch (Exception e) {
					LOGGER.error(e);
				}
			}
			// 逻辑磁盘
			if (!"null".equals(hardwareDTOs.getLogicalDisk()) && StringUtils.hasText(hardwareDTOs.getLogicalDisk())) {
				try {
					JSONArray jsonArray = JSONArray.fromObject(hardwareDTOs.getLogicalDisk());
					int iSize = jsonArray.size();
					if (iSize > 0) {
						for (int i = 0; i < iSize; i++) {
							JSONObject jsonObj = jsonArray.getJSONObject(i);
							PhysicalMemoryDTO phy = new PhysicalMemoryDTO();
							phy.setpName(jsonObj.optString("Caption"));
							phy.setpInfo(jsonObj.optString("Description"));
							phy.setpSystem(jsonObj.optString("FileSystem"));
							phy.setpAvailable(jsonObj.optString("FreeSpace"));
							phy.setpSize(jsonObj.optString("Size"));
							listLogi.add(phy);
						}
					}
				} catch (Exception e) {
					LOGGER.error(e);
				}
			}

			// IDE控制器
			if (!"null".equals(hardwareDTOs.getIdeController()) && StringUtils.hasText(hardwareDTOs.getIdeController())) {
				try {
					JSONArray jsonArray = JSONArray.fromObject(hardwareDTOs.getIdeController());
					int iSize = jsonArray.size();
					if (iSize > 0) {
						for (int i = 0; i < iSize; i++) {
							JSONObject jsonObj = jsonArray.getJSONObject(i);
							PhysicalMemoryDTO phy = new PhysicalMemoryDTO();
							phy.setpName(jsonObj.optString("Name"));
							phy.setpInfo(jsonObj.optString("Manufacturer"));

							listIDE.add(phy);
						}
					}
				} catch (Exception e) {
					LOGGER.error(e);
				}
			}

			// usb 控制器
			if (!"null".equals(hardwareDTOs.getUsbController()) && StringUtils.hasText(hardwareDTOs.getUsbController())) {
				try {
					JSONArray jsonArray = JSONArray.fromObject(hardwareDTOs.getUsbController());
					int iSize = jsonArray.size();
					if (iSize > 0) {
						for (int i = 0; i < iSize; i++) {
							JSONObject jsonObj = jsonArray.getJSONObject(i);
							PhysicalMemoryDTO phy = new PhysicalMemoryDTO();
							phy.setpName(jsonObj.optString("Name"));
							listUSB.add(phy);
						}
					}
				} catch (Exception e) {
					LOGGER.error(e);
				}
			}
			// USB Hub
			if (!"null".equals(hardwareDTOs.getUsbHub()) && StringUtils.hasText(hardwareDTOs.getUsbHub())) {
				try {
					JSONArray jsonArray = JSONArray.fromObject(hardwareDTOs.getUsbHub());
					int iSize = jsonArray.size();
					if (iSize > 0) {
						for (int i = 0; i < iSize; i++) {
							JSONObject jsonObj = jsonArray.getJSONObject(i);
							PhysicalMemoryDTO phy = new PhysicalMemoryDTO();
							phy.setpName(jsonObj.optString("Name"));
							listUSBHub.add(phy);
						}
					}
				} catch (Exception e) {
					LOGGER.error(e);
				}
			}
			// 端口(COM)
			if (!"null".equals(hardwareDTOs.getSerialPort()) && StringUtils.hasText(hardwareDTOs.getSerialPort())) {
				try {
					JSONArray jsonArray = JSONArray.fromObject(hardwareDTOs.getSerialPort());
					int iSize = jsonArray.size();
					if (iSize > 0) {
						for (int i = 0; i < iSize; i++) {
							JSONObject jsonObj = jsonArray.getJSONObject(i);
							PhysicalMemoryDTO phy = new PhysicalMemoryDTO();
							phy.setpName(jsonObj.optString("Name"));
							phy.setpInfo(jsonObj.optString("ProviderType"));
							listCOM.add(phy);
						}
					}
				} catch (Exception e) {
					LOGGER.error(e);
				}
			}
			// 端口(LPT)
			if (!"null".equals(hardwareDTOs.getParallelPort()) && StringUtils.hasText(hardwareDTOs.getParallelPort())) {
				try {
					JSONArray jsonArray = JSONArray.fromObject(hardwareDTOs.getParallelPort());
					int iSize = jsonArray.size();
					if (iSize > 0) {
						for (int i = 0; i < iSize; i++) {
							JSONObject jsonObj = jsonArray.getJSONObject(i);
							PhysicalMemoryDTO phy = new PhysicalMemoryDTO();
							phy.setpName(jsonObj.optString("Name"));
							phy.setpInfo(jsonObj.optString("Description"));
							listLPT.add(phy);
						}
					}
				} catch (Exception e) {
					LOGGER.error(e);
				}
			}
			// 光驱
			if (!"null".equals(hardwareDTOs.getCdRomDrive()) && StringUtils.hasText(hardwareDTOs.getCdRomDrive())) {
				try {
					JSONArray jsonArray = JSONArray.fromObject(hardwareDTOs.getCdRomDrive());
					int iSize = jsonArray.size();
					if (iSize > 0) {
						for (int i = 0; i < iSize; i++) {
							JSONObject jsonObj = jsonArray.getJSONObject(i);
							PhysicalMemoryDTO phy = new PhysicalMemoryDTO();
							phy.setpName(jsonObj.optString("Name"));
							phy.setpInfo(jsonObj.optString("MediaType"));
							listCD.add(phy);
						}
					}
				} catch (Exception e) {
					LOGGER.error(e);
				}
			}
		}
	}
}
