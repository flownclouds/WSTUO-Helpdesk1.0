package com.wstuo.itsm.cim.dto;

import java.util.Date;

import com.wstuo.common.dto.BaseDTO;

/**
 * 修改行为
 * @author QXY
 */
@SuppressWarnings("serial")
public class BehaviorModificationDTO extends BaseDTO{
	private Long id;//ID
	private String actContent;//修改内容
	private String userName;//操作人
	private Date updateDate=new Date();//更新时间
	private Long ciId;//关联配置编号
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getActContent() {
		return actContent;
	}
	public void setActContent(String actContent) {
		this.actContent = actContent;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public Long getCiId() {
		return ciId;
	}
	public void setCiId(Long ciId) {
		this.ciId = ciId;
	}
	
}
