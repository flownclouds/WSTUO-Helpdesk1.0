package com.wstuo.itsm.cim.dao;


import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.itsm.cim.entity.CIHistoryUpdate;

/**
 * 配置项历史记录
 * @author QXY
 *
 */
public interface ICIHistoryUpdateDAO extends IEntityDAO<CIHistoryUpdate>{
	/**
	 * 查询配置项历史更新记录
	 */
	PageDTO findReleasePager(Long ciid);
}
