package com.wstuo.itsm.cim.service;

import java.util.Set;

import com.wstuo.itsm.cim.dto.CIUtilDTO;
/**
 * 配置项关联关系管理
 * @author QXY
 *
 */
public interface ICIprefuseService {
	/**
	 * 查询配置项的节点
	 * */
	public String findCINode(Long ciid,CIUtilDTO ciutil,Set<Long> ids);
	/**
	 * 查询配置项关联关系显示
	 */
	public String findCiPrefuseView(Long ciid,CIUtilDTO ciutil);
	/**
	 * 查询配置项的节点 *(转换Jason字符)
	 * @param ciid
	 * @param ciutil
	 * @return String
	 */
	String newPrefuseFindCINode(Long ciid,CIUtilDTO ciutil,Set<Long> ids);
	/**
	 * 图标地址
	 * @return
	 */
	String findUrl();
	/**
	 * 连接颜色
	 * @return
	 */
	public String findColor();
}