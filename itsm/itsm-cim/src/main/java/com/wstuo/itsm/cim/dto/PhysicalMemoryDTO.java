package com.wstuo.itsm.cim.dto;
/**
 * 物理内存信息DTO
 * @author QXY
 *
 */
public class PhysicalMemoryDTO {
	
	
	
	private String pName; //
	private String pInfo;
	private String pSystem;
	private String pAvailable;
	private String pSize;
	
	public String getpName() {
		return pName;
	}
	public void setpName(String pName) {
		this.pName = pName;
	}
	public String getpInfo() {
		return pInfo;
	}
	public void setpInfo(String pInfo) {
		this.pInfo = pInfo;
	}
	public String getpSystem() {
		return pSystem;
	}
	public void setpSystem(String pSystem) {
		this.pSystem = pSystem;
	}
	public String getpAvailable() {
		return pAvailable;
	}
	public void setpAvailable(String pAvailable) {
		this.pAvailable = pAvailable;
	}
	public String getpSize() {
		return pSize;
	}
	public void setpSize(String pSize) {
		this.pSize = pSize;
	}

}
