package com.wstuo.itsm.config.visit.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.wstuo.common.entity.BaseEntity;

/**
 * 请求回访事项实体类
 * @author QXY
 *
 */
@SuppressWarnings({ "serial", "rawtypes" })
@Entity
public class VisitItem extends BaseEntity{
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long visitItemNo;
	@Column(nullable=false)
	private String visitItemName;
	@ManyToOne
	private Visit visit;
	public Long getVisitItemNo() {
		return visitItemNo;
	}

	public void setVisitItemNo(Long visitItemNo) {
		this.visitItemNo = visitItemNo;
	}

	public String getVisitItemName() {
		return visitItemName;
	}

	public void setVisitItemName(String visitItemName) {
		this.visitItemName = visitItemName;
	}

	public Visit getVisit() {
		return visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

	
}
