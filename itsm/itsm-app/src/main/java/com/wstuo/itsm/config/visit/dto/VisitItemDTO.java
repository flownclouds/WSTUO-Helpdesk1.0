package com.wstuo.itsm.config.visit.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * 请求回访事项DTO类
 * 
 * @author QXY
 * 
 */
@SuppressWarnings("serial")
public class VisitItemDTO extends BaseDTO {

	private Long visitItemNo;
	private String visitItemName;
	private Long visitNo;

	public Long getVisitItemNo() {
		return visitItemNo;
	}

	public void setVisitItemNo(Long visitItemNo) {
		this.visitItemNo = visitItemNo;
	}

	public String getVisitItemName() {
		return visitItemName;
	}

	public void setVisitItemName(String visitItemName) {
		this.visitItemName = visitItemName;
	}

	public Long getVisitNo() {
		return visitNo;
	}

	public void setVisitNo(Long visitNo) {
		this.visitNo = visitNo;
	}

	public VisitItemDTO() {

	}

	public VisitItemDTO(Long visitItemNo, String visitItemName, Long visitNo) {
		super();
		this.visitItemNo = visitItemNo;
		this.visitItemName = visitItemName;
		this.visitNo = visitNo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((visitItemName == null) ? 0 : visitItemName.hashCode());
		result = prime * result
				+ ((visitItemNo == null) ? 0 : visitItemNo.hashCode());
		result = prime * result + ((visitNo == null) ? 0 : visitNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VisitItemDTO other = (VisitItemDTO) obj;
		if (visitItemName == null) {
			if (other.visitItemName != null)
				return false;
		} else if (!visitItemName.equals(other.visitItemName))
			return false;
		if (visitItemNo == null) {
			if (other.visitItemNo != null)
				return false;
		} else if (!visitItemNo.equals(other.visitItemNo))
			return false;
		if (visitNo == null) {
			if (other.visitNo != null)
				return false;
		} else if (!visitNo.equals(other.visitNo))
			return false;
		return true;
	}

}
