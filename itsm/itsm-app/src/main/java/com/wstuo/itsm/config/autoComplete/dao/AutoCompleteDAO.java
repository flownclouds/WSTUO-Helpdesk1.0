package com.wstuo.itsm.config.autoComplete.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;

import com.wstuo.itsm.config.autoComplete.dto.AutoCompleteParamDTO;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.AutoCompleteDTO;
import com.wstuo.common.entity.BaseEntity;
import com.wstuo.common.security.entity.Company;
import com.wstuo.common.security.service.IMyAllCustomerCompanys;
import com.wstuo.common.security.utils.AppContext;
import com.wstuo.common.util.StringUtils;


/**
 * 自动补全DAO类
 * @author QXY
 *
 */
public class AutoCompleteDAO extends BaseDAOImplHibernate<BaseEntity> implements IAutoCompleteDAO {
	@Autowired
	private AppContext appctx;
	@Autowired
	private IMyAllCustomerCompanys myAllCustomerCompanys;
	
	/**
	 * 自动补全数据查询方法
	 * @param paramDto
	 * @return  List<AutoCompleteDTO> 补全的数据
	 */
	public List<AutoCompleteDTO> autoComplete(AutoCompleteParamDTO paramDto) {
		DetachedCriteria dc = DetachedCriteria.forEntityName(paramDto.getEntityName());
		if(paramDto.getEntityName().contains("ITSOPUser")&&paramDto.getTerm().length()>0){//所负责的客户
				 dc.createAlias("technicians", "t");
			 	 dc.add(Restrictions.eq("t.loginName", paramDto.getTerm()));
		}
		
		String value = "value" ;
		if("Long".equals(paramDto.getReturnValueType())){
			value = "longValue";
		}
		
		if(paramDto.getEntityName().contains("com.wstuo.common.security.entity.User")){
			Disjunction dj = Restrictions.disjunction();
			dc.add(dj);
			dj.add(Restrictions.like(paramDto.getPropertyName(), paramDto.getQueryValue(), MatchMode.ANYWHERE));
			dj.add(Restrictions.like("loginName", paramDto.getQueryValue(), MatchMode.ANYWHERE));
			dc.createAlias("orgnization", "org");
			dj.add(Restrictions.like("org.remark", paramDto.getQueryValue(), MatchMode.ANYWHERE));
            if (paramDto.getTerm()!=null && paramDto.getTerm().length()>0 && Integer.parseInt(paramDto.getTerm()) > 0 ){
                dc.add( Restrictions.eq("companyNo",Long.parseLong(paramDto.getTerm())) );	//只查用户公司的用户
            }
			dc.setProjection(
					Projections.projectionList()
					.add(Projections.property(paramDto.getReturnLabel()).as("label"))
					.add(Projections.property("fullName").as("lName"))//登录名
					.add(Projections.property("org.orgName").as("remark1"))//登录名
					.add(Projections.property(paramDto.getReturnValue()).as(value))
					);
		}else if(paramDto.getEntityName().contains("com.wstuo.common.security.entity.Organization")){
			Disjunction dj = Restrictions.disjunction();
			dc.add(dj);
			dj.add(Restrictions.like(paramDto.getPropertyName(), paramDto.getQueryValue(), MatchMode.ANYWHERE));
			if (StringUtils.hasText(paramDto.getTerm())) {
				dc.add(Restrictions.like("path", paramDto.getTerm()+"/", MatchMode.START));
			}else{
				List<Long> companyNos =  getMyAllCustomerCompanyNos();
			 	dc.add(Restrictions.in("orgNo", companyNos));
			}
			dj.add(Restrictions.like("remark", paramDto.getQueryValue(), MatchMode.ANYWHERE));
			dc.setProjection(
					Projections.projectionList()
					.add(Projections.property(paramDto.getReturnLabel()).as("label"))
					.add(Projections.property(paramDto.getReturnValue()).as(value))
					);
		}else if(paramDto.getEntityName().contains("com.wstuo.common.rules.entity.RulePackage")){
			Disjunction dj = Restrictions.disjunction();
			dc.add(dj);
			dj.add(Restrictions.like(paramDto.getPropertyName(), paramDto.getQueryValue(), MatchMode.ANYWHERE));
			dj.add(Restrictions.ne("dataFlag",BaseEntity.DELETED.byteValue()));
			dj.add(Restrictions.eq("flagName","requestProce"));
			dc.setProjection(
					Projections.projectionList()
					.add(Projections.property(paramDto.getReturnLabel()).as("label"))
					.add(Projections.property(paramDto.getReturnValue()).as(value))
					);
		}else{		
			dc.add(Restrictions.like(paramDto.getPropertyName(), paramDto.getQueryValue(), MatchMode.ANYWHERE));
			dc.setProjection(
					Projections.projectionList()
					.add(Projections.property(paramDto.getReturnLabel()).as("label"))
					.add(Projections.property(paramDto.getReturnValue()).as(value))
					);
		}
		dc.setResultTransformer(Transformers.aliasToBean(AutoCompleteDTO.class));
		List<AutoCompleteDTO> list = super.getHibernateTemplate().findByCriteria(dc, 0, 10);
		return list;
	}
	
	// 负责的所属客户
	private List<Long> getMyAllCustomerCompanyNos(){
		List<Long> companyNos = new ArrayList<Long>();
		String loginName=appctx.getCurrentLoginName();
		Set<Company> companys = myAllCustomerCompanys.findCompanysMyAllCustomer(loginName);
		for (Company company : companys) {
			companyNos.add(company.getOrgNo());
		}
		return companyNos;
	}

}
