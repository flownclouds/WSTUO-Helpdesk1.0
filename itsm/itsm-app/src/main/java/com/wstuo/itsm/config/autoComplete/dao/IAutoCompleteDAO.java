package com.wstuo.itsm.config.autoComplete.dao;

import java.util.List;

import com.wstuo.itsm.config.autoComplete.dto.AutoCompleteParamDTO;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.AutoCompleteDTO;
import com.wstuo.common.entity.BaseEntity;
/**
 * 自动补全DAO接口
 * @author QXY
 *
 */
public interface IAutoCompleteDAO extends IEntityDAO<BaseEntity>{
	
	/**
	 * 自动补全数据查询方法
	 * @param dto
	 * @return  List<AutoCompleteDTO> 补全的数据
	 */
	List<AutoCompleteDTO> autoComplete(AutoCompleteParamDTO dto);
}
