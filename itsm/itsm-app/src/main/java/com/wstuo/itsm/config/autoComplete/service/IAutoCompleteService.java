package com.wstuo.itsm.config.autoComplete.service;

import java.util.List;

import com.wstuo.itsm.config.autoComplete.dto.AutoCompleteParamDTO;
import com.wstuo.common.dto.AutoCompleteDTO;
/**
 *  自动补全 业务层接口
 *
 */
public interface IAutoCompleteService{
	
	/**
	 * 自动补全数据查询方法
	 * @param paramDto 自定补全参数DTO
	 * @return List<AutoCompleteDTO> 
	 */
	List<AutoCompleteDTO> autoComplete(AutoCompleteParamDTO paramDto);
}
