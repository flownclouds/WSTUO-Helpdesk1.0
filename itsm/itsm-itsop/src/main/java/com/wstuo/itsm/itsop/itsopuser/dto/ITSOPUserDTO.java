package com.wstuo.itsm.itsop.itsopuser.dto;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.wstuo.common.dto.BaseDTO;
/**
 * 外包客户管理数据传递类
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class ITSOPUserDTO extends BaseDTO {

	private Long orgNo;
	private String orgName;
	private String address;
	private String email;
	private String officePhone;
	private String officeFax;
	private String technicianNamesStr;
	private String corporate;// 法人代表
	private String companySize;// 公司规模
	private String regNumber;// 工商注册号;
	private Long regCapital;// 注册资金
	private Long typeNo;
	private Long typeName;
	private File importFile;
	private String logo;
	
	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getCorporate() {
		return corporate;
	}

	public void setCorporate(String corporate) {
		this.corporate = corporate;
	}

	public File getImportFile() {
		return importFile;
	}

	public void setImportFile(File importFile) {
		this.importFile = importFile;
	}

	public String getCompanySize() {
		return companySize;
	}

	public void setCompanySize(String companySize) {
		this.companySize = companySize;
	}

	public String getRegNumber() {
		return regNumber;
	}

	public void setRegNumber(String regNumber) {
		this.regNumber = regNumber;
	}

	public Long getRegCapital() {
		return regCapital;
	}

	public void setRegCapital(Long regCapital) {
		this.regCapital = regCapital;
	}

	public String getTechnicianNamesStr() {
		return technicianNamesStr;
	}

	public void setTechnicianNamesStr(String technicianNamesStr) {
		this.technicianNamesStr = technicianNamesStr;
	}

	public List<String> getTechnicianNames() {

		List<String> arrs = new ArrayList<String>();

		String[] arr = technicianNamesStr.split(";");

		if (arr != null && arr.length > 0) {

			for (int z = 0; z < arr.length; z++) {

				if (arr[z] != null && !arr[z].equals("")) {
					arrs.add(arr[z]);
				}
			}
		}
		return arrs;
	}

	public Long getOrgNo() {
		return orgNo;
	}

	public void setOrgNo(Long orgNo) {
		this.orgNo = orgNo;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOfficePhone() {
		return officePhone;
	}

	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}

	public String getOfficeFax() {
		return officeFax;
	}

	public void setOfficeFax(String officeFax) {
		this.officeFax = officeFax;
	}

	public Long getTypeNo() {
		return typeNo;
	}

	public void setTypeNo(Long typeNo) {
		this.typeNo = typeNo;
	}

	public Long getTypeName() {
		return typeName;
	}

	public void setTypeName(Long typeName) {
		this.typeName = typeName;
	}

}
