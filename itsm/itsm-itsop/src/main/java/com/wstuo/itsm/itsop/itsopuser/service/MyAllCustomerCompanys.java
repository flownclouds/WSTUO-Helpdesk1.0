package com.wstuo.itsm.itsop.itsopuser.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.itsm.itsop.itsopuser.dao.IITSOPUserDAO;
import com.wstuo.itsm.itsop.itsopuser.entity.ITSOPUser;
import com.wstuo.common.security.dao.ICompanyDAO;
import com.wstuo.common.security.dao.IUserDAO;
import com.wstuo.common.security.entity.Company;
import com.wstuo.common.security.entity.User;
import com.wstuo.common.security.service.IMyAllCustomerCompanys;

public class MyAllCustomerCompanys implements IMyAllCustomerCompanys{
	@Autowired
	private IITSOPUserDAO itsopUserDAO;
	@Autowired
	private IUserDAO userDAO;
	@Autowired
	private ICompanyDAO companyDAO;
	/**
	 * 我所在的公司和我负责的客户
	 * @return Set<Company>
	 */
	@Transactional
	public Set<Company> findCompanysMyAllCustomer(String loginName) {
		Set<Company> companyNos = new HashSet<Company>();// 唯一集合
		//查出我负责的外包客户
		List<ITSOPUser> itsopUsers = itsopUserDAO.findMyRelatedCustomer(loginName);
		for(ITSOPUser itsopUser : itsopUsers){
			companyNos.add(itsopUser);
		}
		//我所在的公司
		User user = userDAO.findUniqueBy("loginName", loginName);
		if(user!=null){
			companyNos.add(companyDAO.findById(user.getCompanyNo()));
		}
		return companyNos;
	}
}
