package com.wstuo.itsm.itsop.itsopuser.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * 客户数据统计
 * @author WSTUO_QXY
 *
 */
@SuppressWarnings("serial")
public class CustomerDataCountDTO extends BaseDTO {
	private Long orgNo;
	private String orgName;
	private Integer total;
	public Long getOrgNo() {
		return orgNo;
	}
	public void setOrgNo(Long orgNo) {
		this.orgNo = orgNo;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	
}
