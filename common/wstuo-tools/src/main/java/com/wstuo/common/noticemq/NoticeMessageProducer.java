package com.wstuo.common.noticemq;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.jms.core.JmsTemplate;

import com.wstuo.common.tools.dto.NoticeSendDTO;

/**
 * 发送通知消息
 * @author will
 *
 */
public class NoticeMessageProducer {
	private JmsTemplate template;

	public void setTemplate(JmsTemplate template) {
	   this.template = template;
	}

	public void send(NoticeSendDTO sendDTO) {
   
		ActiveMQQueue queue=new ActiveMQQueue("NoticeMessageQueue."+sendDTO.getTenantId());
		template.convertAndSend(queue, sendDTO);
	} 
}
