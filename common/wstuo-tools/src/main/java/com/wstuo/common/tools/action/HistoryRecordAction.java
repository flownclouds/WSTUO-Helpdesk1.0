package com.wstuo.common.tools.action;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.tools.dto.HistoryRecordDTO;
import com.wstuo.common.tools.service.IHistoryRecordService;

/**
 * History Record Action class
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class HistoryRecordAction extends ActionSupport {
	@Autowired
	private IHistoryRecordService historyRecordService;
	private List<HistoryRecordDTO> historyRecordDto;
	private HistoryRecordDTO hisotryRecordDto;
	public List<HistoryRecordDTO> getHistoryRecordDto() {
		return historyRecordDto;
	}
	public void setHistoryRecordDto(List<HistoryRecordDTO> historyRecordDto) {
		this.historyRecordDto = historyRecordDto;
	}
	public HistoryRecordDTO getHisotryRecordDto() {
		return hisotryRecordDto;
	}
	public void setHisotryRecordDto(HistoryRecordDTO hisotryRecordDto) {
		this.hisotryRecordDto = hisotryRecordDto;
	}
	
	/**
	 * 查询全部历史 
	 * @return String 
	 */
	public String findAllHistoryRecord(){
		historyRecordDto=historyRecordService.findAllHistoryRecord(hisotryRecordDto);
		return SUCCESS;
	}
	
	/**
	 *  search historyRecord By client
	 * @return historyRecordDto
	 */
	public String findAllHistoryRecordByClient(){
		historyRecordDto=historyRecordService.findAllHistoryRecord(hisotryRecordDto);
		return SUCCESS;
	}
	
	
	/**
	 * 保存历史
	 * @return null
	 */
	public String saveHistoryRecord(){
		historyRecordService.saveHistoryRecord(hisotryRecordDto);
		return SUCCESS;
	}
}
