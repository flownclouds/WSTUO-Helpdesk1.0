package com.wstuo.common.tools.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.tools.dto.EventAttachmentDTO;
import com.wstuo.common.tools.entity.EventAttachment;
import com.wstuo.common.dao.BaseDAOImplHibernate;



/**
 * Event Attachment DAO class
 * @author QXY
 */
public class EventAttachmentDAO extends BaseDAOImplHibernate<EventAttachment> implements IEventAttachmentDAO {
	private final static int defaultStart=0;
	private final static int defaultLimit=10000;
	
	/**
	 * 查询全部
	 * @param dto EventAttachmentDTO
	 * @return List<EventAttachment>
	 */
    public List<EventAttachment> findAllEventAttachment(EventAttachmentDTO dto){
    	final DetachedCriteria dc = DetachedCriteria.forClass(EventAttachment.class);
		if(dto!=null && dto.getEventType()!=null && dto.getEno()!=null){
			dc.add(Restrictions.eq("eno", dto.getEno()));
			dc.add(Restrictions.eq("eventType", dto.getEventType()));
		}
		return super.findPageByCriteria(dc, defaultStart, defaultLimit).getData();
    }; 
    /**
	 * 根据ID查询
	 * @param ids
	 * @param eno
	 * @return List<EventAttachment>
	 */
    public List<EventAttachment> findByEventAttachment(Long[] ids,Long eno){
    	final DetachedCriteria dc = DetachedCriteria.forClass(EventAttachment.class);
    	if(ids!=null && eno!=null){
    		dc.add(Restrictions.eq("eno", eno));
    		dc.add(Restrictions.in("attachment.aid", ids));
    	}
		return super.findPageByCriteria(dc, defaultStart, defaultLimit).getData();
    }; 
}
