package com.wstuo.common.noticemq;

import org.springframework.beans.factory.annotation.Autowired;

import com.wstuo.common.activemq.consumer.IMessageConsumer;
import com.wstuo.common.noticeRule.service.INoticeRuleService;
import com.wstuo.common.tools.dto.NoticeSendDTO;
/**
 * 发送邮件消息
 * @author will
 *
 */
public class EmailMessageConumer implements IMessageConsumer{
	@Autowired
	private INoticeRuleService noticeService;
	
	public void messageConsumer(Object obj) {
		NoticeSendDTO sendDTO=(NoticeSendDTO)obj;
		noticeService.sendEmail(sendDTO);//发送邮件
	}

}
