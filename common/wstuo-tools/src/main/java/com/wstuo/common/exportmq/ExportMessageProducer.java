package com.wstuo.common.exportmq;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.jms.core.JmsTemplate;

import com.wstuo.common.tools.dto.ExportQueryDTO;

/**
 * 发送导出信息
 * @author will
 *
 */
public class ExportMessageProducer {
	private JmsTemplate template;

	public void setTemplate(JmsTemplate template) {
	   this.template = template;
	}

	/**
	 * 发送
	 * @param order
	 */
	public void send(ExportQueryDTO order) {//传入MQ里的dto
		ActiveMQQueue queue=new ActiveMQQueue("ExportMessageQueue."+order.getTenantId());
		template.convertAndSend(queue, order);
	} 
}
