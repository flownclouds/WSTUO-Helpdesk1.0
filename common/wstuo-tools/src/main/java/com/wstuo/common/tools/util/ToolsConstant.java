package com.wstuo.common.tools.util;

/**
 * Tools 公共的常量类
 * @author gs60
 *
 */
public class ToolsConstant {
	
	public final static String TASK_TIME_SEPARATED = ";";
	public final static String TASK_TYPE_NORMAL = "NO";
	public final static String TASK_TYPE_CYCLE_WEEK = "WEEK";
	
	public final static String TASK_TYPE_WEEK_SUN = "SUN";
	public final static String TASK_TYPE_WEEK_MON = "MON";
	public final static String TASK_TYPE_WEEK_TUE = "TUE";
	public final static String TASK_TYPE_WEEK_WED = "WED";
	public final static String TASK_TYPE_WEEK_THU = "THU";
	public final static String TASK_TYPE_WEEK_FRI = "FRI";
	public final static String TASK_TYPE_WEEK_SAT = "SAT";

	public final static String TASK_TYPE_WEEK[] = new String[]{
		TASK_TYPE_WEEK_SUN,TASK_TYPE_WEEK_MON,TASK_TYPE_WEEK_TUE,TASK_TYPE_WEEK_WED,
		TASK_TYPE_WEEK_THU,TASK_TYPE_WEEK_FRI,TASK_TYPE_WEEK_SAT};

	public final static String TASK_TYPE_CYCLE_END_NO = "NO";
	public final static String TASK_TYPE_CYCLE_END_COUNT = "COUNT";
	public final static String TASK_TYPE_CYCLE_END_DATE = "DATE";
	
	
	
	public final static String TYPE_OTHER_IS_TASK = "itsm.task";
	
	
	/**
	 * 将数据库中保存的星期转换为具体的值
	 * @param weekIndexs
	 * @return
	 */
	public static String[] weeksIndex(int[] weekIndexs) {
		String[] weekValues = null;
		int len = 0;
		if (weekIndexs != null && ( len = weekIndexs.length ) > 0) {
			weekValues = new String[len];
			for (int i = 0; i < len; i++) {
				weekValues[i] = TASK_TYPE_WEEK[weekIndexs[i] - 1];
			}
		}
		return weekValues;
	}
	/**
	 * 将星期由具体的值转换为索引位置 从1 开始
	 * @param weekIndexs
	 * @return
	 */
	public static int weekIndex(String week) {
		int result = 0;
		if (week != null) {
			int len = TASK_TYPE_WEEK.length;
			for (int i = 0; i < len; i++) {
				if ( TASK_TYPE_WEEK[i].equals( week ) ) {
					result = i + 1;
					break;
				}
			}
		}
		return result;
	}
	public static int[] weeksIndex(String weeks) {
		String[] weekValues = weeks.split(";");
		int[] result = new int[weekValues.length];
		for (int i = 0; i < result.length; i++) {
			for (int j = 0; j < TASK_TYPE_WEEK.length; j++ ) {
				if ( TASK_TYPE_WEEK[j].equals( weekValues[i] ) ) {
					result[i] = j + 1;
					break;
				}
			}
		}
		return result;
	}
}
