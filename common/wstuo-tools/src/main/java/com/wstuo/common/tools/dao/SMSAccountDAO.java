package com.wstuo.common.tools.dao;

import java.util.List;

import com.wstuo.common.tools.entity.SMSAccount;
import com.wstuo.common.dao.BaseDAOImplHibernate;
/**
 * 短信DAO类
 * @author QXY
 * date 2010-10-11
 */
public class SMSAccountDAO
    extends BaseDAOImplHibernate<SMSAccount>
    implements ISMSAccountDAO
{

	/**
	 * 获取短信账号信息
	 * @return 短信账号Entity
	 */
	public SMSAccount getSmsAccount() {
		// TODO Auto-generated method stub
		List<SMSAccount> smsa = super.findAll();
		if(smsa!=null && smsa.size()>0){
			return smsa.get(0);
		}else{
			return null;
		}
	}
   
}
