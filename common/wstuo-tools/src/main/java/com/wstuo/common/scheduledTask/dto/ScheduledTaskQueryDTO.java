package com.wstuo.common.scheduledTask.dto;

import java.util.Date;

/**
 * 定期任务搜索DTO
 * @author WSTUO
 *
 */
public class ScheduledTaskQueryDTO{
	private Long scheduledTaskId;//ID
	private String keywork;
	private String sord;
    private String sidx;
    private Integer start;
    private Integer limit = 10;
    private Long companyNo;
    private Date startCreateTime;
    private Date endCreateTime;
    private Date startUpdateTime;
    private Date endUpdateTime;
    private Date taskDate;//具体时间(开始时间)
	private Date taskEndDate;//结束时间
	private String scheduledTaskType;
	private String timeType;//时间类型
	public Long getScheduledTaskId() {
		return scheduledTaskId;
	}
	public void setScheduledTaskId(Long scheduledTaskId) {
		this.scheduledTaskId = scheduledTaskId;
	}
	public String getKeywork() {
		return keywork;
	}
	public void setKeywork(String keywork) {
		this.keywork = keywork;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	public Long getCompanyNo() {
		return companyNo;
	}
	public void setCompanyNo(Long companyNo) {
		this.companyNo = companyNo;
	}
	public Date getStartCreateTime() {
		return startCreateTime;
	}
	public void setStartCreateTime(Date startCreateTime) {
		this.startCreateTime = startCreateTime;
	}
	public Date getEndCreateTime() {
		return endCreateTime;
	}
	public void setEndCreateTime(Date endCreateTime) {
		this.endCreateTime = endCreateTime;
	}
	public Date getStartUpdateTime() {
		return startUpdateTime;
	}
	public void setStartUpdateTime(Date startUpdateTime) {
		this.startUpdateTime = startUpdateTime;
	}
	public Date getEndUpdateTime() {
		return endUpdateTime;
	}
	public void setEndUpdateTime(Date endUpdateTime) {
		this.endUpdateTime = endUpdateTime;
	}
	public Date getTaskDate() {
		return taskDate;
	}
	public void setTaskDate(Date taskDate) {
		this.taskDate = taskDate;
	}
	public Date getTaskEndDate() {
		return taskEndDate;
	}
	public void setTaskEndDate(Date taskEndDate) {
		this.taskEndDate = taskEndDate;
	}
	public String getScheduledTaskType() {
		return scheduledTaskType;
	}
	public void setScheduledTaskType(String scheduledTaskType) {
		this.scheduledTaskType = scheduledTaskType;
	}
	public String getTimeType() {
		return timeType;
	}
	public void setTimeType(String timeType) {
		this.timeType = timeType;
	}

    
}
