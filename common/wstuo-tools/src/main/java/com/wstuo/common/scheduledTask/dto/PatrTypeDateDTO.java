package com.wstuo.common.scheduledTask.dto;

import java.util.List;

public class PatrTypeDateDTO {
	
	private List<String[]> patrData;// 批注信息
	private List<String[]> types;// 批注信息
	private List<String[]> data;// 批注信息
	
	public List<String[]> getPatrData() {
		return patrData;
	}
	public void setPatrData(List<String[]> patrData) {
		this.patrData = patrData;
	}
	public List<String[]> getTypes() {
		return types;
	}
	public void setTypes(List<String[]> types) {
		this.types = types;
	}
	public List<String[]> getData() {
		return data;
	}
	public void setData(List<String[]> data) {
		this.data = data;
	}
	
}
