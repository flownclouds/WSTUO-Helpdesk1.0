package com.wstuo.common.tools.sms;

import java.util.Date;

/**
 * 短信接收接口
 * @author Administrator
 *
 */
public interface ISMSReceive {
	/**
	 * 短信接收回调方法
	 * @param callNo 发送号码
	 * @param smsContent 短信内容
	 * @param sendDate 发送时间
	 */
	void receiveSMS(String callNo,String smsContent,Date sendDate);
	
	/*void SendSMSReceive(Long requestEno,String ApporUser);*/
}
