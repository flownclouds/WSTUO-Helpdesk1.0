package com.wstuo.common.customForm.dao;

import java.util.List;

import com.wstuo.common.customForm.dto.FormCustomQueryDTO;
import com.wstuo.common.customForm.entity.FormCustom;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

/**
 * 自定义表单模块DAO接口
 * 
 * @author Haley
 *
 */
public interface IFormCustomDAO extends IEntityDAO<FormCustom>{

	/**
	 * 分页查询模块信息
	 * @param queryDto
	 * @param sord 排序时是正序还是反序
	 * @param sidx 排序时是哪个字段
	 * @return PageDTO
	 */
	PageDTO findPager(FormCustomQueryDTO queryDto, String sord, String sidx);
	
	/**
	 * 查询自定义表单
	 * @param queryDto
	 * @return
	 */
	FormCustom findFormCustom(FormCustomQueryDTO queryDto);
	
	/**
	 *  查询所有的自定义表单模板
	 * @param isTemplate
	 * @return
	 */
	List<FormCustom> findSimilarFormCustom(FormCustomQueryDTO queryDto);
	
	/**
	 * 是否存在相同表单名
	 * @param dto
	 * @return
	 */
	Boolean isFormCustomNameExisted(FormCustomQueryDTO dto);
	
}
