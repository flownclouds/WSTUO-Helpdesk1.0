package com.wstuo.common.tools.dto;

import java.util.Date;

import com.wstuo.common.dto.BaseDTO;

/**
 * 用户回访信息DTO
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class UserReturnVisitDTO extends BaseDTO {

	private Long visitId;
	private Long eno;
	private String entityType;
	private Long satisfaction;
	private String returnVisitDetail;
	private Long state;
	private Date returnVisitSubmitTime;
	private Date returnVisitTime ;
	private Long returnVisitMode;
	private String returnVisitUser;
	private String returnVisitSubmitUser;
	private String returnVisitTechnicianName;  //保存请求操作的技术员
	
	
	public String getReturnVisitTechnicianName() {
		return returnVisitTechnicianName;
	}
	public void setReturnVisitTechnicianName(String returnVisitTechnicianName) {
		this.returnVisitTechnicianName = returnVisitTechnicianName;
	}
	
	
	public Long getVisitId() {
		return visitId;
	}
	public void setVisitId(Long visitId) {
		this.visitId = visitId;
	}
	public Long getEno() {
		return eno;
	}
	public void setEno(Long eno) {
		this.eno = eno;
	}
	public String getEntityType() {
		return entityType;
	}
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}
	public Long getSatisfaction() {
		return satisfaction;
	}
	public void setSatisfaction(Long satisfaction) {
		this.satisfaction = satisfaction;
	}
	public String getReturnVisitDetail() {
		return returnVisitDetail;
	}
	public void setReturnVisitDetail(String returnVisitDetail) {
		this.returnVisitDetail = returnVisitDetail;
	}
	public Long getState() {
		return state;
	}
	public void setState(Long state) {
		this.state = state;
	}
	public Date getReturnVisitSubmitTime() {
		return returnVisitSubmitTime;
	}
	public void setReturnVisitSubmitTime(Date returnVisitSubmitTime) {
		this.returnVisitSubmitTime = returnVisitSubmitTime;
	}
	public Date getReturnVisitTime() {
		return returnVisitTime;
	}
	public void setReturnVisitTime(Date returnVisitTime) {
		this.returnVisitTime = returnVisitTime;
	}

	public Long getReturnVisitMode() {
		return returnVisitMode;
	}
	public void setReturnVisitMode(Long returnVisitMode) {
		this.returnVisitMode = returnVisitMode;
	}
	public String getReturnVisitUser() {
		return returnVisitUser;
	}
	public void setReturnVisitUser(String returnVisitUser) {
		this.returnVisitUser = returnVisitUser;
	}
	public String getReturnVisitSubmitUser() {
		return returnVisitSubmitUser;
	}
	public void setReturnVisitSubmitUser(String returnVisitSubmitUser) {
		this.returnVisitSubmitUser = returnVisitSubmitUser;
	}
	
}
