package com.wstuo.common.customForm.action;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.customForm.dto.FormCustomDTO;
import com.wstuo.common.customForm.dto.FormCustomQueryDTO;
import com.wstuo.common.customForm.dto.SimpleFileDTO;
import com.wstuo.common.customForm.service.IFormCustomService;
import com.wstuo.common.dto.PageDTO;

@SuppressWarnings("serial")
public class FormCustomAction extends ActionSupport {
	private static final Logger LOGGER = Logger.getLogger(FormCustomAction.class);

	@Autowired
	private IFormCustomService formCustomService;
	private FormCustomDTO formCustomDTO = new FormCustomDTO();
	private FormCustomQueryDTO formCustomQueryDTO = new FormCustomQueryDTO();
	private PageDTO pageDto;
	private String sidx;
	private String sord;
	private Boolean bool;
	private int page = 1;
	private int rows = 10;
	private Long formCustomId;
	private Long[] formCustomIds;
	private String type;
	private List<FormCustomQueryDTO> dtos = new ArrayList<FormCustomQueryDTO>();

    private InputStream exportStream;
    private Long categoryNo;
    private String fileName="";
	
	public InputStream getExportStream() {
		return exportStream;
	}

	public void setExportStream(InputStream exportStream) {
		this.exportStream = exportStream;
	}

	public Long getCategoryNo() {
		return categoryNo;
	}

	public void setCategoryNo(Long categoryNo) {
		this.categoryNo = categoryNo;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public FormCustomDTO getFormCustomDTO() {
		return formCustomDTO;
	}

	public void setFormCustomDTO(FormCustomDTO formCustomDTO) {
		this.formCustomDTO = formCustomDTO;
	}

	public FormCustomQueryDTO getFormCustomQueryDTO() {
		return formCustomQueryDTO;
	}

	public void setFormCustomQueryDTO(FormCustomQueryDTO formCustomQueryDTO) {
		this.formCustomQueryDTO = formCustomQueryDTO;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public PageDTO getPageDto() {
		return pageDto;
	}

	public void setPageDto(PageDTO pageDto) {
		this.pageDto = pageDto;
	}

	public Long getFormCustomId() {
		return formCustomId;
	}

	public void setFormCustomId(Long formCustomId) {
		this.formCustomId = formCustomId;
	}

	public Long[] getFormCustomIds() {
		return formCustomIds;
	}

	public void setFormCustomIds(Long[] formCustomIds) {
		this.formCustomIds = formCustomIds;
	}

	public List<FormCustomQueryDTO> getDtos() {
		return dtos;
	}

	public void setDtos(List<FormCustomQueryDTO> dtos) {
		this.dtos = dtos;
	}
	public Boolean getBool() {
		return bool;
	}

	public void setBool(Boolean bool) {
		this.bool = bool;
	}

	/**
	 * 添加自定义表单
	 * 
	 * @return
	 */
	public String saveFormCustom() {
		formCustomId=formCustomService.saveFormCustom(formCustomDTO);
		return "saveFormCustom";
	}

	/**
	 * 删除自定义表单
	 * 
	 * @return
	 */
	public String deleteFormCustom() {
		formCustomService.deleteFormCustom(formCustomIds);
		return "deleteFormCustom";
	}

	/**
	 * 根据id查询表单
	 * 
	 * @return
	 */
	public String findFormCustomById() {
		formCustomQueryDTO = formCustomService.findFormCustomById(formCustomId);
		return "findFormCustomById";
	}
	
	/**
	 * 查询默认的表单编号
	 * @return
	 */
	public String findIsDefault() {
		formCustomDTO=formCustomService.findIsDefault(type);
		return "findFormCustom";
	}
	/**
	 * 修改一个自定义表单
	 * 
	 * @return
	 */
	public String updateFormCustom() {
		formCustomService.updateFormCustom(formCustomDTO);
		return "updateFormCustom";
	}

	/**
	 * 分页查询
	 * 
	 * @return
	 */
	public String findPagerFormCustom() {
		int start = (page - 1) * rows;
		formCustomQueryDTO.setLimit(rows);
		formCustomQueryDTO.setStart(start);
		pageDto = formCustomService.findFormCustomPager(formCustomQueryDTO,
				sord, sidx);
		pageDto.setPage(page);
		pageDto.setRows(rows);
		return SUCCESS;
	}

	/**
	 * 根据服务目录Id查询自定义表单
	 * 
	 * @return
	 */
	public String findFormCustomByServiceDirId() {
		formCustomDTO = formCustomService
				.findFormCustomByServiceDirId(formCustomQueryDTO);
		return "findFormCustom";
	}

	/**
	 * 查询所有自定义模板
	 * @return
	 */
	public String findSimilarFormCustom() {
		dtos = formCustomService.findSimilarFormCustom(formCustomQueryDTO);
		return "findSimilarFormCustom";
	}
	
	public String findAllFormCustom(){
		dtos = formCustomService.findAllFormCustom();
		return "findAllFormCustom";
	}
	
	public String findAllRequestFormCustom(){
		dtos = formCustomService.findAllRequestFormCustom();
		return "findAllFormCustom";
	}
	/**
	 * 根据配置项的分类ID查询自定义表单
	 * @return
	 */
	public String findFormCustomByCiCategoryNo(){
		formCustomDTO=formCustomService.findFormCustomByCiCategoryNo(formCustomDTO.getCiCategoryNo());
		return "formCustomDTO";
	}
	
	public String isFormCustomNameExisted(){
		bool = formCustomService.isFormCustomNameExisted(formCustomQueryDTO);
		return "isExisted";
	}
	
	public String isFormCustomNameExistedOnEdit(){
		bool = formCustomService.isFormCustomNameExistedOnEdit(formCustomQueryDTO);
		return "isExisted";
	}

	/**
	 * 生成导入模板
	 * @return
	 */
	@Deprecated
	public String createTemplateWithCategory(){
		try {
			SimpleFileDTO fileDTO = formCustomService.generateTemplateWithCategory(categoryNo);
	   		if( fileDTO != null ){
	   			fileName = fileDTO.getFileName();
	   			exportStream = fileDTO.getInputStream();
	   		}
		} catch (Exception e) {
			LOGGER.error(e);//e.printStackTrace();
		}
   		return "exportSuccess";
   	}
}
