package com.wstuo.common.tools.action;
import java.io.File;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.tools.dto.FileInfoDTO;
import com.wstuo.common.tools.service.IUploadFile;

/**
 * 文件上传Action类
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class UploadAction extends ActionSupport {
	final static Logger LOGGER = Logger.getLogger(UploadAction.class);
    private String fileExt="jpg,png,gif,bmp";
    private File filedata;
    private String filedataFileName;
    private FileInfoDTO fileInfo = new FileInfoDTO();
    private String deleteFileName;
    private String flag;
    
    @Autowired
    private IUploadFile uploadFile;
    
    
    public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getDeleteFileName() {
		return deleteFileName;
	}
	public void setDeleteFileName(String deleteFileName) {
		this.deleteFileName = deleteFileName;
	}
	public File getFiledata() {
		return filedata;
	}
	public void setFiledata(File filedata) {
		this.filedata = filedata;
	}
	public FileInfoDTO getFileInfo() {
        return fileInfo;
    }
    public void setFileInfo(FileInfoDTO fileInfo) {
        this.fileInfo = fileInfo;
    }
    public String getFiledataFileName() {
		return filedataFileName;
	}
	public void setFiledataFileName(String filedataFileName) {
		this.filedataFileName = filedataFileName;
	}


	/**
	 * 上传图片
	 * @return String
	 */
    public String uploadImage() {
    	
    	this.fileExt="jpg,gif,png,bmp";
    	fileInfo=uploadFile.uploadFile(fileInfo, filedataFileName, filedata, fileExt, flag);
        return "result";
    }
    
    /**
	 * 上传附件
	 * @return String
	 */
    public String uploadAttr() {
    	this.fileExt="doc,docx,xls,rar,zip,txt,pdf,jpg,png,gif,bmp,swf,xlsx,ppt,pptx,gif,jpg,png,bmp,vsd";
    	fileInfo=uploadFile.uploadFile(fileInfo, filedataFileName, filedata, fileExt, flag);
    	return "result";
    }
    
    /**
	 * 上传FLASH
	 * @return String
	 */
    public String uploadFlash() {
    	this.fileExt="swf";
    	uploadFile.uploadFile(fileInfo, filedataFileName, filedata, fileExt, flag);
        return "result";
    }
    
    /**
	 * 删除文件
	 * @return String
	 */
    public String deleteFile(){
    	uploadFile.deleteFile(deleteFileName);
    	return "result";
    	
    }
    
    
}