package com.wstuo.common.tools.dto;

import com.wstuo.common.dto.BaseDTO;
/**
 * 附件上传DTO
 *
 */
@SuppressWarnings("serial")
public class FileUploadDTO extends BaseDTO {

	private String result;
	private String imageFileName;
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getImageFileName() {
		return imageFileName;
	}
	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}
	
}
