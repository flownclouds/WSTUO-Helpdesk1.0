package com.wstuo.common.tools.dao;

import com.wstuo.common.tools.entity.EmailServer;
import com.wstuo.common.dao.BaseDAOImplHibernate;

/**
 * 服务邮箱地址DAO
 * @author QXY
 *
 */
public class EmailServerDAO
    extends BaseDAOImplHibernate<EmailServer>
    implements IEmailServerDAO
{
}
