package com.wstuo.common.tools.service;

import java.util.List;

import com.moyosoft.exchange.ExchangeServiceException;
import com.wstuo.common.tools.dto.EmailDTO;
import com.wstuo.common.tools.dto.EmailMessageDTO;
import com.wstuo.common.tools.dto.EmailServerDTO;

/**
 * Exchange Service
 * @author WSTUO
 *
 */
public interface IExchangeService {
	/**
	 * Exchange连接测试
	 * @param dto
	 * @return boolean
	 */
	boolean exchangeConnectionTest(EmailServerDTO dto);
	
	/**
	 * 邮件发送
	 * @param dto
	 */
	boolean sendEmail(EmailDTO dto);
	
	/**
	 * 邮件接收
	 * @return List<EmailMessageDTO>	
	 * @throws ExchangeServiceException
	 */
	List<EmailMessageDTO> getInboxMail() throws ExchangeServiceException;
}
