package com.wstuo.common.noticeRule.action;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.noticeRule.dto.EmailTemplatesDTO;
import com.wstuo.common.noticeRule.dto.NoticeRuleDTO;
import com.wstuo.common.noticeRule.dto.TemplateVariableDTO;
import com.wstuo.common.noticeRule.service.INoticeRuleService;
import com.wstuo.common.tools.dto.EmailConnectionDTO;
import com.wstuo.common.tools.service.IEmailService;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.exception.ApplicationException;

/**
 * 通知规则Action类
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class NoticeRuleAction extends ActionSupport {
	@Autowired
	private INoticeRuleService noticeRuleService;
	private PageDTO noticeRules;
	private NoticeRuleDTO noticeRuleDTO = new NoticeRuleDTO();
	private int page = 1;
    private int rows = 100;
    private int count = 1;
    private String sord;
    private String sidx;
    private Long[] ids;
    private List<String> loginName;
	private String titleString;
	private String contentString;
	private EmailTemplatesDTO emailTemplatesDTO;
    private Map<String,String> noticeRuleNos;
    private Map<String,Boolean>  useStatuses;
    private boolean result;
    private String templatesType;
    private String status;
    private TemplateVariableDTO templateVariableDTO;
    @Autowired
    private IEmailService emailService;
    private EmailConnectionDTO emailConnectionDto = new EmailConnectionDTO();
    private int testErrorEncode = 0;
    private String userName;
    private String clientId;
    
	public int getTestErrorEncode() {
		return testErrorEncode;
	}
	public void setTestErrorEncode(int testErrorEncode) {
		this.testErrorEncode = testErrorEncode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTemplatesType() {
		return templatesType;
	}
	public void setTemplatesType(String templatesType) {
		this.templatesType = templatesType;
	}
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public Map<String, String> getNoticeRuleNos() {
		return noticeRuleNos;
	}
	public void setNoticeRuleNos(Map<String, String> noticeRuleNos) {
		this.noticeRuleNos = noticeRuleNos;
	}
	public Map<String, Boolean> getUseStatuses() {
		return useStatuses;
	}
	public void setUseStatuses(Map<String, Boolean> useStatuses) {
		this.useStatuses = useStatuses;
	}
	public NoticeRuleDTO getNoticeRuleDTO() {
		return noticeRuleDTO;
	}
	public void setNoticeRuleDTO(NoticeRuleDTO noticeRuleDTO) {
		this.noticeRuleDTO = noticeRuleDTO;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public String getSidx() {
		return sidx;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public PageDTO getNoticeRules() {
		return noticeRules;
	}
	public void setNoticeRules(PageDTO noticeRules) {
		this.noticeRules = noticeRules;
	}
	public Long[] getIds() {
		return ids;
	}
	public void setIds(Long[] ids) {
		this.ids = ids;
	}
	public List<String> getLoginName() {
		return loginName;
	}
	public void setLoginName(List<String> loginName) {
		this.loginName = loginName;
	}
	public String getTitleString() {
		return titleString;
	}
	public void setTitleString(String titleString) {
		this.titleString = titleString;
	}
	public String getContentString() {
		return contentString;
	}
	public void setContentString(String contentString) {
		this.contentString = contentString;
	}
	public EmailTemplatesDTO getEmailTemplatesDTO() {
		return emailTemplatesDTO;
	}
	public void setEmailTemplatesDTO(EmailTemplatesDTO emailTemplatesDTO) {
		this.emailTemplatesDTO = emailTemplatesDTO;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public TemplateVariableDTO getTemplateVariableDTO() {
		return templateVariableDTO;
	}
	public void setTemplateVariableDTO(TemplateVariableDTO templateVariableDTO) {
		this.templateVariableDTO = templateVariableDTO;
	}
	/**
	 * 分页查询
	 * @return PageDTO
	 */
	public String findPager(){
		int start = (page - 1) * rows;
		if("all".equals(status)){
			noticeRuleDTO.setUseStatus(null);
		}
		noticeRules = noticeRuleService.findPagerNoticeRule(noticeRuleDTO, start,
                rows, sord, sidx);
		noticeRules.setRows(rows);
		noticeRules.setPage(page);
		
		return SUCCESS;
	};

	
	/**
	 * 更新
	 * @return String
	 */
	public String noticeRuleUpdate(){
		noticeRuleService.updateNoticeRule(useStatuses,noticeRuleNos,noticeRuleDTO.getUrl());
		return SUCCESS;
	}

	
	/**
	 * 根据模板名称查询邮件模板内容和标题
	 * @return edit page
	 */
	public String findByFileName(){
		noticeRuleDTO = noticeRuleService.findByFileName(noticeRuleDTO.getNoticeRuleId());
		return "editNoticeRule";
	}
	/**
	 * 审批加签
	 * @return boolean
	 */
	public String apporCountersign(){
		result=noticeRuleService.apporCountersign(noticeRuleDTO,templateVariableDTO);
		return "result";
	}
	/**
	 * 修改通知规则
	 * @return null
	 */
	public String editNotice(){
		noticeRuleService.editNoticeMethod(noticeRuleDTO);
		return "resultNotice";
	}

	/**
	 * 模板预览
	 * @return html content
	 */
	public String templatePreview(){
		try{
			contentString = noticeRuleService.templatePreview(templatesType,contentString);
		}catch (Exception e) {
			LOG.error("", e);
			throw new ApplicationException("Template_content_malformed");
		}
		
		return "content";
	}
	
	/**
	 * verifiReturnVisit
	 * @return String
	 */
	public String verifiReturnVisit(){
		result=noticeRuleService.verifiReturnVisit();
		return "result";
	}
	/**
	 * 取得用户自定义模板
	 * @return to add notice rule page
	 */
	public String findByNoticesModel(){
		noticeRuleDTO = noticeRuleService.findByNoticesModel(noticeRuleDTO, "notices_model", "NoModule");
		return "addNoticeRule";
	}
	
	/**
	 * 新增通知规则
	 * @return null
	 */
	public String addNotices(){
		noticeRuleService.addNoticesRule(noticeRuleDTO);
		return "resultNotice";
	}
	
	/**
	 * 根据Id查询NoticeRule
	 * @return noticeRuleDTO
	 */
	public String findById(){
		noticeRuleDTO = noticeRuleService.findById(noticeRuleDTO.getNoticeRuleId());
		return "noticeRuleDTO";
	}
	
	
	/**
	 * 批量删除
	 * @return null
	 */
	public String delNoticeRules(){
		noticeRuleService.delsNoticeRules(ids);
		return "resultNotice";
	}
	/**
	 * 回访验证
	 * @return testErrorEncode
	 */
	/*public String testemai(){
		emailConnectionDto = emailService.assignmentEmailServerDTO(emailConnectionDto);
		testErrorEncode = emailService.emailConnTest(emailConnectionDto);
        return "testErrorEncode";
	}*/
	//scott 2015-06-16 原有验证方法是直接调用普通邮件方式，并没有验证exchange的连接
	public String testemai(){
		testErrorEncode = emailService.testEmailConn(emailConnectionDto,null);
        return "testErrorEncode";
	}
	
	/**消息推送的ClientId绑定
	 * @return pushBind
	 */
	public String pushUserBind(){
		noticeRuleService.pushUserBind(userName, clientId);
		return SUCCESS;
	}
	/**消息数量修改
	 * @return messageCount
	 */
	public String pushCount(){
		noticeRuleService.pushCount(userName, count);
		return SUCCESS;
	}
}
