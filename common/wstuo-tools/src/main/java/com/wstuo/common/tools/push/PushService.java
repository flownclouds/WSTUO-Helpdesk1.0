package com.wstuo.common.tools.push;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.wstuo.common.noticeRule.service.NoticeRuleService;

import com.wstuo.common.tools.dto.PushMessageDTO;
import com.wstuo.common.tools.push.getui.GeTuiService;
import com.wstuo.common.util.StringUtils;

/**
 * 推送消息主方法；
 * @author gs60
 *
 */
public class PushService implements IPushService {
	private final static Logger LOGGER = Logger
			.getLogger(NoticeRuleService.class);
	@Autowired
	private GeTuiService geTuiService;

	/**
	 * 推送业务实现方法
	 */
	@Override
	public void pushMessage( PushMessageDTO dto ) {
		if ( checkPushParam(dto) ) {
			geTuiService.pushMessage( dto);
		}
	}
	public void pushMessage( Set<PushMessageDTO> dtos ) {
		if (( dtos = checkPushParam(dtos)).size() > 0) {
			geTuiService.pushMessage( dtos );
		}
	}

	/**
	 * 检查推送消息DTO参数是否正确；
	 * @param dto
	 * @return
	 */
	public boolean checkPushParam( PushMessageDTO dto ){
		LOGGER.debug(dto);
		boolean result = false;
		if (dto != null && dto.getAccept() != null 
				//&& StringUtils.hasText( dto.getTitle() )//标题已有默认值，可以无需校验
				&& StringUtils.hasText( dto.getContent() )
				) {
			result = true;
		}
		return result;
	}
	
	public Set<PushMessageDTO> checkPushParam( Set<PushMessageDTO> dtos ){
		Set<PushMessageDTO> dtoResult = new HashSet<PushMessageDTO>();
		for (PushMessageDTO dto : dtos) {
			if (checkPushParam(dto)) {
				dtoResult.add(dto);
			}
		}
		return dtoResult;
	}
}
