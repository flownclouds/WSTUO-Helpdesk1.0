package com.wstuo.common.tools.action;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.tools.dto.EventAttachmentDTO;
import com.wstuo.common.tools.service.IEventAttachmentService;
import com.wstuo.common.security.dao.IUserDAO;
import com.wstuo.common.security.entity.User;

/**
 * Event Attachment Action class
 * @author WSTUO
 * 
 */
@SuppressWarnings("serial")
public class EventAttachmentAction extends ActionSupport {
	@Autowired
	private IEventAttachmentService eventAttachmentService;
	private List<EventAttachmentDTO> eventAttachmentDtos;
	private EventAttachmentDTO eventAttachmentDto;
	private String attachmentStr;
	private Long eno;
	private String eventType;
	private Long[] ids;
	private String creator;
	@Autowired
	private IUserDAO userDAO;
	private String filePath;
	private Long aid;

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Long getAid() {
		return aid;
	}

	public void setAid(Long aid) {
		this.aid = aid;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public List<EventAttachmentDTO> getEventAttachmentDtos() {
		return eventAttachmentDtos;
	}

	public void setEventAttachmentDtos(
			List<EventAttachmentDTO> eventAttachmentDtos) {
		this.eventAttachmentDtos = eventAttachmentDtos;
	}

	public EventAttachmentDTO getEventAttachmentDto() {
		return eventAttachmentDto;
	}

	public void setEventAttachmentDto(EventAttachmentDTO eventAttachmentDto) {
		this.eventAttachmentDto = eventAttachmentDto;
	}

	public String getAttachmentStr() {
		return attachmentStr;
	}

	public void setAttachmentStr(String attachmentStr) {
		this.attachmentStr = attachmentStr;
	}

	public Long getEno() {
		return eno;
	}

	public void setEno(Long eno) {
		this.eno = eno;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public Long[] getIds() {
		return ids;
	}

	public void setIds(Long[] ids) {
		this.ids = ids;
	}

	/**
	 * 查询全部附件
	 * @return String
	 */
	public String findAllEventAttachment() {
		eventAttachmentDtos = eventAttachmentService
				.findAllEventAttachment(eventAttachmentDto);
		return SUCCESS;
	}

	/**
	 * 删除附件
	 * @return String
	 */
	public String deleteEventAttachment() {
		eventAttachmentService.deleteEventAttachment(ids, eno);
		return SUCCESS;
	}

	/**
	 * 保存附件
	 * @return String
	 */
	public String saveEventAttachment() {
		User user = userDAO.findUniqueBy("loginName", creator);
		if (user == null) {
			user = new User();
			user.setFullName("");
		}
		eventAttachmentService.saveEventAttachment(user.getLoginName(), ids,
				attachmentStr, eno, eventType);
		return SUCCESS;
	}

	/**
	 * 删除附件和数据库
	 * @return String
	 */
	public String deleteByUrl() {
		// 先根据id去查出他的requestid,再做删除数据库,最后删除文件
		filePath=eventAttachmentService.deleteEventAttachmentByAid(aid,filePath);
		return "deleteAttachment";
/*		StringBuffer fileName = new StringBuffer();
		fileName.append(AppConfigUtils.getInstance().getAttachmentPath()).append("/").append(filePath);
		File file = new File(fileName.toString());
		if (!file.isDirectory()) {
			file.delete();
		}*/
	}
}
