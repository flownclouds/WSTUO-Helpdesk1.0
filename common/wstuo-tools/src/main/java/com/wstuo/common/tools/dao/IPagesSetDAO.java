package com.wstuo.common.tools.dao;

import com.wstuo.common.tools.entity.PagesSet;
import com.wstuo.common.dao.IEntityDAO;

/**
 * 页面设置DAO
 * @author WSTUO
 *
 */
public interface IPagesSetDAO extends IEntityDAO<PagesSet> {

}
