package com.wstuo.common.tools.service;

import java.util.List;

import com.wstuo.common.tools.dto.CostDTO;
import com.wstuo.common.tools.dto.TaskDTO;
import com.wstuo.common.dto.PageDTO;

/**
 * cost service interface class
 * @author WSTUO
 *
 */
public interface ICostService {
	
	/**
	 * 成本分页查询
	 * @param dto
	 * @param start
	 * @param limit
	 * @param sidx
	 * @param sord
	 * @return PageDTO
	 */
    public PageDTO findPagerCost( CostDTO dto, int start, int limit,String sidx,String sord );
    /**
     * 保存成本
     * @param dto
     */
    public void saveCost(CostDTO dto);
    /**
     * 编辑成本
     * @param dto
     */
    public void editCost(CostDTO dto);
    /**
     * 删除成本
     * @param ids
     */
    void deleteCost(final Long[] ids);
    void deleteCost(final List<CostDTO> dtos);
    /**
     *根据任务ID删除进展及成本
     * @param ids
     */
    void deleteCostBySchedule(List<Long> scheduleTaskIds);
    /**
     * 根据ID查找进展及成本
     * @param id
     * @return CostDTO
     */
    CostDTO findCostById(Long id);
    
    
    CostDTO findScheduleCost(TaskDTO dto);
    List<CostDTO> findTaskCostWithTaskId(Long taskId);
}
