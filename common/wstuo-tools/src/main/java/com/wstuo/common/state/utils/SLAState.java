package com.wstuo.common.state.utils;

public enum SLAState {

	/**
	 * 挂起
	 */
	Hang_Up("RequestGetPending"),
	/**
	 * 待响应
	 */
	The_Response("ToBeResponded"),
	/**
	 * 超时未响应
	 */
	Overtime_Is_Not_Responding("ToBeRespondedOverdue"),
	/**
	 * 未完成
	 */
	Unfinished("ToBeSettledOverdue"),
	/**
	 * 其他的
	 */
	OTHER("Other"), // 其他
	/**
	 * 超时响应
	 */
	The_Timeout("RespondedOverdue"),
	/**
	 * 正常响应
	 */
	Normal_Response("RespondedOnTime"),
	/**
	 * 待完成
	 */
	To_Be_Bcompleted("ToBeSettled"),
	/**
	 * 正常完成
	 */
	The_Normal_Completion("SettledOnTime"),
	/**
	 * 超时完成
	 */
	Time("SettledOverdue");

	// 定义私有变量
	private String nCode;

	// 构造函数，枚举类型只能为私有
	private SLAState(String _nCode) {
		this.nCode = _nCode;
	}

	public String getValue() {
		return nCode;
	}

}
