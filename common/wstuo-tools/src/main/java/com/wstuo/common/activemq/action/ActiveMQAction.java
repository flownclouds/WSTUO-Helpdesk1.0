package com.wstuo.common.activemq.action;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.activemq.dto.ActiveMQInfoDTO;
import com.wstuo.common.activemq.dto.ActiveMQMessageInfoDTO;
import com.wstuo.common.activemq.dto.QueueConfigureDTO;
import com.wstuo.common.activemq.service.IActiveMQService;
import com.wstuo.common.exception.ApplicationException;

/**
 * MQ Action 类
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class ActiveMQAction extends ActionSupport{
	@Autowired
	private IActiveMQService activeMQService;
	private List<ActiveMQInfoDTO> dtos;
	private List<ActiveMQMessageInfoDTO> messages;
	private String queueName;
	private String messageId;
	private Boolean result = false;
	private QueueConfigureDTO queueConfigureDTO;
	
	
	public QueueConfigureDTO getQueueConfigureDTO() {
		return queueConfigureDTO;
	}
	public void setQueueConfigureDTO(QueueConfigureDTO queueConfigureDTO) {
		this.queueConfigureDTO = queueConfigureDTO;
	}
	public Boolean getResult() {
		return result;
	}
	public void setResult(Boolean result) {
		this.result = result;
	}
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public List<ActiveMQMessageInfoDTO> getMessages() {
		return messages;
	}
	public void setMessages(List<ActiveMQMessageInfoDTO> messages) {
		this.messages = messages;
	}
	public String getQueueName() {
		return queueName;
	}
	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}
	public List<ActiveMQInfoDTO> getDtos() {
		return dtos;
	}
	public void setDtos(List<ActiveMQInfoDTO> dtos) {
		this.dtos = dtos;
	}
	/**
	 * 获取全部队列
	 * @return List<ActiveMQInfoDTO> JSON DATA
	 */
	public String showQueues(){
		try{
			dtos=activeMQService.showQueues();
		 }catch(Exception ex){
	        throw new ApplicationException("label_error_jmsException");
		}
		return SUCCESS;
	}
	/**
	 * 查询队列中的所有消息
	 * @return List<ActiveMQMessageInfoDTO>  JSON DATA
	 */
	public String showQueueInfo(){
		messages=activeMQService.showQueueinfo(queueName);
		return "MESSAGE";
	}
	/**
	 * 删除消息
	 * @return boolean type result
	 */
	public String deleteMessage(){
		result=activeMQService.deleteMessage(messageId, queueName);
		return "DELETESUCCESS";
	}
	/**
	 * 查询队列状态
	 * @return boolean type result
	 */
	public String findQueueStatus(){
		result=activeMQService.findQueueStatus(queueName);
		return "result";
	}
	/**
	 * 查找队列参数配置信息
	 * @return 队列配置详细信息
	 */
	public String findQueueConfigure(){
		queueConfigureDTO=activeMQService.findQueueConfigure();
		return "QueueConfigureFind";
	}
	
	/**
	 * 保存队列配置参数
	 * @return null
	 */
	public String saveQueueConfigure(){
		activeMQService.saveQueueConfigure(queueConfigureDTO);
		return SUCCESS;
	}
}
