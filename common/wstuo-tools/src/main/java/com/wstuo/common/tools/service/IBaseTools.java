package com.wstuo.common.tools.service;

import com.wstuo.common.tools.dto.EmailConnectionDTO;
import com.wstuo.common.tools.dto.EmailServerDTO;
import com.wstuo.common.tools.dto.SMSAccountDTO;

/**
 * 公共的工具类
 * @author QXY
 * @date 20140719
 */
public interface IBaseTools {

	/**
	 * 使用Base64进行解码
	 * @param in 需要解码的字符串
	 * @return 返回解码后的字符串，如果解码失败，返回 in
	 */
	public String base64Decode(String in);
	/**
	 * 使用Base64进行编码
	 * @param in 需要编码的字符串
	 * @return 返回编码后的字符串，如果编码失败，返回 in
	 */
	public String base64Encode(String in);

	/**
	 * 使用Base64编码EmailConnectionDTO的密码属性
	 * 
	 * @param dto
	 */
	public void encodeDTO(EmailConnectionDTO dto);

	/**
	 * 使用Base64解码EmailConnectionDTO的密码属性
	 * 
	 * @param dto
	 */
	public void decodeDTO(EmailConnectionDTO dto);

	/**
	 * 使用Base64编码EmailServerDTO的密码属性
	 * 
	 * @param dto
	 */
	public void encodeDTO(EmailServerDTO dto) ;

	/**
	 * 使用Base64解码EmailServerDTO的密码属性
	 * 
	 * @param dto
	 */
	public void decodeDTO(EmailServerDTO dto) ;
	/**
	 * 使用Base64编码SMSAccountDTO的密码属性
	 * 
	 * @param dto
	 */
	public void encodeDTO(SMSAccountDTO dto) ;

	/**
	 * 使用Base64解码SMSAccountDTO的密码属性
	 * 
	 * @param dto
	 */
	public void decodeDTO(SMSAccountDTO dto) ;
}
