package com.wstuo.common.customForm.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.wstuo.common.entity.BaseEntity;

@SuppressWarnings({ "rawtypes", "serial" })
@Entity
public class Field extends BaseEntity{
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
	private Long id;//ID
	private String module;//模块 request
	private String name;//字段名称 自动生成
	private String fieldName;//字段显示名称
	private String type;//字段类型 text int
	
	private Boolean required; //是否必填
	private Boolean showList; //是否显示列表上
	private Boolean export; //是否导出
	private Boolean search; //是否搜索
	private int sort; //排序
	private String dataDictionary;//关联数据字典

	

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public Boolean getShowList() {
		return showList;
	}

	public void setShowList(Boolean showList) {
		this.showList = showList;
	}

	public Boolean getExport() {
		return export;
	}

	public void setExport(Boolean export) {
		this.export = export;
	}

	public Boolean getSearch() {
		return search;
	}

	public void setSearch(Boolean search) {
		this.search = search;
	}

	public Boolean getRequired() {
		return required;
	}

	public void setRequired(Boolean required) {
		this.required = required;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setDataDictionary(String dataDictionary) {
		this.dataDictionary = dataDictionary;
	}

	public String getDataDictionary() {
		return dataDictionary;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	
}
