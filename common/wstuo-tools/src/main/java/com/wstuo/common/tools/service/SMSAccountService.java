package com.wstuo.common.tools.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.tools.dao.ISMSAccountDAO;
import com.wstuo.common.tools.dto.SMSAccountDTO;
import com.wstuo.common.tools.entity.SMSAccount;
import com.wstuo.common.tools.sms.SMSSenderHelper;
/**
 * 短信连接信息帐号Service类
 * @author QXY
 */
public class SMSAccountService implements ISMSAccountService{

	@Autowired
	private ISMSAccountDAO smsAccountDAO;
	@Autowired
	private SMSSenderHelper smsSenderHelper;

	/**
	 * 获取短信帐号信息
	 * @return SMSAccountDTO
	 */
	public SMSAccountDTO findSMSAccount() {
		List<SMSAccount> accounts=smsAccountDAO.findAll();
		
		SMSAccountDTO dto=new SMSAccountDTO();
		
		if(accounts != null && accounts.size()>0){
			
			SMSAccountDTO.entity2dto(accounts.get(0), dto);
		}
		return dto;
	}

	/**
	 * 保存或更新短信信息
	 * @param dto
	 */
	@Transactional()
	public void saveOrUpdateSMSAccount(SMSAccountDTO dto) {
		SMSAccount account=null;
		if(dto.getSaid()!=null){
			account=smsAccountDAO.findById(dto.getSaid());
		}
		if(account==null){
			account=new SMSAccount();
		}
		SMSAccountDTO.dto2entity(dto, account);
		smsAccountDAO.merge(account);
	}
	

	/**
	 * 根据系统状态测试连接。
	 * @return true连接成功，false连接失败
	 */
	public Boolean testConnection(){
		SMSAccountDTO smsAccountDTO=findSMSAccount();
		return smsSenderHelper.testConnection(smsAccountDTO);
	}

}
