package com.wstuo.common.activemq.consumer;


public interface IMessageConsumer {

	void messageConsumer(Object obj);
}
