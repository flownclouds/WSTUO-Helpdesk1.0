package com.wstuo.common.tools.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * 排班管理显示
 * 
 * @author wing
 * 
 */
public class TaskViewDTO {
	// 全部排班最大的时间
	private String minimumTime;
	// 全部排班最大的时间
	private String maximumTime;
	//排班管理数据
    private List<TaskShowDataDTO> taskShowDataDTO=new ArrayList<TaskShowDataDTO>();
	public String getMinimumTime() {
		return minimumTime;
	}
	public void setMinimumTime(String minimumTime) {
		this.minimumTime = minimumTime;
	}
	public String getMaximumTime() {
		return maximumTime;
	}
	public void setMaximumTime(String maximumTime) {
		this.maximumTime = maximumTime;
	}
	public List<TaskShowDataDTO> getTaskShowDataDTO() {
		return taskShowDataDTO;
	}
	public void setTaskShowDataDTO(List<TaskShowDataDTO> taskShowDataDTO) {
		this.taskShowDataDTO = taskShowDataDTO;
	}
    
    
    

}
