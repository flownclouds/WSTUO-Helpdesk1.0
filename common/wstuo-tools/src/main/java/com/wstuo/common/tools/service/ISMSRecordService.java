package com.wstuo.common.tools.service;

import com.wstuo.common.tools.dto.SMSRecordDTO;
import com.wstuo.common.dto.PageDTO;

/**
 * 短信发送记录Service接口
 * @author QXY
 *
 */
public interface ISMSRecordService {

	/**
	 * 分页查询信息记录.
	 * @param qdto
	 * @param start
	 * @param limit
	 * @param sidx
	 * @param sord
	 * @return PageDTO
	 */
	PageDTO findPager(SMSRecordDTO qdto, int start, int limit,
			String sidx, String sord);

	/**
	 * 保存短信发送记录.
	 * @param dto
	 */
	void save(SMSRecordDTO dto);

	/**
	 * 批量删除.
	 * @param ids
	 */
	void delete(Long[] ids);
	
	/**
	 * 保存历史信息.
	 * @param mobile 手机号码
	 * @param message 信息内容
	 * @param isSystem false 为手动发送，true为系统发送
	 */
	void saveSMSHistory(String mobile,String message,boolean isSystem);

}