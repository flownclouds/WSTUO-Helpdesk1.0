package com.wstuo.common.tools.dto;

import java.util.Date;

import com.wstuo.common.dto.BaseDTO;

/**
 * 用户回访结果查询条件
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class UserReturnVisitQueryDTO extends BaseDTO {
	private Long visitId;
	private Long satisfaction;
	private Long state;
	private Date returnVisitSubmitTimeStart;
	private Date returnVisitSubmitTimeEnd;
	private Date returnVisitTimeStart;
	private Date returnVisitTimeEnd;
	private Long returnVisitMode;
	private String returnVisitUser;
	private String returnVisitSubmitUser;
	private Integer start;
	private Integer limit;
	private Long eno;
	
	
	public Long getEno() {
		return eno;
	}
	public void setEno(Long eno) {
		this.eno = eno;
	}
	public Long getVisitId() {
		return visitId;
	}
	public void setVisitId(Long visitId) {
		this.visitId = visitId;
	}
	public Long getSatisfaction() {
		return satisfaction;
	}
	public void setSatisfaction(Long satisfaction) {
		this.satisfaction = satisfaction;
	}
	public Long getState() {
		return state;
	}
	public void setState(Long state) {
		this.state = state;
	}
	public Date getReturnVisitSubmitTimeStart() {
		return returnVisitSubmitTimeStart;
	}
	public void setReturnVisitSubmitTimeStart(Date returnVisitSubmitTimeStart) {
		this.returnVisitSubmitTimeStart = returnVisitSubmitTimeStart;
	}
	public Date getReturnVisitSubmitTimeEnd() {
		return returnVisitSubmitTimeEnd;
	}
	public void setReturnVisitSubmitTimeEnd(Date returnVisitSubmitTimeEnd) {
		this.returnVisitSubmitTimeEnd = returnVisitSubmitTimeEnd;
	}
	public Date getReturnVisitTimeStart() {
		return returnVisitTimeStart;
	}
	public void setReturnVisitTimeStart(Date returnVisitTimeStart) {
		this.returnVisitTimeStart = returnVisitTimeStart;
	}
	public Date getReturnVisitTimeEnd() {
		return returnVisitTimeEnd;
	}
	public void setReturnVisitTimeEnd(Date returnVisitTimeEnd) {
		this.returnVisitTimeEnd = returnVisitTimeEnd;
	}

	public Long getReturnVisitMode() {
		return returnVisitMode;
	}
	public void setReturnVisitMode(Long returnVisitMode) {
		this.returnVisitMode = returnVisitMode;
	}
	public String getReturnVisitUser() {
		return returnVisitUser;
	}
	public void setReturnVisitUser(String returnVisitUser) {
		this.returnVisitUser = returnVisitUser;
	}
	public String getReturnVisitSubmitUser() {
		return returnVisitSubmitUser;
	}
	public void setReturnVisitSubmitUser(String returnVisitSubmitUser) {
		this.returnVisitSubmitUser = returnVisitSubmitUser;
	}
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	
}
