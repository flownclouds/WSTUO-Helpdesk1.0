package com.wstuo.common.tools.service;

import com.wstuo.common.tools.entity.PagesSet;

/**
 * 页面设置Service接口
 * @author Candy
 *
 */
public interface IPagesSetSerice {
	public void updatePagesSet(PagesSet entity);
	public PagesSet showPagesSet();
}
