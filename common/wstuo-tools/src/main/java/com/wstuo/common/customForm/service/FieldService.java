package com.wstuo.common.customForm.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.config.dictionary.dao.IDataDictionaryGroupDAO;
import com.wstuo.common.config.dictionary.entity.DataDictionaryGroup;
import com.wstuo.common.config.dictionary.entity.DataDictionaryItems;
import com.wstuo.common.customForm.dao.IFieldDAO;
import com.wstuo.common.customForm.dto.FieldDTO;
import com.wstuo.common.customForm.entity.Field;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.StringUtils;

public class FieldService implements IFieldService{
	@Autowired
	private IFieldDAO fieldDAO;
	@Autowired
	private IDataDictionaryGroupDAO dataDictionaryGroupDAO;
	public PageDTO findPageField(Field field,int start,int limit,String sidx,String  sord) {
		return fieldDAO.findPageField(field, start, limit,sidx,sord);
	}

	@Transactional
	public boolean add(Field field) {
		fieldDAO.save(field);
		field.setName(field.getModule()+field.getId());
		
		int result=fieldDAO.addField(field.getModule(), field.getName(),"");// getType(field.getType())
		return result==0?true:false;
	}
	
	@Transactional
	public boolean update(Field field) {
		fieldDAO.update(field);
		return true;
	}

	@Transactional
	public boolean del(Long[] ids) {
		for (int i = 0; i < ids.length; i++) {
			Field field=fieldDAO.findById(ids[i]);
			fieldDAO.delField(field.getModule(), field.getName());
		}
		fieldDAO.deleteByIds(ids);
		return true;
	}

	public Field findfieldById(Long id) {
		return fieldDAO.findById(id);
	}
	
	public PageDTO findByCustom(String table,int start,int limit,String sidx,String  sord){
		return fieldDAO.findByCustom(table,start,limit,sidx,sord);
	}
	public List<FieldDTO> findAll(String module){
		Field f =new Field();
		f.setModule(module);
		List<Field> list= fieldDAO.findListField( f,null);
		List<FieldDTO> dtos=new ArrayList<FieldDTO>();
		for (Field field : list) {
			FieldDTO dto=new FieldDTO();
			FieldDTO.entity2dto(field, dto);
			/*if(StringUtils.hasText(field.getDataDictionary())){
				DataDictionaryGroup group = dataDictionaryGroupDAO.findUniqueBy(
						"groupCode", field.getDataDictionary());
				List<DataDictionaryItems> entities = group.getDatadicItems();
				StringBuffer itemName=new StringBuffer();
				StringBuffer itemNo=new StringBuffer();
				for (DataDictionaryItems entity : entities) {
					itemName.append(','+entity.getDname());
					itemNo.append(','+entity.getDcode());
				}
				if(itemName.toString().length()>0){
					dto.setAttrItemName(itemName.toString().substring(1));
					dto.setAttrItemNo(itemNo.toString().substring(1));
				}
			}*/
			dtos.add(dto);
		}
		return dtos;
	}
	private String getType(String type){
		switch (type) {
		case "text":
			return "varchar(255)";
		case "textarea":
			return "LONGTEXT";
		case "number":
			return "int(10)";
		case "double":
			return "FLOAT";
		case "date":
			return "DATETIME";
		case "select":
			return "BIGINT(20)";
		case "radio":
			return "BIGINT(20)";
		case "tree":
			return "BIGINT(20)";
		default:
			return "varchar(255)";
		}
	}
}
