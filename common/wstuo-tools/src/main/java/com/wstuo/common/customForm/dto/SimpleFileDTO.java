package com.wstuo.common.customForm.dto;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;

public class SimpleFileDTO {

	private String fileName;
	private InputStream inputStream;
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName,String extension) {
		if( fileName != null ){
			String name = fileName + extension;
			try {
				//导出文件的文件名中文乱码转码
				this.fileName = new String(name.getBytes("GBK"),"ISO-8859-1");
			} catch (UnsupportedEncodingException e) {
				this.fileName = name;
			}
		}
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public InputStream getInputStream() {
		return inputStream;
	}
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}
}
