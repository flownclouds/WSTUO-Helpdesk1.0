package com.wstuo.common.noticeRule.dto;

import java.util.List;

import com.wstuo.common.dto.BaseDTO;
import com.wstuo.common.security.entity.Organization;
import com.wstuo.common.security.entity.User;
/**
 * 通知信息DTO类
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class NoticeInfoDTO extends BaseDTO {
	//模板变量
	private TemplateVariableDTO templateVariableDTO = new TemplateVariableDTO();
	//通知规则ID
	private Long noticeRuleId;
	//通知规则编号
	private String noticeRuleNo;
	//请求人、报告人
	private User requester;
	//指派的技术员
	private User assignTechnician;
	//升级到或负责人
	private User owner;
	//指派的机构
	private Organization assignOrg;
	//要通知的用户(只要有值都会进行通知)
	private List<User> noticeUsers;
	//要通知的机构(只要有值都会进行通知)
	private List<Organization> noticeOrgs;
	//拥有者
	private User ciOwner;
	//使用人
	private User ciUse;
	
	//要通知的用户
	private List<String> loginName;
	//要通知的用户ID
	private Long userNo;
	//要通知的机构编号ID
	private Long orgNo;
	//需要发送的邮件地址
	private String emailAddress;
	//技术员
	private String[] technician;
	//任务指派的机构
	private Organization taskAssignOrg;
	//任务指派的技术员
	private User taskAssignTechnician;
	//通知模板变量
	private Object variables;
	
	public void setVariables(Object variables) {
		this.variables = variables;
	}
	public TemplateVariableDTO getTemplateVariableDTO() {
		return templateVariableDTO;
	}
	public void setTemplateVariableDTO(TemplateVariableDTO templateVariableDTO) {
		this.templateVariableDTO = templateVariableDTO;
	}
	public String getNoticeRuleNo() {
		return noticeRuleNo;
	}
	public void setNoticeRuleNo(String noticeRuleNo) {
		this.noticeRuleNo = noticeRuleNo;
	}
	public List<String> getLoginName() {
		return loginName;
	}
	public void setLoginName(List<String> loginName) {
		this.loginName = loginName;
	}
	public Long getUserNo() {
		return userNo;
	}
	public void setUserNo(Long userNo) {
		this.userNo = userNo;
	}
	public Long getOrgNo() {
		return orgNo;
	}
	public void setOrgNo(Long orgNo) {
		this.orgNo = orgNo;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public Long getNoticeRuleId() {
		return noticeRuleId;
	}
	public void setNoticeRuleId(Long noticeRuleId) {
		this.noticeRuleId = noticeRuleId;
	}
	public String[] getTechnician() {
		return technician;
	}
	public void setTechnician(String[] technician) {
		this.technician = technician;
	}
	public User getRequester() {
		return requester;
	}
	public void setRequester(User requester) {
		this.requester = requester;
	}
	public User getAssignTechnician() {
		return assignTechnician;
	}
	public void setAssignTechnician(User assignTechnician) {
		this.assignTechnician = assignTechnician;
	}
	public Organization getAssignOrg() {
		return assignOrg;
	}
	public void setAssignOrg(Organization assignOrg) {
		this.assignOrg = assignOrg;
	}
	public List<User> getNoticeUsers() {
		return noticeUsers;
	}
	public void setNoticeUsers(List<User> noticeUsers) {
		this.noticeUsers = noticeUsers;
	}
	public List<Organization> getNoticeOrgs() {
		return noticeOrgs;
	}
	public void setNoticeOrgs(List<Organization> noticeOrgs) {
		this.noticeOrgs = noticeOrgs;
	}
	public User getCiOwner() {
		return ciOwner;
	}
	public void setCiOwner(User ciOwner) {
		this.ciOwner = ciOwner;
	}
	public User getCiUse() {
		return ciUse;
	}
	public void setCiUse(User ciUse) {
		this.ciUse = ciUse;
	}
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
	public Organization getTaskAssignOrg() {
		return taskAssignOrg;
	}
	public void setTaskAssignOrg(Organization taskAssignOrg) {
		this.taskAssignOrg = taskAssignOrg;
	}
	public User getTaskAssignTechnician() {
		return taskAssignTechnician;
	}
	public void setTaskAssignTechnician(User taskAssignTechnician) {
		this.taskAssignTechnician = taskAssignTechnician;
	}
	public Object getVariables() {
		return variables;
	}
	
}
