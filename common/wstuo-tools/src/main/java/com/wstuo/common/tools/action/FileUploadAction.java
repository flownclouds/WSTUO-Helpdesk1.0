package com.wstuo.common.tools.action;

import java.io.File;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.tools.dto.FileUploadDTO;
import com.wstuo.common.tools.service.IFileUpload;
import com.wstuo.common.tools.util.ToolsUtils;
import com.wstuo.common.security.utils.AppConfigUtils;

/**
 * 文件上传Action类.
 * 
 * @author QXY. 修改日期：2011-02-10
 */
public class FileUploadAction extends ActionSupport {
	final static Logger LOGGER = Logger.getLogger(FileUploadAction.class);
	private static final long serialVersionUID = 572146812454L;
	@Autowired
	private IFileUpload fileUpload;
	private File myFile;
	private String fileName;
	private String imageFileName;
	private String id;
	private String myFileName;
	private static final String REQUESTPATH = AppConfigUtils.getInstance()
			.getAddImagePath();
	private String imgSize;
	private String imageFileNametoreuqest;

	public String getImageFileNametoreuqest() {
		return imageFileNametoreuqest;
	}

	public void setImageFileNametoreuqest(String imageFileNametoreuqest) {
		this.imageFileNametoreuqest = imageFileNametoreuqest;
	}

	public String getImgSize() {
		return imgSize;
	}

	public void setImgSize(String imgSize) {
		this.imgSize = imgSize;
	}

	public String getMyFileName() {
		return myFileName;
	}

	public void setMyFileName(String myFileName) {
		this.myFileName = myFileName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setMyFileFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}

	public String getImageFileName() {
		return imageFileName;
	}

	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}

	

	public void fileUpload() {
		fileUpload(myFile, fileName);
	}

	public void fileUpload(File file, String fileName) {
		File outputJrxmlFile = new File(REQUESTPATH + "/"
				+ imageFileNametoreuqest);
		ToolsUtils.copy(file, outputJrxmlFile);
	}

	

	/**
	 * 执行上传操作.
	 */
	@Override
	public String execute() {
		FileUploadDTO fileUploadDTO = fileUpload.fileUpload(myFile, fileName, imageFileName, imgSize, myFileName);
		imageFileName = fileUploadDTO.getImageFileName();
		return fileUploadDTO.getResult();
	}
}
