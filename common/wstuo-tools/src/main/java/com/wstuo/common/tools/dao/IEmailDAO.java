package com.wstuo.common.tools.dao;

import com.wstuo.common.tools.dto.EmailMessageQueryDTO;
import com.wstuo.common.tools.entity.EmailMessage;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

/**
 * 邮箱信息DAO接口
 * @author Mars
 *
 */
public interface IEmailDAO
    extends IEntityDAO<EmailMessage>
{
    /**
     * 找数据库接收邮件的最后时间
     * @param receiveMail
     */
    public EmailMessage findLastestReceiveDate(String receiveMail);

    /**
     * 邮件信息分页查询
     * @param dto 查询条件DTO
     * @param start 查询第几页
     * @param limit 每页行数
     * @param sidx
     * @param sord
     * @return PageDTO
     */
    public PageDTO findPager( EmailMessageQueryDTO dto, int start, int limit, String sidx, String sord  );
}
