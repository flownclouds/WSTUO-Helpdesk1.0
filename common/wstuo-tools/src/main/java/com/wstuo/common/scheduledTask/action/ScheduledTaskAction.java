package com.wstuo.common.scheduledTask.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.scheduledTask.dto.ScheduledTaskDTO;
import com.wstuo.common.scheduledTask.dto.ScheduledTaskQueryDTO;
import com.wstuo.common.scheduledTask.dto.ScheduledTaskRequestDTO;
import com.wstuo.common.scheduledTask.service.IScheduledTaskService;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.exception.ApplicationException;

/**
 * 定期任务Action类
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class ScheduledTaskAction extends ActionSupport {
	@Autowired
	private IScheduledTaskService scheduledTaskService;
	private PageDTO pageDTO;
	private ScheduledTaskDTO scheduledTaskDTO;
	private ScheduledTaskQueryDTO queryDTO;
	private Long[] ids;
	private Long[] cinos;
	private Long[] serviceDirNos;
    private String sord;
    private String sidx;
    private int page = 1;
    private int rows = 10;
    private ScheduledTaskRequestDTO requestDTO;
    
	public Long[] getCinos() {
		return cinos;
	}
	public void setCinos(Long[] cinos) {
		this.cinos = cinos;
	}
	public PageDTO getPageDTO() {
		return pageDTO;
	}
	public void setPageDTO(PageDTO pageDTO) {
		this.pageDTO = pageDTO;
	}
	public ScheduledTaskDTO getScheduledTaskDTO() {
		return scheduledTaskDTO;
	}
	public void setScheduledTaskDTO(ScheduledTaskDTO scheduledTaskDTO) {
		this.scheduledTaskDTO = scheduledTaskDTO;
	}
	public ScheduledTaskQueryDTO getQueryDTO() {
		return queryDTO;
	}
	public void setQueryDTO(ScheduledTaskQueryDTO queryDTO) {
		this.queryDTO = queryDTO;
	}
	public Long[] getIds() {
		return ids;
	}
	public void setIds(Long[] ids) {
		this.ids = ids;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
    
	public ScheduledTaskRequestDTO getRequestDTO() {
		return requestDTO;
	}
	public void setRequestDTO(ScheduledTaskRequestDTO requestDTO) {
		this.requestDTO = requestDTO;
	}
	
	public Long[] getServiceDirNos() {
		return serviceDirNos;
	}
	public void setServiceDirNos(Long[] serviceDirNos) {
		this.serviceDirNos = serviceDirNos;
	}
	
	/**
	 * 赋值ScheduledTaskQueryDTO
	 */
	public void assignmentScheduledTaskQueryDTO(){
		int start = ( page - 1 ) * rows;
		queryDTO.setSidx(sidx);
		queryDTO.setSord(sord);
		queryDTO.setStart(start);
		queryDTO.setLimit(rows);
	}
	/**
	 * 分页查询
	 * @return PageDTO
	 */
	public String findPageScheduledTask(){
		assignmentScheduledTaskQueryDTO();
		pageDTO=scheduledTaskService.findPageScheduledTask(queryDTO);
		pageDTO.setPage(page);
		return SUCCESS;
	}
	/**
	 * 分页查询所有报表任务
	 * @return PageDTO
	 * */
	public String findPageScheduledTaskReportId(){
		assignmentScheduledTaskQueryDTO();
		queryDTO.setScheduledTaskType("senReport");
		pageDTO=scheduledTaskService.findPageScheduledTask(queryDTO);
		pageDTO.setPage(page);
		return SUCCESS;
	}
	/**
	 * 根据ID获取定期任务详细
	 * @return scheduledTaskDTO
	 */
	public String findScheduledTaskById(){
		scheduledTaskDTO=scheduledTaskService.findScheduledTaskById(queryDTO.getScheduledTaskId());
		return "scheduledTaskDTO";
	}
	
	
	/**
	 * 保存定期任务
	 * @return NULL
	 */
	public String saveScheduledTask(){
		scheduledTaskDTO = scheduledTaskService.convertScheduledTaskDTOValue(scheduledTaskDTO, requestDTO, cinos, serviceDirNos);
		scheduledTaskService.saveScheduledTask(scheduledTaskDTO);
		return SUCCESS;
	}
	
	/**
	 * 编辑定期任务
	 * @return null
	 */
	public String editScheduledTask(){
		scheduledTaskDTO = scheduledTaskService.convertScheduledTaskDTOValue(scheduledTaskDTO, requestDTO, cinos, serviceDirNos);
		scheduledTaskService.editScheduledTask(scheduledTaskDTO);
		return SUCCESS;
	}
	/**
	 * 删除定期任务
	 * @return null
	 */
	public String deleteScheduledTask(){
		try {
			scheduledTaskService.deleteScheduledTask(ids);
		}catch (Exception e) {
			throw new ApplicationException("ERROR_DATA_CAN_NOT_DELETE", e);
		}
		scheduledTaskService.removeJob(ids);
		return SUCCESS;
	}
	
	
}
