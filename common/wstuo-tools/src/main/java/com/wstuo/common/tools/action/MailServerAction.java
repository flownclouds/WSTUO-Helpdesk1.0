package com.wstuo.common.tools.action;

import com.opensymphony.xwork2.ActionSupport;

import com.wstuo.common.tools.dto.EmailServerDTO;
import com.wstuo.common.tools.service.IBaseTools;
import com.wstuo.common.tools.service.IEmailServerService;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * 服务邮箱Action
 * @author QXY
 *
 */
@SuppressWarnings( "serial" )
public class MailServerAction
    extends ActionSupport
{
    @Autowired
    private IEmailServerService emailServerService;
	@Autowired
	private IBaseTools baseTools;
    private EmailServerDTO emailServerDTO = new EmailServerDTO(  );

    public EmailServerDTO getEmailServerDTO(  )
    {
        return emailServerDTO;
    }

    public void setEmailServerDTO( EmailServerDTO emailServerDTO )
    {
        this.emailServerDTO = emailServerDTO;
    }

    /**
     * saveOrUpdateEmailServer.
     * @return String
     */
    public String saveOrUpdateEmailServer(  )
    {
		baseTools.decodeDTO( emailServerDTO );//密码解码
        emailServerService.saveOrUpdateEmailServer( emailServerDTO );

        return SUCCESS;
    }

    /**
     * findServereMail
     * @return String
     */
    public String findServereMail(  )
    {
        emailServerDTO = emailServerService.findEmail(  );
		baseTools.encodeDTO(emailServerDTO);//密码编码
        return SUCCESS;
    }
    /**
     * 查找发送邮箱
     * @return
     */
    public String findReceiveServiceMail(){
    	 emailServerDTO = emailServerService.findReceiveEmail();
 		 baseTools.encodeDTO(emailServerDTO);//密码编码
    	return SUCCESS;
    }
}
