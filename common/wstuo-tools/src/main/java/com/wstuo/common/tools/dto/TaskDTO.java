package com.wstuo.common.tools.dto;

import java.util.Date;

import com.wstuo.common.dto.BaseDTO;
/**
 * 任务信息DTO类
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class TaskDTO extends BaseDTO {
	private Long taskId;
	private String introduction;
	private Date startTime;
	private Date endTime;
	private Long taskStatus;
	private Long taskType;
	private String owner ;
	private Boolean allDay;
	private String title;
	private String location;
	private Long eno;
	private String creator;
	private Date createTime;
	private Date createdOn;
	private Date realStartTime;
	private Date realEndTime;
	private Double realFree;
	private String treatmentResults;//处理结果
	private String operator;
	private String taskCreator;
	private String ownerLoginName;
	
	//aaron 20140925
	private String eventType;
	private String etitle;
	private Date createDate;
	private String edesc;
	private String solutions;	
	private String technicianName; //技术员
	private String createdByName; //创建人
	private String planTime;	//计划时间
	private String type;
	private String dno;//任务的类型编码
	private String taskCycle;
	
	private Date startDate;
	private Date endDate;
	private Integer cycleCount;
	
	private String[] weekWeeks;
	private String cycleWeekEnd;
	private Long dCode;
	
	//cost
	private Long progressId;
	private CostDTO costDTO;

	private Byte dataFlag = 0; // 0:normal, 1:system, 2:test,99:delete
	
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Long getdCode() {
		return dCode;
	}
	public void setdCode(Long dCode) {
		this.dCode = dCode;
	}
	public String getDno() {
		if (this.dno != null && this.dno.equals("-1")) {
			this.setDno("task_personal");
		}
		return dno;
	}
	public void setDno(String dno) {
		this.dno = dno;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getPlanTime() {
		return planTime;
	}
	public void setPlanTime(String planTime) {
		this.planTime = planTime;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public String getEtitle() {
		return etitle;
	}
	public void setEtitle(String etitle) {
		this.etitle = etitle;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getEdesc() {
		return edesc;
	}
	public void setEdesc(String edesc) {
		this.edesc = edesc;
	}
	public String getSolutions() {
		return solutions;
	}
	public void setSolutions(String solutions) {
		this.solutions = solutions;
	}
	public String getTechnicianName() {
		return technicianName;
	}
	public void setTechnicianName(String technicianName) {
		this.technicianName = technicianName;
	}
	public String getCreatedByName() {
		return createdByName;
	}
	public void setCreatedByName(String createdByName) {
		this.createdByName = createdByName;
	}
	public String getOwnerLoginName() {
		return ownerLoginName;
	}
	public void setOwnerLoginName(String ownerLoginName) {
		this.ownerLoginName = ownerLoginName;
	}
	public String getTaskCreator() {
		return taskCreator;
	}
	public void setTaskCreator(String taskCreator) {
		this.taskCreator = taskCreator;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getRealStartTime() {
		return realStartTime;
	}
	public void setRealStartTime(Date realStartTime) {
		this.realStartTime = realStartTime;
	}
	public Date getRealEndTime() {
		return realEndTime;
	}
	public void setRealEndTime(Date realEndTime) {
		this.realEndTime = realEndTime;
	}
	public Double getRealFree() {
		return realFree;
	}
	public void setRealFree(Double realFree) {
		this.realFree = realFree;
	}
	public String getTreatmentResults() {
		return treatmentResults;
	}
	public void setTreatmentResults(String treatmentResults) {
		this.treatmentResults = treatmentResults;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getIntroduction() {
		return introduction;
	}
	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Long getEno() {
		return eno;
	}
	public void setEno(Long eno) {
		this.eno = eno;
	}
	public Long getTaskId() {
		return taskId;
	}
	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}
	public Long getTaskStatus() {
		if (taskStatus == null) {
			return 0L;
		}
		return taskStatus;
	}
	public void setTaskStatus(Long taskStatus) {
		if (taskStatus == null) {
			taskStatus = 0L;
		}
		this.taskStatus = taskStatus;
	}
	public Long getTaskType() {
		return taskType;
	}
	public void setTaskType(Long taskType) {
		this.taskType = taskType;
	}
	public Boolean getAllDay() {
		return allDay;
	}
	public void setAllDay(Boolean allDay) {
		this.allDay = allDay;
	}
	public String getTaskCycle() {
		return taskCycle;
	}
	public void setTaskCycle(String taskCycle) {
		this.taskCycle = taskCycle;
	}
	public String[] getWeekWeeks() {
		return weekWeeks;
	}
	public void setWeekWeeks(String[] weekWeeks) {
		this.weekWeeks = weekWeeks;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Integer getCycleCount() {
		return cycleCount;
	}
	public void setCycleCount(Integer cycleCount) {
		this.cycleCount = cycleCount;
	}
	public String getCycleWeekEnd() {
		return cycleWeekEnd;
	}
	public void setCycleWeekEnd(String cycleWeekEnd) {
		this.cycleWeekEnd = cycleWeekEnd;
	}
	public Long getProgressId() {
		return progressId;
	}
	public void setProgressId(Long progressId) {
		this.progressId = progressId;
	}
	public CostDTO getCostDTO() {
		return costDTO;
	}
	public void setCostDTO(CostDTO costDTO) {
		if (costDTO != null && costDTO.checkProgressId()) {
			this.setTaskStatus( costDTO.getStatus() );
			this.setRealStartTime( costDTO.getStartTime() );
			this.setRealEndTime( costDTO.getEndTime() );
		}else{
			this.setTaskStatus( 0L );
		}
		this.costDTO = costDTO;
	}
	@Override
	public String toString() {
		return "TaskDTO [taskId=" + taskId + ", introduction=" + introduction
				+ ", startTime=" + startTime + ", endTime=" + endTime
				+ ", taskStatus=" + taskStatus + ", taskType=" + taskType
				+ ", owner=" + owner + ", allDay=" + allDay + ", title="
				+ title + ", location=" + location + ", eno=" + eno
				+ ", creator=" + creator + ", createTime=" + createTime
				+ ", realStartTime=" + realStartTime + ", realEndTime="
				+ realEndTime + ", realFree=" + realFree
				+ ", treatmentResults=" + treatmentResults + ", operator="
				+ operator + ", taskCreator=" + taskCreator
				+ ", ownerLoginName=" + ownerLoginName + ", eventType="
				+ eventType + ", etitle=" + etitle + ", createDate="
				+ createDate + ", edesc=" + edesc + ", solutions=" + solutions
				+ ", technicianName=" + technicianName + ", createdByName="
				+ createdByName + ", planTime=" + planTime + ", type=" + type
				+ ", dno=" + dno + ", taskCycle=" + taskCycle +", startDate="
				+ startDate+ ", endDate=" + endDate + ", cycleCount=" + cycleCount  
				+ ", cycleWeekEnd =" +cycleWeekEnd + "]";
	}
	public Byte getDataFlag() {
		return dataFlag;
	}
	public void setDataFlag(Byte dataFlag) {
		this.dataFlag = dataFlag;
	}
	public static boolean validTask(TaskDTO task){
		if (task != null && task.getTaskId() != null 
				&& task.getStartTime() != null && task.getEndTime() != null
				) {
			return true;
		}
		return false;
	}
}
