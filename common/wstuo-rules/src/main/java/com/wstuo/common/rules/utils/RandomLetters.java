package com.wstuo.common.rules.utils;

import java.util.Random;

/**
 * 随机生成字母组成的字符串
 * @author QXY
 */
public class RandomLetters {
	/**
	 * @return 随机生成10个小写字母组成的一个字符串
	 */
    public static String getWord(  )
    {
        char[] A_Z =
            {
                'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
                'v', 'w', 'x', 'y', 'z'
            };

        StringBuffer result = new StringBuffer();

        Random rd = new Random(  );

        for ( int i = 0; i <= 10; i++ ) //随即10个拿出来看看
        {
        	result.append( A_Z[rd.nextInt( 26 )] );
        }

        return result.toString();
    }
}
