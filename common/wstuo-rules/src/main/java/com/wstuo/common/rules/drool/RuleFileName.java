package com.wstuo.common.rules.drool;

public enum RuleFileName {
	/**
	 * 请求规则文件名
	 */
	RequestRule("request-rule.drl"),
	/**
	 * 邮件转请求规则文件名
	 */
	MailRequestRule("mail-to-request-rule.drl"),
	/**
	 * 优先级矩阵规则文件名
	 */
	PrioritymatrixRule("prioritymatrix-rule.drl"),
	/**
	 * 变更规则文件名
	 */
	ChangeApprovalRule("change-approval-rule.drl");

	// 定义私有变量
	private String nCode;

	// 构造函数，枚举类型只能为私有
	private RuleFileName(String _nCode) {
		this.nCode = _nCode;
	}

	public String getValue() {
		return nCode;
	}
}
