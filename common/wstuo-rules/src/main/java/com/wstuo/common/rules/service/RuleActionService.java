package com.wstuo.common.rules.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.rules.dao.IRuleActionDAO;
import com.wstuo.common.rules.dao.IRuleDAO;
import com.wstuo.common.rules.dto.RuleActionDTO;
import com.wstuo.common.rules.entity.Rule;
import com.wstuo.common.rules.entity.RuleAction;

/**
 * the service class of RuleActionService
 * 
 * @author <a href="mailto:376890523@qq.com">jeff</a>
 * @version 0.1 2010-9-30
 * */
public class RuleActionService implements IRuleActionService {
	@Autowired
	private IRuleDAO ruleDAO;
	@Autowired
	private IRuleActionDAO ruleActionDAO;

	/**
	 * entity2dto Method
	 * 
	 * @param entity
	 *            ,dto
	 */
	private void entity2dto(RuleAction entity, RuleActionDTO dto) {
		if (entity.getRule() != null) {
			Long ruleNo = entity.getRule().getRuleNo();
			String ruleName = entity.getRule().getRuleName();
			dto.setRuleNo(ruleNo);
			dto.setRuleName(ruleName);
		}
	}

	/**
	 * --------------save,remove,update Method--------------
	 * */

	/**
	 * save action
	 * 
	 * @param dto
	 */
	@Transactional
	public void saveRuleAction(RuleActionDTO dto) {
		RuleAction entity = new RuleAction();
		RuleActionDTO.dto2entity(dto, entity);
		if (null != dto.getRuleNo()) {
			Rule rule = ruleDAO.findById(dto.getRuleNo());
			entity.setRule(rule);
		}
		ruleActionDAO.save(entity);
		dto.setActionNo(entity.getActionNo());
	}

	/**
	 * remove action
	 * 
	 * @param no
	 */
	@Transactional
	public void removeRuleAction(Long no) {
		ruleActionDAO.delete(ruleActionDAO.findById(no));
	}

	/**
	 * remove actions
	 * 
	 * @param nos
	 */
	@Transactional
	public void removeRuleActions(Long[] nos) {
		ruleActionDAO.deleteByIds(nos);
	}

	/**
	 * merge action
	 * 
	 * @param dto
	 * @return RuleActionDTO
	 */
	@Transactional
	public RuleActionDTO mergeRuleAction(RuleActionDTO dto) {
		RuleAction entity = new RuleAction();
		RuleActionDTO.dto2entity(dto, entity);
		if (dto.getRuleNo() != null) {
			Rule rule = ruleDAO.findById(dto.getRuleNo());
			entity.setRule(rule);
		}
		entity = ruleActionDAO.merge(entity);
		entity2dto(entity, dto);
		RuleActionDTO.entity2dto(entity, dto);
		return dto;
	}

	/**
	 * merge actions
	 * 
	 * @param dtos
	 */
	@Transactional
	public void mergeAllRuleAction(List<RuleActionDTO> dtos) {
		List<RuleAction> entities = new ArrayList<RuleAction>();
		for (RuleActionDTO dto : dtos) {
			RuleAction entity = new RuleAction();
			RuleActionDTO.dto2entity(dto, entity);
			if (dto.getRuleNo() != null) {
				Rule rule = ruleDAO.findById(dto.getRuleNo());
				entity.setRule(rule);
			}
			entities.add(entity);
		}
		ruleActionDAO.mergeAll(entities);
	}

	/**
	 * find all actions
	 * 
	 * @return List<RuleActionDTO>
	 */
	public List<RuleActionDTO> findRuleActions() {
		List<RuleAction> entities = ruleActionDAO.findAll();
		List<RuleActionDTO> dtos = new ArrayList<RuleActionDTO>(entities.size());
		for (RuleAction entity : entities) {
			RuleActionDTO dto = new RuleActionDTO();
			entity2dto(entity, dto);
			RuleActionDTO.entity2dto(entity, dto);
			dtos.add(dto);
		}
		return dtos;
	}

	/**
	 * find action by id
	 * 
	 * @param no
	 * @return RuleActionDTO
	 */
	public RuleActionDTO findRuleActionById(Long no) {
		RuleAction entity = ruleActionDAO.findById(no);
		RuleActionDTO dto = new RuleActionDTO();
		entity2dto(entity, dto);
		RuleActionDTO.entity2dto(entity, dto);
		return dto;
	}

	/**
	 * find action by ruleNo
	 * 
	 * @param ruleNo
	 * @return List<RuleActionDTO>
	 */
	public List<RuleActionDTO> findRuleActionByRuleNo(Long ruleNo) {
		Rule rule = ruleDAO.findById(ruleNo);
		List<RuleAction> entities = rule.getActions();
		List<RuleActionDTO> dtos = new ArrayList<RuleActionDTO>(entities.size());
		for (RuleAction entity : entities) {
			RuleActionDTO dto = new RuleActionDTO();
			entity2dto(entity, dto);
			RuleActionDTO.entity2dto(entity, dto);
			dtos.add(dto);
		}
		return dtos;
	}
}
