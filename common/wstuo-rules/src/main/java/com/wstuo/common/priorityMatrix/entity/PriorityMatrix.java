package com.wstuo.common.priorityMatrix.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.wstuo.common.config.dictionary.entity.DataDictionaryItems;

/**
 * 优先级矩阵实体类
 * @author WSTUO
 *
 */
@Entity
@Table(name="prioritymatrix")
public class PriorityMatrix {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;//ID
	@ManyToOne
	private DataDictionaryItems urgency ;//紧急度
	@ManyToOne
	private DataDictionaryItems affect;//影响范围
	@ManyToOne
	private DataDictionaryItems priority;//优先级
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public DataDictionaryItems getUrgency() {
		return urgency;
	}
	public void setUrgency(DataDictionaryItems urgency) {
		this.urgency = urgency;
	}
	public DataDictionaryItems getAffect() {
		return affect;
	}
	public void setAffect(DataDictionaryItems affect) {
		this.affect = affect;
	}
	public DataDictionaryItems getPriority() {
		return priority;
	}
	public void setPriority(DataDictionaryItems priority) {
		this.priority = priority;
	}
	
}
