package com.wstuo.common.priorityMatrix.dto;

import java.util.List;

import com.wstuo.common.config.dictionary.dto.DataDictionaryItemsDTO;

/**
 * 优先级矩阵数据显示DTO
 * @author WSTUO
 *
 */
public class PriorityMatrixDataShowDTO {
	private List<DataDictionaryItemsDTO> urgency;
	private List<DataDictionaryItemsDTO> affect;
	private List<PriorityMatrixGridDTO> priority;

	public List<DataDictionaryItemsDTO> getUrgency() {
		return urgency;
	}
	public void setUrgency(List<DataDictionaryItemsDTO> urgency) {
		this.urgency = urgency;
	}
	public List<DataDictionaryItemsDTO> getAffect() {
		return affect;
	}
	public void setAffect(List<DataDictionaryItemsDTO> affect) {
		this.affect = affect;
	}
	public List<PriorityMatrixGridDTO> getPriority() {
		return priority;
	}
	public void setPriority(List<PriorityMatrixGridDTO> priority) {
		this.priority = priority;
	} 
}
