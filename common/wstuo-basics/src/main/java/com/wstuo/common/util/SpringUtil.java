package com.wstuo.common.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class SpringUtil {

    static ApplicationContext ctx = new ClassPathXmlApplicationContext(
            "applicationContext-common.xml");

    public static Object getBean(String beanName) {

        return ctx.getBean(beanName);
    }
}