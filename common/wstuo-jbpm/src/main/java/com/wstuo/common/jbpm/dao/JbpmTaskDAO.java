package com.wstuo.common.jbpm.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.wstuo.common.proxy.dao.IProxyDAO;
import com.wstuo.common.proxy.entity.AuthorizeProxy;
import com.wstuo.common.bpm.dto.TaskQueryDTO;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.jbpm.entity.JbpmTask;
import com.wstuo.common.util.DaoUtils;

public class JbpmTaskDAO  extends BaseDAOImplHibernate<JbpmTask> implements IJbpmTaskDAO{
	@Autowired
	private IProxyDAO proxyDAO;
	/**
	 * JPBM任务分页查询
	 * @param queryDTO
	 * @param sidx
	 * @param sord
	 * @param start
	 * @param rows
	 * @return PageDTO
	 */
	public PageDTO findJbpmTaskPager(TaskQueryDTO queryDTO,String sidx,String sord,int start,int rows) {
		// TODO Auto-generated method stub
		DetachedCriteria dc = DetachedCriteria.forClass(JbpmTask.class);
		if(queryDTO.getAssignee()!=null){//代理人状态
			String[] ligins = null;
			List<AuthorizeProxy> list=proxyDAO.findProxyOrpersonal(queryDTO.getAssignee(),null);
			if(list!=null&&list.size()>0){
				ligins=new String[list.size()];
				 int i=0;
				 for(AuthorizeProxy ap:list){
					 ligins[i]=ap.getProxieduser().getLoginName();
					 i++;
				 }
		   }
	   		if(ligins!=null){
	   			dc.add( Restrictions.in("assignee",ligins));
	   		}else{
	   			dc.add( Restrictions.eq("assignee","JbpmTask"));
	   		}
		}
		//排序
        dc = DaoUtils.orderBy(sidx, sord, dc);
		return super.findPageByCriteria(dc, start, rows);
	}
	/**
	 * 指派给我任务的统计
	 * @param assignee
	 * @return Integer
	 */
	public Integer countJbpmTask(String assignee){
		final DetachedCriteria dc1 = DetachedCriteria.forClass( JbpmTask.class );
		String[] ligins = null;
		List<AuthorizeProxy> list=proxyDAO.findProxyOrpersonal(assignee,null);
		if(list!=null&&list.size()>0){
			ligins=new String[list.size()];
			 int i=0;
			 for(AuthorizeProxy ap:list){
				 ligins[i]=ap.getProxieduser().getLoginName();
				 i++;
			 }
	   }
   		if(ligins!=null){
   			dc1.add( Restrictions.in("assignee",ligins));
   		}else{
   			dc1.add( Restrictions.eq("assignee","JbpmTask"));
   		}
		return super.statCriteria(dc1);
	}
}
