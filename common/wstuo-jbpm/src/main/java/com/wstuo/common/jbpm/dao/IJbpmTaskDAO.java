package com.wstuo.common.jbpm.dao;

import com.wstuo.common.bpm.dto.TaskQueryDTO;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.jbpm.entity.JbpmTask;

/**
 * JBPM Task DAO Interface Class
 * @author wstuo
 *
 */
public interface IJbpmTaskDAO extends IEntityDAO<JbpmTask>{
	/**
	 * 分页查询用户ID
	 * @param queryDTO
	 * @param start
	 * @param limit
	 * @return PageDTO
	 */
	public PageDTO findJbpmTaskPager(TaskQueryDTO queryDTO,String sidx,String sord,int start,int rows);
	
	/**
	 * 指派给我任务的统计
	 * @param assignee
	 * @return Integer
	 */
	public Integer countJbpmTask(String assignee);
}
