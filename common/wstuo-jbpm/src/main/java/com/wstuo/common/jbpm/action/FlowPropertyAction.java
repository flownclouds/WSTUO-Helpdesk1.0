package com.wstuo.common.jbpm.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.bpm.dto.FlowActivityDTO;
import com.wstuo.common.bpm.dto.FlowPropertyDTO;
import com.wstuo.common.bpm.service.IFlowPropertyService;

/**
 * Flow Property Action Class
 * @author wstuo
 *
 */
@SuppressWarnings("serial")
public class FlowPropertyAction extends ActionSupport {
	@Autowired
	private IFlowPropertyService flowPropertyService;
	private FlowPropertyDTO flowPropertyDTO;
	private String processDefinitionId;
	private FlowActivityDTO flowActivityDTO;
	
	public FlowPropertyDTO getFlowPropertyDTO() {
		return flowPropertyDTO;
	}


	public void setFlowPropertyDTO(FlowPropertyDTO flowPropertyDTO) {
		this.flowPropertyDTO = flowPropertyDTO;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}


	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}


	public String findFlowPropertyByProcessDefinitionId(){
		flowPropertyDTO =  flowPropertyService.findFlowPropertyByProcessDefinitionId(processDefinitionId);
		return "flowPropertyDTO";
	}
	
	
	public FlowActivityDTO getFlowActivityDTO() {
		return flowActivityDTO;
	}


	public void setFlowActivityDTO(FlowActivityDTO flowActivityDTO) {
		this.flowActivityDTO = flowActivityDTO;
	}


	/**
	 * 更新流程活动
	 * @return success
	 */
	public String updateFlowActivity(){
		flowPropertyService.updateFlowActivity(flowActivityDTO);
		return SUCCESS;
	}
	/**
	 * 查询流程活动详细
	 * @return flow activity dto
	 */
	public String findFlowActivityById(){
		
		flowActivityDTO = flowPropertyService.findFlowActivityById(flowActivityDTO.getId());
		
		return "flowActivityDto";
	}
}
