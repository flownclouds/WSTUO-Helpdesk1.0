package com.wstuo.itsm.knowledge.entity;

import javax.persistence.Cacheable;
import javax.persistence.Entity;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.wstuo.common.config.category.entity.Category;

/**
 * Knowledge Entity Bean.
 * @author Spring
 */
@SuppressWarnings( "serial" )
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class KnowledgeCategory
    extends Category{
	
}
