package com.wstuo.common.security.dao;

import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.security.entity.ServiceTime;

import java.util.List;

/**
 * 服务时间DAO.
 * @author will
 *
 */
public class ServiceTimeDAO
    extends BaseDAOImplHibernate<ServiceTime>
    implements IServiceTimeDAO
{
	/**
     * 根据机构编号查找服务时间.
     * @param orgNo 机构编号：Long orgNo
     * @return 服务时间对象：ServiceTime
     */
    @SuppressWarnings( "unchecked" )
    public ServiceTime findByOrgNo( Long orgNo )
    {
    	ServiceTime serviceTime = null;
        List<Object> obj =
            super.getHibernateTemplate(  ).find( " from ServiceTime st where st.organization.orgNo=" + orgNo );

        if ( ! obj.isEmpty(  ) )
        {
        	serviceTime = (ServiceTime) obj.get( 0 );
        }
        
        return serviceTime;
    }
}
