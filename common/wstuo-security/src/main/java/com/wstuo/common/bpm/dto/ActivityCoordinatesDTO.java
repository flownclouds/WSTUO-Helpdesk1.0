package com.wstuo.common.bpm.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * 活动坐标DTO
 * @author wstuo
 *
 */
@SuppressWarnings("serial")
public class ActivityCoordinatesDTO extends BaseDTO {
	private int x;
	private int y;
	private int width;
	private int height;
	private String nodeType;
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public String getNodeType() {
		return nodeType;
	}
	public void setNodeType(String nodeType) {
		this.nodeType = nodeType;
	}
	
	
}
