package com.wstuo.common.bpm.api;

import java.io.File;
import java.io.InputStream;

import com.wstuo.common.bpm.dto.ProcessDefinitionDTO;
import com.wstuo.common.dto.PageDTO;

/**
 * business process definition interface class
 * @author wst
 *
 */
public interface IBPD {
	/**
	 * 流程布署
	 * @param file ZIP文件
	 * @return String
	 */
	String deployProcessDefinitionFile(File file);
	
	/**
	 * 流程布署
	 * @param fis 流程文件流
	 * @return IBPD.java
	 */
	String deployProcessDefinitionFile(InputStream fis);
	
	/**
	 * 流程布署(XML格式文件)
	 * @param jpdlfile jpdl xml file
	 * @return String
	 */
	String deployProcessDefinitionXmlFile(File jpdlfile);
	
	/**
	 * 流程删除
	 * @param deploymentId 流程布署ID
	 */
	void unDeployProcessDefinition(String deploymentId);
	
	/**
     * find process definition id by deployment Id
     * @param deploymentId
     * @return process definition id
     */
	String findProcessDefinitionIdByDeploymentId(String deploymentId);
	
	
	/**
	 * 分页查找流程定义
	 * @param start
	 * @param limit
	 * @param sord
	 * @param sidx
	 * @return PageDTO
	 */
	PageDTO findPageProcessDefinitions(ProcessDefinitionDTO qdto, int start,int limit,String sord,String sidx);
}
