package com.wstuo.common.bpm.dto;

/**
 * 流程处理参数DTO CLASS
 * @author wstuo
 *
 */
public class ProcessHandleDTO {
	private Long eno;
	private String pid;
	private String remark;
	private String outcome;
	private String operator;//操作者
	private String taskId;//任务ID
	private String taskName;//任务名称
	private String nextActivityType;//下一活动类型
	private String approversMemberSet;//审批成员
	private Boolean approverResult = false;//成员审批结果
	private Long groupNo;//指派组
	private String groupName;//指派组
	private Long assigneeNo;//指派技术员
	private String assigneeName;//指派技术员
	private String assigneeNames;//指派多个技术员
	private String noticeRule;//通知规则
	private Boolean dynamicAssignee = false;//是否动态指派
	private Long statusNo;//对应状态
	private String assignType;//指派方式
	private Boolean approverTask = false;//是否是审批任务
	@Deprecated
	private Boolean allowUseDynamicAssignee = false;//允许动态指派覆盖已有指派;
	private Long flowActivityId;//动作ID
	private String module;//模块
	private String handleType;//处理类型
	private String variablesAssigneeType;//变量指派类型
	private Long leaderNum = 0L;//级层
	private Boolean allowUseVariablesAssignee = false; //是否允许变量指派覆盖已有指派
	private Long variablesAssigneeGroupNo; //变量指派指派组
	private String variablesAssigneeGroupName; //变量指派指派组
	private Boolean mailHandlingProcess = false; //是否邮件处理流程
	private String emailAddress;//通知的邮箱地址
	private String activityName;//活动名称 
	private Boolean isApproversMember = false;
	private Boolean isOpenProcess = false; //是否是开启流程的动作
	
	public Boolean getIsOpenProcess() {
		return isOpenProcess;
	}
	public void setIsOpenProcess(Boolean isOpenProcess) {
		this.isOpenProcess = isOpenProcess;
	}
	public Boolean getIsApproversMember() {
		return isApproversMember;
	}
	public void setIsApproversMember(Boolean isApproversMember) {
		this.isApproversMember = isApproversMember;
	}
	public Boolean isMailHandlingProcess() {
		return mailHandlingProcess;
	}
	public void setMailHandlingProcess(Boolean mailHandlingProcess) {
		this.mailHandlingProcess = mailHandlingProcess;
	}
	public String getVariablesAssigneeGroupName() {
		return variablesAssigneeGroupName;
	}
	public void setVariablesAssigneeGroupName(String variablesAssigneeGroupName) {
		this.variablesAssigneeGroupName = variablesAssigneeGroupName;
	}
	public Long getVariablesAssigneeGroupNo() {
		return variablesAssigneeGroupNo;
	}
	public void setVariablesAssigneeGroupNo(Long variablesAssigneeGroupNo) {
		this.variablesAssigneeGroupNo = variablesAssigneeGroupNo;
	}
	public Boolean isAllowUseVariablesAssignee() {
		return allowUseVariablesAssignee;
	}
	public void setAllowUseVariablesAssignee(Boolean allowUseVariablesAssignee) {
		this.allowUseVariablesAssignee = allowUseVariablesAssignee;
	}
	public String getVariablesAssigneeType() {
		return variablesAssigneeType;
	}
	public void setVariablesAssigneeType(String variablesAssigneeType) {
		this.variablesAssigneeType = variablesAssigneeType;
	}
	public Long getLeaderNum() {
		return leaderNum;
	}
	public void setLeaderNum(Long leaderNum) {
		this.leaderNum = leaderNum;
	}
	public Long getEno() {
		return eno;
	}
	public void setEno(Long eno) {
		this.eno = eno;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getOutcome() {
		return outcome;
	}
	public void setOutcome(String outcome) {
		this.outcome = outcome;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getNextActivityType() {
		return nextActivityType;
	}
	public void setNextActivityType(String nextActivityType) {
		this.nextActivityType = nextActivityType;
	}
	public String getApproversMemberSet() {
		return approversMemberSet;
	}
	public void setApproversMemberSet(String approversMemberSet) {
		this.approversMemberSet = approversMemberSet;
	}
	public Boolean isApproverResult() {
		return approverResult;
	}
	public void setApproverResult(Boolean approverResult) {
		this.approverResult = approverResult;
	}
	public Long getGroupNo() {
		return groupNo;
	}
	public void setGroupNo(Long groupNo) {
		this.groupNo = groupNo;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public Long getAssigneeNo() {
		return assigneeNo;
	}
	public void setAssigneeNo(Long assigneeNo) {
		this.assigneeNo = assigneeNo;
	}
	public String getAssigneeName() {
		return assigneeName;
	}
	public void setAssigneeName(String assigneeName) {
		this.assigneeName = assigneeName;
	}
	public String getAssigneeNames() {
		return assigneeNames;
	}
	public void setAssigneeNames(String assigneeNames) {
		this.assigneeNames = assigneeNames;
	}
	public String getNoticeRule() {
		return noticeRule;
	}
	public void setNoticeRule(String noticeRule) {
		this.noticeRule = noticeRule;
	}
	public Boolean isDynamicAssignee() {
		return dynamicAssignee;
	}
	public void setDynamicAssignee(Boolean dynamicAssignee) {
		this.dynamicAssignee = dynamicAssignee;
	}
	public Long getStatusNo() {
		return statusNo;
	}
	public void setStatusNo(Long statusNo) {
		this.statusNo = statusNo;
	}
	public String getAssignType() {
		return assignType;
	}
	public void setAssignType(String assignType) {
		this.assignType = assignType;
	}
	public Boolean isApproverTask() {
		return approverTask;
	}
	public void setApproverTask(Boolean approverTask) {
		this.approverTask = approverTask;
	}
	public Boolean isAllowUseDynamicAssignee() {
		return allowUseDynamicAssignee;
	}
	public void setAllowUseDynamicAssignee(Boolean allowUseDynamicAssignee) {
		this.allowUseDynamicAssignee = allowUseDynamicAssignee;
	}
	public Long getFlowActivityId() {
		return flowActivityId;
	}
	public void setFlowActivityId(Long flowActivityId) {
		this.flowActivityId = flowActivityId;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getHandleType() {
		return handleType;
	}
	public void setHandleType(String handleType) {
		this.handleType = handleType;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public Boolean getApproverResult() {
		return approverResult;
	}
	public Boolean getDynamicAssignee() {
		return dynamicAssignee;
	}
	public Boolean getApproverTask() {
		return approverTask;
	}
	public Boolean getAllowUseDynamicAssignee() {
		return allowUseDynamicAssignee;
	}
	public Boolean getAllowUseVariablesAssignee() {
		return allowUseVariablesAssignee;
	}
	public Boolean getMailHandlingProcess() {
		return mailHandlingProcess;
	}
	
	public ProcessHandleDTO(){
		super();
	}
	
}
