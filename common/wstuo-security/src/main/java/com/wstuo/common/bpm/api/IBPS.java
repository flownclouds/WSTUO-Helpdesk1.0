package com.wstuo.common.bpm.api;

/**
 * business process user or group interface class
 * @author wst
 *
 */
public interface IBPS {

	/**
	 * 创建工作流程组
	 * @param groupName
	 * @param parentGroupId
	 */
	void createGroup(String groupName,String parentGroupId);
	/**
	 * 创建工作流程用户
	 * @param id
	 * @param familyName
	 * @param givenName
	 * @param businessEmail
	 */
	void createUser(String id,String familyName,String givenName,String businessEmail);
	
	/**
	 * 修改工作流程用户
	 * @param id
	 * @param familyName
	 * @param givenName
	 * @param businessEmail
	 */
	void modifyUser(String id,String familyName,String givenName,String businessEmail);
	
	/**
	 * 添加用户与组关系
	 * @param groupId
	 * @param userIds
	 */
	void createMembership(String groupId,String parentGroupId, String[] userIds);
}
