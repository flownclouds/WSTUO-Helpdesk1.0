package com.wstuo.common.security.dao;

import java.util.List;
import java.util.Set;

import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.security.entity.Resource;

/**
 * 资源DAO接口.
 * 
 * @author will
 */
public interface IResourceDAO extends IEntityDAO<Resource> {
	/**
	 * 根据资源地址查找资源.
	 * 
	 * @param requestUrl
	 *            资源地址 String
	 * @return 资源对象 Resource
	 */
	Resource findByResUrl(String requestUrl);

	/**
	 * 查询下一个资源编号
	 * 
	 * @return next res no
	 */
	Long getNextResNo();

	/**
	 * 累加机构编号.
	 */
	void increment();

	/**
	 * 查询最后一个Res No
	 * 
	 * @return late res no
	 */
	Long getLatesResNo();

	/**
	 * 直接设置编号
	 * 
	 * @param no
	 */
	void setLatesResNo(Long no);

	/**
	 * 编辑，查找相同的数据.
	 * 
	 * @param resName
	 * @param resCode
	 * @param resUrl
	 * @param id
	 */
	void findExists2(String resName, String resCode, String resUrl, Long id);
	/**
	 * 根据ResourceCode查询Resources
	 * @param ResourceCode
	 * @param roleCodes
	 * @return List<Resource>
	 */
	List<Resource> findResourcesByResourceCode(String resourceCode,List<String> roleCodes);
	
	/**
	 * 根据角色及资源URL查询资源
	 * @param roles 角色集合
	 * @param resUrls 资源URL集合
	 * @return List<Resource>
	 */
	List<Resource> findResourcesByRoleAndResUrl(Set<Long> roles , String[] resUrls );
}
