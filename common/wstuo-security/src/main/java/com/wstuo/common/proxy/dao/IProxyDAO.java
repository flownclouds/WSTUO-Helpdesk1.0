package com.wstuo.common.proxy.dao;

import java.util.List;

import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.proxy.dto.ProxyDTO;
import com.wstuo.common.proxy.entity.AuthorizeProxy;

/**
 * 用户代理DAO接口类
 * @author Administrator
 *
 */
public interface IProxyDAO extends IEntityDAO<AuthorizeProxy>{
	/**
	 *分页查询方法
	 * @param proxyDTO
	 * @param sidx
	 * @param sord
	 * @param start
	 * @param rows
	 * @return PageDTO
	 */
	PageDTO findPager(ProxyDTO proxyDTO,String sidx,String sord,int start,int rows);
	/**
	 * 查询代理了那些用户
	 * @param loginName
	 * @param technicalloginName
	 * @return List<AuthorizeProxy>
	 */
	List<AuthorizeProxy> findProxyOrpersonal(String loginName,String technicalloginName);
}