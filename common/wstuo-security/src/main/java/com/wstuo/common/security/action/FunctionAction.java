package com.wstuo.common.security.action;

import com.opensymphony.xwork2.ActionSupport;

import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.exception.ApplicationException;
import com.wstuo.common.security.dto.FunctionDTO;
import com.wstuo.common.security.dto.FunctionQueryDTO;
import com.wstuo.common.security.dto.FunctionTreeDTO;
import com.wstuo.common.security.service.IFunctionService;

import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 功能Action类
 * 
 * @author will
 */
@SuppressWarnings("serial")
public class FunctionAction extends ActionSupport {
	private static final Logger LOGGER = Logger.getLogger(FunctionAction.class);
	@Autowired
	private IFunctionService functionService;
	private FunctionTreeDTO functionTreeDto;
	private FunctionDTO functionDto = new FunctionDTO();
	private FunctionQueryDTO functionQueryDTO = new FunctionQueryDTO();
	private int page = 1;
	private int rows = 10;
	private PageDTO functions;
	private Long[] resNos;
	private Boolean isExist = false;

	private File importFile;
	private InputStream exportFile;
	private String effect;

	private String sidx;
	private String sord;
	private String resCode;

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public File getImportFile() {
		return importFile;
	}

	public void setImportFile(File importFile) {
		this.importFile = importFile;
	}

	public InputStream getExportFile() {
		return exportFile;
	}

	public void setExportFile(InputStream exportFile) {
		this.exportFile = exportFile;
	}

	public String getEffect() {
		return effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}

	public Boolean getIsExist() {
		return isExist;
	}

	public void setIsExist(Boolean isExist) {
		this.isExist = isExist;
	}

	public FunctionDTO getFunctionDto() {
		return functionDto;
	}

	public void setFunctionDto(FunctionDTO functionDto) {
		this.functionDto = functionDto;
	}

	public IFunctionService getFunctionService() {
		return functionService;
	}

	public void setFunctionService(IFunctionService functionService) {
		this.functionService = functionService;
	}

	public FunctionTreeDTO getFunctionTreeDto() {
		return functionTreeDto;
	}

	public void setFunctionTreeDto(FunctionTreeDTO functionTreeDto) {
		this.functionTreeDto = functionTreeDto;
	}

	public FunctionQueryDTO getFunctionQueryDTO() {
		return functionQueryDTO;
	}

	public void setFunctionQueryDTO(FunctionQueryDTO functionQueryDTO) {
		this.functionQueryDTO = functionQueryDTO;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public PageDTO getFunctions() {
		return functions;
	}

	public void setFunctions(PageDTO functions) {
		this.functions = functions;
	}

	public Long[] getResNos() {
		return resNos;
	}

	public void setResNos(Long[] resNos) {
		this.resNos = resNos;
	}

	public String getResCode() {
		return resCode;
	}

	public void setResCode(String resCode) {
		this.resCode = resCode;
	}

	/**
	 * 分页查找功能（列表）.
	 * 
	 * @return PageDTO
	 */
	public String findGrid() {
		functionQueryDTO.setSidx(sidx);
		functionQueryDTO.setSord(sord);
		int start = (page - 1) * rows;
		functionQueryDTO.setStart(start);
		functionQueryDTO.setLimit(rows);
		functions = functionService.findPagerFunction(functionQueryDTO);
		functions.setPage(page);
		functions.setRows(rows);
		return "functionGrid";
	}

	/**
	 * 查找功能树.
	 * 
	 * @return function tree dto
	 */
	public String findTree() {
		functionTreeDto = new FunctionTreeDTO();
		functionTreeDto.setData("ROOT");
		functionTreeDto.setChildren(functionService.findFunctionTreeDtos(resCode));

		return SUCCESS;
	}

	/**
	 * 添加功能.
	 * 
	 * @return String
	 */
	public String addFunction() {
		functionService.addFunction(functionDto);

		return SUCCESS;
	}

	/**
	 * 编辑功能.
	 * 
	 * @return String
	 */
	public String updateFunction() {
		functionService.upadteFunction(functionDto);

		return SUCCESS;
	}

	/**
	 * 删除功能.
	 * 
	 * @return String
	 */
	public String deleteFunction() {
		try {
			isExist = functionService.deleteFunction(functionDto.getResNo());
		} catch (Exception ex) {
			throw new ApplicationException("ERROR_DATA_CAN_NOT_DELETE");
		}

		return "result";
	}

	/**
	 * 批量删除功能.
	 * 
	 * @return String
	 */
	public String deleteFunctionByIds() {
		isExist = functionService.deleteFunction(resNos);
		return "result";
	}

	/**
	 * 复制功能.
	 * 
	 * @return String
	 */
	public String copyFunction() {
		functionDto = functionService.copyFunction(functionDto.getResNo(), functionDto.getParentFunctionNo());

		return SUCCESS;
	}

	/**
	 * 剪切功能（更改功能的父节点）.
	 * 
	 * @return String result
	 */
	public String cutFunction() {
		functionService.cutFunction(functionDto.getResNo(), functionDto.getParentFunctionNo());

		return SUCCESS;
	}

	/**
	 * 验证功能是否存在.
	 * 
	 * @return true or false
	 * @throws UnsupportedEncodingException
	 */
	public String findExist() throws UnsupportedEncodingException {
		if (functionQueryDTO.getResName() != null) {
			String resName = new String(functionQueryDTO.getResName().getBytes("ISO-8859-1"), "UTF-8");
			functionQueryDTO.setResName(resName);
		}

		if (functionQueryDTO.getResCode() != null) {
			String resCode = new String(functionQueryDTO.getResCode().getBytes("ISO-8859-1"), "UTF-8");
			functionQueryDTO.setResCode(resCode);
		}

		if (functionQueryDTO.getResUrl() != null) {
			String resUrl = new String(functionQueryDTO.getResUrl().getBytes("ISO-8859-1"), "UTF-8");
			functionQueryDTO.setResUrl(resUrl);
		}

		isExist = functionService.findFunctionExist(functionQueryDTO);

		return "result";
	}

	/**
	 * 导出CSV.
	 * 
	 * @return inputStream
	 */
	public String exportFunction() {

		functionQueryDTO.setStart(0);
		functionQueryDTO.setLimit(10000);
		exportFile = functionService.exportFunction(functionQueryDTO);
		return "exportFile";
	}

	/**
	 * 导入CSV
	 * 
	 * @return import result
	 */
	public String importFunction() {
		effect = "failure";
		try {
			effect = functionService.importFunction(importFile.getAbsolutePath());
		} catch (Exception ex) {
			LOGGER.error(ex);
		}
		return "importResult";
	}

}
