package com.wstuo.common.security.dto;

import java.io.File;

import com.wstuo.common.dto.AbstractValueObject;

/**
 * Customer Query DTO
 * 
 * @author will
 * 
 */
@SuppressWarnings("serial")
public class CustomerQueryDTO extends AbstractValueObject {
	private Long orgNo;
	private String orgName;
	private String address;
	private String email;
	private String officePhone;
	private String officeFax;
	private Long companyNo;
	private Integer start;
	private Integer limit;
	private File importFile;

	public CustomerQueryDTO() {
		super();
	}

	public File getImportFile() {
		return importFile;
	}

	public void setImportFile(File importFile) {
		this.importFile = importFile;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Long getOrgNo() {
		return orgNo;
	}

	public void setOrgNo(Long orgNo) {
		this.orgNo = orgNo;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOfficePhone() {
		return officePhone;
	}

	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}

	public String getOfficeFax() {
		return officeFax;
	}

	public void setOfficeFax(String officeFax) {
		this.officeFax = officeFax;
	}

	public Long getCompanyNo() {
		return companyNo;
	}

	public void setCompanyNo(Long companyNo) {
		this.companyNo = companyNo;
	}
}
