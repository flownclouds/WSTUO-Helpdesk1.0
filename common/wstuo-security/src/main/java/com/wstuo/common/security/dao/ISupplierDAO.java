package com.wstuo.common.security.dao;

import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dto.SupplierQueryDTO;
import com.wstuo.common.security.entity.Supplier;

import java.util.List;

/**
 * 供应商DAO接口.
 * @author will
 *
 */
public interface ISupplierDAO
    extends IEntityDAO<Supplier>
{
    /**
     * 分页查询供应商信息
     * @param qdto 分页查询DTO:SupplierQueryDTO
     * @param sidx
     * @param sord
     * @return 分页数据：PageDTO
     */
    PageDTO findPager( SupplierQueryDTO qdto, String sidx, String sord );

    /**
     * 根据机构编号删除供应商
     * @param orgNo 机构编号：Long orgNo
     */
    void delete( Long orgNo );

    /**
     * 根据公司编号查找供应商
     * @param companyNo 公司编号：Long companyNo
     * @return 供应商列表：List<Supplier>
     */
    List<Supplier> findByCompanyNo( Long companyNo );
}
