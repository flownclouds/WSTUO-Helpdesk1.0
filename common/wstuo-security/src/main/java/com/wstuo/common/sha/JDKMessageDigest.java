package com.wstuo.common.sha;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.log4j.Logger;

/**
 * JDK Message Digest
 *
 */
public class JDKMessageDigest{
	private static final Logger LOGGER = Logger.getLogger(JDKMessageDigest.class );
	/**
	 * 将16位byte[] 转换为32位String
	 * 
	 * @param buffer
	 * @return
	 */
	private static String toHex(byte buffer[]) {
		StringBuffer sb = new StringBuffer(buffer.length * 2);
		for (int i = 0; i < buffer.length; i++) {
			sb.append(Character.forDigit((buffer[i] & 240) >> 4, 16));
			sb.append(Character.forDigit(buffer[i] & 15, 16));
		}
		return sb.toString();
	}
	
	/**
	 *  判断字符串是否相等
	 * @param str  字符串1
	 * @param str1   字符串2
	 * @return boolean
	 */
	public boolean isEqual(String str,String str1){
		return str.equals(str1);
	}
	
	/**
	 *  加密
	 * @param str 需要加密字符串
	 * @return   加密后字符串
	 */
	public String Encryption(String str){
		MessageDigest messageDigest = null;
		try {
			messageDigest = MessageDigest.getInstance("SHA-1");
			messageDigest.update(str.getBytes());
		} catch (NoSuchAlgorithmException e) {
			LOGGER.error(e.getMessage());
		}
		return toHex(messageDigest.digest());
	}
	
}  
