package com.wstuo.common.security.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * class LDAPQueryDTO
 * @author will
 */
@SuppressWarnings("serial")
public class LDAPQueryDTO extends BaseDTO {
    private Long ldapId;
    private String ldapName;
    private String ldapURL;
    private String ldapType;
    private Integer start;
    private Integer limit;

    public LDAPQueryDTO(  )
    {
        super(  );
    }


    public String getLdapName(  )
    {
        return ldapName;
    }

    public void setLdapName( String ldapName )
    {
        this.ldapName = ldapName;
    }

    public String getLdapURL(  )
    {
        return ldapURL;
    }

    public void setLdapURL( String ldapURL )
    {
        this.ldapURL = ldapURL;
    }

    public String getLdapType(  )
    {
        return ldapType;
    }

    public void setLdapType( String ldapType )
    {
        this.ldapType = ldapType;
    }


	public Long getLdapId() {
		return ldapId;
	}


	public void setLdapId(Long ldapId) {
		this.ldapId = ldapId;
	}


	public Integer getStart() {
		return start;
	}


	public void setStart(Integer start) {
		this.start = start;
	}


	public Integer getLimit() {
		return limit;
	}


	public void setLimit(Integer limit) {
		this.limit = limit;
	}

   
}
