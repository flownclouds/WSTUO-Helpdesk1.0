package com.wstuo.common.bpm.dao;

import com.wstuo.common.bpm.dao.IFlowTransitionDAO;
import com.wstuo.common.bpm.entity.FlowTransition;
import com.wstuo.common.dao.BaseDAOImplHibernate;

/**
 * Flow Transition DAO Class
 * @author wst
 *
 */
public class FlowTransitionDAO extends BaseDAOImplHibernate<FlowTransition> implements IFlowTransitionDAO {

}
