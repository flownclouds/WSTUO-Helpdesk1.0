package com.wstuo.common.security.dao;

import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.security.entity.LDAPAuthenticationSetting;

/**
 * LDAP验证DAO类
 * @author will
 *
 */
public class LDAPAuthenticationSettingDAO extends BaseDAOImplHibernate<LDAPAuthenticationSetting> implements ILDAPAuthenticationSettingDAO {
	
}
