package com.wstuo.common.security.entity;

import javax.persistence.Entity;
import javax.persistence.Transient;

/**
 * Entity class OrganizationInner.
 */
@SuppressWarnings( "serial" )
@Entity
public class OrganizationInner
    extends Organization
{
    @Transient
    public String getOrgType() {
		return "inner";
	}

    
}
