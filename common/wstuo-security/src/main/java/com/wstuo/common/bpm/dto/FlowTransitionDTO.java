package com.wstuo.common.bpm.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * Flow Transition DTO Class
 * @author wstuo
 *
 */
@SuppressWarnings("serial")
public class FlowTransitionDTO extends BaseDTO {
	private Long id;
	private String transitionName;//过渡名
	private String destination;//目的地
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTransitionName() {
		return transitionName;
	}
	public void setTransitionName(String transitionName) {
		this.transitionName = transitionName;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	
}
