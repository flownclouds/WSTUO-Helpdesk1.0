package com.wstuo.common.security.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * this class is FunctionDTO.
 * @author will
 * 
 */
@SuppressWarnings("serial")
public class FunctionDTO extends BaseDTO {
	private Long resNo;
	private String resCode;
	private Long parentFunctionNo;
	private String resName;
	private String resIcon;
	private String resUrl;
	private String parentFunctionName;
	private Byte dataFlag;
	private String parentFunctionResCode;// tan add 20110721

	public FunctionDTO(Long resNo, String resCode, String resName,
			String resUrl, Long parentFunctionNo) {
		this.resNo = resNo;
		this.resName = resName;
		this.resCode = resCode;
		this.resUrl = resUrl;
		this.parentFunctionNo = parentFunctionNo;
	}

	public FunctionDTO(String resCode, String resName, String resUrl) {

		this.resName = resName;
		this.resCode = resCode;
		this.resUrl = resUrl;
	}

	public String getParentFunctionName() {
		return parentFunctionName;
	}

	public void setParentFunctionName(String parentFunctionName) {
		this.parentFunctionName = parentFunctionName;
	}

	public FunctionDTO() {
		super();
	}

	public Long getResNo() {
		return resNo;
	}

	public void setResNo(Long resNo) {
		this.resNo = resNo;
	}

	public String getResIcon() {
		return resIcon;
	}

	public void setResIcon(String resIcon) {
		this.resIcon = resIcon;
	}

	public String getResCode() {
		return resCode;
	}

	public void setResCode(String resCode) {
		this.resCode = resCode;
	}

	public Long getParentFunctionNo() {
		return parentFunctionNo;
	}

	public void setParentFunctionNo(Long parentFunctionNo) {
		this.parentFunctionNo = parentFunctionNo;
	}

	public String getResName() {
		return resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	public String getResUrl() {
		return resUrl;
	}

	public void setResUrl(String resUrl) {
		this.resUrl = resUrl;
	}

	public Byte getDataFlag() {
		return dataFlag;
	}

	public void setDataFlag(Byte dataFlag) {
		this.dataFlag = dataFlag;
	}

	public String getParentFunctionResCode() {
		return parentFunctionResCode;
	}

	public void setParentFunctionResCode(String parentFunctionResCode) {
		this.parentFunctionResCode = parentFunctionResCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dataFlag == null) ? 0 : dataFlag.hashCode());
		result = prime
				* result
				+ ((parentFunctionName == null) ? 0 : parentFunctionName
						.hashCode());
		result = prime
				* result
				+ ((parentFunctionNo == null) ? 0 : parentFunctionNo.hashCode());
		result = prime
				* result
				+ ((parentFunctionResCode == null) ? 0 : parentFunctionResCode
						.hashCode());
		result = prime * result + ((resCode == null) ? 0 : resCode.hashCode());
		result = prime * result + ((resIcon == null) ? 0 : resIcon.hashCode());
		result = prime * result + ((resName == null) ? 0 : resName.hashCode());
		result = prime * result + ((resNo == null) ? 0 : resNo.hashCode());
		result = prime * result + ((resUrl == null) ? 0 : resUrl.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FunctionDTO other = (FunctionDTO) obj;
		if (dataFlag == null) {
			if (other.dataFlag != null)
				return false;
		} else if (!dataFlag.equals(other.dataFlag))
			return false;
		if (parentFunctionName == null) {
			if (other.parentFunctionName != null)
				return false;
		} else if (!parentFunctionName.equals(other.parentFunctionName))
			return false;
		if (parentFunctionNo == null) {
			if (other.parentFunctionNo != null)
				return false;
		} else if (!parentFunctionNo.equals(other.parentFunctionNo))
			return false;
		if (parentFunctionResCode == null) {
			if (other.parentFunctionResCode != null)
				return false;
		} else if (!parentFunctionResCode.equals(other.parentFunctionResCode))
			return false;
		if (resCode == null) {
			if (other.resCode != null)
				return false;
		} else if (!resCode.equals(other.resCode))
			return false;
		if (resIcon == null) {
			if (other.resIcon != null)
				return false;
		} else if (!resIcon.equals(other.resIcon))
			return false;
		if (resName == null) {
			if (other.resName != null)
				return false;
		} else if (!resName.equals(other.resName))
			return false;
		if (resNo == null) {
			if (other.resNo != null)
				return false;
		} else if (!resNo.equals(other.resNo))
			return false;
		if (resUrl == null) {
			if (other.resUrl != null)
				return false;
		} else if (!resUrl.equals(other.resUrl))
			return false;
		return true;
	}

}
