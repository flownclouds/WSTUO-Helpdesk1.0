package com.wstuo.common.security.service;

import java.util.List;

import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dto.LDAPAuthenticationSettingDTO;
import com.wstuo.common.security.dto.LDAPDTO;
import com.wstuo.common.security.dto.LDAPQueryDTO;
import com.wstuo.common.security.dto.UserDTO;

/**
 * ILDAPService interface
 */
public interface ILDAPService
{
    /**
     * 保存LDAP连接信息
     * @param ldapDto
     */
    void saveLdap( LDAPDTO ldapDto );

    /**
     * 删除LDAP连接信息
     * @param ldapIds
     */
    void deleteLdap( Long[] ldapIds );

    /**
     * 修改LDAP连接信息
     * @param ldapdto
     */
    void mergeLdap( LDAPDTO ldapdto );

    /**
     * 分页查询LDAP配置信息列表
     * @param querydto
     * @param sidx
     * @param sord
     * @return PageDTO
     */
    PageDTO findPagerLdap( LDAPQueryDTO querydto,String sidx,String sord);

    /**
    * getLdap GET AD USER
    * @param page
    * @param rows
    * @param ldapId
    * @return PageDTO
    */
    PageDTO getAllLdap( int page, int rows, Long ldapId );

    /**
     * adUserImport
     * @param userDto
     * @return boolean
     */
    boolean adUserImport( UserDTO userDto );

    /**
     * getLDAPAll
     * @return List<LDAPDTO>
     */
    List<LDAPDTO> getLDAPAll(  );

    /**
     * 根据ID获取LDAP信息 getLDAPById
     * @param ldapId
     * @return LDAPDTO
     */
    LDAPDTO getLDAPById( long ldapId );

    /**
     * LDAPAuthentication 在所有LDAP下进行验证
     * @param userName
     * @param password
     * @return boolean
     */
    boolean ldapAuthentication( String userName, String password );

    /**
     * 根据LDAP ID进行连接测试，并把测试结果保存
     * @param ldapId
     * @return boolean
     */
    boolean ldapConnTest(Long ldapId);
    
    
    /**
     * 检查连接LDAP配置信息连接是否正确
     * @param ldapCheckDto
     * @return boolean
     */
    boolean ldapConfigCheck( LDAPDTO ldapCheckDto );
    
    /**
     * 检测用户名在指定AD里是否存在
     * @param ldapDTO
     * @return UserDTO
     */
    UserDTO checkUserNameIsExist( LDAPDTO ldapDTO);
    
    /**
	 * LDAP验证配置连接测试
	 * @param dto
	 * @return boolean
	 */
	boolean LDAPAuthenticationSettingConnTest(LDAPAuthenticationSettingDTO dto);
	/**
	 * connTestkerberos验证配置连接测试
	 * @param dto
	 * @return boolean
	 */
	boolean connTestkerberos(LDAPAuthenticationSettingDTO dto);
	
	/**
	 * LDAP 连接测试
	 * @param dto
	 * @return boolean
	 */
	boolean connTestldap(LDAPAuthenticationSettingDTO dto);
	/**
     * LDAP用户导入
     * @param userDTO
     * @param ldapId
     * @return ldap user import result
     */
    String ldapUserImport(UserDTO userDTO,Long ldapId);
    
    /**
     * AD域用户自动更新
     */
    void adAutoUpdate();
    /**
     * MQ保存AD域用户
     */
    void saveUser(UserDTO userDTO);
   
}
