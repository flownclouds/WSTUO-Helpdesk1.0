package com.wstuo.common.security.dao;

import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.security.entity.OrganizationServices;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * 服务机构DAO.
 * @author will
 */
public class OrganizationServicesDAO
    extends BaseDAOImplHibernate<OrganizationServices>
    implements IOrganizationServicesDAO
{
	
	
	@Autowired
	private IOrganizationDAO organizationDAO;
	
	
	 /**
     * 根据公司编号查询服务机构信息.
     * @param companyNo 公司编号 Long companyNo
     * @return 服务机构列表 List<OrganizationServices>
     */
    public List<OrganizationServices> findByCompany( Long companyNo )
    {
        @SuppressWarnings( "unchecked" )
        List<OrganizationServices> organizationServices =
            super.getHibernateTemplate(  )
                 .find( " from OrganizationServices services where services.parentOrg.orgNo=" + companyNo  +" order by services.orgNo");

        return organizationServices;
    }
    
    /**
     * 重写SAVE方法.
     * @param entity
     */
     @Override
     public void save(OrganizationServices entity){
     	if(entity.getOrgNo()==null || entity.getOrgNo()==0){
     		entity.setOrgNo(organizationDAO.getLatestOrganizationNo());
     	}else{
     		
     		if(entity.getOrgNo()>organizationDAO.getLatestOrganizationNo()){
     			
     			organizationDAO.setLatesOrganizationNo(entity.getOrgNo());
     		}
     		
     	}
     	super.save(entity);
     	organizationDAO.increment();//自增1.
     	
     }
     /**
      * 重写merge方法.
      * @param entity
      * @return OrganizationServices
      */
     @Override
     public OrganizationServices merge(OrganizationServices entity){
     	if(entity.getOrgNo()!=null){
      		if(entity.getOrgNo()>=organizationDAO.getLatestOrganizationNo()){
      			organizationDAO.setLatesOrganizationNo(entity.getOrgNo()+1);
      		}
      	}else{
      		organizationDAO.increment();//自增1.
    	}
    	 
    	 return super.merge(entity);
     }
     
}
