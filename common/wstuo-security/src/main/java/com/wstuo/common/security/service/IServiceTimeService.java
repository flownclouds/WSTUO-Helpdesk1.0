package com.wstuo.common.security.service;

import com.wstuo.common.security.dto.ServiceTimeDTO;

/**
 * 服务时间Service接口
 */
public interface IServiceTimeService
{
    /**
     * 根据机构ID查询服务时间
     * @param id
     * @return ServiceTimeDTO
     */
    ServiceTimeDTO findServiceTimeByOrgNo( Long id );

    /**
     * 保存或修改服务时
      * @param dto
     */
    void saveOrUpdateServiceTime( ServiceTimeDTO dto );
}
