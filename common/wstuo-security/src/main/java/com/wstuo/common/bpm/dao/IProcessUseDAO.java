package com.wstuo.common.bpm.dao;

import com.wstuo.common.bpm.entity.ProcessUse;
import com.wstuo.common.dao.IEntityDAO;

/**
 * ProcessUseDAO Interface Class
 * @author wstuo
 *
 */
public interface IProcessUseDAO extends IEntityDAO<ProcessUse> {
	
}
