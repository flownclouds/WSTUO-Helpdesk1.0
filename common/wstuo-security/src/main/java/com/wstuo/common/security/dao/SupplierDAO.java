package com.wstuo.common.security.dao;

import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dto.SupplierQueryDTO;
import com.wstuo.common.security.entity.Supplier;
import com.wstuo.common.util.DaoUtils;
import com.wstuo.common.util.StringUtils;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 供应商DAO.
 * @author will
 *
 */
@SuppressWarnings( "unchecked" )
public class SupplierDAO
    extends BaseDAOImplHibernate<Supplier>
    implements ISupplierDAO
{
	
	@Autowired
	private IOrganizationDAO organizationDAO;
	
	/**
     * 分页查询供应商信息
     * @param qdto 分页查询DTO:SupplierQueryDTO
     * @param sidx
     * @param sord
     * @return 分页数据：PageDTO
     */
    public PageDTO findPager( SupplierQueryDTO qdto, String sidx, String sord )
    {
        DetachedCriteria dc = DetachedCriteria.forClass( Supplier.class );
        int start = 0;
        int limit = 0;

        if ( qdto != null )
        {
            start = qdto.getStart(  );
            limit = qdto.getLimit(  );

            
           //数据隔离
            if(qdto.getCompanyNo()!=null){
            	
            	 dc.add(Restrictions.eq("companyNo", qdto.getCompanyNo()));
            }
            
            if ( qdto.getCompanyNo(  ) != null )
            {
                dc.createAlias( "parentOrg", "par" );
                dc.add( Restrictions.eq( "par.orgNo",
                                         qdto.getCompanyNo(  ) ) );
            }

            if ( StringUtils.hasText( qdto.getOrgName(  ) ) )
            {
                dc.add( Restrictions.like( "orgName",
                                           qdto.getOrgName(  ),
                                           MatchMode.ANYWHERE ) );
            }

            if ( StringUtils.hasText( qdto.getOfficePhone(  ) ) )
            {
                dc.add( Restrictions.like( "officePhone",
                                           qdto.getOfficePhone(  ),
                                           MatchMode.ANYWHERE ) );
            }

            if ( StringUtils.hasText( qdto.getEmail(  ) ) )
            {
                dc.add( Restrictions.like( "email",
                                           qdto.getEmail(  ),
                                           MatchMode.ANYWHERE ) );
            }

            if ( StringUtils.hasText( qdto.getAddress(  ) ) )
            {
                dc.add( Restrictions.like( "address",
                                           qdto.getAddress(  ),
                                           MatchMode.ANYWHERE ) );
            }
        }
      //排序
        dc = DaoUtils.orderBy(sidx, sord, dc);
        return super.findPageByCriteria( dc, start, limit );
    }

    /**
     * 根据机构编号删除供应商
     * @param orgNo 机构编号：Long orgNo
     */
    public void delete( Long orgNo )
    {
        String hql = "delete from Supplier sp where sp.orgNo='" + orgNo + "'";

        this.getSessionFactory(  ).getCurrentSession(  ).createQuery( hql );
    }

    /**
     * 根据公司编号查找供应商
     * @param companyNo 公司编号：Long companyNo
     * @return 供应商列表：List<Supplier>
     */
    public List<Supplier> findByCompanyNo( Long companyNo )
    {
        List<Supplier> suppliers =
            super.getHibernateTemplate(  ).find( " from Supplier sp where sp.parentOrg.orgNo=" + companyNo );

        return suppliers;
    }
    
    /**
     * 重写SAVE方法.
     * @param entity
     */
     @Override
     public void save(Supplier entity){
     	
     	if(entity.getOrgNo()==null || entity.getOrgNo()==0){
     		entity.setOrgNo(organizationDAO.getLatestOrganizationNo());
     	}else{
     		
     		if(entity.getOrgNo()>organizationDAO.getLatestOrganizationNo()){
     			
     			organizationDAO.setLatesOrganizationNo(entity.getOrgNo());
     		}
     		
     	}
     	super.save(entity);
     	organizationDAO.increment();//自增1.
     }
     
     /**
      * 重写SAVE方法.
      * @param entity
      * @return Supplier
      */
     @Override
     public Supplier merge(Supplier entity){
    	 
    	 
    	 if(entity.getOrgNo()!=null){
     		if(entity.getOrgNo()>=organizationDAO.getLatestOrganizationNo()){
     			organizationDAO.setLatesOrganizationNo(entity.getOrgNo()+1);
     		}
     	}else{
     		organizationDAO.increment();//自增1.
   		}
    	 
    	 return super.merge(entity);
     }
     
    
}
