package com.wstuo.common.bpm.dto;

import com.wstuo.common.bpm.dto.FlowActivityDTO;


public class ProcessAssignDTO {
	private String groupName;
	private String assigneeName;
	private Long groupId;
	private Long assigneeId;
	private FlowActivityDTO nextFlowActivityDTO;
	private FlowActivityDTO flowActivityDTO;
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getAssigneeName() {
		return assigneeName;
	}
	public void setAssigneeName(String assigneeName) {
		this.assigneeName = assigneeName;
	}
	public Long getGroupId() {
		return groupId;
	}
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
	public Long getAssigneeId() {
		return assigneeId;
	}
	public void setAssigneeId(Long assigneeId) {
		this.assigneeId = assigneeId;
	}
	public FlowActivityDTO getNextFlowActivityDTO() {
		return nextFlowActivityDTO;
	}
	public void setNextFlowActivityDTO(FlowActivityDTO nextFlowActivityDTO) {
		this.nextFlowActivityDTO = nextFlowActivityDTO;
	}
	public FlowActivityDTO getFlowActivityDTO() {
		return flowActivityDTO;
	}
	public void setFlowActivityDTO(FlowActivityDTO flowActivityDTO) {
		this.flowActivityDTO = flowActivityDTO;
	}
	
}
