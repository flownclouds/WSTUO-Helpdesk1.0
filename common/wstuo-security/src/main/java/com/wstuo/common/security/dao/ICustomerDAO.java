package com.wstuo.common.security.dao;

import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dto.CustomerQueryDTO;
import com.wstuo.common.security.entity.Customer;

import java.util.List;

/**
 * 客户信息DAO方法接口.
 * @author will
 */
public interface ICustomerDAO
    extends IEntityDAO<Customer>
{
    /**
     * 分页查找公司分页信息.
     * @param qdto CustomerQueryDTO
     * @param sidx
     * @param sord
     * @return 分页信息 PageDTO
     */
    PageDTO findPager( CustomerQueryDTO qdto,String sidx, String sord );

    /**
     * 根据公司信息查找客户列表.
     * @param companyNo 公司编号 Long
     * @return 客户列表  List<Customer>
     */
    List<Customer> findByCompanyNo( Long companyNo );
}
