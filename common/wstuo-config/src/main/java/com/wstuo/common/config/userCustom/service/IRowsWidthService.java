package com.wstuo.common.config.userCustom.service;


import com.wstuo.common.config.userCustom.entity.RowsWidth;

/**
 * RowsWidth interface
 * @author Cnady
 *
 */
public interface IRowsWidthService {

	/**
	 * update and save RowsWidth
	 * @param rowsWidth
	 * @return boolean
	 */
	boolean updateRowsWidth(final RowsWidth rowsWidth);
	/**
	 * find RowsWidth By LoginName
	 * @param loginName
	 * @return RowsWidth
	 */
	RowsWidth findRowsWidthByLoginName(final String loginName);
}
