package com.wstuo.common.config.onlinelog.action;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.config.onlinelog.dto.UserOptLogQueryDTO;
import com.wstuo.common.config.onlinelog.service.IUserOptLogServiceUtil;
import com.wstuo.common.dto.PageDTO;

import org.springframework.beans.factory.annotation.Autowired;


/***
 * 用户操作列表Action类.
 * @author QXY
 * date 2011-02-11
 */
@SuppressWarnings("serial")
public class UserOptLogAction extends ActionSupport {

    @Autowired
    private IUserOptLogServiceUtil useroptlogService;
    private UserOptLogQueryDTO useroptlogQueryDTO;
    private PageDTO pageDTO;
    private int page = 1;
    private int rows = 10;
    private String sidx;
    private String sord;
	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public UserOptLogQueryDTO getUseroptlogQueryDTO() {

        return useroptlogQueryDTO;
    }

    public void setUseroptlogQueryDTO(
        UserOptLogQueryDTO useroptlogQueryDTO) {

        this.useroptlogQueryDTO = useroptlogQueryDTO;
    }

    public PageDTO getPageDTO() {

        return pageDTO;
    }

    public void setPageDTO(PageDTO pageDTO) {

        this.pageDTO = pageDTO;
    }

    public int getPage() {

        return page;
    }

    public void setPage(int page) {

        this.page = page;
    }

    public int getRows() {

        return rows;
    }

    public void setRows(int rows) {

        this.rows = rows;
    }

    /**
     * 分页查询用户操作列表.
     * @return SUCCESS
     */
    public String findPager() {
    	useroptlogQueryDTO.setSidx(sidx);
    	useroptlogQueryDTO.setSord(sord);
        int start = (page - 1) * rows;
        pageDTO = useroptlogService.findUserOptLogByPager(useroptlogQueryDTO, start,rows);
        pageDTO.setPage(page);
        pageDTO.setRows(rows);
        return Action.SUCCESS;
    }
}