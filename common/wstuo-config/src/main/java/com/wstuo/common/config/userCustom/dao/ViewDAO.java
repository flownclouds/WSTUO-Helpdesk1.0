package com.wstuo.common.config.userCustom.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.config.userCustom.dto.ViewQueryDTO;
import com.wstuo.common.config.userCustom.entity.View;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.StringUtils;

/**
 *  viewDAO
 * @author Mark
 *
 */
public class ViewDAO extends BaseDAOImplHibernate<View> implements IViewDAO{

	/**
	 * 分页查询
	 * @param queryDTO
	 * @return PageDTO
	 */
	public PageDTO findPageView(ViewQueryDTO queryDTO){
		final DetachedCriteria dc = DetachedCriteria.forClass(View.class);
		Integer start = 0;
		Integer limit = 10;
		if(queryDTO!=null){
			start = queryDTO.getStart();
			limit = queryDTO.getLimit();
			if(StringUtils.hasText(queryDTO.getViewName())){
				dc.add(Restrictions.eq("viewName", queryDTO.getViewName()));
			}
			if(StringUtils.hasText(queryDTO.getViewContent())){
				dc.add(Restrictions.eq("viewContent", queryDTO.getViewContent()));
			}
			if(StringUtils.hasText(queryDTO.getViewType())){
				dc.add(Restrictions.eq("viewType", queryDTO.getViewType()));
			}
			if(queryDTO.getProportion()!=null && queryDTO.getProportion() != 0){
				dc.add( Restrictions.eq("proportion",queryDTO.getProportion() ) );
			}
			if(StringUtils.hasText(queryDTO.getViewCode())){
				dc.add( Restrictions.eq("viewCode",queryDTO.getViewCode() ) );
			}
			//排序
	        if(StringUtils.hasText(queryDTO.getSord())&&StringUtils.hasText(queryDTO.getSidx())){
	        	if("desc".equals(queryDTO.getSord())){
	        		dc.addOrder(Order.desc(queryDTO.getSidx()));
	        	}else{
	        		dc.addOrder(Order.asc(queryDTO.getSidx()));
	        	}
	        }
	        
		}
        return super.findPageByCriteria(dc,start,limit);
	}
	/**
	 * 查询List
	 * @param queryDTO
	 * @return PageDTO
	 */
	public List<View> findListView(ViewQueryDTO queryDTO){
		final DetachedCriteria dc = DetachedCriteria.forClass(View.class);
		if(queryDTO!=null){
			if(StringUtils.hasText(queryDTO.getViewName())){
				dc.add(Restrictions.eq("viewName", queryDTO.getViewName()));
			}
			if(StringUtils.hasText(queryDTO.getViewContent())){
				dc.add(Restrictions.eq("viewContent", queryDTO.getViewContent()));
			}
			if(StringUtils.hasText(queryDTO.getViewType())){
				dc.add(Restrictions.eq("viewType", queryDTO.getViewType()));
			}
			if(queryDTO.getProportion()!=null && queryDTO.getProportion() != 0){
				dc.add( Restrictions.eq("proportion",queryDTO.getProportion() ) );
			}
			//排序
	        if(StringUtils.hasText(queryDTO.getSord())&&StringUtils.hasText(queryDTO.getSidx())){
	        	if("desc".equals(queryDTO.getSord())){
	        		dc.addOrder(Order.desc(queryDTO.getSidx()));
	        	}else{
	        		dc.addOrder(Order.asc(queryDTO.getSidx()));
	        	}
	        }
	        
		}
		return super.getHibernateTemplate().findByCriteria(dc);
	}

}
