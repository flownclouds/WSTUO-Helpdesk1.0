package com.wstuo.common.config.dictionary.action;

import java.io.InputStream;
import java.util.List;
import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.config.dictionary.dto.DataDictionaryGroupDTO;
import com.wstuo.common.config.dictionary.dto.DataDictionaryGroupQueryDTO;
import com.wstuo.common.config.dictionary.service.IDataDictionaryGroupService;
import com.wstuo.common.dto.PageDTO;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * 数据字典分组Action类.
 * 
 * @author QXY date 2011-02-11
 */
@SuppressWarnings("serial")
public class DataDictionaryGroupAction extends ActionSupport {

	@Autowired
	private IDataDictionaryGroupService idataDictionaryGroupService;
	private DataDictionaryGroupDTO dataDictionaryQueryDto = new DataDictionaryGroupDTO();
	private Long[] groupNo;
	private List<DataDictionaryGroupDTO> dataDictionaryGroups;
	private String fileName = "";
	private DataDictionaryGroupQueryDTO reDto = new DataDictionaryGroupQueryDTO();
	private PageDTO pageDtos;
	private int page = 1;
	private int rows = 10;
	private DataDictionaryGroupDTO ddgDto = new DataDictionaryGroupDTO();
	private InputStream exportStream;

	private String sidx;
	private String sord;

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public DataDictionaryGroupDTO getDataDictionaryQueryDto() {

		return dataDictionaryQueryDto;
	}

	public void setDataDictionaryQueryDto(DataDictionaryGroupDTO dataDictionaryQueryDto) {

		this.dataDictionaryQueryDto = dataDictionaryQueryDto;
	}

	public Long[] getGroupNo() {

		return groupNo;
	}

	public void setGroupNo(final Long[] groupNo) {

		this.groupNo = groupNo;
	}

	public List<DataDictionaryGroupDTO> getDataDictionaryGroups() {

		return dataDictionaryGroups;
	}

	public void setDataDictionaryGroups(List<DataDictionaryGroupDTO> dataDictionaryGroups) {

		this.dataDictionaryGroups = dataDictionaryGroups;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public PageDTO getPageDtos() {
		return pageDtos;
	}

	public void setPageDtos(PageDTO pageDtos) {
		this.pageDtos = pageDtos;
	}

	public int getPage() {

		return page;
	}

	public void setPage(int page) {

		this.page = page;
	}

	public int getRows() {

		return rows;
	}

	public void setRows(int rows) {

		this.rows = rows;
	}

	public DataDictionaryGroupQueryDTO getReDto() {

		return reDto;
	}

	public void setReDto(DataDictionaryGroupQueryDTO reDto) {

		this.reDto = reDto;
	}

	public IDataDictionaryGroupService getIdataDictionaryGroupService() {

		return idataDictionaryGroupService;
	}

	public void setIdataDictionaryGroupService(IDataDictionaryGroupService idataDictionaryGroupService) {

		this.idataDictionaryGroupService = idataDictionaryGroupService;
	}

	public DataDictionaryGroupDTO getDdgDto() {

		return ddgDto;
	}

	public void setDdgDto(DataDictionaryGroupDTO ddgDto) {

		this.ddgDto = ddgDto;
	}

	public InputStream getExportStream() {
		return exportStream;
	}

	public void setExportStream(InputStream exportStream) {
		this.exportStream = exportStream;
	}

	/**
	 * 分页查询数据字典.
	 * 
	 * @return SUCCESS
	 */
	public String find() {
		int start = (page - 1) * rows;

		reDto.setSidx(sidx);
		reDto.setSord(sord);

		reDto.setStart(start);
		reDto.setLimit(rows);
		pageDtos = idataDictionaryGroupService.findDictionaryGroupByPage(reDto);
		pageDtos.setRows(rows);
		pageDtos.setPage(page);

		return SUCCESS;
	}

	/**
	 * 查找所有数据字典分组.
	 * 
	 * @return SUCCESS
	 */
	public String findGroup() {

		dataDictionaryGroups = idataDictionaryGroupService.findAllDataDictionaryGroup();

		return "showgroups";
	}

	/**
	 * 添加数据字典分组.
	 * 
	 * @return SUCCESS
	 */
	public String saveGroup() {
		idataDictionaryGroupService.saveDataDictionaryGroup(ddgDto);
		return SUCCESS;
	}

	/**
	 * 保存数据字典分组.
	 * 
	 * @return SUCCESS
	 */
	public String mergeGroup() {

		idataDictionaryGroupService.mergeDataDictionaryGroup(ddgDto);
		return SUCCESS;
	}

	/**
	 * 删除数据字典分组.
	 * 
	 * @return SUCCESS
	 */
	public String removeGroup() {

		idataDictionaryGroupService.removeDataDictionaryGroups(groupNo);
		return SUCCESS;
	}

	/**
	 * 导出
	 * 
	 * @return String
	 */
	public String exportDataDictionaryGroup() {
		fileName = "DataDictionaryGroup.csv";

		reDto.setStart(0);
		reDto.setLimit(10000);
		exportStream = idataDictionaryGroupService.exportDataDictionaryGroup(reDto);
		return "exportFileSuccessful";
	}

	/**
	 * 查询扩展属性中的数据字典类
	 * 
	 * @return String
	 */
	public String findDataDictionary() {
		dataDictionaryGroups = idataDictionaryGroupService.findAllDataDictionaryGroup();
		return "dataDictionaryGroups";
	}
	
	public String findDataDictionaryByType() {
		dataDictionaryGroups = idataDictionaryGroupService.findAllDataDictionaryGroupByType(reDto.getGroupType());
		return "dataDictionaryGroups";
	}

	/**
	 * 根据ID查询扩展属性中的数据字典名称
	 * 
	 * @return String
	 */
	public String findDataDictionaryById() {
		ddgDto = idataDictionaryGroupService.findDataDictionaryGroupById(ddgDto.getGroupNo());
		return "dataDictionarygroupDTO";
	}
}