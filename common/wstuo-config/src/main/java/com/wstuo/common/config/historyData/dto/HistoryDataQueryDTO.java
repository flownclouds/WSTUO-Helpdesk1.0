package com.wstuo.common.config.historyData.dto;

/**
 * 历史数据查询DTO
 * @author Will
 *
 */
public class HistoryDataQueryDTO {
	
	
	private String entityName; //实体名称
	private Long entityId;     //实体ID引用 
	
	private Integer start;
	private Integer limit;
	private String sidx;
	private String sord;
	
	
	public String getEntityName() {
		return entityName;
	}
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
	public Long getEntityId() {
		return entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	
	
	
}
