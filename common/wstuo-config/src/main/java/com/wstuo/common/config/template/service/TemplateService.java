package com.wstuo.common.config.template.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.ezmorph.object.DateMorpher;
import net.sf.json.JSONObject;
import net.sf.json.util.JSONUtils;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.config.category.dao.EventCategoryDAO;
import com.wstuo.common.config.category.dto.EventCategoryDTO;
import com.wstuo.common.config.category.entity.EventCategory;
import com.wstuo.common.config.category.service.IEventCategoryService;
import com.wstuo.common.config.template.dao.ITemplateDAO;
import com.wstuo.common.config.template.dto.TemplateDTO;
import com.wstuo.common.config.template.dto.TemplateRequestDTO;
import com.wstuo.common.config.template.entity.Template;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.exception.ApplicationException;
import com.wstuo.common.security.dto.CompanyDTO;
import com.wstuo.common.security.entity.Company;
import com.wstuo.common.security.service.ICompanyService;
import com.wstuo.common.security.service.IMyAllCustomerCompanys;
import com.wstuo.common.security.service.IUserInfoService;
import com.wstuo.common.security.utils.AppContext;
import com.wstuo.common.util.StringUtils;
import com.wstuo.common.util.TimeUtils;

/**
 * 内容模板Service
 * @author WSTUO
 *
 */
public class TemplateService implements ITemplateService{
    /**
     * Logger for this class
     */
    private static final Logger LOGGER = Logger.getLogger(TemplateService.class);

	@Autowired
	private ITemplateDAO templateDAO;
	@Autowired
	private EventCategoryDAO eventCategoryDAO;
	@Autowired
	private ICompanyService companyService;
	@Autowired
	private IEventCategoryService eventCategoryService;
	@Autowired
	private IMyAllCustomerCompanys myAllCustomerCompanys;
	@Autowired
	private AppContext appctx;
	@Autowired
	private IUserInfoService userInfoService;
	/**
	 * 查询内容模板所有
	 * @param templateDTO 内容模板
	 * @param start 开始页数
	 * @param limit 开始记录数
	 * @param sord 排序规则
	 * @param sidx 排序的属性
	 * @return 分页DTO
	 */
	@SuppressWarnings("unchecked")
    @Transactional
	public PageDTO findTemplatePager(TemplateDTO templateDTO, int start, int limit,String sord, String sidx){
		PageDTO page = templateDAO.findTemplatePager(templateDTO, start, limit, sord, sidx);
		List<Template> entities=(List<Template>) page.getData();
		List<TemplateDTO> dtos=new ArrayList<TemplateDTO>();
		for(Template entity:entities){
			TemplateDTO dto=new TemplateDTO();
			TemplateDTO.entity2dto(entity, dto);
			dtos.add(dto);
		}
		page.setData(entities);
		return page;
	}
	
	/**
	 * 查询所有内容模板
	 * @param templateDTO
	 */
	public List<TemplateDTO> findAllTemplate(TemplateDTO templateDTO){
		templateDTO = setTemplateDTO(templateDTO);
		List<Template> entities = templateDAO.findAllTemplate(templateDTO);
		List<TemplateDTO> dtos=new ArrayList<TemplateDTO>();
		for(Template entity:entities){
			TemplateDTO dto=new TemplateDTO();
			TemplateDTO.entity2dto(entity, dto);
			dtos.add(dto);
		}
		return dtos;
	}
	//设置templateDTO
	private TemplateDTO setTemplateDTO(TemplateDTO templateDTO){
		String loginName=appctx.getCurrentLoginName();
		Set<Company> companys = new HashSet<Company>();// 唯一集合
		Set<String> templateCompanyNoJson = new HashSet<String>();
		Set<String> templateCategoryNoJson = new HashSet<String>();
		Long[] categoryNos =null;
		if(StringUtils.hasLength(loginName)){
			// 负责的所属客户
			companys.addAll(myAllCustomerCompanys.findCompanysMyAllCustomer(loginName));
			if(!"knowledge".equals(templateDTO.getTemplateType())){
				for (Company company : companys) {
					Long comNo  = company.getOrgNo();
					String str = "\"companyNo\":"+comNo+",";
					templateCompanyNoJson.add(str);
				}
			}
			if("request".equals(templateDTO.getTemplateType())){
				categoryNos = userInfoService.findCategoryNosByLoginName(loginName, "Request_Category_");// 设置查看权限
				for (Long categoryNo : categoryNos) {
					String str = "\"requestCategoryNo\":"+categoryNo+",";
					templateCategoryNoJson.add(str);
				}
			}else if("knowledge".equals(templateDTO.getTemplateType())){
				categoryNos = userInfoService.findCategoryNosByLoginName(loginName, "Knowledge_Category_");// 设置查看权限
				for (Long categoryNo : categoryNos) {
					String str = "\"categoryNo\":"+categoryNo+",";
					templateCategoryNoJson.add(str);
				}
			}else if("configureItem".equals(templateDTO.getTemplateType())){
				categoryNos = userInfoService.findCategoryNosByLoginName(loginName, "CONFIGUREITEMCATEGORY_RES");// 设置查看权限
				for (Long categoryNo : categoryNos) {
					String str = "\"categoryNo\":"+categoryNo+",";
					templateCategoryNoJson.add(str);
				}
			}
		}
		templateDTO.setTemplateCompanyNoJson(templateCompanyNoJson);
		templateDTO.setTemplateCategoryNoJson(templateCategoryNoJson);
		return templateDTO;
	}
	/**
	 * 新增内容模板
	 * @param dto 模板ＤＴＯ
	 * @param obj 对象
	 */
	@Transactional
	public void saveTemplate(Object obj,TemplateDTO dto){
		if(dto.getTemplateName()!=null){
			if(dto.getTemplateType()!=null){
				TemplateRequestDTO trDTO = new TemplateRequestDTO();
				dto2entity(obj, trDTO);
				//如果是知识库模版，需要处理他的服务目录。
				if(dto.getTemplateType().equals("knowledge")
				        && trDTO.getKnowledServiceNo()!=null 
				        && trDTO.getKnowledServiceNo().size()>0){
					//服务
					Map<Long,String> map=new HashMap<Long, String>();
		        	for(Long id:trDTO.getKnowledServiceNo()){
		        		EventCategory ev=eventCategoryDAO.findById(id);
		        		map.put(ev.getEventId(), ev.getEventName());
		        	}
		        	trDTO.setKnowledgeServiceLi(map);
				}
				if(dto.getDto()!=null){
					trDTO.setCiId(dto.getDto().getCiId());
					trDTO.setCiname(dto.getDto().getCiname());
					trDTO.setCino(dto.getDto().getCino());
					trDTO.setCiCategoryName(dto.getDto().getCiCategoryName());
					trDTO.setCiStatus(dto.getDto().getCiStatus());
					trDTO.setSubServiceName(dto.getDto().getSubServiceName());
				}
				dto.setDto(trDTO);
				Template entity=new Template();
				TemplateDTO.dto2entity(dto, entity);
				//dto to json
				if(dto.getDto().getCompanyNo()!=null){
					entity.setCompanyNo(dto.getDto().getCompanyNo());
				}
				
				try {
					entity.setTemplateJson(JSONUtil.serialize(dto.getDto()));
				} catch (JSONException e) {
					throw new ApplicationException("ERROR_CONVERT_DTO_TO_JSON/n",e);
				}
				entity.setFormId(trDTO.getFormId());
				entity.setServiceDirId(dto.getServiceDirId());
				if(dto.getTemplateId()==null){
					templateDAO.save(entity);
					dto.setTemplateId(entity.getTemplateId());
				}else{
					templateDAO.merge(entity);
				}
				
			}else{
				throw new ApplicationException("ERROR_TEMPLATETYPE_NULL","ERROR_TEMPLATETYPE_IS_NULL");
			}
		}else{
			throw new ApplicationException("ERROR_TEMPLATENAME_NULL","ERROR_TEMPLATENAME_IS_NULL");
		}
	}
	/**
     * Transform dto to entity.
     * @param dto        DTO object
     * @param entity domain entity object
     */
	private static void dto2entity(Object dto, Object entity) {

        try {
            if (dto != null) {
                BeanUtils.copyProperties(entity, dto);
            }
        } catch (Exception ex) {

            throw new ApplicationException(ex);
        }
    }
	/**
	 * 根据内容模板类型查询
	 * @param type 模板类型
	 * @return 内容模板实体
	 */
	public List<Template> findByTemplateType(String type){
		List<Template> li=templateDAO.findBy("templateType", type);
		return li;
	}
	/**
	 * 根据内容模板ID查询出内容详细信息
	 * @param id 内容模板ID
	 * @return 模板DTO
	 */
	public TemplateDTO findByTemplateId(Long id){ 
		if(id!=null){
			TemplateDTO dto=new TemplateDTO();
			TemplateRequestDTO requestDTO=new TemplateRequestDTO();
			Template entity=templateDAO.findById(id);
			try {
		        String[] dateFormats = new String[] {TimeUtils.DATE_PATTERN};
		        JSONUtils.getMorpherRegistry().registerMorpher(new DateMorpher(dateFormats));
		        JSONObject jsonObject = JSONObject.fromObject( 
		        		JSONUtil.deserialize(entity.getTemplateJson()) );
				requestDTO=(TemplateRequestDTO)JSONObject.toBean(
						jsonObject,
						Class.forName("com.wstuo.common.config.template.dto.TemplateRequestDTO"));
				templateJsonDate(jsonObject, requestDTO);//解决丢失时分秒的问题
				CompanyDTO com =companyService.findCompany(requestDTO.getCompanyNo());
				if(com!=null){
					requestDTO.setCompanyName(com.getCompanyName());
				}
			} catch (JSONException e) {
			    LOGGER.error(e);
			} catch (ClassNotFoundException e) {
			    LOGGER.error(e);
            }
			if(requestDTO.getLocId()!=null){
				EventCategoryDTO categoryDTO=eventCategoryService.findLocationNameById(requestDTO.getLocId());
				requestDTO.setLocationName(categoryDTO.getCategoryLocation());
			}
				
			dto.setTemplateId(entity.getTemplateId());
			dto.setTemplateType(entity.getTemplateType());
			dto.setTemplateName(entity.getTemplateName());
			dto.setIsNewForm(entity.getIsNewForm());
			dto.setIsShowBorder(entity.getIsShowBorder());
			dto.setFormId(entity.getFormId());
			dto.setServiceDirId(entity.getServiceDirId());
			dto.setDto(requestDTO);
			return dto;
		}
		else{
			throw new ApplicationException("ERROR_TEMPLATEID_NULL","ERROR_TEMPLATEID_IS_NULL");
		}
		
	}
	/**
	 * json转换时丢失时分秒；
	 * @param jsonObject
	 * @param requestDTO
	 */
	private void templateJsonDate(JSONObject jsonObject,TemplateRequestDTO requestDTO){
		if( jsonObject == null || requestDTO == null ){ return; }
		String palnEndTime = jsonObject.getString("planEndTime");
		String planStartTime = jsonObject.getString("planStartTime");
		if( planStartTime != null){
			Date startDate = TimeUtils.parse(planStartTime.replace("T", " "), TimeUtils.DATETIME_PATTERN);
			if( startDate != null){
				requestDTO.setPlanStartTime(startDate);
			}
		}
		if( palnEndTime != null){
			Date endDate = TimeUtils.parse(palnEndTime.replace("T", " "), TimeUtils.DATETIME_PATTERN);
			if( endDate != null){
				requestDTO.setPlanEndTime(endDate);
			}
		}
	}
	
	/**
	 * 根据ID删除内容模板
	 * @param templateId 内容模板ID
	 */
	@Transactional
	public void delTemplate(Long[] templateId){
		if(templateId!=null && templateId.length>0){
			templateDAO.deleteByIds(templateId);
		}else{
			throw new ApplicationException("ERROR_TEMPLATEID_NULL","ERROR_TEMPLATEID_IS_NULL");
		}
	}
	/**
	 * 编辑内容模板名称
	 * @param dto 内容模板DTO
	 */
	@Transactional
	public void editTemplateName(TemplateDTO dto) {
		Template entity=templateDAO.findById(dto.getTemplateId());
		entity.setTemplateName(dto.getTemplateName());
		templateDAO.merge(entity);
	}
	
	/**
	 * 查询关联表单的数量
	 * @param formId
	 * @return
	 */
	public Integer findTemplateNumByFormId(Long[] formIds){
		Integer num =templateDAO.findTemplateNumByFormId(formIds);
		return num;
	}
	
}
