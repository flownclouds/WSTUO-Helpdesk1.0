package com.wstuo.common.config.onlinelog.dto;

import java.util.Date;

import com.wstuo.common.dto.AbstractValueObject;

/***
 * UserOptLog QueryDTO Class.
 *
 * @author spring date 2010-10-11
 */
public class UserOptLogQueryDTO extends AbstractValueObject {

		/**
	    * UserName
	    */
	    private String userName;
	    /**
	     * MethodName
	     */
	    private String methodName;
	    /**
	     * Time Start
	     */
	    private Date timeStart;
	    /**
	     * Time End
	     */
	    private Date timeEnd;
	    /**
	     * Start
	     */
	    private Integer start;

	    /**
	     * Limit
	     */
	    private Integer limit;

	    
	    private Long resourceId; 
	    
	    private Long companyNo;
	    
	    private String sidx;
	    private String sord;
	    private String resourceName;
	    
	    private String userFullName;
	    
	    
	    
		public String getSidx() {
			return sidx;
		}

		public void setSidx(String sidx) {
			this.sidx = sidx;
		}

		public String getSord() {
			return sord;
		}

		public void setSord(String sord) {
			this.sord = sord;
		}

		public Long getCompanyNo() {
			return companyNo;
		}

		public void setCompanyNo(Long companyNo) {
			this.companyNo = companyNo;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

		public String getMethodName() {
			return methodName;
		}

		public void setMethodName(String methodName) {
			this.methodName = methodName;
		}

		public Date getTimeStart() {
			return timeStart;
		}

		public void setTimeStart(Date timeStart) {
			this.timeStart = timeStart;
		}

		public Date getTimeEnd() {
			return timeEnd;
		}

		public void setTimeEnd(Date timeEnd) {
			this.timeEnd = timeEnd;
		}

		public Integer getStart() {
			return start;
		}

		public void setStart(Integer start) {
			this.start = start;
		}

		public Integer getLimit() {
			return limit;
		}

		public void setLimit(Integer limit) {
			this.limit = limit;
		}

		public Long getResourceId() {
			return resourceId;
		}

		public void setResourceId(Long resourceId) {
			this.resourceId = resourceId;
		}

		public String getResourceName() {
			return resourceName;
		}

		public void setResourceName(String resourceName) {
			this.resourceName = resourceName;
		}

		public String getUserFullName() {
			return userFullName;
		}

		public void setUserFullName(String userFullName) {
			this.userFullName = userFullName;
		}

	    
	
		
}
