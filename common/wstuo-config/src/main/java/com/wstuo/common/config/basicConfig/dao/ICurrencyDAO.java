package com.wstuo.common.config.basicConfig.dao;

import com.wstuo.common.config.basicConfig.entity.Currency;
import com.wstuo.common.dao.IEntityDAO;

public interface ICurrencyDAO extends IEntityDAO<Currency>{

}