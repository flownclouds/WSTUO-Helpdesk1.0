package com.wstuo.common.config.userCustom.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.config.userCustom.comparator.ComparatorDashboard;
import com.wstuo.common.config.userCustom.dao.IDashboardDAO;
import com.wstuo.common.config.userCustom.dto.DashboardDTO;
import com.wstuo.common.config.userCustom.dto.DashboardQueryDTO;
import com.wstuo.common.config.userCustom.entity.Dashboard;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.file.csv.CSVReader;
import com.wstuo.common.security.utils.FileEncodeUtils;
import com.wstuo.common.util.StringUtils;

/**
 * 面板Service类
 * @author WSTUO
 *
 */
public class DashboardService implements IDashboardService {
	@Autowired
	private IDashboardDAO dashboardDAO;
	private static final Logger LOGGER = Logger.getLogger(DashboardService.class );    
	
	/**
	 * 查询全部面板信息
	 * @return List<DashboardDTO>
	 */
	@Transactional
	public List<DashboardDTO> findDashboardAll(){
		List<Dashboard> list=dashboardDAO.findAll();
		List<DashboardDTO> dtos=new ArrayList<DashboardDTO>(list.size());
		ComparatorDashboard comparator=new ComparatorDashboard();
    	Collections.sort(list,comparator);
		for(Dashboard entity:list){
			DashboardDTO dto=new DashboardDTO();
			DashboardDTO.entity2dto(entity, dto);
			dtos.add(dto);
		}
		return dtos;
	};
	
	/**
	 * 分页查询
	 * @param queryDTO
	 * @return PageDTO
	 */
	@Transactional
	public PageDTO findPageDashboard(DashboardQueryDTO queryDTO){
		
		PageDTO page=dashboardDAO.findPageDashboard(queryDTO);
		List<Dashboard> list=(List<Dashboard>)page.getData();
		List<DashboardDTO> listDTO=new ArrayList<DashboardDTO>(list.size());
		for(Dashboard entity:list){
			DashboardDTO dto=new DashboardDTO();
			DashboardDTO.entity2dto(entity, dto);
			listDTO.add(dto);
		}
		page.setData(listDTO);
		return page;
	} 
	
	
	
	/**
	 * 添加面板
	 * @param dto
	 */
	@Transactional
	public void saveDashboard(final DashboardDTO dto){
		Dashboard entity=new Dashboard();
		DashboardDTO.dto2entity(dto, entity);
		dashboardDAO.merge(entity);
	}
	/**
	 * 编辑面板
	 * @param dto
	 */
	@Transactional
	public void editDashboard(final DashboardDTO dto){
		Dashboard entity=dashboardDAO.findById(dto.getDashboardId());
		DashboardDTO.dto2entity(dto, entity);
		dashboardDAO.merge(entity);
	}
	/**
	 * 删除面板
	 * @param ids
	 */
	@Transactional
	public void deteleDashboard(final Long[] ids){
		dashboardDAO.deleteByIds(ids);
	}
	
	/**
	 * 面板模块导入
	 * @param importFile
	 * @return String
	 */
	@Transactional
    public String importDashboard(File importFile){
		String result = "";
    	int insert=0;
    	int update=0;
    	int total=0;
    	int failure=0;
    	Reader rd = null;
    	CSVReader reader = null;
    	try {
			String fileEncode = FileEncodeUtils.getFileEncode(importFile);
			rd = new InputStreamReader(new FileInputStream(importFile), fileEncode);// 以字节流方式读取数据
			reader=new CSVReader(rd);
			String [] line=null;
			try {
				while((line=reader.readNext())!=null){
					Long sortNo=0L;
					Long defaultShow=0L;
					if(StringUtils.hasText(line[2])){
						sortNo=Long.parseLong(line[2]);
					}
					if(StringUtils.hasText(line[3])){
						defaultShow=Long.parseLong(line[3]);
					}
					Dashboard entity=new Dashboard();
					entity.setDashboardName(line[0]);
					entity.setDashboardDivId(line[1]);
					entity.setSortNo(sortNo);
					entity.setDefaultShow(defaultShow);
					dashboardDAO.save(entity);
					insert++;
				 	total++;
				}
				result = "Total:"+total+",&nbsp;Insert:"+insert+",&nbsp;Update:"+update+",&nbsp;Failure:"+failure;
			} catch (Exception e) {
				LOGGER.error("import Dashboard", e);
				result = "IOError";
			}
		} catch (Exception e1) {
			LOGGER.error("import Dashboard", e1);
			result = "FileNotFound";
		}finally {
			try {
				if(rd!=null){
					rd.close();
				}
				if(reader!=null){
					reader.close();
				}
			} catch (IOException e) {
				LOGGER.error(e);
			}
		}
    	return result;
    }
	
}
