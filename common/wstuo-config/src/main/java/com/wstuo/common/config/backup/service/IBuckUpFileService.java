package com.wstuo.common.config.backup.service;

import java.io.InputStream;

import com.wstuo.common.dto.PageDTO;

public interface IBuckUpFileService {
	/**
	 * 取得备份文件列表.
	 */
	PageDTO showAllBackFiles();
	/**
	 * 删除备份文件
	 * @param fileNames
	 */
	void deleteBackup(String[] fileNames);
	/**
	 * 创建备份文件
	 */
	String createBackup();
	
	/**
	 * 还原数据库.
	 */
	public void restoreBackup(String fileName);
	/**
	 * 下载备份文件.
	 */
	public InputStream download(String fileName);
}
