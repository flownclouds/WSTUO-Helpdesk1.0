package com.wstuo.common.config.category.service;

import com.wstuo.common.config.category.dto.CategoryDTO;
import com.wstuo.common.config.category.dto.CategoryTreeViewDTO;
import com.wstuo.common.config.category.entity.CICategory;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

/**
 * CICategoryService Interface 2010.9.13
 *
 * @author spring
 *
 */
public interface ICICategoryService
{
    
    /**
     * 判断编辑时分类是否存在
     * @param dto
     * @return
     */
    public boolean isCategoryExistdOnEdit(CategoryDTO dto);
    
    /**
     * 判断分类是否存在
     * @param ec
     * @return
     */
    public boolean isCategoryExisted(CategoryDTO dto);
    
    /**
     * Check all CICategoryTree
     * @param categoryRoot
     * @param isAl
     * @param userName
     * @param parentEventId
     * @return  List<CategoryTreeViewDTO>
     */

    List<CategoryTreeViewDTO> findCICategoryTreeDtos( String categoryRoot,boolean isAl,String userName,Long parentEventId);
    
	/**
     * view configuration category tree
     */
    CategoryTreeViewDTO findCICategoryTree(String categoryRoot,boolean isAll,String userName,boolean isSimpleDto,Long parentEventId);
    /**
     * view configuration category list
     */
    List<CategoryTreeViewDTO> findCICategoryTreeSub(String categoryRoot,boolean isAll,String userName,boolean isSimpleDto,Long parentEventId);
    
    /**
     * Find CICategory
     *
     * @param dto
     * @return CategoryDTO
     */
    CategoryDTO findCICategoryById( Long dto );

    /**
     * Save CICategory
     *
     * @param dto
     */
    void saveCICategory( CategoryDTO dto );

    /**
     * Remove CICategory
     *
     * @param kno
     * @return boolean
     */
    boolean removeCICategory( Long kno );

    /**
     * Update CICategory
     *
     * @param dto
     */
    void updateCICategory( CategoryDTO dto );

    /**
     * Change CICategory
     *
     * @param dto
     */
    void changeCICategory( CategoryDTO dto );
    
     /**
     * copyCICategory
     * @param dto
     */
    Long copyCICategory( CategoryDTO dto );
    
    /**
     * 导出配置项分类
     * @param dtype
     */
    InputStream exportCiCategory(String dtype);
    /**
     * 导入配置项分类
     * @param importFile
     */
    String importCiCategory(File importFile);
    
    /**
     * 同步配置项分类和资源.
     */
    void syncCICategory();
    
     /**
      * 递归查找子分类
      * @param categoryId
      */
     List<CategoryDTO> findSubCategorys(Long categoryId);
     
     /**
      * 根据权限递归查找子分类
      * @param categoryId
      */
     List<CategoryDTO> findSubCategorysByResType(Long categoryId);
     
     /**
      * 递归查找子分类.数组
      * @param categoryId
      */
     @Transactional
     public List<CategoryDTO> findSubCategorysArray(Long[] categoryId);
     
     Long findCategoryId(Long cno);
     
     /**
      * 根据指定的分类id查询分类及父分类名称，如：配置项分类-桌面系统；“-”是默认分隔符
      * @param categoryId
      * @return
      */
     String findCategoryNameIncludParent(CICategory ciCategory, Long categoryId);
     /**
      * 根据eavNos查询分类
      */
     void deleteEavEntity(Long[] eavNos);
}
