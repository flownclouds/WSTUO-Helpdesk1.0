package com.wstuo.common.config.onlinelog.dao;


import com.wstuo.common.config.onlinelog.dto.UserOptLogQueryDTO;
import com.wstuo.common.config.onlinelog.entity.UserOptLog;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

/**
 * 用户操作日志DAO类接口.
 * @author QXY date 2011-02-11
 *
 */
public interface IUserOptLogDAO extends IEntityDAO<UserOptLog> {

	/**
	 * 分页查询数据.
	 * @param useroptlogQueryDTO 查询DTO
	 * @param start 开始数据行
	 * @param limit 每页数据行
	 */
    PageDTO findPager(final UserOptLogQueryDTO useroptlogQueryDTO,int start, int limit);
}