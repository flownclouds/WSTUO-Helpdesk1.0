package com.wstuo.common.config.template.dto;

import java.util.Set;

import com.wstuo.common.dto.BaseDTO;

/**
 * 模板DTO
 * @author WSTUO
 *
 */

@SuppressWarnings("serial")
public class TemplateDTO extends BaseDTO{
	private Long templateId;//Id
	private String templateName;//模板名称
	private String templateType;//模板类型
	private String templateJson;//模板详细信息
	private TemplateRequestDTO dto;
	
	private Set<String> templateCompanyNoJson;
	private Set<String> templateCategoryNoJson;
	
	private String isShowBorder;
	private Boolean isNewForm;
	private Long serviceDirId=0l;//服务目录id
	private Long categoryNo=0L;//配置项分类
	
	public Set<String> getTemplateCompanyNoJson() {
		return templateCompanyNoJson;
	}
	public void setTemplateCompanyNoJson(Set<String> templateCompanyNoJson) {
		this.templateCompanyNoJson = templateCompanyNoJson;
	}
	public Set<String> getTemplateCategoryNoJson() {
		return templateCategoryNoJson;
	}
	public void setTemplateCategoryNoJson(Set<String> templateCategoryNoJson) {
		this.templateCategoryNoJson = templateCategoryNoJson;
	}
	public Long getCategoryNo() {
		return categoryNo;
	}
	public void setCategoryNo(Long categoryNo) {
		this.categoryNo = categoryNo;
	}
	public Long getServiceDirId() {
		return serviceDirId;
	}
	public void setServiceDirId(Long serviceDirId) {
		this.serviceDirId = serviceDirId;
	}
	public Boolean getIsNewForm() {
		return isNewForm;
	}
	public void setIsNewForm(Boolean isNewForm) {
		this.isNewForm = isNewForm;
	}
	public String getIsShowBorder() {
		return isShowBorder;
	}
	public void setIsShowBorder(String isShowBorder) {
		this.isShowBorder = isShowBorder;
	}
	private Long formId;//表单Id
	
	public Long getFormId() {
		return formId;
	}
	public void setFormId(Long formId) {
		this.formId = formId;
	}
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	public Long getTemplateId() {
		return templateId;
	}
	public void setTemplateId(Long templateId) {
		this.templateId = templateId;
	}
	public String getTemplateType() {
		return templateType;
	}
	public void setTemplateType(String templateType) {
		this.templateType = templateType;
	}
	public String getTemplateJson() {
		return templateJson;
	}
	public void setTemplateJson(String templateJson) {
		this.templateJson = templateJson;
	}
	public TemplateRequestDTO getDto() {
		return dto;
	}
	public void setDto(TemplateRequestDTO dto) {
		this.dto = dto;
	}
}
