package com.wstuo.common.config.server.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.config.server.dto.ServerUrlDTO;
import com.wstuo.common.config.server.service.IServerUrlService;

/**
 * Server Url Action class 
 * @author Will
 *
 */
@SuppressWarnings("serial")
public class ServerUrlAction extends ActionSupport{
	
	@Autowired
	private IServerUrlService serverUrlService;
	
	private ServerUrlDTO serverUrlDTO;

	public ServerUrlDTO getServerUrlDTO() {
		return serverUrlDTO;
	}

	public void setServerUrlDTO(ServerUrlDTO serverUrlDTO) {
		this.serverUrlDTO = serverUrlDTO;
	}
	
	/**
	 * find ServerUrl
	 * @return String
	 */
	public String findServerUrl(){
		
		serverUrlDTO = serverUrlService.findServiceUrl();
		return "serverUrlDTO";
	}
	/**
	 * save ServerUrl
	 * @return String
	 */
	public String saveServerUrl(){
		
		serverUrlService.saveOrUpdateServiceUrl(serverUrlDTO);
		return "result";
	}
	

}
