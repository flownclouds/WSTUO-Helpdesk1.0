package com.wstuo.common.config.basicConfig.service;

import org.apache.log4j.Logger;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.config.basicConfig.dao.ICurrencyDAO;
import com.wstuo.common.config.basicConfig.dto.CurrencyDTO;
import com.wstuo.common.config.basicConfig.entity.Currency;
import com.wstuo.common.security.utils.AppContext;

/**
 * 货币Service实现类
 * @author will
 */
public class CurrencyService implements ICurrencyService {
    /**
     * Logger for this class
     */
    private static final Logger LOGGER = Logger.getLogger(CurrencyService.class);
    /** 人民币简写符号 */
    public static final String CURRENCY_CNY = "CNY";
	@Autowired
	private ICurrencyDAO currencyDAO;
	@Autowired
	private AppContext appctx;
	
	/**
	 * 获取货币信息
	 */
	@Transactional
	public CurrencyDTO findDefaultCurrency(){
		CurrencyDTO dto = new CurrencyDTO();
		List<Currency> list = currencyDAO.findAll();
		if(list!=null && list.size()>0){
			CurrencyDTO.entity2dto(list.get(0), dto); 
		}else{
			dto.setSign(CURRENCY_CNY);//默认
		}
		return dto;
	}
	/**
	 * 添加修改货币信息
	 * @param dto
	 */ 	
	@Transactional
	public String saveOrUpdateCurrency(CurrencyDTO dto){
		Currency entity = new Currency();
		CurrencyDTO.dto2entity(dto, entity);
		currencyDAO.merge(entity);
		dto.setCno(entity.getCno());
		try{
	    	appctx.setAttribute("currency_sign",entity.getSign());
		}catch (Exception e) {
		    LOGGER.error("session 获取失败");
		}
		return entity.getSign();
	}

	
	
	
}
