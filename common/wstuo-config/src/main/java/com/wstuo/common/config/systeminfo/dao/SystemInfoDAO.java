package com.wstuo.common.config.systeminfo.dao;

import com.wstuo.common.config.systeminfo.entity.SystemInfo;
import com.wstuo.common.dao.BaseDAOImplHibernate;

/**
 * SystemInfo DAO class
 * @author Will
 *
 */
public class SystemInfoDAO extends BaseDAOImplHibernate<SystemInfo> implements ISystemInfoDAO {

}
