package com.wstuo.common.config.cab.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;

import com.wstuo.common.config.cab.dto.CABMemberDTO;
import com.wstuo.common.config.cab.dto.CABMemberQueryDTO;
import com.wstuo.common.config.cab.service.ICABMemberService;
import com.wstuo.common.dto.PageDTO;
/**
 * 变更审批委员会成员Action类
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class CABMemberAction extends ActionSupport {
	/**
	 * 变更审批委员会成员业务实现接口
	 */
	@Autowired
	private ICABMemberService cabMemberService;
	private PageDTO cabMemberPager;
	private CABMemberQueryDTO cabMemberQueryDto;
	private CABMemberDTO cabMemberDto;
	private Long cabId;
	private Long[] cabMemberIds;
    private String sidx;
    private String sord;
    
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public ICABMemberService getCabMemberService() {
		return cabMemberService;
	}
	public void setCabMemberService(ICABMemberService cabMemberService) {
		this.cabMemberService = cabMemberService;
	}
	public PageDTO getCabMemberPager() {
		return cabMemberPager;
	}
	public void setCabMemberPager(PageDTO cabMemberPager) {
		this.cabMemberPager = cabMemberPager;
	}
	public CABMemberQueryDTO getCabMemberQueryDto() {
		return cabMemberQueryDto;
	}
	public void setCabMemberQueryDto(CABMemberQueryDTO cabMemberQueryDto) {
		this.cabMemberQueryDto = cabMemberQueryDto;
	}
	public CABMemberDTO getCabMemberDto() {
		return cabMemberDto;
	}
	public void setCabMemberDto(CABMemberDTO cabMemberDto) {
		this.cabMemberDto = cabMemberDto;
	}
	public Long getCabId() {
		return cabId;
	}
	public void setCabId(Long cabId) {
		this.cabId = cabId;
	}
	public Long[] getCabMemberIds() {
		return cabMemberIds;
	}
	public void setCabMemberIds(Long[] cabMemberIds) {
		this.cabMemberIds = cabMemberIds;
	}
	
	/**
	 * 保存审批委员
	 * @return String
	 */
	public String addCabMember(){
		cabMemberService.saveCABMember(cabMemberDto);
		return SUCCESS;
	}
	
	/**
	 * 编辑审批委员
	 * @return String
	 */
	public String editCabMember(){
		cabMemberService.editCABMember(cabMemberDto);
		return SUCCESS;
	}
	
	/**
	 * 删除审批委员
	 * @return String
	 */
	public String deleteCABMember(){
		cabMemberService.deleteCABMember(cabId, cabMemberIds);
		return SUCCESS;
	}
}
