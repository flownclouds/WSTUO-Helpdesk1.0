package com.wstuo.common.config.template.dao;

import java.util.List;

import com.wstuo.common.config.template.dto.TemplateDTO;
import com.wstuo.common.config.template.entity.Template;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;
/**
 * 模板DAO接口
 * @author QXY
 *
 */
public interface ITemplateDAO extends IEntityDAO<Template>{
	/**
	 * 分页查找内容模板列表.
	 * @param dto 查询DTO
	 * @param start 开始数据行
	 * @param limit 每页数据条数
	 * @param sord 排序规则
	 * @param sidx 排序的属性
	 * @return PageDTO 分页数据
	 */
	PageDTO findTemplatePager(TemplateDTO dto,int start, int limit, String sord,String sidx);

	/**
	 * 查询所有内容模板
	 * @param dto
	 */
	List<Template> findAllTemplate(TemplateDTO dto);
	/**
	 * 查询关联表单的数量
	 * @param formId
	 * @return
	 */
	Integer findTemplateNumByFormId(Long[] formIds);

}
