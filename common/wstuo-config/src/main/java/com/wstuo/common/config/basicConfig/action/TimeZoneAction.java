package com.wstuo.common.config.basicConfig.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.config.basicConfig.dto.TimeZoneDTO;
import com.wstuo.common.config.basicConfig.service.ITimeZoneService;
import com.wstuo.common.security.utils.AppContext;

/**
 * 时区设置ACTION.
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class TimeZoneAction extends ActionSupport{

	@Autowired
	private ITimeZoneService timeZoneService;
	private String clientTimeZone;
	private TimeZoneDTO timeZoneDTO; 
	@Autowired
	private AppContext appctx;
	public TimeZoneDTO getTimeZoneDTO() {
		return timeZoneDTO;
	}

	public void setTimeZoneDTO(TimeZoneDTO timeZoneDTO) {
		this.timeZoneDTO = timeZoneDTO;
	}


	public String getClientTimeZone() {
		return clientTimeZone;
	}

	public void setClientTimeZone(String clientTimeZone) {
		this.clientTimeZone = clientTimeZone;
	}

	/**
	 * 查找时区信息.
	 */
	public String findTimeZone(){
		
		timeZoneDTO=timeZoneService.findTimeZone();
		
		return SUCCESS;
	}
	
	/**
	 * 保存时区信息
	 */
	public String saveTimeZone(){
		timeZoneService.saveOrUpdateTomeZone(timeZoneDTO);
		return SUCCESS;
	}
	
	/**
	 * 加载时区信息
	 */
	public String loadClientTimeZone(){
    	appctx.setAttribute("clientTimeZone", clientTimeZone.replace("^", "+"));
    	return SUCCESS;
    }
	
}
