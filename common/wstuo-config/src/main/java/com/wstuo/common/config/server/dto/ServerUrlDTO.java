package com.wstuo.common.config.server.dto;

import com.wstuo.common.dto.BaseDTO;
/**
 * ServerUrl DTO Class
 * @author Will
 *
 */
@SuppressWarnings("serial")
public class ServerUrlDTO extends BaseDTO{
	private Long Id ;
	
	private String urlName;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getUrlName() {
		return urlName;
	}

	public void setUrlName(String urlName) {
		this.urlName = urlName;
	}

	
	
	
}
