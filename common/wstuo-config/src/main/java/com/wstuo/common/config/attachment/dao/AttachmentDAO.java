package com.wstuo.common.config.attachment.dao;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.config.attachment.dto.AttachmentDTO;
import com.wstuo.common.config.attachment.entity.Attachment;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.DaoUtils;
import com.wstuo.common.util.StringUtils;

/**
 * 附件DAO
 * 
 * @author QXY 修改时间2011-02-11
 */
public class AttachmentDAO extends BaseDAOImplHibernate<Attachment> implements
		IAttachmentDAO {
	final static Logger LOGGER = Logger.getLogger(AttachmentDAO.class);
	public PageDTO findattachmentPager(AttachmentDTO dto, int start, int limit,
			String sidx, String sord) {
		DetachedCriteria dc = DetachedCriteria.forClass(Attachment.class);
		if (dto != null) {// 查询
			if (StringUtils.hasText(dto.getAttachmentName())) {
				dc.add(Restrictions.like("attachmentName",
						dto.getAttachmentName(), MatchMode.ANYWHERE));
			}
			if (StringUtils.hasText(dto.getUrl())) {
				dc.add(Restrictions.like("url", dto.getUrl(),
						MatchMode.ANYWHERE));
			}
			if (StringUtils.hasText(dto.getCreator())) {
				dc.add(Restrictions.like("creator", dto.getCreator(),
						MatchMode.ANYWHERE));
			}
			if (StringUtils.hasText(dto.getType())) {
				dc.add(Restrictions.or(Restrictions.like("type", dto.getType(),
						MatchMode.ANYWHERE), Restrictions.isNull("type")));
			}
			// 时间搜索
			if (dto.getStartTime() != null && dto.getEndTime() == null) {
				dc.add(Restrictions.ge("createTime", dto.getStartTime()));
			}
			if (dto.getStartTime() == null && dto.getEndTime() != null) {
				Calendar endTimeCl = new GregorianCalendar();
				endTimeCl.setTime(dto.getEndTime());
				endTimeCl.set(Calendar.DATE, endTimeCl.get(Calendar.DATE) + 1);
				dc.add(Restrictions.le("createTime", endTimeCl.getTime()));
			}
			if (dto.getStartTime() != null && dto.getEndTime() != null) {
				Calendar endTimeCl = new GregorianCalendar();
				endTimeCl.setTime(dto.getEndTime());
				endTimeCl.set(Calendar.DATE, endTimeCl.get(Calendar.DATE) + 1);
				dc.add(Restrictions.and(
						Restrictions.le("createTime", endTimeCl.getTime()),
						Restrictions.ge("createTime", dto.getStartTime())));
			}
			if (StringUtils.hasText(dto.getStartSize())
					&& (isInteger(dto.getStartSize()) || isDouble(dto
							.getStartSize()))) {
				dc.add(Restrictions.ge("size",
						Long.parseLong(dto.getStartSize())));
			}
			if (StringUtils.hasText(dto.getEndSize())
					&& (isInteger(dto.getEndSize()) || isDouble(dto
							.getEndSize()))) {
				dc.add(Restrictions.le("size", Long.parseLong(dto.getEndSize())));
			}
			if (dto.getStartSize() != null
					&& dto.getEndSize() != null
					&& (isInteger(dto.getStartSize()) || isDouble(dto
							.getStartSize()))
					&& (isInteger(dto.getEndSize()) || isDouble(dto
							.getEndSize()))) {
				dc.add(Restrictions.between("size",
						Long.parseLong(dto.getStartSize()),
						Long.parseLong(dto.getEndSize())));
			}
		}

		// 排序
		dc = DaoUtils.orderBy(sidx, sord, dc);
		return super.findPageByCriteria(dc, start, limit);
	}

	/** * 判断字符串是否是整数 */
	public static boolean isInteger(String value) {
		boolean result = false;
		try {
			Integer.parseInt(value);
			result = true;
		} catch (NumberFormatException e) {
			LOGGER.error(e);
		}
		return result;
	}

	/** * 判断字符串是否是浮点数 */
	public static boolean isDouble(String value) {
		boolean result = false;
		try {
			Double.parseDouble(value);
			if (value.contains("."))
				result = true;
			
		} catch (NumberFormatException e) {
			LOGGER.error(e);
		}
		return result;
	}

	/**
	 * 批量删除
	 * 
	 * @param ids
	 */
	public void delete(Long[] ids) {

		super.deleteByIds(ids);

	}

	/**
	 * 删除附件
	 */
	public void deleteKnowlegdeAttr() {

	}

}
