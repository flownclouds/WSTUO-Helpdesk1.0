package com.wstuo.common.config.onlinelog.service;

import java.util.ArrayList;
import java.util.List;
import com.wstuo.common.config.onlinelog.dao.UserOptLogDAO;
import com.wstuo.common.config.onlinelog.dto.UserOptLogDTO;
import com.wstuo.common.config.onlinelog.dto.UserOptLogQueryDTO;
import com.wstuo.common.config.onlinelog.entity.UserOptLog;
import com.wstuo.common.dto.PageDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
/**
 * 用户操作业务类.
 * @author QXY date 2011-02-11
 *
 */
public class UserOptLogServiceUtil implements IUserOptLogServiceUtil {

	@Autowired
    private UserOptLogDAO useroptlogDAO;
  
	/**
	 * 分页查询用户操作日志.
	 * @param useroptlogQueryDTO 查询DTO
	 * @param start 开始数据行
	 * @param limit 每页数据条数
	 * @return PageDTO 分页数据
	 */
    @SuppressWarnings("unchecked")
	@Transactional
    public PageDTO findUserOptLogByPager(UserOptLogQueryDTO useroptlogQueryDTO,
        int start, int limit) {
        PageDTO p = useroptlogDAO.findPager(useroptlogQueryDTO, start,
                limit);
        List<UserOptLog> useroptlog = (List<UserOptLog>) p.getData();
        List<UserOptLogDTO> dtos = new ArrayList<UserOptLogDTO>(useroptlog
                .size());
        for (UserOptLog entity : useroptlog) {
            UserOptLogDTO dto = new UserOptLogDTO();
            dto.entity2dto(entity, dto);
            //entity2dto(entity, dto);
            dtos.add(dto);
        }
        p.setData(dtos);
        return p;
    }
 
    /**
     * 保存操作日志.
     * @param entity
     */
    @Transactional
    public void saveUserOptLog(UserOptLog entity) {
        useroptlogDAO.save(entity);
    }
    
    /**
     * 保存操作日志.
     * @param dto
     */
    @Transactional
    public void saveUserOptLog(UserOptLogDTO dto) {
        UserOptLog entity = new UserOptLog();
        dto.dto2entity(dto, entity);
        useroptlogDAO.save(entity);
        dto.setId(entity.getId());
    }

//    /**
//     * dto2entity
//     * @param dto
//     * @param entity
//     */
//    private static void dto2entity(UserOptLogDTO dto, UserOptLog entity) {
//        try {
//            BeanUtils.copyProperties(entity, dto);
//        } catch (Exception ex) {
//
//            throw new ApplicationException(ex);
//        }
//    }

//    /**
//     * entity2dto
//     * @param entity
//     * @param dto
//     */
//    private static void entity2dto(UserOptLog entity, UserOptLogDTO dto) {
//        try {
//            BeanUtils.copyProperties(dto, entity);
//        } catch (Exception ex) {
//
//            throw new ApplicationException(ex);
//        }
//    }
}