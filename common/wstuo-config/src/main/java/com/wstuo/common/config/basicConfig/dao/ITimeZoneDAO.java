package com.wstuo.common.config.basicConfig.dao;

import com.wstuo.common.config.basicConfig.entity.TimeZone;
import com.wstuo.common.dao.IEntityDAO;

public interface ITimeZoneDAO extends IEntityDAO<TimeZone>{

}