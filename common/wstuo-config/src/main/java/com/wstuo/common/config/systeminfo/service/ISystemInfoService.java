package com.wstuo.common.config.systeminfo.service;

public interface ISystemInfoService {
	/**
	 * 初始化系统信息数据
	 */
	void initSystemInfoData(String languageVersion);
}
