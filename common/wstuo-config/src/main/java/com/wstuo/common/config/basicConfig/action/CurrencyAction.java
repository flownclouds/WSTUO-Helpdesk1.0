package com.wstuo.common.config.basicConfig.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.config.basicConfig.dto.CurrencyDTO;
import com.wstuo.common.config.basicConfig.dto.CurrencyQueryDTO;
import com.wstuo.common.config.basicConfig.service.ICurrencyService;
import com.wstuo.common.dto.PageDTO;

/**
 * 货币查找Action
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class CurrencyAction extends ActionSupport{

	@Autowired
	private ICurrencyService currencyService;
	
	private PageDTO pageDTO;
	private int page = 1;
	private int rows = 10;
	private CurrencyDTO currencyDTO;
	private CurrencyQueryDTO currencyQueryDTO=new CurrencyQueryDTO();
	private Long [] cnos;
	private String defaultCurrency;
    private String sign;
	private String sidx;
	private String sord;

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public Long[] getCnos() {
		return cnos;
	}

	public void setCnos(Long[] cnos) {
		this.cnos = cnos;
	}

	public CurrencyDTO getCurrencyDTO() {
		return currencyDTO;
	}

	public void setCurrencyDTO(CurrencyDTO currencyDTO) {
		this.currencyDTO = currencyDTO;
	}

	public CurrencyQueryDTO getCurrencyQueryDTO() {
		return currencyQueryDTO;
	}

	public void setCurrencyQueryDTO(CurrencyQueryDTO currencyQueryDTO) {
		this.currencyQueryDTO = currencyQueryDTO;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public PageDTO getPageDTO() {
		return pageDTO;
	}

	public void setPageDTO(PageDTO pageDTO) {
		this.pageDTO = pageDTO;
	}

	
	public String getDefaultCurrency() {
		return defaultCurrency;
	}

	public void setDefaultCurrency(String defaultCurrency) {
		this.defaultCurrency = defaultCurrency;
	}
	
	
	/**
	 * 添加或修改货币信息.
	 */
	public String saveOrUpdateCurrency(){
		sign = currencyService.saveOrUpdateCurrency(currencyDTO);
		return "result";
	}
	

	/**
	 * 查询默认货币信息
	 */
	public String findDefaultCurrency(){
		currencyDTO=currencyService.findDefaultCurrency();
		return "currencyDTO";
	}
	
}
