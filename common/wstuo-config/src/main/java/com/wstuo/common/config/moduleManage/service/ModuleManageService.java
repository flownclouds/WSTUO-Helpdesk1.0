package com.wstuo.common.config.moduleManage.service;

import java.io.File;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;


import com.wstuo.common.config.moduleManage.dao.IModuleManageDAO;
import com.wstuo.common.config.moduleManage.dto.ModuleManageDTO;
import com.wstuo.common.config.moduleManage.entity.ModuleManage;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.utils.AppContext;
/**
 * 模块管理Service层
 * @author Wstuo
 *
 */
public class ModuleManageService implements IModuleManageService {

	final static Logger LOGGER = Logger.getLogger(ModuleManageService.class);
	    
	@Autowired
	private IModuleManageDAO moduleManageDAO;
	@Autowired
	private AppContext appctx;
	/**
	 * 分页查询模块
	 * 
	 * @param queryDto
	 * @param sord
	 * @param sidx
	 * @return PageDTO
	 */
	public PageDTO findModuleManagePager(ModuleManageDTO queryDto,
			String sord, String sidx) {
		PageDTO pageDto = moduleManageDAO.findPager(queryDto, sord, sidx);
		return pageDto;
	}

	/**
	 * 查询所有模块
	 * 
	 * @return  List<ModuleManage>
	 */
	@Transactional
	public List<ModuleManage> findAll() {
		List<ModuleManage> mos = moduleManageDAO.findAllC();
		return mos;
	}

	/**
	 * 保存模块信息
	 * 
	 * @param moduleManage
	 */
	@Transactional
	public void saveModule(ModuleManageDTO dto) {
		ModuleManage entity=new ModuleManage();
		ModuleManageDTO.dto2entity(dto, entity);
		entity.setCreateName(appctx.getCurrentLoginName());
		entity.setCreateTime(new Date());
		entity.setDataFlag((byte) 0);
		moduleManageDAO.saveC(entity);
		
	}


	@Transactional
	public void moduleDisable(Long id){
		ModuleManage entity=moduleManageDAO.findById(id);
		if(entity.getDataFlag()==0){
			entity.setDataFlag((byte) 99);
		}else if(entity.getDataFlag()==99){
			entity.setDataFlag((byte) 0);
		}
			
	}
	
	/**
	 * 修改模块显示顺序
	 * @param dto
	 */
	@Transactional
	public void editModule(ModuleManageDTO dto){
		ModuleManage entity=moduleManageDAO.findById(dto.getModuleId());
		entity.setShowSort(dto.getShowSort());
		entity.setDescription(dto.getDescription());
		entity.setTitle(dto.getTitle());
		entity.setPid(dto.getPid());
	}
	

}
