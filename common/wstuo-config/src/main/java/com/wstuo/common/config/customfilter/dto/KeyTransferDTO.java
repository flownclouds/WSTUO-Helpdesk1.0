package com.wstuo.common.config.customfilter.dto;

public class KeyTransferDTO {
	public String rowKey;//列键
	public String colKey;//行键
	public String rowValue;//列值
	public String colValue;//行值
	public String customFilterNo;//过滤器编号
	private String stisticField;//统计项编号
	private String entityClass;//实体路径
	private String filterCategory;//过滤器类型
	
	public String getRowKey() {
		return rowKey;
	}
	public void setRowKey(String rowKey) {
		this.rowKey = rowKey;
	}
	public String getColKey() {
		return colKey;
	}
	public void setColKey(String colKey) {
		this.colKey = colKey;
	}
	public String getColValue() {
		return colValue;
	}
	public void setColValue(String colValue) {
		this.colValue = colValue;
	}
	public String getRowValue() {
		return rowValue;
	}
	public void setRowValue(String rowValue) {
		this.rowValue = rowValue;
	}
	public String getCustomFilterNo() {
		return customFilterNo;
	}
	public void setCustomFilterNo(String customFilterNo) {
		this.customFilterNo = customFilterNo;
	}
	public String getStisticField() {
		return stisticField;
	}
	public void setStisticField(String stisticField) {
		this.stisticField = stisticField;
	}
	public String getEntityClass() {
		return entityClass;
	}
	public void setEntityClass(String entityClass) {
		this.entityClass = entityClass;
	}
	public String getFilterCategory() {
		return filterCategory;
	}
	public void setFilterCategory(String filterCategory) {
		this.filterCategory = filterCategory;
	}
}
