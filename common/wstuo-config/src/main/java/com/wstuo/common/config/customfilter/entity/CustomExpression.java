package com.wstuo.common.config.customfilter.entity;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.wstuo.common.entity.BaseEntity;




/**
 * entity of CustomExpression
 * @author Mars
 * */
@SuppressWarnings( {"serial",
    "rawtypes"
} )
@Entity
public class CustomExpression extends BaseEntity{
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
	private Long expId;
	
	private String propDisplayName;//变量显示
	
	private String propName;//变量名称
	
	private String displayOperator;//操作显示
	
	private String operator;//操作
	
	private String propValue;//值
	
	private String propDisplayValue;//值显示
	
	private String joinType;//连接类别---and 还是or
	
	private String propType;//变量类型int String time.....
	
	 /**
     * RulePattern
     */
    @ManyToOne
	private CustomFilter customFilter; //关联Filter

	public Long getExpId() {
		return expId;
	}

	public void setExpId(Long expId) {
		this.expId = expId;
	}

	public String getPropName() {
		return propName;
	}

	public void setPropName(String propName) {
		this.propName = propName;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getPropValue() {
		return propValue;
	}

	public void setPropValue(String propValue) {
		this.propValue = propValue;
	}

	public String getJoinType() {
		return joinType;
	}

	public void setJoinType(String joinType) {
		this.joinType = joinType;
	}

	public CustomFilter getCustomFilter() {
		return customFilter;
	}

	public void setCustomFilter(CustomFilter customFilter) {
		this.customFilter = customFilter;
	}

	public String getPropType() {
		return propType;
	}

	public void setPropType(String propType) {
		this.propType = propType;
	}

	public String getPropDisplayValue() {
		return propDisplayValue;
	}

	public void setPropDisplayValue(String propDisplayValue) {
		this.propDisplayValue = propDisplayValue;
	}

	public String getPropDisplayName() {
		return propDisplayName;
	}

	public void setPropDisplayName(String propDisplayName) {
		this.propDisplayName = propDisplayName;
	}

	public String getDisplayOperator() {
		return displayOperator;
	}

	public void setDisplayOperator(String displayOperator) {
		this.displayOperator = displayOperator;
	}

}
