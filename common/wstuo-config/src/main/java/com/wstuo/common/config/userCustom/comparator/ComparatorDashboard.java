package com.wstuo.common.config.userCustom.comparator;
import java.util.Comparator;

import com.wstuo.common.config.userCustom.entity.Dashboard;
/**
 * 面板比較器
 * @author QXY
 *
 */
@SuppressWarnings("rawtypes")
public class ComparatorDashboard implements Comparator{
	public int compare(Object arg0, Object arg1) {
		Dashboard d1=(Dashboard)arg0;
		Dashboard d2=(Dashboard)arg1;
		int flag=d1.getSortNo().compareTo(d2.getSortNo());
		if(flag==0){
			flag = d1.getDashboardName().compareTo(d2.getDashboardName());
		}
		return flag;
	}
	
}
