package com.wstuo.common.sla.dao;


import java.util.List;

import com.wstuo.common.sla.dto.SLARuleQueryDTO;
import com.wstuo.common.sla.entity.SLARule;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.StringUtils;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * SLA规则DAO类
 * @author QXY
 *
 */
public class SLARuleDAO extends BaseDAOImplHibernate<SLARule>
    implements ISLARuleDAO {

	/**
	 * 分页查找数据.
	 * @param qdto 查询DTO
	 * @param start 开始数据行
	 * @param limit 每页数据条数
	 * @return PageDTO 分页数据
	 */
    public PageDTO findPager(SLARuleQueryDTO qdto, String sidx, String sord,int start, int limit) {

        final DetachedCriteria dc = DetachedCriteria.forClass(SLARule.class);

        if (qdto != null) {
        	if (qdto.getRulePackageNo() != null) {
                dc.createAlias("rulePackage", "package")
                  .add(Restrictions.eq("package.rulePackageNo",
                        qdto.getRulePackageNo()));
            }
            if (qdto.getContractNo() != null) {

                dc.createAlias("slaContract", "contract")
                  .add(Restrictions.eq("contract.contractNo",
                        qdto.getContractNo()));
            }
            if (StringUtils.hasText(qdto.getRuleName())) {

                dc.add(Restrictions.like("ruleName", qdto.getRuleName(),
                        MatchMode.ANYWHERE));
            }

            if (StringUtils.hasText(qdto.getDescription())) {

                dc.add(Restrictions.like("description",
                        qdto.getDescription(), MatchMode.ANYWHERE));
            }
        }
        
        
        if(sidx!=null && sord!=null){
        	
        	if("desc".equals(sord)){
        		
        		dc.addOrder(Order.desc(sidx));
        		
        	}else{
        		dc.addOrder(Order.asc(sidx));
        	}
        }else{
        	dc.addOrder(Order.desc("ruleNo"));
        }

        
        return super.findPageByCriteria(dc, start, limit);
    }
    
    
    /**
     * 查询SLARule
     * @param qdto
     * @return SLARule List
     */
    public List<SLARule> findSLARule(SLARuleQueryDTO qdto){
    	final DetachedCriteria dc = DetachedCriteria.forClass(SLARule.class);
    	if (qdto.getContractNo() != null) {
            dc.createAlias("slaContract", "contract")
            	.add(Restrictions.eq("contract.contractNo",qdto.getContractNo()));
        }
    	if (StringUtils.hasText(qdto.getRuleName())) {
            dc.add(Restrictions.like("ruleName", qdto.getRuleName(),
                    MatchMode.ANYWHERE));
        }
    	return super.getHibernateTemplate().findByCriteria(dc);
    }
    
    
    /**
     * 查询SLARule
     * @param qdto
     * @return SLARule List
     */
    public SLARule findUniqueSLARule(SLARuleQueryDTO qdto){
    	final DetachedCriteria dc = DetachedCriteria.forClass(SLARule.class);
    	SLARule rule = null;
    	if (qdto.getContractNo() != null) {
            dc.createAlias("slaContract", "contract")
            	.add(Restrictions.eq("contract.contractNo",qdto.getContractNo()));
        }
    	if (StringUtils.hasText(qdto.getRuleName())) {
            dc.add(Restrictions.eq("ruleName", qdto.getRuleName()));
        }
    	List<SLARule> list = super.getHibernateTemplate().findByCriteria(dc);
    	if (list != null && list.size() > 0)
    		rule = list.get(0);
    	return rule;
    }
    
    
}