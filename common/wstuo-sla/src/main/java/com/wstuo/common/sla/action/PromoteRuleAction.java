package com.wstuo.common.sla.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.sla.dto.PromoteRuleDTO;
import com.wstuo.common.sla.dto.PromoteRuleQueryDTO;
import com.wstuo.common.sla.entity.PromoteRule;
import com.wstuo.common.sla.service.IPromoteRuleService;
import com.wstuo.common.dto.PageDTO;

/**
 * 自动升级Action类.
 * @author QXY
 */
@SuppressWarnings("serial")
public class PromoteRuleAction extends ActionSupport{
	

	/**
	 * 自动升级规则业务类接口
	 */
    @Autowired
    private IPromoteRuleService promoteRuleService;
    /**
     * 页数
     */
    private int page = 1;
    /**
     * 记录数
     */
    private int rows = 10;
    /**
     * 排序的属性
     */
    private String sidx;
    /**
     * 排序方式
     */
    private String sord;
    /**
     * 分页DTO
     */
    private PageDTO pageDTO;
    /**
     * 自动升级规则DTO
     */
    private PromoteRuleDTO promoteRuleDTO = new PromoteRuleDTO();
    /**
     * 自动升级规则查询DTO
     */
    private PromoteRuleQueryDTO promoteRuleQueryDTO = new PromoteRuleQueryDTO();
    /**
     * 自动升级规则实体
     */
    private PromoteRule promoteRule = new PromoteRule();
    /**
     * Sla协议ID
     */
    private Long contractNo;
    /**
     * 规则ID
     */
    private Long ruleNo;
    /**
     * 规则ID数组
     */
    private Long[] ruleNos;
    /**
     * 升级时间：天
     */
    public Integer promoteRuleDay;
    /**
     * 升级时间：小时
     */
    public Integer promoteRuleHour;
    /**
     * 升级时间：分钟
     */
    public Integer promoteRuleMinute;
    /**
     * 规则类型
     */
    public String ruleType;
    

    
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public String getRuleType() {
		return ruleType;
	}
	public void setRuleType(String ruleType) {
		this.ruleType = ruleType;
	}
	public Integer getPromoteRuleDay() {
		return promoteRuleDay;
	}
	public void setPromoteRuleDay(Integer promoteRuleDay) {
		this.promoteRuleDay = promoteRuleDay;
	}
	public Integer getPromoteRuleHour() {
		return promoteRuleHour;
	}
	public void setPromoteRuleHour(Integer promoteRuleHour) {
		this.promoteRuleHour = promoteRuleHour;
	}
	public Integer getPromoteRuleMinute() {
		return promoteRuleMinute;
	}
	public void setPromoteRuleMinute(Integer promoteRuleMinute) {
		this.promoteRuleMinute = promoteRuleMinute;
	}
	public IPromoteRuleService getPromoteRuleService() {
		return promoteRuleService;
	}
	public void setPromoteRuleService(IPromoteRuleService promoteRuleService) {
		this.promoteRuleService = promoteRuleService;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	public PageDTO getPageDTO() {
		return pageDTO;
	}
	public void setPageDTO(PageDTO pageDTO) {
		this.pageDTO = pageDTO;
	}
	public PromoteRuleDTO getPromoteRuleDTO() {
		return promoteRuleDTO;
	}
	public void setPromoteRuleDTO(PromoteRuleDTO promoteRuleDTO) {
		this.promoteRuleDTO = promoteRuleDTO;
	}
	public PromoteRuleQueryDTO getPromoteRuleQueryDTO() {
		return promoteRuleQueryDTO;
	}
	public void setPromoteRuleQueryDTO(PromoteRuleQueryDTO promoteRuleQueryDTO) {
		this.promoteRuleQueryDTO = promoteRuleQueryDTO;
	}
	public PromoteRule getPromoteRule() {
		return promoteRule;
	}
	public void setPromoteRule(PromoteRule promoteRule) {
		this.promoteRule = promoteRule;
	}
	public Long getContractNo() {
		return contractNo;
	}
	public void setContractNo(Long contractNo) {
		this.contractNo = contractNo;
	}
	public Long getRuleNo() {
		return ruleNo;
	}
	public void setRuleNo(Long ruleNo) {
		this.ruleNo = ruleNo;
	}
	public Long[] getRuleNos() {
		return ruleNos;
	}
	public void setRuleNos(Long[] ruleNos) {
		this.ruleNos = ruleNos;
	}
    
	/**
	 * 分页查找自动升级列表.
	 * @return String 自动升级分页DTO
	 */
    public String findPromoteRulePage() {
    	promoteRuleQueryDTO.setContractNo(contractNo);
    	promoteRuleQueryDTO.setRuleType(ruleType);
        int start = (page - 1) * rows;

        pageDTO = promoteRuleService.findPromoteRuleByContractNo(promoteRuleQueryDTO,
                start, rows,sidx,sord);
        pageDTO.setPage(page);
        pageDTO.setRows(rows);

        return SUCCESS;
    }
    
    /**
     * 根据ID查找自动升级.
     * @return String 自动升级规则实体
     */
    public String findPromoteRuleById() {

    	promoteRule = promoteRuleService.findPromoteRuleById(ruleNo);

        return "automaticEscalationRules";
    }

    /**
     * 修改自动升级.
     * @return String 无
     */
    public String mergePromoteRule() {
    	
    	Long time=promoteRuleDay*86400+promoteRuleHour*3600+promoteRuleMinute*60L;
    	promoteRuleDTO.setRuleTime(time);
    	promoteRuleService.mergePromoteRuleEntity(promoteRuleDTO);
        return SUCCESS;
    }

    /**
     * 添加自动升级.
     * @return String 无
     */
    public String savePromoteRule() {
    	Long time=promoteRuleDay*86400+promoteRuleHour*3600+promoteRuleMinute*60L;
    	promoteRuleDTO.setRuleTime(time);
    	promoteRuleService.savePromoteRuleEntity(promoteRuleDTO);
        return SUCCESS;
    }

    /**
     * 删除自动升级.
     * @return String 无
     */
    public String deletePromoteRules() {

    	promoteRuleService.removePromoteRules(ruleNos);
        return SUCCESS;
    }
    
    
	
}

