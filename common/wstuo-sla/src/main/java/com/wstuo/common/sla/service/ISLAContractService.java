package com.wstuo.common.sla.service;

import java.io.InputStream;
import java.util.List;

import com.wstuo.common.sla.dto.SLAContractDTO;
import com.wstuo.common.sla.dto.SLAContractQueryDTO;
import com.wstuo.common.dto.PageDTO;



/**
 * SLA业务类接口.
 * @author QXY
 */
public interface ISLAContractService {
	

	/**
	 * 分页查找数据.
	 * @param queryDTO 查询DTO
	 * @param start 开始数据行
	 * @param limit 每页数据条数
	 * @return PageDTO 分页数据
	 */
	PageDTO findSLAContractByPager(SLAContractQueryDTO queryDTO, int start,int limit,String sidx,String sord);
	/**
	 * 查找所有的SLA协议
	 * @return List<SLAContractDTO>
	 */
	List<SLAContractDTO> findAllSLAContract();
	/**
	 * 保存SLA
	 * @param dto  sla协议DTO
	 */
    void saveSLAContract(SLAContractDTO dto);

    /**
     * 根据ID删除SLA
     * @param no SLA编号
     * @return boolean 
     */
    boolean removeSLAContract(Long no);

    /**
     * 批量删除SLA
     * @param nos SLA编号数组
     * @return boolean
     */
    boolean removeSLAContracts(Long[] nos);

    /**
     * 保存（修改）SLA信息
     * @param dto  sla协议DTO
     */
    void mergeSLAContract(SLAContractDTO dto);

   
    /**
     * 根据ID查找SLA信息
     * @param no SLA编号
     * @return SLAContractDTO
     */
    SLAContractDTO findById(Long no);
    
    /**
     * 根据SLA编号查找被服务机构编号数组.
     * @param contractNo SLA编号
     * @return Long [] 被服务机构编号数组
     */
    Long [] findByServicesNosByContractNo(Long contractNo);
    
    /**
     * 导出CSV.
     * @param qdto SLAContractQueryDTO
     */
    InputStream exportSLA(SLAContractQueryDTO qdto);
    
    
    /**
     * 导入CSV.
     * @param importFilePath 导入文件路径
     * @return String
     */
    String importSLA(String importFilePath);
    
    
    void defaultSLAContract();
    
    /**
     * 根据SLA编号查找关联服务
     * @param contractNo 关联服务NO
     * @return  Long[]
     */
    Long[] findByServicesDirNosByContractNo(Long contractNo);
    
}